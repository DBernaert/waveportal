object PortalNotActiveFrm: TPortalNotActiveFrm
  Left = 0
  Top = 0
  Width = 1040
  Height = 856
  Layout = 'vbox'
  LayoutAttribs.Align = 'center'
  LayoutAttribs.Pack = 'center'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerError: TUniContainerPanel
    Left = 224
    Top = 32
    Width = 585
    Height = 665
    Hint = ''
    ParentColor = False
    TabOrder = 0
    object ImageError: TUniImage
      Left = 37
      Top = 0
      Width = 512
      Height = 512
      Hint = ''
      AutoSize = True
      Url = 'images/wv_wavedesk_logo_500.svg'
    end
    object LblDutch: TUniLabel
      Left = 112
      Top = 536
      Width = 361
      Height = 25
      Hint = ''
      Caption = 'De portaalsite is momenteel niet actief'
      ParentFont = False
      Font.Height = -21
      TabOrder = 2
      LayoutConfig.Cls = 'boldtext'
    end
    object LblFrench: TUniLabel
      Left = 98
      Top = 568
      Width = 389
      Height = 25
      Hint = ''
      Caption = 'Le site portail n'#39'est actuellement pas actif'
      ParentFont = False
      Font.Height = -21
      TabOrder = 3
      LayoutConfig.Cls = 'boldtext'
    end
    object LblEnglish: TUniLabel
      Left = 126
      Top = 600
      Width = 332
      Height = 25
      Hint = ''
      Caption = 'The portal is currently not available'
      ParentFont = False
      Font.Height = -21
      TabOrder = 4
      LayoutConfig.Cls = 'boldtext'
    end
  end
end
