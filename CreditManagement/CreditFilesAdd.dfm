object CreditFilesAddFrm: TCreditFilesAddFrm
  Left = 0
  Top = 0
  ClientHeight = 777
  ClientWidth = 1081
  Caption = 'Kredieten'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditStatus
  Images = UniMainModule.ImageList
  ImageIndex = 17
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = FormCreate
  TextHeight = 15
  object PageControlCreditDemand: TUniPageControl
    Left = 16
    Top = 152
    Width = 1049
    Height = 569
    Hint = ''
    ActivePage = TabSheetPlan
    Images = UniMainModule.ImageList
    LayoutConfig.Cls = 'boldtext'
    LayoutConfig.Region = 'center'
    TabOrder = 8
    object TabSheetPlan: TUniTabSheet
      Hint = ''
      ImageIndex = 21
      Caption = 'Financieel plan'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object LblPurchase: TUniLabel
        Left = 16
        Top = 88
        Width = 48
        Height = 13
        Hint = ''
        Caption = 'Aankoop'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 2
        LayoutConfig.Cls = 'boldtext'
      end
      object EditPurchasePriceProperty: TUniDBFormattedNumberEdit
        Left = 24
        Top = 112
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_PRICE_PROPERTY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 3
        SelectOnFocus = True
        FieldLabel = 'Aankoopprijs'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPurchaseCostProperty: TUniDBFormattedNumberEdit
        Left = 272
        Top = 112
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_COST_PROPERTY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 4
        SelectOnFocus = True
        FieldLabel = 'Registratierechten'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPropertyType: TUniDBLookupComboBox
        Left = 16
        Top = 16
        Width = 489
        Height = 23
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsPropertyTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'PROPERTY_TYPE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 0
        Color = clWindow
        FieldLabel = 'Type pand'
        FieldLabelWidth = 120
        ForceSelection = True
        Style = csDropDown
      end
      object EditPropertyAddress: TUniDBEdit
        Left = 16
        Top = 48
        Width = 489
        Height = 22
        Hint = ''
        DataField = 'PROPERTY_ADDRESS'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 1
        FieldLabel = 'Adres'
        FieldLabelWidth = 120
        SelectOnFocus = True
      end
      object LblNewBuilding: TUniLabel
        Left = 16
        Top = 152
        Width = 64
        Height = 13
        Hint = ''
        Caption = 'Nieuwbouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 5
        LayoutConfig.Cls = 'boldtext'
      end
      object EditPurchasePriceGroundNewProp: TUniDBFormattedNumberEdit
        Left = 24
        Top = 176
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_PRICE_GROUND_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 6
        SelectOnFocus = True
        FieldLabel = 'Grond'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPurchaseCostNewProp: TUniDBFormattedNumberEdit
        Left = 272
        Top = 176
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_COST_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 7
        SelectOnFocus = True
        FieldLabel = 'Kosten'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostPriceBuildNewProp: TUniDBFormattedNumberEdit
        Left = 24
        Top = 208
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_BUILD_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 8
        SelectOnFocus = True
        FieldLabel = 'Bouw'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatBuildNewProp: TUniDBFormattedNumberEdit
        Left = 272
        Top = 208
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_BUILD_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 9
        SelectOnFocus = True
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostArchitectNewProp: TUniDBFormattedNumberEdit
        Left = 24
        Top = 240
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_ARCHITECT_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 10
        SelectOnFocus = True
        FieldLabel = 'Architect'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatArchitectNewProp: TUniDBFormattedNumberEdit
        Left = 272
        Top = 240
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_ARCHITECT_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 11
        SelectOnFocus = True
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostPriceStudiesNewProp: TUniDBFormattedNumberEdit
        Left = 24
        Top = 272
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_STUDIES_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 12
        SelectOnFocus = True
        FieldLabel = 'EPB / VCO'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatStudiesNewProp: TUniDBFormattedNumberEdit
        Left = 272
        Top = 272
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_STUDIES_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 13
        SelectOnFocus = True
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditOterCostsNewProp: TUniDBFormattedNumberEdit
        Left = 24
        Top = 304
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'OTHER_COSTS_NEW_PROP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 14
        SelectOnFocus = True
        FieldLabel = 'Overige kosten'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblRefinance: TUniLabel
        Left = 528
        Top = 16
        Width = 80
        Height = 13
        Hint = ''
        Caption = 'Herfinanciering'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 23
        LayoutConfig.Cls = 'boldtext'
      end
      object EditSaldoToTakeOverOtherCredits: TUniDBFormattedNumberEdit
        Left = 536
        Top = 48
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'SALDO_TO_TAKEOVER_OTHER_CREDITS'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 24
        SelectOnFocus = True
        FieldLabel = 'Saldo over te nemen kredieten'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditHandLighting: TUniDBFormattedNumberEdit
        Left = 536
        Top = 80
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'HANDLIGHTING'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 25
        SelectOnFocus = True
        FieldLabel = 'Handlichting'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditReinvestmentallowance: TUniDBFormattedNumberEdit
        Left = 536
        Top = 112
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'REINVESTMENT_ALLOWANCE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 26
        SelectOnFocus = True
        FieldLabel = 'Wederbeleggingsvergoeding'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblDiverseCosts: TUniLabel
        Left = 528
        Top = 152
        Width = 76
        Height = 13
        Hint = ''
        Caption = 'Diverse kosten'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 27
        LayoutConfig.Cls = 'boldtext'
      end
      object EditVariouscostsfile: TUniDBFormattedNumberEdit
        Left = 536
        Top = 176
        Width = 241
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VARIOUS_COSTS_FILE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 28
        SelectOnFocus = True
        FieldLabel = 'Dossier'
        FieldLabelWidth = 120
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVariousCostsEstimation: TUniDBFormattedNumberEdit
        Left = 784
        Top = 176
        Width = 241
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VARIOUS_COSTS_ESTIMATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 29
        SelectOnFocus = True
        FieldLabel = 'Schatting'
        FieldLabelWidth = 120
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPurchaseSumDebtBalansIns: TUniDBFormattedNumberEdit
        Left = 536
        Top = 208
        Width = 241
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_SUM_DEBTBAL_INS'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 30
        SelectOnFocus = True
        FieldLabel = 'Koopsom'
        FieldLabelWidth = 120
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVariousCostsOther: TUniDBFormattedNumberEdit
        Left = 536
        Top = 240
        Width = 241
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VARIOUS_COSTS_OTHER'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 32
        SelectOnFocus = True
        FieldLabel = 'Andere'
        FieldLabelWidth = 120
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblCosts: TUniLabel
        Left = 528
        Top = 312
        Width = 87
        Height = 13
        Hint = ''
        Caption = 'Waarborgkosten'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 33
        LayoutConfig.Cls = 'boldtext'
      end
      object EditMortGageRegistration: TUniDBFormattedNumberEdit
        Left = 536
        Top = 336
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MORTGAGE_REGISTRATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 34
        SelectOnFocus = True
        FieldLabel = 'Hypothecaire inschrijving'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditMortGageMandate: TUniDBFormattedNumberEdit
        Left = 536
        Top = 368
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MORTGAGE_MANDATE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 35
        SelectOnFocus = True
        FieldLabel = 'Hypothecair mandaat'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblFinancing: TUniLabel
        Left = 528
        Top = 440
        Width = 64
        Height = 13
        Hint = ''
        Caption = 'Financiering'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 37
        LayoutConfig.Cls = 'boldtext'
      end
      object EditOwnResources: TUniDBFormattedNumberEdit
        Left = 536
        Top = 464
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'OWN_RESOURCES'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 38
        SelectOnFocus = True
        FieldLabel = 'Eigen middelen'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditTotalInvestMent: TUniDBFormattedNumberEdit
        Left = 536
        Top = 400
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'TOTAL_INVESTMENT'
        DataSource = Ds_CreditFiles_Edit
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 36
        TabStop = False
        ReadOnly = True
        SelectOnFocus = True
        FieldLabel = '<b>Totale investering</b>'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditDemandedCredit: TUniDBFormattedNumberEdit
        Left = 536
        Top = 496
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'DEMANDED_CREDIT'
        DataSource = Ds_CreditFiles_Edit
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 39
        TabStop = False
        ReadOnly = True
        SelectOnFocus = True
        FieldLabel = '<b>Gevraagd kredietbedrag</b>'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object ImageLink: TUniImage
        Left = 512
        Top = 336
        Width = 16
        Height = 16
        Cursor = crHandPoint
        Hint = 'Link naar notaris.be'
        ShowHint = True
        ParentShowHint = False
        Stretch = True
        Url = 'images/wv_link_25.svg'
        OnClick = ImageLinkClick
      end
      object LblRenovation: TUniLabel
        Left = 16
        Top = 344
        Width = 63
        Height = 13
        Hint = ''
        Caption = 'Verbouwing'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 15
        LayoutConfig.Cls = 'boldtext'
      end
      object EditCostPriceBuildRenovate: TUniDBFormattedNumberEdit
        Left = 24
        Top = 368
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_BUILD_RENOVATE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 16
        SelectOnFocus = True
        FieldLabel = 'Bouw'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatBuildRenovation: TUniDBFormattedNumberEdit
        Left = 272
        Top = 368
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_BUILD_RENOVATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 17
        SelectOnFocus = True
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostArchitectRenovation: TUniDBFormattedNumberEdit
        Left = 24
        Top = 400
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_ARCHITECT_RENOVATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 18
        SelectOnFocus = True
        FieldLabel = 'Architect'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatArchitectRenovation: TUniDBFormattedNumberEdit
        Left = 272
        Top = 400
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_ARCHITECT_RENOVATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 19
        SelectOnFocus = True
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostPriceStudiesRenovation: TUniDBFormattedNumberEdit
        Left = 24
        Top = 432
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_STUDIES_RENOVATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 20
        SelectOnFocus = True
        FieldLabel = 'EPB / VCO'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatStudiesRenovation: TUniDBFormattedNumberEdit
        Left = 272
        Top = 432
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_STUDIES_RENOVATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 22
        SelectOnFocus = True
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditOtherCostsRenovation: TUniDBFormattedNumberEdit
        Left = 24
        Top = 464
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'OTHER_COSTS_RENOVATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 21
        SelectOnFocus = True
        FieldLabel = 'Overige'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditNotaryCost: TUniDBFormattedNumberEdit
        Left = 784
        Top = 208
        Width = 241
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_NOTARY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 31
        SelectOnFocus = True
        FieldLabel = 'Notaris'
        FieldLabelWidth = 120
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
    end
    object TabSheetProfile: TUniTabSheet
      Hint = ''
      ImageIndex = 66
      Caption = 'Risicoprofiel / persoonlijke voorkeuren'
      ExplicitHeight = 533
      object LblRiskprofile: TUniLabel
        Left = 16
        Top = 16
        Width = 64
        Height = 13
        Hint = ''
        Caption = 'Risicoprofiel'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 0
        LayoutConfig.Cls = 'boldtext'
      end
      object EditNoRisks: TUniDBCheckBox
        Left = 24
        Top = 40
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'NO_RISKS'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 1
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Geen risico'
        FieldLabelWidth = 180
      end
      object EditLimitedRisks: TUniDBCheckBox
        Left = 24
        Top = 64
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'LIMITED_RISKS'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 2
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Beperkt risico'
        FieldLabelWidth = 180
      end
      object EditHighRisks: TUniDBCheckBox
        Left = 24
        Top = 88
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'HIGH_RISKS'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 3
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Hoog risico'
        FieldLabelWidth = 180
      end
      object EditMaximumMonthlyCharge: TUniDBFormattedNumberEdit
        Left = 24
        Top = 112
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MAXIMUM_MONTHLY_CHARGE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 4
        SelectOnFocus = True
        FieldLabel = 'Maximale maandlast'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object LblPersonalPreferences: TUniLabel
        Left = 528
        Top = 16
        Width = 127
        Height = 13
        Hint = ''
        Caption = 'Persoonlijke voorkeuren'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 6
        LayoutConfig.Cls = 'boldtext'
      end
      object EditFreeChoiceBankAccount: TUniDBCheckBox
        Left = 536
        Top = 40
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'FREE_CHOICE_OF_BANKACCOUNT'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 7
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Vrije keuze zichtrekening'
        FieldLabelWidth = 180
      end
      object EditFreeChoiceDebtInsurance: TUniDBCheckBox
        Left = 536
        Top = 64
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'FREE_CHOICE_DEBT_INSURANCE'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 8
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Vrije keuze schuldsaldo'
        FieldLabelWidth = 180
      end
      object EditCoverageDebtInsurance: TUniDBFormattedNumberEdit
        Left = 536
        Top = 88
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'COVERAGE_DEBT_INSURANCE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 9
        SelectOnFocus = True
        FieldLabel = 'Dekkingsgraad'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditFreeChoiceFireInsurance: TUniDBCheckBox
        Left = 536
        Top = 120
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'FREE_CHOICE_FIRE_INSURANCE'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 10
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Vrije keuze brandverz.'
        FieldLabelWidth = 180
      end
      object EditCommentsContributor: TUniDBMemo
        Left = 24
        Top = 184
        Width = 489
        Height = 329
        Hint = ''
        DataField = 'COMMENTS_CONTRIBUTOR'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 11
        FieldLabel = 'Toelichting tussenpersoon'
        FieldLabelWidth = 180
        FieldLabelAlign = laTop
      end
      object EditCommentsAdmin: TUniDBMemo
        Left = 536
        Top = 184
        Width = 489
        Height = 329
        Hint = ''
        DataField = 'COMMENTS_USER'
        DataSource = Ds_CreditFiles_Edit
        ReadOnly = True
        TabOrder = 12
        TabStop = False
        FieldLabel = 'Toelichting dossierbeheerder'
        FieldLabelWidth = 180
        FieldLabelAlign = laTop
      end
      object EditMaximumDuration: TUniDBNumberEdit
        Left = 24
        Top = 144
        Width = 489
        Height = 22
        Hint = ''
        DataField = 'MAXIMUM_DURATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 5
        SelectOnFocus = True
        FieldLabel = 'Maximale looptijd'
        FieldLabelWidth = 180
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
    end
    object TabSheetStatus: TUniTabSheet
      Hint = ''
      ImageIndex = 17
      Caption = 'Algemeen'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 1090
      ExplicitHeight = 690
      object EditInternalRemarks: TUniDBMemo
        Left = 504
        Top = 32
        Width = 521
        Height = 481
        Hint = ''
        DataField = 'INTERNAL_REMARKS'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 9
        FieldLabel = 'Interne opmerkingen'
        FieldLabelWidth = 150
        FieldLabelAlign = laTop
      end
      object EditCreditType: TUniDBLookupComboBox
        Left = 16
        Top = 240
        Width = 473
        Height = 23
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsCreditTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'TYPE_OF_CREDIT'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Type krediet'
        FieldLabelWidth = 180
        Style = csDropDown
      end
      object ContainerDemanders: TUniContainerPanel
        Left = 16
        Top = 16
        Width = 473
        Height = 217
        Hint = ''
        ParentColor = False
        TabOrder = 0
        Layout = 'border'
        object ContainerFilter: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 473
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerFilterRight: TUniContainerPanel
            Left = 381
            Top = 0
            Width = 92
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alRight
            TabOrder = 1
            LayoutConfig.Region = 'east'
            object BtnAddDemander: TUniThemeButton
              Left = 0
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 1
              Images = UniMainModule.ImageList
              ImageIndex = 10
              OnClick = BtnAddDemanderClick
              ButtonTheme = uctPrimary
            end
            object BtnModifyDemander: TUniThemeButton
              Left = 32
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 2
              Images = UniMainModule.ImageList
              ImageIndex = 11
              OnClick = BtnModifyDemanderClick
              ButtonTheme = uctPrimary
            end
            object BtnDeleteDemander: TUniThemeButton
              Left = 64
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 3
              Images = UniMainModule.ImageList
              ImageIndex = 12
              OnClick = BtnDeleteDemanderClick
              ButtonTheme = uctDanger
            end
          end
          object ContainerFilterLeft: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alLeft
            TabOrder = 2
            LayoutConfig.Region = 'west'
            object LblDemanders: TUniLabel
              Left = 0
              Top = 6
              Width = 78
              Height = 13
              Hint = ''
              Caption = 'Kredietnemers:'
              ParentFont = False
              Font.Style = [fsBold]
              TabOrder = 1
              LayoutConfig.Cls = 'boldtext'
            end
          end
        end
        object GridDemanders: TUniDBGrid
          Left = 0
          Top = 30
          Width = 473
          Height = 187
          Hint = ''
          DataSource = DsDemanders
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
          WebOptions.Paged = False
          LoadMask.Enabled = False
          LoadMask.Message = 'Loading data...'
          ForceFit = True
          LayoutConfig.Cls = 'customGrid'
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '8 0 0 0'
          Align = alClient
          TabOrder = 2
          OnDblClick = BtnModifyDemanderClick
          Columns = <
            item
              FieldName = 'DEMANDER_NAME'
              Title.Caption = 'Naam'
              Width = 64
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end>
        end
      end
      object EditQuotity: TUniDBFormattedNumberEdit
        Left = 16
        Top = 400
        Width = 473
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.CurrencySignPos = cpsRight
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'CREDIT_FILE_QUOTITY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 6
        SelectOnFocus = True
        FieldLabel = 'Quotiteit'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object ComboCreditFileType: TUniComboBox
        Left = 16
        Top = 432
        Width = 473
        Height = 23
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Niet gekend'
          'First time buyer'
          'Opbrengstpand'
          'Huisvesting')
        TabOrder = 7
        FieldLabel = 'Aard krediet'
        FieldLabelWidth = 180
        IconItems = <>
      end
      object Edit_Credit_Purposes: TUniDBLookupComboBox
        Left = 16
        Top = 464
        Width = 473
        Height = 23
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsCreditPurposes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'PURPOSE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 8
        Color = clWindow
        FieldLabel = 'Doel'
        FieldLabelWidth = 180
        ForceSelection = True
        Style = csDropDown
      end
      object EditDateSubmission: TUniDBDateTimePicker
        Left = 16
        Top = 272
        Width = 473
        Hint = ''
        DataField = 'DATE_SUBMISSION'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 2
        FieldLabel = 'Datum indiening'
        FieldLabelWidth = 180
      end
      object EditDateApproval: TUniDBDateTimePicker
        Left = 16
        Top = 304
        Width = 473
        Hint = ''
        DataField = 'DATE_APPROVAL'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 3
        FieldLabel = 'Datum goedkeuring'
        FieldLabelWidth = 180
      end
      object EditDateDeadlineSignOffer: TUniDBDateTimePicker
        Left = 16
        Top = 336
        Width = 473
        Hint = ''
        DataField = 'DATE_DEADLINE_SIGN_OFFER'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 4
        FieldLabel = 'Uiterste datum ondert. aanbod'
        FieldLabelWidth = 180
      end
      object EditDateDeadlineSignDeed: TUniDBDateTimePicker
        Left = 16
        Top = 368
        Width = 473
        Hint = ''
        DataField = 'DATE_DEADLINE_SIGN_DEED'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 5
        FieldLabel = 'Uiterste datum akte notaris'
        FieldLabelWidth = 180
      end
      object BtnAddComment: TUniThemeButton
        Left = 1000
        Top = 16
        Width = 28
        Height = 28
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 10
        Images = UniMainModule.ImageList
        ImageIndex = 49
        OnClick = BtnAddCommentClick
        ButtonTheme = uctPrimary
      end
    end
    object TabSheetDetails: TUniTabSheet
      Hint = ''
      ImageIndex = 7
      Caption = 'Details'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object EditOriginal_amount: TUniDBFormattedNumberEdit
        Left = 16
        Top = 48
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'ORIGINAL_AMOUNT'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 1
        SelectOnFocus = True
        FieldLabel = 'Oorspronkelijk bedrag'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditMonthly_charge: TUniDBFormattedNumberEdit
        Left = 16
        Top = 112
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MONTHLY_CHARGE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 3
        SelectOnFocus = True
        FieldLabel = 'Huidige maandlast'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditStartDate: TUniDBDateTimePicker
        Left = 16
        Top = 144
        Width = 489
        Hint = ''
        DataField = 'STARTDATE'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 4
        FieldLabel = 'Startdatum'
        FieldLabelWidth = 180
      end
      object EditEndDate: TUniDBDateTimePicker
        Left = 16
        Top = 176
        Width = 489
        Hint = ''
        DataField = 'ENDDATE'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 5
        FieldLabel = 'Einddatum'
        FieldLabelWidth = 180
      end
      object EditSaldoDate: TUniDBDateTimePicker
        Left = 16
        Top = 208
        Width = 489
        Hint = ''
        DataField = 'SALDODATE'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 6
        FieldLabel = 'Saldo per'
        FieldLabelWidth = 180
      end
      object EditSaldoAmount: TUniDBFormattedNumberEdit
        Left = 16
        Top = 240
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'SALDO_AMOUNT'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 7
        SelectOnFocus = True
        FieldLabel = 'Bedrag saldo'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object CheckboxPeriodwithouttakingout: TUniDBCheckBox
        Left = 16
        Top = 312
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'PERIOD_WITHOUT_TAKINGOUT'
        DataSource = Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 8
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Duurtijd zonder opname'
        FieldLabelWidth = 180
      end
      object EditMonthsTakingOut: TUniDBNumberEdit
        Left = 16
        Top = 336
        Width = 489
        Height = 22
        Hint = ''
        DataField = 'MONTHS_TAKINGOUT'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 9
        SelectOnFocus = True
        FieldLabel = 'Opnameperiode'
        FieldLabelWidth = 180
        DecimalSeparator = ','
      end
      object EditRefundSystem: TUniDBLookupComboBox
        Left = 16
        Top = 368
        Width = 489
        Height = 23
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsRefundSystems
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'REFUND_SYSTEM'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 10
        Color = clWindow
        FieldLabel = 'Terugbetalingssysteem'
        FieldLabelWidth = 180
        Style = csDropDown
      end
      object EditCapitalDueDateOne: TUniDBFormattedNumberEdit
        Left = 16
        Top = 400
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'CAPITAL_DUEDATE_ONE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 11
        SelectOnFocus = True
        FieldLabel = 'Kapitaal vervaldag 1'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditInterestFormula: TUniDBLookupComboBox
        Left = 536
        Top = 16
        Width = 489
        Height = 23
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsInterestFormulas
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'INTEREST_FORMULA'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 12
        Color = clWindow
        FieldLabel = 'Renteformule'
        FieldLabelWidth = 180
        Style = csDropDown
      end
      object EditStartInterestYearly: TUniDBFormattedNumberEdit
        Left = 536
        Top = 48
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'START_INTEREST_YEARLY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 13
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 6
            ButtonId = 0
            HandleClicks = True
            Hint = 'Omrekenen jaar naar maand'
          end>
        FieldLabel = 'Startrente jaarlijks'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
        OnTriggerEvent = EditStartInterestYearlyTriggerEvent
      end
      object StartInterestMonthly: TUniDBFormattedNumberEdit
        Left = 536
        Top = 80
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'START_INTEREST_MONTHLY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 14
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 6
            ButtonId = 0
            HandleClicks = True
            Hint = 'Omrekenen maand naar jaar'
          end>
        FieldLabel = 'Startrente maandelijks'
        FieldLabelWidth = 180
        DecimalPrecision = 4
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
        OnTriggerEvent = StartInterestMonthlyTriggerEvent
      end
      object EditIndexType: TUniDBLookupComboBox
        Left = 536
        Top = 144
        Width = 489
        Height = 23
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsIndexTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'INDEX_TYPE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 15
        Color = clWindow
        FieldLabel = 'Soort index'
        FieldLabelWidth = 180
        Style = csDropDown
      end
      object EditStartIndexYearly: TUniDBFormattedNumberEdit
        Left = 536
        Top = 176
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'START_INDEX_YEARLY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 16
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 6
            ButtonId = 0
            HandleClicks = True
            Hint = 'Omrekenen jaar naar maand'
          end>
        FieldLabel = 'Startindex jaarlijks'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
        OnTriggerEvent = EditStartIndexYearlyTriggerEvent
      end
      object EditStartIndexMonthly: TUniDBFormattedNumberEdit
        Left = 536
        Top = 208
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'START_INDEX_MONTHLY'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 17
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 6
            ButtonId = 0
            HandleClicks = True
            Hint = 'Omrekenen maand naar jaar'
          end>
        FieldLabel = 'Startindex maandelijks'
        FieldLabelWidth = 180
        DecimalPrecision = 4
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
        OnTriggerEvent = EditStartIndexMonthlyTriggerEvent
      end
      object EditMaximumCap: TUniDBFormattedNumberEdit
        Left = 536
        Top = 272
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'MAXIMUM_CAP'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 18
        SelectOnFocus = True
        FieldLabel = 'Maximale cap'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditMaximumFloor: TUniDBFormattedNumberEdit
        Left = 536
        Top = 304
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'MAXIMUM_FLOOR'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 19
        SelectOnFocus = True
        FieldLabel = 'Maximale floor'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCurrentInterestRate: TUniDBFormattedNumberEdit
        Left = 536
        Top = 336
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'CURRENT_INTEREST_RATE'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 20
        SelectOnFocus = True
        FieldLabel = 'Huidige rentevoet'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditNextRevisionDate: TUniDBDateTimePicker
        Left = 536
        Top = 368
        Width = 489
        Hint = ''
        DataField = 'NEXT_REVISION_DATE'
        DataSource = Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 21
        FieldLabel = 'Volgende herzieningsdatum'
        FieldLabelWidth = 180
      end
      object EditDuration: TUniDBNumberEdit
        Left = 16
        Top = 80
        Width = 489
        Height = 22
        Hint = ''
        DataField = 'DURATION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 2
        SelectOnFocus = True
        FieldLabel = 'Duurtijd'
        FieldLabelWidth = 180
        DecimalSeparator = ','
      end
      object EditCreditNumberInstitution: TUniDBEdit
        Left = 16
        Top = 16
        Width = 489
        Height = 22
        Hint = ''
        DataField = 'CREDIT_NUMBER_INSTITUTION'
        DataSource = Ds_CreditFiles_Edit
        TabOrder = 0
        FieldLabel = 'Nr. dossier maatschappij'
        FieldLabelWidth = 180
      end
    end
    object TabSheetReductions: TUniTabSheet
      Hint = ''
      ImageIndex = 64
      Caption = 'Cross selling'
      ExplicitHeight = 533
      object GridReductions: TUniDBGrid
        Left = 16
        Top = 56
        Width = 1009
        Height = 457
        Hint = ''
        DataSource = DsReductions
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
        WebOptions.Paged = False
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        LayoutConfig.Cls = 'customGrid'
        TabOrder = 0
        OnDblClick = BtnModifyReductionClick
        Columns = <
          item
            Flex = 1
            FieldName = 'REDUCTIONDESCRIPTION'
            Title.Caption = 'Korting'
            Width = 304
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'REDUCTION_PERCENTAGE'
            Title.Alignment = taRightJustify
            Title.Caption = 'Percentage'
            Width = 85
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
      object BtnAddReduction: TUniThemeButton
        Left = 936
        Top = 16
        Width = 28
        Height = 28
        Hint = ''
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddReductionClick
        ButtonTheme = uctPrimary
      end
      object BtnModifyReduction: TUniThemeButton
        Left = 968
        Top = 16
        Width = 28
        Height = 28
        Hint = ''
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyReductionClick
        ButtonTheme = uctPrimary
      end
      object BtnDeleteReduction: TUniThemeButton
        Left = 1000
        Top = 16
        Width = 28
        Height = 28
        Hint = ''
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteReductionClick
        ButtonTheme = uctDanger
      end
    end
    object TabSheetOther: TUniTabSheet
      Hint = ''
      ImageIndex = 28
      Caption = 'Overig'
      ExplicitHeight = 533
      object ContainerLenders: TUniContainerPanel
        Left = 16
        Top = 16
        Width = 497
        Height = 505
        Hint = ''
        ParentColor = False
        TabOrder = 0
        Layout = 'border'
        object ContainerFilterLenders: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 497
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerFilterRightLengers: TUniContainerPanel
            Left = 437
            Top = 0
            Width = 60
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alRight
            TabOrder = 1
            LayoutConfig.Region = 'east'
            object BtnAddLender: TUniThemeButton
              Left = 0
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 1
              Images = UniMainModule.ImageList
              ImageIndex = 10
              OnClick = BtnAddLenderClick
              ButtonTheme = uctPrimary
            end
            object BtnDeleteLender: TUniThemeButton
              Left = 32
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 2
              Images = UniMainModule.ImageList
              ImageIndex = 12
              OnClick = BtnDeleteLenderClick
              ButtonTheme = uctDanger
            end
          end
          object ContainerFilterLeftLenders: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alLeft
            TabOrder = 2
            LayoutConfig.Region = 'west'
            object LblLenders: TUniLabel
              Left = 0
              Top = 6
              Width = 100
              Height = 13
              Hint = ''
              Caption = 'Pandverstrekker(s):'
              ParentFont = False
              Font.Style = [fsBold]
              TabOrder = 1
              LayoutConfig.Cls = 'boldtext'
            end
          end
        end
        object GridLenders: TUniDBGrid
          Left = 0
          Top = 30
          Width = 497
          Height = 475
          Hint = ''
          DataSource = DsLenders
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
          WebOptions.Paged = False
          LoadMask.Enabled = False
          LoadMask.Message = 'Loading data...'
          ForceFit = True
          LayoutConfig.Cls = 'customGrid'
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '8 0 0 0'
          Align = alClient
          TabOrder = 2
          Columns = <
            item
              FieldName = 'LENDER_NAME'
              Title.Caption = 'Naam'
              Width = 64
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end>
        end
      end
      object ContainerGuarantors: TUniContainerPanel
        Left = 528
        Top = 16
        Width = 497
        Height = 505
        Hint = ''
        ParentColor = False
        TabOrder = 1
        Layout = 'border'
        object ContainerFilterGuarantors: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 497
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerFilterRightGuarantors: TUniContainerPanel
            Left = 437
            Top = 0
            Width = 60
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alRight
            TabOrder = 1
            LayoutConfig.Region = 'east'
            object BtnAddGuarantor: TUniThemeButton
              Left = 0
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 1
              Images = UniMainModule.ImageList
              ImageIndex = 10
              OnClick = BtnAddGuarantorClick
              ButtonTheme = uctPrimary
            end
            object BtnDeleteGuarantor: TUniThemeButton
              Left = 32
              Top = 0
              Width = 28
              Height = 28
              Hint = ''
              ParentShowHint = False
              Caption = ''
              TabStop = False
              TabOrder = 2
              Images = UniMainModule.ImageList
              ImageIndex = 12
              OnClick = BtnDeleteGuarantorClick
              ButtonTheme = uctDanger
            end
          end
          object ContainerFilterLeftGuarantors: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alLeft
            TabOrder = 2
            LayoutConfig.Region = 'west'
            object UniLabel2: TUniLabel
              Left = 0
              Top = 6
              Width = 72
              Height = 13
              Hint = ''
              Caption = 'Borgsteller(s):'
              ParentFont = False
              Font.Style = [fsBold]
              TabOrder = 1
              LayoutConfig.Cls = 'boldtext'
            end
          end
        end
        object GridGuarantors: TUniDBGrid
          Left = 0
          Top = 30
          Width = 497
          Height = 475
          Hint = ''
          DataSource = DsGuarantors
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
          WebOptions.Paged = False
          LoadMask.Enabled = False
          LoadMask.Message = 'Loading data...'
          ForceFit = True
          LayoutConfig.Cls = 'customGrid'
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '8 0 0 0'
          Align = alClient
          TabOrder = 2
          Columns = <
            item
              FieldName = 'GUARANTOR_NAME'
              Title.Caption = 'Naam'
              Width = 64
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end>
        end
      end
    end
  end
  object BtnSave: TUniThemeButton
    Left = 800
    Top = 736
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 9
    ScreenMask.Target = Owner
    OnClick = BtnSaveClick
    ButtonTheme = uctSuccess
  end
  object BtnCancel: TUniThemeButton
    Left = 936
    Top = 736
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 10
    ScreenMask.Target = Owner
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object ImageLastModification: TUniImage
    Left = 16
    Top = 728
    Width = 16
    Height = 16
    Cursor = crHandPoint
    Hint = 'Laatste wijziging'
    ShowHint = True
    ParentShowHint = False
    Stretch = True
    Url = 'images/wv_time_gray.svg'
  end
  object LblLastModification: TUniDBText
    Left = 40
    Top = 728
    Width = 101
    Height = 13
    Cursor = crHandPoint
    Hint = ''
    DataField = 'DATE_MODIFIED'
    DataSource = Ds_CreditFiles_Edit
    ParentFont = False
    Font.Color = 9474192
  end
  object EditStatus: TUniDBLookupComboBox
    Left = 16
    Top = 16
    Width = 521
    Height = 23
    Hint = ''
    ListField = 'DUTCH'
    ListSource = DsStatus
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'STATUS'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 0
    Color = clWindow
    FieldLabel = 'Status *'
    FieldLabelWidth = 180
    Style = csDropDown
  end
  object EditInternalNumber: TUniDBEdit
    Left = 16
    Top = 48
    Width = 521
    Height = 22
    Hint = ''
    DataField = 'CREDIT_FILE_NUMBER'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 1
    FieldLabel = 'Intern nummer'
    FieldLabelWidth = 180
    SelectOnFocus = True
    Triggers = <
      item
        ImageIndex = 5
        ButtonId = 0
        HandleClicks = True
        Hint = 'Nummer toekennen'
      end>
    Images = UniMainModule.ImageButtons
    OnTriggerEvent = EditInternalNumberTriggerEvent
  end
  object EditUser: TUniDBLookupComboBox
    Left = 16
    Top = 80
    Width = 521
    Height = 23
    Hint = ''
    ListField = 'USERFULLNAME'
    ListSource = DsUsers
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'ASSIGNED_USER_ID'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 2
    Color = clWindow
    FieldLabel = 'Eigenaar'
    FieldLabelWidth = 180
    ForceSelection = True
    Style = csDropDown
  end
  object Edit_Financial_institution: TUniDBLookupComboBox
    Left = 544
    Top = 16
    Width = 521
    Height = 23
    Hint = ''
    ListField = 'NAME'
    ListSource = DsFinInstitutions
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'FINANCIAL_INSTITUTION'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 4
    Color = clWindow
    FieldLabel = 'Fin. instelling'
    FieldLabelWidth = 180
    ForceSelection = True
    Style = csDropDown
  end
  object EditTotalAmountCredit: TUniDBFormattedNumberEdit
    Left = 544
    Top = 48
    Width = 521
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'TOTAL_AMOUNT_CREDIT'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 5
    SelectOnFocus = True
    FieldLabel = 'Totaalbedrag krediet'
    FieldLabelWidth = 180
    DecimalSeparator = ','
    ThousandSeparator = '.'
    OnExit = EditPurchasePricePropertyExit
  end
  object EditTotalAmountCommission: TUniDBFormattedNumberEdit
    Left = 544
    Top = 80
    Width = 521
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'TOTAL_AMOUNT_COMMISSION'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 6
    SelectOnFocus = True
    FieldLabel = 'Totaalbedrag commissie'
    FieldLabelWidth = 180
    DecimalSeparator = ','
    ThousandSeparator = '.'
    OnExit = EditPurchasePricePropertyExit
  end
  object EditDateCreditFinal: TUniDBDateTimePicker
    Left = 544
    Top = 112
    Width = 521
    Hint = ''
    DataField = 'DATE_CREDIT_FINAL'
    DataSource = Ds_CreditFiles_Edit
    DateTime = 43637.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 7
    FieldLabel = 'Datum akte'
    FieldLabelWidth = 180
  end
  object Edit_Contributor: TUniDBLookupComboBox
    Left = 16
    Top = 112
    Width = 521
    Height = 23
    Hint = ''
    ListField = 'FULLNAME'
    ListSource = DsContributors
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'CONTRIBUTOR_ID'
    DataSource = Ds_CreditFiles_Edit
    TabOrder = 3
    Color = clWindow
    FieldLabel = 'Tussenpersoon'
    FieldLabelWidth = 180
    ForceSelection = True
    Images = UniMainModule.ImageButtons
    Style = csDropDown
  end
  object Users: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, coalesce(NAME,'#39#39') || '#39' '#39' || coalesce(FIRSTNAME,'#39#39') AS' +
        ' "USERFULLNAME" from users'
      'order by USERFULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 88
    Top = 288
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 88
    Top = 344
  end
  object Contributors: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, LAST_NAME || '#39' '#39' || FIRST_NAME AS "FULLNAME" from cre' +
        'dit_contributors'
      'order by FULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 168
    Top = 288
  end
  object DsContributors: TDataSource
    DataSet = Contributors
    Left = 168
    Top = 344
  end
  object Status: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 256
    Top = 288
  end
  object DsStatus: TDataSource
    DataSet = Status
    Left = 256
    Top = 336
  end
  object PropertyTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 88
    Top = 408
  end
  object DsPropertyTypes: TDataSource
    DataSet = PropertyTypes
    Left = 104
    Top = 512
  end
  object LinkedLanguageCreditFilesAdd: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TCreditFilesAddFrm.Layout'
      'TabSheetStatus.Layout'
      'EditInternalRemarks.FieldLabelSeparator'
      'TabSheetProfile.Layout'
      'EditNoRisks.FieldLabelSeparator'
      'EditNoRisks.ValueChecked'
      'EditNoRisks.ValueUnchecked'
      'EditLimitedRisks.FieldLabelSeparator'
      'EditLimitedRisks.ValueChecked'
      'EditLimitedRisks.ValueUnchecked'
      'EditHighRisks.FieldLabelSeparator'
      'EditHighRisks.ValueChecked'
      'EditHighRisks.ValueUnchecked'
      'EditMaximumMonthlyCharge.FieldLabelSeparator'
      'EditFreeChoiceBankAccount.FieldLabelSeparator'
      'EditFreeChoiceBankAccount.ValueChecked'
      'EditFreeChoiceBankAccount.ValueUnchecked'
      'EditFreeChoiceDebtInsurance.FieldLabelSeparator'
      'EditFreeChoiceDebtInsurance.ValueChecked'
      'EditFreeChoiceDebtInsurance.ValueUnchecked'
      'EditCoverageDebtInsurance.FieldLabelSeparator'
      'EditFreeChoiceFireInsurance.FieldLabelSeparator'
      'EditFreeChoiceFireInsurance.ValueChecked'
      'EditFreeChoiceFireInsurance.ValueUnchecked'
      'EditCommentsContributor.FieldLabelSeparator'
      'EditCommentsAdmin.FieldLabelSeparator'
      'EditMaximumDuration.FieldLabelSeparator'
      'TabSheetPlan.Layout'
      'EditPurchasePriceProperty.FieldLabelSeparator'
      'EditPurchaseCostProperty.FieldLabelSeparator'
      'EditPropertyType.FieldLabelSeparator'
      'EditPropertyAddress.FieldLabelSeparator'
      'EditPurchasePriceGroundNewProp.FieldLabelSeparator'
      'EditPurchaseCostNewProp.FieldLabelSeparator'
      'EditCostPriceBuildNewProp.FieldLabelSeparator'
      'EditVatBuildNewProp.FieldLabelSeparator'
      'EditCostArchitectNewProp.FieldLabelSeparator'
      'EditVatArchitectNewProp.FieldLabelSeparator'
      'EditCostPriceStudiesNewProp.FieldLabelSeparator'
      'EditVatStudiesNewProp.FieldLabelSeparator'
      'EditOterCostsNewProp.FieldLabelSeparator'
      'EditSaldoToTakeOverOtherCredits.FieldLabelSeparator'
      'EditHandLighting.FieldLabelSeparator'
      'ComboCreditFileType.FieldLabelSeparator'
      'ContainerDemanders.Layout'
      'ContainerFilter.Layout'
      'ContainerFilterRight.Layout'
      'ContainerFilterLeft.Layout'
      'EditQuotity.FieldLabelSeparator'
      'EditCostPriceBuildRenovate.FieldLabelSeparator'
      'EditVatBuildRenovation.FieldLabelSeparator'
      'EditCostArchitectRenovation.FieldLabelSeparator'
      'EditVatArchitectRenovation.FieldLabelSeparator'
      'EditReinvestmentallowance.FieldLabelSeparator'
      'EditVariouscostsfile.FieldLabelSeparator'
      'EditVariousCostsEstimation.FieldLabelSeparator'
      'EditPurchaseSumDebtBalansIns.FieldLabelSeparator'
      'EditVariousCostsOther.FieldLabelSeparator'
      'EditMortGageRegistration.FieldLabelSeparator'
      'EditMortGageMandate.FieldLabelSeparator'
      'EditOwnResources.FieldLabelSeparator'
      'EditTotalInvestMent.FieldLabelSeparator'
      'EditDemandedCredit.FieldLabelSeparator'
      'ImageLink.Url'
      'EditCostPriceStudiesRenovation.FieldLabelSeparator'
      'EditVatStudiesRenovation.FieldLabelSeparator'
      'EditOtherCostsRenovation.FieldLabelSeparator'
      'EditCreditType.FieldLabelSeparator'
      'CreditFiles_Edit.KeyGenerator'
      'Users.SQLDelete'
      'Users.SQLInsert'
      'Users.SQLLock'
      'Users.SQLRecCount'
      'Users.SQLRefresh'
      'Users.SQLUpdate'
      'Contributors.SQLDelete'
      'Contributors.SQLInsert'
      'Contributors.SQLLock'
      'Contributors.SQLRecCount'
      'Contributors.SQLRefresh'
      'Contributors.SQLUpdate'
      'Status.SQLDelete'
      'Status.SQLInsert'
      'Status.SQLLock'
      'Status.SQLRecCount'
      'Status.SQLRefresh'
      'Status.SQLUpdate'
      'PropertyTypes.SQLDelete'
      'PropertyTypes.SQLInsert'
      'PropertyTypes.SQLLock'
      'PropertyTypes.SQLRecCount'
      'PropertyTypes.SQLRefresh'
      'PropertyTypes.SQLUpdate'
      'FinInstitutions.SQLDelete'
      'FinInstitutions.SQLInsert'
      'FinInstitutions.SQLLock'
      'FinInstitutions.SQLRecCount'
      'FinInstitutions.SQLRefresh'
      'FinInstitutions.SQLUpdate'
      'CreditTypes.SQLDelete'
      'CreditTypes.SQLInsert'
      'CreditTypes.SQLLock'
      'CreditTypes.SQLRecCount'
      'CreditTypes.SQLRefresh'
      'UpdTr_CreditFiles_Edit.Params'
      'CreditFiles_Edit.SQLUpdate'
      'CreditFiles_Edit.SQLRefresh'
      'CreditFiles_Edit.SQLRecCount'
      'CreditFiles_Edit.SQLLock'
      'CreditFiles_Edit.SQLInsert'
      'CreditFiles_Edit.SQLDelete'
      'CreditTypes.SQLUpdate'
      'Demanders.SQLDelete'
      'Demanders.SQLInsert'
      'Demanders.SQLLock'
      'Demanders.SQLRecCount'
      'Demanders.SQLRefresh'
      'Demanders.SQLUpdate'
      'EditNotaryCost.FieldLabelSeparator'
      'ReductionsID.DisplayLabel'
      'ReductionsREDUCTIONDESCRIPTION.DisplayLabel'
      'ReductionsREDUCTION_PERCENTAGE.DisplayLabel'
      'Edit_Credit_Purposes.FieldLabelSeparator'
      'TabSheetDetails.Layout'
      'EditOriginal_amount.FieldLabelSeparator'
      'EditMonthly_charge.FieldLabelSeparator'
      'EditStartDate.DateFormat'
      'EditStartDate.FieldLabelSeparator'
      'EditStartDate.TimeFormat'
      'EditEndDate.DateFormat'
      'EditEndDate.FieldLabelSeparator'
      'EditEndDate.TimeFormat'
      'EditSaldoDate.DateFormat'
      'EditDateCreditFinal.FieldLabelSeparator'
      'EditDateCreditFinal.TimeFormat'
      'TabSheetOther.Layout'
      'ContainerLenders.Layout'
      'ContainerFilterLenders.Layout'
      'ContainerFilterRightLengers.Layout'
      'ContainerFilterLeftLenders.Layout'
      'ContainerGuarantors.Layout'
      'ContainerFilterGuarantors.Layout'
      'ContainerFilterRightGuarantors.Layout'
      'ContainerFilterLeftGuarantors.Layout'
      'EditStatus.FieldLabelSeparator'
      'EditInternalNumber.FieldLabelSeparator'
      'EditUser.FieldLabelSeparator'
      'Edit_Financial_institution.FieldLabelSeparator'
      'EditTotalAmountCredit.FieldLabelSeparator'
      'EditDuration.FieldLabelSeparator'
      'EditNextRevisionDate.FieldLabelSeparator'
      'EditNextRevisionDate.DateFormat'
      'EditCurrentInterestRate.FieldLabelSeparator'
      'EditMaximumFloor.FieldLabelSeparator'
      'EditMaximumCap.FieldLabelSeparator'
      'EditStartIndexMonthly.FieldLabelSeparator'
      'EditStartIndexYearly.FieldLabelSeparator'
      'EditIndexType.FieldLabelSeparator'
      'StartInterestMonthly.FieldLabelSeparator'
      'EditStartInterestYearly.FieldLabelSeparator'
      'EditInterestFormula.FieldLabelSeparator'
      'EditCapitalDueDateOne.FieldLabelSeparator'
      'EditRefundSystem.FieldLabelSeparator'
      'EditMonthsTakingOut.FieldLabelSeparator'
      'CheckboxPeriodwithouttakingout.ValueUnchecked'
      'CheckboxPeriodwithouttakingout.ValueChecked'
      'CheckboxPeriodwithouttakingout.FieldLabelSeparator'
      'EditSaldoAmount.FieldLabelSeparator'
      'EditSaldoDate.FieldLabelSeparator'
      'EditSaldoDate.TimeFormat'
      'TabSheetReductions.Layout'
      'ImageLastModification.Url'
      'EditDateSubmission.DateFormat'
      'EditDateSubmission.FieldLabelSeparator'
      'EditDateSubmission.TimeFormat'
      'EditDateApproval.DateFormat'
      'EditDateApproval.FieldLabelSeparator'
      'EditDateApproval.TimeFormat'
      'EditDateDeadlineSignOffer.DateFormat'
      'EditDateDeadlineSignOffer.FieldLabelSeparator'
      'EditDateDeadlineSignOffer.TimeFormat'
      'EditDateDeadlineSignDeed.DateFormat'
      'EditDateDeadlineSignDeed.FieldLabelSeparator'
      'EditDateDeadlineSignDeed.TimeFormat'
      'EditCreditNumberInstitution.FieldLabelSeparator'
      'EditTotalAmountCommission.FieldLabelSeparator'
      'EditDateCreditFinal.DateFormat'
      'EditNextRevisionDate.TimeFormat'
      'Edit_Contributor.FieldLabelSeparator')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageCreditFilesAddChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 216
    Top = 568
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A005400430072006500640069007400460069006C0065007300
      410064006400460072006D0001004B0072006500640069006500740065006E00
      010043007200E900640069007400730001004300720065006400690074007300
      01000D000A005400610062005300680065006500740053007400610074007500
      7300010041006C00670065006D00650065006E00010053007400610074007500
      7400010053007400610074007500730001000D000A0054006100620053006800
      650065007400500072006F00660069006C006500010052006900730069006300
      6F00700072006F006600690065006C0020002F00200070006500720073006F00
      6F006E006C0069006A006B006500200076006F006F0072006B00650075007200
      65006E000100500072006F00660069006C002000640065002000720069007300
      71007500650020002F00200070007200E9006600E900720065006E0063006500
      7300200070006500720073006F006E006E0065006C006C006500730001005200
      690073006B002000700072006F00660069006C00650020002F00200070006500
      720073006F006E0061006C00200070007200650066006500720065006E006300
      6500730001000D000A004C0062006C005200690073006B00700072006F006600
      69006C0065000100520069007300690063006F00700072006F00660069006500
      6C000100500072006F00660069006C0065002000640065002000720069007300
      71007500650001005200690073006B002000700072006F00660069006C006500
      01000D000A004C0062006C0050006500720073006F006E0061006C0050007200
      650066006500720065006E00630065007300010050006500720073006F006F00
      6E006C0069006A006B006500200076006F006F0072006B006500750072006500
      6E00010050007200E9006600E900720065006E00630065007300200070006500
      720073006F006E006E0065006C006C0065007300010050006500720073006F00
      6E0061006C00200070007200650066006500720065006E006300650073000100
      0D000A005400610062005300680065006500740050006C0061006E0001004600
      69006E0061006E0063006900650065006C00200070006C0061006E0001005000
      6C0061006E002000660069006E0061006E006300690065007200010046006900
      6E0061006E006300690061006C00200070006C0061006E0001000D000A004C00
      62006C00500075007200630068006100730065000100410061006E006B006F00
      6F00700001004100630068006100740001005000750072006300680061007300
      650001000D000A004C0062006C004E00650077004200750069006C0064006900
      6E00670001004E00690065007500770062006F007500770001004E006F007500
      760065006C006C006500200063006F006E007300740072007500630074006900
      6F006E0001004E0065007700200063006F006E00730074007200750063007400
      69006F006E0001000D000A004C0062006C0052006500660069006E0061006E00
      630065000100480065007200660069006E0061006E0063006900650072006900
      6E006700010052006500660069006E0061006E00630065006D0065006E007400
      010052006500660069006E0061006E00630069006E00670001000D000A004C00
      62006C00440069007600650072007300650043006F0073007400730001004400
      69007600650072007300650020006B006F007300740065006E00010046007200
      610069007300200064006900760065007200730001004D006900730063006500
      6C006C0061006E0065006F0075007300200063006F0073007400730001000D00
      0A004C0062006C0043006F007300740073000100570061006100720062006F00
      720067006B006F007300740065006E0001004600720061006900730020006400
      6500200067006100720061006E00740069006500010047007500610072006100
      6E007400650065002000660065006500730001000D000A004C0062006C004600
      69006E0061006E00630069006E0067000100460069006E0061006E0063006900
      6500720069006E0067000100460069006E0061006E00630065006D0065006E00
      74000100460069006E0061006E00630069006E00670001000D000A004C006200
      6C00520065006E006F0076006100740069006F006E0001005600650072006200
      6F007500770069006E00670001005200E9006E006F0076006100740069006F00
      6E00010048006F006D006500200069006D00700072006F00760065006D006500
      6E00740001000D000A00420074006E00530061007600650001004F0070007300
      6C00610061006E00010053006100750076006500670061007200640065007200
      0100530061007600650001000D000A00420074006E00430061006E0063006500
      6C00010041006E006E0075006C006500720065006E00010041006E006E007500
      6C00650072000100430061006E00630065006C0001000D000A004C0062006C00
      440065006D0061006E00640065007200730001004B0072006500640069006500
      74006E0065006D006500720073003A00010045006D007000720075006E007400
      65007500720073003A00010042006F00720072006F0077006500720073003A00
      01000D000A00420074006E004100640064004F007200670061006E0069007300
      6100740069006F006E00010054006F00650076006F006500670065006E002000
      6F007200670061006E00690073006100740069006500010041006A006F007500
      740065007200200075006E00650020006F007200670061006E00690073006100
      740069006F006E00010041006400640020006F007200670061006E0069007A00
      6100740069006F006E0001000D000A00420074006E0041006400640052006500
      6C006100740069006F006E00010054006F00650076006F006500670065006E00
      2000720065006C006100740069006500010041006A006F007500740065007200
      200075006E0065002000720065006C006100740069006F006E00010041006400
      64002000720065006C006100740069006F006E00730068006900700001000D00
      0A0054006100620053006800650065007400440065007400610069006C007300
      0100440065007400610069006C00730001004400E9007400610069006C007300
      0100440065007400610069006C00730001000D000A0054006100620053006800
      65006500740052006500640075006300740069006F006E007300010043007200
      6F00730073002000730065006C006C0069006E0067000100430072006F007300
      73002000730065006C006C0069006E0067000100430072006F00730073002000
      730065006C006C0069006E00670001000D000A00420074006E00410064006400
      4F007200670061006E00690073006100740069006F006E004C0065006E006400
      65007200010054006F00650076006F006500670065006E0020006F0072006700
      61006E00690073006100740069006500010041006A006F007500740065007200
      20006F007200670061006E00690073006100740069006F006E00010041006400
      640020006F007200670061006E0069007A006100740069006F006E0001000D00
      0A00420074006E00410064006400520065006C006100740069006F006E004C00
      65006E00640065007200010054006F00650076006F006500670065006E002000
      720065006C006100740069006500010041006A006F0075007400650072002000
      720065006C006100740069006F006E0001004100640064002000720065006C00
      6100740069006F006E0001000D000A00420074006E004100640064004F007200
      670061006E00690073006100740069006F006E00470075006100720061006E00
      74006F007200010054006F00650076006F006500670065006E0020006F007200
      670061006E00690073006100740069006500010041006A006F00750074006500
      720020006F007200670061006E00690073006100740069006F006E0001004100
      6400640020006F007200670061006E0069007A006100740069006F006E000100
      0D000A00420074006E00410064006400520065006C006100740069006F006E00
      470075006100720061006E0074006F007200010054006F00650076006F006500
      670065006E002000720065006C006100740069006500010041006A006F007500
      7400650072002000720065006C006100740069006F006E000100410064006400
      2000720065006C006100740069006F006E0001000D000A005400610062005300
      68006500650074004F00740068006500720001004F0076006500720069006700
      01004100750074007200650001004F00740068006500720001000D000A004C00
      62006C004C0065006E0064006500720073000100500061006E00640076006500
      720073007400720065006B006B00650072002800730029003A00010050007200
      EA00740065007500720020002800730029003A0001004C0065006E0064006500
      720020002800730029003A0001000D000A0055006E0069004C00610062006500
      6C003200010042006F00720067007300740065006C006C006500720028007300
      29003A00010047006100720061006E00740020002800730029003A0001004700
      75006100720061006E0074006F00720020002800730029003A0001000D000A00
      73007400480069006E00740073005F0055006E00690063006F00640065000D00
      0A0049006D006100670065004C0069006E006B0001004C0069006E006B002000
      6E0061006100720020006E006F00740061007200690073002E00620065000100
      4C00690065006E002000760065007200730020006E006F007400610072006900
      73002E006200650001004C0069006E006B00200074006F0020006E006F007400
      61007200690073002E006200650001000D000A0049006D006100670065004C00
      6100730074004D006F00640069006600690063006100740069006F006E000100
      4C006100610074007300740065002000770069006A007A006900670069006E00
      670001004400650072006E006900E8007200650020006D006F00640069006600
      690063006100740069006F006E0001004C0061007300740020006D006F006400
      69006600690063006100740069006F006E0001000D000A007300740044006900
      730070006C00610079004C006100620065006C0073005F0055006E0069006300
      6F00640065000D000A007300740046006F006E00740073005F0055006E006900
      63006F00640065000D000A00730074004D0075006C00740069004C0069006E00
      650073005F0055006E00690063006F00640065000D000A0043006F006D006200
      6F00430072006500640069007400460069006C00650054007900700065002E00
      4900740065006D007300010022004E006900650074002000670065006B006500
      6E00640022002C002200460069007200730074002000740069006D0065002000
      6200750079006500720022002C004F0070006200720065006E00670073007400
      700061006E0064002C004800750069007300760065007300740069006E006700
      010049006E0063006F006E006E0075002C0022005000720065006D0069006500
      720020006100630068006100740022002C00220049006D006D00650075006200
      6C00650020006400650020007200650074006F007500720022002C004C006F00
      670065006D0065006E007400010022004E006F00740020006B006E006F007700
      6E0022002C002200460069007200730074002000740069006D00650020006200
      750079006500720022002C002200520065007400750072006E00200062007500
      69006C00640069006E00670022002C0048006F007500730069006E0067000100
      0D000A007300740053007400720069006E00670073005F0055006E0069006300
      6F00640065000D000A007300740072007300740072005F007200650063006F00
      720064005F006C006F0063006B00650064000100470065006700650076006500
      6E00730020006B0075006E006E0065006E0020006E0069006500740020006100
      61006E00670065007000610073007400200077006F007200640065006E002C00
      200069006E0020006700650062007200750069006B00200064006F006F007200
      2000650065006E00200061006E00640065007200650020006700650062007200
      750069006B00650072002E0001004C0065007300200064006F006E006E00E900
      6500730020006E0065002000700065007500760065006E007400200070006100
      73002000EA0074007200650020006D006F006400690066006900E90065007300
      20006C006F007200730071007500270065006C006C0065007300200073006F00
      6E00740020007500740069006C0069007300E900650073002000700061007200
      200075006E0020006100750074007200650020007500740069006C0069007300
      610074006500750072002E00010044006100740061002000630061006E006E00
      6F00740020006200650020006D006F0064006900660069006500640020007700
      680065006E0020007500730065006400200062007900200061006E006F007400
      680065007200200075007300650072002E0001000D000A007300740072007300
      740072005F007300740061007400750073005F00720065007100750069007200
      6500640001004400650020007300740061007400750073002000690073002000
      650065006E00200076006500720065006900730074006500200069006E006700
      6100760065002E0001004C006500200073007400610074007500740020006500
      73007400200075006E006500200065006E0074007200E900650020006F006200
      6C0069006700610074006F006900720065002E00010054006800650020007300
      7400610074007500730020006900730020006100200072006500710075006900
      720065006400200065006E007400720079002E0001000D000A00730074007200
      7300740072005F006500720072006F0072005F0073006100760069006E006700
      5F006400610074006100010046006F00750074002000620069006A0020006800
      6500740020006F00700073006C00610061006E002000760061006E0020006400
      650020006700650067006500760065006E007300210001004500720072006500
      7500720020006C006F007200730020006400650020006C00270065006E007200
      65006700690073007400720065006D0065006E00740020006400650073002000
      64006F006E006E00E90065007300210001004500720072006F00720020007300
      6100760069006E00670020007400680065002000640061007400610021000100
      0D000A007300740072007300740072005F00640065006C006500740065005F00
      640065006D0061006E006400650072000100560065007200770069006A006400
      6500720065006E002000610061006E007600720061006700650072003F000100
      5300750070007000720069006D006500720020006C0065002000640065006D00
      61006E0064006500750072003F000100440065006C0065007400650020006100
      700070006C006900630061006E0074003F0001000D000A007300740072007300
      740072005F006500720072006F0072005F00640065006C006500740069006E00
      6700010046006F00750074002000620069006A00200068006500740020007600
      65007200770069006A0064006500720065006E00010045007200720065007500
      720020006C006F007200730020006400650020006C0061002000730075007000
      7000720065007300730069006F006E0001004500720072006F00720020006400
      65006C006500740069006E00670001000D000A00730074007200730074007200
      5F006500720072006F0072005F0061006400640069006E006700010046006F00
      750074002000620069006A002000680065007400200074006F00650076006F00
      6500670065006E00010045007200720065007500720020006C006F0072007300
      20006400650020006C00270061006A006F007500740001004500720072006F00
      7200200061006400640069006E00670001000D000A0073007400720073007400
      72005F006C006F0063006B006500640001004B0061006E002000670065006700
      6500760065006E00730020006E006900650074002000770069006A007A006900
      670065006E002C00200069006E0020006700650062007200750069006B002000
      64006F006F0072002000650065006E00200061006E0064006500720065002000
      6700650062007200750069006B0065007200010049006D0070006F0073007300
      690062006C00650020006400650020006D006F00640069006600690065007200
      20006C0065007300200064006F006E006E00E900650073002C00200065006E00
      200063006F007500720073002000640027007500740069006C00690073006100
      740069006F006E002000700061007200200075006E0020006100750074007200
      650020007500740069006C006900730061007400650075007200010043006100
      6E006E006F00740020006300680061006E006700650020006400610074006100
      2C00200069006E002000750073006500200062007900200061006E006F007400
      6800650072002000750073006500720001000D000A0073007400720073007400
      72005F00610073007300690067006E005F006E0075006D006200650072005F00
      630072006500640069007400010054006F0065006B0065006E006E0065006E00
      20006E0075006D006D0065007200010041007400740072006900620075006500
      7200200075006E0020006E0075006D00E90072006F0001004100730073006900
      67006E0020006E0075006D0062006500720001000D000A007300740072007300
      740072005F0061006C00720065006100640079005F0061005F006E0075006D00
      6200650072005F00700072006500730065006E0074005F006300720065006400
      6900740001004500720020006900730020007200650065006400730020006500
      65006E0020006E0075006D006D00650072002000610061006E00770065007A00
      690067002C002000770065006E007300740020007500200064006F006F007200
      20007400650020006700610061006E003F00010049006C002000790020006100
      20006400E9006A00E000200075006E0020006E0075006D00E90072006F002000
      64006900730070006F006E00690062006C0065002C00200076006F0075006C00
      65007A002D0076006F0075007300200063006F006E00740069006E0075006500
      72003F00010054006800650072006500200069007300200061006C0072006500
      6100640079002000610020006E0075006D006200650072002000610076006100
      69006C00610062006C0065002C00200064006F00200079006F00750020007700
      61006E007400200074006F00200063006F006E00740069006E00750065003F00
      01000D000A007300740072007300740072005F00710075006F00740069007400
      79005F0072006500710075006900720065006400010044006500200071007500
      6F007400690074006500690074002000690073002000650065006E0020007600
      6500720070006C0069006300680074006500200069006E006700610076006500
      2E0001004C0065002000710075006F0074006100200065007300740020007500
      6E006500200065006E0074007200E900650020006F0062006C00690067006100
      74006F006900720065002E0001005400680065002000710075006F0074006100
      20006900730020006100200063006F006D00700075006C0073006F0072007900
      200065006E007400720079002E0001000D000A00730074007200730074007200
      5F00640065006C006500740065005F00670075006100720061006E0074006F00
      72000100560065007200770069006A0064006500720065006E00200062006F00
      720067007300740065006C006C00650072003F00010053007500700070007200
      69006D006500720020006C006500200067006100720061006E0074003F000100
      520065006D006F00760065002000670075006100720061006E0074006F007200
      3F0001000D000A007300740072007300740072005F00640065006C0065007400
      65005F006C0065006E006400650072000100560065007200770069006A006400
      6500720065006E002000700061006E0064007600650072007300740072006500
      6B006B00650072003F0001005300750070007000720069006D00650072002000
      6C006500200070007200EA0074006500750072003F000100520065006D006F00
      7600650020006C0065006E006400650072003F0001000D000A00730074007200
      7300740072005F00640065006C006500740065005F0072006500640075006300
      740069006F006E000100560065007200770069006A0064006500720065006E00
      20006B006F007200740069006E0067003F000100530075007000700072006900
      6D006500720020006C0061002000720065006D006900730065003F0001005200
      65006D006F0076006500200064006900730063006F0075006E0074003F000100
      0D000A007300740072007300740072005F00660069006E0061006E0063006900
      61006C005F0069006E00730074005F0072006500710075006900720065006400
      5F0064006100740065000100440065002000660069006E0061006E0063006900
      EB006C006500200069006E007300740065006C006C0069006E00670020006900
      7300200076006500720070006C006900630068007400200061006C0073002000
      64006500200064006100740075006D002000760061006E002000640065002000
      61006B0074006500200069007300200069006E0067006500760075006C006400
      2E0001004C00270069006E0073007400690074007500740069006F006E002000
      660069006E0061006E0063006900E80072006500200065007300740020007200
      65007100750069007300650020007300690020006C0061002000640061007400
      650020006400650020006C002700610063007400650020006500730074002000
      69006E007300630072006900740065002E000100540068006500200066006900
      6E0061006E006300690061006C00200069006E00730074006900740075007400
      69006F006E0020006900730020006F0062006C00690067006500640020006900
      660020007400680065002000640061007400650020006F006600200074006800
      650020006400650065006400200069007300200065006E007400650072006500
      64002E0001000D000A007300740072007300740072005F006400610074006500
      5F007300750062006D0069007300730069006F006E005F007200650071007500
      6900720065006400010044006500200064006100740075006D00200076006100
      6E00200069006E006400690065006E0069006E00670020006900730020006500
      65006E00200076006500720070006C0069006300680074006500200069006E00
      670061007600650001004C006100200064006100740065002000640065002000
      73006F0075006D0069007300730069006F006E00200065007300740020007500
      6E006500200065006E0074007200E900650020006F0062006C00690067006100
      74006F0069007200650001005400680065002000640061007400650020006F00
      660020007300750062006D0069007300730069006F006E002000690073002000
      610020006D0061006E006400610074006F0072007900200065006E0074007200
      790001000D000A007300740072007300740072005F0064006F00630075006D00
      65006E00740073005F00720065007100750069007200650064005F0063007200
      65006400690074005F00660069006E0061006C000100550020006D006F006500
      740020006400650020006F006E00640065007200740065006B0065006E006400
      650020006B00720065006400690065007400610061006E007600720061006100
      6700200061006C0073006F006F006B0020006400650020006800650074002000
      6F006E00640065007200740065006B0065006E00640065002000610061006E00
      62006F00640020006B006F007000700065006C0065006E002000610061006E00
      2000680065007400200064006F0073007300690065007200200076006F006F00
      720020007500200064006500200061006B007400650064006100740075006D00
      20006B0075006E007400200069006E0067006500760065006E00010056006F00
      75007300200064006500760065007A0020006C0069006500720020006C006100
      2000640065006D0061006E0064006500200064006500200063007200E9006400
      6900740020007300690067006E00E90065002000610069006E00730069002000
      71007500650020006C0027006F00660066007200650020007300690067006E00
      E9006500200061007500200064006F0073007300690065007200200061007600
      61006E007400200064006500200070006F00750076006F006900720020007300
      6100690073006900720020006C00610020006400610074006500200064006500
      20006C0027006100630074006500010059006F00750020006D00750073007400
      20006C0069006E006B00200074006800650020007300690067006E0065006400
      200063007200650064006900740020006100700070006C006900630061007400
      69006F006E002000610073002000770065006C006C0020006100730020007400
      6800650020007300690067006E006500640020006F0066006600650072002000
      74006F0020007400680065002000660069006C00650020006200650066006F00
      72006500200079006F0075002000630061006E00200065006E00740065007200
      2000740068006500200064006500650064002000640061007400650001000D00
      0A007300740072007300740072005F0063006F006D006D006900730073006900
      6F006E005F0061006D006F0075006E0074005F0074006F005F00680069006700
      68000100480065007400200063006F006D006D00690073007300690065006200
      65006400720061006700200069007300200074006500200068006F006F006700
      200069006E002000660075006E0063007400690065002000760061006E002000
      68006500740020006B0072006500640069006500740062006500640072006100
      67002C00200068006500740020006D006100780069006D0075006D0020007000
      65007200630065006E007400610067006500200069007300200001004C006500
      20006D006F006E00740061006E00740020006400650020006C00610020006300
      6F006D006D0069007300730069006F006E002000650073007400200074007200
      6F0070002000E9006C0065007600E9002000730065006C006F006E0020006C00
      650020006D006F006E00740061006E007400200064007500200063007200E900
      6400690074002C0020006C006500200070006F0075007200630065006E007400
      61006700650020006D006100780069006D0075006D0020006500730074002000
      0100540068006500200063006F006D006D0069007300730069006F006E002000
      61006D006F0075006E007400200069007300200074006F006F00200068006900
      67006800200064006500700065006E00640069006E00670020006F006E002000
      740068006500200061006D006F0075006E00740020006F006600200063007200
      65006400690074002C00200074006800650020006D006100780069006D007500
      6D002000700065007200630065006E0074006100670065002000690073002000
      01000D000A00730074004F00740068006500720053007400720069006E006700
      73005F0055006E00690063006F00640065000D000A0045006400690074004900
      6E007400650072006E0061006C00520065006D00610072006B0073002E004600
      690065006C0064004C006100620065006C00010049006E007400650072006E00
      650020006F0070006D00650072006B0069006E00670065006E00010052006500
      6D00610072007100750065007300200069006E007400650072006E0065007300
      010049006E007400650072006E0061006C002000720065006D00610072006B00
      730001000D000A0045006400690074004E006F005200690073006B0073002E00
      4600690065006C0064004C006100620065006C0001004700650065006E002000
      720069007300690063006F00010041007500630075006E002000720069007300
      71007500650001004E006F0020007200690073006B0001000D000A0045006400
      690074004C0069006D0069007400650064005200690073006B0073002E004600
      690065006C0064004C006100620065006C000100420065007000650072006B00
      74002000720069007300690063006F0001005200690073007100750065002000
      6C0069006D0069007400E90001004C0069006D00690074006500640020007200
      690073006B0001000D000A004500640069007400480069006700680052006900
      73006B0073002E004600690065006C0064004C006100620065006C0001004800
      6F006F0067002000720069007300690063006F00010052006900730071007500
      65002000E9006C0065007600E900010048006900670068002000720069007300
      6B0001000D000A0045006400690074004D006100780069006D0075006D004D00
      6F006E00740068006C0079004300680061007200670065002E00460069006500
      6C0064004C006100620065006C0001004D006100780069006D0061006C006500
      20006D00610061006E0064006C00610073007400010043006800610072006700
      650020006D0065006E007300750065006C006C00650020006D00610078006900
      6D0061006C00650001004D006100780069006D0075006D0020006D006F006E00
      740068006C007900200063006800610072006700650001000D000A0045006400
      690074004600720065006500430068006F00690063006500420061006E006B00
      4100630063006F0075006E0074002E004600690065006C0064004C0061006200
      65006C0001005600720069006A00650020006B00650075007A00650020007A00
      6900630068007400720065006B0065006E0069006E00670001004C0069006200
      720065002000630068006F0069007800200063006F006D007000740065002000
      620061006E006300610069007200650001004600720065006500200063006800
      6F006900630065002000620061006E006B0020006100630063006F0075006E00
      740001000D000A0045006400690074004600720065006500430068006F006900
      63006500440065006200740049006E0073007500720061006E00630065002E00
      4600690065006C0064004C006100620065006C0001005600720069006A006500
      20006B00650075007A006500200073006300680075006C006400730061006C00
      64006F0001004C0069006200720065002000630068006F006900780020006400
      7500200073006F006C006400650020006400650020006C006100200064006500
      740074006500010046007200650065002000630068006F006900630065002000
      6F006600200064006500620074002000620061006C0061006E00630065000100
      0D000A00450064006900740043006F0076006500720061006700650044006500
      6200740049006E0073007500720061006E00630065002E004600690065006C00
      64004C006100620065006C000100440065006B006B0069006E00670073006700
      720061006100640001005400610075007800200064006500200063006F007500
      7600650072007400750072006500010043006F00760065007200610067006500
      2000720061007400650001000D000A0045006400690074004600720065006500
      430068006F00690063006500460069007200650049006E007300750072006100
      6E00630065002E004600690065006C0064004C006100620065006C0001005600
      720069006A00650020006B00650075007A00650020006200720061006E006400
      7600650072007A002E0001004C0069006200720065002000630068006F006900
      780020006100730073007500720061006E0063006500200069006E0063006500
      6E00640069006500010046007200650065002000630068006F00690063006500
      20006F00660020006600690072006500200069006E0073007500720061006E00
      6300650001000D000A00450064006900740043006F006D006D0065006E007400
      730043006F006E007400720069006200750074006F0072002E00460069006500
      6C0064004C006100620065006C00010054006F0065006C006900630068007400
      69006E0067002000740075007300730065006E0070006500720073006F006F00
      6E0001004500780070006C006900630061007400690066007300200069006E00
      7400650072006E006D00E9006400690061006900720065000100450078007000
      6C0061006E006100740069006F006E00200069006E007400650072006D006500
      6400690061007200790001000D000A00450064006900740043006F006D006D00
      65006E0074007300410064006D0069006E002E004600690065006C0064004C00
      6100620065006C00010054006F0065006C00690063006800740069006E006700
      200064006F007300730069006500720062006500680065006500720064006500
      720001004500780070006C006900630061007400690066007300200067006500
      7300740069006F006E006E006100690072006500200064006500200066006900
      63006800690065007200730001004500780070006C0061006E00610074006900
      6F006E002000660069006C00650020006D0061006E0061006700650072000100
      0D000A0045006400690074004D006100780069006D0075006D00440075007200
      6100740069006F006E002E004600690065006C0064004C006100620065006C00
      01004D006100780069006D0061006C00650020006C006F006F00700074006900
      6A0064000100440075007200E900650020006D006100780069006D0061006C00
      650001004D006100780069006D0075006D002000640075007200610074006900
      6F006E0001000D000A0045006400690074005000750072006300680061007300
      650050007200690063006500500072006F00700065007200740079002E004600
      690065006C0064004C006100620065006C000100410061006E006B006F006F00
      70007000720069006A0073000100500072006900780020006400270061006300
      6800610074000100500075007200630068006100730065002000700072006900
      6300650001000D000A0045006400690074005000750072006300680061007300
      650043006F0073007400500072006F00700065007200740079002E0046006900
      65006C0064004C006100620065006C0001005200650067006900730074007200
      61007400690065007200650063006800740065006E0001004600720061006900
      730020006400270069006E0073006300720069007000740069006F006E000100
      52006500670069007300740072006100740069006F006E002000660065006500
      730001000D000A004500640069007400500072006F0070006500720074007900
      54007900700065002E004600690065006C0064004C006100620065006C000100
      54007900700065002000700061006E0064000100540079007000650020006400
      65002000700072006F00700072006900E9007400E90001005400790070006500
      20006F0066002000700072006F007000650072007400790001000D000A004500
      640069007400500072006F007000650072007400790041006400640072006500
      730073002E004600690065006C0064004C006100620065006C00010041006400
      7200650073000100410064007200650073007300650001004100640064007200
      65007300730001000D000A004500640069007400500075007200630068006100
      7300650050007200690063006500470072006F0075006E0064004E0065007700
      500072006F0070002E004600690065006C0064004C006100620065006C000100
      470072006F006E006400010053006F006C000100470072006F0075006E006400
      01000D000A004500640069007400500075007200630068006100730065004300
      6F00730074004E0065007700500072006F0070002E004600690065006C006400
      4C006100620065006C0001004B006F007300740065006E00010043006F00FB00
      74007300010043006F0073007400730001000D000A0045006400690074004300
      6F0073007400500072006900630065004200750069006C0064004E0065007700
      500072006F0070002E004600690065006C0064004C006100620065006C000100
      42006F0075007700010043006F006E0073007400720075006300740069006F00
      6E00010043006F006E0073007400720075006300740069006F006E0001000D00
      0A0045006400690074005600610074004200750069006C0064004E0065007700
      500072006F0070002E004600690065006C0064004C006100620065006C000100
      4200540057000100540056004100010056004100540001000D000A0045006400
      6900740043006F00730074004100720063006800690074006500630074004E00
      65007700500072006F0070002E004600690065006C0064004C00610062006500
      6C00010041007200630068006900740065006300740001004100720063006800
      6900740065006300740065000100410072006300680069007400650063007400
      01000D000A004500640069007400560061007400410072006300680069007400
      6500630074004E0065007700500072006F0070002E004600690065006C006400
      4C006100620065006C0001004200540057000100540056004100010056004100
      540001000D000A00450064006900740043006F00730074005000720069006300
      650053007400750064006900650073004E0065007700500072006F0070002E00
      4600690065006C0064004C006100620065006C00010045005000420020002F00
      2000560043004F00010045005000420020002F002000560043004F0001004500
      5000420020002F002000560043004F0001000D000A0045006400690074005600
      6100740053007400750064006900650073004E0065007700500072006F007000
      2E004600690065006C0064004C006100620065006C0001004200540057000100
      540056004100010056004100540001000D000A0045006400690074004F007400
      6500720043006F007300740073004E0065007700500072006F0070002E004600
      690065006C0064004C006100620065006C0001004F0076006500720069006700
      650020006B006F007300740065006E0001004100750074007200650073002000
      6600720061006900730001004F007400680065007200200063006F0073007400
      730001000D000A004500640069007400530061006C0064006F0054006F005400
      61006B0065004F007600650072004F0074006800650072004300720065006400
      6900740073002E004600690065006C0064004C006100620065006C0001005300
      61006C0064006F0020006F0076006500720020007400650020006E0065006D00
      65006E0020006B0072006500640069006500740065006E00010053006F006C00
      640065002000640065007300200063007200E90064006900740073002000E000
      2000720065007000720065006E006400720065000100420061006C0061006E00
      6300650020006F006600200061007000700072006F0070007200690061007400
      69006F006E007300200074006F002000620065002000740061006B0065006E00
      20006F0076006500720001000D000A004500640069007400480061006E006400
      4C00690067006800740069006E0067002E004600690065006C0064004C006100
      620065006C000100480061006E0064006C00690063006800740069006E006700
      01004D00610069006E006C0065007600E90065000100520065006C0065006100
      7300650020006F006600200063006F00760065006E0061006E00740001000D00
      0A0045006400690074005200650069006E0076006500730074006D0065006E00
      740061006C006C006F00770061006E00630065002E004600690065006C006400
      4C006100620065006C00010057006500640065007200620065006C0065006700
      670069006E006700730076006500720067006F006500640069006E0067000100
      43006F006D00700065006E0073006100740069006F006E0020007200E9006900
      6E00760065007300740069007300730065006D0065006E007400010043006F00
      6D00700065006E0073006100740069006F006E0020007200650069006E007600
      6500730074006D0065006E00740001000D000A00450064006900740056006100
      720069006F007500730063006F00730074007300660069006C0065002E004600
      690065006C0064004C006100620065006C00010044006F007300730069006500
      7200010044006F00730073006900650072000100460069006C00650001000D00
      0A00450064006900740056006100720069006F007500730043006F0073007400
      730045007300740069006D006100740069006F006E002E004600690065006C00
      64004C006100620065006C00010053006300680061007400740069006E006700
      010045007300740069006D006100740069006F006E0001004500730074006900
      6D006100740069006F006E0001000D000A004500640069007400500075007200
      63006800610073006500530075006D004400650062007400420061006C006100
      6E00730049006E0073002E004600690065006C0064004C006100620065006C00
      01004B006F006F00700073006F006D0001004100630068006100740001005000
      750072006300680061007300650001000D000A00450064006900740056006100
      720069006F007500730043006F007300740073004F0074006800650072002E00
      4600690065006C0064004C006100620065006C00010041006E00640065007200
      650001004100750074007200650001004F007400680065007200730001000D00
      0A0045006400690074004D006F00720074004700610067006500520065006700
      69007300740072006100740069006F006E002E004600690065006C0064004C00
      6100620065006C0001004800790070006F007400680065006300610069007200
      6500200069006E00730063006800720069006A00760069006E00670001004500
      6E00720065006700690073007400720065006D0065006E007400200068007900
      70006F0074006800E9006300610069007200650001004D006F00720074006700
      610067006500200072006500670069007300740072006100740069006F006E00
      01000D000A0045006400690074004D006F007200740047006100670065004D00
      61006E0064006100740065002E004600690065006C0064004C00610062006500
      6C0001004800790070006F00740068006500630061006900720020006D006100
      6E00640061006100740001004D0061006E006400610074002000680079007000
      6F0074006800E9006300610069007200650001004D006F007200740067006100
      6700650020006D0061006E00640061007400650001000D000A00450064006900
      74004F0077006E005200650073006F00750072006300650073002E0046006900
      65006C0064004C006100620065006C00010045006900670065006E0020006D00
      69006400640065006C0065006E00010052006500730073006F00750072006300
      650073002000700072006F00700072006500730001004F0077006E0020007200
      650073006F007500720063006500730001000D000A0045006400690074005400
      6F00740061006C0049006E0076006500730074004D0065006E0074002E004600
      690065006C0064004C006100620065006C0001003C0062003E0054006F007400
      61006C006500200069006E0076006500730074006500720069006E0067003C00
      2F0062003E0001003C0062003E0049006E007600650073007400690073007300
      65006D0065006E007400200074006F00740061006C003C002F0062003E000100
      3C0062003E0054006F00740061006C00200069006E0076006500730074006D00
      65006E0074003C002F0062003E0001000D000A00450064006900740044006500
      6D0061006E006400650064004300720065006400690074002E00460069006500
      6C0064004C006100620065006C0001003C0062003E0047006500760072006100
      61006700640020006B0072006500640069006500740062006500640072006100
      67003C002F0062003E0001003C0062003E004D006F006E00740061006E007400
      200063007200E9006400690074002000640065006D0061006E006400E9003C00
      2F0062003E0001003C0062003E00430072006500640069007400200061006D00
      6F0075006E00740020007200650071007500650073007400650064003C002F00
      62003E0001000D000A00450064006900740043006F0073007400500072006900
      630065004200750069006C006400520065006E006F0076006100740065002E00
      4600690065006C0064004C006100620065006C00010042006F00750077000100
      43006F006E0073007400720075006300740069006F006E00010043006F006E00
      73007400720075006300740069006F006E0001000D000A004500640069007400
      5600610074004200750069006C006400520065006E006F007600610074006900
      6F006E002E004600690065006C0064004C006100620065006C00010042005400
      57000100540056004100010054005600410001000D000A004500640069007400
      43006F0073007400410072006300680069007400650063007400520065006E00
      6F0076006100740069006F006E002E004600690065006C0064004C0061006200
      65006C0001004100720063006800690074006500630074000100410072006300
      6800690074006500630074006500010041007200630068006900740065006300
      740001000D000A00450064006900740056006100740041007200630068006900
      7400650063007400520065006E006F0076006100740069006F006E002E004600
      690065006C0064004C006100620065006C000100420054005700010054005600
      4100010054005600410001000D000A00450064006900740043006F0073007400
      500072006900630065005300740075006400690065007300520065006E006F00
      76006100740069006F006E002E004600690065006C0064004C00610062006500
      6C00010045005000420020002F002000560043004F0001004500500042002000
      2F002000560043004F00010045005000420020002F002000560043004F000100
      0D000A0045006400690074005600610074005300740075006400690065007300
      520065006E006F0076006100740069006F006E002E004600690065006C006400
      4C006100620065006C0001004200540057000100540056004100010056004100
      540001000D000A0045006400690074004F00740068006500720043006F007300
      74007300520065006E006F0076006100740069006F006E002E00460069006500
      6C0064004C006100620065006C0001004F007600650072006900670065000100
      4100750074007200650001004F00740068006500720001000D000A0045006400
      6900740043007200650064006900740054007900700065002E00460069006500
      6C0064004C006100620065006C000100540079007000650020006B0072006500
      640069006500740001005400790070006500200064006500200063007200E900
      6400690074000100540079007000650020006F00660020006300720065006400
      6900740001000D000A004500640069007400510075006F007400690074007900
      2E004600690065006C0064004C006100620065006C000100510075006F007400
      690074006500690074000100510075006F007400690074007900010051007500
      6F00740069007400790001000D000A0043006F006D0062006F00430072006500
      640069007400460069006C00650054007900700065002E004600690065006C00
      64004C006100620065006C000100410061007200640020006B00720065006400
      69006500740001005400790070006500200064006500200063007200E9006400
      690074000100540079007000650020006F006600200063007200650064006900
      740001000D000A0045006400690074004E006F00740061007200790043006F00
      730074002E004600690065006C0064004C006100620065006C0001004E006F00
      7400610072006900730001004E006F007400610069007200650001004E006F00
      740061007200790001000D000A0045006400690074005F004300720065006400
      690074005F0050007500720070006F007300650073002E004600690065006C00
      64004C006100620065006C00010044006F0065006C0001004200750074000100
      50007500720070006F007300650001000D000A0045006400690074004F007200
      6900670069006E0061006C005F0061006D006F0075006E0074002E0046006900
      65006C0064004C006100620065006C0001004F006F0072007300700072006F00
      6E006B0065006C0069006A006B00200062006500640072006100670001004D00
      6F006E00740061006E00740020006F0072006900670069006E0061006C000100
      4F0072006900670069006E0061006C00200061006D006F0075006E0074000100
      0D000A0045006400690074004D006F006E00740068006C0079005F0063006800
      61007200670065002E004600690065006C0064004C006100620065006C000100
      480075006900640069006700650020006D00610061006E0064006C0061007300
      740001004600720061006900730020006D0065006E007300750065006C007300
      2000610063007400750065006C0073000100430075007200720065006E007400
      20006D006F006E00740068006C00790020006300680061007200670065000100
      0D000A0045006400690074005300740061007200740044006100740065002E00
      4600690065006C0064004C006100620065006C00010053007400610072007400
      64006100740075006D000100440061007400650020006400650020006400E900
      6200750074000100530074006100720074002000640061007400650001000D00
      0A00450064006900740045006E00640044006100740065002E00460069006500
      6C0064004C006100620065006C000100450069006E0064006400610074007500
      6D00010044006100740065002000640065002000660069006E00010045006E00
      64002000640061007400650001000D000A004500640069007400530061006C00
      64006F0044006100740065002E004600690065006C0064004C00610062006500
      6C000100530061006C0064006F002000700065007200010053006F006C006400
      650020007000610072000100420061006C0061006E0063006500200070006500
      720001000D000A004500640069007400530061006C0064006F0041006D006F00
      75006E0074002E004600690065006C0064004C006100620065006C0001004200
      650064007200610067002000730061006C0064006F0001004D006F006E007400
      61006E007400200064007500200073006F006C00640065000100420061006C00
      61006E0063006500200061006D006F0075006E00740001000D000A0043006800
      650063006B0062006F00780050006500720069006F0064007700690074006800
      6F0075007400740061006B0069006E0067006F00750074002E00460069006500
      6C0064004C006100620065006C0001004400750075007200740069006A006400
      20007A006F006E0064006500720020006F0070006E0061006D00650001004400
      75007200E90065002000730061006E0073002000610064006D00690073007300
      69006F006E0001004400750072006100740069006F006E002000770069007400
      68006F00750074002000610064006D0069007300730069006F006E0001000D00
      0A0045006400690074004D006F006E00740068007300540061006B0069006E00
      67004F00750074002E004600690065006C0064004C006100620065006C000100
      4F0070006E0061006D00650070006500720069006F006400650001004400E900
      6C006100690020006400650020007200E9007400720061006300740061007400
      69006F006E0001005700690074006800640072006100770061006C0020007000
      6500720069006F00640001000D000A0045006400690074005200650066007500
      6E006400530079007300740065006D002E004600690065006C0064004C006100
      620065006C0001005400650072007500670062006500740061006C0069006E00
      670073007300790073007400650065006D0001005300790073007400E8006D00
      65002000640065002000720065006D0062006F0075007200730065006D006500
      6E007400010052006500660075006E0064002000730079007300740065006D00
      01000D000A0045006400690074004300610070006900740061006C0044007500
      650044006100740065004F006E0065002E004600690065006C0064004C006100
      620065006C0001004B006100700069007400610061006C002000760065007200
      760061006C006400610067002000310001004300610070006900740061006C00
      2000E90063006800E90061006E00630065002000310001004300610070006900
      740061006C002000640075006500200064006100740065002000310001000D00
      0A00450064006900740049006E0074006500720065007300740046006F007200
      6D0075006C0061002E004600690065006C0064004C006100620065006C000100
      520065006E007400650066006F0072006D0075006C006500010046006F007200
      6D0075006C00650020006400270069006E007400E9007200EA00740001004900
      6E00740065007200650073007400200066006F0072006D0075006C0061000100
      0D000A0045006400690074005300740061007200740049006E00740065007200
      65007300740059006500610072006C0079002E004600690065006C0064004C00
      6100620065006C00010053007400610072007400720065006E00740065002000
      6A006100610072006C0069006A006B007300010049006E007400E9007200EA00
      740020006400650020006400E9007000610072007400200061006E006E007500
      65006C0001005300740061007200740069006E006700200069006E0074006500
      7200650073007400200079006500610072006C00790001000D000A0053007400
      61007200740049006E007400650072006500730074004D006F006E0074006800
      6C0079002E004600690065006C0064004C006100620065006C00010053007400
      610072007400720065006E007400650020006D00610061006E00640065006C00
      69006A006B007300010049006E007400E9007200EA0074002000640065002000
      6400E900700061007200740020006D0065006E007300750065006C0001005300
      740061007200740069006E006700200069006E00740065007200650073007400
      20006D006F006E00740068006C00790001000D000A0045006400690074004900
      6E0064006500780054007900700065002E004600690065006C0064004C006100
      620065006C00010053006F006F0072007400200069006E006400650078000100
      540079007000650020006400270069006E006400650078000100540079007000
      650020006F006600200069006E0064006500780001000D000A00450064006900
      74005300740061007200740049006E0064006500780059006500610072006C00
      79002E004600690065006C0064004C006100620065006C000100530074006100
      7200740069006E0064006500780020006A006100610072006C0069006A006B00
      7300010049006E00640069006300650020006400650020006400E90070006100
      72007400200061006E006E00750065006C000100530074006100720074006900
      6E006700200069006E00640065007800200079006500610072006C0079000100
      0D000A0045006400690074005300740061007200740049006E00640065007800
      4D006F006E00740068006C0079002E004600690065006C0064004C0061006200
      65006C0001005300740061007200740069006E0064006500780020006D006100
      61006E00640065006C0069006A006B007300010049006E006400690063006500
      20006400650020006400E900700061007200740020006D0065006E0073007500
      65006C0001005300740061007200740069006E006700200069006E0064006500
      780020006D006F006E00740068006C00790001000D000A004500640069007400
      4D006100780069006D0075006D004300610070002E004600690065006C006400
      4C006100620065006C0001004D006100780069006D0061006C00650020006300
      61007000010043006100700020006D006100780069006D0075006D0001004D00
      6100780069006D0075006D00200063006100700001000D000A00450064006900
      74004D006100780069006D0075006D0046006C006F006F0072002E0046006900
      65006C0064004C006100620065006C0001004D006100780069006D0061006C00
      6500200066006C006F006F007200010046006C006F006F00720020006D006100
      780069006D0075006D0001004D006100780069006D0075006D00200066006C00
      6F006F00720001000D000A004500640069007400430075007200720065006E00
      740049006E0074006500720065007300740052006100740065002E0046006900
      65006C0064004C006100620065006C0001004800750069006400690067006500
      2000720065006E007400650076006F0065007400010054006100750078002000
      6400270069006E007400E9007200EA0074002000610063007400750065006C00
      0100430075007200720065006E007400200069006E0074006500720065007300
      74002000720061007400650001000D000A0045006400690074004E0065007800
      74005200650076006900730069006F006E0044006100740065002E0046006900
      65006C0064004C006100620065006C00010056006F006C00670065006E006400
      650020006800650072007A00690065006E0069006E0067007300640061007400
      75006D000100500072006F0063006800610069006E0065002000640061007400
      650020006400650020007200E90076006900730069006F006E0001004E006500
      7800740020007200650076006900730069006F006E0020006400610074006500
      01000D000A0045006400690074004400750072006100740069006F006E002E00
      4600690065006C0064004C006100620065006C00010044007500750072007400
      69006A0064000100440075007200E90065000100440075007200610074006900
      6F006E0001000D000A0045006400690074004400610074006500530075006200
      6D0069007300730069006F006E002E004600690065006C0064004C0061006200
      65006C00010044006100740075006D00200069006E006400690065006E006900
      6E00670001004400610074006500200064006500200073006F0075006D006900
      7300730069006F006E000100440061007400650020006F006600200073007500
      62006D0069007300730069006F006E0001000D000A0045006400690074004400
      61007400650041007000700072006F00760061006C002E004600690065006C00
      64004C006100620065006C00010044006100740075006D00200067006F006500
      64006B0065007500720069006E00670001004400610074006500200064002700
      61007000700072006F0062006100740069006F006E0001004400610074006500
      20006F006600200061007000700072006F00760061006C0001000D000A004500
      640069007400440061007400650044006500610064006C0069006E0065005300
      690067006E004F0066006600650072002E004600690065006C0064004C006100
      620065006C000100550069007400650072007300740065002000640061007400
      75006D0020006F006E0064006500720074002E002000610061006E0062006F00
      64000100440061007400650020006C0069006D00690074006500200073006900
      67006E006100740075007200650020006F006600660072006500010044006500
      610064006C0069006E00650020007300690067006E0069006E00670020006F00
      660066006500720001000D000A00450064006900740044006100740065004400
      6500610064006C0069006E0065005300690067006E0044006500650064002E00
      4600690065006C0064004C006100620065006C00010055006900740065007200
      730074006500200064006100740075006D00200061006B007400650020006E00
      6F00740061007200690073000100440061007400650020006C0069006D006900
      7400650020006400750020006E006F007400610069007200650001004E006F00
      7400610072007900200064006500610064006C0069006E006500200064006100
      7400650001000D000A0045006400690074004300720065006400690074004E00
      75006D0062006500720049006E0073007400690074007500740069006F006E00
      2E004600690065006C0064004C006100620065006C0001004E0072002E002000
      64006F007300730069006500720020006D006100610074007300630068006100
      7000700069006A0001004E006F006E002E00200064006F007300730069006500
      7200200073006F0063006900E9007400E90001004E006F002E00200066006900
      6C006500200063006F006D00700061006E00790001000D000A00450064006900
      74005300740061007400750073002E004600690065006C0064004C0061006200
      65006C00010053007400610074007500730020002A0001005300740061007400
      7500740020002A00010053007400610074007500730020002A0001000D000A00
      450064006900740049006E007400650072006E0061006C004E0075006D006200
      650072002E004600690065006C0064004C006100620065006C00010049006E00
      7400650072006E0020006E0075006D006D006500720001004E00B00020006900
      6E007400650072006E006500010049006E007400650072006E0061006C002000
      6E006F002E0001000D000A00450064006900740055007300650072002E004600
      690065006C0064004C006100620065006C00010045006900670065006E006100
      610072000100500072006F00700072006900E900740061006900720065000100
      4F0077006E006500720001000D000A0045006400690074005F00460069006E00
      61006E006300690061006C005F0069006E007300740069007400750074006900
      6F006E002E004600690065006C0064004C006100620065006C00010046006900
      6E002E00200069006E007300740065006C006C0069006E006700010049006E00
      730074002E002000660069006E0061006E0063006900E8007200650001004600
      69006E0061006E006300690061006C00200069006E00730074002E0001000D00
      0A00450064006900740054006F00740061006C0041006D006F0075006E007400
      4300720065006400690074002E004600690065006C0064004C00610062006500
      6C00010054006F007400610061006C0062006500640072006100670020006B00
      72006500640069006500740001004D006F006E00740061006E00740020007400
      6F00740061006C00200063007200E900640069007400010054006F0074006100
      6C002000630072006500640069007400200061006D006F0075006E0074000100
      0D000A00450064006900740054006F00740061006C0041006D006F0075006E00
      740043006F006D006D0069007300730069006F006E002E004600690065006C00
      64004C006100620065006C00010054006F007400610061006C00620065006400
      720061006700200063006F006D006D006900730073006900650001004D006F00
      6E00740061006E007400200074006F00740061006C00200063006F006D006D00
      69007300730069006F006E00010054006F00740061006C00200061006D006F00
      75006E007400200063006F006D006D0069007300730069006F006E0001000D00
      0A00450064006900740044006100740065004300720065006400690074004600
      69006E0061006C002E004600690065006C0064004C006100620065006C000100
      44006100740075006D00200061006B0074006500010044006100740065002000
      6100630074006500010044006100740065002000640065006500640001000D00
      0A0045006400690074005F0043006F006E007400720069006200750074006F00
      72002E004600690065006C0064004C006100620065006C000100540075007300
      730065006E0070006500720073006F006F006E00010049006E00740065007200
      6D00E900640069006100690072006500010049006E007400650072006D006500
      6400690061007200790001000D000A007300740043006F006C006C0065006300
      740069006F006E0073005F0055006E00690063006F00640065000D000A004700
      720069006400440065006D0061006E0064006500720073002E0043006F006C00
      75006D006E0073005B0030005D002E0043006800650063006B0042006F007800
      4600690065006C0064002E004600690065006C006400560061006C0075006500
      7300010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      7300650001000D000A004700720069006400440065006D0061006E0064006500
      720073002E0043006F006C0075006D006E0073005B0030005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001004E00610061006D00
      01004E006F006D0001004E0061006D00650001000D000A004500640069007400
      5300740061007200740049006E00740065007200650073007400590065006100
      72006C0079002E00540072006900670067006500720073005B0030005D002E00
      480069006E00740001004F006D00720065006B0065006E0065006E0020006A00
      61006100720020006E0061006100720020006D00610061006E00640001004300
      6F006E0076006500720074006900720020006C00270061006E006E00E9006500
      200065006E0020006D006F0069007300010043006F006E007600650072007400
      20007900650061007200200074006F0020006D006F006E007400680001000D00
      0A005300740061007200740049006E007400650072006500730074004D006F00
      6E00740068006C0079002E00540072006900670067006500720073005B003000
      5D002E00480069006E00740001004F006D00720065006B0065006E0065006E00
      20006D00610061006E00640020006E0061006100720020006A00610061007200
      010043006F006E0076006500720074006900720020006D006F00690073002000
      65006E00200061006E006E00E9006500010043006F006E007600650072007400
      20006D006F006E0074006800200074006F002000790065006100720001000D00
      0A0045006400690074005300740061007200740049006E006400650078005900
      6500610072006C0079002E00540072006900670067006500720073005B003000
      5D002E00480069006E00740001004F006D00720065006B0065006E0065006E00
      20006A0061006100720020006E0061006100720020006D00610061006E006400
      010043006F006E0076006500720074006900720020006C00270061006E006E00
      E9006500200065006E0020006D006F0069007300010043006F006E0076006500
      7200740020007900650061007200200074006F0020006D006F006E0074006800
      01000D000A0045006400690074005300740061007200740049006E0064006500
      78004D006F006E00740068006C0079002E005400720069006700670065007200
      73005B0030005D002E00480069006E00740001004F006D00720065006B006500
      6E0065006E0020006D00610061006E00640020006E0061006100720020006A00
      610061007200010043006F006E0076006500720074006900720020006D006F00
      69007300200065006E00200061006E006E00E9006500010043006F006E007600
      65007200740020006D006F006E0074006800200074006F002000790065006100
      720001000D000A00470072006900640052006500640075006300740069006F00
      6E0073002E0043006F006C0075006D006E0073005B0030005D002E0043006800
      650063006B0042006F0078004600690065006C0064002E004600690065006C00
      6400560061006C00750065007300010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C0073006500010074007200
      750065003B00660061006C007300650001000D000A0047007200690064005200
      6500640075006300740069006F006E0073002E0043006F006C0075006D006E00
      73005B0030005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001004B006F007200740069006E00670001005200E900640075006300
      740069006F006E00010044006900730063006F0075006E00740001000D000A00
      470072006900640052006500640075006300740069006F006E0073002E004300
      6F006C0075006D006E0073005B0031005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A00470072006900640052006500640075006300
      740069006F006E0073002E0043006F006C0075006D006E0073005B0031005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001005000
      65007200630065006E007400610067006500010050006F007500720063006500
      6E0074006100670065000100500065007200630065006E007400610067006500
      01000D000A0047007200690064004C0065006E0064006500720073002E004300
      6F006C0075006D006E0073005B0030005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A0047007200690064004C0065006E0064006500
      720073002E0043006F006C0075006D006E0073005B0030005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001004E00610061006D00
      01004E006F006D0001004E0061006D00650001000D000A004700720069006400
      470075006100720061006E0074006F00720073002E0043006F006C0075006D00
      6E0073005B0030005D002E0043006800650063006B0042006F00780046006900
      65006C0064002E004600690065006C006400560061006C007500650073000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      01000D000A004700720069006400470075006100720061006E0074006F007200
      73002E0043006F006C0075006D006E0073005B0030005D002E00540069007400
      6C0065002E00430061007000740069006F006E0001004E00610061006D000100
      4E006F006D0001004E0061006D00650001000D000A0045006400690074004900
      6E007400650072006E0061006C004E0075006D006200650072002E0054007200
      6900670067006500720073005B0030005D002E00480069006E00740001004E00
      75006D006D0065007200200074006F0065006B0065006E006E0065006E000100
      410074007400720069006200750065007200200075006E0020006E0075006D00
      E90072006F000100410073007300690067006E0020006E0075006D0062006500
      720001000D000A0073007400430068006100720053006500740073005F005500
      6E00690063006F00640065000D000A00}
  end
  object FinInstitutions: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, NAME from CREDIT_FINANCIALINSTITUTIONS')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 344
    Top = 288
  end
  object DsFinInstitutions: TDataSource
    DataSet = FinInstitutions
    Left = 344
    Top = 336
  end
  object CreditTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 168
    Top = 640
  end
  object DsCreditTypes: TDataSource
    DataSet = CreditTypes
    Left = 168
    Top = 688
  end
  object Demanders: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'SELECT  CRDEMANDER.ID, CRDEMANDER.DEMANDER_TYPE, CRDEMANDER.DEMA' +
        'NDER_ID,'
      'CASE'
      
        '    WHEN CRDEMANDER.DEMANDER_TYPE = 0 THEN COALESCE(SP.LAST_NAME' +
        ', '#39#39') || '#39' '#39' || COALESCE(SP.FIRST_NAME, '#39#39')'
      '    WHEN CRDEMANDER.DEMANDER_TYPE = 1 THEN C.NAME'
      'END AS DEMANDER_NAME'
      ''
      'FROM            credit_file_demanders   CRDEMANDER '
      
        'LEFT OUTER JOIN crm_accounts    C   ON CRDEMANDER.DEMANDER_ID = ' +
        'C.ID'
      
        'LEFT OUTER JOIN crm_contacts    SP  ON CRDEMANDER.DEMANDER_ID = ' +
        'SP.ID'
      'WHERE CRDEMANDER.CREDIT_FILE=:IDCREDITFILE '
      'ORDER BY DEMANDER_NAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 416
    Top = 648
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'IDCREDITFILE'
        ParamType = ptInput
        Value = nil
      end>
  end
  object DsDemanders: TDataSource
    DataSet = Demanders
    Left = 416
    Top = 696
  end
  object PopupMenuDemanderAdd: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 672
    Top = 650
    object BtnAddOrganisation: TUniMenuItem
      Caption = 'Toevoegen organisatie'
      ImageIndex = 20
      OnClick = BtnAddOrganisationClick
    end
    object BtnAddRelation: TUniMenuItem
      Caption = 'Toevoegen relatie'
      ImageIndex = 18
      OnClick = BtnAddRelationClick
    end
  end
  object CreditFiles_Edit: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_CREDIT_FILES_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTr_CreditFiles_Edit
    SQL.Strings = (
      'Select * from credit_files')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 424
    Top = 432
  end
  object Ds_CreditFiles_Edit: TDataSource
    DataSet = CreditFiles_Edit
    Left = 424
    Top = 480
  end
  object UpdTr_CreditFiles_Edit: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 424
    Top = 528
  end
  object CreditPurposes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 168
    Top = 392
  end
  object DsCreditPurposes: TDataSource
    DataSet = CreditPurposes
    Left = 216
    Top = 512
  end
  object RefundSystems: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 328
    Top = 464
  end
  object DsRefundSystems: TDataSource
    DataSet = RefundSystems
    Left = 328
    Top = 520
  end
  object InterestFormulas: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 728
    Top = 528
  end
  object DsInterestFormulas: TDataSource
    DataSet = InterestFormulas
    Left = 728
    Top = 584
  end
  object IndexTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 888
    Top = 528
  end
  object DsIndexTypes: TDataSource
    DataSet = IndexTypes
    Left = 888
    Top = 576
  end
  object Reductions: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    credit_reductions.id,'
      '    basictables.dutch as reductiondescription,'
      '    credit_reductions.reduction_percentage'
      'from credit_reductions'
      
        '   left outer join basictables on (credit_reductions.reduction_i' +
        'd = basictables.id)'
      'order by reductiondescription')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 600
    Top = 584
    object ReductionsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object ReductionsREDUCTIONDESCRIPTION: TWideStringField
      FieldName = 'REDUCTIONDESCRIPTION'
      ReadOnly = True
      Size = 50
    end
    object ReductionsREDUCTION_PERCENTAGE: TFloatField
      FieldName = 'REDUCTION_PERCENTAGE'
      DisplayFormat = '#,##0.00 %'
    end
  end
  object DsReductions: TDataSource
    DataSet = Reductions
    Left = 576
    Top = 696
  end
  object Lenders: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT CRLENDERS.ID, CRLENDERS.LENDER_TYPE, CRLENDERS.LENDER_ID,'
      'CASE'
      
        '    WHEN CRLENDERS.LENDER_TYPE = 0 THEN COALESCE(SP.LAST_NAME, '#39 +
        #39') || '#39' '#39' || COALESCE(SP.FIRST_NAME, '#39#39')'
      '    WHEN CRLENDERS.LENDER_TYPE = 1 THEN C.NAME'
      'END AS LENDER_NAME'
      'FROM credit_file_lenders CRLENDERS '
      'LEFT OUTER JOIN crm_accounts C   ON CRLENDERS.LENDER_ID = C.ID'
      'LEFT OUTER JOIN crm_contacts SP  ON CRLENDERS.LENDER_ID = SP.ID'
      'WHERE CRLENDERS.CREDIT_FILE=:IDCREDITFILE '
      'ORDER BY LENDER_NAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 320
    Top = 648
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'IDCREDITFILE'
        ParamType = ptInput
        Value = nil
      end>
  end
  object DsLenders: TDataSource
    DataSet = Lenders
    Left = 320
    Top = 696
  end
  object PopupMenuLenderAdd: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 800
    Top = 650
    object BtnAddOrganisationLender: TUniMenuItem
      Caption = 'Toevoegen organisatie'
      ImageIndex = 20
      OnClick = BtnAddOrganisationLenderClick
    end
    object BtnAddRelationLender: TUniMenuItem
      Caption = 'Toevoegen relatie'
      ImageIndex = 18
      OnClick = BtnAddRelationLenderClick
    end
  end
  object Guarantors: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'SELECT CRGUARANTORS.ID, CRGUARANTORS.GUARANTOR_TYPE, CRGUARANTOR' +
        'S.GUARANTOR_ID,'
      'CASE'
      
        '    WHEN CRGUARANTORS.GUARANTOR_TYPE = 0 THEN COALESCE(SP.LAST_N' +
        'AME, '#39#39') || '#39' '#39' || COALESCE(SP.FIRST_NAME, '#39#39')'
      '    WHEN CRGUARANTORS.GUARANTOR_TYPE = 1 THEN C.NAME'
      'END AS GUARANTOR_NAME'
      'FROM credit_file_guarantors CRGUARANTORS '
      
        'LEFT OUTER JOIN crm_accounts C   ON CRGUARANTORS.GUARANTOR_ID = ' +
        'C.ID'
      
        'LEFT OUTER JOIN crm_contacts SP  ON CRGUARANTORS.GUARANTOR_ID = ' +
        'SP.ID'
      'WHERE CRGUARANTORS.CREDIT_FILE=:IDCREDITFILE '
      'ORDER BY GUARANTOR_NAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 600
    Top = 520
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'IDCREDITFILE'
        ParamType = ptInput
        Value = nil
      end>
  end
  object DsGuarantors: TDataSource
    DataSet = Guarantors
    Left = 504
    Top = 696
  end
  object PopupMenuGuarantorAdd: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 960
    Top = 650
    object BtnAddOrganisationGuarantor: TUniMenuItem
      Caption = 'Toevoegen organisatie'
      ImageIndex = 20
      OnClick = BtnAddOrganisationGuarantorClick
    end
    object BtnAddRelationGuarantor: TUniMenuItem
      Caption = 'Toevoegen relatie'
      ImageIndex = 18
      OnClick = BtnAddRelationGuarantorClick
    end
  end
end
