object CreditDocumentsAddFrm: TCreditDocumentsAddFrm
  Left = 0
  Top = 0
  ClientHeight = 609
  ClientWidth = 1337
  Caption = 'Selecteren aan te leveren documenten'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditSearch
  Images = UniMainModule.ImageList
  ImageIndex = 100
  OnSubmit = BtnSaveClick
  OnCancel = BtnSaveClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  TextHeight = 15
  object GridDocuments: TUniDBGrid
    Left = 16
    Top = 56
    Width = 697
    Height = 489
    Hint = ''
    HeaderTitle = 'Documenten'
    DataSource = DsDocumentsBrowse
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    TabOrder = 1
    OnDblClick = BtnAddDocumentClick
    Columns = <
      item
        Flex = 1
        FieldName = 'DOCUMENTNAME'
        Title.Caption = 'Omschrijving'
        Width = 500
        ReadOnly = True
        Sortable = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'SHOW_HINT'
        Title.Alignment = taCenter
        Title.Caption = 'Hint'
        Width = 64
        Alignment = taCenter
        Sortable = True
        CheckBoxField.Enabled = False
        CheckBoxField.BooleanFieldOnly = False
        ImageOptions.Width = 12
        ImageOptions.Height = 12
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object GridSelectedDocuments: TUniDBGrid
    Left = 752
    Top = 56
    Width = 569
    Height = 273
    Hint = ''
    HeaderTitle = 'Geselecteerde documenten'
    DataSource = DsSelectedDocuments
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    TabOrder = 2
    OnDblClick = BtnDeleteDocumentClick
    Columns = <
      item
        Flex = 1
        FieldName = 'DESCRIPTION_DUTCH'
        Title.Caption = 'Omschrijving'
        Width = 64
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object BtnSave: TUniThemeButton
    Left = 1184
    Top = 560
    Width = 137
    Height = 33
    Hint = ''
    Caption = 'Sluiten'
    ParentFont = False
    TabOrder = 3
    ScreenMask.Target = Owner
    OnClick = BtnSaveClick
    ButtonTheme = uctSecondary
  end
  object BtnAddDocument: TUniThemeButton
    Left = 720
    Top = 56
    Width = 25
    Height = 25
    Hint = 'Toevoegen document'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 4
    Images = UniMainModule.ImageList
    ImageIndex = 43
    OnClick = BtnAddDocumentClick
    ButtonTheme = uctTertiary
  end
  object BtnDeleteDocument: TUniThemeButton
    Left = 720
    Top = 88
    Width = 25
    Height = 25
    Hint = 'Verwijderen document'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 5
    Images = UniMainModule.ImageList
    ImageIndex = 42
    OnClick = BtnDeleteDocumentClick
    ButtonTheme = uctTertiary
  end
  object PageControlHint: TUniPageControl
    Left = 752
    Top = 344
    Width = 569
    Height = 201
    Hint = ''
    ActivePage = TabSheetDutch
    Images = UniMainModule.ImageList
    TabOrder = 6
    object TabSheetDutch: TUniTabSheet
      Hint = ''
      ImageIndex = 16
      Caption = 'Hint Nederlands'
      object MemoHintDutch: TUniDBMemo
        Left = 16
        Top = 8
        Width = 505
        Height = 137
        Hint = ''
        DataField = 'HINT_DUTCH'
        DataSource = DsSelectedDocuments
        ReadOnly = True
        TabOrder = 0
        TabStop = False
      end
      object BtnEditHintDutch: TUniThemeButton
        Left = 528
        Top = 8
        Width = 28
        Height = 28
        Hint = 'Bewerken hint'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnEditHintDutchClick
        ButtonTheme = uctTertiary
      end
      object BtnSaveHintDutch: TUniThemeButton
        Left = 528
        Top = 40
        Width = 28
        Height = 28
        Hint = 'Opslaan hint'
        Enabled = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnSaveHintDutchClick
        ButtonTheme = uctTertiary
      end
    end
    object TabSheetFrench: TUniTabSheet
      Hint = ''
      ImageIndex = 16
      Caption = 'Hint Frans'
      object MemoHintFrench: TUniDBMemo
        Left = 16
        Top = 8
        Width = 505
        Height = 137
        Hint = ''
        DataField = 'HINT_FRENCH'
        DataSource = DsSelectedDocuments
        ReadOnly = True
        TabOrder = 0
        TabStop = False
      end
      object BtnEditHintFrench: TUniThemeButton
        Left = 528
        Top = 8
        Width = 28
        Height = 28
        Hint = 'Bewerken hint'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnEditHintFrenchClick
        ButtonTheme = uctTertiary
      end
      object BtnSaveHintFrench: TUniThemeButton
        Left = 528
        Top = 40
        Width = 28
        Height = 28
        Hint = 'Opslaan hint'
        Enabled = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnSaveHintFrenchClick
        ButtonTheme = uctTertiary
      end
    end
    object TabSheetEnglish: TUniTabSheet
      Hint = ''
      ImageIndex = 16
      Caption = 'Hint Engels'
      object MemoHintEnglish: TUniDBMemo
        Left = 16
        Top = 8
        Width = 505
        Height = 137
        Hint = ''
        DataField = 'HINT_ENGLISH'
        DataSource = DsSelectedDocuments
        ReadOnly = True
        TabOrder = 0
        TabStop = False
      end
      object BtnEditHintEnglish: TUniThemeButton
        Left = 528
        Top = 8
        Width = 28
        Height = 28
        Hint = 'Bewerken hint'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnEditHintEnglishClick
        ButtonTheme = uctTertiary
      end
      object BtnSaveHintEnglish: TUniThemeButton
        Left = 528
        Top = 40
        Width = 28
        Height = 28
        Hint = 'Opslaan hint'
        Enabled = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnSaveHintEnglishClick
        ButtonTheme = uctTertiary
      end
    end
  end
  object EditSearch: TUniEdit
    Left = 16
    Top = 16
    Width = 297
    Hint = ''
    Text = ''
    TabOrder = 0
    ClearButton = True
    FieldLabel = '<i class="fas fa-search fa-lg " ></i>'
    FieldLabelWidth = 25
    FieldLabelSeparator = ' '
    OnChange = EditSearchChange
  end
  object DocumentsBrowse: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    sys_credit_doc_items.id,'
      
        '    coalesce(sys_credit_doc_categories.description, '#39#39') || '#39' - '#39 +
        ' || coalesce(sys_credit_doc_items.description_dutch, '#39#39') documen' +
        'tname,'
      '    sys_credit_doc_items.description_dutch,'
      '    sys_credit_doc_items.description_french,'
      '    sys_credit_doc_items.description_english,'
      '    sys_credit_doc_items.hint_dutch,'
      '    sys_credit_doc_items.hint_french,'
      '    sys_credit_doc_items.hint_english,'
      '    sys_credit_doc_items.show_hint'
      'from sys_credit_doc_categories'
      
        '   left outer join sys_credit_doc_items on (sys_credit_doc_categ' +
        'ories.id = sys_credit_doc_items.credit_doc_category)'
      
        'where sys_credit_doc_items.removed='#39'0'#39' and sys_credit_doc_items.' +
        'removed='#39'0'#39
      
        'order by sys_credit_doc_categories.description, sys_credit_doc_i' +
        'tems.description_dutch')
    MasterFields = 'ID'
    DetailFields = 'CREDIT_DOC_CATEGORY'
    FetchAll = True
    AutoCommit = False
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 272
    Top = 306
    object DocumentsBrowseID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object DocumentsBrowseDESCRIPTION_DUTCH: TWideStringField
      FieldName = 'DESCRIPTION_DUTCH'
      Size = 200
    end
    object DocumentsBrowseDESCRIPTION_FRENCH: TWideStringField
      FieldName = 'DESCRIPTION_FRENCH'
      Size = 200
    end
    object DocumentsBrowseDESCRIPTION_ENGLISH: TWideStringField
      FieldName = 'DESCRIPTION_ENGLISH'
      Size = 200
    end
    object DocumentsBrowseSHOW_HINT: TSmallintField
      FieldName = 'SHOW_HINT'
      Required = True
      OnGetText = DocumentsBrowseSHOW_HINTGetText
    end
    object DocumentsBrowseHINT_DUTCH: TWideMemoField
      FieldName = 'HINT_DUTCH'
      BlobType = ftWideMemo
    end
    object DocumentsBrowseHINT_FRENCH: TWideMemoField
      FieldName = 'HINT_FRENCH'
      BlobType = ftWideMemo
    end
    object DocumentsBrowseHINT_ENGLISH: TWideMemoField
      FieldName = 'HINT_ENGLISH'
      BlobType = ftWideMemo
    end
    object DocumentsBrowseDOCUMENTNAME: TWideStringField
      FieldName = 'DOCUMENTNAME'
      ReadOnly = True
      Size = 303
    end
  end
  object DsDocumentsBrowse: TDataSource
    DataSet = DocumentsBrowse
    Left = 272
    Top = 374
  end
  object SelectedDocuments: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_CREDIT_DOC_ITEMS_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UniMainModule.UpdTrans
    SQL.Strings = (
      'select '
      '    credit_doc_items.id,'
      '    sys_credit_doc_items.description_dutch,'
      '    sys_credit_doc_items.description_french,'
      '    sys_credit_doc_items.description_english,'
      '    credit_doc_items.sys_credit_doc_item,'
      '    credit_doc_items.hint_dutch,'
      '    credit_doc_items.hint_french,'
      '    credit_doc_items.hint_english'
      'from credit_doc_items'
      
        '   left outer join sys_credit_doc_items on (credit_doc_items.sys' +
        '_credit_doc_item = sys_credit_doc_items.id)')
    FetchAll = True
    AutoCommit = False
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 1000
    Top = 98
  end
  object DsSelectedDocuments: TDataSource
    DataSet = SelectedDocuments
    Left = 1008
    Top = 158
  end
  object LinkedLanguageCreditDocumentsAdd: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'DocumentsBrowseID.DisplayLabel'
      'DocumentsBrowseDESCRIPTION_DUTCH.DisplayLabel'
      'DocumentsBrowseDESCRIPTION_FRENCH.DisplayLabel'
      'DocumentsBrowseDESCRIPTION_ENGLISH.DisplayLabel'
      'DocumentsBrowseSHOW_HINT.DisplayLabel'
      'TCreditDocumentsAddFrm.Layout'
      'DocumentsBrowse.DetailFields'
      'DocumentsBrowseHINT_DUTCH.DisplayLabel'
      'DocumentsBrowseHINT_FRENCH.DisplayLabel'
      'DocumentsBrowseHINT_ENGLISH.DisplayLabel'
      'TabSheetDutch.Layout'
      'MemoHintDutch.FieldLabelSeparator'
      'TabSheetFrench.Layout'
      'MemoHintFrench.FieldLabelSeparator'
      'TabSheetEnglish.Layout'
      'MemoHintEnglish.FieldLabelSeparator'
      'SelectedDocuments.KeyGenerator')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageCreditDocumentsAddChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 272
    Top = 240
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00540043007200650064006900740044006F00630075006D00
      65006E0074007300410064006400460072006D000100530065006C0065006300
      74006500720065006E002000610061006E0020007400650020006C0065007600
      6500720065006E00200064006F00630075006D0065006E00740065006E000100
      5300E9006C0065006300740069006F006E002000640065007300200064006F00
      630075006D0065006E00740073002000E00020006C0069007600720065007200
      0100530065006C0065006300740069006E006700200064006F00630075006D00
      65006E0074007300200074006F002000620065002000640065006C0069007600
      650072006500640001000D000A00420074006E00530061007600650001005300
      6C0075006900740065006E0001004600650072006D0065007200010043006C00
      6F007300650001000D000A005400610062005300680065006500740044007500
      7400630068000100480069006E00740020004E0065006400650072006C006100
      6E0064007300010049006E00640069006300650020004E00E900650072006C00
      61006E0064006100690073000100480069006E00740020004400750074006300
      680001000D000A00540061006200530068006500650074004600720065006E00
      630068000100480069006E00740020004600720061006E007300010049006E00
      640069006300650020004600720061006E00E700610069007300010048006900
      6E00740020004600720065006E006300680001000D000A005400610062005300
      680065006500740045006E0067006C006900730068000100480069006E007400
      200045006E00670065006C007300010049006E00640069006300650020004100
      6E0067006C006100690073000100480069006E007400200045006E0067006C00
      69007300680001000D000A0073007400480069006E00740073005F0055006E00
      690063006F00640065000D000A00420074006E0041006400640044006F006300
      75006D0065006E007400010054006F00650076006F006500670065006E002000
      64006F00630075006D0065006E007400010041006A006F007500740065007200
      200064006F00630075006D0065006E0074000100410064006400200064006F00
      630075006D0065006E00740001000D000A00420074006E00440065006C006500
      7400650044006F00630075006D0065006E007400010056006500720077006900
      6A0064006500720065006E00200064006F00630075006D0065006E0074000100
      5300750070007000720069006D0065007200200064006F00630075006D006500
      6E0074000100440065006C00650074006500200064006F00630075006D006500
      6E00740001000D000A00420074006E004500640069007400480069006E007400
      440075007400630068000100420065007700650072006B0065006E0020006800
      69006E00740001004D006F0064006900660069006500720020006C0027006900
      6E00640069006300650001004D006F0064006900660079002000680069006E00
      740001000D000A00420074006E005300610076006500480069006E0074004400
      750074006300680001004F00700073006C00610061006E002000680069006E00
      740001005300610075007600650067006100720064006500720020006C002700
      69006E006400690063006500010053006100760065002000680069006E007400
      01000D000A00420074006E004500640069007400480069006E00740046007200
      65006E00630068000100420065007700650072006B0065006E00200068006900
      6E00740001004D006F0064006900660069006500720020006C00270069006E00
      640069006300650001004D006F0064006900660079002000680069006E007400
      01000D000A00420074006E005300610076006500480069006E00740046007200
      65006E006300680001004F00700073006C00610061006E002000680069006E00
      740001005300610075007600650067006100720064006500720020006C002700
      69006E006400690063006500010053006100760065002000680069006E007400
      01000D000A00420074006E004500640069007400480069006E00740045006E00
      67006C006900730068000100420065007700650072006B0065006E0020006800
      69006E00740001004D006F0064006900660069006500720020006C0027006900
      6E00640069006300650001004D006F0064006900660079002000680069006E00
      740001000D000A00420074006E005300610076006500480069006E0074004500
      6E0067006C0069007300680001004F00700073006C00610061006E0020006800
      69006E0074000100530061007500760065006700610072006400650072002000
      6C00270069006E00640069006300650001005300610076006500200068006900
      6E00740001000D000A007300740044006900730070006C00610079004C006100
      620065006C0073005F0055006E00690063006F00640065000D000A0073007400
      46006F006E00740073005F0055006E00690063006F00640065000D000A007300
      74004D0075006C00740069004C0069006E00650073005F0055006E0069006300
      6F00640065000D000A007300740053007400720069006E00670073005F005500
      6E00690063006F00640065000D000A007300740072007300740072005F006500
      720072006F0072005F0061006400640069006E006700010046006F0075007400
      2000620069006A002000680065007400200074006F00650076006F0065006700
      65006E00010045007200720065007500720020006C006F007200730020006400
      650020006C00270061006A006F007500740001004500720072006F0072002000
      61006400640069006E00670001000D000A007300740072007300740072005F00
      6500720072006F0072005F00640065006C006500740069006E00670001004600
      6F00750074002000620069006A00200068006500740020007600650072007700
      69006A0064006500720065006E00010045007200720065007500720020006C00
      6F007200730020006400650020006C0061002000730075007000700072006500
      7300730069006F006E0001004500720072006F0072002000640065006C006500
      740069006E00670001000D000A007300740072007300740072005F0065007200
      72006F0072005F0073006100760069006E0067005F0064006100740061000100
      46006F00750074002000620069006A00200068006500740020006F0070007300
      6C00610061006E002000760061006E0020006400650020006700650067006500
      760065006E007300010045007200720065007500720020006C006F0072007300
      20006400650020006C00270065006E0072006500670069007300740072006500
      6D0065006E0074002000640065007300200064006F006E006E00E90065007300
      01004500720072006F007200200073006100760069006E006700200074006800
      65002000640061007400610001000D000A00730074004F007400680065007200
      53007400720069006E00670073005F0055006E00690063006F00640065000D00
      0A00470072006900640044006F00630075006D0065006E00740073002E004800
      650061006400650072005400690074006C006500010044006F00630075006D00
      65006E00740065006E00010044006F00630075006D0065006E00740073000100
      44006F00630075006D0065006E007400730001000D000A004700720069006400
      530065006C006500630074006500640044006F00630075006D0065006E007400
      73002E004800650061006400650072005400690074006C006500010047006500
      730065006C0065006300740065006500720064006500200064006F0063007500
      6D0065006E00740065006E00010044006F00630075006D0065006E0074007300
      20007300E9006C0065006300740069006F006E006E00E9007300010053006500
      6C0065006300740065006400200064006F00630075006D0065006E0074007300
      01000D000A007300740043006F006C006C0065006300740069006F006E007300
      5F0055006E00690063006F00640065000D000A00470072006900640044006F00
      630075006D0065006E00740073002E0043006F006C0075006D006E0073005B00
      30005D002E0043006800650063006B0042006F0078004600690065006C006400
      2E004600690065006C006400560061006C007500650073000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C007300650001000D000A00
      470072006900640044006F00630075006D0065006E00740073002E0043006F00
      6C0075006D006E0073005B0030005D002E005400690074006C0065002E004300
      61007000740069006F006E0001004F006D00730063006800720069006A007600
      69006E00670001004400650073006300720069007000740069006F006E000100
      4400650073006300720069007000740069006F006E0001000D000A0047007200
      6900640044006F00630075006D0065006E00740073002E0043006F006C007500
      6D006E0073005B0031005D002E0043006800650063006B0042006F0078004600
      690065006C0064002E004600690065006C006400560061006C00750065007300
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      650001000D000A00470072006900640044006F00630075006D0065006E007400
      73002E0043006F006C0075006D006E0073005B0031005D002E00540069007400
      6C0065002E00430061007000740069006F006E000100480069006E0074000100
      49006E0064006900630065000100480069006E00740001000D000A0047007200
      69006400530065006C006500630074006500640044006F00630075006D006500
      6E00740073002E0043006F006C0075006D006E0073005B0030005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      530065006C006500630074006500640044006F00630075006D0065006E007400
      73002E0043006F006C0075006D006E0073005B0030005D002E00540069007400
      6C0065002E00430061007000740069006F006E0001004F006D00730063006800
      720069006A00760069006E006700010044006500730063007200690070007400
      69006F006E0001004400650073006300720069007000740069006F006E000100
      0D000A0073007400430068006100720053006500740073005F0055006E006900
      63006F00640065000D000A00}
  end
end
