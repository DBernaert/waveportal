unit CreditDocumentsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniGUIBaseClasses,
  uniBasicGrid, uniDBGrid, uniButton, UniThemeButton, siComp, siLngLnk,
  uniPageControl, uniMemo, uniDBMemo, uniPanel, uniEdit;

var
	str_error_adding:      string = 'Fout bij het toevoegen'; // TSI: Localized (Don't modify!)
	str_error_deleting:    string = 'Fout bij het verwijderen'; // TSI: Localized (Don't modify!)
	str_error_saving_data: string = 'Fout bij het opslaan van de gegevens'; // TSI: Localized (Don't modify!)

type
  TCreditDocumentsAddFrm = class(TUniForm)
    GridDocuments: TUniDBGrid;
    DocumentsBrowse: TIBCQuery;
    DocumentsBrowseID: TLargeintField;
    DocumentsBrowseDESCRIPTION_DUTCH: TWideStringField;
    DocumentsBrowseDESCRIPTION_FRENCH: TWideStringField;
    DocumentsBrowseDESCRIPTION_ENGLISH: TWideStringField;
    DocumentsBrowseSHOW_HINT: TSmallintField;
    DsDocumentsBrowse: TDataSource;
    GridSelectedDocuments: TUniDBGrid;
    BtnSave: TUniThemeButton;
    SelectedDocuments: TIBCQuery;
    DsSelectedDocuments: TDataSource;
    BtnAddDocument: TUniThemeButton;
    BtnDeleteDocument: TUniThemeButton;
    LinkedLanguageCreditDocumentsAdd: TsiLangLinked;
    PageControlHint: TUniPageControl;
    TabSheetDutch: TUniTabSheet;
    TabSheetFrench: TUniTabSheet;
    TabSheetEnglish: TUniTabSheet;
    MemoHintDutch: TUniDBMemo;
    MemoHintFrench: TUniDBMemo;
    MemoHintEnglish: TUniDBMemo;
    BtnEditHintEnglish: TUniThemeButton;
    BtnSaveHintEnglish: TUniThemeButton;
    DocumentsBrowseHINT_DUTCH: TWideMemoField;
    DocumentsBrowseHINT_FRENCH: TWideMemoField;
    DocumentsBrowseHINT_ENGLISH: TWideMemoField;
    BtnEditHintDutch: TUniThemeButton;
    BtnSaveHintDutch: TUniThemeButton;
    BtnEditHintFrench: TUniThemeButton;
    BtnSaveHintFrench: TUniThemeButton;
    DocumentsBrowseDOCUMENTNAME: TWideStringField;
    EditSearch: TUniEdit;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure DocumentsBrowseSHOW_HINTGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure BtnAddDocumentClick(Sender: TObject);
    procedure BtnDeleteDocumentClick(Sender: TObject);
    procedure LinkedLanguageCreditDocumentsAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnEditHintEnglishClick(Sender: TObject);
    procedure BtnSaveHintEnglishClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure BtnEditHintDutchClick(Sender: TObject);
    procedure BtnSaveHintDutchClick(Sender: TObject);
    procedure BtnEditHintFrenchClick(Sender: TObject);
    procedure BtnSaveHintFrenchClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
  private
    { Private declarations }
    _Module_id: integer;
    _Master_id: longint;

    procedure DisableAllControls;
    procedure EnableAllControls;
  public
    { Public declarations }
    function initialize_credit_documents_add(Module_id: integer; Master_id: longint): boolean;
  end;

function CreditDocumentsAddFrm: TCreditDocumentsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function CreditDocumentsAddFrm: TCreditDocumentsAddFrm;
begin
  Result := TCreditDocumentsAddFrm(UniMainModule.GetFormInstance(TCreditDocumentsAddFrm));
end;

{ TCreditDocumentsAddFrm }

procedure TCreditDocumentsAddFrm.BtnAddDocumentClick(Sender: TObject);
var SQL: string;
begin
  if (DocumentsBrowse.Active) and (not DocumentsBrowse.FieldByName('Id').IsNull) then begin
    if not SelectedDocuments.Locate('Sys_credit_doc_item', DocumentsBrowse.FieldByName('Id').AsInteger, []) then begin
      UniMainModule.QBatch.Close;
      SQL := 'insert into credit_doc_items(companyid, module_id, record_id, sys_credit_doc_item, ' +
             'date_entered, date_modified, created_user_id, modified_user_id, present, validated, hint_dutch, hint_french, hint_english) ' +
             'values(:companyid, :module_id, :record_id, :sys_credit_doc_item, :date_entered, ' +
             ':date_modified, :created_user_id, :modified_user_id, :present, :validated, :hint_dutch, :hint_french, :hint_english)';
      UniMainModule.QBatch.SQL.Text := SQL;
      UniMainModule.QBatch.ParamByName('CompanyId').AsInteger           := UniMainModule.Company_id;
      UniMainModule.QBatch.ParamByName('Module_id').AsInteger           := _Module_id;
      UniMainModule.QBatch.ParamByName('Record_id').AsInteger           := _Master_id;
      UniMainModule.QBatch.ParamByName('sys_credit_doc_item').AsInteger := DocumentsBrowse.FieldbyName('Id').AsInteger;
      UniMainModule.QBatch.ParamByName('Date_entered').AsDateTime       := Now;
      UniMainModule.QBatch.ParamByName('Date_modified').AsDateTime      := Now;
      UniMainModule.QBatch.ParamByName('Created_user_id').AsInteger     := UniMainModule.User_id;
      UniMainModule.QBatch.ParamByName('Modified_user_id').AsInteger    := UniMainModule.User_id;
      UniMainModule.QBatch.ParamByName('Present').AsInteger             := 0;
      UniMainModule.QBatch.ParamByName('Validated').AsInteger           := 0;
      UniMainModule.QBatch.ParamByName('Hint_dutch').AsString           := DocumentsBrowse.FieldByName('Hint_dutch').AsString;
      UniMainModule.QBatch.ParamByName('Hint_french').AsString          := DocumentsBrowse.FieldByName('Hint_french').AsString;
      UniMainModule.QBatch.ParamByName('Hint_english').AsString         := DocumentsBrowse.FieldByName('Hint_english').AsString;

      Try
        UniMainModule.QBatch.ExecSQL;
        if UniMainModule.UpdTr_Batch.Active then
          UniMainModule.UpdTr_Batch.Commit;
        UniMainModule.QBatch.Close;
        SelectedDocuments.Refresh;
      Except
        if UniMainModule.UpdTr_Batch.Active then
          UniMainModule.UpdTr_Batch.Rollback;
        UniMainModule.QBatch.Close;
        MainForm.WaveShowErrorToast(str_error_adding);
      End;
    end;
  end;
end;

procedure TCreditDocumentsAddFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TCreditDocumentsAddFrm.BtnDeleteDocumentClick(Sender: TObject);
begin
  if (SelectedDocuments.Active = False) or (SelectedDocuments.FieldByName('Id').IsNull) then
    exit;

  UniMainModule.QBatch.Close;
  UniMainModule.QBatch.SQL.Text := 'Delete from credit_doc_items where id=:Id';
  UniMainModule.QBatch.ParamByName('Id').AsInteger := SelectedDocuments.FieldByName('Id').AsInteger;

  Try
    UniMainModule.QBatch.ExecSQL;
    if UniMainModule.UpdTr_Batch.Active then
      UniMainModule.UpdTr_Batch.Commit;
    UniMainModule.QBatch.Close;
    SelectedDocuments.Refresh;
  Except
    if UniMainModule.UpdTr_Batch.Active then
      UniMainModule.UpdTr_Batch.Rollback;
    UniMainModule.QBatch.Close;
    MainForm.WaveShowErrorToast(str_error_deleting);
  End;
end;

procedure TCreditDocumentsAddFrm.BtnEditHintDutchClick(Sender: TObject);
begin
  if (SelectedDocuments.Active = False) or (SelectedDocuments.FieldByName('Id').IsNull)
  then exit;

  DisableAllControls;
  SelectedDocuments.Edit;
  MemoHintDutch.ReadOnly   := False;
  BtnSaveHintDutch.Enabled := True;
  MemoHintDutch.SetFocus;
end;

procedure TCreditDocumentsAddFrm.BtnEditHintEnglishClick(Sender: TObject);
begin
  if (SelectedDocuments.Active = False) or (SelectedDocuments.FieldByName('Id').IsNull)
  then exit;

  DisableAllControls;
  SelectedDocuments.Edit;
  MemoHintEnglish.ReadOnly   := False;
  BtnSaveHintEnglish.Enabled := True;
  MemoHintEnglish.SetFocus;
end;

procedure TCreditDocumentsAddFrm.BtnEditHintFrenchClick(Sender: TObject);
begin
  if (SelectedDocuments.Active = False) or (SelectedDocuments.FieldByName('Id').IsNull)
  then exit;

  DisableAllControls;
  SelectedDocuments.Edit;
  MemoHintFrench.ReadOnly   := False;
  BtnSaveHintFrench.Enabled := True;
  MemoHintFrench.SetFocus;
end;

procedure TCreditDocumentsAddFrm.DisableAllControls;
begin
  GridDocuments.Enabled          := False;
  BtnAddDocument.Enabled         := False;
  BtnDeleteDocument.Enabled      := False;
  GridSelectedDocuments.Enabled  := False;

  BtnEditHintDutch.Enabled       := False;
  BtnSaveHintDutch.Enabled       := False;
  BtnEditHintFrench.Enabled      := False;
  BtnSaveHintFrench.Enabled      := False;
  BtnEditHintEnglish.Enabled     := False;
  BtnSaveHintEnglish.Enabled     := False;

  BtnSave.Enabled                := False;
end;

procedure TCreditDocumentsAddFrm.EditSearchChange(Sender: TObject);
begin
  if Trim(EditSearch.Text) = ''
  then DocumentsBrowse.Filtered := False
  else begin
         DocumentsBrowse.Filter := 'DocumentName like ' + QuotedStr('%' + EditSearch.Text + '%');
         DocumentsBrowse.Filtered := True;
       end;
end;

procedure TCreditDocumentsAddFrm.EnableAllControls;
begin
  GridDocuments.Enabled          := True;
  BtnAddDocument.Enabled         := True;
  BtnDeleteDocument.Enabled      := True;
  GridSelectedDocuments.Enabled  := True;

  BtnEditHintDutch.Enabled       := True;
  BtnSaveHintDutch.Enabled       := True;
  BtnEditHintFrench.Enabled      := True;
  BtnSaveHintFrench.Enabled      := True;
  BtnEditHintEnglish.Enabled     := True;
  BtnSaveHintEnglish.Enabled     := True;

  BtnSave.Enabled                := True;
end;

procedure TCreditDocumentsAddFrm.BtnSaveClick(Sender: TObject);
begin
  Close;
end;

procedure TCreditDocumentsAddFrm.BtnSaveHintDutchClick(Sender: TObject);
begin
  Try
    SelectedDocuments.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
  Except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    MainForm.WaveShowErrorToast(str_error_saving_data);
  End;

  EnableAllControls;
  MemoHintDutch.ReadOnly   := True;
  BtnSaveHintDutch.Enabled := False;
end;

procedure TCreditDocumentsAddFrm.BtnSaveHintEnglishClick(Sender: TObject);
begin
  Try
    SelectedDocuments.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
  Except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    MainForm.WaveShowErrorToast(str_error_saving_data);
  End;

  EnableAllControls;
  MemoHintEnglish.ReadOnly   := True;
  BtnSaveHintEnglish.Enabled := False;
end;

procedure TCreditDocumentsAddFrm.BtnSaveHintFrenchClick(Sender: TObject);
begin
  Try
    SelectedDocuments.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
  Except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    MainForm.WaveShowErrorToast(str_error_saving_data);
  End;

  EnableAllControls;
  MemoHintFrench.ReadOnly   := True;
  BtnSaveHintFrench.Enabled := False;
end;

procedure TCreditDocumentsAddFrm.DocumentsBrowseSHOW_HINTGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>' //groen
    else Text := '<i class="fas fa-check-circle fa-lg" style=color:#E0E0E0; ></i>' //grijs
  end;
end;

function TCreditDocumentsAddFrm.initialize_credit_documents_add(
  Module_id: integer; Master_id: longint): boolean;
var SQL: string;
begin
  _Module_id := Module_id;
  _Master_id := Master_id;

  DocumentsBrowse.Open;

  SelectedDocuments.Close;
  SQL := 'select credit_doc_items.id, sys_credit_doc_items.description_dutch, sys_credit_doc_items.description_french, ' +
         'sys_credit_doc_items.description_english, credit_doc_items.sys_credit_doc_item, ' +
         'credit_doc_items.hint_dutch, ' +
         'credit_doc_items.hint_french, ' +
         'credit_doc_items.hint_english ' +
         'from credit_doc_items ' +
         'left outer join sys_credit_doc_items on (credit_doc_items.sys_credit_doc_item = sys_credit_doc_items.id) ' +
         'where credit_doc_items.companyid=:CompanyId and credit_doc_items.module_id=:ModuleId ' +
         'and credit_doc_items.record_id=:RecordId ';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by sys_credit_doc_items.description_dutch';
       GridSelectedDocuments.Columns[0].FieldName := 'Description_dutch';
     end;
  2: begin
       SQL := SQL + ' order by sys_credit_doc_items.description_french';
       GridSelectedDocuments.Columns[0].FieldName := 'Description_french';
     end;
  3: begin
       SQL := SQL + ' order by sys_credit_doc_items.description_english';
       GridSelectedDocuments.Columns[0].FieldName := 'Description_english';
     end;
  end;

  SelectedDocuments.SQL.Text := SQL;
  SelectedDocuments.ParamByName('CompanyId').AsInteger  := UniMainModule.Company_id;
  SelectedDocuments.ParamByname('ModuleId').AsInteger   := _Module_id;
  SelectedDocuments.ParamByName('RecordId').AsInteger   := _Master_id;
  SelectedDocuments.Open;

  Result := True;
end;

procedure TCreditDocumentsAddFrm.LinkedLanguageCreditDocumentsAddChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditDocumentsAddFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditDocumentsAddFrm.UniFormReady(Sender: TObject);
begin
  MemoHintDutch.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
  MemoHintFrench.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
  MemoHintEnglish.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
end;

procedure TCreditDocumentsAddFrm.UpdateStrings;
begin
  str_error_saving_data := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens' *) );
  str_error_deleting    := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_error_deleting' (* 'Fout bij het verwijderen' *) );
  str_error_adding      := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_error_adding' (* 'Fout bij het toevoegen' *) );
end;

end.

