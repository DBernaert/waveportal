object CreditFilesAddFrm: TCreditFilesAddFrm
  Left = 0
  Top = 0
  ClientHeight = 657
  ClientWidth = 1097
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditStatus
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControlCreditDemand: TUniPageControl
    Left = 16
    Top = 16
    Width = 1065
    Height = 577
    Hint = ''
    ActivePage = TabSheetStatus
    Images = UniMainModule.ImageList
    LayoutConfig.Region = 'center'
    TabOrder = 0
    object TabSheetStatus: TUniTabSheet
      Hint = ''
      ImageIndex = 26
      Caption = 'Status'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 1090
      ExplicitHeight = 690
      object Edit_Contributor: TUniDBLookupComboBox
        Left = 16
        Top = 16
        Width = 489
        Hint = ''
        ListField = 'FULLNAME'
        ListSource = DsContributors
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'CONTRIBUTOR_ID'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabStop = False
        TabOrder = 0
        ReadOnly = True
        Color = clWindow
        FieldLabel = 'Aanbrenger'
        FieldLabelWidth = 150
        ForceSelection = True
        Style = csDropDown
      end
      object EditStatus: TUniDBLookupComboBox
        Left = 16
        Top = 48
        Width = 489
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsStatus
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'STATUS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Status *'
        FieldLabelWidth = 150
        Style = csDropDown
      end
      object Edit_Financial_institution: TUniDBLookupComboBox
        Left = 16
        Top = 80
        Width = 489
        Hint = ''
        ListField = 'NAME'
        ListSource = DsFinInstitutions
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'FINANCIAL_INSTITUTION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Fin. instelling'
        FieldLabelWidth = 150
        ForceSelection = True
        Style = csDropDown
      end
      object EditCreditType: TUniDBLookupComboBox
        Left = 560
        Top = 16
        Width = 489
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsCreditTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'TYPE_OF_CREDIT'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 3
        Color = clWindow
        FieldLabel = 'Type krediet'
        FieldLabelWidth = 150
        Style = csDropDown
      end
      object EditTotalAmountCredit: TUniDBFormattedNumberEdit
        Left = 560
        Top = 48
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'TOTAL_AMOUNT_CREDIT'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 4
        FieldLabel = 'Totaalbedrag krediet'
        FieldLabelWidth = 150
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditDateCreditFinal: TUniDBDateTimePicker
        Left = 560
        Top = 80
        Width = 489
        Hint = ''
        DataField = 'DATE_CREDIT_FINAL'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        DateTime = 43637.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        ReadOnly = True
        TabOrder = 5
        TabStop = False
        FieldLabel = 'Datum akte'
        FieldLabelWidth = 150
      end
      object ContainerDemanders: TUniContainerPanel
        Left = 16
        Top = 112
        Width = 489
        Height = 417
        Hint = ''
        ParentColor = False
        TabOrder = 6
        Layout = 'border'
        object ContainerFilter: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 489
          Height = 25
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerFilterRight: TUniContainerPanel
            Left = 257
            Top = 0
            Width = 232
            Height = 25
            Hint = ''
            ParentColor = False
            Align = alRight
            TabOrder = 1
            LayoutConfig.Region = 'east'
            object NavigatorDemanders: TUniDBNavigator
              Left = 128
              Top = 0
              Width = 105
              Height = 25
              Hint = ''
              DataSource = DsDemanders
              VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
              IconSet = icsFontAwesome
              TabStop = False
              TabOrder = 1
            end
            object PopupAddDemander: TUniMenuButton
              Left = 0
              Top = 0
              Width = 57
              Height = 25
              Hint = ''
              DropdownMenu = PopupMenuDemanderAdd
              Caption = ''
              TabOrder = 2
              Images = UniMainModule.ImageList
              ImageIndex = 10
            end
            object BtnModifyDemander: TUniThemeButton
              Left = 64
              Top = 0
              Width = 25
              Height = 25
              Hint = 'Wijzigen aanvrager'
              ShowHint = True
              ParentShowHint = False
              Caption = ''
              TabOrder = 3
              Images = UniMainModule.ImageList
              ImageIndex = 11
              OnClick = BtnModifyDemanderClick
              ButtonTheme = uctDefault
            end
            object BtnDeleteDemander: TUniThemeButton
              Left = 96
              Top = 0
              Width = 25
              Height = 25
              Hint = 'Verwijderen aanvrager'
              ShowHint = True
              ParentShowHint = False
              Caption = ''
              TabOrder = 4
              Images = UniMainModule.ImageList
              ImageIndex = 12
              OnClick = BtnDeleteDemanderClick
              ButtonTheme = uctDefault
            end
          end
          object ContainerFilterLeft: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 25
            Hint = ''
            ParentColor = False
            Align = alLeft
            TabOrder = 2
            LayoutConfig.Region = 'west'
            object LblDemanders: TUniLabel
              Left = 0
              Top = 6
              Width = 69
              Height = 13
              Hint = ''
              Caption = 'Aanvragers:'
              ParentFont = False
              Font.Style = [fsBold]
              TabOrder = 1
            end
          end
        end
        object GridDemanders: TUniDBGrid
          Left = 0
          Top = 25
          Width = 489
          Height = 392
          Hint = ''
          DataSource = DsDemanders
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
          WebOptions.Paged = False
          LoadMask.Enabled = False
          LoadMask.Message = 'Loading data...'
          ForceFit = True
          LayoutConfig.Cls = 'customGrid'
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '8 0 0 0'
          Align = alClient
          TabOrder = 2
          OnDblClick = BtnModifyDemanderClick
          Columns = <
            item
              FieldName = 'DEMANDER_NAME'
              Title.Caption = 'Naam'
              Width = 64
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end>
        end
      end
    end
    object TabSheetProfile: TUniTabSheet
      Hint = ''
      ImageIndex = 31
      Caption = 'Risicoprofiel / persoonlijke voorkeuren'
      object LblRiskprofile: TUniLabel
        Left = 16
        Top = 16
        Width = 69
        Height = 13
        Hint = ''
        Caption = 'Risicoprofiel'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 0
      end
      object EditNoRisks: TUniDBCheckBox
        Left = 32
        Top = 40
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'NO_RISKS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 1
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Geen risico'
        FieldLabelWidth = 180
      end
      object EditLimitedRisks: TUniDBCheckBox
        Left = 32
        Top = 64
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'LIMITED_RISKS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 2
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Beperkt risico'
        FieldLabelWidth = 180
      end
      object EditHighRisks: TUniDBCheckBox
        Left = 32
        Top = 88
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'HIGH_RISKS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 3
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Hoog risico'
        FieldLabelWidth = 180
      end
      object EditMaximumMonthlyCharge: TUniDBFormattedNumberEdit
        Left = 32
        Top = 112
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MAXIMUM_MONTHLY_CHARGE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 4
        FieldLabel = 'Maximale maandlast'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object LblPersonalPreferences: TUniLabel
        Left = 536
        Top = 16
        Width = 139
        Height = 13
        Hint = ''
        Caption = 'Persoonlijke voorkeuren'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 6
      end
      object EditFreeChoiceBankAccount: TUniDBCheckBox
        Left = 560
        Top = 40
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'FREE_CHOICE_OF_BANKACCOUNT'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 7
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Vrije keuze zichtrekening'
        FieldLabelWidth = 180
      end
      object EditFreeChoiceDebtInsurance: TUniDBCheckBox
        Left = 560
        Top = 64
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'FREE_CHOICE_DEBT_INSURANCE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 8
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Vrije keuze schuldsaldo'
        FieldLabelWidth = 180
      end
      object EditCoverageDebtInsurance: TUniDBFormattedNumberEdit
        Left = 560
        Top = 88
        Width = 489
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = '%'
        DataField = 'COVERAGE_DEBT_INSURANCE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 9
        FieldLabel = 'Dekkingsgraad'
        FieldLabelWidth = 180
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditFreeChoiceFireInsurance: TUniDBCheckBox
        Left = 560
        Top = 120
        Width = 489
        Height = 17
        Hint = ''
        DataField = 'FREE_CHOICE_FIRE_INSURANCE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 10
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Vrije keuze brandverz.'
        FieldLabelWidth = 180
      end
      object EditCommentsContributor: TUniDBMemo
        Left = 32
        Top = 184
        Width = 489
        Height = 345
        Hint = ''
        DataField = 'COMMENTS_CONTRIBUTOR'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 11
        FieldLabel = 'Toelichting aanbrenger'
        FieldLabelWidth = 180
        FieldLabelAlign = laTop
      end
      object EditCommentsAdmin: TUniDBMemo
        Left = 568
        Top = 184
        Width = 481
        Height = 345
        Hint = ''
        DataField = 'COMMENTS_USER'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ReadOnly = True
        TabOrder = 12
        TabStop = False
        FieldLabel = 'Toelichting dossierbeheerder'
        FieldLabelWidth = 180
        FieldLabelAlign = laTop
      end
      object EditMaximumDuration: TUniDBNumberEdit
        Left = 32
        Top = 144
        Width = 489
        Height = 22
        Hint = ''
        DataField = 'MAXIMUM_DURATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 5
        FieldLabel = 'Maximale looptijd'
        FieldLabelWidth = 180
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
    end
    object TabSheetPlan: TUniTabSheet
      Hint = ''
      ImageIndex = 32
      Caption = 'Financieel plan'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object LblPurchase: TUniLabel
        Left = 16
        Top = 88
        Width = 50
        Height = 13
        Hint = ''
        Caption = 'Aankoop'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 2
      end
      object EditPurchasePriceProperty: TUniDBFormattedNumberEdit
        Left = 32
        Top = 112
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_PRICE_PROPERTY'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 3
        FieldLabel = 'Aankoopprijs'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPurchaseCostProperty: TUniDBFormattedNumberEdit
        Left = 280
        Top = 112
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_COST_PROPERTY'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 4
        FieldLabel = 'Kosten'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPropertyType: TUniDBLookupComboBox
        Left = 16
        Top = 16
        Width = 497
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsPropertyTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'PROPERTY_TYPE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 0
        Color = clWindow
        FieldLabel = 'Type pand'
        FieldLabelWidth = 120
        ForceSelection = True
        Style = csDropDown
      end
      object EditPropertyAddress: TUniDBEdit
        Left = 16
        Top = 48
        Width = 497
        Height = 22
        Hint = ''
        DataField = 'PROPERTY_ADDRESS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 1
        FieldLabel = 'Adres'
        FieldLabelWidth = 120
      end
      object LblNewBuilding: TUniLabel
        Left = 16
        Top = 152
        Width = 63
        Height = 13
        Hint = ''
        Caption = 'Nieuwbouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 5
      end
      object EditPurchasePriceGroundNewProp: TUniDBFormattedNumberEdit
        Left = 32
        Top = 176
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_PRICE_GROUND_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 6
        FieldLabel = 'Grond'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPurchaseCostNewProp: TUniDBFormattedNumberEdit
        Left = 280
        Top = 176
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_COST_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 7
        FieldLabel = 'Kosten'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostPriceBuildNewProp: TUniDBFormattedNumberEdit
        Left = 32
        Top = 208
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_BUILD_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 8
        FieldLabel = 'Bouw'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatBuildNewProp: TUniDBFormattedNumberEdit
        Left = 280
        Top = 208
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_BUILD_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 9
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostArchitectNewProp: TUniDBFormattedNumberEdit
        Left = 32
        Top = 240
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_ARCHITECT_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 10
        FieldLabel = 'Architect'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatArchitectNewProp: TUniDBFormattedNumberEdit
        Left = 280
        Top = 240
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_ARCHITECT_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 11
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostPriceStudiesNewProp: TUniDBFormattedNumberEdit
        Left = 32
        Top = 272
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_STUDIES_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 12
        FieldLabel = 'EPB / VCO'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatStudiesNewProp: TUniDBFormattedNumberEdit
        Left = 280
        Top = 272
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_STUDIES_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 13
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditOterCostsNewProp: TUniDBFormattedNumberEdit
        Left = 32
        Top = 304
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'OTHER_COSTS_NEW_PROP'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 14
        FieldLabel = 'Overige kosten'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblRefinance: TUniLabel
        Left = 536
        Top = 16
        Width = 86
        Height = 13
        Hint = ''
        Caption = 'Herfinanciering'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 23
      end
      object EditSaldoToTakeOverOtherCredits: TUniDBFormattedNumberEdit
        Left = 552
        Top = 48
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'SALDO_TO_TAKEOVER_OTHER_CREDITS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 24
        FieldLabel = 'Saldo over te nemen kredieten'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditHandLighting: TUniDBFormattedNumberEdit
        Left = 552
        Top = 80
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'HANDLIGHTING'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 25
        FieldLabel = 'Handlichting'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditReinvestmentallowance: TUniDBFormattedNumberEdit
        Left = 552
        Top = 112
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'REINVESTMENT_ALLOWANCE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 26
        FieldLabel = 'Wederbeleggingsvergoeding'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblDiverseCosts: TUniLabel
        Left = 536
        Top = 152
        Width = 85
        Height = 13
        Hint = ''
        Caption = 'Diverse kosten'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 27
      end
      object EditVariouscostsfile: TUniDBFormattedNumberEdit
        Left = 552
        Top = 176
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VARIOUS_COSTS_FILE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 28
        FieldLabel = 'Dossierkosten'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVariousCostsEstimation: TUniDBFormattedNumberEdit
        Left = 552
        Top = 208
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VARIOUS_COSTS_ESTIMATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 29
        FieldLabel = 'Schattingskosten'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditPurchaseSumDebtBalansIns: TUniDBFormattedNumberEdit
        Left = 552
        Top = 240
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'PURCHASE_SUM_DEBTBAL_INS'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 30
        FieldLabel = 'Koopsom schuldsaldoverzekering'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVariousCostsOther: TUniDBFormattedNumberEdit
        Left = 552
        Top = 272
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VARIOUS_COSTS_OTHER'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 31
        FieldLabel = 'Andere'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblCosts: TUniLabel
        Left = 536
        Top = 312
        Width = 95
        Height = 13
        Hint = ''
        Caption = 'Waarborgkosten'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 32
      end
      object EditMortGageRegistration: TUniDBFormattedNumberEdit
        Left = 552
        Top = 336
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MORTGAGE_REGISTRATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 33
        FieldLabel = 'Hypothecaire inschrijving'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditMortGageMandate: TUniDBFormattedNumberEdit
        Left = 552
        Top = 368
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'MORTGAGE_MANDATE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 34
        FieldLabel = 'Hypothecair mandaat'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object LblFinancing: TUniLabel
        Left = 536
        Top = 440
        Width = 68
        Height = 13
        Hint = ''
        Caption = 'Financiering'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 36
      end
      object EditOwnResources: TUniDBFormattedNumberEdit
        Left = 552
        Top = 464
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'OWN_RESOURCES'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 37
        FieldLabel = 'Eigen middelen'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditTotalInvestMent: TUniDBFormattedNumberEdit
        Left = 552
        Top = 400
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'TOTAL_INVESTMENT'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 35
        TabStop = False
        ReadOnly = True
        FieldLabel = '<b>Totale investering</b>'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditDemandedCredit: TUniDBFormattedNumberEdit
        Left = 552
        Top = 496
        Width = 497
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'DEMANDED_CREDIT'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 38
        TabStop = False
        ReadOnly = True
        FieldLabel = '<b>Gevraagd kredietbedrag</b>'
        FieldLabelWidth = 320
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object ImageLink: TUniImage
        Left = 528
        Top = 336
        Width = 16
        Height = 16
        Cursor = crHandPoint
        Hint = 'Link naar notaris.be'
        ShowHint = True
        ParentShowHint = False
        AutoSize = True
        Url = 'images/wv_link_16_gray.png'
        OnClick = ImageLinkClick
      end
      object LblRenovation: TUniLabel
        Left = 16
        Top = 344
        Width = 66
        Height = 13
        Hint = ''
        Caption = 'Verbouwing'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 15
      end
      object EditCostPriceBuildRenovate: TUniDBFormattedNumberEdit
        Left = 32
        Top = 368
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_BUILD_RENOVATE'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 16
        FieldLabel = 'Bouw'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatBuildRenovation: TUniDBFormattedNumberEdit
        Left = 280
        Top = 368
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_BUILD_RENOVATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 17
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostArchitectRenovation: TUniDBFormattedNumberEdit
        Left = 32
        Top = 400
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_ARCHITECT_RENOVATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 18
        FieldLabel = 'Architect'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatArchitectRenovation: TUniDBFormattedNumberEdit
        Left = 280
        Top = 400
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_ARCHITECT_RENOVATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 19
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditCostPriceStudiesRenovation: TUniDBFormattedNumberEdit
        Left = 32
        Top = 432
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'COST_PRICE_STUDIES_RENOVATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 20
        FieldLabel = 'EPB / VCO'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditVatStudiesRenovation: TUniDBFormattedNumberEdit
        Left = 280
        Top = 432
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'VAT_STUDIES_RENOVATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 22
        FieldLabel = 'BTW'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
      object EditOtherCostsRenovation: TUniDBFormattedNumberEdit
        Left = 32
        Top = 464
        Width = 233
        Height = 22
        Hint = ''
        FormattedInput.ShowCurrencySign = True
        FormattedInput.DefaultCurrencySign = False
        FormattedInput.CurrencySign = #8364
        DataField = 'OTHER_COSTS_RENOVATION'
        DataSource = UniMainModule.Ds_CreditFiles_Edit
        TabOrder = 21
        FieldLabel = 'Overige'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
        OnExit = EditPurchasePricePropertyExit
      end
    end
  end
  object BtnSave: TUniThemeButton
    Left = 816
    Top = 608
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 1
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 31
    OnClick = BtnSaveClick
    ButtonTheme = uctTertiary
  end
  object BtnCancel: TUniThemeButton
    Left = 952
    Top = 608
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 2
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctDefault
  end
  object Users: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, coalesce(NAME,'#39#39') || '#39' '#39' || coalesce(FIRSTNAME,'#39#39') AS' +
        ' "USERFULLNAME" from users'
      'order by USERFULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 80
    Top = 352
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 80
    Top = 400
  end
  object Contributors: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, LAST_NAME || '#39' '#39' || FIRST_NAME AS "FULLNAME" from cre' +
        'dit_contributors'
      'order by FULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 168
    Top = 352
  end
  object DsContributors: TDataSource
    DataSet = Contributors
    Left = 168
    Top = 400
  end
  object Status: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 256
    Top = 352
  end
  object DsStatus: TDataSource
    DataSet = Status
    Left = 256
    Top = 400
  end
  object PropertyTypes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 328
    Top = 352
  end
  object DsPropertyTypes: TDataSource
    DataSet = PropertyTypes
    Left = 328
    Top = 400
  end
  object LinkedLanguageCreditFilesAdd: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageCreditFilesAddChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 920
    Top = 352
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00540061006200530068006500650074005300740061007400
      7500730001005300740061007400750073000100530074006100740075007400
      010053007400610074007500730001000D000A00540061006200530068006500
      65007400500072006F00660069006C0065000100520069007300690063006F00
      700072006F006600690065006C0020002F00200070006500720073006F006F00
      6E006C0069006A006B006500200076006F006F0072006B006500750072006500
      6E000100500072006F00660069006C0020006400650020007200690073007100
      7500650020002F00200070007200E9006600E900720065006E00630065007300
      200070006500720073006F006E006E0065006C006C0065007300010052006900
      73006B002000700072006F00660069006C00650020002F002000700065007200
      73006F006E0061006C00200070007200650066006500720065006E0063006500
      730001000D000A004C0062006C005200690073006B00700072006F0066006900
      6C0065000100520069007300690063006F00700072006F006600690065006C00
      0100500072006F00660069006C00650020006400650020007200690073007100
      7500650001005200690073006B002000700072006F00660069006C0065000100
      0D000A004C0062006C0050006500720073006F006E0061006C00500072006500
      66006500720065006E00630065007300010050006500720073006F006F006E00
      6C0069006A006B006500200076006F006F0072006B0065007500720065006E00
      010050007200E9006600E900720065006E006300650073002000700065007200
      73006F006E006E0065006C006C0065007300010050006500720073006F006E00
      61006C00200070007200650066006500720065006E0063006500730001000D00
      0A005400610062005300680065006500740050006C0061006E00010046006900
      6E0061006E0063006900650065006C00200070006C0061006E00010050006C00
      61006E002000660069006E0061006E0063006900650072000100460069006E00
      61006E006300690061006C00200070006C0061006E0001000D000A004C006200
      6C00500075007200630068006100730065000100410061006E006B006F006F00
      7000010041006300680061007400010050007500720063006800610073006500
      01000D000A004C0062006C004E00650077004200750069006C00640069006E00
      670001004E00690065007500770062006F007500770001004E006F0075007600
      65006C006C006500200063006F006E0073007400720075006300740069006F00
      6E0001004E0065007700200063006F006E007300740072007500630074006900
      6F006E0001000D000A004C0062006C0052006500660069006E0061006E006300
      65000100480065007200660069006E0061006E00630069006500720069006E00
      6700010052006500660069006E0061006E00630065006D0065006E0074000100
      52006500660069006E0061006E00630069006E00670001000D000A004C006200
      6C00440069007600650072007300650043006F00730074007300010044006900
      7600650072007300650020006B006F007300740065006E000100460072006100
      69007300200064006900760065007200730001004D0069007300630065006C00
      6C0061006E0065006F0075007300200063006F0073007400730001000D000A00
      4C0062006C0043006F007300740073000100570061006100720062006F007200
      67006B006F007300740065006E00010046007200610069007300200064006500
      200067006100720061006E007400690065000100470075006100720061006E00
      7400650065002000660065006500730001000D000A004C0062006C0046006900
      6E0061006E00630069006E0067000100460069006E0061006E00630069006500
      720069006E0067000100460069006E0061006E00630065006D0065006E007400
      0100460069006E0061006E00630069006E00670001000D000A004C0062006C00
      520065006E006F0076006100740069006F006E00010056006500720062006F00
      7500770069006E00670001005200E9006E006F0076006100740069006F006E00
      010048006F006D006500200069006D00700072006F00760065006D0065006E00
      740001000D000A00420074006E00530061007600650001004F00700073006C00
      610061006E000100530061007500760065006700610072006400650072000100
      530061007600650001000D000A00420074006E00430061006E00630065006C00
      010041006E006E0075006C006500720065006E00010041006E006E0075006C00
      650072000100430061006E00630065006C0001000D000A007300740048006900
      6E00740073005F0055006E00690063006F00640065000D000A0049006D006100
      670065004C0069006E006B0001004C0069006E006B0020006E00610061007200
      20006E006F00740061007200690073002E006200650001004C00690065006E00
      2000760065007200730020006E006F00740061007200690073002E0062006500
      01004C0069006E006B00200074006F0020006E006F0074006100720069007300
      2E006200650001000D000A007300740044006900730070006C00610079004C00
      6100620065006C0073005F0055006E00690063006F00640065000D000A007300
      740046006F006E00740073005F0055006E00690063006F00640065000D000A00
      730074004D0075006C00740069004C0069006E00650073005F0055006E006900
      63006F00640065000D000A00550073006500720073002E00530051004C004400
      65006C0065007400650001002200440045004C00450054004500200046005200
      4F004D00200043004F0055004E005400520049004500530022002C0057004800
      4500520045002C0022002000200043004F004400450020003D0020003A004F00
      6C0064005F0043004F004400450022000100010001000D000A00550073006500
      720073002E00530051004C0049006E0073006500720074000100220049004E00
      5300450052005400200049004E0054004F00200043004F0055004E0054005200
      49004500530022002C00220020002000280043004F00440045002C0020004400
      55005400430048002C0020004600520045004E00430048002C00200045004E00
      47004C00490053004800290022002C00560041004C005500450053002C002200
      2000200028003A0043004F00440045002C0020003A0044005500540043004800
      2C0020003A004600520045004E00430048002C0020003A0045004E0047004C00
      490053004800290022002C002200520045005400550052004E0049004E004700
      200022002C0022002000200043004F00440045002C0020004400550054004300
      48002C0020004600520045004E00430048002C00200045004E0047004C004900
      5300480022000100010001000D000A00550073006500720073002E0053005100
      4C004C006F0063006B0001002200530045004C0045004300540020004E005500
      4C004C002000460052004F004D00200043004F0055004E005400520049004500
      530022002C00570048004500520045002C00220043004F004400450020003D00
      20003A004F006C0064005F0043004F004400450022002C00220046004F005200
      20005500500044004100540045002000570049005400480020004C004F004300
      4B0022000100010001000D000A00550073006500720073002E00530051004C00
      52006500630043006F0075006E00740001002200530045004C00450043005400
      200043004F0055004E00540028002A0029002000460052004F004D0020002800
      22002C002200530045004C004500430054002000310020004100530020004300
      20002000460052004F004D00200043004F0055004E0054005200490045005300
      22002C002C00220029002000710022000100010001000D000A00550073006500
      720073002E00530051004C005200650066007200650073006800010022005300
      45004C00450043005400200043004F00440045002C0020004400550054004300
      48002C0020004600520045004E00430048002C00200045004E0047004C004900
      530048002000460052004F004D00200043004F0055004E005400520049004500
      530022002C00570048004500520045002C0022002000200043004F0044004500
      20003D0020003A0043004F004400450022000100010001000D000A0055007300
      6500720073002E00530051004C00550070006400610074006500010022005500
      50004400410054004500200043004F0055004E00540052004900450053002200
      2C005300450054002C0022002000200043004F004400450020003D0020003A00
      43004F00440045002C0020004400550054004300480020003D0020003A004400
      55005400430048002C0020004600520045004E004300480020003D0020003A00
      4600520045004E00430048002C00200045004E0047004C004900530048002000
      3D0020003A0045004E0047004C0049005300480022002C005700480045005200
      45002C0022002000200043004F004400450020003D0020003A004F006C006400
      5F0043004F004400450022000100010001000D000A0043006F006E0074007200
      69006200750074006F00720073002E00530051004C00440065006C0065007400
      650001002200440045004C004500540045002000460052004F004D0020004300
      4F0055004E005400520049004500530022002C00570048004500520045002C00
      22002000200043004F004400450020003D0020003A004F006C0064005F004300
      4F004400450022000100010001000D000A0043006F006E007400720069006200
      750074006F00720073002E00530051004C0049006E0073006500720074000100
      220049004E005300450052005400200049004E0054004F00200043004F005500
      4E005400520049004500530022002C00220020002000280043004F0044004500
      2C002000440055005400430048002C0020004600520045004E00430048002C00
      200045004E0047004C00490053004800290022002C00560041004C0055004500
      53002C0022002000200028003A0043004F00440045002C0020003A0044005500
      5400430048002C0020003A004600520045004E00430048002C0020003A004500
      4E0047004C00490053004800290022002C002200520045005400550052004E00
      49004E004700200022002C0022002000200043004F00440045002C0020004400
      55005400430048002C0020004600520045004E00430048002C00200045004E00
      47004C0049005300480022000100010001000D000A0043006F006E0074007200
      69006200750074006F00720073002E00530051004C004C006F0063006B000100
      2200530045004C0045004300540020004E0055004C004C002000460052004F00
      4D00200043004F0055004E005400520049004500530022002C00570048004500
      520045002C00220043004F004400450020003D0020003A004F006C0064005F00
      43004F004400450022002C00220046004F005200200055005000440041005400
      45002000570049005400480020004C004F0043004B0022000100010001000D00
      0A0043006F006E007400720069006200750074006F00720073002E0053005100
      4C0052006500630043006F0075006E00740001002200530045004C0045004300
      5400200043004F0055004E00540028002A0029002000460052004F004D002000
      280022002C002200530045004C00450043005400200031002000410053002000
      430020002000460052004F004D00200043004F0055004E005400520049004500
      530022002C002C00220029002000710022000100010001000D000A0043006F00
      6E007400720069006200750074006F00720073002E00530051004C0052006500
      6600720065007300680001002200530045004C00450043005400200043004F00
      440045002C002000440055005400430048002C0020004600520045004E004300
      48002C00200045004E0047004C004900530048002000460052004F004D002000
      43004F0055004E005400520049004500530022002C0057004800450052004500
      2C0022002000200043004F004400450020003D0020003A0043004F0044004500
      22000100010001000D000A0043006F006E007400720069006200750074006F00
      720073002E00530051004C005500700064006100740065000100220055005000
      4400410054004500200043004F0055004E005400520049004500530022002C00
      5300450054002C0022002000200043004F004400450020003D0020003A004300
      4F00440045002C0020004400550054004300480020003D0020003A0044005500
      5400430048002C0020004600520045004E004300480020003D0020003A004600
      520045004E00430048002C00200045004E0047004C0049005300480020003D00
      20003A0045004E0047004C0049005300480022002C0057004800450052004500
      2C0022002000200043004F004400450020003D0020003A004F006C0064005F00
      43004F004400450022000100010001000D000A00530074006100740075007300
      2E00530051004C00440065006C0065007400650001002200440045004C004500
      540045002000460052004F004D00200043004F0055004E005400520049004500
      530022002C00570048004500520045002C0022002000200043004F0044004500
      20003D0020003A004F006C0064005F0043004F00440045002200010001000100
      0D000A005300740061007400750073002E00530051004C0049006E0073006500
      720074000100220049004E005300450052005400200049004E0054004F002000
      43004F0055004E005400520049004500530022002C0022002000200028004300
      4F00440045002C002000440055005400430048002C0020004600520045004E00
      430048002C00200045004E0047004C00490053004800290022002C0056004100
      4C005500450053002C0022002000200028003A0043004F00440045002C002000
      3A00440055005400430048002C0020003A004600520045004E00430048002C00
      20003A0045004E0047004C00490053004800290022002C002200520045005400
      550052004E0049004E004700200022002C0022002000200043004F0044004500
      2C002000440055005400430048002C0020004600520045004E00430048002C00
      200045004E0047004C0049005300480022000100010001000D000A0053007400
      61007400750073002E00530051004C004C006F0063006B000100220053004500
      4C0045004300540020004E0055004C004C002000460052004F004D0020004300
      4F0055004E005400520049004500530022002C00570048004500520045002C00
      220043004F004400450020003D0020003A004F006C0064005F0043004F004400
      450022002C00220046004F005200200055005000440041005400450020005700
      49005400480020004C004F0043004B0022000100010001000D000A0053007400
      61007400750073002E00530051004C0052006500630043006F0075006E007400
      01002200530045004C00450043005400200043004F0055004E00540028002A00
      29002000460052004F004D002000280022002C002200530045004C0045004300
      5400200031002000410053002000430020002000460052004F004D0020004300
      4F0055004E005400520049004500530022002C002C0022002900200071002200
      0100010001000D000A005300740061007400750073002E00530051004C005200
      65006600720065007300680001002200530045004C0045004300540020004300
      4F00440045002C002000440055005400430048002C0020004600520045004E00
      430048002C00200045004E0047004C004900530048002000460052004F004D00
      200043004F0055004E005400520049004500530022002C005700480045005200
      45002C0022002000200043004F004400450020003D0020003A0043004F004400
      450022000100010001000D000A005300740061007400750073002E0053005100
      4C00550070006400610074006500010022005500500044004100540045002000
      43004F0055004E005400520049004500530022002C005300450054002C002200
      2000200043004F004400450020003D0020003A0043004F00440045002C002000
      4400550054004300480020003D0020003A00440055005400430048002C002000
      4600520045004E004300480020003D0020003A004600520045004E0043004800
      2C00200045004E0047004C0049005300480020003D0020003A0045004E004700
      4C0049005300480022002C00570048004500520045002C002200200020004300
      4F004400450020003D0020003A004F006C0064005F0043004F00440045002200
      0100010001000D000A00500072006F0070006500720074007900540079007000
      650073002E00530051004C00440065006C006500740065000100220044004500
      4C004500540045002000460052004F004D00200043004F0055004E0054005200
      49004500530022002C00570048004500520045002C0022002000200043004F00
      4400450020003D0020003A004F006C0064005F0043004F004400450022000100
      010001000D000A00500072006F00700065007200740079005400790070006500
      73002E00530051004C0049006E0073006500720074000100220049004E005300
      450052005400200049004E0054004F00200043004F0055004E00540052004900
      4500530022002C00220020002000280043004F00440045002C00200044005500
      5400430048002C0020004600520045004E00430048002C00200045004E004700
      4C00490053004800290022002C00560041004C005500450053002C0022002000
      200028003A0043004F00440045002C0020003A00440055005400430048002C00
      20003A004600520045004E00430048002C0020003A0045004E0047004C004900
      53004800290022002C002200520045005400550052004E0049004E0047002000
      22002C0022002000200043004F00440045002C00200044005500540043004800
      2C0020004600520045004E00430048002C00200045004E0047004C0049005300
      480022000100010001000D000A00500072006F00700065007200740079005400
      79007000650073002E00530051004C004C006F0063006B000100220053004500
      4C0045004300540020004E0055004C004C002000460052004F004D0020004300
      4F0055004E005400520049004500530022002C00570048004500520045002C00
      220043004F004400450020003D0020003A004F006C0064005F0043004F004400
      450022002C00220046004F005200200055005000440041005400450020005700
      49005400480020004C004F0043004B0022000100010001000D000A0050007200
      6F0070006500720074007900540079007000650073002E00530051004C005200
      6500630043006F0075006E00740001002200530045004C004500430054002000
      43004F0055004E00540028002A0029002000460052004F004D00200028002200
      2C002200530045004C0045004300540020003100200041005300200043002000
      2000460052004F004D00200043004F0055004E00540052004900450053002200
      2C002C00220029002000710022000100010001000D000A00500072006F007000
      6500720074007900540079007000650073002E00530051004C00520065006600
      720065007300680001002200530045004C00450043005400200043004F004400
      45002C002000440055005400430048002C0020004600520045004E0043004800
      2C00200045004E0047004C004900530048002000460052004F004D0020004300
      4F0055004E005400520049004500530022002C00570048004500520045002C00
      22002000200043004F004400450020003D0020003A0043004F00440045002200
      0100010001000D000A00500072006F0070006500720074007900540079007000
      650073002E00530051004C005500700064006100740065000100220055005000
      4400410054004500200043004F0055004E005400520049004500530022002C00
      5300450054002C0022002000200043004F004400450020003D0020003A004300
      4F00440045002C0020004400550054004300480020003D0020003A0044005500
      5400430048002C0020004600520045004E004300480020003D0020003A004600
      520045004E00430048002C00200045004E0047004C0049005300480020003D00
      20003A0045004E0047004C0049005300480022002C0057004800450052004500
      2C0022002000200043004F004400450020003D0020003A004F006C0064005F00
      43004F004400450022000100010001000D000A00730074005300740072006900
      6E00670073005F0055006E00690063006F00640065000D000A00730074007200
      7300740072005F007200650063006F00720064005F006C006F0063006B006500
      640001004700650067006500760065006E00730020006B0075006E006E006500
      6E0020006E006900650074002000610061006E00670065007000610073007400
      200077006F007200640065006E002C00200069006E0020006700650062007200
      750069006B00200064006F006F0072002000650065006E00200061006E006400
      65007200650020006700650062007200750069006B00650072002E0001004C00
      65007300200064006F006E006E00E9006500730020006E006500200070006500
      7500760065006E00740020007000610073002000EA0074007200650020006D00
      6F006400690066006900E9006500730020006C006F0072007300710075002700
      65006C006C0065007300200073006F006E00740020007500740069006C006900
      7300E900650073002000700061007200200075006E0020006100750074007200
      650020007500740069006C0069007300610074006500750072002E0001004400
      6100740061002000630061006E006E006F00740020006200650020006D006F00
      64006900660069006500640020007700680065006E0020007500730065006400
      200062007900200061006E006F00740068006500720020007500730065007200
      2E0001000D000A007300740072007300740072005F0073007400610074007500
      73005F0072006500710075006900720065006400010044006500200073007400
      61007400750073002000690073002000650065006E0020007600650072006500
      6900730074006500200069006E0067006100760065002E0001004C0065002000
      7300740061007400750074002000650073007400200075006E00650020006500
      6E0074007200E900650020006F0062006C0069006700610074006F0069007200
      65002E0001005400680065002000730074006100740075007300200069007300
      20006100200072006500710075006900720065006400200065006E0074007200
      79002E0001000D000A007300740072007300740072005F006500720072006F00
      72005F0073006100760069006E0067005F006400610074006100010046006F00
      750074002000620069006A00200068006500740020006F00700073006C006100
      61006E002000760061006E002000640065002000670065006700650076006500
      6E0073002100010045007200720065007500720020006C006F00720073002000
      6400650020006C00270065006E00720065006700690073007400720065006D00
      65006E0074002000640065007300200064006F006E006E00E900650073002100
      01004500720072006F007200200073006100760069006E006700200074006800
      650020006400610074006100210001000D000A00730074007200730074007200
      5F006100640064005F00630072006500640069007400010054006F0065007600
      6F006500670065006E0020006B00720065006400690065007400010041006A00
      6F007500740065007200200075006E00200063007200E9006400690074000100
      410064006400200063007200650064006900740001000D000A00730074007200
      7300740072005F006D006F0064006900660079005F0063007200650064006900
      74000100570069006A007A006900670065006E0020006B007200650064006900
      6500740001004D006F00640069006600690065007200200075006E0020006300
      7200E90064006900740001004300680061006E00670065002000630072006500
      64006900740001000D000A00730074004F007400680065007200530074007200
      69006E00670073005F0055006E00690063006F00640065000D000A0054004300
      72006500640069007400460069006C0065007300410064006400460072006D00
      2E004C00610079006F007500740001006100620073006F006C00750074006500
      01006100620073006F006C0075007400650001006100620073006F006C007500
      7400650001000D000A0054006100620053006800650065007400530074006100
      7400750073002E004C00610079006F007500740001006100620073006F006C00
      75007400650001006100620073006F006C007500740065000100610062007300
      6F006C0075007400650001000D000A0045006400690074005F0043006F006E00
      7400720069006200750074006F0072002E004600690065006C0064004C006100
      620065006C000100410061006E006200720065006E0067006500720001004100
      700070006C00690063006100740065007500720001004100700070006C006900
      6300610074006F00720001000D000A0045006400690074005F0043006F006E00
      7400720069006200750074006F0072002E004600690065006C0064004C006100
      620065006C0053006500700061007200610074006F00720001003A0001003A00
      01003A0001000D000A0045006400690074005300740061007400750073002E00
      4600690065006C0064004C006100620065006C00010053007400610074007500
      730020002A00010053007400610074007500740020002A000100530074006100
      74007500730020002A0001000D000A0045006400690074005300740061007400
      750073002E004600690065006C0064004C006100620065006C00530065007000
      61007200610074006F00720001003A0001003A0001003A0001000D000A005400
      6100620053006800650065007400500072006F00660069006C0065002E004C00
      610079006F007500740001006100620073006F006C0075007400650001006100
      620073006F006C0075007400650001006100620073006F006C00750074006500
      01000D000A0045006400690074004E006F005200690073006B0073002E004600
      690065006C0064004C006100620065006C0001004700650065006E0020007200
      69007300690063006F00010041007500630075006E0020007200690073007100
      7500650001004E006F0020007200690073006B0001000D000A00450064006900
      74004E006F005200690073006B0073002E004600690065006C0064004C006100
      620065006C0053006500700061007200610074006F00720001003A0001003A00
      01003A0001000D000A0045006400690074004E006F005200690073006B007300
      2E00560061006C007500650043006800650063006B0065006400010031000100
      31000100310001000D000A0045006400690074004E006F005200690073006B00
      73002E00560061006C007500650055006E0063006800650063006B0065006400
      01003000010030000100300001000D000A0045006400690074004C0069006D00
      69007400650064005200690073006B0073002E004600690065006C0064004C00
      6100620065006C000100420065007000650072006B0074002000720069007300
      690063006F00010052006900730071007500650020006C0069006D0069007400
      E90001004C0069006D00690074006500640020007200690073006B0001000D00
      0A0045006400690074004C0069006D0069007400650064005200690073006B00
      73002E004600690065006C0064004C006100620065006C005300650070006100
      7200610074006F00720001003A0001003A0001003A0001000D000A0045006400
      690074004C0069006D0069007400650064005200690073006B0073002E005600
      61006C007500650043006800650063006B006500640001003100010031000100
      310001000D000A0045006400690074004C0069006D0069007400650064005200
      690073006B0073002E00560061006C007500650055006E006300680065006300
      6B006500640001003000010030000100300001000D000A004500640069007400
      48006900670068005200690073006B0073002E004600690065006C0064004C00
      6100620065006C00010048006F006F0067002000720069007300690063006F00
      01005200690073007100750065002000E9006C0065007600E900010048006900
      6700680020007200690073006B0001000D000A00450064006900740048006900
      670068005200690073006B0073002E004600690065006C0064004C0061006200
      65006C0053006500700061007200610074006F00720001003A0001003A000100
      3A0001000D000A00450064006900740048006900670068005200690073006B00
      73002E00560061006C007500650043006800650063006B006500640001003100
      010031000100310001000D000A00450064006900740048006900670068005200
      690073006B0073002E00560061006C007500650055006E006300680065006300
      6B006500640001003000010030000100300001000D000A004500640069007400
      4D006100780069006D0075006D004D006F006E00740068006C00790043006800
      61007200670065002E004600690065006C0064004C006100620065006C000100
      4D006100780069006D0061006C00650020006D00610061006E0064006C006100
      73007400010043006800610072006700650020006D0065006E00730075006500
      6C006C00650020006D006100780069006D0061006C00650001004D0061007800
      69006D0075006D0020006D006F006E00740068006C0079002000630068006100
      72006700650001000D000A0045006400690074004D006100780069006D007500
      6D004D006F006E00740068006C0079004300680061007200670065002E004600
      690065006C0064004C006100620065006C005300650070006100720061007400
      6F00720001003A0001003A0001003A0001000D000A0045006400690074004600
      720065006500430068006F00690063006500420061006E006B00410063006300
      6F0075006E0074002E004600690065006C0064004C006100620065006C000100
      5600720069006A00650020006B00650075007A00650020007A00690063006800
      7400720065006B0065006E0069006E00670001004C0069006200720065002000
      630068006F0069007800200063006F006D007000740065002000620061006E00
      63006100690072006500010046007200650065002000630068006F0069006300
      65002000620061006E006B0020006100630063006F0075006E00740001000D00
      0A0045006400690074004600720065006500430068006F006900630065004200
      61006E006B004100630063006F0075006E0074002E004600690065006C006400
      4C006100620065006C0053006500700061007200610074006F00720001003A00
      01003A0001003A0001000D000A00450064006900740046007200650065004300
      68006F00690063006500420061006E006B004100630063006F0075006E007400
      2E00560061006C007500650043006800650063006B0065006400010031000100
      31000100310001000D000A004500640069007400460072006500650043006800
      6F00690063006500420061006E006B004100630063006F0075006E0074002E00
      560061006C007500650055006E0063006800650063006B006500640001003000
      010030000100300001000D000A00450064006900740046007200650065004300
      68006F00690063006500440065006200740049006E0073007500720061006E00
      630065002E004600690065006C0064004C006100620065006C00010056007200
      69006A00650020006B00650075007A006500200073006300680075006C006400
      730061006C0064006F0001004C0069006200720065002000630068006F006900
      7800200064007500200073006F006C006400650020006400650020006C006100
      200064006500740074006500010046007200650065002000630068006F006900
      6300650020006F006600200064006500620074002000620061006C0061006E00
      6300650001000D000A0045006400690074004600720065006500430068006F00
      690063006500440065006200740049006E0073007500720061006E0063006500
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00450064006900
      74004600720065006500430068006F0069006300650044006500620074004900
      6E0073007500720061006E00630065002E00560061006C007500650043006800
      650063006B006500640001003100010031000100310001000D000A0045006400
      690074004600720065006500430068006F006900630065004400650062007400
      49006E0073007500720061006E00630065002E00560061006C00750065005500
      6E0063006800650063006B006500640001003000010030000100300001000D00
      0A00450064006900740043006F00760065007200610067006500440065006200
      740049006E0073007500720061006E00630065002E004600690065006C006400
      4C006100620065006C000100440065006B006B0069006E006700730067007200
      61006100640001005400610075007800200064006500200063006F0075007600
      650072007400750072006500010043006F007600650072006100670065002000
      720061007400650001000D000A00450064006900740043006F00760065007200
      610067006500440065006200740049006E0073007500720061006E0063006500
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00450064006900
      74004600720065006500430068006F0069006300650046006900720065004900
      6E0073007500720061006E00630065002E004600690065006C0064004C006100
      620065006C0001005600720069006A00650020006B00650075007A0065002000
      6200720061006E0064007600650072007A002E0001004C006900620072006500
      2000630068006F006900780020006100730073007500720061006E0063006500
      200069006E00630065006E006400690065000100460072006500650020006300
      68006F0069006300650020006F00660020006600690072006500200069006E00
      73007500720061006E006300650001000D000A00450064006900740046007200
      65006500430068006F00690063006500460069007200650049006E0073007500
      720061006E00630065002E004600690065006C0064004C006100620065006C00
      53006500700061007200610074006F00720001003A0001003A0001003A000100
      0D000A0045006400690074004600720065006500430068006F00690063006500
      460069007200650049006E0073007500720061006E00630065002E0056006100
      6C007500650043006800650063006B0065006400010031000100310001003100
      01000D000A0045006400690074004600720065006500430068006F0069006300
      6500460069007200650049006E0073007500720061006E00630065002E005600
      61006C007500650055006E0063006800650063006B0065006400010030000100
      30000100300001000D000A00450064006900740043006F006D006D0065006E00
      7400730043006F006E007400720069006200750074006F0072002E0046006900
      65006C0064004C006100620065006C00010054006F0065006C00690063006800
      740069006E0067002000610061006E006200720065006E006700650072000100
      4500780070006C0069006300610074006900660073002000640065006D006100
      6E00640065007500720001004500780070006C0061006E006100740069006F00
      6E0020006100700070006C006900630061006E00740001000D000A0045006400
      6900740043006F006D006D0065006E007400730043006F006E00740072006900
      6200750074006F0072002E004600690065006C0064004C006100620065006C00
      53006500700061007200610074006F00720001003A0001003A0001003A000100
      0D000A00450064006900740043006F006D006D0065006E007400730041006400
      6D0069006E002E004600690065006C0064004C006100620065006C0001005400
      6F0065006C00690063006800740069006E006700200064006F00730073006900
      6500720062006500680065006500720064006500720001004500780070006C00
      69006300610074006900660073002000670065007300740069006F006E006E00
      6100690072006500200064006500200066006900630068006900650072007300
      01004500780070006C0061006E006100740069006F006E002000660069006C00
      650020006D0061006E00610067006500720001000D000A004500640069007400
      43006F006D006D0065006E0074007300410064006D0069006E002E0046006900
      65006C0064004C006100620065006C0053006500700061007200610074006F00
      720001003A0001003A0001003A0001000D000A0045006400690074004D006100
      780069006D0075006D004400750072006100740069006F006E002E0046006900
      65006C0064004C006100620065006C0001004D006100780069006D0061006C00
      650020006C006F006F007000740069006A0064000100440075007200E9006500
      20006D006100780069006D0061006C00650001004D006100780069006D007500
      6D0020006400750072006100740069006F006E0001000D000A00450064006900
      74004D006100780069006D0075006D004400750072006100740069006F006E00
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00540061006200
      5300680065006500740050006C0061006E002E004C00610079006F0075007400
      01006100620073006F006C0075007400650001006100620073006F006C007500
      7400650001006100620073006F006C0075007400650001000D000A0045006400
      6900740050007500720063006800610073006500500072006900630065005000
      72006F00700065007200740079002E004600690065006C0064004C0061006200
      65006C000100410061006E006B006F006F0070007000720069006A0073000100
      5000720069007800200064002700610063006800610074000100500075007200
      6300680061007300650020007000720069006300650001000D000A0045006400
      6900740050007500720063006800610073006500500072006900630065005000
      72006F00700065007200740079002E004600690065006C0064004C0061006200
      65006C0053006500700061007200610074006F00720001003A0001003A000100
      3A0001000D000A00450064006900740050007500720063006800610073006500
      43006F0073007400500072006F00700065007200740079002E00460069006500
      6C0064004C006100620065006C0001004B006F007300740065006E0001004600
      7200610069007300010043006F0073007400730001000D000A00450064006900
      74005000750072006300680061007300650043006F0073007400500072006F00
      700065007200740079002E004600690065006C0064004C006100620065006C00
      53006500700061007200610074006F00720001003A0001003A0001003A000100
      0D000A004500640069007400500072006F007000650072007400790054007900
      700065002E004600690065006C0064004C006100620065006C00010054007900
      700065002000700061006E006400010054007900700065002000640065002000
      700072006F00700072006900E9007400E9000100540079007000650020006F00
      66002000700072006F007000650072007400790001000D000A00450064006900
      7400500072006F007000650072007400790054007900700065002E0046006900
      65006C0064004C006100620065006C0053006500700061007200610074006F00
      720001003A0001003A0001003A0001000D000A00450064006900740050007200
      6F007000650072007400790041006400640072006500730073002E0046006900
      65006C0064004C006100620065006C0001004100640072006500730001004100
      6400720065007300730065000100410064006400720065007300730001000D00
      0A004500640069007400500072006F0070006500720074007900410064006400
      72006500730073002E004600690065006C0064004C006100620065006C005300
      6500700061007200610074006F00720001003A0001003A0001003A0001000D00
      0A00450064006900740050007500720063006800610073006500500072006900
      63006500470072006F0075006E0064004E0065007700500072006F0070002E00
      4600690065006C0064004C006100620065006C000100470072006F006E006400
      010053006F006C000100470072006F0075006E00640001000D000A0045006400
      6900740050007500720063006800610073006500500072006900630065004700
      72006F0075006E0064004E0065007700500072006F0070002E00460069006500
      6C0064004C006100620065006C0053006500700061007200610074006F007200
      01003A0001003A0001003A0001000D000A004500640069007400500075007200
      6300680061007300650043006F00730074004E0065007700500072006F007000
      2E004600690065006C0064004C006100620065006C0001004B006F0073007400
      65006E00010043006F00FB0074007300010043006F0073007400730001000D00
      0A0045006400690074005000750072006300680061007300650043006F007300
      74004E0065007700500072006F0070002E004600690065006C0064004C006100
      620065006C0053006500700061007200610074006F00720001003A0001003A00
      01003A0001000D000A00450064006900740043006F0073007400500072006900
      630065004200750069006C0064004E0065007700500072006F0070002E004600
      690065006C0064004C006100620065006C00010042006F007500770001004300
      6F006E0073007400720075006300740069006F006E00010043006F006E007300
      7400720075006300740069006F006E0001000D000A0045006400690074004300
      6F0073007400500072006900630065004200750069006C0064004E0065007700
      500072006F0070002E004600690065006C0064004C006100620065006C005300
      6500700061007200610074006F00720001003A0001003A0001003A0001000D00
      0A0045006400690074005600610074004200750069006C0064004E0065007700
      500072006F0070002E004600690065006C0064004C006100620065006C000100
      4200540057000100540056004100010056004100540001000D000A0045006400
      690074005600610074004200750069006C0064004E0065007700500072006F00
      70002E004600690065006C0064004C006100620065006C005300650070006100
      7200610074006F00720001003A0001003A0001003A0001000D000A0045006400
      6900740043006F00730074004100720063006800690074006500630074004E00
      65007700500072006F0070002E004600690065006C0064004C00610062006500
      6C00010041007200630068006900740065006300740001004100720063006800
      6900740065006300740065000100410072006300680069007400650063007400
      01000D000A00450064006900740043006F007300740041007200630068006900
      74006500630074004E0065007700500072006F0070002E004600690065006C00
      64004C006100620065006C0053006500700061007200610074006F0072000100
      3A0001003A0001003A0001000D000A0045006400690074005600610074004100
      720063006800690074006500630074004E0065007700500072006F0070002E00
      4600690065006C0064004C006100620065006C00010042005400570001005400
      56004100010056004100540001000D000A004500640069007400560061007400
      4100720063006800690074006500630074004E0065007700500072006F007000
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00450064006900
      740043006F007300740050007200690063006500530074007500640069006500
      73004E0065007700500072006F0070002E004600690065006C0064004C006100
      620065006C00010045005000420020002F002000560043004F00010045005000
      420020002F002000560043004F00010045005000420020002F00200056004300
      4F0001000D000A00450064006900740043006F00730074005000720069006300
      650053007400750064006900650073004E0065007700500072006F0070002E00
      4600690065006C0064004C006100620065006C00530065007000610072006100
      74006F00720001003A0001003A0001003A0001000D000A004500640069007400
      56006100740053007400750064006900650073004E0065007700500072006F00
      70002E004600690065006C0064004C006100620065006C000100420054005700
      0100540056004100010056004100540001000D000A0045006400690074005600
      6100740053007400750064006900650073004E0065007700500072006F007000
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00450064006900
      74004F0074006500720043006F007300740073004E0065007700500072006F00
      70002E004600690065006C0064004C006100620065006C0001004F0076006500
      720069006700650020006B006F007300740065006E0001004100750074007200
      6500730020006600720061006900730001004F00740068006500720020006300
      6F0073007400730001000D000A0045006400690074004F007400650072004300
      6F007300740073004E0065007700500072006F0070002E004600690065006C00
      64004C006100620065006C0053006500700061007200610074006F0072000100
      3A0001003A0001003A0001000D000A004500640069007400530061006C006400
      6F0054006F00540061006B0065004F007600650072004F007400680065007200
      43007200650064006900740073002E004600690065006C0064004C0061006200
      65006C000100530061006C0064006F0020006F00760065007200200074006500
      20006E0065006D0065006E0020006B0072006500640069006500740065006E00
      010053006F006C00640065002000640065007300200063007200E90064006900
      740073002000E0002000720065007000720065006E0064007200650001004200
      61006C0061006E006300650020006F006600200061007000700072006F007000
      720069006100740069006F006E007300200074006F0020006200650020007400
      61006B0065006E0020006F0076006500720001000D000A004500640069007400
      530061006C0064006F0054006F00540061006B0065004F007600650072004F00
      740068006500720043007200650064006900740073002E004600690065006C00
      64004C006100620065006C0053006500700061007200610074006F0072000100
      3A0001003A0001003A0001000D000A004500640069007400480061006E006400
      4C00690067006800740069006E0067002E004600690065006C0064004C006100
      620065006C000100480061006E0064006C00690063006800740069006E006700
      01004D00610069006E006C0065007600E90065000100520065006C0065006100
      7300650020006F006600200063006F00760065006E0061006E00740001000D00
      0A004500640069007400480061006E0064004C00690067006800740069006E00
      67002E004600690065006C0064004C006100620065006C005300650070006100
      7200610074006F00720001003A0001003A0001003A0001000D000A0045006400
      690074005200650069006E0076006500730074006D0065006E00740061006C00
      6C006F00770061006E00630065002E004600690065006C0064004C0061006200
      65006C00010057006500640065007200620065006C0065006700670069006E00
      6700730076006500720067006F006500640069006E006700010043006F006D00
      700065006E0073006100740069006F006E0020007200E90069006E0076006500
      7300740069007300730065006D0065006E007400010043006F006D0070006500
      6E0073006100740069006F006E0020007200650069006E007600650073007400
      6D0065006E00740001000D000A0045006400690074005200650069006E007600
      6500730074006D0065006E00740061006C006C006F00770061006E0063006500
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00450064006900
      740056006100720069006F007500730063006F00730074007300660069006C00
      65002E004600690065006C0064004C006100620065006C00010044006F007300
      73006900650072006B006F007300740065006E00010046007200610069007300
      200064006500200064006F00730073006900650072000100460069006C006500
      200063006F0073007400730001000D000A004500640069007400560061007200
      69006F007500730063006F00730074007300660069006C0065002E0046006900
      65006C0064004C006100620065006C0053006500700061007200610074006F00
      720001003A0001003A0001003A0001000D000A00450064006900740056006100
      720069006F007500730043006F0073007400730045007300740069006D006100
      740069006F006E002E004600690065006C0064004C006100620065006C000100
      53006300680061007400740069006E00670073006B006F007300740065006E00
      010043006F00FB007400730020006400270065007300740069006D0061007400
      69006F006E00010045007300740069006D006100740069006F006E0020006300
      6F0073007400730001000D000A00450064006900740056006100720069006F00
      7500730043006F0073007400730045007300740069006D006100740069006F00
      6E002E004600690065006C0064004C006100620065006C005300650070006100
      7200610074006F00720001003A0001003A0001003A0001000D000A0045006400
      6900740050007500720063006800610073006500530075006D00440065006200
      7400420061006C0061006E00730049006E0073002E004600690065006C006400
      4C006100620065006C0001004B006F006F00700073006F006D00200073006300
      680075006C006400730061006C0064006F007600650072007A0065006B006500
      720069006E00670001004100730073007500720061006E006300650020006400
      7500200073006F006C006400650020006400650020006C006100200064006500
      7400740065000100500075007200630068006100730065002000730075006D00
      200064006500620074002000620061006C0061006E0063006500200069006E00
      73007500720061006E006300650001000D000A00450064006900740050007500
      720063006800610073006500530075006D004400650062007400420061006C00
      61006E00730049006E0073002E004600690065006C0064004C00610062006500
      6C0053006500700061007200610074006F00720001003A0001003A0001003A00
      01000D000A00450064006900740056006100720069006F007500730043006F00
      7300740073004F0074006800650072002E004600690065006C0064004C006100
      620065006C00010041006E006400650072006500010041007500740072006500
      01004F007400680065007200730001000D000A00450064006900740056006100
      720069006F007500730043006F007300740073004F0074006800650072002E00
      4600690065006C0064004C006100620065006C00530065007000610072006100
      74006F00720001003A0001003A0001003A0001000D000A004500640069007400
      4D006F0072007400470061006700650052006500670069007300740072006100
      740069006F006E002E004600690065006C0064004C006100620065006C000100
      4800790070006F0074006800650063006100690072006500200069006E007300
      63006800720069006A00760069006E006700010045006E007200650067006900
      73007400720065006D0065006E00740020006800790070006F0074006800E900
      6300610069007200650001004D006F0072007400670061006700650020007200
      6500670069007300740072006100740069006F006E0001000D000A0045006400
      690074004D006F00720074004700610067006500520065006700690073007400
      72006100740069006F006E002E004600690065006C0064004C00610062006500
      6C0053006500700061007200610074006F00720001003A0001003A0001003A00
      01000D000A0045006400690074004D006F007200740047006100670065004D00
      61006E0064006100740065002E004600690065006C0064004C00610062006500
      6C0001004800790070006F00740068006500630061006900720020006D006100
      6E00640061006100740001004D0061006E006400610074002000680079007000
      6F0074006800E9006300610069007200650001004D006F007200740067006100
      6700650020006D0061006E00640061007400650001000D000A00450064006900
      74004D006F007200740047006100670065004D0061006E006400610074006500
      2E004600690065006C0064004C006100620065006C0053006500700061007200
      610074006F00720001003A0001003A0001003A0001000D000A00450064006900
      74004F0077006E005200650073006F00750072006300650073002E0046006900
      65006C0064004C006100620065006C00010045006900670065006E0020006D00
      69006400640065006C0065006E00010052006500730073006F00750072006300
      650073002000700072006F00700072006500730001004F0077006E0020007200
      650073006F007500720063006500730001000D000A0045006400690074004F00
      77006E005200650073006F00750072006300650073002E004600690065006C00
      64004C006100620065006C0053006500700061007200610074006F0072000100
      3A0001003A0001003A0001000D000A00450064006900740054006F0074006100
      6C0049006E0076006500730074004D0065006E0074002E004600690065006C00
      64004C006100620065006C0001003C0062003E0054006F00740061006C006500
      200069006E0076006500730074006500720069006E0067003C002F0062003E00
      01003C0062003E0049006E00760065007300740069007300730065006D006500
      6E007400200074006F00740061006C003C002F0062003E0001003C0062003E00
      54006F00740061006C00200069006E0076006500730074006D0065006E007400
      3C002F0062003E0001000D000A00450064006900740054006F00740061006C00
      49006E0076006500730074004D0065006E0074002E004600690065006C006400
      4C006100620065006C0053006500700061007200610074006F00720001003A00
      01003A0001003A0001000D000A004500640069007400440065006D0061006E00
      6400650064004300720065006400690074002E004600690065006C0064004C00
      6100620065006C0001003C0062003E0047006500760072006100610067006400
      20006B007200650064006900650074006200650064007200610067003C002F00
      62003E0001003C0062003E004D006F006E00740061006E007400200063007200
      E9006400690074002000640065006D0061006E006400E9003C002F0062003E00
      01003C0062003E00430072006500640069007400200061006D006F0075006E00
      740020007200650071007500650073007400650064003C002F0062003E000100
      0D000A004500640069007400440065006D0061006E0064006500640043007200
      65006400690074002E004600690065006C0064004C006100620065006C005300
      6500700061007200610074006F00720001003A0001003A0001003A0001000D00
      0A0049006D006100670065004C0069006E006B002E00550072006C0001006900
      6D0061006700650073002F006C0069006E006B002D00310036002E0070006E00
      6700010069006D0061006700650073002F006C0069006E006B002D0031003600
      2E0070006E006700010069006D0061006700650073002F006C0069006E006B00
      2D00310036002E0070006E00670001000D000A00450064006900740043006F00
      73007400500072006900630065004200750069006C006400520065006E006F00
      76006100740065002E004600690065006C0064004C006100620065006C000100
      42006F0075007700010043006F006E0073007400720075006300740069006F00
      6E00010043006F006E0073007400720075006300740069006F006E0001000D00
      0A00450064006900740043006F00730074005000720069006300650042007500
      69006C006400520065006E006F0076006100740065002E004600690065006C00
      64004C006100620065006C0053006500700061007200610074006F0072000100
      3A0001003A0001003A0001000D000A0045006400690074005600610074004200
      750069006C006400520065006E006F0076006100740069006F006E002E004600
      690065006C0064004C006100620065006C000100420054005700010054005600
      4100010054005600410001000D000A0045006400690074005600610074004200
      750069006C006400520065006E006F0076006100740069006F006E002E004600
      690065006C0064004C006100620065006C005300650070006100720061007400
      6F00720001003A0001003A0001003A0001000D000A0045006400690074004300
      6F0073007400410072006300680069007400650063007400520065006E006F00
      76006100740069006F006E002E004600690065006C0064004C00610062006500
      6C00010041007200630068006900740065006300740001004100720063006800
      6900740065006300740065000100410072006300680069007400650063007400
      01000D000A00450064006900740043006F007300740041007200630068006900
      7400650063007400520065006E006F0076006100740069006F006E002E004600
      690065006C0064004C006100620065006C005300650070006100720061007400
      6F00720001003A0001003A0001003A0001000D000A0045006400690074005600
      61007400410072006300680069007400650063007400520065006E006F007600
      6100740069006F006E002E004600690065006C0064004C006100620065006C00
      01004200540057000100540056004100010054005600410001000D000A004500
      6400690074005600610074004100720063006800690074006500630074005200
      65006E006F0076006100740069006F006E002E004600690065006C0064004C00
      6100620065006C0053006500700061007200610074006F00720001003A000100
      3A0001003A0001000D000A00450064006900740043006F007300740050007200
      6900630065005300740075006400690065007300520065006E006F0076006100
      740069006F006E002E004600690065006C0064004C006100620065006C000100
      45005000420020002F002000560043004F00010045005000420020002F002000
      560043004F00010045005000420020002F002000560043004F0001000D000A00
      450064006900740043006F007300740050007200690063006500530074007500
      6400690065007300520065006E006F0076006100740069006F006E002E004600
      690065006C0064004C006100620065006C005300650070006100720061007400
      6F00720001003A0001003A0001003A0001000D000A0045006400690074005600
      610074005300740075006400690065007300520065006E006F00760061007400
      69006F006E002E004600690065006C0064004C006100620065006C0001004200
      540057000100540056004100010056004100540001000D000A00450064006900
      74005600610074005300740075006400690065007300520065006E006F007600
      6100740069006F006E002E004600690065006C0064004C006100620065006C00
      53006500700061007200610074006F00720001003A0001003A0001003A000100
      0D000A0045006400690074004F00740068006500720043006F00730074007300
      520065006E006F0076006100740069006F006E002E004600690065006C006400
      4C006100620065006C0001004F00760065007200690067006500010041007500
      74007200650001004F00740068006500720001000D000A004500640069007400
      4F00740068006500720043006F00730074007300520065006E006F0076006100
      740069006F006E002E004600690065006C0064004C006100620065006C005300
      6500700061007200610074006F00720001003A0001003A0001003A0001000D00
      0A007300740043006F006C006C0065006300740069006F006E0073005F005500
      6E00690063006F00640065000D000A0073007400430068006100720053006500
      740073005F0055006E00690063006F00640065000D000A00}
  end
  object FinInstitutions: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, NAME from CREDIT_FINANCIALINSTITUTIONS')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 80
    Top = 456
  end
  object DsFinInstitutions: TDataSource
    DataSet = FinInstitutions
    Left = 80
    Top = 504
  end
  object CreditTypes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 168
    Top = 456
  end
  object DsCreditTypes: TDataSource
    DataSet = CreditTypes
    Left = 168
    Top = 504
  end
  object Demanders: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'SELECT  CRDEMANDER.ID, CRDEMANDER.DEMANDER_TYPE, CRDEMANDER.DEMA' +
        'NDER_ID,'
      'CASE'
      
        '    WHEN CRDEMANDER.DEMANDER_TYPE = 0 THEN COALESCE(SP.LAST_NAME' +
        ', '#39#39') || '#39' '#39' || COALESCE(SP.FIRST_NAME, '#39#39')'
      '    WHEN CRDEMANDER.DEMANDER_TYPE = 1 THEN C.NAME'
      'END AS DEMANDER_NAME'
      ''
      'FROM            credit_file_demanders   CRDEMANDER '
      
        'LEFT OUTER JOIN crm_accounts    C   ON CRDEMANDER.DEMANDER_ID = ' +
        'C.ID'
      
        'LEFT OUTER JOIN crm_contacts    SP  ON CRDEMANDER.DEMANDER_ID = ' +
        'SP.ID'
      'WHERE CRDEMANDER.CREDIT_FILE=:IDCREDITFILE '
      'ORDER BY DEMANDER_NAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 416
    Top = 464
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'IDCREDITFILE'
        ParamType = ptInput
        Value = nil
      end>
  end
  object DsDemanders: TDataSource
    DataSet = Demanders
    Left = 416
    Top = 512
  end
  object PopupMenuDemanderAdd: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 520
    Top = 458
    object BtnAddOrganisation: TUniMenuItem
      Caption = 'Toevoegen organisatie'
      ImageIndex = 20
      OnClick = BtnAddOrganisationClick
    end
    object BtnAddRelation: TUniMenuItem
      Caption = 'Toevoegen relatie'
      ImageIndex = 18
      OnClick = BtnAddRelationClick
    end
  end
end
