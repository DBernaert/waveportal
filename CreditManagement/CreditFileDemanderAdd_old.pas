unit CreditFileDemanderAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniCheckBox, uniDBCheckBox, uniDBEdit, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniButton, UniThemeButton, uniLabel, uniDateTimePicker, uniDBDateTimePicker, uniEdit,
  uniGUIBaseClasses, uniGroupBox, Data.DB, MemDS, DBAccess, IBC;

type
  TCreditFileDemanderAddFrm = class(TUniForm)
    GroupboxProfession: TUniGroupBox;
    Edit_Name_Employer: TUniDBEdit;
    Edit_Company_Number: TUniDBEdit;
    Edit_StartDate_Independent: TUniDBDateTimePicker;
    Edit_Independent_activity: TUniDBEdit;
    LblIndependentDemander1: TUniLabel;
    Edit_Contract_Type: TUniDBLookupComboBox;
    EditInserviceSinceD1: TUniDBDateTimePicker;
    LblEmployedDemander1: TUniLabel;
    Edit_Profession: TUniDBLookupComboBox;
    GroupboxCosts: TUniGroupBox;
    Edit_Rental_Cost: TUniDBFormattedNumberEdit;
    Edit_Rental_Cost_Stops: TUniDBCheckBox;
    Edit_Credit_Openings: TUniDBFormattedNumberEdit;
    Edit_Credit_Purposes: TUniDBLookupComboBox;
    Edit_Alimentation: TUniDBFormattedNumberEdit;
    Edit_Other_Charges: TUniDBFormattedNumberEdit;
    GroupboxIncome: TUniGroupBox;
    LblProfessionalIncomeD1: TUniLabel;
    Edit_net_monthly_income: TUniDBFormattedNumberEdit;
    Edit_foreign_income: TUniDBFormattedNumberEdit;
    Edit_country_of_origine: TUniDBLookupComboBox;
    Edit_holiday_pay: TUniDBFormattedNumberEdit;
    Edit_end_of_year_bonus: TUniDBFormattedNumberEdit;
    Edit_meal_vouchers: TUniDBFormattedNumberEdit;
    Edit_child_money: TUniDBFormattedNumberEdit;
    LabelRentalIncomeD1: TUniLabel;
    Edit_rental_income_current_private: TUniDBFormattedNumberEdit;
    Edit_rental_income_future_private: TUniDBFormattedNumberEdit;
    Edit_rental_income_current_professional: TUniDBFormattedNumberEdit;
    Edit_rental_income_future_professional: TUniDBFormattedNumberEdit;
    LblOtherIncomeD1: TUniLabel;
    Edit_other_income: TUniDBFormattedNumberEdit;
    Edit_other_income_kind: TUniDBEdit;
    Professions: TIBCQuery;
    DsProfessions: TDataSource;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    ContractTypes: TIBCQuery;
    DsContractTypes: TDataSource;
    CreditPurposes: TIBCQuery;
    DsCreditPurposes: TDataSource;
    Countries: TIBCQuery;
    DsCountries: TDataSource;
    Edit_demander_contact: TUniDBLookupComboBox;
    BtnAddContact: TUniThemeButton;
    BtnModifyContact: TUniThemeButton;
    Contacts: TIBCQuery;
    DsContacts: TDataSource;
    CrmAccounts: TIBCQuery;
    DsCrmAccounts: TDataSource;
    Edit_demander_company: TUniDBLookupComboBox;
    BtnAddCompany: TUniThemeButton;
    BtnModifyCompany: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure BtnAddContactClick(Sender: TObject);
    procedure BtnModifyContactClick(Sender: TObject);
    procedure BtnAddCompanyClick(Sender: TObject);
    procedure BtnModifyCompanyClick(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure CallBackInsertUpdateContact(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateAccount(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Initialize_insert_credit_file_demander(CreditFile: longint; RelationType: integer): boolean;
    function Initialize_edit_credit_file_demander(DemanderId: longint): boolean;
  end;

function CreditFileDemanderAddFrm: TCreditFileDemanderAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule, ContactsAdd, AccountsAdd, Main;

function CreditFileDemanderAddFrm: TCreditFileDemanderAddFrm;
begin
  Result := TCreditFileDemanderAddFrm(UniMainModule.GetFormInstance(TCreditFileDemanderAddFrm));
end;

{ TCreditFileDemanderAddFrm }


procedure TCreditFileDemanderAddFrm.BtnAddCompanyClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.Accounts_Edit.Close;
  UniMainModule.Accounts_Edit.SQL.Clear;
  UniMainModule.Accounts_Edit.SQL.Add('Select first 0 * from crm_accounts');
  UniMainModule.Accounts_Edit.Open;
  UniMainModule.Accounts_Edit.Append;
  UniMainModule.Accounts_Edit.FieldByName('CompanyId').AsInteger             := UniMainModule.Company_id;
  UniMainModule.Accounts_Edit.FieldByName('Removed').AsString                := '0';
  UniMainModule.Accounts_Edit.FieldByName('DATE_ENTERED').AsDateTime         := Now;
  UniMainModule.Accounts_Edit.FieldByName('Account_customer').AsInteger      := 1;
  UniMainModule.Accounts_Edit.FieldByName('Account_supplier').AsInteger      := 0;
  UniMainModule.Accounts_Edit.FieldByName('Language_doc').AsInteger          := 0;

  With AccountsAddFrm do begin
    Init_account_creation;
    ShowModal(CallBackInsertUpdateAccount);
  end;
end;

procedure TCreditFileDemanderAddFrm.BtnAddContactClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.Contacts_Edit.Close;
  UniMainModule.Contacts_Edit.SQL.Clear;
  UniMainModule.Contacts_Edit.SQL.Add('Select first 0 * from crm_contacts');
  UniMainModule.Contacts_Edit.Open;
  UniMainModule.Contacts_Edit.Append;
  UniMainModule.Contacts_Edit.FieldByName('CompanyId').AsInteger             := UniMainModule.Company_id;
  UniMainModule.Contacts_Edit.FieldByName('Removed').AsString                := '0';
  UniMainModule.Contacts_Edit.FieldByName('Do_not_call').AsInteger           := 0;
  UniMainModule.Contacts_Edit.FieldByName('Portal_active').AsInteger         := 0;
  UniMainModule.Contacts_Edit.FieldByName('DATE_ENTERED').AsDateTime         := Now;
  UniMainModule.Contacts_Edit.FieldByName('Language_doc').AsInteger          := 0;

  With ContactsAddFrm do begin
    Init_contact_creation;
    ShowModal(CallBackInsertUpdateContact);
  end;
end;

procedure TCreditFileDemanderAddFrm.CallBackInsertUpdateAccount(Sender: TComponent; AResult: Integer);
begin
  if CrmAccounts.Active
  then begin
         CrmAccounts.Refresh;
         if UniMainModule.Result_dbAction <> 0
         then UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_id').AsInteger := UniMainModule.Result_dbAction;
       end;
end;

procedure TCreditFileDemanderAddFrm.CallBackInsertUpdateContact(Sender: TComponent; AResult: Integer);
begin
  if Contacts.Active
  then begin
         Contacts.Refresh;
         if UniMainModule.Result_dbAction <> 0
         then UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_id').AsInteger := UniMainModule.Result_dbAction;
       end;
end;

procedure TCreditFileDemanderAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.CreditFile_Demander_Edit.Cancel;
  Close;
end;

procedure TCreditFileDemanderAddFrm.BtnModifyCompanyClick(Sender: TObject);
begin
  if (UniMainModule.CreditFiles_Edit.Active) and (not UniMainModule.CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').isNull) then begin
    UniMainModule.Result_dbAction := 0;
    UniMainModule.Accounts_Edit.Close;
    UniMainModule.Accounts_Edit.SQL.Clear;
    UniMainModule.Accounts_Edit.SQL.Add('Select * from crm_accounts where id=' +
                                         UniMainModule.CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').AsString);
    UniMainModule.Accounts_Edit.Open;
    Try
      UniMainModule.Accounts_Edit.Edit;
    Except
      UniMainModule.Accounts_Edit.Cancel;
      UniMainModule.Accounts_Edit.Close;
      MainForm.WaveShowErrorToast('Kan gegevens niet wijzigingen, in gebruik door een andere gebruiker');
      Exit;
    End;

    With AccountsAddFrm do begin
        Init_account_modification(UniMainModule.CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').AsInteger);
        ShowModal(CallBackInsertUpdateAccount);
    end;
  end;
end;

procedure TCreditFileDemanderAddFrm.BtnModifyContactClick(Sender: TObject);
begin
  if (UniMainModule.CreditFiles_Edit.Active) and (not UniMainModule.CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').isNull) then begin
    UniMainModule.Result_dbAction := 0;
    UniMainModule.Contacts_Edit.Close;
    UniMainModule.Contacts_Edit.SQL.Clear;
    UniMainModule.Contacts_Edit.SQL.Add('Select * from crm_contacts where id=' +
                                         UniMainModule.CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').AsString);
    UniMainModule.Contacts_Edit.Open;
    Try
      UniMainModule.Contacts_Edit.Edit;
    Except
      UniMainModule.Contacts_Edit.Cancel;
      UniMainModule.Contacts_Edit.Close;
      MainForm.WaveShowErrorToast('Kan contact niet wijzigen, in gebruik door een andere gebruiker');
      Exit;
    End;

    With ContactsAddFrm do begin
        Init_contact_modification(UniMainModule.CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').AsInteger);
        ShowModal(CallBackInsertUpdateContact);
    end;
  end;
end;

procedure TCreditFileDemanderAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  if UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_id').isNull
  then begin
         MainForm.WaveShowWarningToast('De aanvrager is een verplichte ingave.');
         if UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_type').AsInteger = 0
         then Edit_demander_contact.SetFocus;
         Exit;
       end;

  Try
    UniMainModule.CreditFile_Demander_Edit.Post;
    if UniMainModule.UpdTr_CreditFile_Demander_Edit.Active
    then UniMainModule.UpdTr_CreditFile_Demander_Edit.Commit;
    UniMainModule.Result_dbAction := UniMainModule.CreditFile_Demander_Edit.FieldByName('Id').AsInteger;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       if UniMainModule.UpdTr_CreditFile_Demander_Edit.Active
       then UniMainModule.UpdTr_CreditFile_Demander_Edit.Rollback;
       Exit;
     end;
  end;
  Close;
end;

function TCreditFileDemanderAddFrm.Initialize_edit_credit_file_demander(DemanderId: longint): boolean;
begin
  Try
    Caption := 'Wijzigen aanvrager';
    UniMainModule.CreditFile_Demander_Edit.Close;
    UniMainModule.CreditFile_Demander_Edit.SQL.Clear;
    UniMainModule.CreditFile_Demander_Edit.SQL.Add('Select * from credit_file_demanders where Id=' + IntToStr(DemanderId));
    UniMainModule.CreditFile_Demander_Edit.Open;
    UniMainModule.CreditFile_Demander_Edit.Edit;

    if UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_Type').AsInteger = 0
    then begin
           Edit_demander_company.Visible := False;
           BtnAddCompany.Visible := False;
           BtnModifyCompany.Visible := False;
           Edit_demander_contact.Visible := True;
           BtnAddContact.Visible := True;
           BtnModifyContact.Visible := True;
           ActiveControl := Edit_demander_contact;
         end
    else if UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_Type').AsInteger = 1
         then begin
                Edit_demander_contact.Visible := False;
                BtnAddContact.Visible := False;
                BtnModifyContact.Visible := False;
                Edit_demander_company.Visible := True;
                BtnAddCompany.Visible := True;
                BtnModifyCompany.Visible := True;
                ActiveControl := Edit_demander_company;
              end;
    Open_reference_tables;
    Result := True;
  Except
    Result := False;
  End;
end;

function TCreditFileDemanderAddFrm.Initialize_insert_credit_file_demander(CreditFile: longint; RelationType: integer): boolean;
begin
  Try
    Caption := 'Toevoegen aanvrager';
    UniMainModule.CreditFile_Demander_Edit.Close;
    UniMainModule.CreditFile_Demander_Edit.SQL.Clear;
    UniMainModule.CreditFile_Demander_Edit.SQL.Add('Select first 0 * from credit_file_demanders');
    UniMainModule.CreditFile_Demander_Edit.Open;
    UniMainModule.CreditFile_Demander_Edit.Append;
    UniMainModule.CreditFile_Demander_Edit.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
    UniMainModule.CreditFile_Demander_Edit.FieldByName('Credit_File').AsInteger := CreditFile;
    UniMainModule.CreditFile_Demander_Edit.FieldByName('Rental_Cost_Stops').AsInteger := 0;
    UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_type').AsInteger := RelationType;

    if RelationType = 0
    then begin
           Edit_demander_company.Visible := False;
           BtnAddCompany.Visible := False;
           BtnModifyCompany.Visible := False;
           Edit_demander_contact.Visible := True;
           BtnAddContact.Visible := True;
           BtnModifyContact.Visible := True;
           ActiveControl := Edit_demander_contact;
         end
    else if RelationType = 1
         then begin
                Edit_demander_contact.Visible := False;
                BtnAddContact.Visible := False;
                BtnModifyContact.Visible := False;
                Edit_demander_company.Visible := True;
                BtnAddCompany.Visible := True;
                BtnModifyCompany.Visible := True;
                ActiveControl := Edit_demander_company;
              end;
    Open_reference_tables;
    Result := True;
  Except
    Result := False;
  End;
end;

procedure TCreditFileDemanderAddFrm.Open_reference_tables;
var SQL: string;
begin
  if UniMainModule.CreditFile_Demander_Edit.Fieldbyname('Demander_type').asInteger = 0
  then begin
         Contacts.Close;
         Contacts.SQL.Clear;
         SQL := 'Select id, coalesce(last_name, '''') || '' '' || coalesce(first_name, '''') as FullName ' +
         'from crm_contacts where CompanyID=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' order by FullName';
         Contacts.SQL.Add(SQL);
         Contacts.Open;
       end
  else if UniMainModule.CreditFile_Demander_Edit.FieldByName('Demander_type').AsInteger = 1
       then begin
              CrmAccounts.Close;
              CrmAccounts.SQL.Clear;
              SQL := 'Select id, name from crm_accounts where companyid=' + IntToStr(UniMainModule.Company_id) +
                     ' and removed=''0'' order by name';
              CrmAccounts.SQL.Add(SQL);
              CrmAccounts.Open;
            end;

  Professions.Close;
  professions.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2003 ' +
         ' and REMOVED=''0''';

  SQL := SQL + ' order by DUTCH';
  Edit_Profession.ListField := 'DUTCH';

  Professions.SQL.Add(SQL);
  professions.Open;

  ContractTypes.Close;
  ContractTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2004 ' +
         ' and REMOVED=''0''';

  SQL := SQL + ' order by DUTCH';
  Edit_Contract_Type.ListField := 'DUTCH';

  ContractTypes.SQL.Add(SQL);
  ContractTypes.Open;

  CreditPurposes.Close;
  CreditPurposes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2005 ' +
         ' and REMOVED=''0''';

  SQL := SQL + ' order by DUTCH';
  Edit_Credit_Purposes.ListField := 'DUTCH';

  CreditPurposes.SQL.Add(SQL);
  CreditPurposes.Open;

  Countries.Close;
  Countries.SQL.Clear;

  Countries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by dutch');
  Edit_country_of_origine.ListField := 'DUTCH';

  Countries.Open;
end;

procedure TCreditFileDemanderAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
