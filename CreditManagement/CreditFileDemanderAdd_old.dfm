object CreditFileDemanderAddFrm: TCreditFileDemanderAddFrm
  Left = 0
  Top = 0
  ClientHeight = 673
  ClientWidth = 1105
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupboxProfession: TUniGroupBox
    Left = 16
    Top = 48
    Width = 529
    Height = 329
    Hint = ''
    Caption = 'Beroepsgegevens'
    TabOrder = 4
    object Edit_Name_Employer: TUniDBEdit
      Left = 24
      Top = 88
      Width = 489
      Height = 22
      Hint = ''
      DataField = 'NAME_EMPLOYER'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 1
      FieldLabel = 'Naam werkgever'
      FieldLabelWidth = 180
    end
    object Edit_Company_Number: TUniDBEdit
      Left = 24
      Top = 280
      Width = 489
      Height = 22
      Hint = ''
      DataField = 'COMPANY_NUMBER'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 6
      FieldLabel = 'Ondernemingsnummer'
      FieldLabelWidth = 180
    end
    object Edit_StartDate_Independent: TUniDBDateTimePicker
      Left = 24
      Top = 248
      Width = 489
      Hint = ''
      DataField = 'STARTDATE_INDEPENDENCE'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      DateTime = 43479.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 5
      FieldLabel = 'Startdatum'
      FieldLabelWidth = 180
    end
    object Edit_Independent_activity: TUniDBEdit
      Left = 24
      Top = 216
      Width = 489
      Height = 22
      Hint = ''
      DataField = 'INDEPENDENT_ACTIVITY'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 4
      FieldLabel = 'Activiteit'
      FieldLabelWidth = 180
    end
    object LblIndependentDemander1: TUniLabel
      Left = 8
      Top = 192
      Width = 70
      Height = 13
      Hint = ''
      Caption = 'Zelfstandige'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 8
    end
    object Edit_Contract_Type: TUniDBLookupComboBox
      Left = 24
      Top = 152
      Width = 489
      Hint = ''
      ListField = 'DUTCH'
      ListSource = DsContractTypes
      KeyField = 'ID'
      ListFieldIndex = 0
      DataField = 'TYPE_OF_CONTRACT'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 3
      Color = clWindow
      FieldLabel = 'Type contract'
      FieldLabelWidth = 180
      ForceSelection = True
      Style = csDropDown
    end
    object EditInserviceSinceD1: TUniDBDateTimePicker
      Left = 24
      Top = 120
      Width = 489
      Hint = ''
      DataField = 'IN_SERVICE_SINCE'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      DateTime = 43479.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 2
      FieldLabel = 'In dienst sedert'
      FieldLabelWidth = 180
    end
    object LblEmployedDemander1: TUniLabel
      Left = 8
      Top = 64
      Width = 86
      Height = 13
      Hint = ''
      Caption = 'Loontrekkende'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 9
    end
    object Edit_Profession: TUniDBLookupComboBox
      Left = 24
      Top = 24
      Width = 489
      Hint = ''
      ListField = 'DUTCH'
      ListSource = DsProfessions
      KeyField = 'ID'
      ListFieldIndex = 0
      DataField = 'PROFESSION'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 0
      Color = clWindow
      FieldLabel = 'Beroep'
      FieldLabelWidth = 180
      ForceSelection = True
      Style = csDropDown
    end
  end
  object GroupboxCosts: TUniGroupBox
    Left = 16
    Top = 384
    Width = 529
    Height = 225
    Hint = ''
    Caption = 'Lasten'
    TabOrder = 5
    object Edit_Rental_Cost: TUniDBFormattedNumberEdit
      Left = 24
      Top = 24
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'RENTAL_COST'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 1
      FieldLabel = 'Huur'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_Rental_Cost_Stops: TUniDBCheckBox
      Left = 24
      Top = 56
      Width = 489
      Height = 17
      Hint = ''
      DataField = 'RENTAL_COST_STOPS'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      ValueChecked = '1'
      ValueUnchecked = '0'
      Caption = ''
      TabOrder = 2
      ParentColor = False
      Color = clBtnFace
      FieldLabel = 'Valt weg ?'
      FieldLabelWidth = 180
    end
    object Edit_Credit_Openings: TUniDBFormattedNumberEdit
      Left = 24
      Top = 80
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'CREDIT_OPENINGS'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 3
      FieldLabel = 'LOA / kredietopeningen'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_Credit_Purposes: TUniDBLookupComboBox
      Left = 24
      Top = 112
      Width = 489
      Hint = ''
      ListField = 'DUTCH'
      ListSource = DsCreditPurposes
      KeyField = 'ID'
      ListFieldIndex = 0
      DataField = 'CREDIT_PURPOSES'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 4
      Color = clWindow
      FieldLabel = 'Doel'
      FieldLabelWidth = 180
      ForceSelection = True
      Style = csDropDown
    end
    object Edit_Alimentation: TUniDBFormattedNumberEdit
      Left = 24
      Top = 144
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'ALIMENTATION'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 5
      FieldLabel = 'Alimentatie'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_Other_Charges: TUniDBFormattedNumberEdit
      Left = 24
      Top = 176
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'OTHER_CHARGES'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 6
      FieldLabel = 'Andere'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
  end
  object GroupboxIncome: TUniGroupBox
    Left = 560
    Top = 48
    Width = 529
    Height = 561
    Hint = ''
    Caption = 'Inkomsten'
    TabOrder = 6
    object LblProfessionalIncomeD1: TUniLabel
      Left = 8
      Top = 24
      Width = 106
      Height = 13
      Hint = ''
      Caption = 'Beroepsinkomsten'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 1
    end
    object Edit_net_monthly_income: TUniDBFormattedNumberEdit
      Left = 24
      Top = 48
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'NET_MONTHLY_INCOME'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 2
      FieldLabel = 'Netto maandinkomen'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_foreign_income: TUniDBFormattedNumberEdit
      Left = 24
      Top = 80
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'FOREIGN_INCOME'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 3
      FieldLabel = 'Buitenlandse inkomsten'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_country_of_origine: TUniDBLookupComboBox
      Left = 24
      Top = 112
      Width = 489
      Hint = ''
      ListField = 'DUTCH'
      ListSource = DsCountries
      KeyField = 'CODE'
      ListFieldIndex = 0
      DataField = 'COUNTRY_OF_ORIGINE'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 4
      Color = clWindow
      FieldLabel = 'Land van oorsprong'
      FieldLabelWidth = 180
      ForceSelection = True
      Style = csDropDown
    end
    object Edit_holiday_pay: TUniDBFormattedNumberEdit
      Left = 24
      Top = 144
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'HOLIDAY_PAY'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 5
      FieldLabel = 'Vakantiegeld'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_end_of_year_bonus: TUniDBFormattedNumberEdit
      Left = 24
      Top = 176
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'END_OF_YEAR_BONUS'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 6
      FieldLabel = 'Eindejaarspremie'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_meal_vouchers: TUniDBFormattedNumberEdit
      Left = 24
      Top = 208
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'MEAL_VOUCHERS'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 7
      FieldLabel = 'Maaltijdcheques'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_child_money: TUniDBFormattedNumberEdit
      Left = 24
      Top = 240
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'CHILD_MONEY'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 8
      FieldLabel = 'Kindergeld'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object LabelRentalIncomeD1: TUniLabel
      Left = 8
      Top = 280
      Width = 87
      Height = 13
      Hint = ''
      Caption = 'Huurinkomsten'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 9
    end
    object Edit_rental_income_current_private: TUniDBFormattedNumberEdit
      Left = 24
      Top = 304
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'RENT_INCOME_CUR_PRIV'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 10
      FieldLabel = 'Huidige priv'#233
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_rental_income_future_private: TUniDBFormattedNumberEdit
      Left = 24
      Top = 336
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'RENT_INCOME_FUT_PRIV'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 11
      FieldLabel = 'Toekomstige priv'#233
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_rental_income_current_professional: TUniDBFormattedNumberEdit
      Left = 24
      Top = 368
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'RENT_INCOME_CUR_PROF'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 12
      FieldLabel = 'Huidige beroeps'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_rental_income_future_professional: TUniDBFormattedNumberEdit
      Left = 24
      Top = 400
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'RENT_INCOME_FUT_PROF'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 13
      FieldLabel = 'Toekomstige beroeps'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object LblOtherIncomeD1: TUniLabel
      Left = 8
      Top = 440
      Width = 104
      Height = 13
      Hint = ''
      Caption = 'Andere inkomsten'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 14
    end
    object Edit_other_income: TUniDBFormattedNumberEdit
      Left = 24
      Top = 464
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'OTHER_INCOME'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 15
      FieldLabel = 'Bedrag'
      FieldLabelWidth = 180
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object Edit_other_income_kind: TUniDBEdit
      Left = 24
      Top = 496
      Width = 489
      Height = 22
      Hint = ''
      DataField = 'OTHER_INCOME_KIND'
      DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
      TabOrder = 16
      FieldLabel = 'Aard'
      FieldLabelWidth = 180
    end
  end
  object BtnSave: TUniThemeButton
    Left = 824
    Top = 624
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 7
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 31
    OnClick = BtnSaveClick
    ButtonTheme = uctTertiary
  end
  object BtnCancel: TUniThemeButton
    Left = 960
    Top = 624
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 8
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctDefault
  end
  object Edit_demander_contact: TUniDBLookupComboBox
    Left = 16
    Top = 16
    Width = 425
    Hint = ''
    ListField = 'FULLNAME'
    ListSource = DsContacts
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'DEMANDER_ID'
    DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
    TabOrder = 0
    Color = clWindow
    FieldLabel = 'Aanvrager'
    FieldLabelWidth = 150
    Style = csDropDown
  end
  object BtnAddContact: TUniThemeButton
    Left = 448
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Toevoegen aanvrager'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 1
    Images = UniMainModule.ImageList
    ImageIndex = 10
    OnClick = BtnAddContactClick
    ButtonTheme = uctTertiary
  end
  object BtnModifyContact: TUniThemeButton
    Left = 480
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Wijzigen aanvrager'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 2
    Images = UniMainModule.ImageList
    ImageIndex = 11
    OnClick = BtnModifyContactClick
    ButtonTheme = uctTertiary
  end
  object Edit_demander_company: TUniDBLookupComboBox
    Left = 16
    Top = 16
    Width = 425
    Hint = ''
    ListField = 'NAME'
    ListSource = DsCrmAccounts
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'DEMANDER_ID'
    DataSource = UniMainModule.Ds_CreditFile_Demander_Edit
    TabOrder = 3
    Color = clWindow
    FieldLabel = 'Aanvrager'
    FieldLabelWidth = 150
    ForceSelection = True
    Style = csDropDown
  end
  object BtnAddCompany: TUniThemeButton
    Left = 448
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Toevoegen aanvrager'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 9
    Images = UniMainModule.ImageList
    ImageIndex = 10
    OnClick = BtnAddCompanyClick
    ButtonTheme = uctDefault
  end
  object BtnModifyCompany: TUniThemeButton
    Left = 480
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Wijzigen aanvrager'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 10
    Images = UniMainModule.ImageList
    ImageIndex = 11
    OnClick = BtnModifyCompanyClick
    ButtonTheme = uctDefault
  end
  object Professions: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 312
    Top = 288
  end
  object DsProfessions: TDataSource
    DataSet = Professions
    Left = 312
    Top = 336
  end
  object ContractTypes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 456
    Top = 288
  end
  object DsContractTypes: TDataSource
    DataSet = ContractTypes
    Left = 456
    Top = 336
  end
  object CreditPurposes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 592
    Top = 288
  end
  object DsCreditPurposes: TDataSource
    DataSet = CreditPurposes
    Left = 592
    Top = 336
  end
  object Countries: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from countries order by dutch')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 712
    Top = 280
  end
  object DsCountries: TDataSource
    DataSet = Countries
    Left = 712
    Top = 335
  end
  object Contacts: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO CREDIT_FINANCIALINSTITUTIONS'
      
        '  (ID, COMPANYID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAI' +
        'L, WEBSITE, REMOVED, SPREADEDPAYMENT, PERCENTAGEDIRECT, NUMBEROF' +
        'MONTHSSPREAD, PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1, MONTHPLANNED' +
        'PAYMENT2, MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4, MONTHPLANN' +
        'EDPAYMENT5, MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7, MONTHPLA' +
        'NNEDPAYMENT8, MONTHPLANNEDPAYMENT9, MONTHPLANNEDPAYMENT10, PERCE' +
        'NTAGEPAYMENT1, PERCENTAGEPAYMENT2, PERCENTAGEPAYMENT3, PERCENTAG' +
        'EPAYMENT4, PERCENTAGEPAYMENT5, PERCENTAGEPAYMENT6, PERCENTAGEPAY' +
        'MENT7, PERCENTAGEPAYMENT8, PERCENTAGEPAYMENT9, PERCENTAGEPAYMENT' +
        '10, REMARKS)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :NAME, :ADDRESS, :ZIPCODE, :CITY, :PHONE, :F' +
        'AX, :EMAIL, :WEBSITE, :REMOVED, :SPREADEDPAYMENT, :PERCENTAGEDIR' +
        'ECT, :NUMBEROFMONTHSSPREAD, :PLANNEDPAYMENT, :MONTHPLANNEDPAYMEN' +
        'T1, :MONTHPLANNEDPAYMENT2, :MONTHPLANNEDPAYMENT3, :MONTHPLANNEDP' +
        'AYMENT4, :MONTHPLANNEDPAYMENT5, :MONTHPLANNEDPAYMENT6, :MONTHPLA' +
        'NNEDPAYMENT7, :MONTHPLANNEDPAYMENT8, :MONTHPLANNEDPAYMENT9, :MON' +
        'THPLANNEDPAYMENT10, :PERCENTAGEPAYMENT1, :PERCENTAGEPAYMENT2, :P' +
        'ERCENTAGEPAYMENT3, :PERCENTAGEPAYMENT4, :PERCENTAGEPAYMENT5, :PE' +
        'RCENTAGEPAYMENT6, :PERCENTAGEPAYMENT7, :PERCENTAGEPAYMENT8, :PER' +
        'CENTAGEPAYMENT9, :PERCENTAGEPAYMENT10, :REMARKS)'
      'RETURNING '
      
        '  ID, COMPANYID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAIL' +
        ', WEBSITE, REMOVED, SPREADEDPAYMENT, PERCENTAGEDIRECT, NUMBEROFM' +
        'ONTHSSPREAD, PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1, MONTHPLANNEDP' +
        'AYMENT2, MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4, MONTHPLANNE' +
        'DPAYMENT5, MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7, MONTHPLAN' +
        'NEDPAYMENT8, MONTHPLANNEDPAYMENT9, MONTHPLANNEDPAYMENT10, PERCEN' +
        'TAGEPAYMENT1, PERCENTAGEPAYMENT2, PERCENTAGEPAYMENT3, PERCENTAGE' +
        'PAYMENT4, PERCENTAGEPAYMENT5, PERCENTAGEPAYMENT6, PERCENTAGEPAYM' +
        'ENT7, PERCENTAGEPAYMENT8, PERCENTAGEPAYMENT9, PERCENTAGEPAYMENT1' +
        '0')
    SQLDelete.Strings = (
      'DELETE FROM CREDIT_FINANCIALINSTITUTIONS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE CREDIT_FINANCIALINSTITUTIONS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, NAME = :NAME, ADDRESS = :ADD' +
        'RESS, ZIPCODE = :ZIPCODE, CITY = :CITY, PHONE = :PHONE, FAX = :F' +
        'AX, EMAIL = :EMAIL, WEBSITE = :WEBSITE, REMOVED = :REMOVED, SPRE' +
        'ADEDPAYMENT = :SPREADEDPAYMENT, PERCENTAGEDIRECT = :PERCENTAGEDI' +
        'RECT, NUMBEROFMONTHSSPREAD = :NUMBEROFMONTHSSPREAD, PLANNEDPAYME' +
        'NT = :PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1 = :MONTHPLANNEDPAYMEN' +
        'T1, MONTHPLANNEDPAYMENT2 = :MONTHPLANNEDPAYMENT2, MONTHPLANNEDPA' +
        'YMENT3 = :MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4 = :MONTHPLA' +
        'NNEDPAYMENT4, MONTHPLANNEDPAYMENT5 = :MONTHPLANNEDPAYMENT5, MONT' +
        'HPLANNEDPAYMENT6 = :MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7 =' +
        ' :MONTHPLANNEDPAYMENT7, MONTHPLANNEDPAYMENT8 = :MONTHPLANNEDPAYM' +
        'ENT8, MONTHPLANNEDPAYMENT9 = :MONTHPLANNEDPAYMENT9, MONTHPLANNED' +
        'PAYMENT10 = :MONTHPLANNEDPAYMENT10, PERCENTAGEPAYMENT1 = :PERCEN' +
        'TAGEPAYMENT1, PERCENTAGEPAYMENT2 = :PERCENTAGEPAYMENT2, PERCENTA' +
        'GEPAYMENT3 = :PERCENTAGEPAYMENT3, PERCENTAGEPAYMENT4 = :PERCENTA' +
        'GEPAYMENT4, PERCENTAGEPAYMENT5 = :PERCENTAGEPAYMENT5, PERCENTAGE' +
        'PAYMENT6 = :PERCENTAGEPAYMENT6, PERCENTAGEPAYMENT7 = :PERCENTAGE' +
        'PAYMENT7, PERCENTAGEPAYMENT8 = :PERCENTAGEPAYMENT8, PERCENTAGEPA' +
        'YMENT9 = :PERCENTAGEPAYMENT9, PERCENTAGEPAYMENT10 = :PERCENTAGEP' +
        'AYMENT10, REMARKS = :REMARKS'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, ' +
        'EMAIL, WEBSITE, REMOVED, SPREADEDPAYMENT, PERCENTAGEDIRECT, NUMB' +
        'EROFMONTHSSPREAD, PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1, MONTHPLA' +
        'NNEDPAYMENT2, MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4, MONTHP' +
        'LANNEDPAYMENT5, MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7, MONT' +
        'HPLANNEDPAYMENT8, MONTHPLANNEDPAYMENT9, MONTHPLANNEDPAYMENT10, P' +
        'ERCENTAGEPAYMENT1, PERCENTAGEPAYMENT2, PERCENTAGEPAYMENT3, PERCE' +
        'NTAGEPAYMENT4, PERCENTAGEPAYMENT5, PERCENTAGEPAYMENT6, PERCENTAG' +
        'EPAYMENT7, PERCENTAGEPAYMENT8, PERCENTAGEPAYMENT9, PERCENTAGEPAY' +
        'MENT10, REMARKS FROM CREDIT_FINANCIALINSTITUTIONS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM CREDIT_FINANCIALINSTITUTIONS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM CREDIT_FINANCIALINSTITUTIONS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, coalesce(last_name, '#39#39') || '#39' '#39' || coalesce(first_name' +
        ', '#39#39') as FullName'
      'from crm_contacts'
      'order by FullName')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 816
    Top = 280
  end
  object DsContacts: TDataSource
    DataSet = Contacts
    Left = 816
    Top = 336
  end
  object CrmAccounts: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select id, name from crm_accounts')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 952
    Top = 280
  end
  object DsCrmAccounts: TDataSource
    DataSet = CrmAccounts
    Left = 952
    Top = 327
  end
end
