unit ValidateCreditDocuments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniBasicGrid, uniDBGrid,
  uniButton, UniThemeButton, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk,
  VirtualTable, uniEdit, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniPanel;

var
	str_error_moving_documents: string = 'Fout bij het verplaatsen van de documenten'; // TSI: Localized (Don't modify!)
	str_file_not_found:         string = 'Bestand niet gevonden'; // TSI: Localized (Don't modify!)

type
  TValidateCreditDocumentsFrm = class(TUniForm)
    GridTypes: TUniDBGrid;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    BasicBrowse: TIBCQuery;
    DsBasicBrowse: TDataSource;
    LinkedLanguageValidateCreditDocuments: TsiLangLinked;
    GridFiles: TUniDBGrid;
    TblFiles: TVirtualTable;
    DsFiles: TDataSource;
    BtnViewDocument: TUniThemeButton;
    TblFilesId: TLargeintField;
    TblFilesDescription: TStringField;
    TblFilesOriginal_name: TStringField;
    TblFilesFileType: TLargeintField;
    TblFilesFileTypeDescription: TStringField;
    BtnAssignType: TUniThemeButton;
    BasicSelect: TIBCQuery;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageValidateCreditDocumentsChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure BtnViewDocumentClick(Sender: TObject);
    procedure BtnAssignTypeClick(Sender: TObject);
  private
    { Private declarations }
    _DocumentItemId: longint;
    _ModuleId:       integer;
    _MasterId:       longint;
  public
    { Public declarations }
    procedure Init_credit_document_validation(DocumentItemId: longint;
                                              MasterId: longint;
                                              ModuleId: integer;
                                              Description: string);
  end;

function ValidateCreditDocumentsFrm: TValidateCreditDocumentsFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule, Utils, System.IOUtils,
  ContactSelect;

function ValidateCreditDocumentsFrm: TValidateCreditDocumentsFrm;
begin
  Result := TValidateCreditDocumentsFrm(UniMainModule.GetFormInstance(TValidateCreditDocumentsFrm));
end;

{ TValidateCreditDocumentsFrm }

procedure TValidateCreditDocumentsFrm.BtnAssignTypeClick(Sender: TObject);
begin
  if (BasicBrowse.Active = False) or (BasicBrowse.FieldByName('Id').isNull)
  then exit;

  if TblFiles.State <> dsBrowse
  then TblFiles.Post;

  TblFiles.First;

  while not TblFiles.Eof do begin
    TblFiles.Edit;

    TblFiles.FieldByName('FileType').AsInteger :=
    BasicBrowse.FieldByName('Id').AsInteger;

    TblFiles.Post;
    TblFiles.Next;
  end;

  TblFiles.First;
end;

procedure TValidateCreditDocumentsFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TValidateCreditDocumentsFrm.BtnSaveClick(Sender: TObject);
var Valid: boolean;
begin
  BtnSave.SetFocus;

  if TblFiles.State <> dsBrowse
  then TblFiles.Post;

  Valid := True;

  TblFiles.First;

  while not TblFiles.Eof
  do begin
    if TblFiles.FieldByName('FileType').IsNull
    then Valid := False;

    TblFiles.Next;
  end;

  if Valid = False then begin
    MainForm.WaveShowWarningToast('Er zijn documenten waar geen type is aan toegekend');
    Exit;
  end;

  //Create
  Try
    TblFiles.First;

    while not TblFiles.Eof
    do begin
         if _MasterId <> 0
         then begin
                UniMainModule.QWork4.Close;
                UniMainModule.QWork4.SQL.Text := 'Select * from files_archive where id=:id';
                UniMainModule.QWork4.ParamByName('Id').AsInteger := TblFiles.FieldByName('Id').AsInteger;
                UniMainModule.QWork4.Open;

                UniMainModule.QBatch2.Close;
                UniMainModule.QBatch2.SQL.Text := 'insert into files_archive(companyId, Module_id, ' +
                                                  'record_id, original_name, destination_name, removed,' +
                                                  'date_entered, date_modified, created_user_id, ' +
                                                  'modified_user_id, description, file_type, '+
                                                  'visible_portal, language_id, storage_type, folder, subfolder) ' +
                                                  'values(:CompanyId, :Module_id, :Record_id, :Original_name, ' +
                                                  ':Destination_name, :Removed, :Date_entered, :Date_modified, ' +
                                                  ':Created_user_id, :Modified_user_id, :Description, :File_type, ' +
                                                  ':Visible_portal, :Language_id, :Storage_type, :Folder, :SubFolder)';

                UniMainModule.QBatch2.ParamByName('CompanyId').AsInteger         := UniMainModule.QWork4.FieldByName('CompanyId').AsInteger;
                UniMainModule.QBatch2.ParamByName('Module_id').AsInteger         := _ModuleId;
                UniMainModule.QBatch2.ParamByName('Record_id').AsInteger         := _MasterId;
                UniMainModule.QBatch2.ParamByName('Original_name').AsString      := UniMainModule.QWork4.FieldByName('Original_name').AsString;
                UniMainModule.QBatch2.ParamByName('Destination_name').AsString   := UniMainModule.QWork4.FieldByName('Destination_name').AsString;
                UniMainModule.QBatch2.ParamByName('Removed').AsString            := '0';
                UniMainModule.QBatch2.ParamByName('Date_entered').AsDateTime     := Now;
                UniMainModule.QBatch2.ParamByName('Date_modified').AsDateTime    := Now;
                UniMainModule.QBatch2.ParamByName('Created_user_id').AsInteger   := UniMainModule.User_id;
                UniMainModule.QBatch2.ParamByName('Modified_user_id').AsInteger  := UniMainModule.User_id;
                UniMainModule.QBatch2.ParamByName('Description').AsString        := TblFiles.FieldByName('Description').AsString;
                UniMainModule.QBatch2.ParamByName('File_type').AsInteger         := TblFiles.FieldByName('FileType').AsInteger;
                UniMainModule.QBatch2.ParamByName('Visible_portal').AsInteger    := 1;
                UniMainModule.QBatch2.ParamByName('Language_id').AsInteger       := UniMainModule.QWork4.FieldByName('Language_id').AsInteger;
                UniMainModule.QBatch2.ParamByName('Storage_type').AsInteger      := UniMainModule.QWork4.FieldByName('Storage_type').AsInteger;
                UniMainModule.QBatch2.ParamByName('Folder').AsString             := UniMainModule.QWork4.FieldByName('Folder').AsString;
                UniMainModule.QBatch2.ParamByName('SubFolder').AsString          := UniMainModule.QWork4.FieldByName('SubFolder').AsString;
                UniMainModule.QBatch2.ExecSQL;
                UniMainModule.QWork4.Close;
              end;

         UniMainModule.QBatch.Close;
         UniMainModule.QBatch.SQL.Text := 'update credit_doc_items set validated=:validated where id=:id';
         UniMainModule.QBatch.ParamByName('Id').AsInteger := _DocumentItemId;
         UniMainModule.QBatch.ParamByName('Validated').AsInteger := 1;
         UniMainModule.QBatch.ExecSQL;
         TblFiles.Next;
       end;

    if UniMainModule.UpdTr_Batch.Active
    then UniMainModule.UpdTr_Batch.Commit;

    UniMainModule.QBatch.Close;

    Close;
  Except
    if UniMainModule.UpdTr_Batch.Active
    then UniMainModule.UpdTr_Batch.Rollback;

    UniMainModule.QBatch.Close;
    MainForm.WaveShowErrorToast(str_error_moving_documents);
  End;
end;

procedure TValidateCreditDocumentsFrm.BtnViewDocumentClick(Sender: TObject);
var Extension:   string;
    Imagefile:   string;
    Pdffile:     string;
    Officefile:  string;
    AUrl:        string;
    Destination: string;
begin
  if ((TblFiles.Active) and (not TblFiles.fieldbyname('Id').isNull)) then begin
    UniMainModule.QWork5.Close;
    UniMainModule.QWork5.SQL.Text := 'Select * from files_archive where id=:Id';
    UniMainModule.QWork5.ParamByName('Id').AsInteger := TblFiles.FieldByName('Id').AsInteger;
    UniMainModule.QWork5.Open;

    if UniMainModule.QWork5.FieldByName('Storage_type').AsInteger = 0
    then begin
           Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';

           if Trim(UniMainModule.QWork5.FieldByName('Folder').AsString) <> ''
           then Destination := Destination + UniMainModule.QWork5.FieldByName('Folder').AsString + '\';

           if Trim(UniMainModule.QWork5.FieldByName('SubFolder').AsString) <> ''
           then Destination := Destination + UniMainModule.QWork5.FieldByName('SubFolder').AsString + '\';

           Destination := Destination + UniMainModule.QWork5.FieldByName('Destination_Name').AsString;

           if FileExists(Destination)
           then begin
                  Extension := UpperCase(ExtractFileExt(UniMainModule.QWork5.FieldByName('Original_name').AsString));

                  if (Extension = '.PNG') or (Extension = '.JPG') or
                     (Extension = '.GIF') or (Extension = '.JPEG')
                  then begin
                         ImageFile := UniServerModule.NewCacheFileUrl(False, copy(Extension, 2, length(Extension)),
                                                                      Utils.EscapeIllegalChars(TPath.GetFileNameWithoutExtension(UniMainModule.QWork5.FieldByName('Original_name').AsString)),
                                                                      '', AUrl, True);

                         if CopyFile(PChar(Destination), PChar(ImageFile), False)
                         then UniSession.AddJS('window.open(''' + AUrl + ''', ''_blank'');');
                       end
                  else if (Extension = '.PDF')
                       then begin
                              Pdffile := UniServerModule.NewCacheFileUrl(False, copy(Extension, 2, length(Extension)),
                                                                         Utils.EscapeIllegalChars(TPath.GetFileNameWithoutExtension(UniMainModule.QWork5.FieldByName('Original_name').AsString)),
                                                                         '', AUrl, True);

                              if CopyFile(PChar(Destination), PChar(Pdffile), False)
                              then UniSession.AddJS('window.open(''' + AUrl + ''', ''_blank'');');
                            end
                       else if (Extension = '.DOCX') or (Extension = '.XLSX') or
                               (Extension = '.DOC')  or (Extension = '.XLS')
                            then begin
                                   OfficeFile := 'c:\inetpub\wwwroot\Shared_doc\' +
                                                 UniMainModule.QWork5.FieldByName('Destination_name').AsString +
                                                 Extension;

                                   if CopyFile(PChar(Destination), PChar(OfficeFile), False)
                                   then UniSession.AddJS('window.open(''' + 'https://view.officeapps.live.com/op/embed.aspx?src=' +
                                                         'https://wavedesk.cloud/Shared_doc/' + ExtractFileName(OfficeFile) +
                                                         ''', ''_blank'');');
                                 end;
                end
           else MainForm.WaveShowErrorToast(str_file_not_found);
         end;
    UniMainModule.QWork5.Close;
  end;
end;

procedure TValidateCreditDocumentsFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TValidateCreditDocumentsFrm.Init_credit_document_validation(DocumentItemId: longint;
                                                                      MasterId: longint;
                                                                      ModuleId: integer;
                                                                      Description: string);
begin
  _DocumentItemId := DocumentItemId;
  _MasterId := MasterId;
  _ModuleId := ModuleId;

  BasicBrowse.Close;
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: begin
         BasicBrowse.SQL.Text := 'Select * from basictables where CompanyId=:Companyid and tableid=:TableId and Removed=:Removed order by dutch';
         GridTypes.Columns[0].FieldName := 'Dutch';
       end;
    2: begin
         BasicBrowse.SQL.Text := 'Select * from basictables where CompanyId=:Companyid and tableid=:TableId and Removed=:Removed order by french';
         GridTypes.Columns[0].FieldName := 'French';
       end;
    3: begin
         BasicBrowse.SQL.Text := 'Select * from basictables where CompanyId=:Companyid and tableid=:TableId and Removed=:Removed order by english';
         GridTypes.Columns[0].FieldName := 'English';
       end;
  end;
  BasicBrowse.ParamByName('CompanyId').AsInteger := UniMainModule.Company_id;
  BasicBrowse.ParamByName('TableId').AsInteger   := 2010;
  BasicBrowse.ParamByName('Removed').AsString    := '0';
  BasicBrowse.Open;

  BasicSelect.Close;
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: begin
         BasicSelect.SQL.Text := 'Select * from basictables where CompanyId=:Companyid and tableid=:TableId and Removed=:Removed order by dutch';
         GridTypes.Columns[0].FieldName := 'Dutch';
       end;
    2: begin
         BasicSelect.SQL.Text := 'Select * from basictables where CompanyId=:Companyid and tableid=:TableId and Removed=:Removed order by french';
         GridTypes.Columns[0].FieldName := 'French';
       end;
    3: begin
         BasicSelect.SQL.Text := 'Select * from basictables where CompanyId=:Companyid and tableid=:TableId and Removed=:Removed order by english';
         GridTypes.Columns[0].FieldName := 'English';
       end;
  end;

  BasicSelect.ParamByName('CompanyId').AsInteger := UniMainModule.Company_id;
  BasicSelect.ParamByName('TableId').AsInteger   := 2010;
  BasicSelect.ParamByName('Removed').AsString    := '0';
  BasicSelect.Open;

  //Fetch the documents and display them
  TblFiles.First;
  while not TblFiles.Eof do
    TblFiles.Delete;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Text := 'select id, original_name from files_archive ' +
                                  'where module_id=:from_module_id and record_id=:from_record_id ' +
                                  'and removed=:Removed';
  UniMainModule.QWork.ParamByName('From_module_id').AsInteger := 100;
  UniMainModule.QWork.ParamByName('From_record_id').AsInteger := _DocumentItemId;
  UniMainModule.QWork.ParamByName('Removed').AsString         := '0';
  UniMainModule.QWork.Open;
  UniMainModule.QWork.First;

  while not UniMainModule.QWork.Eof
  do begin
       TblFiles.Append;
       TblFiles.FieldByName('Id').AsInteger           := UniMainModule.QWork.FieldByName('Id').AsInteger;
       TblFiles.FieldByName('Original_name').AsString := UniMainModule.QWork.FieldByName('Original_name').AsString;
       TblFiles.FieldByName('Description').AsString   := Description;
       TblFiles.Post;
       UniMainModule.QWork.Next;
     end;
  UniMainModule.QWork.Close;
  TblFiles.First;
end;

procedure TValidateCreditDocumentsFrm.LinkedLanguageValidateCreditDocumentsChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TValidateCreditDocumentsFrm.UpdateStrings;
begin
  str_file_not_found         := LinkedLanguageValidateCreditDocuments.GetTextOrDefault('strstr_file_not_found' (* 'Bestand niet gevonden' *) );
  str_error_moving_documents := LinkedLanguageValidateCreditDocuments.GetTextOrDefault('strstr_error_moving_documents' (* 'Fout bij het verplaatsen van de documenten' *) );
end;

end.


