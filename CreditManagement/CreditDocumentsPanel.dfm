object CreditDocumentsPanelFrm: TCreditDocumentsPanelFrm
  Left = 0
  Top = 0
  Width = 1137
  Height = 431
  OnCreate = UniFrameCreate
  Layout = 'hbox'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerDocItems: TUniContainerPanel
    Left = 8
    Top = 16
    Width = 521
    Height = 369
    Hint = ''
    ParentColor = False
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Flex = 1
    LayoutConfig.Height = '100%'
    object HeaderDocItems: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 521
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Region = 'north'
      object HeaderRightDocItems: TUniContainerPanel
        Left = 397
        Top = 0
        Width = 124
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alRight
        TabOrder = 1
        LayoutConfig.Region = 'east'
        LayoutConfig.Margin = '0 8 0 0'
        object BtnEditDocItem: TUniThemeButton
          Left = 0
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Wijzigen vereiste documenten'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 1
          Images = UniMainModule.ImageList
          ImageIndex = 11
          OnClick = BtnEditDocItemClick
          ButtonTheme = uctPrimary
        end
        object BtnExportDocItem: TUniThemeButton
          Left = 96
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Lijst exporteren'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 2
          Images = UniMainModule.ImageList
          ImageIndex = 31
          OnClick = BtnExportDocItemClick
          ButtonTheme = uctSecondary
        end
        object BtnMarkValidated: TUniThemeButton
          Left = 32
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Valideren documenten'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 3
          Images = UniMainModule.ImageList
          ImageIndex = 8
          OnClick = BtnMarkValidatedClick
          ButtonTheme = uctSuccess
        end
        object BtnMarkinvalid: TUniThemeButton
          Left = 64
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Markeren als ongeldig'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 4
          Images = UniMainModule.ImageList
          ImageIndex = 98
          OnClick = BtnMarkinvalidClick
          ButtonTheme = uctWarning
        end
      end
    end
    object GridDocItems: TUniDBGrid
      Left = 8
      Top = 42
      Width = 497
      Height = 295
      Hint = ''
      DataSource = Ds_VwDocItems
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 8 8 8'
      TabOrder = 2
      OnDblClick = BtnMarkValidatedClick
      OnDrawColumnCell = GridDocItemsDrawColumnCell
      Columns = <
        item
          Flex = 1
          FieldName = 'DESCRIPTION_DUTCH'
          Title.Caption = 'Omschrijving'
          Width = 200
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'Dummy'
          Title.Caption = ' '
          Width = 25
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
  end
  object ContainerDocuments: TUniContainerPanel
    Left = 552
    Top = 16
    Width = 561
    Height = 369
    Hint = ''
    ParentColor = False
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Flex = 1
    LayoutConfig.Height = '100%'
    object HeaderDocuments: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 561
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Region = 'north'
      object HeaderRightDocuments: TUniContainerPanel
        Left = 437
        Top = 0
        Width = 124
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alRight
        TabOrder = 1
        LayoutConfig.Region = 'east'
        LayoutConfig.Margin = '0 8 0 0'
        object BtnDeleteDocument: TUniThemeButton
          Left = 64
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Verwijderen document'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 1
          Images = UniMainModule.ImageList
          ImageIndex = 12
          OnClick = BtnDeleteDocumentClick
          ButtonTheme = uctDanger
        end
        object BtnOpenDocument: TUniThemeButton
          Left = 32
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Openen document'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 2
          Images = UniMainModule.ImageList
          ImageIndex = 55
          OnClick = BtnOpenDocumentClick
          ButtonTheme = uctPrimary
        end
        object BtnExportList: TUniThemeButton
          Left = 96
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Exporteren lijst'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 3
          Images = UniMainModule.ImageList
          ImageIndex = 31
          OnClick = BtnExportListClick
          ButtonTheme = uctSecondary
        end
        object BtnViewDocument: TUniThemeButton
          Left = 0
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Bekijken document'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 4
          Images = UniMainModule.ImageList
          ImageIndex = 7
          OnClick = BtnViewDocumentClick
          ButtonTheme = uctPrimary
        end
      end
    end
    object GridDocuments: TUniDBGrid
      Left = 8
      Top = 42
      Width = 545
      Height = 319
      Hint = ''
      DataSource = Ds_VwAttachments
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 8 8 8'
      TabOrder = 2
      OnColumnSort = GridDocumentsColumnSort
      OnDblClick = BtnViewDocumentClick
      Columns = <
        item
          Flex = 1
          FieldName = 'ORIGINAL_NAME'
          Title.Caption = 'Naam'
          Width = 304
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'DATE_ENTERED'
          Title.Caption = 'Tijdstip upload'
          Width = 135
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
  end
  object VwDocItems: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    credit_doc_items.id,'
      '    credit_doc_items.module_id,'
      '    sys_credit_doc_items.description_dutch,'
      '    sys_credit_doc_items.description_french,'
      '    sys_credit_doc_items.description_english,'
      '    credit_doc_items.sys_credit_doc_item,'
      '    credit_doc_items.present,'
      '    credit_doc_items.validated,'
      '    credit_doc_items.rejected'
      'from credit_doc_items'
      
        '   left outer join sys_credit_doc_items on (credit_doc_items.sys' +
        '_credit_doc_item = sys_credit_doc_items.id)')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 232
    Top = 136
    object VwDocItemsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object VwDocItemsDESCRIPTION_DUTCH: TWideStringField
      FieldName = 'DESCRIPTION_DUTCH'
      ReadOnly = True
      Size = 200
    end
    object VwDocItemsDESCRIPTION_FRENCH: TWideStringField
      FieldName = 'DESCRIPTION_FRENCH'
      ReadOnly = True
      Size = 200
    end
    object VwDocItemsDESCRIPTION_ENGLISH: TWideStringField
      FieldName = 'DESCRIPTION_ENGLISH'
      ReadOnly = True
      Size = 200
    end
    object VwDocItemsSYS_CREDIT_DOC_ITEM: TLargeintField
      FieldName = 'SYS_CREDIT_DOC_ITEM'
      Required = True
    end
    object VwDocItemsPRESENT: TSmallintField
      FieldName = 'PRESENT'
      Required = True
    end
    object VwDocItemsVALIDATED: TSmallintField
      FieldName = 'VALIDATED'
      Required = True
    end
    object VwDocItemsREJECTED: TSmallintField
      FieldName = 'REJECTED'
    end
    object VwDocItemsDummy: TStringField
      FieldKind = fkCalculated
      FieldName = 'Dummy'
      Size = 1
      Calculated = True
    end
    object VwDocItemsMODULE_ID: TIntegerField
      FieldName = 'MODULE_ID'
      Required = True
    end
  end
  object Ds_VwDocItems: TDataSource
    DataSet = VwDocItems
    Left = 232
    Top = 192
  end
  object VwAttachments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    files_archive.id,'
      '    files_archive.record_id,'
      '    files_archive.original_name,'
      '    files_archive.destination_name,'
      '    files_archive.description,'
      '    files_archive.visible_portal,'
      '    files_archive.date_entered,'
      '    files_archive.storage_type,'
      '    files_archive.folder,'
      '    files_archive.subfolder,'
      '    basictables.dutch typedescription'
      'from files_archive'
      
        '   left outer join basictables on (files_archive.file_type = bas' +
        'ictables.id)'
      '')
    MasterFields = 'ID'
    DetailFields = 'RECORD_ID'
    MasterSource = Ds_VwDocItems
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 832
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        Value = nil
      end>
    object VwAttachmentsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object VwAttachmentsORIGINAL_NAME: TWideStringField
      FieldName = 'ORIGINAL_NAME'
      Required = True
      Size = 255
    end
    object VwAttachmentsDESTINATION_NAME: TWideStringField
      FieldName = 'DESTINATION_NAME'
      Required = True
      Size = 255
    end
    object VwAttachmentsDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Size = 250
    end
    object VwAttachmentsVISIBLE_PORTAL: TSmallintField
      FieldName = 'VISIBLE_PORTAL'
      Required = True
    end
    object VwAttachmentsDATE_ENTERED: TDateTimeField
      FieldName = 'DATE_ENTERED'
    end
    object VwAttachmentsSTORAGE_TYPE: TSmallintField
      FieldName = 'STORAGE_TYPE'
    end
    object VwAttachmentsTYPEDESCRIPTION: TWideStringField
      FieldName = 'TYPEDESCRIPTION'
      ReadOnly = True
      Size = 50
    end
    object VwAttachmentsOriginal_name_description: TStringField
      FieldKind = fkCalculated
      FieldName = 'Original_name_description'
      Size = 255
      Calculated = True
    end
    object VwAttachmentsRECORD_ID: TLargeintField
      FieldName = 'RECORD_ID'
      Required = True
    end
    object VwAttachmentsFOLDER: TWideStringField
      FieldName = 'FOLDER'
    end
    object VwAttachmentsSUBFOLDER: TWideStringField
      FieldName = 'SUBFOLDER'
    end
  end
  object Ds_VwAttachments: TDataSource
    DataSet = VwAttachments
    Left = 832
    Top = 184
  end
  object LinkedLanguageCreditDocumentsPanel: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'VwDocItemsID.DisplayLabel'
      'VwDocItemsDESCRIPTION_DUTCH.DisplayLabel'
      'VwDocItemsDESCRIPTION_FRENCH.DisplayLabel'
      'VwDocItemsDESCRIPTION_ENGLISH.DisplayLabel'
      'VwDocItemsSYS_CREDIT_DOC_ITEM.DisplayLabel'
      'VwDocItemsPRESENT.DisplayLabel'
      'VwAttachmentsID.DisplayLabel'
      'VwAttachmentsORIGINAL_NAME.DisplayLabel'
      'VwAttachmentsDESTINATION_NAME.DisplayLabel'
      'VwAttachmentsDESCRIPTION.DisplayLabel'
      'VwAttachmentsVISIBLE_PORTAL.DisplayLabel'
      'VwAttachmentsDATE_ENTERED.DisplayLabel'
      'VwAttachmentsSTORAGE_TYPE.DisplayLabel'
      'VwAttachmentsTYPEDESCRIPTION.DisplayLabel'
      'VwAttachmentsOriginal_name_description.DisplayLabel'
      'VwAttachmentsRECORD_ID.DisplayLabel'
      'VwAttachmentsFOLDER.DisplayLabel'
      'VwAttachmentsSUBFOLDER.DisplayLabel'
      'TCreditDocumentsPanelFrm.Layout'
      'ContainerDocItems.Layout'
      'HeaderDocItems.Layout'
      'HeaderRightDocItems.Layout'
      'ContainerDocuments.Layout'
      'HeaderDocuments.Layout'
      'HeaderRightDocuments.Layout'
      'VwAttachments.DetailFields')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageCreditDocumentsPanelChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 336
    Top = 264
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00420074006E0049006E00760061006C006900640061007400
      650044006F00630075006D0065006E00740073000100410066006B0065007500
      720065006E00200064006F00630075006D0065006E00740065006E0001005200
      65006A006500740065007200200064006F00630075006D0065006E0074007300
      0100520065006A00650063007400200064006F00630075006D0065006E007400
      730001000D000A00420074006E0049006E00760061006C006900640061007400
      650044006F00630075006D0065006E007400730041006E0064004D0061006900
      6C000100410066006B0065007500720065006E00200064006F00630075006D00
      65006E00740065006E00200065006E0020006D00610069006C00200076006500
      7200730074007500720065006E000100520065006A0065007400650072002000
      64006F00630075006D0065006E0074007300200065007400200065006E007600
      6F00790065007200200064007500200063006F00750072007200690065007200
      0100520065006A00650063007400200064006F00630075006D0065006E007400
      7300200061006E0064002000730065006E00640020006D00610069006C000100
      0D000A0073007400480069006E00740073005F0055006E00690063006F006400
      65000D000A00420074006E00450064006900740044006F006300490074006500
      6D000100570069006A007A006900670065006E00200076006500720065006900
      730074006500200064006F00630075006D0065006E00740065006E0001004D00
      6F0064006900660069006500720020006C0065007300200064006F0063007500
      6D0065006E007400730020007200650071007500690073000100430068006100
      6E0067006500200072006500710075006900720065006400200064006F006300
      75006D0065006E007400730001000D000A00420074006E004500780070006F00
      7200740044006F0063004900740065006D0001004C0069006A00730074002000
      6500780070006F00720074006500720065006E0001004500780070006F007200
      74006500720020006C00610020006C0069007300740065000100450078007000
      6F007200740020006C0069007300740001000D000A00420074006E004D006100
      72006B00560061006C006900640061007400650064000100560061006C006900
      64006500720065006E00200064006F00630075006D0065006E00740065006E00
      0100560061006C006900640065007200200064006F00630075006D0065006E00
      740073000100560061006C0069006400610074006500200064006F0063007500
      6D0065006E007400730001000D000A00420074006E004D00610072006B006900
      6E00760061006C006900640001004D00610072006B006500720065006E002000
      61006C00730020006F006E00670065006C0064006900670001004D0061007200
      7100750065007200200063006F006D006D006500200069006E00760061006C00
      69006400650001004D00610072006B00200061007300200069006E0076006100
      6C006900640001000D000A00420074006E00440065006C006500740065004400
      6F00630075006D0065006E0074000100560065007200770069006A0064006500
      720065006E00200064006F00630075006D0065006E0074000100530075007000
      7000720069006D006500720020006C006500200064006F00630075006D006500
      6E0074000100440065006C00650074006500200064006F00630075006D006500
      6E00740001000D000A00420074006E004F00700065006E0044006F0063007500
      6D0065006E00740001004F00700065006E0065006E00200064006F0063007500
      6D0065006E00740001004F007500760072006900720020006C00650020006400
      6F00630075006D0065006E00740001004F00700065006E00200064006F006300
      75006D0065006E00740001000D000A00420074006E004500780070006F007200
      74004C0069007300740001004500780070006F00720074006500720065006E00
      20006C0069006A007300740001004500780070006F0072007400650072002000
      6C00610020006C00690073007400650001004500780070006F00720074002000
      6C0069007300740001000D000A00420074006E00560069006500770044006F00
      630075006D0065006E0074000100420065006B0069006A006B0065006E002000
      64006F00630075006D0065006E007400010056006F006900720020006C006500
      200064006F00630075006D0065006E0074000100560069006500770020006400
      6F00630075006D0065006E00740001000D000A00730074004400690073007000
      6C00610079004C006100620065006C0073005F0055006E00690063006F006400
      65000D000A007300740046006F006E00740073005F0055006E00690063006F00
      640065000D000A00730074004D0075006C00740069004C0069006E0065007300
      5F0055006E00690063006F00640065000D000A00730074005300740072006900
      6E00670073005F0055006E00690063006F00640065000D000A00730074007200
      7300740072005F006500720072006F0072005F00660069006C0065006E006F00
      740066006F0075006E0064000100480065007400200064006F00630075006D00
      65006E00740020006B006F006E0020006E00690065007400200077006F007200
      640065006E0020006700650076006F006E00640065006E0001004C0065002000
      64006F00630075006D0065006E0074002000650073007400200069006E007400
      72006F0075007600610062006C0065000100540068006500200064006F006300
      75006D0065006E007400200063006F0075006C00640020006E006F0074002000
      62006500200066006F0075006E00640001000D000A0073007400720073007400
      72005F00640065006C006500740065005F0064006F00630075006D0065006E00
      74000100570065006E0073007400200075002000680065007400200064006F00
      630075006D0065006E0074002000740065002000760065007200770069006A00
      64006500720065006E003F00010053006F007500680061006900740065007A00
      2D0076006F007500730020007300750070007000720069006D00650072002000
      6C006500200064006F00630075006D0065006E00740020003F00010044006F00
      200079006F00750020007700690073006800200074006F002000640065006C00
      6500740065002000740068006500200064006F00630075006D0065006E007400
      3F0001000D000A007300740072007300740072005F006500720072006F007200
      5F006300680061006E00670065005F0073007400610074007500730001004600
      6F00750074002000620069006A0020006800650074002000610061006E007000
      61007300730065006E002000760061006E002000640065002000730074006100
      740075007300010045007200720065007500720020006400650020007200E900
      67006C0061006700650020006400650020006C002700E9007400610074000100
      4500720072006F0072002000610064006A0075007300740069006E0067002000
      73007400610074007500730001000D000A007300740072007300740072005F00
      6500720072006F0072005F00640065006C006500740069006E00670001004600
      6F00750074002000620069006A00200068006500740020007600650072007700
      69006A0064006500720065006E00010045007200720065007500720020006C00
      6F007200730020006400650020006C0061002000730075007000700072006500
      7300730069006F006E0001004500720072006F0072002000640065006C006500
      740069006E00670001000D000A007300740072007300740072005F0063007200
      65006400690074005F0064006F00630075006D0065006E007400730001004B00
      72006500640069006500740064006F00630075006D0065006E00740065006E00
      010044006F00630075006D0065006E0074007300200064006500200063007200
      E9006400690074000100430072006500640069007400200064006F0063007500
      6D0065006E007400730001000D000A007300740072007300740072005F007200
      65006A006500630074005F0064006F00630075006D0065006E00740073000100
      410066006B0065007500720065006E00200064006F00630075006D0065006E00
      740065006E000100520065006A006500740065007200200064006F0063007500
      6D0065006E00740073000100520065006A00650063007400200064006F006300
      75006D0065006E007400730001000D000A007300740072007300740072005F00
      63006F006E006600690072006D005F00720065006A006500630074005F006400
      6F00630075006D0065006E00740073000100570065006E007300740020005500
      200064006500200064006F00630075006D0065006E00740065006E0020006100
      660020007400650020006B0065007500720065006E003F00010056006F007500
      7300200073006F007500680061006900740065007A002000720065006A006500
      74006500720020006C0065007300200064006F00630075006D0065006E007400
      730020003F00010044006F00200079006F007500200077006900730068002000
      74006F002000720065006A006500630074002000740068006500200064006F00
      630075006D0065006E00740073003F0001000D000A0073007400720073007400
      72005F006500720072006F0072005F00720065006A0065006300740069006E00
      67005F0064006F00630075006D0065006E0074007300010046006F0075007400
      2000620069006A0020006800650074002000610066006B006500750072006500
      6E002000760061006E00200064006500200064006F00630075006D0065006E00
      740065006E002100010045007200720065007500720020006C006F0072007300
      2000640075002000720065006A00650074002000640065007300200064006F00
      630075006D0065006E0074007300A000210001004500720072006F0072002000
      720065006A0065006300740069006E0067002000740068006500200064006F00
      630075006D0065006E0074007300210001000D000A00730074004F0074006800
      6500720053007400720069006E00670073005F0055006E00690063006F006400
      65000D000A007300740043006F006C006C0065006300740069006F006E007300
      5F0055006E00690063006F00640065000D000A00470072006900640044006F00
      63004900740065006D0073002E0043006F006C0075006D006E0073005B003000
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      72006900640044006F0063004900740065006D0073002E0043006F006C007500
      6D006E0073005B0030005D002E005400690074006C0065002E00430061007000
      740069006F006E0001004F006D00730063006800720069006A00760069006E00
      670001004400650073006300720069007000740069006F006E00010044006500
      73006300720069007000740069006F006E0001000D000A004700720069006400
      44006F0063004900740065006D0073002E0043006F006C0075006D006E007300
      5B0031005D002E0043006800650063006B0042006F0078004600690065006C00
      64002E004600690065006C006400560061006C00750065007300010074007200
      750065003B00660061006C0073006500010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001000D00
      0A00470072006900640044006F0063004900740065006D0073002E0043006F00
      6C0075006D006E0073005B0031005D002E005400690074006C0065002E004300
      61007000740069006F006E000100410061006E00770065007A00690067000100
      50007200E900730065006E0074000100500072006500730065006E0074000100
      0D000A00470072006900640044006F00630075006D0065006E00740073002E00
      43006F006C0075006D006E0073005B0030005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A00470072006900640044006F0063007500
      6D0065006E00740073002E0043006F006C0075006D006E0073005B0030005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001004E00
      610061006D0001004E006F006D0001004E0061006D00650001000D000A004700
      72006900640044006F00630075006D0065006E00740073002E0043006F006C00
      75006D006E0073005B0031005D002E0043006800650063006B0042006F007800
      4600690065006C0064002E004600690065006C006400560061006C0075006500
      7300010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      7300650001000D000A00470072006900640044006F00630075006D0065006E00
      740073002E0043006F006C0075006D006E0073005B0031005D002E0054006900
      74006C0065002E00430061007000740069006F006E000100540069006A006400
      73007400690070002000750070006C006F00610064000100540065006D007000
      730020007400E9006C00E900630068002E000100550070006C006F0061006400
      2000740069006D00650001000D000A0073007400430068006100720053006500
      740073005F0055006E00690063006F00640065000D000A00}
  end
  object PopupMenuInvalid: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 416
    Top = 144
    object BtnInvalidateDocuments: TUniMenuItem
      Caption = 'Afkeuren documenten'
      ImageIndex = 98
      OnClick = BtnInvalidateDocumentsClick
    end
    object BtnInvalidateDocumentsAndMail: TUniMenuItem
      Caption = 'Afkeuren documenten en mail versturen'
      ImageIndex = 98
      OnClick = BtnInvalidateDocumentsAndMailClick
    end
  end
end
