unit CreditFileDemanderAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniCheckBox, uniDBCheckBox, uniDBEdit, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniButton, UniThemeButton, uniLabel, uniDateTimePicker, uniDBDateTimePicker, uniEdit,
  uniGUIBaseClasses, uniGroupBox, Data.DB, MemDS, DBAccess, IBC, uniImage,
  siComp, siLngLnk;

type
  TCreditFileDemanderAddFrm = class(TUniForm)
    GroupboxProfession: TUniGroupBox;
    Edit_Name_Employer: TUniDBEdit;
    Edit_Company_Number: TUniDBEdit;
    Edit_StartDate_Independent: TUniDBDateTimePicker;
    Edit_Independent_activity: TUniDBEdit;
    LblIndependentDemander1: TUniLabel;
    Edit_Contract_Type: TUniDBLookupComboBox;
    EditInserviceSinceD1: TUniDBDateTimePicker;
    LblEmployedDemander1: TUniLabel;
    Edit_Profession: TUniDBLookupComboBox;
    GroupboxCosts: TUniGroupBox;
    Edit_Rental_Cost: TUniDBFormattedNumberEdit;
    Edit_Rental_Cost_Stops: TUniDBCheckBox;
    Edit_Credit_Openings: TUniDBFormattedNumberEdit;
    Edit_Credit_Purposes: TUniDBLookupComboBox;
    Edit_Alimentation: TUniDBFormattedNumberEdit;
    Edit_Other_Charges: TUniDBFormattedNumberEdit;
    GroupboxIncome: TUniGroupBox;
    LblProfessionalIncomeD1: TUniLabel;
    Edit_net_monthly_income: TUniDBFormattedNumberEdit;
    Edit_foreign_income: TUniDBFormattedNumberEdit;
    Edit_country_of_origine: TUniDBLookupComboBox;
    Edit_holiday_pay: TUniDBFormattedNumberEdit;
    Edit_end_of_year_bonus: TUniDBFormattedNumberEdit;
    Edit_meal_vouchers: TUniDBFormattedNumberEdit;
    Edit_child_money: TUniDBFormattedNumberEdit;
    LabelRentalIncomeD1: TUniLabel;
    Edit_rental_income_current_private: TUniDBFormattedNumberEdit;
    Edit_rental_income_future_private: TUniDBFormattedNumberEdit;
    Edit_rental_income_current_professional: TUniDBFormattedNumberEdit;
    Edit_rental_income_future_professional: TUniDBFormattedNumberEdit;
    LblOtherIncomeD1: TUniLabel;
    Edit_other_income: TUniDBFormattedNumberEdit;
    Edit_other_income_kind: TUniDBEdit;
    Professions: TIBCQuery;
    DsProfessions: TDataSource;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    ContractTypes: TIBCQuery;
    DsContractTypes: TDataSource;
    CreditPurposes: TIBCQuery;
    DsCreditPurposes: TDataSource;
    Countries: TIBCQuery;
    DsCountries: TDataSource;
    Edit_demander_contact: TUniDBLookupComboBox;
    Contacts: TIBCQuery;
    DsContacts: TDataSource;
    CrmAccounts: TIBCQuery;
    DsCrmAccounts: TDataSource;
    Edit_demander_company: TUniDBLookupComboBox;
    CreditFile_Demander_Edit: TIBCQuery;
    Ds_CreditFile_Demander_Edit: TDataSource;
    UpdTr_CreditFile_Demander_Edit: TIBCTransaction;
    LinkedLanguageCreditFileDemanderAdd: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageCreditFileDemanderAddChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure Edit_demander_contactTriggerEvent(Sender: TUniCustomComboBox;
      AButtonId: Integer);
    procedure Edit_demander_companyTriggerEvent(Sender: TUniCustomComboBox;
      AButtonId: Integer);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure CallBackInsertUpdateContact(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateAccount(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Initialize_insert_credit_file_demander(CreditFile: longint; RelationType: integer): boolean;
    function Initialize_edit_credit_file_demander(DemanderId: longint): boolean;
  end;

var
	strdemanderrequired: string = 'De aanvrager is een verplichte ingave'; // TSI: Localized (Don't modify!)
	strmodifydemander:   string = 'Wijzigen aanvrager'; // TSI: Localized (Don't modify!)
	stradddemander:      string = 'Toevoegen aanvrager'; // TSI: Localized (Don't modify!)

function CreditFileDemanderAddFrm: TCreditFileDemanderAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule, ContactsAdd, AccountsAdd, Main;

function CreditFileDemanderAddFrm: TCreditFileDemanderAddFrm;
begin
  Result := TCreditFileDemanderAddFrm(UniMainModule.GetFormInstance(TCreditFileDemanderAddFrm));
end;

{ TCreditFileDemanderAddFrm }


procedure TCreditFileDemanderAddFrm.CallBackInsertUpdateAccount(Sender: TComponent; AResult: Integer);
begin
  if CrmAccounts.Active
  then begin
         CrmAccounts.Refresh;
         if UniMainModule.Result_dbAction <> 0
         then CreditFile_Demander_Edit.FieldByName('Demander_id').AsInteger := UniMainModule.Result_dbAction;
       end;
end;

procedure TCreditFileDemanderAddFrm.CallBackInsertUpdateContact(Sender: TComponent; AResult: Integer);
begin
  if Contacts.Active
  then begin
         Contacts.Refresh;
         if UniMainModule.Result_dbAction <> 0
         then CreditFile_Demander_Edit.FieldByName('Demander_id').AsInteger := UniMainModule.Result_dbAction;
       end;
end;

procedure TCreditFileDemanderAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  CreditFile_Demander_Edit.Cancel;
  Close;
end;

procedure TCreditFileDemanderAddFrm.Edit_demander_companyTriggerEvent(
  Sender: TUniCustomComboBox; AButtonId: Integer);
begin
  case AButtonId of
    1: begin
         With AccountsAddFrm
         do begin
              if Init_account_creation
              then ShowModal(CallBackInsertUpdateAccount)
              else Close;
            end;
       end;
    2: begin
         if not CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').isNull
         then begin
                With AccountsAddFrm
                do begin
                     if Init_account_modification(CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').AsInteger)
                     then ShowModal(CallBackInsertUpdateAccount)
                     else Close;
                   end;
              end;
       end;
  end;
end;

procedure TCreditFileDemanderAddFrm.Edit_demander_contactTriggerEvent(
  Sender: TUniCustomComboBox; AButtonId: Integer);
begin
  case AButtonId of
    1: begin
         With ContactsAddFrm
         do begin
              if Init_contact_creation(0)
              then ShowModal(CallBackInsertUpdateContact)
              else Close;
            end;
       end;
    2: begin
         if not CreditFile_Demander_Edit.fieldbyname('DEMANDER_ID').isNull
         then begin
                With ContactsAddFrm
                do begin
                     if Init_contact_modification(CreditFile_Demander_Edit.FieldByName('Demander_id').AsInteger)
                     then ShowModal(CallBackInsertUpdateContact)
                     else Close;
                   end;
              end;
       end;
  end;
end;

procedure TCreditFileDemanderAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditFileDemanderAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  if CreditFile_Demander_Edit.FieldByName('Demander_id').isNull
  then begin
         MainForm.WaveShowWarningToast(strdemanderrequired);
         if CreditFile_Demander_Edit.FieldByName('Demander_type').AsInteger = 0
         then Edit_demander_contact.SetFocus;
         Exit;
       end;

  Try
    CreditFile_Demander_Edit.Post;
    if UpdTr_CreditFile_Demander_Edit.Active
    then UpdTr_CreditFile_Demander_Edit.Commit;
    UniMainModule.Result_dbAction := CreditFile_Demander_Edit.FieldByName('Id').AsInteger;
  Except on E: EDataBaseError
  do begin
       if UpdTr_CreditFile_Demander_Edit.Active
       then UpdTr_CreditFile_Demander_Edit.Rollback;
       Exit;
     end;
  end;
  Close;
end;

function TCreditFileDemanderAddFrm.Initialize_edit_credit_file_demander(DemanderId: longint): boolean;
begin
  Try
    Caption := strmodifydemander;
    CreditFile_Demander_Edit.Close;
    CreditFile_Demander_Edit.SQL.Clear;
    CreditFile_Demander_Edit.SQL.Add('Select * from credit_file_demanders where Id=' + IntToStr(DemanderId));
    CreditFile_Demander_Edit.Open;
    CreditFile_Demander_Edit.Edit;

    if CreditFile_Demander_Edit.FieldByName('Demander_Type').AsInteger = 0
    then begin
           Edit_demander_company.Visible         := False;
           Edit_demander_contact.Visible         := True;
           ActiveControl                         := Edit_demander_contact;
         end
    else if CreditFile_Demander_Edit.FieldByName('Demander_Type').AsInteger = 1
         then begin
                Edit_demander_contact.Visible    := False;
                Edit_demander_company.Visible    := True;
                ActiveControl                    := Edit_demander_company;
              end;
    Open_reference_tables;
    Result := True;
  Except
    Result := False;
  End;
end;

function TCreditFileDemanderAddFrm.Initialize_insert_credit_file_demander(CreditFile: longint; RelationType: integer): boolean;
begin
  Try
    Caption := stradddemander;
    CreditFile_Demander_Edit.Close;
    CreditFile_Demander_Edit.SQL.Clear;
    CreditFile_Demander_Edit.SQL.Add('Select first 0 * from credit_file_demanders');
    CreditFile_Demander_Edit.Open;
    CreditFile_Demander_Edit.Append;
    CreditFile_Demander_Edit.FieldByName('CompanyId').AsInteger         := UniMainModule.Company_id;
    CreditFile_Demander_Edit.FieldByName('Credit_File').AsInteger       := CreditFile;
    CreditFile_Demander_Edit.FieldByName('Rental_Cost_Stops').AsInteger := 0;
    CreditFile_Demander_Edit.FieldByName('Demander_type').AsInteger     := RelationType;

    if RelationType = 0
    then begin
           Edit_demander_company.Visible        := False;
           Edit_demander_contact.Visible        := True;
           ActiveControl                        := Edit_demander_contact;
         end
    else if RelationType = 1
         then begin
                Edit_demander_contact.Visible   := False;
                Edit_demander_company.Visible   := True;
                ActiveControl                   := Edit_demander_company;
              end;
    Open_reference_tables;
    Result := True;
  Except
    Result := False;
  End;
end;

procedure TCreditFileDemanderAddFrm.LinkedLanguageCreditFileDemanderAddChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditFileDemanderAddFrm.Open_reference_tables;
var SQL: string;
begin
  if CreditFile_Demander_Edit.Fieldbyname('Demander_type').asInteger = 0
  then begin
         Contacts.Close;
         Contacts.SQL.Clear;
         SQL := 'Select id, coalesce(last_name, '''') || '' '' || coalesce(first_name, '''') as FullName ' +
                'from crm_contacts where CompanyID=' + IntToStr(UniMainModule.Company_id) + ' ' +
                'and removed=''0'' order by FullName';
         Contacts.SQL.Add(SQL);
         Contacts.Open;
       end
  else if CreditFile_Demander_Edit.FieldByName('Demander_type').AsInteger = 1
       then begin
              CrmAccounts.Close;
              CrmAccounts.SQL.Clear;
              SQL := 'Select id, name from crm_accounts where companyid=' + IntToStr(UniMainModule.Company_id) +
                     ' and removed=''0'' order by name';
              CrmAccounts.SQL.Add(SQL);
              CrmAccounts.Open;
            end;

  Professions.Close;
  professions.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2003 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Profession.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Profession.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Profession.ListField := 'ENGLISH';
     end;
  end;
  Professions.SQL.Add(SQL);
  professions.Open;

  ContractTypes.Close;
  ContractTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2004 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Contract_Type.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Contract_Type.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Contract_Type.ListField := 'ENGLISH';
     end;
  end;
  ContractTypes.SQL.Add(SQL);
  ContractTypes.Open;

  CreditPurposes.Close;
  CreditPurposes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2005 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Credit_Purposes.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Credit_Purposes.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Credit_Purposes.ListField := 'ENGLISH';
     end;
  end;
  CreditPurposes.SQL.Add(SQL);
  CreditPurposes.Open;

  Countries.Close;
  Countries.SQL.Clear;
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       Countries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by dutch');
       Edit_country_of_origine.ListField := 'DUTCH';
     end;
  2: begin
       Countries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by french');
       Edit_country_of_origine.ListField := 'FRENCH';
     end;
  3: begin
       Countries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by english');
       Edit_country_of_origine.ListField := 'ENGLISH';
     end;
  end;
  Countries.Open;
end;

procedure TCreditFileDemanderAddFrm.UpdateStrings;
begin
  stradddemander      := LinkedLanguageCreditFileDemanderAdd.GetTextOrDefault('strstradddemander' (* 'Toevoegen aanvrager' *) );
  strmodifydemander   := LinkedLanguageCreditFileDemanderAdd.GetTextOrDefault('strstrmodifydemander' (* 'Wijzigen aanvrager' *) );
  strdemanderrequired := LinkedLanguageCreditFileDemanderAdd.GetTextOrDefault('strstrdemanderrequired' (* 'De aanvrager is een verplichte ingave' *) );
end;

end.
