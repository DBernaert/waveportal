unit CreditDocumentAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniGUIBaseClasses,
  uniButton, UniThemeButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniCheckBox, uniDBCheckBox, uniPanel, uniPageControl, siComp, siLngLnk;

var
	str_error_saving_data:      string = 'Fout bij het opslaan van de gegevens!'; // TSI: Localized (Don't modify!)
	str_error_adding:           string = 'Fout bij het toevoegen'; // TSI: Localized (Don't modify!)
	str_error_modifying:        string = 'Fout bij het wijzigen'; // TSI: Localized (Don't modify!)
	str_add_credit_document:    string = 'Toevoegen kredietdocument'; // TSI: Localized (Don't modify!)
	str_modify_credit_document: string = 'Wijzigen kredietdocument'; // TSI: Localized (Don't modify!)

type
  TCreditDocumentAddFrm = class(TUniForm)
    DocumentsEdit: TIBCQuery;
    DsDocumentsEdit: TDataSource;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    PageControlDocuments: TUniPageControl;
    TabSheetDutch: TUniTabSheet;
    TabSheetFrench: TUniTabSheet;
    TabSheetEnglish: TUniTabSheet;
    CheckboxShowHint: TUniDBCheckBox;
    EditDescriptionDutch: TUniDBEdit;
    EditHintDutch: TUniDBMemo;
    EditDescriptionFrench: TUniDBEdit;
    EditHintFrench: TUniDBMemo;
    EditDescriptionEnglish: TUniDBEdit;
    EditHintEnglish: TUniDBMemo;
    LinkedLanguageCreditDocumentsAdd: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageCreditDocumentsAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_add_document(Category: longint);
    procedure Init_edit_document(Id: longint);
  end;

function CreditDocumentAddFrm: TCreditDocumentAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function CreditDocumentAddFrm: TCreditDocumentAddFrm;
begin
  Result := TCreditDocumentAddFrm(UniMainModule.GetFormInstance(TCreditDocumentAddFrm));
end;

{ TCreditDocumentAddFrm }

procedure TCreditDocumentAddFrm.BtnCancelClick(Sender: TObject);
begin
  DocumentsEdit.Cancel;
  DocumentsEdit.Close;
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TCreditDocumentAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  Try
    DocumentsEdit.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
    UniMainModule.Result_dbAction := DocumentsEdit.FieldByName('Id').AsInteger;
    DocumentsEdit.Close;
    Close;
  Except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    DocumentsEdit.Close;
    MainForm.WaveShowErrorToast(str_error_saving_data);
  End;
end;

procedure TCreditDocumentAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditDocumentAddFrm.Init_add_document(Category: longint);
begin
  Caption := str_add_credit_document;
  Try
    DocumentsEdit.Close;
    DocumentsEdit.SQL.Text := 'Select first 0 * from sys_credit_doc_items';
    DocumentsEdit.Open;
    DocumentsEdit.Append;
    DocumentsEdit.FieldByName('CompanyId').AsInteger           := UniMainModule.Company_id;
    DocumentsEdit.FieldByName('Credit_doc_category').AsInteger := Category;
    DocumentsEdit.FieldByName('Show_hint').AsInteger           := 0;
    DocumentsEdit.FieldByName('Removed').AsString              := '0';
  Except
    MainForm.WaveShowErrorMessage(str_error_adding);
    Close;
  End;
end;

procedure TCreditDocumentAddFrm.Init_edit_document(Id: longint);
begin
  Caption := str_modify_credit_document;
  Try
    DocumentsEdit.Close;
    DocumentsEdit.SQL.Text := 'Select * from sys_credit_doc_items where Id=:Id';
    DocumentsEdit.ParamByName('Id').AsInteger := Id;
    DocumentsEdit.Open;
    DocumentsEdit.Edit;
  Except
    MainForm.WaveShowErrorMessage(str_error_modifying);
    Close;
  End;
end;

procedure TCreditDocumentAddFrm.LinkedLanguageCreditDocumentsAddChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditDocumentAddFrm.UniFormReady(Sender: TObject);
begin
  EditHintDutch.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
  EditHintFrench.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
  EditHintEnglish.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
end;

procedure TCreditDocumentAddFrm.UpdateStrings;
begin
  str_modify_credit_document := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_modify_credit_document' (* 'Wijzigen kredietdocument' *) );
  str_add_credit_document    := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_add_credit_document' (* 'Toevoegen kredietdocument' *) );
  str_error_modifying        := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_error_modifying' (* 'Fout bij het wijzigen' *) );
  str_error_adding           := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_error_adding' (* 'Fout bij het toevoegen' *) );
  str_error_saving_data      := LinkedLanguageCreditDocumentsAdd.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens!' *) );
end;

end.


