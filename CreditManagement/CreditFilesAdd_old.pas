unit CreditFilesAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniImage, uniCheckBox, uniDBCheckBox, uniDBEdit, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniEdit, uniMemo, uniDBMemo, uniComboBox, uniMultiItem, uniDBComboBox, uniDBLookupComboBox,
  uniPanel, uniPageControl, uniGUIBaseClasses, uniButton, UniThemeButton, Data.DB, MemDS, DBAccess, IBC, siComp,
  siLngLnk, uniDBNavigator, uniBasicGrid, uniDBGrid, uniBitBtn, uniMenuButton, Vcl.Menus, uniMainMenu;

var
	str_record_locked: string = 'Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.'; // TSI: Localized (Don't modify!)
	str_status_required: string = 'De status is een vereiste ingave.'; // TSI: Localized (Don't modify!)
	str_error_saving_data: string = 'Fout bij het opslaan van de gegevens!'; // TSI: Localized (Don't modify!)
	str_add_credit: string = 'Toevoegen krediet'; // TSI: Localized (Don't modify!)
	str_modify_credit: string = 'Wijzigen krediet'; // TSI: Localized (Don't modify!)

type
  TCreditFilesAddFrm = class(TUniForm)
    PageControlCreditDemand: TUniPageControl;
    TabSheetStatus: TUniTabSheet;
    Edit_Contributor: TUniDBLookupComboBox;
    TabSheetProfile: TUniTabSheet;
    LblRiskprofile: TUniLabel;
    EditNoRisks: TUniDBCheckBox;
    EditLimitedRisks: TUniDBCheckBox;
    EditHighRisks: TUniDBCheckBox;
    EditMaximumMonthlyCharge: TUniDBFormattedNumberEdit;
    LblPersonalPreferences: TUniLabel;
    EditFreeChoiceBankAccount: TUniDBCheckBox;
    EditFreeChoiceDebtInsurance: TUniDBCheckBox;
    EditCoverageDebtInsurance: TUniDBFormattedNumberEdit;
    EditFreeChoiceFireInsurance: TUniDBCheckBox;
    EditCommentsContributor: TUniDBMemo;
    EditCommentsAdmin: TUniDBMemo;
    EditMaximumDuration: TUniDBNumberEdit;
    TabSheetPlan: TUniTabSheet;
    LblPurchase: TUniLabel;
    EditPurchasePriceProperty: TUniDBFormattedNumberEdit;
    EditPurchaseCostProperty: TUniDBFormattedNumberEdit;
    EditPropertyType: TUniDBLookupComboBox;
    EditPropertyAddress: TUniDBEdit;
    LblNewBuilding: TUniLabel;
    EditPurchasePriceGroundNewProp: TUniDBFormattedNumberEdit;
    EditPurchaseCostNewProp: TUniDBFormattedNumberEdit;
    EditCostPriceBuildNewProp: TUniDBFormattedNumberEdit;
    EditVatBuildNewProp: TUniDBFormattedNumberEdit;
    EditCostArchitectNewProp: TUniDBFormattedNumberEdit;
    EditVatArchitectNewProp: TUniDBFormattedNumberEdit;
    EditCostPriceStudiesNewProp: TUniDBFormattedNumberEdit;
    EditVatStudiesNewProp: TUniDBFormattedNumberEdit;
    EditOterCostsNewProp: TUniDBFormattedNumberEdit;
    LblRefinance: TUniLabel;
    EditSaldoToTakeOverOtherCredits: TUniDBFormattedNumberEdit;
    EditHandLighting: TUniDBFormattedNumberEdit;
    EditReinvestmentallowance: TUniDBFormattedNumberEdit;
    LblDiverseCosts: TUniLabel;
    EditVariouscostsfile: TUniDBFormattedNumberEdit;
    EditVariousCostsEstimation: TUniDBFormattedNumberEdit;
    EditPurchaseSumDebtBalansIns: TUniDBFormattedNumberEdit;
    EditVariousCostsOther: TUniDBFormattedNumberEdit;
    LblCosts: TUniLabel;
    EditMortGageRegistration: TUniDBFormattedNumberEdit;
    EditMortGageMandate: TUniDBFormattedNumberEdit;
    LblFinancing: TUniLabel;
    EditOwnResources: TUniDBFormattedNumberEdit;
    EditTotalInvestMent: TUniDBFormattedNumberEdit;
    EditDemandedCredit: TUniDBFormattedNumberEdit;
    ImageLink: TUniImage;
    LblRenovation: TUniLabel;
    EditCostPriceBuildRenovate: TUniDBFormattedNumberEdit;
    EditVatBuildRenovation: TUniDBFormattedNumberEdit;
    EditCostArchitectRenovation: TUniDBFormattedNumberEdit;
    EditVatArchitectRenovation: TUniDBFormattedNumberEdit;
    EditCostPriceStudiesRenovation: TUniDBFormattedNumberEdit;
    EditVatStudiesRenovation: TUniDBFormattedNumberEdit;
    EditOtherCostsRenovation: TUniDBFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    Contributors: TIBCQuery;
    DsContributors: TDataSource;
    EditStatus: TUniDBLookupComboBox;
    Status: TIBCQuery;
    DsStatus: TDataSource;
    PropertyTypes: TIBCQuery;
    DsPropertyTypes: TDataSource;
    LinkedLanguageCreditFilesAdd: TsiLangLinked;
    Edit_Financial_institution: TUniDBLookupComboBox;
    FinInstitutions: TIBCQuery;
    DsFinInstitutions: TDataSource;
    EditCreditType: TUniDBLookupComboBox;
    CreditTypes: TIBCQuery;
    DsCreditTypes: TDataSource;
    EditTotalAmountCredit: TUniDBFormattedNumberEdit;
    EditDateCreditFinal: TUniDBDateTimePicker;
    ContainerDemanders: TUniContainerPanel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorDemanders: TUniDBNavigator;
    GridDemanders: TUniDBGrid;
    Demanders: TIBCQuery;
    DsDemanders: TDataSource;
    PopupAddDemander: TUniMenuButton;
    PopupMenuDemanderAdd: TUniPopupMenu;
    BtnAddOrganisation: TUniMenuItem;
    BtnAddRelation: TUniMenuItem;
    BtnModifyDemander: TUniThemeButton;
    BtnDeleteDemander: TUniThemeButton;
    ContainerFilterLeft: TUniContainerPanel;
    LblDemanders: TUniLabel;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure EditPurchasePricePropertyExit(Sender: TObject);
    procedure ImageLinkClick(Sender: TObject);
    procedure LinkedLanguageCreditFilesAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnAddRelationClick(Sender: TObject);
    procedure BtnAddOrganisationClick(Sender: TObject);
    procedure BtnModifyDemanderClick(Sender: TObject);
    procedure BtnDeleteDemanderClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure Calculate_totals;
    function  Save_file: boolean;
    procedure CallBackInsertUpdateDemander(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Init_creditfile_creation: boolean;
    function Init_creditfile_modification(Id: longint): boolean;
  end;

function CreditFilesAddFrm: TCreditFilesAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule, CreditFileDemanderAdd, Main, UniFSConfirm;

function CreditFilesAddFrm: TCreditFilesAddFrm;
begin
  Result := TCreditFilesAddFrm(UniMainModule.GetFormInstance(TCreditFilesAddFrm));
end;

procedure TCreditFilesAddFrm.BtnAddOrganisationClick(Sender: TObject);
begin
  if UniMainModule.CreditFiles_Edit.State = dsInsert
  then begin
         if Save_file = false
         then exit
         else begin
                UniMainModule.CreditFiles_Edit.Edit;
                Demanders.Close;
                Demanders.ParamByName('IdCreditFile').AsInteger := UniMainModule.CreditFiles_Edit.FieldByName('Id').AsInteger;
                Demanders.Open;
              end;
       end;

  UniMainModule.Result_dbAction := 0;
  with CreditFileDemanderAddFrm
  do begin
       if Initialize_insert_credit_file_demander(UniMainModule.CreditFiles_Edit.FieldByName('Id').AsInteger, 1)
       then ShowModal(CallBackInsertUpdateDemander);
     end;
end;

procedure TCreditFilesAddFrm.BtnAddRelationClick(Sender: TObject);
begin
  if UniMainModule.CreditFiles_Edit.State = dsInsert
  then begin
         if Save_file = false
         then exit
         else begin
                UniMainModule.CreditFiles_Edit.Edit;
                Demanders.Close;
                Demanders.ParamByName('IdCreditFile').AsInteger := UniMainModule.CreditFiles_Edit.FieldByName('Id').AsInteger;
                Demanders.Open;
              end;
       end;

  UniMainModule.Result_dbAction := 0;
  with CreditFileDemanderAddFrm
  do begin
       if Initialize_insert_credit_file_demander(UniMainModule.CreditFiles_Edit.FieldByName('Id').AsInteger, 0)
       then ShowModal(CallBackInsertUpdateDemander);
     end;
end;

procedure TCreditFilesAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.CreditFiles_Edit.Cancel;
  UniMainModule.CreditFiles_Edit.Close;
  Close;
end;

procedure TCreditFilesAddFrm.BtnDeleteDemanderClick(Sender: TObject);
begin
  if (not Demanders.Active) or (Demanders.FieldByName('Id').IsNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Portal_name, 'Verwijderen aanvrager?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.CreditFile_Demander_Edit.Close;
          UniMainModule.CreditFile_Demander_Edit.SQL.Clear;
          UniMainModule.CreditFile_Demander_Edit.SQL.Add('Select * from credit_file_demanders where id=' + Demanders.FieldByName('Id').AsString);
          UniMainModule.CreditFile_Demander_Edit.Open;
          UniMainModule.CreditFile_Demander_Edit.Delete;
          UniMainModule.UpdTr_CreditFile_Demander_Edit.Commit;
          UniMainModule.CreditFile_Demander_Edit.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTr_CreditFile_Demander_Edit.Rollback;
            UniMainModule.CreditFile_Demander_Edit.Close;
            MainForm.WaveShowErrorToast('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Demanders.Refresh;
      end;
    end);
end;

procedure TCreditFilesAddFrm.BtnModifyDemanderClick(Sender: TObject);
begin
  if BtnModifyDemander.Enabled = False
  then exit;

  if (Demanders.Active) and (not Demanders.FieldByName('Id').IsNull)
  then begin
         UniMainModule.Result_dbAction := 0;
         with CreditFileDemanderAddFrm
         do begin
              if Initialize_edit_credit_file_demander(Demanders.FieldByName('Id').AsInteger)
              then ShowModal(CallBackInsertUpdateDemander);
            end;
       end;
end;

procedure TCreditFilesAddFrm.BtnSaveClick(Sender: TObject);
begin
  if Save_file = True
  then begin
         UniMainModule.CreditFiles_Edit.Close;
         Close;
       end;
end;

function TCreditFilesAddFrm.Save_file: boolean;
begin
  BtnSave.SetFocus;

  if EditStatus.ItemIndex = -1 then begin
    MainForm.WaveShowWarningToast(str_status_required);
    PageControlCreditDemand.ActivePage := TabSheetStatus;
    EditStatus.SetFocus;
    Result := False;
    Exit;
  end;

  UniMainModule.CreditFiles_Edit.FieldByName('Modified_User_id').AsInteger := UniMainModule.User_id;
  UniMainModule.CreditFiles_Edit.FieldByName('Date_modified').AsDateTime   := Now;

  Try
    UniMainModule.CreditFiles_Edit.Post;
    UniMainModule.UpdTr_CreditFiles_Edit.Commit;
    UniMainModule.Result_dbAction := UniMainModule.CreditFiles_Edit.FieldByName('Id').AsInteger;
    Result := True;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       UniMainModule.UpdTr_CreditFiles_Edit.Rollback;
       MainForm.WaveShowErrorToast(str_error_saving_data);
       Result := False;
     end;
  end;
end;

procedure TCreditFilesAddFrm.Calculate_totals;
var totalinvestment: currency;
begin
  with UniMainModule.CreditFiles_Edit do begin
    totalinvestment :=
    fieldbyname('PURCHASE_PRICE_PROPERTY').AsCurrency            +
    fieldbyname('PURCHASE_COST_PROPERTY').asCurrency             +
    fieldbyname('PURCHASE_PRICE_GROUND_NEW_PROP').AsCurrency     +
    fieldbyname('PURCHASE_COST_NEW_PROP').AsCurrency             +
    fieldbyname('COST_PRICE_BUILD_NEW_PROP').AsCurrency          +
    fieldbyname('VAT_BUILD_NEW_PROP').AsCurrency                 +
    fieldbyname('COST_ARCHITECT_NEW_PROP').AsCurrency            +
    fieldbyname('VAT_ARCHITECT_NEW_PROP').AsCurrency             +
    fieldbyname('COST_PRICE_STUDIES_NEW_PROP').AsCurrency        +
    fieldbyname('VAT_STUDIES_NEW_PROP').AsCurrency               +
    fieldbyname('OTHER_COSTS_NEW_PROP').AsCurrency               +
    fieldbyname('COST_PRICE_BUILD_RENOVATE').AsCurrency          +
    fieldbyname('VAT_BUILD_RENOVATION').AsCurrency               +
    fieldbyname('COST_ARCHITECT_RENOVATION').AsCurrency          +
    fieldbyname('VAT_ARCHITECT_RENOVATION').AsCurrency           +
    fieldbyname('COST_PRICE_STUDIES_RENOVATION').AsCurrency      +
    fieldbyname('VAT_STUDIES_RENOVATION').AsCurrency             +
    fieldbyname('OTHER_COSTS_RENOVATION').AsCurrency             +
    fieldbyname('SALDO_TO_TAKEOVER_OTHER_CREDITS').AsCurrency    +
    fieldbyname('HANDLIGHTING').AsCurrency                       +
    fieldbyname('REINVESTMENT_ALLOWANCE').AsCurrency             +
    fieldbyname('VARIOUS_COSTS_FILE').AsCurrency                 +
    fieldbyname('VARIOUS_COSTS_ESTIMATION').AsCurrency           +
    fieldbyname('PURCHASE_SUM_DEBTBAL_INS').AsCurrency           +
    fieldbyname('VARIOUS_COSTS_OTHER').AsCurrency                +
    fieldbyname('MORTGAGE_REGISTRATION').AsCurrency              +
    fieldbyname('MORTGAGE_MANDATE').AsCurrency;

    fieldbyname('TOTAL_INVESTMENT').AsCurrency := totalinvestment;
    fieldbyname('DEMANDED_CREDIT').AsCurrency  := totalinvestment - fieldbyname('OWN_RESOURCES').AsCurrency;
  end;
end;

procedure TCreditFilesAddFrm.CallBackInsertUpdateDemander(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if Demanders.Active then begin
      Demanders.Refresh;
      Demanders.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCreditFilesAddFrm.EditPurchasePricePropertyExit(Sender: TObject);
begin
  Calculate_totals;
end;

procedure TCreditFilesAddFrm.ImageLinkClick(Sender: TObject);
begin
  UniSession.AddJS('window.open(''https://www.notaris.be/rekenmodules'', ''_blank'');');
end;

function TCreditFilesAddFrm.Init_creditfile_creation: boolean;
begin
  UniMainModule.Result_dbAction := 0;
  Caption := str_add_credit;
  Open_reference_tables;
  Try
    UniMainModule.CreditFiles_Edit.Close;
    UniMainModule.CreditFiles_Edit.SQL.Clear;
    UniMainModule.CreditFiles_Edit.SQL.Add('Select first 0 * from credit_files');
    UniMainModule.CreditFiles_Edit.Open;
    UniMainModule.CreditFiles_Edit.Append;
    UniMainModule.CreditFiles_Edit.FieldByName('CompanyId').AsInteger                      := UniMainModule.Company_id;
    UniMainModule.CreditFiles_Edit.FieldByName('Removed').AsString                         := '0';
    UniMainModule.CreditFiles_Edit.FieldByName('CONTRIBUTOR_ID').AsInteger                 := UniMainModule.User_id;
    UniMainModule.CreditFiles_Edit.FieldByName('Date_Entered').asDateTime                  := Now;
    UniMainModule.CreditFiles_Edit.FieldByName('NO_RISKS').AsInteger                       := 0;
    UniMainModule.CreditFiles_Edit.FieldByName('LIMITED_RISKS').AsInteger                  := 0;
    UniMainModule.CreditFiles_Edit.FieldByName('HIGH_RISKS').AsInteger                     := 0;
    UniMainModule.CreditFiles_Edit.FieldByName('FREE_CHOICE_OF_BANKACCOUNT').AsInteger     := 0;
    UniMainModule.CreditFiles_Edit.FieldByName('FREE_CHOICE_DEBT_INSURANCE').AsInteger     := 0;
    UniMainModule.CreditFiles_Edit.FieldByName('FREE_CHOICE_FIRE_INSURANCE').AsInteger     := 0;
    Result                                             := True;
  Except
    UniMainModule.CreditFiles_Edit.Close;
    Result                                             := False;
  End;
end;

function TCreditFilesAddFrm.Init_creditfile_modification(Id: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  Caption := str_modify_credit;
  if UniMainModule.User_allow_credit_edit = False
  then begin
         PopupAddDemander.Visible                := False;
         BtnAddOrganisation.Enabled              := False;
         BtnAddRelation.Enabled                  := False;
         BtnModifyDemander.Enabled               := False;
         BtnModifyDemander.Visible               := False;
         BtnDeleteDemander.Enabled               := False;
         BtnDeleteDemander.Visible               := False;
         BtnSave.Visible                         := False;
         BtnSave.Enabled                         := False;
         UniMainModule.CreditFiles_Edit.ReadOnly := True;
       end;

  Open_reference_tables;
  Try
    UniMainModule.CreditFiles_Edit.Close;
    UniMainModule.CreditFiles_Edit.SQL.Clear;
    UniMainModule.CreditFiles_Edit.SQL.Add('Select * from credit_files where id=' + IntToStr(Id));
    UniMainModule.CreditFiles_Edit.Open;
    if UniMainModule.User_allow_credit_edit = True
    then UniMainModule.CreditFiles_Edit.Edit;
    Demanders.Close;
    Demanders.ParamByName('IdCreditFile').AsInteger := UniMainModule.CreditFiles_Edit.FieldByName('Id').AsInteger;
    Demanders.Open;
    Result := True;
  Except
    UniMainModule.CreditFiles_Edit.Cancel;
    UniMainModule.CreditFiles_Edit.Close;
    Result := False;
    MainForm.WaveShowErrorToast('Kan gegevens niet wijzigen, in gebruik door een andere gebruiker.');
  End;
end;

procedure TCreditFilesAddFrm.LinkedLanguageCreditFilesAddChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditFilesAddFrm.Open_reference_tables;
var SQL: string;
begin
  Users.Close;
  Users.SQL.Clear;
  SQL := 'Select ID, coalesce(NAME,'''') || '' '' || coalesce(FIRSTNAME,'''') AS "USERFULLNAME" from users ' +
         'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by USERFULLNAME';
  Users.SQL.Add(SQL);
  Users.Open;

  Contributors.Close;
  Contributors.SQL.Clear;
  SQL := 'Select ID, coalesce(LAST_NAME,'''') || '' '' || coalesce(FIRST_NAME,'''') AS "FULLNAME" from credit_contributors ' +
         'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' ' +
         'order by FULLNAME';
  Contributors.SQL.Add(SQL);
  Contributors.Open;

  Status.Close;
  Status.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2008 ' +
         ' and REMOVED=''0''';

  SQL := SQL + ' order by DUTCH';
  EditStatus.ListField := 'DUTCH';

  Status.SQL.Add(SQL);
  Status.Open;

  PropertyTypes.Close;
  PropertyTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2006 ' +
         ' and REMOVED=''0''';

  SQL := SQL + ' order by DUTCH';
  EditPropertyType.ListField := 'DUTCH';

  PropertyTypes.SQL.Add(SQL);
  PropertyTypes.Open;

  FinInstitutions.Close;
  FinInstitutions.SQL.Clear;
  FinInstitutions.SQL.Add('Select id, Name from credit_financialinstitutions where companyid=' + IntTostr(UniMainModule.Company_id) +
                          ' and removed=''0'' order by name');
  FinInstitutions.Open;

  CreditTypes.Close;
  CreditTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2009 ' +
         ' and REMOVED=''0''';

  SQL := SQL + ' order by DUTCH';
  EditCreditType.ListField := 'DUTCH';

  CreditTypes.SQL.Add(SQL);
  CreditTypes.Open;
end;

procedure TCreditFilesAddFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
  PopupAddDemander.JSInterface.JSConfig('menuAlign', ['tr-br?']);
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TCreditFilesAddFrm.UniFormReady(Sender: TObject);
begin
  EditCommentsContributor.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  EditCommentsAdmin.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

procedure TCreditFilesAddFrm.UpdateStrings;
begin
  str_modify_credit := LinkedLanguageCreditFilesAdd.GetTextOrDefault('strstr_modify_credit' (* 'Wijzigen krediet' *) );
  str_add_credit := LinkedLanguageCreditFilesAdd.GetTextOrDefault('strstr_add_credit' (* 'Toevoegen krediet' *) );
  str_error_saving_data := LinkedLanguageCreditFilesAdd.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens!' *) );
  str_status_required := LinkedLanguageCreditFilesAdd.GetTextOrDefault('strstr_status_required' (* 'De status is een vereiste ingave.' *) );
  str_record_locked := LinkedLanguageCreditFilesAdd.GetTextOrDefault('strstr_record_locked' (* 'Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.' *) );
end;

end.

