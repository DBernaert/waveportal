unit CreditReductionAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, DBAccess, IBC, Data.DB,
  MemDS, uniEdit, uniDBEdit, uniGUIBaseClasses, uniMultiItem, uniComboBox,
  uniDBComboBox, uniDBLookupComboBox, siComp, siLngLnk;

var
	str_percentage_required: string = 'Het percentage is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_type_required:       string = 'Het type korting is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_error_saving:        string = 'Fout bij het opslaan van de gegevens.'; // TSI: Localized (Don't modify!)
	str_change_reduction:    string = 'Wijzigen cross selling korting'; // TSI: Localized (Don't modify!)
	str_add_reduction:       string = 'Toevoegen cross selling korting'; // TSI: Localized (Don't modify!)

type
  TCreditReductionAddFrm = class(TUniForm)
    EditReductionType: TUniDBLookupComboBox;
    EditPercentage: TUniDBFormattedNumberEdit;
    Reductions_Edit: TIBCQuery;
    DsReductions_Edit: TDataSource;
    UpdTransReductionsEdit: TIBCTransaction;
    ReductionTypes: TIBCQuery;
    DsReductionTypes: TDataSource;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    LinkedLanguageCreditReductinAdd: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure LinkedLanguageCreditReductinAddChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    function Initialize_insert_reduction(CreditFileId: longint): boolean;
    function Initialize_edit_reduction(ReductionId: longint): boolean;
  end;

function CreditReductionAddFrm: TCreditReductionAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function CreditReductionAddFrm: TCreditReductionAddFrm;
begin
  Result := TCreditReductionAddFrm(UniMainModule.GetFormInstance(TCreditReductionAddFrm));
end;

{ TCreditReductionAddFrm }

procedure TCreditReductionAddFrm.BtnCancelClick(Sender: TObject);
begin
   UniMainModule.Result_dbAction := 0;
   Reductions_Edit.Cancel;
   Close;
end;

procedure TCreditReductionAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  if Reductions_Edit.FieldByName('Reduction_percentage').isNull
  then begin
         MainForm.WaveShowWarningToast(str_percentage_required);
         Exit;
       end;

  if Reductions_Edit.FieldByName('Reduction_id').isNull
  then begin
         MainForm.WaveShowWarningToast(str_type_required);
         EditPercentage.SetFocus;
         Exit;
       end;

  Try
    Reductions_Edit.Post;
    if UpdTransReductionsEdit.Active
    then UpdTransReductionsEdit.Commit;
    UniMainModule.Result_dbAction := Reductions_Edit.FieldByName('Id').AsInteger;
  Except on E: EDataBaseError
  do begin
       if UpdTransReductionsEdit.Active
       then UpdTransReductionsEdit.Rollback;
       MainForm.WaveShowErrorToast(str_error_saving);
     end;
  end;
  Close;
end;

function TCreditReductionAddFrm.Initialize_edit_reduction(
  ReductionId: longint): boolean;
begin
  Try
    Caption := str_change_reduction;
    Reductions_Edit.Close;
    Reductions_Edit.SQL.Clear;
    Reductions_Edit.SQL.Add('Select * from credit_reductions where id=' + IntToStr(ReductionId));
    Reductions_Edit.Open;
    Reductions_Edit.Edit;
    Open_reference_tables;
    Result := True;
  Except
    Result := False;
  End;
end;

function TCreditReductionAddFrm.Initialize_insert_reduction(
  CreditFileId: longint): boolean;
begin
  Try
    Caption := str_add_reduction;
    Reductions_Edit.Close;
    Reductions_Edit.SQL.Clear;
    Reductions_Edit.SQL.Add('Select first 0 * from credit_reductions');
    Reductions_Edit.Open;
    Reductions_Edit.Append;
    Reductions_Edit.FieldByName('CompanyId').AsInteger     := UniMainModule.Company_id;
    Reductions_Edit.FieldByName('Credit_Id').AsInteger     := CreditFileId;
    Open_reference_tables;
    Result := True;
  Except
    Result := False;
  End;
end;

procedure TCreditReductionAddFrm.LinkedLanguageCreditReductinAddChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditReductionAddFrm.Open_reference_tables;
var SQL: string;
begin
  ReductionTypes.Close;
  ReductionTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2018 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       EditReductionType.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       EditReductionType.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       EditReductionType.ListField := 'ENGLISH';
     end;
  end;
  ReductionTypes.SQL.Add(SQL);
  ReductionTypes.Open;
end;

procedure TCreditReductionAddFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditReductionAddFrm.UpdateStrings;
begin
  str_add_reduction        := LinkedLanguageCreditReductinAdd.GetTextOrDefault('strstr_add_reduction' (* 'Toevoegen cross selling korting' *) );
  str_change_reduction     := LinkedLanguageCreditReductinAdd.GetTextOrDefault('strstr_change_reduction' (* 'Wijzigen cross selling korting' *) );
  str_error_saving         := LinkedLanguageCreditReductinAdd.GetTextOrDefault('strstr_error_saving' (* 'Fout bij het opslaan van de gegevens.' *) );
  str_type_required        := LinkedLanguageCreditReductinAdd.GetTextOrDefault('strstr_type_required' (* 'Het type korting is een verplichte ingave.' *) );
  str_percentage_required  := LinkedLanguageCreditReductinAdd.GetTextOrDefault('strstr_percentage_required' (* 'Het percentage is een verplichte ingave.' *) );
end;

end.
