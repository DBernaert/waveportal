unit CreditDocumentsPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniBasicGrid,
  uniDBGrid, uniButton, UniThemeButton, uniEdit, uniGUIBaseClasses, uniPanel,
  siComp, siLngLnk, Vcl.Menus, uniMainMenu;

var
	str_error_filenotfound:        string = 'Het document kon niet worden gevonden'; // TSI: Localized (Don't modify!)
	str_delete_document:           string = 'Wenst u het document te verwijderen?'; // TSI: Localized (Don't modify!)
	str_error_change_status:       string = 'Fout bij het aanpassen van de status'; // TSI: Localized (Don't modify!)
	str_error_deleting:            string = 'Fout bij het verwijderen'; // TSI: Localized (Don't modify!)
	str_credit_documents:          string = 'Kredietdocumenten'; // TSI: Localized (Don't modify!)
	str_reject_documents:          string = 'Afkeuren documenten'; // TSI: Localized (Don't modify!)
	str_confirm_reject_documents:  string = 'Wenst U de documenten af te keuren?'; // TSI: Localized (Don't modify!)
	str_error_rejecting_documents: string = 'Fout bij het afkeuren van de documenten!'; // TSI: Localized (Don't modify!)

type
  TCreditDocumentsPanelFrm = class(TUniFrame)
    VwDocItems: TIBCQuery;
    Ds_VwDocItems: TDataSource;
    ContainerDocItems: TUniContainerPanel;
    HeaderDocItems: TUniContainerPanel;
    HeaderRightDocItems: TUniContainerPanel;
    BtnEditDocItem: TUniThemeButton;
    BtnExportDocItem: TUniThemeButton;
    GridDocItems: TUniDBGrid;
    ContainerDocuments: TUniContainerPanel;
    HeaderDocuments: TUniContainerPanel;
    HeaderRightDocuments: TUniContainerPanel;
    BtnDeleteDocument: TUniThemeButton;
    BtnOpenDocument: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    GridDocuments: TUniDBGrid;
    VwDocItemsID: TLargeintField;
    VwDocItemsDESCRIPTION_DUTCH: TWideStringField;
    VwDocItemsDESCRIPTION_FRENCH: TWideStringField;
    VwDocItemsDESCRIPTION_ENGLISH: TWideStringField;
    VwDocItemsSYS_CREDIT_DOC_ITEM: TLargeintField;
    VwDocItemsPRESENT: TSmallintField;
    VwAttachments: TIBCQuery;
    VwAttachmentsID: TLargeintField;
    VwAttachmentsORIGINAL_NAME: TWideStringField;
    VwAttachmentsDESTINATION_NAME: TWideStringField;
    VwAttachmentsDESCRIPTION: TWideStringField;
    VwAttachmentsVISIBLE_PORTAL: TSmallintField;
    VwAttachmentsDATE_ENTERED: TDateTimeField;
    VwAttachmentsSTORAGE_TYPE: TSmallintField;
    VwAttachmentsTYPEDESCRIPTION: TWideStringField;
    VwAttachmentsOriginal_name_description: TStringField;
    Ds_VwAttachments: TDataSource;
    VwAttachmentsRECORD_ID: TLargeintField;
    BtnMarkValidated: TUniThemeButton;
    VwAttachmentsFOLDER: TWideStringField;
    VwAttachmentsSUBFOLDER: TWideStringField;
    LinkedLanguageCreditDocumentsPanel: TsiLangLinked;
    BtnViewDocument: TUniThemeButton;
    BtnMarkinvalid: TUniThemeButton;
    PopupMenuInvalid: TUniPopupMenu;
    BtnInvalidateDocuments: TUniMenuItem;
    BtnInvalidateDocumentsAndMail: TUniMenuItem;
    VwDocItemsVALIDATED: TSmallintField;
    VwDocItemsREJECTED: TSmallintField;
    VwDocItemsDummy: TStringField;
    VwDocItemsMODULE_ID: TIntegerField;
    procedure BtnEditDocItemClick(Sender: TObject);
    procedure BtnExportDocItemClick(Sender: TObject);
    procedure GridDocumentsColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure BtnExportListClick(Sender: TObject);
    procedure BtnOpenDocumentClick(Sender: TObject);
    procedure BtnDeleteDocumentClick(Sender: TObject);
    procedure BtnMarkValidatedClick(Sender: TObject);
    procedure BtnViewDocumentClick(Sender: TObject);
    procedure LinkedLanguageCreditDocumentsPanelChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure BtnMarkinvalidClick(Sender: TObject);
    procedure BtnInvalidateDocumentsClick(Sender: TObject);
    procedure BtnInvalidateDocumentsAndMailClick(Sender: TObject);
    procedure GridDocItemsDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
      Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
  private
    { Private declarations }
    ModuleId: integer;
    procedure CallBackEditCreditDocuments(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_credit_documents(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, CreditDocumentAdd, CreditDocumentsAdd, Main, LinkPage, Utils,
     AttachmentsAdd, ValidateCreditDocuments, ServerModule, System.IOUtils,
  MailAdd;

{ TCreditDocumentsPanelFrm }

procedure TCreditDocumentsPanelFrm.BtnDeleteDocumentClick(Sender: TObject);
begin
  if (not VwAttachments.Active) or (VwAttachments.fieldbyname('Id').isNull)
  then exit;

  if MainForm.WaveConfirmBlocking(UniMainModule.Company_name, str_delete_document)
  then begin
         UniMainModule.QBatch.Close;
         UniMainModule.QBatch.SQL.Clear;
         UniMainModule.QBatch.SQL.Add('Update Files_archive set removed=''1'' where id=' + VwAttachments.FieldByName('Id').AsString);
         UniMainModule.QBatch.ExecSQL;
         Try
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Commit;
           VwAttachments.Refresh;

           if VwAttachments.RecordCount = 0 then begin
             UniMainModule.QBatch.Close;
             UniMainModule.QBatch.SQL.Text := 'Update credit_doc_items set present=0, validated=0  where id=:Id';
             UniMainModule.QBatch.ParamByName('Id').AsInteger := VwDocItems.FieldByName('Id').AsInteger;
             UniMainModule.QBatch.ExecSQL;
             Try
               if UniMainModule.UpdTr_Batch.Active then
                 UniMainModule.UpdTr_Batch.Commit;
               UniMainModule.QBatch.Close;
               VwDocItems.Refresh;
               //MainForm.Check_warnings_credit_documents;
             Except
               if UniMainModule.UpdTr_Batch.Active then
                 UniMainModule.UpdTr_Batch.Rollback;
               UniMainModule.QBatch.Close;
               MainForm.WaveShowErrorToast(str_error_change_status);
             End;
           end;
         Except
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Rollback;
           MainForm.WaveShowErrorToast(str_error_deleting);
           Exit;
         End;
       end;
end;

procedure TCreditDocumentsPanelFrm.BtnEditDocItemClick(Sender: TObject);
begin
  if VwDocItems.MasterSource.DataSet.FieldByName('Id').isNull
  then exit;

  With CreditDocumentsAddFrm
  do begin
       if initialize_credit_documents_add(ModuleId, VwDocItems.MasterSource.DataSet.FieldByName('Id').AsInteger)
       then ShowModal(CallBackEditCreditDocuments)
       else Close;
     end;
end;

procedure TCreditDocumentsPanelFrm.BtnExportDocItemClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridDocItems, str_credit_documents, VwDocItems, 'Id');
end;

procedure TCreditDocumentsPanelFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridDocuments, str_credit_documents, VwAttachments, 'Id');
end;

procedure TCreditDocumentsPanelFrm.BtnInvalidateDocumentsAndMailClick(
  Sender: TObject);
var ContactId: longint;
    ReasonRejected: string;
begin
  if (VwDocItems.Active = False) or (VwDocItems.FieldByName('Id').IsNull)
  then exit;

  if (VwDocItems.FieldByName('Present').AsInteger = 0) and
     (VwAttachments.RecordCount = 0)
  then exit;

  UniMainModule.QWork.Close;

  UniMainModule.QWork.SQL.Text :=
  'Select first 1 * from credit_file_demanders where demander_type=0 and credit_file=:credit_file ' +
  'order by id';

  UniMainModule.QWork.ParamByName('Credit_file').AsInteger :=
  VwDocItems.MasterSource.DataSet.FieldByName('Id').AsInteger;

  UniMainModule.QWork.Open;

  ContactId := UniMainModule.QWork.FieldByName('Demander_id').AsInteger;

  UniMainModule.QWork.Close;

  if MainForm.WaveConfirmBlocking(str_reject_documents, str_confirm_reject_documents)
  then begin
         //Verwijderen van de bijlagen
         VwAttachments.First;
         while not VwAttachments.Eof
         do begin
              UniMainModule.QBatch.Close;
              UniMainModule.QBatch.SQL.Clear;
              UniMainModule.QBatch.SQL.Add('Update Files_archive set removed=''1'' where id=' + VwAttachments.FieldByName('Id').AsString);
              UniMainModule.QBatch.ExecSQL;
              VwAttachments.Next;
            end;

         ReasonRejected := '';

         if MainForm.WaveInputText('Reden afkeuring', 'Reden van de afkeuring:', '')
         then ReasonRejected :=  Trim(MainForm.SweetAlert.InputResult);

         //Markeren van de lijn als niet aanwezig
         UniMainModule.QBatch.Close;
         UniMainModule.QBatch.SQL.Clear;
         UniMainModule.QBatch.SQL.Add('Update credit_doc_items set present=:Present, Validated=:Validated, Reason_rejected=:reason_rejected where Id=:Id');
         UniMainModule.QBatch.ParamByName('Present').AsInteger   := 0;
         UniMainModule.QBatch.ParamByName('Validated').AsInteger := 0;
         UniMainModule.QBatch.ParamByName('Reason_rejected').AsString := ReasonRejected;
         UniMainModule.QBatch.ParamByName('Id').AsInteger        := VwDocItems.FieldByName('Id').AsInteger;
         UniMainModule.QBatch.ExecSQL;
         UniMainModule.QBatch.Close;

         Try
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Commit;
           VwDocItems.Refresh;
           VwAttachments.Refresh;

           With MailAddFrm
           do begin
                if Start_mail_module(2, ContactId, 0, '', 8002)
                then ShowModal
                else Close;
              end;
         Except
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Rollback;
           MainForm.WaveShowErrorToast(str_error_rejecting_documents);
         End;
       end;
end;

procedure TCreditDocumentsPanelFrm.BtnInvalidateDocumentsClick(Sender: TObject);
var ReasonRejected: string;
begin
  if (VwDocItems.Active = False) or (VwDocItems.FieldByName('Id').IsNull)
  then exit;

  if (VwDocItems.FieldByName('Present').AsInteger = 0) and
     (VwAttachments.RecordCount = 0)
  then exit;

  if MainForm.WaveConfirmBlocking(str_reject_documents, str_confirm_reject_documents)
  then begin
         //Verwijderen van de bijlagen
         VwAttachments.First;
         while not VwAttachments.Eof
         do begin
              UniMainModule.QBatch.Close;
              UniMainModule.QBatch.SQL.Clear;
              UniMainModule.QBatch.SQL.Add('Update Files_archive set removed=''1'' where id=' + VwAttachments.FieldByName('Id').AsString);
              UniMainModule.QBatch.ExecSQL;
              VwAttachments.Next;
            end;

         ReasonRejected := '';

         if MainForm.WaveInputText('Reden afkeuring', 'Reden van de afkeuring:', '')
         then ReasonRejected :=  Trim(MainForm.SweetAlert.InputResult);

         //Markeren van de lijn als niet aanwezig
         UniMainModule.QBatch.Close;
         UniMainModule.QBatch.SQL.Text :=
         'Update credit_doc_items set present=:Present, Validated=:Validated, ' +
         'Rejected=:Rejected, Reason_rejected=:Reason_rejected where Id=:Id';

         UniMainModule.QBatch.ParamByName('Present').AsInteger   := 0;
         UniMainModule.QBatch.ParamByName('Validated').AsInteger := 0;
         UniMainModule.QBatch.ParamByName('Rejected').AsInteger  := 1;
         UniMainModule.QBatch.ParamByName('Reason_rejected').AsString := ReasonRejected;

         UniMainModule.QBatch.ParamByName('Id').AsInteger        := VwDocItems.FieldByName('Id').AsInteger;
         UniMainModule.QBatch.ExecSQL;
         UniMainModule.QBatch.Close;

         Try
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Commit;
           VwDocItems.Refresh;
           VwAttachments.Refresh;
         Except
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Rollback;
           MainForm.WaveShowErrorToast(str_error_rejecting_documents);
         End;
       end;
end;

procedure TCreditDocumentsPanelFrm.BtnMarkinvalidClick(Sender: TObject);
begin
  PopupMenuInvalid.PopupBy(BtnMarkInvalid);
end;

procedure TCreditDocumentsPanelFrm.BtnMarkValidatedClick(Sender: TObject);
var Description: string;
begin
  if (VwDocItems.Active = False) or (VwDocItems.FieldByName('Id').IsNull) then
    exit;

  if VwAttachments.RecordCount = 0
  then exit;

  Case UniMainModule.LanguageManager.ActiveLanguage of
  1:   Description := VwDocItems.FieldByName('Description_dutch').asString;
  2:   Description := VwDocItems.FieldByName('Description_french').asString;
  3:   Description := VwDocItems.FieldByName('Description_english').asString;
  else Description := VwDocItems.FieldByName('Description_dutch').asString;
  end;

  With ValidateCreditDocumentsFrm
  do begin
       Init_credit_document_validation(VwDocItems.FieldByName('Id').AsInteger,
                                       VwDocItems.MasterSource.DataSet.FieldByName('Id').AsInteger,
                                       VwDocItems.FieldByName('Module_id').AsInteger,
                                       Description);
       ShowModal(CallBackEditCreditDocuments);
     end;
end;

procedure TCreditDocumentsPanelFrm.BtnOpenDocumentClick(Sender: TObject);
var DestinationName: string;
begin
  if ((VwAttachments.Active) and (not VwAttachments.fieldbyname('Id').isNull))
  then begin
         DestinationName := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
         if Trim(VwAttachments.FieldByName('Folder').AsString) <> ''
         then DestinationName := DestinationName + VwAttachments.FieldByName('Folder').AsString + '\';
         if Trim(VwAttachments.FieldByname('SubFolder').AsString) <> ''
         then DestinationName := DestinationName + VwAttachments.FieldByName('SubFolder').AsString + '\';
         DestinationName := DestinationName + VwAttachments.FieldByName('Destination_name').AsString;

         if VwAttachments.FieldByName('Storage_type').AsInteger = 1
         then UniSession.AddJS('window.open(' + QuotedStr(DestinationName) + ', ''_blank'')')
         else if VwAttachments.FieldByName('Storage_type').AsInteger = 2 //Eurofinco
              then begin
                     With LinkPageFrm
                     do begin
                          Initialize_linkpage(VwAttachments.FieldByName('Destination_name').asString);
                          ShowModal;
                        end;
                   end
              else begin
                     if FileExists(DestinationName)
                     then UniSession.SendFile(DestinationName, Utils.EscapeIllegalChars(VwAttachments.fieldbyname('Original_Name').asString))
                     else MainForm.WaveShowErrorToast(str_error_filenotfound);
                   end;
       end;
end;

procedure TCreditDocumentsPanelFrm.BtnViewDocumentClick(Sender: TObject);
var Extension:   string;
    Imagefile:   string;
    Pdffile:     string;
    Officefile:  string;
    AUrl:        string;
    Destination: string;
begin
  if GridDocuments.SelectedRows.Count > 1
  then exit;

  if ((VwAttachments.Active) and (not VwAttachments.fieldbyname('Id').isNull)) then begin
    if VwAttachments.FieldByName('Storage_type').AsInteger = 0
    then begin
           Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
           if Trim(VwAttachments.FieldByName('Folder').AsString) <> ''
           then Destination := Destination + VwAttachments.FieldByName('Folder').AsString + '\';
           if Trim(VwAttachments.FieldByName('SubFolder').AsString) <> ''
           then Destination := Destination + VwAttachments.FieldByName('SubFolder').AsString + '\';
           Destination := Destination + VwAttachments.FieldByName('Destination_Name').AsString;
           if FileExists(Destination)
           then begin
                  Extension := UpperCase(ExtractFileExt(VwAttachments.FieldByName('Original_name').AsString));

                  if (Extension = '.PNG') or (Extension = '.JPG') or
                     (Extension = '.GIF') or (Extension = '.JPEG')
                  then begin
                         ImageFile := UniServerModule.NewCacheFileUrl(False, copy(Extension, 2, length(Extension)),
                                                                      Utils.EscapeIllegalChars(TPath.GetFileNameWithoutExtension(VwAttachments.FieldByName('Original_name').AsString)),
                                                                      '', AUrl, True);

                         if CopyFile(PChar(Destination), PChar(ImageFile), False)
                         then UniSession.AddJS('window.open(''' + AUrl + ''', ''_blank'');');
                       end
                  else if (Extension = '.PDF')
                       then begin
                              Pdffile := UniServerModule.NewCacheFileUrl(False, copy(Extension, 2, length(Extension)),
                                                                         Utils.EscapeIllegalChars(TPath.GetFileNameWithoutExtension(VwAttachments.FieldByName('Original_name').AsString)),
                                                                         '', AUrl, True);

                              if CopyFile(PChar(Destination), PChar(Pdffile), False)
                              then UniSession.AddJS('window.open(''' + AUrl + ''', ''_blank'');');
                            end
                       else if (Extension = '.DOCX') or (Extension = '.XLSX') or
                               (Extension = '.DOC')  or (Extension = '.XLS')
                            then begin
                                   OfficeFile := 'c:\inetpub\wwwroot\Shared_doc\' +
                                                 VwAttachments.FieldByName('Destination_name').AsString +
                                                 Extension;

                                   if CopyFile(PChar(Destination), PChar(OfficeFile), False)
                                   then UniSession.AddJS('window.open(''' + 'https://view.officeapps.live.com/op/embed.aspx?src=' +
                                                         'https://wavedesk.cloud/Shared_doc/' + ExtractFileName(OfficeFile) +
                                                         ''', ''_blank'');');
                                 end
                            else BtnOpenDocumentClick(nil);
                end
           else MainForm.WaveShowErrorToast(str_error_filenotfound);
         end
    else BtnOpenDocumentClick(nil);
  end;
end;

procedure TCreditDocumentsPanelFrm.CallBackEditCreditDocuments(Sender: TComponent; AResult: Integer);
begin
  if VwDocItems.Active then
    VwDocItems.Refresh;
end;

procedure TCreditDocumentsPanelFrm.Start_credit_documents(IdModule: integer;
  MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;
  VwDocItems.Close;
  VwDocItems.SQL.Clear;
  SQL := 'select ' +
         'credit_doc_items.id, sys_credit_doc_items.description_dutch, ' +
         'sys_credit_doc_items.description_french, sys_credit_doc_items.description_english, ' +
         'credit_doc_items.sys_credit_doc_item, credit_doc_items.present, ' +
         'credit_doc_items.validated, credit_doc_items.rejected, credit_doc_items.module_id ' +
         'from credit_doc_items ' +
         'left outer join sys_credit_doc_items on (credit_doc_items.sys_credit_doc_item = sys_credit_doc_items.id) ';

  SQL := SQL +
         'where credit_doc_items.companyId=:CompanyId ' +
         'and credit_doc_items.Module_id=:ModuleId ';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + 'order by sys_credit_doc_items.description_dutch';
       GridDocItems.Columns[0].FieldName := 'Description_dutch';
     end;
  2: begin
       SQL := SQL + 'order by sys_credit_doc_items.description_french';
       GridDocItems.Columns[0].FieldName := 'Description_french';
     end;
  3: begin
       SQL := SQL + 'order by sys_credit_doc_items.description_english';
       GridDocItems.Columns[0].FieldName := 'Description_english';
     end;
  end;

  VwDocItems.SQL.Add(SQL);
  VwDocItems.ParamByName('CompanyId').AsInteger := UniMainModule.Company_id;
  VwDocItems.ParamByName('ModuleId').AsInteger  := IdModule;
  VwDocItems.MasterSource := MasterDs;
  VwDocItems.MasterFields := 'ID';
  VwDocItems.DetailFields := 'RECORD_ID';
  VwDocItems.Open;

  VwAttachments.Close;
  VwAttachments.SQL.Clear;
  SQL := 'select files_archive.id, files_archive.record_id, files_archive.original_name, files_archive.destination_name, ' +
         'files_archive.description, files_archive.visible_portal, ' +
         'files_archive.date_entered, files_archive.folder, files_archive.subfolder, storage_type, ';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: SQL := SQL + 'basictables.dutch typedescription ';
  2: SQL := SQL + 'basictables.french typedescription ';
  3: SQL := SQL + 'basictables.english typedescription ';
  end;

  SQL := SQL +
         'from files_archive ' +
         'left outer join basictables on (files_archive.file_type = basictables.id) ' +
         'where files_archive.companyId=' + IntToStr(UniMainModule.Company_id) +
         ' and Module_id=100' +
         ' and files_archive.removed=''0''';
  SQL := SQL + ' order by files_archive.original_name';
  VwAttachments.SQL.Add(SQL);
  VwAttachments.Open;
end;

procedure TCreditDocumentsPanelFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditDocumentsPanelFrm.UpdateStrings;
begin
  str_error_rejecting_documents := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_error_rejecting_documents' (* 'Fout bij het afkeuren van de documenten!' *) );
  str_confirm_reject_documents  := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_confirm_reject_documents' (* 'Wenst U de documenten af te keuren?' *) );
  str_reject_documents          := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_reject_documents' (* 'Afkeuren documenten' *) );
  str_credit_documents          := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_credit_documents' (* 'Kredietdocumenten' *) );
  str_error_deleting            := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_error_deleting' (* 'Fout bij het verwijderen' *) );
  str_error_change_status       := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_error_change_status' (* 'Fout bij het aanpassen van de status' *) );
  str_delete_document           := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_delete_document' (* 'Wenst u het document te verwijderen?' *) );
  str_error_filenotfound        := LinkedLanguageCreditDocumentsPanel.GetTextOrDefault('strstr_error_filenotfound' (* 'Het document kon niet worden gevonden' *) );
end;

procedure TCreditDocumentsPanelFrm.GridDocItemsDrawColumnCell(Sender: TObject;
  ACol, ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if Column.Index = 1
  then begin
         if VwDocItems.FieldByName('Present').AsInteger = 0
         then Attribs.Color := ClRed;

         if VwDocItems.FieldByName('Rejected').AsInteger = 1
         then begin
                Attribs.Color := ClRed;
                Exit;
              end;

         if VwDocItems.FieldByName('Present').AsInteger = 1
         then Attribs.Color := $004080FF;

         if VwDocItems.FieldByName('Validated').AsInteger = 1
         then Attribs.Color := ClGreen;
       end;
end;

procedure TCreditDocumentsPanelFrm.GridDocumentsColumnSort(
  Column: TUniDBGridColumn; Direction: Boolean);
var OrderStr: string;
begin
  OrderStr := Column.FieldName;
  If Direction
  then OrderStr := OrderStr + ' ASC,'
  else OrderStr := OrderStr + ' DESC,';
  (GridDocuments.DataSource.DataSet as TIBCQuery).IndexFieldNames := orderStr;
end;

procedure TCreditDocumentsPanelFrm.LinkedLanguageCreditDocumentsPanelChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

end.


