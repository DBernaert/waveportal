unit AgendaMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniGUIBaseClasses, uniPanel,
  uniImage, uniHTMLFrame, uniButton, UniThemeButton, siComp, siLngLnk,
  uUniSyncFusionResourceCalendarPanelStandard;

var
	str_error_adding:         string = 'Fout bij het toevoegen'; // TSI: Localized (Don't modify!)
	str_free_block:           string = 'Vrij blok'; // TSI: Localized (Don't modify!)
	str_error_editing:        string = 'Fout bij het wijzigen'; // TSI: Localized (Don't modify!)
	str_delete_agenda:        string = 'Verwijderen agenda'; // TSI: Localized (Don't modify!)
	str_delete_item:          string = 'Verwijderen item '; // TSI: Localized (Don't modify!)
	str_error_resizing:       string = 'Fout bij het aanpassen'; // TSI: Localized (Don't modify!)
	str_error_moving:         string = 'Fout bij het verplaatsen'; // TSI: Localized (Don't modify!)

	str_title:                string = 'Titel: '; // TSI: Localized (Don't modify!)
	str_description:          string = 'Omschrijving: '; // TSI: Localized (Don't modify!)
	str_from:                 string = 'Van: '; // TSI: Localized (Don't modify!)
	str_to:                   string = 'Tot: '; // TSI: Localized (Don't modify!)
	str_responsable:          string = 'Verantwoordelijke: '; // TSI: Localized (Don't modify!)
	str_add:                  string = 'Toevoegen...'; // TSI: Localized (Don't modify!)
	str_modify:               string = 'Wijzigen...'; // TSI: Localized (Don't modify!)
	str_delete:               string = 'Verwijderen...'; // TSI: Localized (Don't modify!)
	str_to_be_confirmed:      string = 'Te bestigen afspraak'; // TSI: Localized (Don't modify!)
	str_confirmed:            string = 'Bevestigde afspraak'; // TSI: Localized (Don't modify!)

type
  TAgendaMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    ContainerFooterCalendar: TUniContainerPanel;
    ContainerFreeBlocsColor: TUniContainerPanel;
    LblFreeBlocs: TUniLabel;
    SyncCalendarPanelAgenda: TUniSyncFusionResourceCalendarPanelStandard;
    LinkedLanguageAgendaMainn: TsiLangLinked;
    BtnAddMeeting: TUniThemeButton;
    ContainerToConfirmBlocsColor: TUniContainerPanel;
    LblToConfirm: TUniLabel;
    ContainerConfirmedBlocsColor: TUniContainerPanel;
    LblConfirmed: TUniLabel;
    procedure UniFrameAjaxEvent(Sender: TComponent; EventName: string;
      Params: TUniStrings);
    procedure UniFrameCreate(Sender: TObject);
    procedure SyncCalendarPanelAgendaAjaxEvent(Sender: TComponent;
      EventName: string; Params: TUniStrings);
    procedure BtnAddMeetingClick(Sender: TObject);
    procedure LinkedLanguageAgendaMainnChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    procedure Load_calendar;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses MainModule, MeetingAdd, Main, DateUtils, MeetingBlocAdd;



{ TAgendaMainFrm }

procedure TAgendaMainFrm.BtnAddMeetingClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  with MeetingAddFrm
  do begin
       if Meeting_insert(Now, IncHour(Now)) = True
       then ShowModal(
              procedure (Sender: TComponent; ARes: Integer)
              begin
                Load_calendar;
              end)
       else MainForm.WaveShowWarningMessage(str_error_adding);
     end;
end;

procedure TAgendaMainFrm.LinkedLanguageAgendaMainnChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TAgendaMainFrm.Load_calendar;
var JSonStr:     string;
    SQL:         string;
    startyear, startmonth, startday, starthour, startminutes, endyear, endmonth, endday, endhour, endminutes: integer;
    description: string;
    subject:     string;
begin
  UniMainModule.QWork2.SQL.Clear;
  SQL := 'Select crm_meetings.id, crm_meetings.title, meeting_start, meeting_end, meeting_status, description, ' +
         'crm_contacts.last_name, crm_contacts.first_name, crm_meetings.responsable_id, users.name, users.firstname, ' +
         'crm_meetings.location, crm_meetings.bloc_contact_name, ' +
         'credit_contributors.last_name contributor_lastname, ' +
         'credit_contributors.first_name contributor_firstname ' +
         'from credit_contributors ' +
         'right outer join crm_meetings on (credit_contributors.id = crm_meetings.crm_contributor) ' +
         'left outer join users on (crm_meetings.responsable_id = users.id) ' +
         'left outer join crm_contacts on (crm_meetings.crm_contact = crm_contacts.id) ' +
         'where crm_meetings.companyid=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and crm_meetings.removed = ''0'' ' +
         'and crm_meetings.meeting_start > ' + QuotedStr(FormatDateTime('yyyy-mm-dd', IncYear(Now, -2))) +
         'and ((crm_meetings.responsable_type = 2 and crm_meetings.responsable_id=' + IntToStr(UniMainModule.User_id) + ') or ' +
         '(crm_meetings.responsable_type = 1 and crm_contributor=' + IntToStr(UniMainModule.User_id) + ') or ' +
         '(crm_meetings.responsable_type = 1 and crm_meetings.meeting_status = 0)) ' +
         'order by crm_meetings.responsable_id';

  UniMainModule.QWork2.SQL.Add(SQL);
  UniMainModule.QWork2.Open;
  UniMainModule.QWork2.First;
  while not UniMainModule.QWork2.Eof
  do begin
       startyear    := StrToInt(FormatDateTime('yyyy', UniMainModule.QWork2.FieldByName('Meeting_start').AsDateTime));
       startmonth   := StrToInt(FormatDateTime('mm',   UniMainModule.QWork2.FieldByName('Meeting_start').AsDateTime));
       startday     := StrToInt(FormatDateTime('dd',   UniMainModule.QWork2.FieldByName('Meeting_start').AsDateTime));
       starthour    := StrToInt(FormatDatetime('hh',   UniMainModule.QWork2.FieldByName('Meeting_start').AsDateTime));
       startminutes := StrToInt(FormatDatetime('nn',   UniMainModule.QWork2.FieldByName('Meeting_start').AsDateTime));
       endyear      := StrToInt(FormatDateTime('yyyy', UniMainModule.QWork2.FieldByName('Meeting_end').AsDateTime));
       endmonth     := StrToInt(FormatDateTime('mm',   UniMainModule.QWork2.FieldByName('Meeting_end').AsDateTime));
       endday       := StrToInt(FormatDatetime('dd',   UniMainModule.QWork2.FieldByName('Meeting_end').AsDateTime));
       endhour      := StrToInt(FormatDateTime('hh',   UniMainModule.QWork2.FieldByName('Meeting_end').AsDateTime));
       endminutes   := StrToInt(FormatDateTime('nn',   UniMainModule.QWork2.FieldByName('Meeting_end').AsDateTime));

       if UniMainModule.QWork2.FieldByName('Meeting_status').AsInteger = 1
       then begin
              description  := TrimRight(UniMainModule.QWork2.FieldByName('Description').AsString).Replace('"', '\"');
              description  := StringReplace(description, #13#10, '<br>', [rfReplaceAll]);
              if description = ''
              then description := '--';
            end
       else description := '--';

       case UniMainModule.QWork2.FieldByName('Meeting_status').AsInteger of
         0:   begin
                Subject      := Trim(UniMainModule.QWork2.FieldByName('Contributor_lastName').AsString + ' ' +
                                     UniMainModule.QWork2.FieldByName('Contributor_firstname').asString);
                if Trim(Subject) = ''
                then Subject := str_free_block;
                if Trim(UniMainModule.QWork2.FieldByName('Location').AsString) <> ''
                then Subject := Subject + ' - ' + UniMainModule.QWork2.FieldByName('Location').AsString;
                Subject      := TrimRight(Subject).Replace('"', '\"');
              end;
         2:   begin
                Subject      := UniMainModule.QWork2.FieldByName('Bloc_contact_name').AsString;
                //Subject      := Trim(UniMainModule.QWork2.FieldByName('Contributor_lastName').AsString + ' ' +
                //                     UniMainModule.QWork2.FieldByName('Contributor_firstname').asString);
                if Trim(Subject) = ''
                then Subject := str_to_be_confirmed;
                if Trim(UniMainModule.QWork2.FieldByName('Location').AsString) <> ''
                then Subject := Subject + ' - ' + UniMainModule.QWork2.FieldByName('Location').AsString;
                Subject      := TrimRight(Subject).Replace('"', '\"');

              end;
         3:   begin
                Subject      := UniMainModule.QWork2.FieldByName('Bloc_contact_name').AsString;
                             // Trim(UniMainModule.QWork2.FieldByName('Contributor_lastName').AsString + ' ' +
                             //        UniMainModule.QWork2.FieldByName('Contributor_firstname').asString);
                if Trim(Subject) = ''
                then Subject := str_confirmed;
                if Trim(UniMainModule.QWork2.FieldByName('Location').AsString) <> ''
                then Subject := Subject + ' - ' + UniMainModule.QWork2.FieldByName('Location').AsString;
                Subject      := TrimRight(Subject).Replace('"', '\"');
              end;
         else begin
                subject      := Trim(UniMainModule.QWork2.FieldByName('Last_name').AsString + ' ' + UniMainModule.QWork2.FieldByName('First_name').AsString);
                if Trim(subject) = ''
                then subject := UniMainModule.QWork2.FieldByName('Title').AsString
                else subject := subject + ' - ' + UniMainModule.QWork2.FieldByName('Title').AsString;
                Subject      := TrimRight(Subject).Replace('"', '\"');
              end;
       end;

       JSONStr := JSONStr + '{'+
        '"Subject": "' + subject
        + '",' +
        '"Id": ' + UniMainModule.QWork2.FieldByName('Id').AsString + ',' +
        'StartTime: new Date('+ IntToStr(Startyear)      + ', ' +
                                IntToStr(Startmonth - 1) + ', ' +
                                IntToStr(Startday)       + ', ' +
                                IntToStr(Starthour)      + ', ' +
                                IntToStr(Startminutes)   + '),' +
        'EndTime: new Date('  + IntToStr(Endyear)        + ', ' +
                                IntToStr(Endmonth - 1)   + ', ' +
                                IntToStr(Endday)         + ', ' +
                                IntToStr(Endhour)        + ', ' +
                                IntToStr(Endminutes)     + '),' +
        'isAllDay: false,'+
        'Description: "'  + description  + '",' +
        'ResourceName: "' + Trim(UniMainModule.QWork2.FieldByName('Name').AsString + ' ' + UniMainModule.QWork2.FieldByName('FirstName').AsString) + '",';
        case UniMainModule.QWork2.FieldByName('Meeting_status').AsInteger of
          0:   JSONStr := JSONStr + 'ResourceId: -1';
          2:   JSonStr := JSonStr + 'ResourceId: -2';
          3:   JSonStr := JSonStr + 'ResourceId: -3';
          else JSONStr := JSONStr + 'ResourceId: ' + UniMainModule.Qwork2.Fieldbyname('Responsable_id').AsString;
        end;
        JSONStr := JSONStr + '},';
      UniMainModule.Qwork2.Next;
     end;
  UniMainModule.QWork2.Close;

  if JSONStr <> ''
  then JSONStr := Copy(JSONStr, 1, Length(JSONStr)-1);
  JSONStr := '[' + JSONStr + ']';
  SyncCalendarPanelAgenda.JSONData := JSONStr;
end;

procedure TAgendaMainFrm.SyncCalendarPanelAgendaAjaxEvent(
  Sender: TComponent; EventName: string; Params: TUniStrings);
var
  BTitle:               String;
  BDescription:         String;
  BResourceName:        String;
  newDataID:            Integer;
  newStartTimeYear:     Word;
  newStartTimeMonth:    Word;
  newStartTimeDay:      Word;
  newStartTimeHours:    Word;
  newStartTimeMinutes:  Word;
  newEndTimeYear:       Word;
  newEndTimeMonth:      Word;
  newEndTimeDay:        Word;
  newEndTimeHours:      Word;
  newEndTimeMinutes:    Word;
  AStartDate, AEndDate: TDateTime;
  TimeBetween:          TTime;
  SQL:                  String;
  ViewOnly:             boolean;

  sYear, sMonth, sDay, sHours, sMinutes, sSec, sMSec: Word;
  eYear, eMonth, eDay, eHours, eMinutes, eSec, eMSec: Word;
begin
  if EventName = 'addEvent'
  then begin
         newStartTimeYear    := Params.Values['StartTimeYear'].ToInteger();
         newStartTimeMonth   := Params.Values['StartTimeMonth'].ToInteger();
         newStartTimeDay     := Params.Values['StartTimeDay'].ToInteger();
         newStartTimeHours   := Params.Values['StartTimeHours'].ToInteger();
         newStartTimeMinutes := Params.Values['StartTimeMinutes'].ToInteger();

         newEndTimeYear      := Params.Values['EndTimeYear'].ToInteger();
         newEndTimeMonth     := Params.Values['EndTimeMonth'].ToInteger();
         newEndTimeDay       := Params.Values['EndTimeDay'].ToInteger();
         newEndTimeHours     := Params.Values['EndTimeHours'].ToInteger();
         newEndTimeMinutes   := Params.Values['EndTimeMinutes'].ToInteger();

         AStartDate := EncodeDateTime(newStartTimeYear, newStartTimeMonth+1, newStartTimeDay, newStartTimeHours, newStartTimeMinutes, 0, 0);
         if newStartTimeHours = 23
         then AEndDate := EncodeDateTime(newEndTimeYear, newEndTimeMonth+1, newEndTimeDay-1, 23, 59, 59, 999)
         else AEndDate := EncodeDateTime(newEndTimeYear, newEndTimeMonth+1, newEndTimeDay, newEndTimeHours, newEndTimeMinutes, 0, 0);

         UniMainModule.Result_dbAction := 0;
         with MeetingAddFrm
         do begin
              if Meeting_insert(AStartDate, AEndDate) = True
              then ShowModal(
                       procedure (Sender: TComponent; ARes: Integer)
                       begin
                         Load_calendar;
                       end)
              else MainForm.WaveShowWarningMessage(str_error_adding);
            end;
       end
  else if EventName = 'editEvent'
       then begin
              UniMainModule.Result_dbAction := 0;
              newDataID                     := Params.Values['Id'].ToInteger();

              UniMainModule.QWork.Close;
              UniMainModule.QWork.SQL.Text := 'Select meeting_status from crm_meetings where id=:Id';
              UniMainModule.QWork.ParamByName('Id').AsInteger := NewDataId;
              UniMainModule.QWork.Open;
              if (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 0) or
                 (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 2) or
                 (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 3)
              then begin
                     if UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 3
                     then ViewOnly := True
                     else ViewOnly := False;
                     UniMainModule.QWork.Close;

                     with MeetingBlocAddFrm
                     do begin
                          if Meetingbloc_edit(newDataId, ViewOnly)
                          then begin
                                 ShowModal(
                                 procedure (Sender: TComponent; ARes: Integer)
                                 begin
                                   if UniMainModule.Result_dbAction <> 0
                                   then Load_calendar;
                                 end);
                               end
                          else begin
                                 MainForm.WaveShowErrorMessage(str_error_editing);
                                 Close;
                               end;
                        end;
                   end
              else begin
                     with MeetingAddFrm
                     do begin
                          if Meeting_edit(newDataId)
                          then begin
                                 ShowModal(
                                 procedure (Sender: TComponent; ARes: Integer)
                                 begin
                                   if UniMainModule.Result_dbAction <> 0
                                   then Load_calendar;
                                 end);
                               end
                          else begin
                                 MainForm.WaveShowErrorMessage(str_error_editing);
                                 Close;
                               end;
                        end;
                   end;
            end
       else if EventName = 'deleteEvent'
            then begin
                   UniMainModule.QWork.Close;
                   UniMainModule.QWork.SQL.Text := 'Select meeting_status from crm_meetings where id=:Id';
                   UniMainModule.QWork.ParamByName('Id').AsInteger := Params.Values['Id'].ToInteger();
                   UniMainModule.QWork.Open;
                   if (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 0) or
                      (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 2) or
                      (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 3)
                   then begin
                          UniMainModule.QWork.Close;
                          Exit;
                        end
                   else UniMainModule.QWork.Close;

                   newDataID     := Params.Values['Id'].ToInteger();
                   BTitle        := Params.Values['Subject'];
                   BDescription  := Params.Values['Description'];
                   BResourceName := Params.Values['ResourceName'];

                   if MainForm.WaveConfirmBlocking(str_delete_agenda, str_delete_item + Btitle + ' ?')
                   then begin
                          if UniMainModule.ExecuteBatch('update crm_meetings set removed=''1'' where id=' + IntToStr(newDataID))
                          then SyncCalendarPanelAgenda.JSInterface.JSCode(#1'.scheduleObj.deleteEvent('#1'.scheduleObj.eventObj);');
                        end;
                 end
            else if EventName = 'resizeEvent'
                 then begin
                        if Params.Values['Id'].ToInteger = 0
                        then exit;

                        UniMainModule.QWork.Close;
                        UniMainModule.QWork.SQL.Text := 'Select meeting_status from crm_meetings where id=:Id';
                        UniMainModule.QWork.ParamByName('Id').AsInteger := Params.Values['Id'].ToInteger();
                        UniMainModule.QWork.Open;
                        if (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 0) or
                           (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 2) or
                           (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 3)
                        then begin
                               UniMainModule.QWork.Close;
                               Exit;
                              end
                        else UniMainModule.QWork.Close;

                        newDataID              := Params.Values['Id'].ToInteger();
                        newStartTimeYear       := Params.Values['startYear'].ToInteger();
                        newStartTimeMonth      := Params.Values['startMonth'].ToInteger() + 1;
                        newStartTimeDay        := Params.Values['startDay'].ToInteger();
                        newStartTimeHours      := Params.Values['startHours'].ToInteger();
                        newStartTimeMinutes    := Params.Values['startMinutes'].ToInteger();
                        AStartDate             := EnCodeDateTime(newStartTimeYear, newStartTimeMonth, newStartTimeDay, newStartTimeHours, newStartTimeMinutes, 0, 0);

                        newEndTimeYear         := Params.Values['endYear'].ToInteger();
                        newEndTimeMonth        := Params.Values['endMonth'].ToInteger() + 1;
                        newEndTimeDay          := Params.Values['endDay'].ToInteger();
                        newEndTimeHours        := Params.Values['endHours'].ToInteger();
                        newEndTimeMinutes      := Params.Values['endMinutes'].ToInteger();
                        AEndDate               := EnCodeDateTime(newEndTimeYear, newEndTimeMonth, newEndTimeDay, newEndTimeHours, newEndTimeMinutes, 0, 0);

                        SQL :=
                        'update crm_meetings set meeting_start=' +
                                                       QuotedStr(FormatDateTime('yyyy-mm-dd hh:mm', AStartDate)) +
                                                       ', meeting_end=' +
                                                       QuotedStr(FormatDateTime('yyyy-mm-dd hh:mm', AEndDate)) +
                                                      'where id=' + intToStr(newDataID);

                        if UniMainModule.ExecuteBatch(SQL)
                        then Load_calendar
                        else MainForm.WaveShowErrorToast(str_error_resizing);
                      end
                 else if EventName = 'dropEvent'
                      then begin
                             if Params.Values['Id'].ToInteger = 0
                             then exit;

                             UniMainModule.QWork.Close;
                             UniMainModule.QWork.SQL.Text := 'Select meeting_status from crm_meetings where id=:Id';
                             UniMainModule.QWork.ParamByName('Id').AsInteger := Params.Values['Id'].ToInteger();
                             UniMainModule.QWork.Open;
                             if (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 0) or
                                (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 2) or
                                (UniMainModule.QWork.FieldByName('Meeting_status').AsInteger = 3)
                             then begin
                                    UniMainModule.QWork.Close;
                                    Exit;
                                  end
                             else UniMainModule.QWork.Close;

                             newDataID             := Params.Values['Id'].ToInteger();
                             newStartTimeYear      := Params.Values['newYear'].ToInteger();
                             newStartTimeMonth     := Params.Values['newMonth'].ToInteger() + 1;
                             newStartTimeDay       := Params.Values['newDay'].ToInteger();
                             newStartTimeHours     := Params.Values['newStartHours'].ToInteger();
                             newStartTimeMinutes   := Params.Values['newStartMinutes'].ToInteger();
                             AStartDate            := EncodeDateTime(newStartTimeYear, newStartTimeMonth, newStartTimeDay, newStartTimeHours, newStartTimeMinutes, 0, 0);

                             UniMainModule.QWork.Close;
                             UniMainModule.QWork.SQL.Clear;
                             UniMainModule.QWork.SQL.Add('Select meeting_start, meeting_end from crm_meetings where id=' + IntToStr(NewDataId));
                             UniMainModule.QWork.Open;
                             TimeBetween := UniMainModule.QWork.FieldByName('Meeting_end').AsDateTime - UniMainModule.QWork.FieldByName('Meeting_start').AsDateTime;
                             UniMainModule.QWork.Close;
                             AEndDate              := AStartDate + TimeBetween;
                             SQL :=
                             'update crm_meetings set meeting_start=' +
                                                       QuotedStr(FormatDateTime('yyyy-mm-dd hh:mm', AStartDate)) +
                                                       ', meeting_end=' +
                                                       QuotedStr(FormatDateTime('yyyy-mm-dd hh:mm', AEndDate)) +
                                                      'where id=' + intToStr(newDataID);

                             if UniMainModule.ExecuteBatch(SQL)
                             then Load_calendar
                             else MainForm.WaveShowErrorToast(str_error_moving);
                           end;
end;

procedure TAgendaMainFrm.UniFrameAjaxEvent(Sender: TComponent;
  EventName: string; Params: TUniStrings);
begin
  if EventName='_ready'
  then Load_calendar;
end;

procedure TAgendaMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  ClientEvents.ExtEvents.Add('boxready=function boxready(sender, width, height, eOpts){ajaxRequest(sender, "_ready", [])}');
  SyncCalendarPanelAgenda.StartHour           := 8;
  SyncCalendarPanelAgenda.TextTitle           := str_title;
  SyncCalendarPanelAgenda.TextDescription     := str_description;
  SyncCalendarPanelAgenda.TextFrom            := str_from;
  SyncCalendarPanelAgenda.TextTo              := str_to;
  SyncCalendarPanelAgenda.TextResponcible     := str_responsable;
  SyncCalendarPanelAgenda.TextMenuAdd         := str_add;
  SyncCalendarPanelAgenda.TextMenuEdit        := str_modify;
  SyncCalendarPanelAgenda.TextMenuDelete      := str_delete;

  SyncCalendarPanelAgenda.ResourcesItems.Clear;

  With UniMainModule.QWork
  do begin
       Close;
       SQL.Clear;
       SQL.Add('Select id, name, firstname, color_scheduler from users where companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
               'and removed=0 order by id');
       Open;
       First;
       while not Eof
       do begin
            SyncCalendarPanelAgenda.ResourcesItems.Add;
            SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Text  := Trim(FieldByName('Name').AsString + ' ' + FieldByName('FirstName').AsString);
            SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Id    := FieldByName('Id').AsInteger;
            if (not FieldByName('Color_scheduler').IsNull) and (FieldByName('Color_scheduler').AsString <> '')
            then SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Color := StringToColor(TrimRight(FieldByName('Color_scheduler').AsString));
            Next;
          end;
       Close;
     end;

  //Create template calendar
  SyncCalendarPanelAgenda.ResourcesItems.Add;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Text  := str_free_block;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Id    := -1;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Color := $00E5D3C1;

  SyncCalendarPanelAgenda.ResourcesItems.Add;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Text  := str_to_be_confirmed;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Id    := -2;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Color := $00625FBB;

  SyncCalendarPanelAgenda.ResourcesItems.Add;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Text  := str_confirmed;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Id    := -3;
  SyncCalendarPanelAgenda.ResourcesItems[SyncCalendarPanelAgenda.ResourcesItems.Count-1].Color := $000E601F;

  SyncCalendarPanelAgenda.SelectedDate := Now;
end;

procedure TAgendaMainFrm.UpdateStrings;
begin
  str_confirmed        := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_confirmed' (* 'Bevestigde afspraak' *) );
  str_to_be_confirmed  := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_to_be_confirmed' (* 'Te bestigen afspraak' *) );
  str_delete           := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_delete' (* 'Verwijderen...' *) );
  str_modify           := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_modify' (* 'Wijzigen...' *) );
  str_add              := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_add' (* 'Toevoegen...' *) );
  str_responsable      := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_responsable' (* 'Verantwoordelijke: ' *) );
  str_to               := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_to' (* 'Tot: ' *) );
  str_from             := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_from' (* 'Van: ' *) );
  str_description      := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_description' (* 'Omschrijving: ' *) );
  str_title            := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_title' (* 'Titel: ' *) );
  str_error_moving     := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_error_moving' (* 'Fout bij het verplaatsen' *) );
  str_error_resizing   := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_error_resizing' (* 'Fout bij het aanpassen' *) );
  str_delete_item      := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_delete_item' (* 'Verwijderen item ' *) );
  str_delete_agenda    := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_delete_agenda' (* 'Verwijderen agenda' *) );
  str_error_editing    := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_error_editing' (* 'Fout bij het wijzigen' *) );
  str_free_block       := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_free_block' (* 'Vrij blok' *) );
  str_error_adding     := LinkedLanguageAgendaMainn.GetTextOrDefault('strstr_error_adding' (* 'Fout bij het toevoegen' *) );
end;

end.

