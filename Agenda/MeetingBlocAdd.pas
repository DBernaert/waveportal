unit MeetingBlocAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, siComp, siLngLnk, Data.DB, MemDS, DBAccess, IBC,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox,
  uniDateTimePicker, uniDBDateTimePicker, uniButton, UniThemeButton, uniMemo,
  uniDBMemo, uniGUIBaseClasses, uniEdit, uniDBEdit;

var
	str_name_contact_required: string = 'Naam contact is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_error_saving_data:     string = 'Fout bij het opslaan van de gegevens'; // TSI: Localized (Don't modify!)


type
  TMeetingBlocAddFrm = class(TUniForm)
    EditContactName: TUniDBEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditContactEmail: TUniDBEdit;
    Crm_meetings: TIBCQuery;
    DsCrm_meetings: TDataSource;
    EditContactPhone: TUniDBEdit;
    LinkedLanguageMeetingBlocAdd: TsiLangLinked;
    MemoBlocInfo: TUniDBMemo;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageMeetingBlocAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFormReady(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Meetingbloc_edit(Id: longint; ViewOnly: boolean): boolean;
  end;

function MeetingBlocAddFrm: TMeetingBlocAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function MeetingBlocAddFrm: TMeetingBlocAddFrm;
begin
  Result := TMeetingBlocAddFrm(UniMainModule.GetFormInstance(TMeetingBlocAddFrm));
end;

{ TMeetingBlocAddFrm }

procedure TMeetingBlocAddFrm.BtnCancelClick(Sender: TObject);
begin
  Crm_meetings.Cancel;
  Crm_meetings.Close;
  Close;
end;

procedure TMeetingBlocAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(Crm_meetings.FieldByName('Bloc_contact_name').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_name_contact_required);
         EditContactName.SetFocus;
         Exit;
       end;

  Try
    Crm_meetings.FieldByName('Crm_Contributor').AsInteger := UniMainModule.User_id;
    Crm_meetings.FieldByName('Description').AsString      := Crm_meetings.FieldByName('Bloc_contact_name').AsString;

    if Trim(Crm_meetings.FieldByName('Bloc_contact_email').AsString) <> ''
    then Crm_meetings.FieldByName('Description').AsString := Crm_meetings.FieldByName('Description').asString + chr(13) +
                                                             Crm_meetings.FieldByName('Bloc_contact_email').asString;

    if Trim(Crm_meetings.FieldByName('Bloc_contact_phone').AsString) <> ''
    then Crm_meetings.FieldByName('Description').AsString := Crm_meetings.FieldByName('Description').asString + chr(13) +
                                                             Crm_meetings.FieldByName('Bloc_contact_phone').asString;

    if Trim(Crm_meetings.FieldByName('Bloc_info').AsString) <> ''
    then Crm_meetings.FieldByName('Description').AsString := Crm_meetings.FieldByName('Description').asString + chr(13) + chr(13) +
                                                             Crm_meetings.FieldByName('Bloc_info').asString;
    Crm_meetings.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
    UniMainModule.Result_dbAction := Crm_meetings.FieldByName('Id').AsInteger;
    Crm_meetings.Close;
  Except
    Crm_meetings.Cancel;
    Crm_meetings.Close;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    ShowToast(str_error_saving_data);
  End;
  Close;
end;

procedure TMeetingBlocAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMeetingBlocAddFrm.LinkedLanguageMeetingBlocAddChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

function TMeetingBlocAddFrm.Meetingbloc_edit(Id: longint; ViewOnly: boolean): boolean;
var SQL: string;
begin
  Crm_meetings.Close;
  SQL := 'Select * from crm_meetings where id=:Id';
  Crm_meetings.SQL.Text := SQL;
  Crm_meetings.ParamByName('Id').AsInteger := Id;
  Crm_meetings.Open;
  try
    if ViewOnly
    then begin
           EditContactName.ReadOnly  := True;
           EditContactEmail.ReadOnly := True;
           EditContactPhone.ReadOnly := True;
           BtnSave.Enabled           := False;
           Result                    := True;
         end
    else begin
           Crm_meetings.Edit;
           crm_meetings.FieldByName('Meeting_status').AsInteger  := 2;
           Crm_meetings.FieldByName('Crm_contributor').AsInteger := UniMainModule.User_id;
           Result := True;
         end;
  except
    Crm_meetings.Cancel;
    Crm_meetings.Close;
    Result := False;
  end
end;

procedure TMeetingBlocAddFrm.UniFormReady(Sender: TObject);
begin
  MemoBlocInfo.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
end;

procedure TMeetingBlocAddFrm.UpdateStrings;
begin
  str_error_saving_data     := LinkedLanguageMeetingBlocAdd.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens' *) );
  str_name_contact_required := LinkedLanguageMeetingBlocAdd.GetTextOrDefault('strstr_name_contact_required' (* 'Naam contact is een verplichte ingave' *) );
end;

end.

