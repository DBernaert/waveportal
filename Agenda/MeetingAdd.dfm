object MeetingAddFrm: TMeetingAddFrm
  Left = 0
  Top = 0
  ClientHeight = 377
  ClientWidth = 681
  Caption = 'Afspraak'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditTitle
  Images = UniMainModule.ImageList
  ImageIndex = 37
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditTitle: TUniDBEdit
    Left = 16
    Top = 16
    Width = 649
    Height = 22
    Hint = ''
    DataField = 'TITLE'
    DataSource = DsCrm_meetings
    TabOrder = 0
    FieldLabel = 'Titel'
    FieldLabelWidth = 150
  end
  object EditDescription: TUniDBMemo
    Left = 16
    Top = 112
    Width = 649
    Height = 137
    Hint = ''
    DataField = 'DESCRIPTION'
    DataSource = DsCrm_meetings
    TabOrder = 3
    FieldLabel = 'Omschrijving'
    FieldLabelWidth = 150
  end
  object BtnSave: TUniThemeButton
    Left = 400
    Top = 328
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 6
    ScreenMask.Target = Owner
    OnClick = BtnSaveClick
    ButtonTheme = uctSuccess
  end
  object BtnCancel: TUniThemeButton
    Left = 536
    Top = 328
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 7
    ScreenMask.Target = Owner
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object EditStart: TUniDBDateTimePicker
    Left = 16
    Top = 256
    Width = 649
    Hint = ''
    DataField = 'MEETING_START'
    DataSource = DsCrm_meetings
    DateTime = 43846.538178692130000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm'
    Kind = tUniDateTime
    TabOrder = 4
    FieldLabel = 'Begintijdstip *'
    FieldLabelWidth = 150
  end
  object EditEnd: TUniDBDateTimePicker
    Left = 16
    Top = 288
    Width = 649
    Hint = ''
    DataField = 'MEETING_END'
    DataSource = DsCrm_meetings
    DateTime = 43846.538178692130000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm'
    Kind = tUniDateTime
    TabOrder = 5
    FieldLabel = 'Eindtijdstip *'
    FieldLabelWidth = 150
  end
  object EditContact: TUniDBLookupComboBox
    Left = 16
    Top = 48
    Width = 649
    Hint = ''
    ListField = 'FULLNAME'
    ListSource = DsContacts
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'CRM_CONTACT'
    DataSource = DsCrm_meetings
    TabOrder = 1
    Color = clWindow
    FieldLabel = 'Contact'
    FieldLabelWidth = 150
    Style = csDropDown
  end
  object EditLocation: TUniDBEdit
    Left = 16
    Top = 80
    Width = 649
    Height = 22
    Hint = ''
    DataField = 'LOCATION'
    DataSource = DsCrm_meetings
    TabOrder = 2
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Locatie'
    FieldLabelWidth = 150
  end
  object Crm_meetings: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_CRM_MEETINGS_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UniMainModule.UpdTrans
    SQL.Strings = (
      'Select * from crm_meetings')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 368
    Top = 104
  end
  object DsCrm_meetings: TDataSource
    DataSet = Crm_meetings
    Left = 368
    Top = 152
  end
  object Contacts: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, coalesce(last_name, '#39#39') || '#39' '#39' || coalesce(first_name' +
        ', '#39#39') as FullName'
      'from crm_contacts'
      'order by FullName')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 261
    Top = 104
  end
  object DsContacts: TDataSource
    DataSet = Contacts
    Left = 261
    Top = 152
  end
  object LinkedLanguageMeetingAdd: TsiLangLinked
    Version = '7.9.0.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TMeetingAddFrm.Layout'
      'EditTitle.FieldLabelSeparator'
      'EditDescription.FieldLabelSeparator'
      'EditStart.DateFormat'
      'EditStart.FieldLabelSeparator'
      'EditStart.TimeFormat'
      'EditEnd.DateFormat'
      'EditEnd.FieldLabelSeparator'
      'EditEnd.TimeFormat'
      'EditContact.FieldLabelSeparator'
      'EditLocation.FieldLabelSeparator'
      'Crm_meetings.KeyGenerator')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 256
    Top = 208
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A0054004D0065006500740069006E0067004100640064004600
      72006D00010041006600730070007200610061006B000100520065006E006400
      65007A002D0076006F007500730001004100700070006F0069006E0074006D00
      65006E00740001000D000A00420074006E00530061007600650001004F007000
      73006C00610061006E0001005300610075007600650067006100720064006500
      72000100530061007600650001000D000A00420074006E00430061006E006300
      65006C00010041006E006E0075006C006500720065006E00010041006E006E00
      75006C00650072000100430061006E00630065006C0001000D000A0073007400
      480069006E00740073005F0055006E00690063006F00640065000D000A007300
      740044006900730070006C00610079004C006100620065006C0073005F005500
      6E00690063006F00640065000D000A007300740046006F006E00740073005F00
      55006E00690063006F00640065000D000A00730074004D0075006C0074006900
      4C0069006E00650073005F0055006E00690063006F00640065000D000A007300
      740053007400720069006E00670073005F0055006E00690063006F0064006500
      0D000A007300740072007300740072005F006500720072006F0072005F007300
      6100760069006E006700010046006F00750074002000620069006A0020006800
      6500740020006F00700073006C00610061006E002000760061006E0020006400
      650020006700650067006500760065006E0073002E0001004500720072006500
      7500720020006C006F007200730020006400650020006C00270065006E007200
      65006700690073007400720065006D0065006E00740020006400650073002000
      64006F006E006E00E900650073002E0001004500720072006F00720020007300
      6100760069006E0067002000740068006500200064006100740061002E000100
      0D000A00730074004F00740068006500720053007400720069006E0067007300
      5F0055006E00690063006F00640065000D000A00450064006900740054006900
      74006C0065002E004600690065006C0064004C006100620065006C0001005400
      6900740065006C0001005400690074007200650001005400690074006C006500
      01000D000A004500640069007400440065007300630072006900700074006900
      6F006E002E004600690065006C0064004C006100620065006C0001004F006D00
      730063006800720069006A00760069006E006700010044006500730063007200
      69007000740069006F006E000100440065007300630072006900700074006900
      6F006E0001000D000A004500640069007400530074006100720074002E004600
      690065006C0064004C006100620065006C00010042006500670069006E007400
      69006A006400730074006900700020002A0001004400E9006200750074000100
      5300740061007200740001000D000A00450064006900740045006E0064002E00
      4600690065006C0064004C006100620065006C000100450069006E0064007400
      69006A006400730074006900700020002A000100460069006E00010045006E00
      640001000D000A00450064006900740043006F006E0074006100630074002E00
      4600690065006C0064004C006100620065006C00010043006F006E0074006100
      63007400010043006F006E007400610063007400010043006F006E0074006100
      6300740001000D000A0045006400690074004C006F0063006100740069006F00
      6E002E004600690065006C0064004C006100620065006C0001004C006F006300
      610074006900650001004C0069006500750001004C006F006300610074006900
      6F006E0001000D000A007300740043006F006C006C0065006300740069006F00
      6E0073005F0055006E00690063006F00640065000D000A007300740043006800
      6100720053006500740073005F0055006E00690063006F00640065000D000A00}
  end
end
