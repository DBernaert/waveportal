unit MeetingAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniDBComboBox, uniMultiItem, uniComboBox,
  uniDBLookupComboBox, uniDateTimePicker, uniDBDateTimePicker, uniButton,
  UniThemeButton, uniMemo, uniDBMemo, uniGUIBaseClasses, uniEdit, uniDBEdit,
  Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

var
	str_error_saving: string = 'Fout bij het opslaan van de gegevens.'; // TSI: Localized (Don't modify!)

type
  TMeetingAddFrm = class(TUniForm)
    Crm_meetings: TIBCQuery;
    DsCrm_meetings: TDataSource;
    EditTitle: TUniDBEdit;
    EditDescription: TUniDBMemo;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditStart: TUniDBDateTimePicker;
    EditEnd: TUniDBDateTimePicker;
    EditContact: TUniDBLookupComboBox;
    Contacts: TIBCQuery;
    DsContacts: TDataSource;
    EditLocation: TUniDBEdit;
    LinkedLanguageMeetingAdd: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
  public
    { Public declarations }
    function Meeting_insert(StartDate, EndDate: TDateTime): boolean;
    function Meeting_edit(Id: longint): boolean;
    procedure Open_reference_tables;
  end;

function MeetingAddFrm: TMeetingAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function MeetingAddFrm: TMeetingAddFrm;
begin
  Result := TMeetingAddFrm(UniMainModule.GetFormInstance(TMeetingAddFrm));
end;

procedure TMeetingAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  Try
    Crm_meetings.FieldByName('Crm_Contributor').AsInteger := UniMainModule.User_id;
    Crm_meetings.FieldByName('Meeting_status').AsInteger := 1;
    Crm_meetings.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
    UniMainModule.Result_dbAction := Crm_meetings.FieldByName('Id').AsInteger;
    Crm_meetings.Close;
  Except
    Crm_meetings.Cancel;
    Crm_meetings.Close;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    ShowToast(str_error_saving);
  End;
  Close;
end;

procedure TMeetingAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMeetingAddFrm.Open_reference_tables;
var SQL: string;
begin
  Contacts.Close;
  Contacts.SQL.Clear;
  SQL := 'Select id, coalesce(last_name, '''') || '' '' || coalesce(first_name, '''') as FullName ' +
         'from crm_contacts where CompanyID=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' order by FullName';
  Contacts.SQL.Add(SQL);
  Contacts.Open;
end;

function TMeetingAddFrm.Meeting_edit(Id: longint): boolean;
var SQL: string;
begin
  Crm_meetings.Close;
  Crm_meetings.SQL.Clear;
  SQL := 'Select * from crm_meetings where id=' + IntToStr(Id);
  Crm_meetings.SQL.Add(SQL);
  Crm_meetings.Open;
  try
    Crm_meetings.Edit;
    Open_reference_tables;
    Result := True;
  except
    Crm_meetings.Cancel;
    Crm_meetings.Close;
    Result := False;
  end
end;

function TMeetingAddFrm.Meeting_insert(StartDate, EndDate: TDateTime): boolean;
var SQL: string;
begin
  Crm_meetings.Close;
  Crm_meetings.SQL.Clear;
  SQL := 'Select first 0 * from crm_meetings';
  Crm_meetings.SQL.Add(SQL);
  Crm_meetings.Open;
  try
    Crm_meetings.Append;
    Crm_meetings.FieldByName('CompanyId').AsInteger                    := UniMainModule.Company_id;
    Crm_meetings.FieldByName('Removed').AsString                       := '0';
    Crm_meetings.FieldByName('Meeting_start').AsDateTime               := StartDate;
    Crm_meetings.FieldByName('Meeting_end').AsDateTime                 := EndDate;
    Crm_meetings.FieldByName('Responsable_type').AsInteger             := 2;
    Crm_meetings.FieldByName('Responsable_id').AsInteger               := UniMainModule.User_id;
    Crm_meetings.FieldByName('Confirmation_contact').AsInteger         := 0;
    Crm_meetings.FieldByname('Confirmation_intermediary').AsInteger    := 0;
    Open_reference_tables;
    Result := True;
  except
    Crm_meetings.Cancel;
    Crm_meetings.Close;
    Result := False;
  end
end;

procedure TMeetingAddFrm.UniFormReady(Sender: TObject);
begin
  EditDescription.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

procedure TMeetingAddFrm.UpdateStrings;
begin
  str_error_saving := LinkedLanguageMeetingAdd.GetTextOrDefault('strstr_error_saving' (* 'Fout bij het opslaan van de gegevens.' *) );
end;

procedure TMeetingAddFrm.BtnCancelClick(Sender: TObject);
begin
  Crm_meetings.Cancel;
  Crm_meetings.Close;
  Close;
end;

end.

