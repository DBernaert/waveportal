unit PortalNotActive;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniImage, uniGUIBaseClasses, uniPanel, Vcl.Imaging.pngimage;

type
  TPortalNotActiveFrm = class(TUniFrame)
    ContainerError: TUniContainerPanel;
    ImageError: TUniImage;
    LblDutch: TUniLabel;
    LblFrench: TUniLabel;
    LblEnglish: TUniLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}



end.
