object PortalNotFoundFrm: TPortalNotFoundFrm
  Left = 0
  Top = 0
  Width = 1090
  Height = 860
  Layout = 'vbox'
  LayoutAttribs.Align = 'center'
  LayoutAttribs.Pack = 'center'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerError: TUniContainerPanel
    Left = 224
    Top = 32
    Width = 585
    Height = 665
    Hint = ''
    ParentColor = False
    TabOrder = 0
    object ImageError: TUniImage
      Left = 36
      Top = 1
      Width = 512
      Height = 512
      Hint = ''
      AutoSize = True
      Url = 'images/wv_wavedesk_logo_500.svg'
      OnClick = ImageErrorClick
    end
    object LblDutch: TUniLabel
      Left = 99
      Top = 536
      Width = 383
      Height = 30
      Hint = ''
      Caption = 'De portaalsite kon niet worden gevonden'
      ParentFont = False
      Font.Height = -21
      TabOrder = 2
      LayoutConfig.Cls = 'boldtext'
    end
    object LblFrench: TUniLabel
      Left = 127
      Top = 568
      Width = 324
      Height = 30
      Hint = ''
      Caption = 'Le site portail n'#39'a pa pu '#234'tre trouv'#233
      ParentFont = False
      Font.Height = -21
      TabOrder = 3
      LayoutConfig.Cls = 'boldtext'
    end
    object LblEnglish: TUniLabel
      Left = 152
      Top = 600
      Width = 278
      Height = 30
      Hint = ''
      Caption = 'The portal could not be found'
      ParentFont = False
      Font.Height = -21
      TabOrder = 4
      LayoutConfig.Cls = 'boldtext'
    end
  end
end
