unit MailAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  uniGUITypes, uniGUIAbstractClasses, uniGUIClasses, uniGUIForm, uniButton,
  UniThemeButton, uniMemo, uniGUIBaseClasses, uniEdit, siComp, siLngLnk,
  uniLabel, uniImage, uniCheckBox, uniHTMLMemo, uniMultiItem, uniComboBox,
  Data.DB, MemDS, VirtualTable, uniBasicGrid, uniDBGrid, uniFileUpload,
  uniPanel, uniPageControl, DBAccess, IBC, uniListBox, Vcl.Menus, uniMainMenu;

type
  TMailAddFrm = class(TUniForm)
    BtnSend: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    LinkedLanguageMailAdd: TsiLangLinked;
    PageControlMail: TUniPageControl;
    TabSheetMail: TUniTabSheet;
    EditCC: TUniEdit;
    EditBCC: TUniEdit;
    EditSubJect: TUniEdit;
    EditContentHtml: TUniHTMLMemo;
    LblEmail: TUniLabel;
    LblCC: TUniLabel;
    LblBCC: TUniLabel;
    LblSubject: TUniLabel;
    LblContent: TUniLabel;
    GridAddresses: TUniDBGrid;
    TblAddresses: TVirtualTable;
    DsAddresses: TDataSource;
    BtnChangeSelected: TUniThemeButton;
    BtnDeleteAddress: TUniThemeButton;
    BtnAddAddress: TUniThemeButton;
    TblAddressesSelected: TIntegerField;
    TblAddressesEmail: TStringField;
    TblAddressesDescription: TStringField;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSendClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageMailAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnChangeSelectedClick(Sender: TObject);
    procedure BtnDeleteAddressClick(Sender: TObject);
    procedure BtnAddAddressClick(Sender: TObject);
    procedure TblAddressesSelectedGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
    _IdModule:     integer;
    _IdKey:        longint;
    _IdRecordId:   longint;
    _Language:     integer;
    _DocumentType: integer;
    procedure Get_email_address;
    procedure Add_email_address(Address: string; Description: string; Selected: boolean);
    procedure Get_template(DocumentType: integer; language: integer);
  public
    { Public declarations }
    function Start_mail_module(Module:           integer;
                               Key:              longint;
                               RecordId:         longint;
                               Email:            string;
                               DocumentType:     integer): boolean;
  end;

var
	str_email_required:            string = 'Email is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_subject_required:          string = 'Onderwerp is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_content_required:          string = 'Inhoud is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_mail_send:                 string = 'De mail werd verstuurd'; // TSI: Localized (Don't modify!)
	str_error_sending_mail:        string = 'Fout bij het versturen van de mail'; // TSI: Localized (Don't modify!)
	str_compose_mail:              string = 'Mail opstellen'; // TSI: Localized (Don't modify!)
	str_error_archiving:           string = 'Fout bij het archiveren.'; // TSI: Localized (Don't modify!)
	str_error_adding_attachments:  string = 'Fout bij het toevoegen van de bijlagen.'; // TSI: Localized (Don't modify!)
	str_error_creating_directory:  string = 'Fout bij het aanmaken van de directory!'; // TSI: Localized (Don't modify!)
	str_standard_account:          string = 'Standaard account'; // TSI: Localized (Don't modify!)
	str_company_invoicing:         string = 'Bedrijf - facturatie'; // TSI: Localized (Don't modify!)
	str_company_standard:          string = 'Bedrijf - standaard'; // TSI: Localized (Don't modify!)
	str_contact_standard:          string = 'Contact - standaard'; // TSI: Localized (Don't modify!)
	str_intermediary_standard:     string = 'Tussenpersoon - standaard'; // TSI: Localized (Don't modify!)
	str_intermediary:              string = 'Tussenpersoon'; // TSI: Localized (Don't modify!)
	str_manual_entry:              string = 'Manuele ingave'; // TSI: Localized (Don't modify!)
	str_agent_standard:            string = 'Agent - standaard'; // TSI: Localized (Don't modify!)
	str_forward_mail:              string = 'Mail doorsturen'; // TSI: Localized (Don't modify!)
	str_error_decrypting:          string = 'Fout bij het decrypteren: '; // TSI: Localized (Don't modify!)
	str_error_check_signature:     string = 'Fout bij de verificatie van de handtekening: '; // TSI: Localized (Don't modify!)
	str_company:                   string = 'Maatschappij'; // TSI: Localized (Don't modify!)
	str_add_email:                 string = 'Toevoegen e-mail'; // TSI: Localized (Don't modify!)

function MailAddFrm: TMailAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, EASendMailObjLib_TLB, IOUtils, Main,
  ServerModule, Utils, Mailparser, System.StrUtils, System.DateUtils,
  ContactSelect, AccountsSelect;

function MailAddFrm: TMailAddFrm;
begin
  Result := TMailAddFrm(UniMainModule.GetFormInstance(TMailAddFrm));
end;

procedure TMailAddFrm.BtnDeleteAddressClick(Sender: TObject);
begin
  if TblAddresses.Recordcount <> 0
  then TblAddresses.Delete;
end;

procedure TMailAddFrm.BtnSendClick(Sender: TObject);
var oSmtp:                    EASendMailObjLib_TLB.TMail;
    Body:                     string;
    SelectedCount:            integer;
    EmailAddresses:           string;

    Recipients:               string;
begin
  BtnSend.SetFocus;

  //Check email addresses
  SelectedCount := 0;
  if TblAddresses.RecordCount <> 0
  then begin
         TblAddresses.First;
         while not TblAddresses.Eof
         do begin
              if TblAddresses.FieldByName('Selected').AsInteger = 1
              then Inc(SelectedCount);
              TblAddresses.Next;
            end;
         TblAddresses.First;
       end;

  if SelectedCount = 0
  then begin
         GridAddresses.SetFocus;
         MainForm.WaveShowWarningToast(str_email_required);
         Exit;
       end;

  if Trim(EditSubject.Text) = ''
  then begin
         EditSubject.SetFocus;
         MainForm.WaveShowWarningToast(str_subject_required);
         Exit;
       end;

  if Trim(EditContentHtml.Text) = ''
  then begin
         EditContentHtml.SetFocus;
         MainForm.WaveShowWarningToast(str_content_required);
         Exit;
       end;

  oSmtp := EASendMailObjLib_TLB.TMail.Create(UniApplication);

  Try
    oSmtp.SSL_init();

    oSmtp.LicenseCode   := 'ES-D1508812687-00323-4AC1DB277882BAF4-1878D6BC21AAA48U';

    oSmtp.ServerAddr    := 'mail.smtp2go.com';
    oSmtp.ServerPort    := 465;
    oSmtp.FromAddr      := 'notifications@digitalcloud.be';
    oSmtp.From          := UniMainModule.Company_name;

    TblAddresses.First;
    EmailAddresses := '';
    Recipients     := '';

    while not TblAddresses.Eof
    do begin
         if TblAddresses.FieldByName('Selected').AsInteger = 1
         then begin
                if Trim(EmailAddresses) <> ''
                then EmailAddresses := EmailAddresses + ';' + TblAddresses.FieldByName('Email').asString
                else EmailAddresses := TblAddresses.FieldByName('Email').AsString;

                if Trim(Recipients) <> ''
                then Recipients := Recipients + ' - ' + TblAddresses.FieldbyName('Email').asString
                else Recipients := TblAddresses.FieldByName('Email').asString;
              end;
         TblAddresses.Next;
       end;
    TblAddresses.First;

    oSmtp.AddRecipientEx(EmailAddresses, 0);

    if Trim(EditCC.Text) <> ''
    then begin
           oSmtp.AddRecipientEx(EditCC.Text, 1);
           Recipients := Recipients + ' - ' + EditCC.Text;
         end;

    if Trim(EditBCC.Text) <> ''
    then begin
           oSmtp.AddRecipientEx(EditBCC.Text, 2);
           Recipients := Recipients + ' - ' + EditBCC.Text;
         end;

    oSmtp.Subject            := EditSubJect.Text;
    oSmtp.BodyFormat         := 1;

    Body                     := '';
    oSmtp.ImportHtml(EditContentHtml.Text, UniServerModule.StartPath);

    oSmtp.UserName      := 'adm-concept.be';
    oSmtp.Password      := 'Zandstraat11';
    oSmtp.Protocol      := 0;

    if osmtp.SendMail()  = 0
    then begin
           MainForm.WaveShowConfirmationToast(str_mail_send);
           UniMainModule.Result_dbAction := 1;
           Close;
         end
    else begin
           MainForm.WaveShowErrorMessage(oSmtp.GetLastErrDescription);
           MainForm.WaveShowErrorToast(str_error_sending_mail);
         end;
  Finally
    oSmtp.Free;
  End;
end;

procedure TMailAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMailAddFrm.Get_email_address;
begin
  if _IdKey = 0
  then exit;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select email from crm_contacts where id=' + IntToStr(_IdKey));
  UniMainModule.QWork.Open;

  if Trim(UniMainModule.QWork.FieldByName('Email').AsString) <> ''
  then Add_email_address(UniMainModule.QWork.FieldByName('Email').asString, str_contact_standard, True);

  UniMainModule.QWork.Close;
end;

procedure TMailAddFrm.Get_template(DocumentType: integer; Language: integer);
var workvalue: string;
begin
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from mail_templates where companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
                              'and Template_id=' + IntToStr(DocumentType));
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.RecordCount = 1
  then begin
         case Language of
           0: begin
                WorkValue            := UniMainModule.QWork.FieldByName('Title_dutch').asString;
                Mailparser.Parsetemplate(workvalue, DocumentType, _IdKey, _IdModule, _IdRecordId);
                EditSubject.Text     := workValue;
                WorkValue            := UniMainModule.QWork.FieldByName('Content_dutch').AsString;
                Mailparser.Parsetemplate(workvalue, DocumentType, _IdKey, _idModule, _IdRecordId);
                EditContentHtml.Text := WorkValue;
              end;
           1: begin
                WorkValue            := UniMainModule.QWork.FieldByName('Title_french').asString;
                Mailparser.Parsetemplate(workvalue, DocumentType, _IdKey, _IdModule, _IdRecordId);
                EditSubject.Text     := workvalue;
                WorkValue            := UniMainModule.QWork.FieldByName('Content_french').AsString;
                Mailparser.Parsetemplate(workvalue, DocumentType, _IdKey, _idModule, _IdRecordId);
                EditContentHtml.Text := WorkValue;
              end;
           2: begin
                WorkValue            := UniMainModule.QWork.FieldByName('Title_english').asString;
                Mailparser.Parsetemplate(workvalue, DocumentType, _IdKey, _IdModule, _IdRecordId);
                EditSubject.Text     := workvalue;
                WorkValue            := UniMainModule.QWork.FieldByName('Content_english').AsString;
                Mailparser.Parsetemplate(workvalue, DocumentType, _IdKey, _idModule, _IdRecordId);
                EditContentHtml.Text := WorkValue;
              end;
         end;
       end;
  UniMainModule.QWork.Close;
end;

procedure TMailAddFrm.LinkedLanguageMailAddChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

function TMailAddFrm.Start_mail_module(Module:           integer;
                                       Key:              longint;
                                       RecordId:         longint;
                                       Email:            string;
                                       DocumentType:     integer): boolean;
begin
  _IdModule := Module;
  _IdKey := Key;
  _IdRecordId := RecordId;
  _Language := UniMainModule.LanguageManager.ActiveLanguage-1;
  _DocumentType := DocumentType;
  Result := True;
  Caption := str_compose_mail;

  TblAddresses.DisableControls;
  TblAddresses.First;
  while not TblAddresses.Eof
  do TblAddresses.Delete;

  if Trim(Email) <> ''
  then begin
         if _IdModule = 3
         then Add_email_address(Email, str_contact_standard, False)
         else Add_email_address(Email, str_contact_standard, True);
       end;

  //If email is empty, we try to fetch the address from the tables
  if TblAddresses.RecordCount = 0
  then Get_email_address;

  if _IdKey <> 0
  then begin
         //Check the language
         case _IdModule of
           1: begin
                UniMainModule.QWork.Close;
                UniMainModule.QWork.SQL.Clear;
                UniMainModule.QWork.SQL.Add('Select language_doc from crm_accounts where id=' + IntToStr(_IdKey));
                UniMainModule.QWork.Open;
                _Language := UniMainModule.QWork.FieldByName('Language_doc').AsInteger;
                UniMainModule.QWork.Close;
              end;
           2: begin
                UniMainModule.QWork.Close;
                UniMainModule.QWork.SQL.Clear;
                UniMainModule.QWork.SQL.Add('Select language_doc from crm_contacts where id=' + IntToStr(_IdKey));
                UniMainModule.QWork.Open;
                _Language := UniMainModule.QWork.FieldByName('Language_doc').AsInteger;
                UniMainModule.QWork.Close;
              end;
           3: begin
                UniMainModule.QWork.Close;
                UniMainModule.QWork.SQL.Clear;
                UniMainModule.QWork.SQL.Add('Select language_doc from credit_contributors where id=' + IntToStr(_IdKey));
                UniMainModule.QWork.Open;
                _Language := UniMainModule.QWork.FieldByName('Language_doc').AsInteger;
                UniMainModule.QWork.Close;
              end;
          end;
       end
  else _Language := UniMainModule.LanguageManager.ActiveLanguage - 1;

  //Check for templates
  Get_template(DocumentType, _Language);

  TblAddresses.First;
  TblAddresses.EnableControls;
  ActiveControl := EditSubject;
end;

procedure TMailAddFrm.TblAddressesSelectedGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-check-circle fa-lg" style=color:#E0E0E0; ></i>'
  end;
end;

procedure TMailAddFrm.UpdateStrings;
begin
  str_add_email                := LinkedLanguageMailAdd.GetTextOrDefault('strstr_add_email' (* 'Toevoegen e-mail' *) );
  str_company                  := LinkedLanguageMailAdd.GetTextOrDefault('strstr_company' (* 'Maatschappij' *) );
  str_error_check_signature    := LinkedLanguageMailAdd.GetTextOrDefault('strstr_error_check_signature' (* 'Fout bij de verificatie van de handtekening: ' *) );
  str_error_decrypting         := LinkedLanguageMailAdd.GetTextOrDefault('strstr_error_decrypting' (* 'Fout bij het decrypteren: ' *) );
  str_forward_mail             := LinkedLanguageMailAdd.GetTextOrDefault('strstr_forward_mail' (* 'Mail doorsturen' *) );
  str_agent_standard           := LinkedLanguageMailAdd.GetTextOrDefault('strstr_agent_standard' (* 'Agent - standaard' *) );
  str_manual_entry             := LinkedLanguageMailAdd.GetTextOrDefault('strstr_manual_entry' (* 'Manuele ingave' *) );
  str_intermediary             := LinkedLanguageMailAdd.GetTextOrDefault('strstr_intermediary' (* 'Tussenpersoon' *) );
  str_intermediary_standard    := LinkedLanguageMailAdd.GetTextOrDefault('strstr_intermediary_standard' (* 'Tussenpersoon - standaard' *) );
  str_contact_standard         := LinkedLanguageMailAdd.GetTextOrDefault('strstr_contact_standard' (* 'Contact - standaard' *) );
  str_company_standard         := LinkedLanguageMailAdd.GetTextOrDefault('strstr_company_standard' (* 'Bedrijf - standaard' *) );
  str_company_invoicing        := LinkedLanguageMailAdd.GetTextOrDefault('strstr_company_invoicing' (* 'Bedrijf - facturatie' *) );
  str_standard_account         := LinkedLanguageMailAdd.GetTextOrDefault('strstr_standard_account' (* 'Standaard account' *) );
  str_error_creating_directory := LinkedLanguageMailAdd.GetTextOrDefault('strstr_error_creating_directory' (* 'Fout bij het aanmaken van de directory!' *) );
  str_error_adding_attachments := LinkedLanguageMailAdd.GetTextOrDefault('strstr_error_adding_attachments' (* 'Fout bij het toevoegen van de bijlagen.' *) );
  str_error_archiving          := LinkedLanguageMailAdd.GetTextOrDefault('strstr_error_archiving' (* 'Fout bij het archiveren.' *) );
  str_compose_mail             := LinkedLanguageMailAdd.GetTextOrDefault('strstr_compose_mail' (* 'Mail opstellen' *) );
  str_error_sending_mail       := LinkedLanguageMailAdd.GetTextOrDefault('strstr_error_sending_mail' (* 'Fout bij het versturen van de mail' *) );
  str_mail_send                := LinkedLanguageMailAdd.GetTextOrDefault('strstr_mail_send' (* 'De mail werd verstuurd' *) );
  str_content_required         := LinkedLanguageMailAdd.GetTextOrDefault('strstr_content_required' (* 'Inhoud is een verplichte ingave' *) );
  str_subject_required         := LinkedLanguageMailAdd.GetTextOrDefault('strstr_subject_required' (* 'Onderwerp is een verplichte ingave' *) );
  str_email_required           := LinkedLanguageMailAdd.GetTextOrDefault('strstr_email_required' (* 'Email is een verplichte ingave' *) );
end;

procedure TMailAddFrm.Add_email_address(Address: string; Description: string; Selected: boolean);
begin
  if not TblAddresses.Locate('Email', Address, [loCaseInsensitive])
  then begin
         TblAddresses.Append;
         if Selected = True
         then TblAddresses.FieldByName('Selected').AsInteger   := 1
         else TblAddresses.FieldByName('Selected').AsInteger   := 0;
         TblAddresses.FieldByName('Email').asString       := Address;
         TblAddresses.FieldByName('Description').AsString := Description;
         TblAddresses.Post;
       end
  else begin
         TblAddresses.Edit;
         if Selected = True
         then TblAddresses.FieldByname('Selected').AsInteger := 1
         else TblAddresses.FieldByName('Selected').AsInteger := 0;
         TblAddresses.Post;
       end;
end;

procedure TMailAddFrm.BtnAddAddressClick(Sender: TObject);
begin
  if MainForm.WaveInputText(UniMainModule.Company_name, str_add_email, '')
  then begin
         if Trim(MainForm.SweetAlert.InputResult) <> ''
         then begin
                TblAddresses.Append;
                TblAddresses.FieldByName('Selected').AsInteger    := 1;
                TblAddresses.FieldByName('Email').asString        := Trim(MainForm.SweetAlert.InputResult);
                TblAddresses.FieldByName('Description').asString  := str_manual_entry;
                TblAddresses.Post;
              end;
       end;
end;

procedure TMailAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TMailAddFrm.BtnChangeSelectedClick(Sender: TObject);
begin
  if TblAddresses.RecordCount <> 0
  then begin
         TblAddresses.Edit;
         if TblAddresses.FieldByName('Selected').AsInteger = 0
         then TblAddresses.FieldByName('Selected').AsInteger := 1
         else TblAddresses.FieldByName('Selected').AsInteger := 0;
         TblAddresses.Post;
       end;
end;

end.


