﻿unit xlsGrid;

interface
uses
  SysUtils,System.UITypes, VCL.FlexCel.Core, FlexCel.XlsAdapter,
  DB, FlexCel.Render,Classes,Variants, uniGUIClasses,uniGUIBaseClasses,uniBasicGrid,
  uniDBGrid, IdSSLOpenSSLHeaders;

 type
    TxlsGrid = class
     private
        RedColor:        TUIColor; //Red
        GreenColor:      TUIColor; //Green
        BlackColor:      TUIColor; //Black
     private
       FileName:         String;
       Grid:             TUniDBGrid;
       Header:           string;
       Footer:           string;
       isRTL:            boolean;
       useGridFontColor: boolean;
       Visible_Cols:     SmallInt;

       GroupField:       TField;
       GroupValue:       string;
       row:              SmallInt;
       rowG:             SmallInt;
       procedure ReportHeader(const xls: TExcelFile);
       procedure BandGroupHeader(const xls: TExcelFile);
       procedure BandGroupFooter(const xls: TExcelFile);
       procedure BandSum(const xls: TExcelFile);
       procedure BandData(const xls: TExcelFile);
       procedure BandFooter(const xls: TExcelFile);
       procedure CreateFile();
     public
       class procedure ExportXls(aGrid:TUniDBGrid;aHeader,aFooter,aFileName: String;
           isRTLSheet:boolean=false;uzGridFontColor:boolean=false);
    end;

implementation

{ TxlsGrid }

uses MainModule;

procedure TxlsGrid.BandData(const xls: TExcelFile);
var
  fmtNor, fmtCur, fmtNumCredit, fmtNumDebit, fmtNumZiro: TFlxFormat;
  Data:                                                  TDataSet;
  i , Visible_i:                                         SmallInt;
begin
  fmtNor                          := xls.GetStyle('Normal 2', true);
  fmtNor.FillPattern.BgColor      := TExcelColor.Automatic;
  fmtNor.FillPattern.FgColor      := TUIColor.FromArgb($EF, $F6, $EA); //Line Color

  fmtNumDebit                     := xls.GetStyle('Normal 2', true);
  fmtNumDebit.Font.Color          := RedColor; //Red
  fmtNumDebit.Font.Style          := [TFlxFontStyles.Bold];
  fmtNumDebit.Font.Size20         := 240;
  fmtNumDebit.HAlignment          := THFlxAlignment.right;
  fmtNumDebit.FillPattern.FgColor := TUIColor.FromArgb($EF, $F6, $EA); //Line Color

  fmtNumCredit                    := fmtNumDebit.Clone;
  fmtNumCredit.Font.Color         := GreenColor; //Green
  fmtNumZiro                      := fmtNumCredit.Clone;
  fmtNumZiro.Font.Color           := BlackColor;

  Data:=Grid.DataSource.DataSet;

  row                             :=2;
  rowG                            :=1;
  Data.First;
  with Data do  //use recno
  while not Eof
  do begin
       BandGroupHeader(xls);
       Visible_i := 0;
       for I := 0 to Grid.Columns.Count-1 do
       if Grid.Columns[i].Visible then
         begin
          inc(Visible_i);
          fmtCur:=fmtNor;
          fmtNumCredit.Font.Color:=GreenColor;
          fmtNumDebit.Font.Color:=RedColor;
          fmtNumZiro.Font.Color:=BlackColor;

          if Grid.Columns[i].Field.DataType=ftFloat then
          begin
             if Grid.Columns[i].Field.AsFloat<0 then
                 fmtCur:=fmtNumCredit
             else if Grid.Columns[i].Field.AsFloat>0 then
                 fmtCur:=fmtNumDebit
             else
                 fmtCur:=fmtNumZiro;
          end;

          if useGridFontColor then
            if Grid.Columns[i].Font.Color<>-16777208 then //clWindowText
                fmtCur.Font.Color:=Grid.Columns[i].Font.Color;

          if rowG mod 2 =1 then
             fmtCur.FillPattern.Pattern := TFlxPatternStyle.Solid //colored
          else
             fmtCur.FillPattern.Pattern := TFlxPatternStyle.None; //normal

          xls.SetCellFormat(row, Visible_i, xls.AddFormat(fmtCur));
          if Grid.Columns[i].Field.DataType=ftFloat
          then xls.SetCellValue(row, Visible_i,  Grid.Columns[i].Field.Value)
          else begin
                 if SameText(Grid.Columns[i].Field.DisplayText, '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>')
                 then begin
                        case UniMainModule.LanguageManager.ActiveLanguage of
                          1: xls.SetCellValue(row, Visible_i, 'Ja');
                          2: xls.SetCellValue(row, Visible_i, 'Oui');
                          3: xls.SetCellValue(row, Visible_i, 'Yes');
                        end;
                      end
                 else if SameText(Grid.Columns[i].Field.DisplayText, '<i class="fas fa-check-circle fa-lg" style=color:#E0E0E0; ></i>') or
                         SameText(Grid.Columns[i].Field.DisplayText, '<i class="fas fa-times-circle fa-lg" style=color:#B71C1C; ></i>')
                      then begin
                             case UniMainModule.LanguageManager.ActiveLanguage of
                               1: xls.SetCellValue(row, Visible_i, 'Nee');
                               2: xls.SetCellValue(row, Visible_i, 'Non');
                               3: xls.SetCellValue(row, Visible_i, 'No');
                             end;
                           end
                      else xls.SetCellValue(row, Visible_i,  Grid.Columns[i].Field.DisplayText);
               end;
         end;

        //add to column - sub Total
        BandSum(xls);

        //Next Record
        inc(row);
        inc(rowG);
        Next;
   end;

 //Last Group
 BandGroupFooter(xls);
 //
 BandFooter(xls);

 //Cell selection and scroll position.
  xls.SelectCell(3, 2, false);

  //Standard Document Properties - Most are only for xlsx files. In xls files FlexCel will only change the Creation Date and Modified Date.
  xls.DocumentProperties.SetStandardProperty(TPropertyId.Author, 'WAVEDESK');
  xls.DocumentProperties.SetStandardProperty(TPropertyId.Company, 'WAVEDESK (www.wavedesk.be)');
end;

procedure TxlsGrid.BandFooter(const xls: TExcelFile);
var
  fmt:TFlxFormat;
  I , Visible_i: Integer;
  F:Real;
begin

 if grid.Summary.Enabled then
   begin
    fmt := xls.GetStyle('Normal 2', true);
    fmt.Font.Style := [TFlxFontStyles.Bold];
    fmt.Font.Size20 := 240;
    fmt.Borders.Bottom.Style := TFlxBorderStyle.Thin;
    fmt.Borders.Bottom.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.Borders.Left.Style := TFlxBorderStyle.Thin;
    fmt.Borders.Left.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.Borders.top.Style := TFlxBorderStyle.Thin;
    fmt.Borders.top.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.Borders.Right.Style := TFlxBorderStyle.Thin;
    fmt.Borders.Right.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.FillPattern.Pattern := TFlxPatternStyle.Solid;
    fmt.FillPattern.FgColor := TUIColor.FromArgb($DC, $EB, $D1);
    fmt.FillPattern.BgColor := TExcelColor.Automatic;
    fmt.VAlignment:=TVFlxAlignment.center;
    fmt.Format := '#,##0.00';

    Visible_i:=0;
    with grid do
        for I := 0 to Columns.Count-1 do
        if Columns[i].Visible then
          begin
             inc(Visible_i);
             if not Columns[i].ShowSummary              then  continue;
             if not (Columns[i].Field.DataType=ftFloat) then  continue;

             F:=0;
             if Columns[i].AuxValues[1]<>null then
                F:=Columns[i].AuxValues[1];

             if (F<0) then
                 fmt.Font.Color:=GreenColor
             else if (F>0) then
                 fmt.Font.Color:=RedColor
             else
                 fmt.Font.Color:=BlackColor;

            if useGridFontColor then
              if Grid.Columns[i].Font.Color<>-16777208 then //clWindowText
                  fmt.Font.Color:=Grid.Columns[i].Font.Color;

             xls.SetCellFormat(row, Visible_i, xls.AddFormat(fmt));

             xls.SetCellValue(row, Visible_i, abs(F));
          end;

     xls.SetRowHeight(row, 582);  //23.25 * 25
     inc(row);
   end;
end;

procedure TxlsGrid.BandGroupFooter(const xls: TExcelFile);
var
  fmt:TFlxFormat;
  I , Visible_i: Integer;
  F:Real;
begin

if Assigned(GroupField)and(grid.Summary.Enabled) then
   begin
    fmt := xls.GetStyle('Normal 2', true);
    fmt.Font.Style := [TFlxFontStyles.Bold];
    fmt.Font.Size20 := 240;
    fmt.Borders.Bottom.Style := TFlxBorderStyle.Thin;
    fmt.Borders.Bottom.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.Borders.Left.Style := TFlxBorderStyle.Thin;
    fmt.Borders.Left.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.Borders.top.Style := TFlxBorderStyle.Thin;
    fmt.Borders.top.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.Borders.Right.Style := TFlxBorderStyle.Thin;
    fmt.Borders.Right.Color := TUIColor.FromArgb($00, $99, $00);
    fmt.VAlignment:=TVFlxAlignment.center;
    fmt.Format := '#,##0.00';

    Visible_i:=0;
    with grid do
        for I := 0 to Columns.Count-1 do
        if Columns[i].Visible then
          begin
             inc(Visible_i);
             if not Columns[i].ShowSummary              then  continue;
             if not (Columns[i].Field.DataType=ftFloat) then  continue;

             F:=Columns[i].AuxValue;
             Columns[i].AuxValue:=null;
             if (F<0) then
                 fmt.Font.Color:=GreenColor
             else if (F>0) then
                 fmt.Font.Color:=RedColor
             else
                 fmt.Font.Color:=BlackColor;

            if useGridFontColor then
              if Grid.Columns[i].Font.Color<>-16777208 then //clWindowText
                  fmt.Font.Color:=Grid.Columns[i].Font.Color;

             xls.SetCellFormat(row, Visible_i, xls.AddFormat(fmt));

             xls.SetCellValue(row, Visible_i, abs(F));
          end;

     xls.SetRowHeight(row, 582);  //23.25 * 25
     inc(row);
   end;
end;

procedure TxlsGrid.BandGroupHeader(const xls: TExcelFile);
var
  fmt:TFlxFormat;
  I : Integer;
begin
 //Grid is grouped bro.
 if Assigned(GroupField) then
 if GroupValue<>GroupField.DisplayText then
    begin
      //Sum Of Old Group
      if Grid.DataSource.DataSet.RecNo>1 then
         BandGroupFooter(xls);


      //New Group
      rowG:=1;
      GroupValue:=GroupField.DisplayText;

      //Merged Cells
      xls.MergeCells(row, 1, row, Visible_Cols);

      fmt := xls.GetStyle('Normal 2', true);
      fmt.Font.Style := [TFlxFontStyles.Bold];
      fmt.Font.Size20 := 280;
      fmt.Borders.Bottom.Style := TFlxBorderStyle.Thin;
      fmt.Borders.Bottom.Color := TUIColor.FromArgb($00, $99, $00);
      if isRTL then
         fmt.HAlignment:=THFlxAlignment.right
      else
         fmt.HAlignment:=THFlxAlignment.left;

      for I := 1 to Visible_Cols do
         xls.SetCellFormat(row, i, xls.AddFormat(fmt));

      xls.SetRowHeight(row, 582);  //23.25 * 25
      xls.SetCellValue(row, 1, GroupValue);

      inc(row);
    end;

end;

procedure TxlsGrid.BandSum(const xls: TExcelFile);
var
 i :smallint;
begin
   with grid do
    if Summary.Enabled then
      for I := 0 to Columns.Count-1 do
      if Columns[i].Visible then
        begin
           if not Columns[i].ShowSummary              then  continue;
           if not (Columns[i].Field.DataType=ftFloat) then  continue;

          if Columns[i].AuxValue=NULL then Columns[i].AuxValue:=0.0;
          Columns[i].AuxValue    :=Columns[i].AuxValue     + Columns[i].Field.AsFloat;
        end;
end;

procedure TxlsGrid.CreateFile;
var
 xls: TXlsFile;
begin
      xls := TXlsFile.Create(true);
      try
        ReportHeader(xls);
        BandData(xls);
        xls.Save(FileName);
      finally
        xls.Free;
      end
end;

class procedure TxlsGrid.ExportXls(aGrid:TUniDBGrid;aHeader,aFooter,aFileName: String;
           isRTLSheet:boolean=false;uzGridFontColor:boolean=false);
var
  G:TxlsGrid;
begin
 G:=TxlsGrid.Create;
 with G do
   try
      Header:=aHeader;
      Footer:=aFooter;
      FileName:=aFileName;
      Grid:= aGrid;
      isRTL:=isRTLSheet;
      useGridFontColor:=uzGridFontColor;
      CreateFile();
   finally
     G.free;
   end;

end;

procedure TxlsGrid.ReportHeader(const xls: TExcelFile);
var
  StyleFmt: TFlxFormat;
  MajorLatin: TThemeTextFont;
  MajorEastAsian: TThemeTextFont;
  MajorComplexScript: TThemeTextFont;
  MajorFont: TThemeFont;
  MinorLatin: TThemeTextFont;
  MinorEastAsian: TThemeTextFont;
  MinorComplexScript: TThemeTextFont;
  MinorFont: TThemeFont;
  fmt: TFlxFormat;
  i , Visible_i:SmallInt;
  w:integer;
begin
  xls.NewFile(1, TExcelFileFormat.v2019);  //Create a new Excel file with 1 sheets.

  //Set the names of the sheets
  xls.ActiveSheet := 1;
  xls.SheetName   := Header;
  xls.ActiveSheet := 1;  //Set the sheet we are working in.
  //Global Workbook Options
  xls.OptionsCheckCompatibility := false;
  //Sheet Options
  //There are 2 ways to set the sheet options. You can use the code above to set them all, or the commented code after it to set them one by one.
  //xls.SheetOptions := TSheetOptions(246);
  xls.SheetIsRightToLeft := isRTL;
  //Styles.
  StyleFmt := xls.GetStyle(xls.GetBuiltInStyleName(TBuiltInStyle.Normal, 0));
  StyleFmt.Font.CharSet := 162;
  xls.SetStyle('Normal 2', StyleFmt);

  //Theme - You might use GetTheme/SetTheme methods here instead.
  xls.SetColorTheme(TThemeColor.Background2, TUIColor.FromArgb($EE, $EC, $E1));
  xls.SetColorTheme(TThemeColor.Foreground2, TUIColor.FromArgb($1F, $49, $7D));
  xls.SetColorTheme(TThemeColor.Accent1, TUIColor.FromArgb($4F, $81, $BD));
  xls.SetColorTheme(TThemeColor.Accent2, TUIColor.FromArgb($C0, $50, $4D));
  xls.SetColorTheme(TThemeColor.Accent3, TUIColor.FromArgb($9B, $BB, $59));
  xls.SetColorTheme(TThemeColor.Accent4, TUIColor.FromArgb($80, $64, $A2));
  xls.SetColorTheme(TThemeColor.Accent5, TUIColor.FromArgb($4B, $AC, $C6));
  xls.SetColorTheme(TThemeColor.Accent6, TUIColor.FromArgb($F7, $96, $46));
  xls.SetColorTheme(TThemeColor.HyperLink, TUIColor.FromArgb($00, $00, $FF));
  xls.SetColorTheme(TThemeColor.FollowedHyperLink, TUIColor.FromArgb($80, $00, $80));

  //Major font
  MajorLatin := TThemeTextFont.Create('Cambria', '020F0302020204030204', TPitchFamily.DEFAULT_PITCH__UNKNOWN_FONT_FAMILY, TFontCharSet.Default);
  MajorEastAsian := TThemeTextFont.Create('', '', TPitchFamily.DEFAULT_PITCH__UNKNOWN_FONT_FAMILY, TFontCharSet.Default);
  MajorComplexScript := TThemeTextFont.Create('', '', TPitchFamily.DEFAULT_PITCH__UNKNOWN_FONT_FAMILY, TFontCharSet.Default);
  MajorFont := TThemeFont.Create(MajorLatin, MajorEastAsian, MajorComplexScript);
  MajorFont.AddFont('Jpan', 'ＭＳ Ｐゴシック');
  MajorFont.AddFont('Hang', '맑은 고딕');
  MajorFont.AddFont('Hans', '宋体');
  MajorFont.AddFont('Hant', '新細明體');
  MajorFont.AddFont('Arab', 'Times New Roman');
  MajorFont.AddFont('Hebr', 'Times New Roman');
  MajorFont.AddFont('Thai', 'Tahoma');
  MajorFont.AddFont('Ethi', 'Nyala');
  MajorFont.AddFont('Beng', 'Vrinda');
  MajorFont.AddFont('Gujr', 'Shruti');
  MajorFont.AddFont('Khmr', 'MoolBoran');
  MajorFont.AddFont('Knda', 'Tunga');
  MajorFont.AddFont('Guru', 'Raavi');
  MajorFont.AddFont('Cans', 'Euphemia');
  MajorFont.AddFont('Cher', 'Plantagenet Cherokee');
  MajorFont.AddFont('Yiii', 'Microsoft Yi Baiti');
  MajorFont.AddFont('Tibt', 'Microsoft Himalaya');
  MajorFont.AddFont('Thaa', 'MV Boli');
  MajorFont.AddFont('Deva', 'Mangal');
  MajorFont.AddFont('Telu', 'Gautami');
  MajorFont.AddFont('Taml', 'Latha');
  MajorFont.AddFont('Syrc', 'Estrangelo Edessa');
  MajorFont.AddFont('Orya', 'Kalinga');
  MajorFont.AddFont('Mlym', 'Kartika');
  MajorFont.AddFont('Laoo', 'DokChampa');
  MajorFont.AddFont('Sinh', 'Iskoola Pota');
  MajorFont.AddFont('Mong', 'Mongolian Baiti');
  MajorFont.AddFont('Viet', 'Times New Roman');
  MajorFont.AddFont('Uigh', 'Microsoft Uighur');
  MajorFont.AddFont('Geor', 'Sylfaen');
  xls.SetThemeFont(TFontScheme.Major, MajorFont);

  //Minor font
  MinorLatin := TThemeTextFont.Create('Calibri', '020F0502020204030204', TPitchFamily.DEFAULT_PITCH__UNKNOWN_FONT_FAMILY, TFontCharSet.Default);
  MinorEastAsian := TThemeTextFont.Create('', '', TPitchFamily.DEFAULT_PITCH__UNKNOWN_FONT_FAMILY, TFontCharSet.Default);
  MinorComplexScript := TThemeTextFont.Create('', '', TPitchFamily.DEFAULT_PITCH__UNKNOWN_FONT_FAMILY, TFontCharSet.Default);
  MinorFont := TThemeFont.Create(MinorLatin, MinorEastAsian, MinorComplexScript);
  MinorFont.AddFont('Jpan', 'ＭＳ Ｐゴシック');
  MinorFont.AddFont('Hang', '맑은 고딕');
  MinorFont.AddFont('Hans', '宋体');
  MinorFont.AddFont('Hant', '新細明體');
  MinorFont.AddFont('Arab', 'Arial');
  MinorFont.AddFont('Hebr', 'Arial');
  MinorFont.AddFont('Thai', 'Tahoma');
  MinorFont.AddFont('Ethi', 'Nyala');
  MinorFont.AddFont('Beng', 'Vrinda');
  MinorFont.AddFont('Gujr', 'Shruti');
  MinorFont.AddFont('Khmr', 'DaunPenh');
  MinorFont.AddFont('Knda', 'Tunga');
  MinorFont.AddFont('Guru', 'Raavi');
  MinorFont.AddFont('Cans', 'Euphemia');
  MinorFont.AddFont('Cher', 'Plantagenet Cherokee');
  MinorFont.AddFont('Yiii', 'Microsoft Yi Baiti');
  MinorFont.AddFont('Tibt', 'Microsoft Himalaya');
  MinorFont.AddFont('Thaa', 'MV Boli');
  MinorFont.AddFont('Deva', 'Mangal');
  MinorFont.AddFont('Telu', 'Gautami');
  MinorFont.AddFont('Taml', 'Latha');
  MinorFont.AddFont('Syrc', 'Estrangelo Edessa');
  MinorFont.AddFont('Orya', 'Kalinga');
  MinorFont.AddFont('Mlym', 'Kartika');
  MinorFont.AddFont('Laoo', 'DokChampa');
  MinorFont.AddFont('Sinh', 'Iskoola Pota');
  MinorFont.AddFont('Mong', 'Mongolian Baiti');
  MinorFont.AddFont('Viet', 'Arial');
  MinorFont.AddFont('Uigh', 'Microsoft Uighur');
  MinorFont.AddFont('Geor', 'Sylfaen');
  xls.SetThemeFont(TFontScheme.Minor, MinorFont);


  RedColor  :=TUIColor.FromArgb($FF, $00, $00); //Red
  GreenColor:=TUIColor.FromArgb($00, $99, $00); //Green
  BlackColor:=TUIColor.FromArgb($10, $10, $10); //Black

  Visible_i:=0;
  //Set up rows and columns
  for I := 0 to grid.Columns.Count-1 do
  if grid.Columns[i].Visible then
    begin
      inc(Visible_i);
      w:=Trunc(grid.Columns[i].Width/5 + 0.75)* 256;
      if w>10000 then w:= 10000;

      xls.SetColWidth(Visible_i, Visible_i, w);  //3766=(13.96 + 0.75) * 256
    end;

  Visible_Cols:=Visible_i;

  //Grid Column Headers
  fmt := xls.GetStyle('Normal 2', true);
  fmt.Font.Style := [TFlxFontStyles.Bold];
  fmt.Font.Size20 := 240;
  fmt.Borders.Left.Style := TFlxBorderStyle.Thin;
  fmt.Borders.Left.Color := TUIColor.FromArgb($00, $99, $00);
  fmt.Borders.Top.Style := TFlxBorderStyle.Thin;
  fmt.Borders.Top.Color := TUIColor.FromArgb($00, $99, $00);;
  fmt.Borders.Bottom.Style := TFlxBorderStyle.Thin;
  fmt.Borders.Bottom.Color := TUIColor.FromArgb($00, $99, $00);
  fmt.Borders.Right.Style := TFlxBorderStyle.Thin;
  fmt.Borders.Right.Color := TUIColor.FromArgb($00, $99, $00);
  fmt.FillPattern.Pattern := TFlxPatternStyle.Solid;
  fmt.FillPattern.FgColor := TUIColor.FromArgb($A2, $CC, $86);
  fmt.FillPattern.BgColor := TExcelColor.Automatic;
  fmt.VAlignment := TVFlxAlignment.center;

  Visible_i:=0;
  for I := 0 to grid.Columns.Count-1 do
  if grid.Columns[i].Visible then
    begin
       inc(Visible_i);
       fmt.HAlignment := THFlxAlignment.general;
       if Grid.Columns[i].Field.DataType=ftFloat then
          fmt.HAlignment := THFlxAlignment.right;

       xls.SetCellFormat(1, Visible_i, xls.AddFormat(fmt));
       xls.SetCellValue(1, Visible_i, grid.Columns[i].Title.Caption);
    end;

   //Prepear Grouping
  GroupField:=nil;
  if grid.Grouping.Enabled then
     GroupField:=grid.DataSource.DataSet.FieldByName(grid.Grouping.FieldName);
end;

end.
