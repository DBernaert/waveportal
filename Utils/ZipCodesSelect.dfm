object ZipCodesSelectFrm: TZipCodesSelectFrm
  Left = 0
  Top = 0
  ClientHeight = 449
  ClientWidth = 579
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = GridZipCodes
  Images = UniMainModule.ImageList
  ImageIndex = 57
  OnSubmit = BtnSelectClick
  OnCancel = BtnCloseClick
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridZipCodes: TUniDBGrid
    Left = 16
    Top = 16
    Width = 545
    Height = 369
    Hint = ''
    DataSource = DsZipCodes
    Options = [dgTitles, dgIndicator, dgColumnResize, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    TabOrder = 0
    OnColumnSort = GridZipCodesColumnSort
    OnDblClick = BtnSelectClick
    Columns = <
      item
        FieldName = 'ZIPCODE'
        Title.Caption = 'Postcode'
        Width = 100
        Sortable = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'CITY'
        Title.Caption = 'Locatie'
        Width = 64
        Sortable = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object BtnSelect: TUniThemeButton
    Left = 280
    Top = 400
    Width = 137
    Height = 33
    Hint = ''
    Caption = 'Selecteren'
    TabOrder = 1
    OnClick = BtnSelectClick
    ButtonTheme = uctSuccess
  end
  object BtnClose: TUniThemeButton
    Left = 424
    Top = 400
    Width = 137
    Height = 33
    Hint = ''
    Caption = 'Sluiten'
    TabOrder = 2
    OnClick = BtnCloseClick
    ButtonTheme = uctSecondary
  end
  object ZipCodes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO ZIPCODES'
      '  (ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY)'
      'VALUES'
      '  (:ID, :COMPANYID, :COUNTRYCODE, :ZIPCODE, :CITY)'
      'RETURNING '
      '  ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY')
    SQLDelete.Strings = (
      'DELETE FROM ZIPCODES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE ZIPCODES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, COUNTRYCODE = :COUNTRYCODE, ' +
        'ZIPCODE = :ZIPCODE, CITY = :CITY'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY FROM ZIPCODES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM ZIPCODES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM ZIPCODES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from zipcodes')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 272
    Top = 112
  end
  object DsZipCodes: TDataSource
    DataSet = ZipCodes
    Left = 272
    Top = 168
  end
  object LinkedLanguageZipCodesSelect: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'ZipCodes.SQLDelete'
      'ZipCodes.SQLInsert'
      'ZipCodes.SQLLock'
      'ZipCodes.SQLRecCount'
      'ZipCodes.SQLRefresh'
      'ZipCodes.SQLUpdate'
      'TZipCodesSelectFrm.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageZipCodesSelectChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 272
    Top = 224
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00420074006E00530065006C00650063007400010053006500
      6C006500630074006500720065006E0001005300E9006C006500630074006900
      6F006E006E00650072000100530065006C0065006300740001000D000A004200
      74006E0043006C006F0073006500010053006C0075006900740065006E000100
      4600650072006D0065007200010043006C006F007300650001000D000A007300
      7400480069006E00740073005F0055006E00690063006F00640065000D000A00
      7300740044006900730070006C00610079004C006100620065006C0073005F00
      55006E00690063006F00640065000D000A007300740046006F006E0074007300
      5F0055006E00690063006F00640065000D000A00730074004D0075006C007400
      69004C0069006E00650073005F0055006E00690063006F00640065000D000A00
      7300740053007400720069006E00670073005F0055006E00690063006F006400
      65000D000A00730074007200730074007200530065006C006500630074004C00
      6F0063006100740069006F006E000100530065006C0065006300740065007200
      65006E0020006C006F006300610074006900650001005300E9006C0065006300
      740069006F006E006E006500720020006C006900650075000100530065006C00
      65006300740020006C006F0063006100740069006F006E0001000D000A007300
      74007200730074007200530065006C006500630074005A006900700043006F00
      640065000100530065006C006500630074006500720065006E00200070006F00
      7300740063006F006400650001005300E9006C0065006300740069006F006E00
      6E0065007200200063006F0064006500200070006F007300740061006C000100
      530065006C00650063007400200070006F007300740061006C00200063006F00
      6400650001000D000A00730074004F0074006800650072005300740072006900
      6E00670073005F0055006E00690063006F00640065000D000A00730074004300
      6F006C006C0065006300740069006F006E0073005F0055006E00690063006F00
      640065000D000A0047007200690064005A006900700043006F00640065007300
      2E0043006F006C0075006D006E0073005B0030005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A0047007200690064005A0069007000
      43006F006400650073002E0043006F006C0075006D006E0073005B0030005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001005000
      6F007300740063006F0064006500010043006F0064006500200070006F007300
      740061006C00010050006F007300740061006C00200063006F00640065000100
      0D000A0047007200690064005A006900700043006F006400650073002E004300
      6F006C0075006D006E0073005B0031005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A0047007200690064005A006900700043006F00
      6400650073002E0043006F006C0075006D006E0073005B0031005D002E005400
      690074006C0065002E00430061007000740069006F006E0001004C006F006300
      610074006900650001004C0069006500750001004C006F006300610074006900
      6F006E0001000D000A0073007400430068006100720053006500740073005F00
      55006E00690063006F00640065000D000A00}
  end
end
