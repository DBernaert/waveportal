unit ReportPreview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniPanel, uniGUIBaseClasses, uniURLFrame,
  siComp, siLngLnk;

type
  TReportPreviewFrm = class(TUniForm)
    URLFrame: TUniURLFrame;
    PanelFooter: TUniContainerPanel;
    ContainerButtons: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    LinkedLanguageReportPreview: TsiLangLinked;
    procedure BtnCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Preview_document(Filename: string);
  end;

function ReportPreviewFrm: TReportPreviewFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function ReportPreviewFrm: TReportPreviewFrm;
begin
  Result := TReportPreviewFrm(UniMainModule.GetFormInstance(TReportPreviewFrm));
end;

procedure TReportPreviewFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TReportPreviewFrm.Preview_document(Filename: string);
begin
  WindowState  := WsMaximized;
  UrlFrame.URL := FileName;
end;

end.
