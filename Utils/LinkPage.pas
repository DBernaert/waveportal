unit LinkPage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniLabel, uniButton,
  UniThemeButton, siComp, siLngLnk, uniTimer;

type
  TLinkPageFrm = class(TUniForm)
    LblLink2: TUniLabel;
    BtnClose: TUniThemeButton;
    LinkedLanguageLinkPage: TsiLangLinked;
    CloseTimer: TUniTimer;
    procedure BtnCloseClick(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Initialize_linkpage(URL: string);
  end;

function LinkPageFrm: TLinkPageFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function LinkPageFrm: TLinkPageFrm;
begin
  Result := TLinkPageFrm(UniMainModule.GetFormInstance(TLinkPageFrm));
end;

{ TLinkPageFrm }

procedure TLinkPageFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TLinkPageFrm.CloseTimerTimer(Sender: TObject);
begin
  BtnCloseClick(nil);
end;

procedure TLinkPageFrm.Initialize_linkpage(URL: string);
begin
  LblLink2.Caption := '<a href="file:///' + StringReplace(URL, '\', '/', [RfReplaceAll]) + '" target="_blank">Openen extern document</a>';
end;

procedure TLinkPageFrm.UniFormShow(Sender: TObject);
begin
  CloseTimer.Enabled := True;
end;

end.
