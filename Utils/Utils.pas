unit Utils;


interface

uses Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Graphics, System.Classes, System.UITypes,
     Db, VCL.FlexCel.Core, FlexCel.XlsAdapter;

function   GetSoftwareVersion: String;
function   GetSoftwareVersionFormatted: String;
function   Crypt(const strText: string; const intKey: longint): string;
function   Decrypt(const strText: string; const intKey: longint): string;
function   RoundTo2dp(Value: Currency): Currency;
function   EscapeIllegalChars(AString: string): string;
function   RemoveQuotes(AString: string): string;
function   IsMatch(const Input, Pattern: string): boolean;
function   IsValidEmailRegEx(const EmailAddress: string): boolean;
function   MakeRandomString(const ALength: Integer;
           const ACharSequence: String = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'): String;
function   CreateSearchString(AString: string): string;
function   ExtractUrlFileName(const AUrl: string): string;
procedure  DumpDataSet(const ds: TDataSet; FileName: string);
function   Calculate_distance(Start, Destination: string): integer;
function   SearchValue(AString: string): string;
function   Format_mobile_phone(line: string): string;
function   Format_belgium_phone(line: string): string;
function   Check_nis_number(NumberToCheck: string): boolean;
function   Format_nis_number(line: string): string;
function   Capitalize_string(line: string): string;
function   Capitalize_first_letter(line: string): string;
function   CheckIban(iban: string): Boolean;
function   BEcheck(VatNumber: string): string;

const
  intDefKey = -967283;

implementation

uses Windows, SysUtils, Forms, System.Math, System.RegularExpressions, XSuperObject, REST.Client;

Function BEcheck(VatNumber: string): string;
var Vat_racine_alfa: string;
    Vat_racine:      real;
    Vat_rest:        real;
    Vat_quotient:    real;
    Calculated_rest: real;
    I:               integer;
    Valid:           boolean;
begin
  Result    := '';
  VatNumber := Trim(VatNumber);
  VatNumber := TRegEx.Replace(VatNumber, '\D', '');

  if Copy(VatNumber, 1, 10) = ''
  then begin
         Result := '';
         Exit;
       end;

  if (Copy(VatNumber, 1, 1) <> ' ') and
     (Copy(VatNumber, 1, 1) <> '0') and
     (Copy(VatNumber, 1, 1) <> '1')
  then begin
         Result := '';
         Exit;
       end;

  if Length(VatNumber) = 9
  then Vat_racine_alfa := VatNumber
  else Vat_racine_alfa := Copy(VatNumber, 2, length(VatNumber));

  if Length(Vat_racine_alfa) <> 9
  then  begin
          Result := '';
          Exit;
        end;

  Valid := True;
  for I := 1 to 9
  do begin
       if (Vat_racine_alfa [I] < '0') or
          (Vat_racine_alfa [I] > '9')
       then Valid := False;
     end;

  if Valid = False
  then begin
         Result := '';
         Exit;
       end;

  Try
    Vat_racine      := StrToFloat(Copy(Vat_racine_alfa, 1, 7));
    Vat_rest        := StrToFloat(Copy(Vat_racine_alfa, 8, 2));
    Vat_quotient    := Int(Vat_racine / 97);
    Calculated_rest := 97 - (Vat_racine - (Vat_quotient * 97));
    if Calculated_rest <> Vat_rest
    then Result := ''
    else begin
           if Length(Vat_racine_alfa) = 9
           then Vat_racine_alfa := '0' + Vat_racine_alfa;
           Result := 'BE' + copy(Vat_racine_alfa, 1, 4) + '.' +
                            copy(Vat_racine_alfa, 5, 3) + '.' +
                            copy(Vat_racine_alfa, 8, 3);
         end;
  Except
    Result := '';
  End;
End;

function ChangeAlpha(input: string): string;
  // A -> 10, B -> 11, C -> 12 ...
var
  a: Char;
begin
  Result := input;
  for a := 'A' to 'Z' do
  begin
    Result := StringReplace(Result, a, IntToStr(Ord(a) - 55), [rfReplaceAll]);
  end;
end;

function CalculateDigits(iban: string): Integer;
var
  v, l: Integer;
  alpha: string;
  number: Longint;
  rest: Integer;
begin
  iban := UpperCase(iban);
  if Pos('IBAN', iban) > 0 then
    Delete(iban, Pos('IBAN', iban), 4);
  iban := iban + Copy(iban, 1, 4);
  Delete(iban, 1, 4);
  iban := ChangeAlpha(iban);
  v := 1;
  l := 9;
  rest := 0;
  alpha := '';
  try
    while v <= Length(iban) do
    begin
      if l > Length(iban) then
        l := Length(iban);
      alpha := alpha + Copy(iban, v, l);
      number := StrToInt(alpha);
      rest := number mod 97;
      v := v + l;
      alpha := IntToStr(rest);
      l := 9 - Length(alpha);
    end;
  except
    rest := 0;
  end;
  Result := rest;
end;

function CheckIBAN(iban: string): Boolean;
begin
  iban := StringReplace(iban, ' ', '', [rfReplaceAll]);
  if CalculateDigits(iban) = 1 then
    Result := True
  else
    Result := False;
end;

function Format_belgium_phone(line: string): string;
begin
  line := TRegEx.Replace(line, '\D', '');

  line := CreateSearchString(line);
  if copy(line, 1, 3) = '+32'
  then line := copy(line, 4, length(line));

  if copy(line, 1, 4) = '0032'
  then line := copy(line, 5, length(line));

  if copy(line, 1, 2) = '32'
  then line := copy(line, 3, length(line));

  if copy(line, 1, 1) = '0'
  then line := copy(line, 2, length(line));

  line := trim(line);

  if length(line) = 9
  then line := copy(line, 1, 3) + ' ' + copy(line, 4, 2) + ' ' + copy(line, 6, 2) + ' ' + copy(line, 8, 2);

  if trim(line) = ''
  then Result := line
  else Result := '+32 ' + line;
end;

function Capitalize_first_letter(line: string): string;
begin
  Result := UpperCase(Copy(line, 1, 1)) + LowerCase(Copy(line, 2, length(line)));
end;

function Capitalize_string(line: string): string;
begin
  Result := UpperCase(Line);
end;

function Format_nis_number(line: string): string;
begin
  line   := TRegEx.Replace(line, '\D', '');
  line   := CreateSearchString(line);
  line   := StringReplace(line, '.', '', [rfReplaceAll]);
  line   := StringReplace(line, ',', '', [rfReplaceAll]);
  line   := copy(line, 1, 2) + '.' + copy(line, 3, 2) + '.' + copy(line, 5, 2) + '-' + copy(line, 7,3) + '.' + copy(line, 10, 2);
  Result := line;
end;

Function Format_mobile_phone(line: string): string;
begin
  Result := TRegEx.Replace(line, '\D', '');
  if copy(Result, 1, 1) = '0'
  then Result := Copy(Result, 2, length(Result));

  if copy(Result, 1, 3) <> '+32'
  then Result := '+32' + Result;
end;

function Check_nis_number(NumberToCheck: string): boolean;
var FirstPart:  Integer;
    CheckPart:  Integer;
    CalculatedPart: real;
begin
  NumberToCheck  := StringReplace(NumberToCheck, ' ', '', [rfReplaceAll, rfIgnoreCase]);
  NumberToCheck  := TRegEx.Replace(NumberToCheck, '\D', '');
  if length(NumberToCheck) <> 11
  then begin
         Result := False;
         Exit;
       end;

  Try
    FirstPart      := StrToInt(Copy(NumberToCheck, 1, 9));
    CheckPart      := StrToInt(Copy(NumberToCheck, 10, 2));
    CalculatedPart := Int(FirstPart / 97);
    CalculatedPart := 97 - (FirstPart - (CalculatedPart * 97));
    if CalculatedPart = CheckPart
    then begin
           Result := True;
           Exit;
         end
    else begin
           FirstPart      := FirstPart + 2000000000;
           CalculatedPart := Int(FirstPart / 97);
           CalculatedPart := 97 - (FirstPart - (CalculatedPart * 97));
           if CalculatedPart = CheckPart
           then begin
                  Result := True;
                  Exit;
                end
           else begin
                  Result := False;
                  Exit;
                end;
         end;
  Except
    Result := False;
  End;
end;

function Calculate_distance(Start, Destination: string): integer;
var Client:     TRESTClient;
    Request:    TRESTRequest;
    Response:   TRESTResponse;
    X:          ISuperObject;
    Obj:        ISuperObject;
begin
  if Trim(Start) = ''
  then begin
         Result := 0;
         exit;
       end;

  if Trim(Destination) = ''
  then begin
         Result := 0;
         Exit;
       end;

  try
    Client    := TRESTClient.Create(nil);
    Client.BaseURL := 'https://maps.googleapis.com/maps/api/distancematrix/json';
    Request   := TRESTRequest.Create(nil);
    Response  := TRESTResponse.Create(nil);
    Request.Client := Client;
    Request.Response := Response;

    Request.Params.Clear;
    Request.Params.AddItem;
    Request.Params[0].Name  := 'units';
    Request.Params[0].Value := 'metric';

    Request.Params.AddItem;
    Request.Params[1].Name  := 'origins';
    Request.Params[1].Value := Start;

    Request.Params.AddItem;
    Request.Params[2].Name  := 'destinations';
    Request.Params[2].Value := Destination;

    Request.Params.AddItem;
    Request.Params[3].Name  := 'language';
    Request.Params[3].Value := 'nl';

    Request.Params.AddItem;
    Request.Params[4].Name  := 'key';
    Request.Params[4].Value := 'AIzaSyDaFLtgeEgH-ifrL8ftJjiwGa7pH5Qqd0w';

    Request.Execute;
    X := TSuperObject.Create(Response.Content);
    Obj := X.A['rows'].O[0];
    Obj := Obj.A['elements'].O[0];
    Result := Obj['distance."value"'].AsInteger;
    //ShowMessage(Obj['distance."value"'].AsString); //asInteger
  except
    Result := 0;
  end;
end;

procedure DumpDataSet(const ds: TDataSet; FileName: string);
var
  xls: TXlsFile;
  Row, Col: integer;
  Fmt: TFlxFormat;
  DateXF, DateTimeXF: integer;
begin
  xls := TXlsFile.Create(1, TExcelFileFormat.v2019, true);
  try
    //Generate the formats we will be using to format dates and times.
    Fmt := xls.GetDefaultFormat;
    Fmt.Format := 'dd/mm/yyyy hh:mm';
    DateTimeXF := xls.AddFormat(Fmt);

    Fmt := xls.GetDefaultFormat;
    Fmt.Format := 'dd/mm/yyyy';
    DateXF := xls.AddFormat(Fmt);

    //Now loop over all records and send them to the file.
    for Col := 1 to ds.FieldCount
    do begin
         xls.SetCellValue(1, Col, ds.Fields[col - 1].FieldName);
       end;

    ds.First;
    Row := 2;
    while not ds.Eof do
    begin
      for Col := 1 to ds.FieldCount do
      begin
        case ds.Fields[Col - 1].DataType of
          TFieldType.ftDateTime:
          begin
            xls.SetCellValue(Row, Col, ds.Fields[col - 1].AsDateTime, DateTimeXF);
          end;

          TFieldType.ftDate:
          begin
            xls.SetCellValue(Row, Col, ds.Fields[col - 1].AsDateTime, DateXF);
          end;

          else
          begin
            xls.SetCellValue(Row, Col, ds.Fields[col - 1].Value);
          end;
        end;
      end;

      ds.Next;
      Inc(Row);
    end;

    xls.Save(FileName);
  finally
    xls.Free;
  end;
end;

function ExtractUrlFileName(const AUrl: string): string;
var i: Integer;
begin
  i := LastDelimiter('/', AUrl);
  Result := Copy(AUrl, i + 1, Length(AUrl) - (i));
end;

function CreateSearchString(AString: string): string;
begin
  Result := StringReplace(AString, ' ', '', [rfReplaceAll]);
end;

function IsMatch(const Input, Pattern: string): boolean;
begin
  Result := TRegEx.IsMatch(Input, Pattern);
end;

function IsValidEmailRegEx(const EmailAddress: string): boolean;
const
  EMAIL_REGEX = '^((?>[a-zA-Z\d!#$%&''*+\-/=?^_`{|}~]+\x20*|"((?=[\x01-\x7f])'
             +'[^"\\]|\\[\x01-\x7f])*"\x20*)*(?<angle><))?((?!\.)'
             +'(?>\.?[a-zA-Z\d!#$%&''*+\-/=?^_`{|}~]+)+|"((?=[\x01-\x7f])'
             +'[^"\\]|\\[\x01-\x7f])*")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]'
             +'{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d))'
             +'{4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\'
             +'[\x01-\x7f])+)\])(?(angle)>)$';
begin
  Result := IsMatch(EmailAddress, EMAIL_REGEX);
end;

function MakeRandomString(const ALength: Integer;
                          const ACharSequence: String = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'): String;
var
  C1, sequence_length: Integer;
begin
  sequence_length := Length(ACharSequence);
  SetLength(result, ALength);
  for C1 := 1 to ALength
  do result[C1] := ACharSequence[Random(sequence_length) + 1];
end;

function RoundTo2dp(Value: Currency): Currency;
begin
  Result := Trunc(Value*100+IfThen(Value>0, 0.5, -0.5))/100;
end;

function SearchValue(AString: string): string;
begin
  AString := RemoveQuotes(AString);
  AString := StringReplace(Astring, ' ', '', [rfReplaceAll, rfIgnoreCase]);
  Astring := QuotedStr('%' + UpperCase(AString) + '%');
  Result  := AString;
end;

function RemoveQuotes(AString: string): string;
begin
  Result := stringreplace(Astring, '''', '', [rfReplaceAll,rfIgnoreCase]);
end;

function EscapeIllegalChars(AString: string): string;
var
  x: integer;
begin
  for x := 1 to Length(AString) do
    if CharInSet(AString[x], ['|','<','>','\','^','+','=','?','/','[',']','"',';',',','*'])
    then AString[x] := '_';
  Result := AString;
end;

function GetSoftwareVersionFormatted: String;
var
  sFileName:    String;
  iBufferSize:  DWORD;
  iDummy:       DWORD;
  pBuffer:      Pointer;
  pFileInfo:    Pointer;
  iVer:         array [1 .. 4] of Word;
begin
  // set default value
  Result      := '';
  sFileName   := Application.Exename;
  iBufferSize := GetFileVersionInfoSize(PChar(sFileName), iDummy);
  if (iBufferSize > 0)
  then begin
         GetMem(pBuffer, iBufferSize);
         try
           // get fixed file info (language independent)
           GetFileVersionInfo(PChar(sFileName), 0, iBufferSize, pBuffer);
           VerQueryValue(pBuffer, '\', pFileInfo, iDummy);
           // read version blocks
           iVer[1] := HiWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionMS);
           iVer[2] := LoWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionMS);
           iVer[3] := HiWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionLS);
           iVer[4] := LoWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionLS);
         finally
           FreeMem(pBuffer);
         end;
         // format result string
         Result := Format('%d.%d.%d.%d', [iVer[1], iVer[2], iVer[3], iVer[4]]);
       end;
end;

function GetSoftwareVersion: String;
var
  sFileName:     String;
  iBufferSize:   DWORD;
  iDummy:        DWORD;
  pBuffer:       Pointer;
  pFileInfo:     Pointer;
  iVer:          array [1 .. 4] of Word;
begin
  Result      := '';
  sFileName   := Application.Exename;
  iBufferSize := GetFileVersionInfoSize(PChar(sFileName), iDummy);
  if (iBufferSize > 0)
  then begin
         GetMem(pBuffer, iBufferSize);
         try
           // get fixed file info (language independent)
           GetFileVersionInfo(PChar(sFileName), 0, iBufferSize, pBuffer);
           VerQueryValue(pBuffer, '\', pFileInfo, iDummy);
           // read version blocks
           iVer[1] := HiWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionMS);
           iVer[2] := LoWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionMS);
           iVer[3] := HiWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionLS);
           iVer[4] := LoWord(PVSFixedFileInfo(pFileInfo)^.dwFileVersionLS);
         finally
           FreeMem(pBuffer);
         end;
         // format result string
         Result := Format('%d%d%d%d', [iVer[1], iVer[2], iVer[3], iVer[4]]);
       end;
end;

function Crypt(const strText: string; const intKey: longint): string;
var
  i: integer;
  strResult: string;
begin
  strResult := strText;
  RandSeed  := intKey;
  for i := 1 to Length(strText)
  do strResult[i] := Chr(Ord(strResult[i]) xor Random(255));
  Crypt     := strResult;
end;

function Decrypt(const strText: string; const intKey: longint): string;
begin
  Decrypt := Crypt(strText, intKey);
end;

end.
