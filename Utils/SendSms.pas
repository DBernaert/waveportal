unit SendSms;

interface

uses IdURI, Vcl.StdCtrls, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
     IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection,
     IdTCPClient, IdHTTP, System.Classes, System.SysUtils;

function Send_sms(SmsSender: string; SmsReceivers: string; SmsMessage: string): boolean;

implementation

uses MainModule, ServerModule;

function Send_sms(SmsSender: string; SmsReceivers: string; SmsMessage: string): boolean;
var idHTTP:      TIdHTTP;
    ResponseStr: string;
    ReturnCode:  Integer;
    JSonQuery:   string;
    JSonToSend:  TStringStream;
    Access_token: string;
begin
  Access_token := 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1MDAwOTBlNzgwZDVjYzY4OWMzMDI3MTNkYzQzNjg0MDAyMjNmN2U3ZDAzOGQwOT' +
                  'FkMDI2ODQ1ZDA4NTJmNWQ2Y2FjZDA3Njk2Y2E0OWVlIn0.eyJhdWQiOiIzIiwianRpIjoiNzUwMDA5MGU3ODBkNWNjNjg5YzMwMjcxM2RjNDM2' +
                  'ODQwMDIyM2Y3ZTdkMDM4ZDA5MWQwMjY4NDVkMDg1MmY1ZDZjYWNkMDc2OTZjYTQ5ZWUiLCJpYXQiOjE1OTgyNjE2ODksIm5iZiI6MTU5ODI2MT' +
                  'Y4OSwiZXhwIjo0NzUzOTM4ODg5LCJzdWIiOiI2Nzk2NyIsInNjb3BlcyI6W119.kGPBvWt-VdVwvF8Afcc0XgtR49wLIdhmzThWGKRaa2r5yZK' +
                  'ucalBO9wYCKNmC5hcfL0HYg5VdQuP9NHH7dTJCkNj97YlmbYIdUO7UlmUkAYfsVNkYyP7l1TPDKzMeY07Fe_jGBhmJNuyJHyPIteqtuMXqops7' +
                  'Eq2olfe16z6jwL2e-Q23vNBmPLx1W2_nEjfzb2RpaXxWCTNA1SlOR0kIpfMRnNjXRhlYfpoI7pWENd3hvK1UI05ZvRBpWvcYMorIwJvmYP2Otz' +
                  'dBDH86ifFNhExSPDv60xB8mbUnV_9Xmroapz3cWNy517vGp0Wc-imxuQjNMwHsdC77Xh0KJSTjSr-ygWS0Afw7X8FKEiRtgpn-fzwoouBzmRE9' +
                  'ctr3rdWT9B8py6-KnwdSf02if1Ubt_xDCpBMTTtmX9Kj0BEmQSyO2Kqzg4tmgEZZZlqMsGRVNxM3v051XfhpNvmHSbGvmodRw3K2yN6YIsFy6N' +
                  'YJX3nZmekpWxxJc5AxJvHT1afKkdA0pgj3sL_Zk0VF0sAt3tGy-YcuaaH-gPuF6JTt6lllN0-ME6nPm0LGMRA41Sy3chK8vlLoAR-2RNE4qmfT' +
                  'RmER5ACEZTqq03bv5nnkAseuIMG-an6cvtVgFyGOq6fJVeUdNRjU-NQbEB6gTgho14cIESDBQ6b8YqHcXMQ54Y';

  idHTTP := TIdHTTP.Create(nil);
  idHTTP.HTTPOptions := [hoForceEncodeParams];
  idHTTP.Request.BasicAuthentication := False;

  jSonQuery := '{' +
               '"body": "' + SmsMessage + '",'      +
               '"originator": "' + SmsSender + '",' +
               '"recipients": ['                    +
               '"' + SmsReceivers + '"'             +
               '],'                                 +
               '"route": "business"'                +
               '}';

  JSonToSend := TStringStream.Create(JsonQuery, TEncoding.UTF8);

  try
    IdHTTP.Request.Clear;
    IdHTTP.Request.CustomHeaders.Clear;
    IdHTTP.Request.CustomHeaders.Add('Authorization:Bearer ' + Access_Token);
    IdHTTP.Request.ContentType := 'application/json';
    try
      ResponseStr := IdHTTP.Post('https://rest.spryngsms.com/v1/messages', JSonToSend);
      ReturnCode  := IdHTTP.ResponseCode;
    except
    on E: EIdHTTPProtocolException
    do begin
         ReturnCode := IdHTTP.ResponseCode;
       end;
    end;

    if ReturnCode = 200
    then begin
           //Log sms in backlog for invoicing
           //if UniMainModule.Log_sms(SmsReceivers, SmsMessage)
           //then Result := True
           //else Result := False;
           Result := True;
         end
    else Result := False;
  finally
    JSonToSend.Free;
    idHTTP.Free;
  end;
end;

end.
