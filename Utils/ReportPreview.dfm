object ReportPreviewFrm: TReportPreviewFrm
  Left = 0
  Top = 0
  ClientHeight = 693
  ClientWidth = 1005
  Caption = 'Rapport'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'border'
  LayoutConfig.Padding = '16 16 16 16'
  Images = UniMainModule.ImageList
  ImageIndex = 51
  OnCancel = BtnCloseClick
  PixelsPerInch = 96
  TextHeight = 13
  object URLFrame: TUniURLFrame
    Left = 0
    Top = 0
    Width = 1005
    Height = 658
    Hint = ''
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '0 0 16 0'
    Align = alClient
    TabOrder = 0
    ParentColor = False
    Color = clBtnFace
    ExplicitHeight = 660
  end
  object PanelFooter: TUniContainerPanel
    Left = 0
    Top = 658
    Width = 1005
    Height = 35
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 1
    Layout = 'border'
    object ContainerButtons: TUniContainerPanel
      Left = 868
      Top = 0
      Width = 137
      Height = 35
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      ExplicitLeft = 864
      object BtnClose: TUniThemeButton
        Left = 0
        Top = 0
        Width = 137
        Height = 33
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        ScreenMask.Target = Owner
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object LinkedLanguageReportPreview: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TReportPreviewFrm.Layout'
      'PanelFooter.Layout'
      'ContainerButtons.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 488
    Top = 344
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A0054005200650070006F007200740050007200650076006900
      65007700460072006D00010052006100700070006F0072007400010052006100
      700070006F007200740001005200650070006F007200740001000D000A004200
      74006E0043006C006F0073006500010053006C0075006900740065006E000100
      4600650072006D0065007200010043006C006F007300650001000D000A007300
      7400480069006E00740073005F0055006E00690063006F00640065000D000A00
      7300740044006900730070006C00610079004C006100620065006C0073005F00
      55006E00690063006F00640065000D000A007300740046006F006E0074007300
      5F0055006E00690063006F00640065000D000A00730074004D0075006C007400
      69004C0069006E00650073005F0055006E00690063006F00640065000D000A00
      7300740053007400720069006E00670073005F0055006E00690063006F006400
      65000D000A00730074004F00740068006500720053007400720069006E006700
      73005F0055006E00690063006F00640065000D000A007300740043006F006C00
      6C0065006300740069006F006E0073005F0055006E00690063006F0064006500
      0D000A0073007400430068006100720053006500740073005F0055006E006900
      63006F00640065000D000A00}
  end
end
