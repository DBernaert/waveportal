object LinkPageFrm: TLinkPageFrm
  Left = 0
  Top = 0
  ClientHeight = 105
  ClientWidth = 473
  Caption = 'Externe link openen'
  OnShow = UniFormShow
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Images = UniMainModule.ImageList
  ImageIndex = 64
  OnCancel = BtnCloseClick
  PixelsPerInch = 96
  TextHeight = 13
  object LblLink2: TUniLabel
    Left = 16
    Top = 16
    Width = 441
    Height = 13
    Hint = ''
    Alignment = taCenter
    TextConversion = txtHTML
    AutoSize = False
    Caption = ''
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 0
  end
  object BtnClose: TUniThemeButton
    Left = 168
    Top = 56
    Width = 137
    Height = 33
    Hint = ''
    Caption = 'Sluiten'
    TabOrder = 1
    OnClick = BtnCloseClick
    ButtonTheme = uctSecondary
  end
  object LinkedLanguageLinkPage: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TLinkPageFrm.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 224
    Top = 48
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A0054004C0069006E006B005000610067006500460072006D00
      0100450078007400650072006E00650020006C0069006E006B0020006F007000
      65006E0065006E0001004F007500760072006900720020006C00690065006E00
      2000650078007400650072006E00650001004F00700065006E00200065007800
      7400650072006E0061006C0020006C0069006E006B0001000D000A0042007400
      6E0043006C006F0073006500010053006C0075006900740065006E0001004600
      650072006D0065007200010043006C006F007300650001000D000A0073007400
      480069006E00740073005F0055006E00690063006F00640065000D000A007300
      740044006900730070006C00610079004C006100620065006C0073005F005500
      6E00690063006F00640065000D000A007300740046006F006E00740073005F00
      55006E00690063006F00640065000D000A00730074004D0075006C0074006900
      4C0069006E00650073005F0055006E00690063006F00640065000D000A007300
      740053007400720069006E00670073005F0055006E00690063006F0064006500
      0D000A00730074004F00740068006500720053007400720069006E0067007300
      5F0055006E00690063006F00640065000D000A007300740043006F006C006C00
      65006300740069006F006E0073005F0055006E00690063006F00640065000D00
      0A0073007400430068006100720053006500740073005F0055006E0069006300
      6F00640065000D000A00}
  end
  object CloseTimer: TUniTimer
    Interval = 5000
    Enabled = False
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    OnTimer = CloseTimerTimer
    Left = 376
    Top = 32
  end
end
