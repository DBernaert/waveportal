unit Mailparser;

interface

procedure Parse_general_variables(var Body: string);
procedure Parsetemplate(var Body: string; DocumentType: integer; _IdKey: longint; _IdModule: longint; _IdRecordId: longint);
function  Parse_field(FieldId: integer; FieldValue: string; Body: string): string;

implementation

uses SysUtils, MainModule, Utils, EASendMailObjLib_TLB;

procedure Parse_general_variables(var Body: string);
var SQL: string;
begin
  //Process general variables
  UniMainModule.QWork2.Close;
  UniMainModule.QWork2.SQL.Clear;
  SQL := 'Select address, zipcode, city, phone, fax, email, portal_address from companyprofile where id=' + IntToStr(UniMainModule.Company_id);
  UniMainModule.QWork2.SQL.Add(SQL);
  UniMainModule.QWork2.Open;
  Body   := Parse_Field(20000, UniMainModule.User_name, Body);                                                                                        //User name
  //Body   := Parse_Field(20001, UniMainModule.User_firstname, Body);                                                                                   //User firstname
  Body   := Parse_Field(20002, UniMainModule.User_email, Body);                                                                                       //User email
  Body   := Parse_Field(20003, UniMainModule.Company_name, Body);                                                                                     //Company name
  Body   := Parse_Field(20004, UniMainModule.QWork2.FieldByName('Address').AsString, Body);                                                           //Company address
  Body   := Parse_Field(20005, UniMainModule.QWork2.FieldByName('Zipcode').AsString, Body);                                                           //Company zip code
  Body   := Parse_Field(20006, UniMainModule.QWork2.FieldByName('City').AsString,    Body);                                                           //Company city
  Body   := Parse_Field(20007, UniMainModule.QWork2.FieldByName('Phone').AsString,   Body);                                                           //Company phone
  Body   := Parse_Field(20008, UniMainModule.QWork2.FieldByName('Fax').AsString,     Body);                                                           //Company fax
  Body   := Parse_Field(20009, UniMainModule.QWork2.FieldByName('Email').AsString,   Body);                                                           //Company email
  Body   := Parse_Field(20010, FormatDatetime('dd/mm/yyyy', Date),                   Body);                                                           //Current date
  //Body   := Parse_Field(20011, UniMainModule.User_function, Body);                                                                                    //User function
  //Body   := Parse_Field(20012, UniMainModule.User_representation, Body);                                                                              //User representation
  Body   := Parse_Field(1000,  'https://wavedesk.cloud/WaveDeskPortal/?Portal=' + UniMainModule.QWork2.FieldByName('Portal_address').AsString, Body); //Portal address
  UniMainModule.QWork2.Close;
end;

procedure Parsetemplate(var Body: string; DocumentType: integer; _IdKey: longint; _IdModule: longint; _IdRecordId: longint);
begin
  Parse_general_variables(Body);

  if (DocumentType = 1002) and (_IDRecordId <> 0) and (_IDModule = 2) //Invitation portal contact
  then begin
         UniMainModule.QWork2.Close;
         UniMainModule.QWork2.SQL.Clear;
         UniMainModule.QWork2.SQL.Add('Select last_name, first_name, email, portal_password from crm_contacts where id=' + IntToStr(_IDRecordId));
         UniMainModule.QWork2.Open;
         Body   := Parse_field(1001, Utils.Decrypt(UniMainModule.QWork2.FieldByName('Portal_password').AsString, 35462), Body);  //Password portal contact
         Body   := Parse_field(1002, UniMainModule.QWork2.FieldByName('Email').AsString, Body);                                  //Login portal contact
         Body   := Parse_field(1003, UniMainModule.QWork2.FieldByName('Last_name').AsString, Body);                              //Name of the contact
         Body   := Parse_field(1004, UniMainModule.QWork2.FieldByName('First_name').AsString, Body);                             //First name of the contact
         UniMainModule.QWork2.Close;
       end;

  if (DocumentType = 8002) and (_IdKey <> 0) // Mail invalid credit documents portal
  then begin
         UniMainModule.QWork2.Close;
         UniMainModule.QWork2.SQL.Clear;
         UniMainModule.QWork2.SQL.Add('Select last_name, first_name from crm_contacts where id=' + IntToStr(_IdKey));
         UniMainModule.QWork2.Open;
         Body   := Parse_field(2,   UniMainModule.QWork2.FieldByName('Last_name').asString, Body);    //Name contact appointment
         Body   := Parse_field(3,   UniMainModule.QWork2.FieldByName('First_name').asString, Body);   //First name contact appointment
         UniMainModule.QWork2.Close;
       end;
end;

function  Parse_field(FieldId: integer; FieldValue: string; Body: string): string;
var position:          integer;
    tempBody:          string;
    positionendmarker: integer;
    searchfield:       string;
begin
  searchfield := '{' + IntToStr(FieldId) + ':';
  position    := Pos(searchfield, Body);
  while position <> 0
  do begin
         TempBody := Body;
         Body     := copy(Body, 0, position - 1);
         Body     := Body + FieldValue;
         TempBody := copy(TempBody, position, length(Tempbody));
         positionendmarker := Pos('}', TempBody);
         if positionendmarker <> 0
         then TempBody := copy(tempBody, positionendmarker + 1, length(tempBody));
         Body := Body + Tempbody;
         position := Pos(searchfield, Body);
       end;
  Result := Body;
end;

end.
