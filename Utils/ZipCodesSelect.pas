unit ZipCodesSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniButton,
  UniThemeButton, uniBasicGrid, uniDBGrid, uniImage, uniEdit,
  uniGUIBaseClasses, uniPanel, siComp, siLngLnk;

type
  TZipCodesSelectFrm = class(TUniForm)
    GridZipCodes: TUniDBGrid;
    ZipCodes: TIBCQuery;
    DsZipCodes: TDataSource;
    LinkedLanguageZipCodesSelect: TsiLangLinked;
    BtnSelect: TUniThemeButton;
    BtnClose: TUniThemeButton;
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnSelectClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageZipCodesSelectChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure GridZipCodesColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_zipcode_selection(city: string);
    procedure Init_city_selection(zipcode: string);
  end;

function ZipCodesSelectFrm: TZipCodesSelectFrm;

var
	strSelectLocation: string = 'Selecteren locatie'; // TSI: Localized (Don't modify!)
	strSelectZipCode:  string = 'Selecteren postcode'; // TSI: Localized (Don't modify!)

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function ZipCodesSelectFrm: TZipCodesSelectFrm;
begin
  Result := TZipCodesSelectFrm(UniMainModule.GetFormInstance(TZipCodesSelectFrm));
end;

{ TZipCodesSelectFrm }

procedure TZipCodesSelectFrm.BtnCloseClick(Sender: TObject);
begin
  UniMainModule.Result_zipSelection.selected := false;
  Close;
end;

procedure TZipCodesSelectFrm.BtnSelectClick(Sender: TObject);
begin
  if (ZipCodes.Active) and (not ZipCodes.FieldByName('Id').isNull)
  then begin
         UniMainModule.Result_zipSelection.selected := true;
         UniMainModule.Result_zipSelection.zipcode  := ZipCodes.FieldByName('ZipCode').AsString;
         UniMainModule.Result_zipSelection.city     := ZipCodes.FieldByName('City').AsString;
         UniMainModule.Result_zipSelection.multiple := false;
         Close;
       end;
end;

procedure TZipCodesSelectFrm.FormCreate(Sender: TObject);
var position: integer;
begin
  UpdateStrings;
  Left := UniApplication.ScreenWidth + 5;
//  Height := UniApplication.ScreenHeight - 40;
  Top  := 20;
  Position := UniApplication.ScreenWidth - Self.Width - 20;
  unisession.AddJS(self.WebForm.JSName + '.animate( {  duration: 200, from: { x: ' + inttostr(UniApplication.ScreenWidth) + '  }, to: { x: ' + inttostr(Position) + '  } });');
end;

procedure TZipCodesSelectFrm.GridZipCodesColumnSort(Column: TUniDBGridColumn;
  Direction: Boolean);
var OrderStr: string;
begin
  OrderStr := Column.FieldName;
  If Direction
  then OrderStr := OrderStr + ' ASC,'
  else OrderStr := OrderStr + ' DESC,';
  (GridZipCodes.DataSource.DataSet as TIBCQuery).IndexFieldNames := orderStr;
end;

procedure TZipCodesSelectFrm.Init_city_selection(zipcode: string);
begin
  Caption := strSelectLocation;
  UniMainModule.Result_zipSelection.selected := false;
  UniMainModule.Result_zipSelection.zipcode  := '';
  UniMainModule.Result_zipSelection.city     := '';
  UniMainModule.Result_zipSelection.multiple := false;
  ZipCodes.Close;
  ZipCodes.SQL.Clear;
  ZipCodes.SQL.Add('Select * from zipcodes where companyid=' +
                   IntToStr(UniMainModule.Company_Id) +
                   ' and UPPER(Zipcode) like ' + QuotedStr(UpperCase(zipcode) + '%') +
                   ' order by city');
  ZipCodes.Open;
end;

procedure TZipCodesSelectFrm.Init_zipcode_selection(city: string);
begin
  Caption := strSelectZipCode;
  UniMainModule.Result_zipSelection.selected := false;
  UniMainModule.Result_zipSelection.zipcode  := '';
  UniMainModule.Result_zipSelection.city     := '';
  UniMainModule.Result_zipSelection.multiple := false;
  ZipCodes.Close;
  ZipCodes.SQL.Clear;
  ZipCodes.SQL.Add('Select * from zipcodes where companyid=' +
                   IntToStr(UniMainModule.Company_Id) +
                   ' and UPPER(City) like ' + QuotedStr('%' + UpperCase(city) + '%') +
                   ' order by zipcode');
  ZipCodes.Open;
end;

procedure TZipCodesSelectFrm.LinkedLanguageZipCodesSelectChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TZipCodesSelectFrm.UpdateStrings;
begin
  strSelectZipCode  := LinkedLanguageZipCodesSelect.GetTextOrDefault('strstrSelectZipCode' (* 'Selecteren postcode' *) );
  strSelectLocation := LinkedLanguageZipCodesSelect.GetTextOrDefault('strstrSelectLocation' (* 'Selecteren locatie' *) );
end;

end.
