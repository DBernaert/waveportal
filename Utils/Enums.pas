//Contains enumeration values used throughout the application and some helper method to convert between integers and values

unit Enums;

interface

uses siComp, siLngLnk;

type
    TPremiumPayment = (ppUnknown = -1, ppMonthly = 0, ppQuarterly = 1, ppHalfYearly = 2, ppYearly = 3, ppLumpSum = 4, ppPaymentDifference = 5);
    TPaymentMethod = (pmUnknown = -1, pmManual = 0, pmSEPA = 1, pmOnline = 2, pmDifference = 3, pmReimbursement = 4, pmTransfertFrom = 5, pmTransfertTo = 6);
    TInsuranceFileStatus = (ifsUnknown = -1, ifsActive = 1412, ifsWaiting = 1432, ifsMandate = 1437, ifsCancelled = 1440, ifsDoubleEntry = 1442);
    TTwikeyCommunicationType = (tctUnknown = -1, tctTransactionFeed = 1, tctMandateFeed = 2);
    TFileType = (ftUnknown = -1, ftSepa = 1439);

function SafePremiumPaymentCast(Value: Integer): TPremiumPayment;
function PremiumPaymentToText (Value: TPremiumPayment; TsiLang: TsiLangLinked): string;

function SafePaymentMethodCast(Value: Integer): TPaymentMethod;
function PaymentMethodToText (Value: TPaymentMethod; TsiLang: TsiLangLinked): string;

function SafeInsuranceFileStatusCast(Value: Integer): TInsuranceFileStatus;
function SafeTwikeyCommunicationTypeCast(Value: Integer): TTwikeyCommunicationType;

implementation

var
  str_pp_unknown:           string = 'Onbekend'; // TSI: Localized (Don't modify!)
	str_pp_Monthly:           string = 'Maandpremie'; // TSI: Localized (Don't modify!)
	str_pp_Quarterly:         string = 'Kwartaalpremie'; // TSI: Localized (Don't modify!)
	str_pp_HalfYearly:        string = '6-maand premie'; // TSI: Localized (Don't modify!)
	str_pp_Yearly:            string = 'Jaarpremie'; // TSI: Localized (Don't modify!)
	str_pp_LumpSum:           string = 'Koopsom'; // TSI: Localized (Don't modify!)
	str_pp_PaymentDifference: string = 'Betalingsverschil'; // TSI: Localized (Don't modify!)

  str_pm_unknown_payment:       string = 'Onbekend'; // TSI: Localized (Don't modify!)
	str_pm_manual_payment:        string = 'Manuele betaling'; // TSI: Localized (Don't modify!)
	str_pm_sepa:                  string = 'Sepa'; // TSI: Localized (Don't modify!)
	str_pm_online_payment:        string = 'Online betaling'; // TSI: Localized (Don't modify!)
	str_pm_difference_payment:    string = 'Betalingsverschil'; // TSI: Localized (Don't modify!)
	str_pm_reimbursement_payment: string = 'Terugbetaling'; // TSI: Localized (Don't modify!)
	str_pm_TransfertFrom:				  string = 'Overboeking van'; // TSI: Localized (Don't modify!)
	str_pm_TransfertTo:				 	  string = 'Overboeking naar'; // TSI: Localized (Don't modify!)




//**********************  Premium Payment  *************************************


function SafePremiumPaymentCast(Value: Integer): TPremiumPayment;
begin
  if (Value >= Ord(Low(TPremiumPayment))) and (Value <= Ord(High(TPremiumPayment))) then
    Result := TPremiumPayment(Value)
  else
    Result := ppUnknown;
end;

function PremiumPaymentToText (Value: TPremiumPayment; TsiLang: TsiLangLinked): string;
begin
  if TsiLang <> nil then
  begin
    case Value of
      ppMonthly:            result := TsiLang.GetTextOrDefault('str_pp_Monthly');
      ppQuarterly:          result := TsiLang.GetTextOrDefault('str_pp_Quarterly');
      ppHalfYearly:         result := TsiLang.GetTextOrDefault('str_pp_HalfYearly');
      ppYearly:             result := TsiLang.GetTextOrDefault('str_pp_Yearly');
      ppLumpSum:            result := TsiLang.GetTextOrDefault('str_pp_LumpSum');
      ppPaymentDifference:  result := TsiLang.GetTextOrDefault('str_pp_PaymentDifference');
      else                  result := TsiLang.GetTextOrDefault('str_pp_unknown');
    end;
  end
  else begin
    case Value of
      ppMonthly:            result := str_pp_Monthly;
      ppQuarterly:          result := str_pp_Quarterly;
      ppHalfYearly:         result := str_pp_HalfYearly;
      ppYearly:             result := str_pp_Yearly;
      ppLumpSum:            result := str_pp_LumpSum;
      ppPaymentDifference:  result := str_pp_PaymentDifference;
      else                  result := str_pp_unknown;
    end;
  end;
end;


//**********************  Payment Method  **************************************

function SafePaymentMethodCast(Value: Integer): TPaymentMethod;
begin
  if (Value >= Ord(Low(TPaymentMethod))) and (Value <= Ord(High(TPaymentMethod))) then
    Result := TPaymentMethod(Value)
  else
    Result := pmUnknown;
end;

function PaymentMethodToText (Value: TPaymentMethod; TsiLang: TsiLangLinked): string;
begin
  if TsiLang <> nil then
  begin
    case Value of
      pmManual:         result := TsiLang.GetTextOrDefault('strstr_pm_manual_payment');
      pmSEPA:           result := TsiLang.GetTextOrDefault('strstr_pm_sepa');
      pmOnline:         result := TsiLang.GetTextOrDefault('strstr_pm_online_payment');
      pmDifference:     result := TsiLang.GetTextOrDefault('strstr_pm_difference_payment');
      pmReimbursement:  result := TsiLang.GetTextOrDefault('strstr_pm_reimbursement_payment');
      pmTransfertFrom:  result := TsiLang.GetTextOrDefault('strstr_pm_TransfertFrom');
      pmTransfertTo:    result := TsiLang.GetTextOrDefault('strstr_pm_TransfertTo');
      else              result := TsiLang.GetTextOrDefault('strstr_pm_unknown_payment');
    end;
  end
  else begin
    case Value of
      pmManual:         result := str_pm_manual_payment;
      pmSEPA:           result := str_pm_sepa;
      pmOnline:         result := str_pm_online_payment;
      pmDifference:     result := str_pm_difference_payment;
      pmReimbursement:  result := str_pm_reimbursement_payment;
      pmTransfertFrom:  result := str_pm_TransfertFrom;
      pmTransfertTo:    result := str_pm_TransfertTo;
      else              result := str_pm_unknown_payment;
    end;
  end;
end;

//**********************  Insurance file status ********************************

function SafeInsuranceFileStatusCast(Value: Integer): TInsuranceFileStatus;
begin
  if (Value >= Ord(Low(TInsuranceFileStatus))) and (Value <= Ord(High(TInsuranceFileStatus))) then
    Result := TInsuranceFileStatus(Value)
  else
    Result := ifsUnknown;
end;

function SafeTwikeyCommunicationTypeCast(Value: Integer): TTwikeyCommunicationType;
begin
  if (Value >= Ord(Low(TTwikeyCommunicationType))) and (Value <= Ord(High(TTwikeyCommunicationType))) then
    Result := TTwikeyCommunicationType(Value)
  else
    Result := tctUnknown;
end;

end.
