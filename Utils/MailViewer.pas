unit MailViewer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniURLFrame, EAGetMailObjLib_TLB,
  uniLabel, uniImage, uniButton, UniThemeButton, uniPanel, siComp, siLngLnk;

var
	strErrorDecrypting:            string = 'Fout bij het decrypteren van het bericht: '; // TSI: Localized (Don't modify!)
	strErrorVerificationSignature: string = 'Fout bij de verificatie van de digitale ondertekening: '; // TSI: Localized (Don't modify!)
	strFrom:                       string = 'Van:'; // TSI: Localized (Don't modify!)
	strSubject:                    string = 'Onderwerp:'; // TSI: Localized (Don't modify!)
	strAttachments:                string = 'Bijlagen:'; // TSI: Localized (Don't modify!)
	str_to:                        string = 'Aan'; // TSI: Localized (Don't modify!)
	str_cc:                        string = 'Cc'; // TSI: Localized (Don't modify!)
	strtime:                       string = 'Tijdstip: '; // TSI: Localized (Don't modify!)

type
  TMailViewerFrm = class(TUniForm)
    URLFrame: TUniURLFrame;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterRight: TUniContainerPanel;
    BtnCancel: TUniThemeButton;
    LinkedLanguageMailViewer: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageMailViewerChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    FileMail:     string;
    FileModuleId: integer;
    FileId:       longint;

    function  FormatAddresses(addresses: IAddressCollection; prefix: WideString): WideString;
  public
    { Public declarations }
    procedure Start_mailviewer(MailFile: string; Id: longint; ModuleId: integer);
  end;

function MailViewerFrm: TMailViewerFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule, IOUtils, StrUtils,
  Main;

function MailViewerFrm: TMailViewerFrm;
begin
  Result := TMailViewerFrm(UniMainModule.GetFormInstance(TMailViewerFrm));
end;

{ TMailViewerFrm }

procedure TMailViewerFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

function TMailViewerFrm.FormatAddresses(addresses: IAddressCollection;
  prefix: WideString): WideString;
var value: WideString;
    i:     integer;
begin
  if addresses.Count = 0
  then begin
         result := '';
         exit;
       end;

  value := '<b>' + prefix + ':</b> '; // To or Cc
  for i := 0 to addresses.Count - 1
  do begin
       if(addresses.Item[i].Name = '')
       then value := value + '&lt;' + addresses.Item[i].Address + '&gt;'
       else value := value + addresses.Item[i].Name + ' &lt;' + addresses.Item[i].Address + '&gt;';
       if (i < addresses.Count - 1)
       then value := value + '; ';
      end;
  result := value + '<br>';
end;

procedure TMailViewerFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMailViewerFrm.LinkedLanguageMailViewerChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMailViewerFrm.Start_mailviewer(MailFile: string; Id: longint; ModuleId: integer);
var oMail:                        TMail;
    attachments:                  IAttachmentCollection;
    oAttachment:                  IAttachment;
    I:                            integer;
    html, header, attachmentName: WideString;
    S:                            TStringList;
    Folder:                       string;
    HtmlFileName:                 string;
    AUrl:                         string;
    AttachmentsCounter:           integer;
    AttachmentsLabel:             string;
begin
  FileMail     := MailFile;
  FileModuleId := ModuleId;
  FileId       := Id;

  HtmlFileName := UniServerModule.NewCacheFileUrl(False, 'html', TPath.GetFileNameWithoutExtension(MailFile), '', AUrl, True);
  Folder       := TPath.GetDirectoryName(HtmlFileName);

  URLFrame.URL := '';

  oMail             := TMail.Create(Application);
  oMail.LicenseCode := 'EG-C1508812802-00325-D2F14E8AAFUEFUF7-E44F3CBV95F4A3A2';

  oMail.LoadFile(MailFile, False);
  try
    if oMail.IsEncrypted
    then oMail.Load(oMail.Decrypt(nil).Content);
  except
    on ep:Exception
    do MainForm.WaveShowErrorToast(strErrorDecrypting + ep.Message);
  end;

  try
    if oMail.IsSigned
    then oMail.VerifySignature();
  except
    on ep:Exception
    do MainForm.WaveShowErrorToast(strErrorVerificationSignature + ep.Message);
  end;

  oMail.DecodeTNEF();

  html   := oMail.HtmlBody;
  header := header + '<font face="13px Arial">';
  header := header + '<b>' + strFrom + '</b> ' + oMail.From.Name + ' &lt;' + oMail.From.Address + '&gt;' + '<br>';

  header := header + FormatAddresses(oMail.ToList,  str_to);
  header := header + FormatAddresses(oMail.CcList,  str_cc);
  header := header + '<b>' + strTime + '</b>' + FormatDateTime('dd/mm/yyyy hh:mm', oMail.ReceivedDate) + '<br>';
  header := header + '<b>' + strSubject + '</b> ' + oMail.Subject + '<br>' + #13#10;

  // Parse attachment
  attachments := oMail.AttachmentList;
  AttachmentsCounter := 0;
  if(attachments.Count > 0)
  then begin
         AttachmentsLabel := '<b>' + strAttachments + '</b> ';
         for i:= 0 to attachments.Count - 1
         do begin
              oAttachment := attachments.Item[i];
              attachmentName := Folder + '\' + oAttachment.Name;
              oAttachment.SaveAs(attachmentName, true);
              if Trim(oAttachment.ContentID) = ''
              then begin
                     AttachmentsLabel := AttachmentsLabel + '<a href="' + oAttachment.Name + '" target="_blank">' + oAttachment.Name + '</a> ';
                     Inc(AttachmentsCounter);
                   end
              else begin
                     if not ContainsText(html, oAttachment.ContentID)
                     then begin
                            AttachmentsLabel := AttachmentsLabel + '<a href="' + oAttachment.Name + '" target="_blank">' + oAttachment.Name + '</a> ';
                            Inc(AttachmentsCounter);
                          end;
                   end;

              if oAttachment.ContentID <> ''
              then begin
                     html := StringReplace(html, 'cid:' + oAttachment.ContentID, oAttachment.Name, [rfReplaceAll, rfIgnoreCase]);
                   end
            end;
       end;
  if AttachmentsCounter > 0
  then header := header + AttachmentsLabel;

  header := '<meta http-equiv="Content-Type" content="text-html; charset=utf-8">' + header;
  html   := header + '<hr>' + html;

  S      := TStringList.Create;
  S.Text := html;
  S.SaveToFile(HtmlFileName, TEnCoding.UTF8);
  S.Free;
  URLFrame.URL := AUrl;
end;

procedure TMailViewerFrm.UpdateStrings;
begin
  strtime                       := LinkedLanguageMailViewer.GetTextOrDefault('strstrtime' (* 'Tijdstip: ' *) );
  str_cc                        := LinkedLanguageMailViewer.GetTextOrDefault('strstr_cc' (* 'Cc' *) );
  str_to                        := LinkedLanguageMailViewer.GetTextOrDefault('strstr_to' (* 'Aan' *) );
  strAttachments                := LinkedLanguageMailViewer.GetTextOrDefault('strstrAttachments' (* 'Bijlagen:' *) );
  strSubject                    := LinkedLanguageMailViewer.GetTextOrDefault('strstrSubject' (* 'Onderwerp:' *) );
  strFrom                       := LinkedLanguageMailViewer.GetTextOrDefault('strstrFrom' (* 'Van:' *) );
  strErrorVerificationSignature := LinkedLanguageMailViewer.GetTextOrDefault('strstrErrorVerificationSignature' (* 'Fout bij de verificatie van de digitale ondertekening: ' *) );
  strErrorDecrypting            := LinkedLanguageMailViewer.GetTextOrDefault('strstrErrorDecrypting' (* 'Fout bij het decrypteren van het bericht: ' *) );
end;

end.


