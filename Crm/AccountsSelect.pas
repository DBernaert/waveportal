unit AccountsSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniButton,
  UniThemeButton, uniBasicGrid, uniDBGrid, uniImage, uniEdit, uniGUIBaseClasses,
  uniPanel, siComp, siLngLnk;

var
	str_companies: string = 'Bedrijven'; // TSI: Localized (Don't modify!)

type
  TAccountsSelectFrm = class(TUniForm)
    Accounts: TIBCQuery;
    DsAccounts: TDataSource;
    GridAccounts: TUniDBGrid;
    LinkedlanguageAccountsSelect: TsiLangLinked;
    EditSearch: TUniEdit;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    BtnSelect: TUniThemeButton;
    BtnClose: TUniThemeButton;
    AccountsID: TLargeintField;
    AccountsNAME: TWideStringField;
    AccountsVAT_NUMBER: TWideStringField;
    AccountsBILLING_ADDRESS_STREET: TWideStringField;
    AccountsBILLING_ADDRESS_CITY: TWideStringField;
    AccountsPHONE: TWideStringField;
    AccountsEMAIL: TWideStringField;
    AccountsWEBSITE: TWideStringField;
    AccountsACCOUNT_SUPPLIER: TSmallintField;
    AccountsACCOUNT_CUSTOMER: TSmallintField;
    procedure BtnCloseClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnSelectClick(Sender: TObject);
    procedure BtnExportListClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedlanguageAccountsSelectChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure AccountsACCOUNT_SUPPLIERGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure AccountsACCOUNT_CUSTOMERGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
    procedure CallBackInsertUpdateAccountsTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Init_account_selection(SearchString: string): boolean;
  end;

function AccountsSelectFrm: TAccountsSelectFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, Utils, AccountsAdd;

function AccountsSelectFrm: TAccountsSelectFrm;
begin
  Result := TAccountsSelectFrm(UniMainModule.GetFormInstance(TAccountsSelectFrm));
end;

procedure TAccountsSelectFrm.AccountsACCOUNT_CUSTOMERGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-check-circle fa-lg" style=color:#E0E0E0; ></i>'
  end;
end;

procedure TAccountsSelectFrm.AccountsACCOUNT_SUPPLIERGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-check-circle fa-lg" style=color:#E0E0E0; ></i>'
  end;
end;

procedure TAccountsSelectFrm.BtnAddClick(Sender: TObject);
begin
  With AccountsAddFrm
  do begin
       if Init_account_creation
       then ShowModal(CallBackInsertUpdateAccountsTable)
       else Close;
     end;
end;

procedure TAccountsSelectFrm.CallBackInsertUpdateAccountsTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Accounts.Active
         then begin
                Accounts.Refresh;
                Accounts.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TAccountsSelectFrm.BtnCloseClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TAccountsSelectFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridAccounts, str_companies, Accounts, 'Id');
end;

procedure TAccountsSelectFrm.BtnModifyClick(Sender: TObject);
begin
  if (Accounts.Active) and (not Accounts.fieldbyname('Id').isNull)
  then begin
         With AccountsAddFrm
         do begin
              if Init_account_modification(Accounts.FieldByName('Id').AsInteger)
              then ShowModal(CallBackInsertUpdateAccountsTable)
              else Close;
            end;
       end;
end;

procedure TAccountsSelectFrm.BtnSelectClick(Sender: TObject);
begin
  if (Accounts.Active) and (not Accounts.FieldByName('Id').isNull)
  then begin
         UniMainModule.Result_dbAction := Accounts.FieldByName('Id').AsInteger;
         Close;
       end;
end;

procedure TAccountsSelectFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter          := ' NAME LIKE ''%'                   + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
                     ' VAT_NUMBER LIKE ''%'             + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
                     ' BILLING_ADDRESS_STREET LIKE ''%' + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
                     ' BILLING_ADDRESS_CITY LIKE ''%'   + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
                     ' PHONE LIKE ''%'                  + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
                     ' EMAIL LIKE ''%'                  + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
                     ' WEBSITE LIKE ''%'                + Utils.RemoveQuotes(EditSearch.Text) + '%''';
  Accounts.Filter := Filter;
end;

procedure TAccountsSelectFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

function TAccountsSelectFrm.Init_account_selection(SearchString: string): boolean;
var SQL: string;
begin
  Accounts.Close;
  SQL := 'Select id, name, vat_number, billing_address_street, billing_address_city, phone, email, website, ' +
         'account_customer, account_supplier from crm_accounts ' +
         'where companyid=' + IntToStr(UniMainModule.Company_id) +
         ' and removed=''0'' and obsolete<>1 and bankruptcy<>1';

  SQL := SQL + ' order by name';
  Accounts.SQL.Clear;
  Accounts.SQL.Add(SQL);
  Accounts.Open;
  EditSearch.Text := SearchString;
  if Trim(EditSearch.Text) <> ''
  then begin
         EditSearchChange(nil);
         GridAccounts.SetFocus;
       end
  else EditSearch.SetFocus;
  Result := True;
end;

procedure TAccountsSelectFrm.LinkedlanguageAccountsSelectChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TAccountsSelectFrm.UpdateStrings;
begin
  str_companies := LinkedlanguageAccountsSelect.GetTextOrDefault('strstr_companies' (* 'Bedrijven' *) );
end;

end.
