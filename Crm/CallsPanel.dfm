object CallsPanelFrm: TCallsPanelFrm
  Left = 0
  Top = 0
  Width = 1079
  Height = 531
  Layout = 'border'
  LayoutConfig.Margin = '8 8 8 8'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1079
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerFilterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 113
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
    end
    object ContainerFilterRight: TUniContainerPanel
      Left = 847
      Top = 0
      Width = 232
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object NavigatorCalls: TUniDBNavigator
        Left = 128
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        DataSource = DsCalls
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        IconSet = icsFontAwesome
        TabStop = False
        TabOrder = 1
      end
      object BtnAddCall: TUniThemeButton
        Left = 0
        Top = 0
        Width = 25
        Height = 25
        Hint = 'Toevoegen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddCallClick
        ButtonTheme = uctDanger
      end
      object BtnDeleteCall: TUniThemeButton
        Left = 64
        Top = 0
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteCallClick
        ButtonTheme = uctDanger
      end
      object BtnExportList: TUniThemeButton
        Left = 96
        Top = 0
        Width = 25
        Height = 25
        Hint = 'Exporteren lijst'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 4
        Images = UniMainModule.ImageList
        ImageIndex = 44
        ButtonTheme = uctDanger
      end
      object BtnModifyCall: TUniThemeButton
        Left = 32
        Top = 0
        Width = 25
        Height = 25
        Hint = 'Wijzigen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 5
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyCallClick
        ButtonTheme = uctDanger
      end
    end
  end
  object ContainerNotesBody: TUniContainerPanel
    Left = 0
    Top = 25
    Width = 1079
    Height = 506
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 1
    Layout = 'hbox'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 0 0 0'
    object GridCalls: TUniDBGrid
      Left = 0
      Top = 0
      Width = 656
      Height = 506
      Hint = ''
      DataSource = DsCalls
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      ForceFit = True
      LayoutConfig.Flex = 1
      LayoutConfig.Height = '100%'
      LayoutConfig.Region = 'center'
      Align = alClient
      TabOrder = 1
      OnDblClick = BtnModifyCallClick
      OnFieldImageURL = GridCallsFieldImageURL
      OnDrawColumnCell = GridCallsDrawColumnCell
      Columns = <
        item
          FieldName = 'CALL_START'
          Title.Caption = 'Tijdstip'
          Width = 80
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'TITLE'
          Title.Caption = 'Titel'
          Width = 100
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'RESPONSABLE_NAME'
          Title.Caption = 'Verantwoordelijke'
          Width = 100
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'DIRECTION'
          Title.Alignment = taCenter
          Title.Caption = 'Richting'
          Width = 80
          Alignment = taCenter
          ImageOptions.Visible = True
          ImageOptions.Width = 12
          ImageOptions.Height = 12
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'LEFT_VOICEMAIL'
          Title.Alignment = taCenter
          Title.Caption = 'Voicemail'
          Width = 80
          Alignment = taCenter
          ImageOptions.Visible = True
          ImageOptions.Width = 12
          ImageOptions.Height = 12
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'PRIORITY_DESCRIPTION'
          Title.Alignment = taCenter
          Title.Caption = 'Prioriteit'
          Width = 80
          Alignment = taCenter
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'CALL_STATUS_DESCRIPTION'
          Title.Alignment = taCenter
          Title.Caption = 'Status'
          Width = 100
          Alignment = taCenter
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'CALL_RESULT_DESCRIPTION'
          Title.Alignment = taCenter
          Title.Caption = 'Resultaat'
          Width = 100
          Alignment = taCenter
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
    object ContainerRightCallsPanel: TUniContainerPanel
      Left = 656
      Top = 0
      Width = 423
      Height = 506
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      Layout = 'vbox'
      LayoutConfig.Height = '100%'
      LayoutConfig.Margin = '0 0 0 8'
      object MemoDescription: TUniDBMemo
        Left = 48
        Top = 56
        Width = 185
        Height = 89
        Cursor = crArrow
        Hint = ''
        DataField = 'DESCRIPTION'
        DataSource = DsCalls
        ReadOnly = True
        TabOrder = 1
        TabStop = False
        LayoutConfig.Flex = 1
        LayoutConfig.Width = '100%'
        FieldLabel = 'Omschrijving'
        FieldLabelAlign = laTop
      end
      object MemoNotes: TUniDBMemo
        Left = 24
        Top = 256
        Width = 185
        Height = 89
        Cursor = crArrow
        Hint = ''
        DataField = 'CALL_NOTES'
        DataSource = DsCalls
        ReadOnly = True
        TabOrder = 2
        TabStop = False
        LayoutConfig.Flex = 1
        LayoutConfig.Width = '100%'
        FieldLabel = 'Notities'
        FieldLabelAlign = laTop
      end
    end
  end
  object Calls: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO CRM_NOTES'
      
        '  (ID, COMPANYID, REMOVED, MODULE_ID, RECORD_ID, TITLE, CONTENT,' +
        ' DATE_ENTERED, DATE_MODIFIED, CREATED_USER_ID, MODIFIED_USER_ID,' +
        ' DATE_NOTE)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :REMOVED, :MODULE_ID, :RECORD_ID, :TITLE, :C' +
        'ONTENT, :DATE_ENTERED, :DATE_MODIFIED, :CREATED_USER_ID, :MODIFI' +
        'ED_USER_ID, :DATE_NOTE)'
      'RETURNING '
      
        '  ID, COMPANYID, REMOVED, MODULE_ID, RECORD_ID, TITLE, DATE_ENTE' +
        'RED, DATE_MODIFIED, CREATED_USER_ID, MODIFIED_USER_ID, DATE_NOTE')
    SQLDelete.Strings = (
      'DELETE FROM CRM_NOTES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE CRM_NOTES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, REMOVED = :REMOVED, MODULE_I' +
        'D = :MODULE_ID, RECORD_ID = :RECORD_ID, TITLE = :TITLE, CONTENT ' +
        '= :CONTENT, DATE_ENTERED = :DATE_ENTERED, DATE_MODIFIED = :DATE_' +
        'MODIFIED, CREATED_USER_ID = :CREATED_USER_ID, MODIFIED_USER_ID =' +
        ' :MODIFIED_USER_ID, DATE_NOTE = :DATE_NOTE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, REMOVED, MODULE_ID, RECORD_ID, TITLE, CONT' +
        'ENT, DATE_ENTERED, DATE_MODIFIED, CREATED_USER_ID, MODIFIED_USER' +
        '_ID, DATE_NOTE FROM CRM_NOTES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM CRM_NOTES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM CRM_NOTES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, module_id, record_id, title, description, call_start,' +
        ' left_voicemail, direction, call_notes, priority_code, call_stat' +
        'us,'
      'Case'
      '  WHEN Call_status = 0 THEN '#39'Open'#39
      '  WHEN Call_status = 1 THEN '#39'Afgewerkt'#39
      'END as call_status_description,'
      'Case'
      '  WHEN Call_result = 0 THEN '#39'Open'#39
      '  WHEN Call_result = 1 THEN '#39'Uitgevoerd'#39
      '  WHEN Call_result = 2 THEN '#39'Geannuleerd'#39
      '  WHEN Call_result = 3 THEN '#39'Ontvangen'#39
      'END as call_result_description,'
      'Case'
      '  WHEN Priority_code = 0 THEN '#39'Laag'#39
      '  WHEN Priority_code = 1 THEN '#39'Normaal'#39
      '  WHEN Priority_code = 2 THEN '#39'Hoog'#39
      'END as priority_description,'
      ''
      'CASE'
      
        '  WHEN Responsable_type = 1 THEN (Select COALESCE(USR.NAME, '#39#39') ' +
        '|| '#39' '#39' || COALESCE(USR.FIRSTNAME, '#39#39') from USERS USR where ID = ' +
        'Responsable_id) '
      
        '  WHEN Responsable_type = 2 THEN (Select COALESCE(CONTR.LAST_NAM' +
        'E, '#39#39') || '#39' '#39' || COALESCE(CONTR.FIRST_NAME, '#39#39') from Credit_Cont' +
        'ributors CONTR where ID = Responsable_id)'
      'END AS responsable_name'
      '   '
      'from crm_calls'
      'where companyId=1 and removed=0'
      '')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 448
    Top = 112
    object CallsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object CallsMODULE_ID: TLargeintField
      FieldName = 'MODULE_ID'
      Required = True
    end
    object CallsRECORD_ID: TLargeintField
      FieldName = 'RECORD_ID'
      Required = True
    end
    object CallsTITLE: TWideStringField
      FieldName = 'TITLE'
      Size = 50
    end
    object CallsDESCRIPTION: TWideMemoField
      FieldName = 'DESCRIPTION'
      BlobType = ftWideMemo
    end
    object CallsCALL_START: TDateField
      FieldName = 'CALL_START'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object CallsLEFT_VOICEMAIL: TIntegerField
      FieldName = 'LEFT_VOICEMAIL'
      Required = True
    end
    object CallsDIRECTION: TIntegerField
      FieldName = 'DIRECTION'
      Required = True
    end
    object CallsCALL_NOTES: TWideMemoField
      FieldName = 'CALL_NOTES'
      BlobType = ftWideMemo
    end
    object CallsPRIORITY_CODE: TIntegerField
      FieldName = 'PRIORITY_CODE'
      Required = True
    end
    object CallsCALL_STATUS_DESCRIPTION: TWideStringField
      FieldName = 'CALL_STATUS_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 9
    end
    object CallsCALL_RESULT_DESCRIPTION: TWideStringField
      FieldName = 'CALL_RESULT_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 11
    end
    object CallsRESPONSABLE_NAME: TWideStringField
      FieldName = 'RESPONSABLE_NAME'
      ReadOnly = True
      Size = 101
    end
    object CallsPRIORITY_DESCRIPTION: TWideStringField
      FieldName = 'PRIORITY_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 7
    end
    object CallsCALL_STATUS: TIntegerField
      FieldName = 'CALL_STATUS'
      Required = True
    end
  end
  object DsCalls: TDataSource
    DataSet = Calls
    Left = 448
    Top = 168
  end
end
