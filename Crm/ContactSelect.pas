unit ContactSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniBasicGrid,
  uniDBGrid, uniButton, UniThemeButton, uniImage, uniEdit, uniGUIBaseClasses,
  uniPanel, siComp, siLngLnk;

var
	str_contacts: string = 'Contacten'; // TSI: Localized (Don't modify!)

type
  TContactSelectFrm = class(TUniForm)
    GridContacts: TUniDBGrid;
    Contacts: TIBCQuery;
    DsContacts: TDataSource;
    LinkedLanguageContactSelect: TsiLangLinked;
    EditSearch: TUniEdit;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    BtnSelect: TUniThemeButton;
    BtnClose: TUniThemeButton;
    procedure EditSearchChange(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnSelectClick(Sender: TObject);
    procedure BtnExportListClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageContactSelectChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    procedure CallBackInsertUpdateContactsTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Init_contact_selection(SearchString: string): boolean;
  end;

function ContactSelectFrm: TContactSelectFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, Utils, ContactsAdd;

function ContactSelectFrm: TContactSelectFrm;
begin
  Result := TContactSelectFrm(UniMainModule.GetFormInstance(TContactSelectFrm));
end;

{ TContactSelectFrm }

procedure TContactSelectFrm.BtnAddClick(Sender: TObject);
begin
  With ContactsAddFrm
  do begin
       if Init_contact_creation(0)
       then ShowModal(CallBackInsertUpdateContactsTable)
       else Close;
     end;
end;

procedure TContactSelectFrm.CallBackInsertUpdateContactsTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Contacts.Active
         then begin
                Contacts.Refresh;
                Contacts.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TContactSelectFrm.BtnCloseClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TContactSelectFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridContacts, str_contacts, Contacts, 'Id');
end;

procedure TContactSelectFrm.BtnModifyClick(Sender: TObject);
begin
  if (Contacts.Active) and (not Contacts.fieldbyname('Id').isNull)
  then begin
         With ContactsAddFrm
         do begin
              if Init_contact_modification(Contacts.FieldByName('Id').AsInteger)
              then ShowModal(CallBackInsertUpdateContactsTable)
              else Close;
            end;
       end;
end;

procedure TContactSelectFrm.BtnSelectClick(Sender: TObject);
begin
  if (Contacts.Active) and (not Contacts.FieldByName('Id').isNull)
  then begin
         UniMainModule.Result_dbAction := Contacts.FieldByName('Id').AsInteger;
         Close;
       end;
end;

procedure TContactSelectFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' INTERNAL_NUMBER LIKE ''%' + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
            ' LAST_NAME LIKE ''%'       + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
            ' FIRST_NAME LIKE ''%'      + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
            ' ADDRESS_CITY LIKE ''%'    + Utils.RemoveQuotes(EditSearch.Text) + '%''' + ' OR ' +
            ' PHONE_MOBILE LIKE ''%'    + Utils.RemoveQuotes(EditSearch.Text) + '%''';
  Contacts.Filter := Filter;
end;

procedure TContactSelectFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

function TContactSelectFrm.Init_contact_selection(
  SearchString: string): boolean;
var SQL: string;
begin
  Contacts.Close;
  SQL := 'Select id, internal_number, last_name, first_name, address_city, phone_mobile from crm_contacts ' +
         'where companyid=' + IntToStr(UniMainModule.Company_id) +
         ' and removed=''0''' +
         ' order by Last_name, First_name';
  Contacts.SQL.Clear;
  Contacts.SQL.Add(SQL);
  Contacts.Open;
  EditSearch.Text := SearchString;
  if Trim(EditSearch.Text) <> ''
  then begin
         EditSearchChange(nil);
         GridContacts.SetFocus;
       end
  else EditSearch.SetFocus;
  Result := True;
end;

procedure TContactSelectFrm.LinkedLanguageContactSelectChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContactSelectFrm.UpdateStrings;
begin
  str_contacts := LinkedLanguageContactSelect.GetTextOrDefault('strstr_contacts' (* 'Contacten' *) );
end;

end.
