unit CallsPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniMemo, uniDBMemo, uniBasicGrid, uniDBGrid, uniButton,
  UniThemeButton, uniDBNavigator, uniEdit, uniGUIBaseClasses, uniPanel;

type
  TCallsPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorCalls: TUniDBNavigator;
    BtnAddCall: TUniThemeButton;
    BtnDeleteCall: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    BtnModifyCall: TUniThemeButton;
    ContainerNotesBody: TUniContainerPanel;
    GridCalls: TUniDBGrid;
    Calls: TIBCQuery;
    DsCalls: TDataSource;
    ContainerRightCallsPanel: TUniContainerPanel;
    CallsID: TLargeintField;
    CallsMODULE_ID: TLargeintField;
    CallsRECORD_ID: TLargeintField;
    CallsTITLE: TWideStringField;
    CallsDESCRIPTION: TWideMemoField;
    CallsCALL_START: TDateField;
    CallsLEFT_VOICEMAIL: TIntegerField;
    CallsDIRECTION: TIntegerField;
    CallsCALL_NOTES: TWideMemoField;
    CallsPRIORITY_CODE: TIntegerField;
    CallsCALL_STATUS_DESCRIPTION: TWideStringField;
    CallsCALL_RESULT_DESCRIPTION: TWideStringField;
    CallsRESPONSABLE_NAME: TWideStringField;
    MemoDescription: TUniDBMemo;
    MemoNotes: TUniDBMemo;
    CallsPRIORITY_DESCRIPTION: TWideStringField;
    CallsCALL_STATUS: TIntegerField;
    procedure BtnAddCallClick(Sender: TObject);
    procedure BtnModifyCallClick(Sender: TObject);
    procedure BtnDeleteCallClick(Sender: TObject);
    procedure GridCallsDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
    procedure GridCallsFieldImageURL(const Column: TUniDBGridColumn;
      const AField: TField; var OutImageURL: string);
  private
    { Private declarations }
    ModuleId: integer;
    procedure CallBackInsertUpdateCallsTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_calls(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses CallsAdd, MainModule, Main, UniFSConfirm, ServerModule;



{ TCallsPanelFrm }

procedure TCallsPanelFrm.BtnAddCallClick(Sender: TObject);
begin
  if Calls.MasterSource.DataSet.FieldByName('Id').isNull
  then exit;

  With CallsAddFrm do begin
   if Init_call_creation(ModuleId, Calls.MasterSource.DataSet.FieldByName('Id').AsInteger) = True
   then ShowModal(CallBackInsertUpdateCallsTable)
   else Close;
  end;
end;

procedure TCallsPanelFrm.BtnDeleteCallClick(Sender: TObject);
begin
  if (not Calls.Active) or (Calls.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Portal_name, 'Verwijderen telefoon?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.QBatch.Close;
          UniMainModule.QBatch.SQL.Clear;
          UniMainModule.QBatch.SQL.Add('Update Crm_calls set removed=''1'' where id=' + QuotedStr(Calls.fieldbyname('Id').asString));
          UniMainModule.QBatch.ExecSQL;
          if UniMainModule.UpdTr_Batch.Active
          then UniMainModule.UpdTr_Batch.Commit;
          UniMainModule.QBatch.Close;
        Except
          on E: EDataBaseError
          do begin
               if UniMainModule.UpdTr_Batch.Active
               then UniMainModule.UpdTr_Batch.Rollback;
               UniMainModule.QBatch.Close;
               MainForm.WaveShowErrorToast('Fout bij het verwijderen: ' + E.Message);
             end;
        end;
        Calls.Refresh;
      end;
    end);
end;

procedure TCallsPanelFrm.BtnModifyCallClick(Sender: TObject);
begin
  if (Calls.Active) and (not Calls.FieldByName('Id').IsNull)
  then begin
         With CallsAddFrm
         do begin
              if Init_call_modification(Calls.FieldByName('Id').AsInteger) = True
              then ShowModal(CallBackInsertUpdateCallsTable)
              else begin
                     Close;
                     MainForm.WaveShowWarningToast('Kan niet wijzigen, in gebruik door een andere gebruiker.');
                   end;
            end;
       end;
end;

procedure TCallsPanelFrm.CallBackInsertUpdateCallsTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if Calls.Active then begin
      Calls.Refresh;
      Calls.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCallsPanelFrm.GridCallsDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
  Attribs: TUniCellAttribs);
begin
  if (UpperCase(Column.FieldName) = 'CALL_START')
  then begin
         if ((Calls.FieldByName('Call_start').AsDateTime < Date) and
             (Calls.FieldByName('Call_status').AsInteger = 0))
         then begin
                Attribs.Color := $003738C6;
                Attribs.Font.Color := ClWhite;
              end;
       end;

  if UpperCase(Column.FieldName) = 'PRIORITY_DESCRIPTION'
  then begin
         Case Calls.fieldbyname('Priority_code').AsInteger of
         0: begin
              Attribs.Color := $00343232;
              Attribs.Font.Color := ClWhite;
            end;
         2: begin
              Attribs.Color := $003738C6;
              Attribs.Font.Color := ClWhite;
            end;
         end;
       end;
end;

procedure TCallsPanelFrm.GridCallsFieldImageURL(
  const Column: TUniDBGridColumn; const AField: TField;
  var OutImageURL: string);
begin
  if SameText(UpperCase(AField.FieldName), 'LEFT_VOICEMAIL')
  then begin
         Case AField.AsInteger of
         0: OutImageURL := 'images/wv_check_white.png';
         1: OutImageURL := 'images/wv_check_green.png';
         End;
       end;

  if SameText(UpperCase(AField.FieldName), 'DIRECTION')
  then begin
         Case AField.AsInteger of
         0:   OutImageURL := 'images/wv_arrow_right_12.png';
         1:   OutImageURL := 'images/wv_arrow_left_12.png';
         else OutImageURL := 'images/wv_check_white.png';
         End;
       end;
end;

procedure TCallsPanelFrm.Start_calls(IdModule: integer; MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;
  Calls.Close;
  Calls.SQL.Clear;
  SQL := 'Select id, module_id, record_id, title, description, call_start, left_voicemail, direction, call_notes, priority_code, call_status, ' +
         'Case ' +
         'WHEN Call_status = 0 THEN ''Open'' ' +
         'WHEN Call_status = 1 THEN ''Afgewerkt'' ' +
         'END as call_status_description, ' +
         'Case ' +
         'WHEN Call_result = 0 THEN ''Open'' ' +
         'WHEN Call_result = 1 THEN ''Uitgevoerd'' ' +
         'WHEN Call_result = 2 THEN ''Geannuleerd'' ' +
         'WHEN Call_result = 3 THEN ''Ontvangen'' ' +
         'END as call_result_description, ' +
         'Case ' +
         'WHEN Priority_code = 0 THEN ''Laag'' ' +
         'WHEN Priority_code = 1 THEN ''Normaal'' ' +
         'WHEN Priority_code = 2 THEN ''Hoog'' ' +
         'END as priority_description, ' +
         'CASE ' +
         'WHEN Responsable_type = 1 THEN (Select COALESCE(USR.NAME, '''') || '' '' || COALESCE(USR.FIRSTNAME, '''') from USERS USR where ID = Responsable_id) ' +
         'WHEN Responsable_type = 2 THEN (Select COALESCE(CONTR.LAST_NAME, '''') || '' '' || COALESCE(CONTR.FIRST_NAME, '''') from Credit_Contributors CONTR where ID = Responsable_id) ' +
         'END AS responsable_name ' +
         'from crm_calls ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) + ' and Module_id=' + IntToStr(IdModule) + ' and removed=''0''';

  SQL := SQL + ' order by call_start desc';
  Calls.SQL.Add(SQL);
  Calls.MasterSource := MasterDs;
  Calls.MasterFields := 'ID';
  Calls.DetailFields := 'RECORD_ID';
  Calls.Open;
end;

end.
