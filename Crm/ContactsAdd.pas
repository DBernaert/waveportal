unit ContactsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniEdit, uniDBEdit, uniButton, UniThemeButton, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniPanel, uniPageControl, uniGUIBaseClasses, uniMemo, uniDBMemo, Data.DB, MemDS, DBAccess, IBC,
  uniCheckBox, uniDBCheckBox, uniDateTimePicker, uniDBDateTimePicker, uniListBox, uniFileUpload, uniImage,
  uniDBImage, uniBitBtn, uniSpeedButton, uniLabel, siComp, siLngLnk,
  uniDBText, uniGroupBox, Vcl.Menus, uniMainMenu;

var
	str_name_is_required:                 string = 'De naam is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_email_required_portal:            string = 'Email adres is een vereiste ingave indien de relatie toegang heeft tot het portaal.'; // TSI: Localized (Don't modify!)
	str_password_required_portal:         string = 'Wachtwoord is een vereiste ingave indien de relatie toegang heeft tot het portaal.'; // TSI: Localized (Don't modify!)
	str_email_unique:                     string = 'Er bestaat reeds een relatie met dit email adres, email adres moet uniek zijn.'; // TSI: Localized (Don't modify!)
	str_error_save_data:                  string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	str_add_contact:                      string = 'Toevoegen relatie'; // TSI: Localized (Don't modify!)
	str_modify_contact:                   string = 'Wijzigen relatie'; // TSI: Localized (Don't modify!)
	MsgErrorAddItem:                      string = 'Fout bij het toevoegen!'; // TSI: Localized (Don't modify!)
	MsgRecordLocked:                      string = 'In gebruik door een andere gebruiker!'; // TSI: Localized (Don't modify!)
	MsgNoAddress:                         string = 'Geen adres aanwezig'; // TSI: Localized (Don't modify!)
	MsgNoEidFound:                        string = 'Geen E-Id gegevens gevonden'; // TSI: Localized (Don't modify!)
	MsgEidInfoRead:                       string = 'E-id informatie ingelezen'; // TSI: Localized (Don't modify!)
	MsgUpdateEidData:                     string = 'Fout bij het bijwerken van de E-id database!'; // TSI: Localized (Don't modify!)
	str_email_not_valid:                  string = 'Email adres is niet correct.'; // TSI: Localized (Don't modify!)
	str_assign_number:                    string = 'Toekennen nummer'; // TSI: Localized (Don't modify!)
	str_number_present:                   string = 'Er is reeds een nummer aanwezig, wenst u door te gaan?'; // TSI: Localized (Don't modify!)
	str_language_doc_required:            string = 'De taal voor de documenten is een vereiste ingave.'; // TSI: Localized (Don't modify!)
	str_warning_duplicate_email_contact:  string = 'Opgelet: er bestaat reeds een contact met hetzelfde email adres!'; // TSI: Localized (Don't modify!)
	str_contact_identical_name:           string = 'Identieke naam'; // TSI: Localized (Don't modify!)
	str_warning_contact_duplicate_name:   string = 'Opgelet, contact met identieke naam gevonden. Doorgaan?'; // TSI: Localized (Don't modify!)

type
  TContactsAddFrm = class(TUniForm)
    PageControlContacts: TUniPageControl;
    TabSheetGeneral: TUniTabSheet;
    EditAppelation: TUniDBLookupComboBox;
    EditName: TUniDBEdit;
    EditFirstName: TUniDBEdit;
    EditAddress: TUniDBEdit;
    EditZipCode: TUniDBEdit;
    EditCity: TUniDBEdit;
    ComboCountryCode: TUniDBLookupComboBox;
    EditPhone: TUniDBEdit;
    Edit_mobile: TUniDBEdit;
    Edit_email: TUniDBEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    TabSheetAdditional: TUniTabSheet;
    Appelations: TIBCQuery;
    DsAppelations: TDataSource;
    Countries: TIBCQuery;
    DsCountries: TDataSource;
    Checkbox_do_not_call: TUniDBCheckBox;
    Edit_birthdate: TUniDBDateTimePicker;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    Edit_Title: TUniDBLookupComboBox;
    Titles: TIBCQuery;
    DsTitles: TDataSource;
    Edit_department: TUniDBLookupComboBox;
    Departments: TIBCQuery;
    DsDepartments: TDataSource;
    Edit_Assigned_User_Id: TUniDBLookupComboBox;
    Languages: TIBCQuery;
    DsLanguages: TDataSource;
    Edit_language: TUniDBLookupComboBox;
    Edit_lead_source: TUniDBEdit;
    LblPhoto: TUniLabel;
    PanelPhoto: TUniPanel;
    ImageLogo: TUniDBImage;
    Edit_birth_place: TUniDBEdit;
    Edit_birth_country: TUniDBLookupComboBox;
    Edit_nationality: TUniDBLookupComboBox;
    Nationalities: TIBCQuery;
    DsNationalities: TDataSource;
    Edit_nis_number: TUniDBEdit;
    Edit_id_cardnumber: TUniDBEdit;
    Edit_MaritalStatus: TUniDBLookupComboBox;
    Sex: TIBCQuery;
    DsSex: TDataSource;
    LinkedLanguageContactsAdd: TsiLangLinked;
    Edit_Account: TUniDBLookupComboBox;
    CrmAccounts: TIBCQuery;
    DsCrmAccounts: TDataSource;
    Edit_sex: TUniDBLookupComboBox;
    Edit_MarriageSystem: TUniDBLookupComboBox;
    MaritalStatus: TIBCQuery;
    DsMaritalStatus: TDataSource;
    MarriageSystem: TIBCQuery;
    DsMarriageSystem: TDataSource;
    EditNumberOfDependentChildren: TUniDBNumberEdit;
    EditIdCardValidFrom: TUniDBDateTimePicker;
    EditIdCardValidUntil: TUniDBDateTimePicker;
    Contacts_Edit: TIBCQuery;
    Ds_Contacts_Edit: TDataSource;
    UpdTr_Contacts_Edit: TIBCTransaction;
    TabSheetSocialMedia: TUniTabSheet;
    TabSheetCreditManagement: TUniTabSheet;
    Edit_Facebook: TUniDBEdit;
    Edit_Twitter: TUniDBEdit;
    Edit_instagram: TUniDBEdit;
    Edit_GooglePlus: TUniDBEdit;
    ImageLastModification: TUniImage;
    LblLastModification: TUniDBText;
    Memo_Remarks: TUniDBMemo;
    GroupBoxFutureAddress: TUniGroupBox;
    Edit_Future_Street: TUniDBEdit;
    Edit_Future_Postal_code: TUniDBEdit;
    Edit_Future_City: TUniDBEdit;
    Edit_Future_State: TUniDBEdit;
    Edit_Future_Country: TUniDBLookupComboBox;
    GroupboxAccountCredit: TUniGroupBox;
    Edit_BankAccount_Credit: TUniDBEdit;
    Edit_Iban_Credit: TUniDBEdit;
    Edit_Bic_Credit: TUniDBEdit;
    EditDateOfMarriage: TUniDBDateTimePicker;
    EditNameEmployer: TUniDBEdit;
    Edit_Profession: TUniDBLookupComboBox;
    EditTypeOfContract: TUniDBLookupComboBox;
    EditInServiceSince: TUniDBDateTimePicker;
    Professions: TIBCQuery;
    DsProfessions: TDataSource;
    ContractTypes: TIBCQuery;
    DsContractTypes: TDataSource;
    EditNumberOfDependentPersons: TUniDBNumberEdit;
    Edit_Contact_Type: TUniDBLookupComboBox;
    ContactTypes: TIBCQuery;
    DsContactTypes: TDataSource;
    EditInternalNumber: TUniDBEdit;
    Roles: TIBCQuery;
    DsRoles: TDataSource;
    Edit_contact_role: TUniDBLookupComboBox;
    EditDateFirstContact: TUniDBDateTimePicker;
    EditBankAccount: TUniDBEdit;
    ContactsSearch: TIBCQuery;
    DsContactsSearch: TDataSource;
    EditPartner: TUniDBLookupComboBox;
    Edit_phone_work: TUniDBEdit;
    EditLeadSourceList: TUniDBLookupComboBox;
    ContactSources: TIBCQuery;
    DsContactSources: TDataSource;
    ComboLanguageDoc: TUniComboBox;
    Edit_LinkedIn: TUniDBEdit;
    UniDBCheckBox1: TUniDBCheckBox;
    TabSheetMifidManagement: TUniTabSheet;
    CheckBoxContactCardAvailable: TUniDBCheckBox;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure LinkedLanguageContactsAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure EditZipCodeExit(Sender: TObject);
    procedure EditCityExit(Sender: TObject);
    procedure EditInternalNumberTriggerEvent(Sender: TUniFormControl;
      AButtonId: Integer);
    procedure Edit_AccountTriggerEvent(Sender: TUniCustomComboBox;
      AButtonId: Integer);
    procedure UniFormCreate(Sender: TObject);
    procedure Contacts_EditBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure CallBackSelectCityContact(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateAccount(Sender: TComponent; AResult: Integer);
    function  Save_contact: boolean;
  public
    { Public declarations }
    function Init_contact_creation(Crm_account: longint): boolean;
    function Init_contact_modification(Id: longint): boolean;
  end;

function ContactsAddFrm: TContactsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Utils, ZipCodesSelect, Main,
  AccountsAdd, System.StrUtils;

function ContactsAddFrm: TContactsAddFrm;
begin
  Result := TContactsAddFrm(UniMainModule.GetFormInstance(TContactsAddFrm));
end;

{ TContactsAddFrm }

procedure TContactsAddFrm.BtnCancelClick(Sender: TObject);
begin
  Contacts_Edit.Cancel;
  Contacts_Edit.Close;
  Close;
end;

procedure TContactsAddFrm.BtnSaveClick(Sender: TObject);
begin
  if Save_contact
  then begin
         Contacts_Edit.Close;
         Close;
       end;
end;

procedure TContactsAddFrm.CallBackSelectCityContact(Sender: TComponent;
  AResult: Integer);
begin
  if UniMainModule.Result_zipSelection.selected = true
  then begin
         Contacts_Edit.FieldByName('ADDRESS_POSTAL_CODE').AsString := UniMainModule.Result_zipSelection.zipcode;
         Contacts_Edit.FieldByName('ADDRESS_CITY').AsString        := UniMainModule.Result_zipSelection.city;
       end;
end;

procedure TContactsAddFrm.Contacts_EditBeforePost(DataSet: TDataSet);
begin
  Contacts_Edit.FieldByName('Last_name_sndx').AsString    := Soundex(Contacts_Edit.FieldByname('Last_name').asString, 15);
  Contacts_Edit.FieldByName('First_name_sndx').AsString   := Soundex(Contacts_Edit.FieldByname('First_name').AsString, 15);
end;

procedure TContactsAddFrm.EditCityExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(EditCity.Text) <> '') and (Trim(EditZipCode.Text) = '')
  then begin
         selectlocation := UniMainModule.Search_zipcode(UpperCase(EditCity.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Contacts_Edit.FieldByName('ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Contacts_Edit.FieldByName('ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_zipcode_selection(EditCity.Text);
                     ShowModal(CallBackSelectCityContact);
                   end;
              end
         else
       end;
end;

procedure TContactsAddFrm.EditInternalNumberTriggerEvent(
  Sender: TUniFormControl; AButtonId: Integer);
begin
  case AButtonId of
    0: begin
         if (Contacts_Edit.FieldByName('Internal_number').AsString) <> ''
         then if MainForm.WaveConfirmBlocking(str_assign_number, str_number_present) = False
              then exit;
         Contacts_Edit.FieldByName('Internal_number').AsString := IntToStr(UniMainModule.Get_document_sequence(-5));
         if UniMainModule.UpdTrans.Active
         then UniMainModule.UpdTrans.Commit;
       end;
  end;
end;

procedure TContactsAddFrm.EditZipCodeExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(EditZipCode.Text) <> '') and (Trim(EditCity.Text) = '')
  then begin
         selectlocation := UniMainModule.Search_city(UpperCase(EditZipCode.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Contacts_Edit.FieldByName('ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Contacts_Edit.FieldByName('ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_city_selection(EditZipCode.Text);
                     ShowModal(CallBackSelectCityContact);
                   end;
              end
         else
       end;
end;

procedure TContactsAddFrm.Edit_AccountTriggerEvent(Sender: TUniCustomComboBox;
  AButtonId: Integer);
begin
  case AButtonId of
    1: begin
         With AccountsAddFrm
         do begin
              if Init_account_creation
              then ShowModal(CallBackInsertUpdateAccount)
              else Close;
            end;
       end;
    2: begin
         if not Contacts_Edit.fieldbyname('Crm_Account').isNull
         then begin
                With AccountsAddFrm
                do begin
                     if Init_account_modification(Contacts_Edit.fieldbyname('Crm_Account').AsInteger)
                     then ShowModal(CallBackInsertUpdateAccount)
                     else Close;
                   end;
              end;
       end;
  end;
end;

procedure TContactsAddFrm.CallBackInsertUpdateAccount(Sender: TComponent; AResult: Integer);
begin
  if CrmAccounts.Active
  then begin
         CrmAccounts.Refresh;
         if UniMainModule.Result_dbAction <> 0
         then Contacts_Edit.FieldByName('Crm_Account').AsInteger := UniMainModule.Result_dbAction;
       end;
end;


function TContactsAddFrm.Init_contact_creation(Crm_account: longint): boolean;
begin
  Caption                                := str_add_contact;
  UniMainModule.Result_dbAction          := 0;
  ComboLanguageDoc.ItemIndex             := 0;
  TabSheetCreditManagement.TabVisible    := UniMainModule.portal_contr_credit_module_active;
  TabSheetMifidManagement.TabVisible     := UniMainModule.portal_contr_insurance_module_active;
  Open_reference_tables;

  Try
    Contacts_Edit.Close;
    Contacts_Edit.SQL.Clear;
    Contacts_Edit.SQL.Add('Select first 0 * from crm_contacts');
    Contacts_Edit.Open;
    Contacts_Edit.Append;
    Contacts_Edit.FieldByName('CompanyId').AsInteger              := UniMainModule.Company_id;
    Contacts_Edit.FieldByName('Removed').AsString                 := '0';
    Contacts_Edit.FieldByName('Do_not_call').AsInteger            := 0;
    Contacts_Edit.FieldByName('Portal_active').AsInteger          := 0;
    Contacts_Edit.FieldByName('Email_invalid').AsInteger          := 0;
    Contacts_Edit.FieldByName('Contact_card_available').AsInteger := 0;

    if Crm_Account <> 0
    then Contacts_Edit.FieldByName('CRM_ACCOUNT').AsInteger       := Crm_account;

    Contacts_Edit.FieldByName('DATE_ENTERED').AsDateTime          := Now;
    Contacts_Edit.FieldByName('DATE_MODIFIED').AsDateTime         := Now;
//    Contacts_Edit.FieldByName('CREATED_USER_ID').AsInteger        := UniMainModule.User_id;
//    Contacts_Edit.FieldByName('MODIFIED_USER_ID').AsInteger       := UniMainModule.User_id;
//    Contacts_Edit.FieldByName('ASSIGNED_USER_ID').AsInteger       := UniMainModule.User_id;
    Result                                                        := True;
  Except
    Contacts_Edit.Close;
    MainForm.WaveShowErrorToast(MsgErrorAddItem);
    Result := False;
  End;
end;

function TContactsAddFrm.Init_contact_modification(Id: longint): boolean;
begin
  Caption                             := str_modify_contact;
  UniMainModule.Result_dbAction       := 0;
  TabSheetCreditManagement.TabVisible := UniMainModule.portal_contr_credit_module_active;
  Open_reference_tables;
  Contacts_Edit.Close;
  Contacts_Edit.SQL.Clear;
  Contacts_Edit.SQL.Add('Select * from crm_contacts where id=' + IntToStr(Id));
  Contacts_Edit.Open;
  Try
    Contacts_Edit.Edit;
    ComboLanguageDoc.ItemIndex                                   := Contacts_Edit.FieldByName('Language_doc').AsInteger;
    Contacts_Edit.FieldByName('DATE_MODIFIED').AsDateTime        := Now;
//    Contacts_Edit.FieldByName('MODIFIED_USER_ID').AsInteger      := UniMainModule.User_id;
    Result                                                       := True;
  Except
    Result := False;
    Contacts_Edit.Cancel;
    Contacts_Edit.Close;
    MainForm.WaveShowWarningToast(MsgRecordLocked);
  End;
end;

procedure TContactsAddFrm.LinkedLanguageContactsAddChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContactsAddFrm.Open_reference_tables;
var SQL: string;
begin
  CrmAccounts.Close;
  CrmAccounts.SQL.Clear;
  SQL := 'Select id, name from crm_accounts where companyid=' + IntToStr(UniMainModule.Company_id) +
         ' and removed=''0'' order by name';
  CrmAccounts.SQL.Add(SQL);
  CrmAccounts.Open;

  Appelations.Close;
  Appelations.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=1 ' +
         ' and REMOVED=''0''';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       EditAppelation.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       EditAppelation.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       EditAppelation.ListField := 'ENGLISH';
     end;
  end;
  Appelations.SQL.Add(SQL);
  Appelations.Open;

  Titles.Close;
  Titles.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=10 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Title.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Title.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Title.ListField := 'ENGLISH';
     end;
  end;
  Titles.SQL.Add(SQL);
  Titles.Open;

  Departments.Close;
  Departments.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=11 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_department.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_department.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_department.ListField := 'ENGLISH';
     end;
  end;
  Departments.SQL.Add(SQL);
  Departments.Open;

  Countries.Close;
  Countries.SQL.Clear;
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := 'Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by dutch';
       ComboCountryCode.ListField    := 'DUTCH';
       Edit_birth_country.ListField  := 'DUTCH';
       Edit_Future_Country.ListField := 'DUTCH';
     end;
  2: begin
       SQL := 'Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by french';
       ComboCountryCode.ListField    := 'FRENCH';
       Edit_birth_country.ListField  := 'FRENCH';
       Edit_Future_Country.ListField := 'FRENCH';
     end;
  3: begin
       SQL := 'Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by english';
       ComboCountryCode.ListField    := 'ENGLISH';
       Edit_birth_country.ListField  := 'ENGLISH';
       Edit_Future_Country.ListField := 'ENGLISH';
     end;
  end;
  Countries.SQL.Add(SQL);
  Countries.Open;

  Users.Close;
  Users.SQL.Clear;
  Users.SQL.Add('Select ID, COALESCE(NAME, '''') || '' '' || COALESCE(FIRSTNAME, '''') AS "USERFULLNAME" from users ' +
                'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by USERFULLNAME');
  Users.Open;

  Languages.Close;
  Languages.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=3 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_language.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_language.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_language.ListField := 'ENGLISH';
     end;
  end;
  Languages.SQL.Add(SQL);
  Languages.Open;

  Nationalities.Close;
  Nationalities.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=8 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_nationality.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_nationality.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_nationality.ListField := 'ENGLISH';
     end;
  end;
  Nationalities.SQL.Add(SQL);
  Nationalities.Open;

  Sex.Close;
  Sex.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2000 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_sex.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_sex.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_sex.ListField := 'ENGLISH';
     end;
  end;
  Sex.SQL.Add(SQL);
  Sex.Open;

  MaritalStatus.Close;
  MaritalStatus.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2001 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_MaritalStatus.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_MaritalStatus.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_MaritalStatus.ListField := 'ENGLISH';
     end;
  end;
  MaritalStatus.SQL.Add(SQL);
  MaritalStatus.Open;

  MarriageSystem.Close;
  MarriageSystem.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2002 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_MarriageSystem.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_MarriageSystem.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_MarriageSystem.ListField := 'ENGLISH';
     end;
  end;
  MarriageSystem.SQL.Add(SQL);
  MarriageSystem.Open;

  Professions.Close;
  professions.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2003 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Profession.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Profession.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Profession.ListField := 'ENGLISH';
     end;
  end;
  Professions.SQL.Add(SQL);
  professions.Open;

  ContractTypes.Close;
  ContractTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2004 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       EditTypeOfContract.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       EditTypeOfContract.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       EditTypeOfContract.ListField := 'ENGLISH';
     end;
  end;
  ContractTypes.SQL.Add(SQL);
  ContractTypes.Open;

  ContactTypes.Close;
  ContactTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=14 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Contact_Type.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Contact_Type.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Contact_Type.ListField := 'ENGLISH';
     end;
  end;
  ContactTypes.SQL.Add(SQL);
  ContactTypes.Open;

  Roles.Close;
  Roles.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=17 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Contact_Role.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Contact_Role.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Contact_Role.ListField := 'ENGLISH';
     end;
  end;
  Roles.SQL.Add(SQL);
  Roles.Open;

  ContactSources.Close;
  ContactSources.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=18 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       EditLeadSourceList.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       EditLeadSourceList.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       EditLeadSourceList.ListField := 'ENGLISH';
     end;
  end;
  ContactSources.SQL.Add(SQL);
  ContactSources.Open;

  ContactsSearch.Close;
  ContactsSearch.SQL.Clear;
  SQL := 'Select id, coalesce(last_name, '''') || '' '' || coalesce(first_name, '''') as FullName ' +
         'from crm_contacts where CompanyID=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' order by FullName';
  ContactsSearch.SQL.Add(SQL);
  ContactsSearch.Open;
end;

function TContactsAddFrm.Save_contact: boolean;
var SQL: string;
begin
  Result := False;
  BtnSave.SetFocus;

  if Trim(Contacts_Edit.FieldByName('LAST_NAME').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_name_is_required);
         PageControlContacts.ActivePage := TabSheetGeneral;
         EditName.SetFocus;
         Exit;
       end;

  if ComboLanguageDoc.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_language_doc_required);
         PageControlContacts.ActivePage := TabSheetGeneral;
         ComboLanguageDoc.SetFocus;
         Exit;
       end;

  if (Contacts_Edit.FieldByName('PORTAL_ACTIVE').AsInteger = 1) and (Trim(Contacts_Edit.FieldByName('Email').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast(str_email_required_portal);
         PageControlContacts.ActivePage := TabSheetGeneral;
         Edit_email.SetFocus;
         Exit;
       end;

  //Check email
  if Trim(Contacts_Edit.FieldByName('Email').AsString) <> ''
  then if Utils.IsValidEmailRegEx(Contacts_Edit.FieldByName('Email').asString) = False
       then begin
              MainForm.WaveShowWarningToast(str_email_not_valid);
              PageControlContacts.ActivePage := TabSheetGeneral;
              Edit_Email.SetFocus;
              Exit;
            end;

  //Check for duplicate name + firstname
  if Contacts_Edit.State = dsInsert
  then begin
         if (Trim(Contacts_Edit.FieldByName('Last_name').AsString) <> '') and
            (Trim(Contacts_Edit.FieldByName('First_name').AsString) <> '')
         then SQL := 'Select count(*) as counter from crm_contacts where companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
                     'and removed=''0'' and Last_name=' + QuotedStr(Contacts_Edit.FieldByName('Last_Name').AsString) +  ' ' +
                     'and First_name=' + QuotedStr(Contacts_Edit.FieldByName('First_name').AsString)
         else SQL := 'Select count(*) as counter from crm_contacts where companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
                     'and removed=''0'' and Last_name=' + QuotedStr(Contacts_Edit.FieldByName('Last_Name').AsString);
         UniMainModule.QWork.Close;
         UniMainModule.QWork.SQL.Clear;
         UniMainModule.QWork.SQL.Add(SQL);
         UniMainModule.QWork.Open;
         if UniMainModule.QWork.FieldByName('Counter').AsInteger = 0
         then UniMainModule.QWork.Close
         else begin
                UniMainModule.QWork.Close;
                if MainForm.WaveConfirmBlocking(str_contact_identical_name, str_warning_contact_duplicate_name) = False
                then begin
                       Result := False;
                       Exit;
                     end;
              end;
       end;

  //check duplicate email addresses
  if Trim(Contacts_Edit.FieldByName('Email').asString) <> ''
  then begin
         UniMainModule.QWork.Close;
         UniMainModule.QWork.SQL.Clear;
         if Contacts_Edit.State = dsInsert
         then UniMainModule.QWork.SQL.Add('Select count (*) as counter from crm_contacts where email=' +
                                           QuotedStr(Contacts_Edit.FieldByName('Email').AsString) +
                                          ' and companyid=' + IntToStr(uniMainModule.Company_id) +
                                          ' and removed=''0''')
         else UniMainModule.QWork.SQL.Add('Select count (*) as counter from crm_contacts where email=' +
                                           QuotedStr(Contacts_Edit.FieldByName('Email').AsString) +
                                           ' and companyid=' + IntToStr(UniMainModule.Company_id) +
                                           ' and removed=''0'' and Id<>' + Contacts_Edit.FieldByName('Id').asString);

         UniMainModule.QWork.Open;
         if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0
         then begin
                UniMainModule.QWork.Close;
                MainForm.WaveShowWarningToast(str_warning_duplicate_email_contact);
              end
         else UniMainModule.QWork.Close;
       end;

  if (UniMainModule.Hypotheekwereld = True) and
     (Trim(Contacts_Edit.FieldByName('Email').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast('Email is verplicht in te geven');
         PagecontrolContacts.ActivePage := TabSheetGeneral;
         Edit_email.SetFocus;
         Exit;
       end;

  if (UniMainModule.Hypotheekwereld = True) and
     (Trim(Contacts_Edit.FieldByName('Phone_home').AsString) = '') and
     (Trim(Contacts_Edit.FieldByName('Phone_mobile').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast('Telefoon of gsm is verplicht in te geven');
         PagecontrolContacts.ActivePage := TabSheetGeneral;
         Exit;
       end;

  try
    Contacts_Edit.FieldByName('Language_doc').AsInteger       := ComboLanguageDoc.ItemIndex;
    Contacts_Edit.Post;

    if UpdTr_Contacts_Edit.Active
    then UpdTr_Contacts_Edit.Commit;

    Result := True;
    UniMainModule.Result_dbAction := Contacts_Edit.FieldByName('Id').AsInteger;
  except
    on E: Exception
    do begin
         if UpdTr_Contacts_Edit.Active
         then UpdTr_Contacts_Edit.Rollback;
         Result := False;
         MainForm.WaveShowErrorToast(str_error_save_data);
       end;
  end;
end;

procedure TContactsAddFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContactsAddFrm.UniFormReady(Sender: TObject);
begin
  Memo_Remarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
end;

procedure TContactsAddFrm.UpdateStrings;
begin
  str_warning_contact_duplicate_name  := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_warning_contact_duplicate_name' (* 'Opgelet, contact met identieke naam gevonden. Doorgaan?' *) );
  str_contact_identical_name          := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_contact_identical_name' (* 'Identieke naam' *) );
  str_warning_duplicate_email_contact := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_warning_duplicate_email_contact' (* 'Opgelet: er bestaat reeds een contact met hetzelfde email adres!' *) );
  str_language_doc_required           := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_language_doc_required' (* 'De taal voor de documenten is een vereiste ingave.' *) );
  str_number_present                  := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_number_present' (* 'Er is reeds een nummer aanwezig, wenst u door te gaan?' *) );
  str_assign_number                   := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_assign_number' (* 'Toekennen nummer' *) );
  str_email_not_valid                 := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_email_not_valid' (* 'Email adres is niet correct.' *) );
  MsgUpdateEidData                    := LinkedLanguageContactsAdd.GetTextOrDefault('strMsgUpdateEidData' (* 'Fout bij het bijwerken van de E-id database!' *) );
  MsgEidInfoRead                      := LinkedLanguageContactsAdd.GetTextOrDefault('strMsgEidInfoRead' (* 'E-id informatie ingelezen' *) );
  MsgNoEidFound                       := LinkedLanguageContactsAdd.GetTextOrDefault('strMsgNoEidFound' (* 'Geen E-Id gegevens gevonden' *) );
  MsgNoAddress                        := LinkedLanguageContactsAdd.GetTextOrDefault('strMsgNoAddress' (* 'Geen adres aanwezig' *) );
  MsgRecordLocked                     := LinkedLanguageContactsAdd.GetTextOrDefault('strMsgRecordLocked' (* 'In gebruik door een andere gebruiker!' *) );
  MsgErrorAddItem                     := LinkedLanguageContactsAdd.GetTextOrDefault('strMsgErrorAddItem' (* 'Fout bij het toevoegen!' *) );
  str_modify_contact                  := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_modify_contact' (* 'Wijzigen relatie' *) );
  str_add_contact                     := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_add_contact' (* 'Toevoegen relatie' *) );
  str_error_save_data                 := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_error_save_data' (* 'Fout bij het opslaan van de gegevens: ' *) );
  str_email_unique                    := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_email_unique' (* 'Er bestaat reeds een relatie met dit email adres, email adres moet uniek zijn.' *) );
  str_password_required_portal        := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_password_required_portal' (* 'Wachtwoord is een vereiste ingave indien de relatie toegang heeft tot het portaal.' *) );
  str_email_required_portal           := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_email_required_portal' (* 'Email adres is een vereiste ingave indien de relatie toegang heeft tot het portaal.' *) );
  str_name_is_required                := LinkedLanguageContactsAdd.GetTextOrDefault('strstr_name_is_required' (* 'De naam is een verplichte ingave.' *) );
end;

end.
