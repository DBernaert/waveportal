unit TasksAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniDBCheckBox, uniGroupBox, uniMultiItem, uniComboBox, uniDateTimePicker, uniDBDateTimePicker, uniButton,
  UniThemeButton, uniMemo, uniDBMemo, uniGUIBaseClasses, uniEdit, uniDBEdit,
  uniLabel, uniImage, siComp, siLngLnk;

type
  TTasksAddFrm = class(TUniForm)
    EditTitle: TUniDBEdit;
    EditDescription: TUniDBMemo;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditTaskStart: TUniDBDateTimePicker;
    ComboStatus: TUniComboBox;
    EditResponsable: TUniDBLookupComboBox;
    Responsables: TIBCQuery;
    DsResponsables: TDataSource;
    EditTaskEnd: TUniDBDateTimePicker;
    Tasks_Edit: TIBCQuery;
    Ds_Tasks_Edit: TDataSource;
    UpdTr_Tasks_Edit: TIBCTransaction;
    LinkedLanguageTasksAdd: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageTasksAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    function Init_task_creation(ModuleId: longint; RecordId: longint): boolean;
    function Init_task_modification(TaskId: longint): boolean;
  end;

var
	MsgStartDateRequired:   string = 'De begindatum is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgEndDateRequired:     string = 'De vervaldatum is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgResponsableRequired: string = 'De verantwoordelijke is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgErrorSavingData:     string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	MsgAddTask:             string = 'Toevoegen taak'; // TSI: Localized (Don't modify!)
	MsgModifyTask:          string = 'Wijzigen taak'; // TSI: Localized (Don't modify!)
	MsgRecordLocked:        string = 'In gebruik door een andere gebruiker!'; // TSI: Localized (Don't modify!)
	strEmployee:            string = 'Medewerker'; // TSI: Localized (Don't modify!)
	strApplicator:          string = 'Aanbrenger'; // TSI: Localized (Don't modify!)

function TasksAddFrm: TTasksAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function TasksAddFrm: TTasksAddFrm;
begin
  Result := TTasksAddFrm(UniMainModule.GetFormInstance(TTasksAddFrm));
end;

{ TTasksAddFrm }

procedure TTasksAddFrm.BtnCancelClick(Sender: TObject);
begin
  Tasks_Edit.Cancel;
  Tasks_Edit.Close;
  Close;
end;

procedure TTasksAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Tasks_Edit.FieldByName('Task_Start').IsNull
  then begin
         MainForm.WaveShowWarningToast(MsgStartDateRequired);
         EditTaskStart.SetFocus;
         Exit;
       end;

  if Tasks_Edit.FieldByName('Task_End').IsNull
  then begin
         MainForm.WaveShowWarningToast(MsgEndDateRequired);
         EditTaskEnd.SetFocus;
         Exit;
       end;

  if (Tasks_Edit.Fieldbyname('Responsable_key').isNull)
  then begin
         MainForm.WaveShowWarningToast(MsgResponsableRequired);
         EditResponsable.SetFocus;
         exit;
       end;

  Tasks_Edit.FieldByName('Task_status').AsInteger      := ComboStatus.ItemIndex;
  Tasks_Edit.FieldByName('Date_Modified').AsDateTime   := Now;
  Tasks_Edit.FieldByName('Modified_User_id').AsInteger := UniMainModule.User_id;

  //Fill in key fields
  if Responsables.Locate('Responsable_key', Tasks_Edit.FieldByName('Responsable_key').AsString, [])
  then begin
         Tasks_Edit.FieldByName('Responsable_type').AsInteger := Responsables.FieldByName('Responsable_type').AsInteger;
         Tasks_Edit.FieldByName('Responsable_id').AsInteger   := Responsables.FieldByName('Id').AsInteger;
       end;

  try
    Tasks_Edit.Post;
    if UpdTr_Tasks_Edit.Active
    then UpdTr_Tasks_Edit.Commit;
    UniMainModule.Result_dbAction := Tasks_Edit.FieldByName('Id').AsInteger;
  except
    on E: Exception
    do begin
         if UpdTr_Tasks_Edit.Active
         then UpdTr_Tasks_Edit.Rollback;
         MainForm.WaveShowErrorToast(MsgErrorSavingData + E.Message);
       end;
  end;
  Tasks_Edit.Close;
  Close;
end;

procedure TTasksAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

function TTasksAddFrm.Init_task_creation(ModuleId, RecordId: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  Caption              := MsgAddTask;
  Try
    Tasks_Edit.Close;
    Tasks_Edit.SQL.Clear;
    Tasks_Edit.SQL.Add('Select first 0 * from crm_tasks');
    Tasks_Edit.Open;
    Tasks_Edit.Append;
    Tasks_Edit.FieldByName('CompanyId').AsInteger      := UniMainModule.Company_id;
    Tasks_Edit.FieldByName('Removed').AsString         := '0';
    Tasks_Edit.FieldByName('Module_id').AsInteger      := ModuleId;
    Tasks_Edit.FieldByName('Record_id').AsInteger      := RecordId;
    Tasks_Edit.FieldByName('Date_entered').AsDateTime  := Now;
    Tasks_Edit.FieldByName('Date_modified').AsDateTime := Now;
    Tasks_Edit.FieldByName('Task_start').AsDateTime    := Date;
    Tasks_Edit.FieldByName('Task_end').AsDateTime      := Date;
    Open_reference_tables;
    Result                                             := True;
  Except
    Tasks_Edit.Close;
    Result                                             := False;
  End;
end;

function TTasksAddFrm.Init_task_modification(TaskId: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  Caption              := MsgModifyTask;
  Try
    Tasks_Edit.Close;
    Tasks_Edit.SQL.Clear;
    Tasks_Edit.SQL.Add('Select * from crm_tasks where id=' + IntToStr(TaskId));
    Tasks_Edit.Open;
    Tasks_Edit.Edit;
    ComboStatus.ItemIndex       := Tasks_Edit.FieldByName('Task_status').AsInteger;
    Open_reference_tables;
    Result                      := True;
  Except
    Tasks_Edit.Cancel;
    Tasks_Edit.Close;
    Result                      := False;
    MainForm.WaveShowWarningToast(MsgRecordLocked);
  End;
end;

procedure TTasksAddFrm.LinkedLanguageTasksAddChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TTasksAddFrm.Open_reference_tables;
var SQL: string;
begin
  Responsables.Close;
  Responsables.SQL.Clear;
  SQL := 'Select responsable_key, id, responsable_type, fullname, responsable_type_description from ' +
         '(Select 1 || id as responsable_key, id, 1 as responsable_type, coalesce(name, '''') || '' '' || ' +
         'coalesce(firstname, '''') as fullname, ''' + strEmployee + ''' as responsable_type_description ' +
         'from users where users.COMPANYID = ' + IntToStr(UniMainModule.Company_id) + ' and users.REMOVED=''0'' ' +
         'union all ' +
         'Select 2 || id as responsable_key, id, 2 as responable_type, coalesce(last_name, '''') || '' '' || ' +
         'coalesce(first_name, '''') as fullname, ''' + strApplicator + ''' as responsable_type_description ' +
         'from credit_contributors where credit_contributors.COMPANYID = ' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and credit_contributors.REMOVED=''0'' and credit_contributors.ID=' + IntToStr(UniMainModule.User_id) + ') ' +
         'order by fullname';
  Responsables.SQL.Add(SQL);
  Responsables.Open;
end;

procedure TTasksAddFrm.UniFormReady(Sender: TObject);
begin
  EditDescription.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

procedure TTasksAddFrm.UpdateStrings;
begin
  strApplicator          := LinkedLanguageTasksAdd.GetTextOrDefault('strstrApplicator' (* 'Aanbrenger' *) );
  strEmployee            := LinkedLanguageTasksAdd.GetTextOrDefault('strstrEmployee' (* 'Medewerker' *) );
  MsgRecordLocked        := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgRecordLocked' (* 'In gebruik door een andere gebruiker!' *) );
  MsgModifyTask          := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgModifyTask' (* 'Wijzigen taak' *) );
  MsgAddTask             := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgAddTask' (* 'Toevoegen taak' *) );
  MsgErrorSavingData     := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgErrorSavingData' (* 'Fout bij het opslaan van de gegevens: ' *) );
  MsgResponsableRequired := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgResponsableRequired' (* 'De verantwoordelijke is een verplichte ingave.' *) );
  MsgEndDateRequired     := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgEndDateRequired' (* 'De vervaldatum is een verplichte ingave.' *) );
  MsgStartDateRequired   := LinkedLanguageTasksAdd.GetTextOrDefault('strMsgStartDateRequired' (* 'De begindatum is een verplichte ingave.' *) );
end;

end.

