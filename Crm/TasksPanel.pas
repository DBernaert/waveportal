unit TasksPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniMemo, uniDBMemo, uniBasicGrid, uniDBGrid, uniButton,
  UniThemeButton, uniDBNavigator, uniGUIBaseClasses, uniPanel, siComp,
  siLngLnk, uniImage;

type
  TTasksPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    ContainerTasksBody: TUniContainerPanel;
    GridTasks: TUniDBGrid;
    Tasks: TIBCQuery;
    DsTasks: TDataSource;
    TasksID: TLargeintField;
    TasksMODULE_ID: TLargeintField;
    TasksRECORD_ID: TLargeintField;
    TasksTITLE: TWideStringField;
    TasksDESCRIPTION: TWideMemoField;
    TasksTASK_START: TDateField;
    TasksTASK_STATUS: TIntegerField;
    TasksTASK_STATUS_DESCRIPTION: TWideStringField;
    TasksRESPONSABLE_NAME: TWideStringField;
    TasksTASK_END: TDateField;
    LinkedLanguageTasksPanel: TsiLangLinked;
    MemoDescription: TUniDBMemo;
    BtnAddTask: TUniThemeButton;
    BtnModifyTask: TUniThemeButton;
    BtnDeleteTask: TUniThemeButton;
    procedure BtnAddTaskClick(Sender: TObject);
    procedure BtnDeleteTaskClick(Sender: TObject);
    procedure BtnModifyTaskClick(Sender: TObject);
    procedure GridTasksDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
    procedure LinkedLanguageTasksPanelChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
  private
    { Private declarations }
    ModuleId: integer;
    procedure CallBackInsertUpdateTasksTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_tasks(IdModule: integer; MasterDs: TDataSource);
  end;

var
	MsgDeleteTask:   string = 'Verwijderen taak?'; // TSI: Localized (Don't modify!)
	MsgErrorDelete:  string = 'Fout bij het verwijderen: '; // TSI: Localized (Don't modify!)
	strTasks:        string = 'Taken'; // TSI: Localized (Don't modify!)
	strNotStarted:   string = 'Niet gestart'; // TSI: Localized (Don't modify!)
	strInExecution:  string = 'Wordt uitgevoerd'; // TSI: Localized (Don't modify!)
	strCompleted:    string = 'Voltooid'; // TSI: Localized (Don't modify!)
	strWaitingFor:   string = 'Wachten op iemand anders'; // TSI: Localized (Don't modify!)
	strDelayed:      string = 'Uitgesteld'; // TSI: Localized (Don't modify!)

implementation

{$R *.dfm}

uses MainModule, ServerModule, UniFSConfirm, TasksAdd, Main;

{ TTasksPanelFrm }

procedure TTasksPanelFrm.BtnAddTaskClick(Sender: TObject);
begin
  if Tasks.MasterSource.DataSet.FieldByName('Id').isNull
  then exit;

  With TasksAddFrm
  do begin
       if Init_task_creation(ModuleId, Tasks.MasterSource.DataSet.FieldByName('Id').AsInteger) = True
       then ShowModal(CallBackInsertUpdateTasksTable)
       else Close;
     end;
end;

procedure TTasksPanelFrm.BtnDeleteTaskClick(Sender: TObject);
begin
  if (not Tasks.Active) or (Tasks.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Portal_name, MsgDeleteTask, 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.QBatch.Close;
          UniMainModule.QBatch.SQL.Clear;
          UniMainModule.QBatch.SQL.Add('Update Crm_Tasks set removed=''1'' where id=' + Tasks.fieldbyname('Id').asString);
          UniMainModule.QBatch.ExecSQL;
          if UniMainModule.UpdTr_Batch.Active
          then UniMainModule.UpdTr_Batch.Commit;
          UniMainModule.QBatch.Close;
        Except
          on E: EDataBaseError
          do begin
              if UniMainModule.UpdTr_Batch.Active
              then UniMainModule.UpdTr_Batch.Rollback;
              UniMainModule.QBatch.Close;
              MainForm.WaveShowErrorToast(MsgErrorDelete + E.Message);
             end;
        end;
        Tasks.Refresh;
      end;
    end);
end;

procedure TTasksPanelFrm.BtnModifyTaskClick(Sender: TObject);
begin
  if (Tasks.Active) and (not Tasks.FieldByName('Id').IsNull)
  then begin
         With TasksAddFrm
         do begin
              if Init_task_modification(Tasks.FieldByName('Id').AsInteger) = True
              then ShowModal(CallBackInsertUpdateTasksTable)
              else Close;
            end;
       end;
end;

procedure TTasksPanelFrm.CallBackInsertUpdateTasksTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Tasks.Active
         then begin
                Tasks.Refresh;
                Tasks.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TTasksPanelFrm.GridTasksDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
  Attribs: TUniCellAttribs);
begin
  if (UpperCase(Column.FieldName) = 'TASK_END')
  then begin
         if ((Tasks.FieldByName('Task_end').AsDateTime < Date) and
             (Tasks.FieldByName('Task_status').AsInteger <> 2))
         then begin
                Attribs.Color      := UniMainModule.Color_red;
                Attribs.Font.Color := ClWhite;
              end;
       end;
end;

procedure TTasksPanelFrm.LinkedLanguageTasksPanelChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TTasksPanelFrm.Start_tasks(IdModule: integer; MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;
  Tasks.Close;
  Tasks.SQL.Clear;
  SQL := 'Select id, module_id, record_id, title, description, task_start, task_end, task_status, ' +
         'Case ' +
         'WHEN task_status = 0 THEN ''' + strNotStarted  + ''' ' +
         'WHEN task_status = 1 THEN ''' + strInExecution + ''' ' +
         'WHEN task_status = 2 THEN ''' + strCompleted   + ''' ' +
         'WHEN task_status = 3 THEN ''' + strWaitingFor  + ''' ' +
         'WHEN task_status = 4 THEN ''' + strDelayed     + ''' ' +
         'END as task_status_description, ' +
         'CASE ' +
         'WHEN Responsable_type = 1 THEN (Select COALESCE(USR.NAME, '''') || '' '' || COALESCE(USR.FIRSTNAME, '''') from USERS USR where ID = Responsable_id) ' +
         'WHEN Responsable_type = 2 THEN (Select COALESCE(CONTR.LAST_NAME, '''') || '' '' || COALESCE(CONTR.FIRST_NAME, '''') from Credit_Contributors CONTR where ID = Responsable_id) ' +
         'END AS responsable_name ' +
         'from crm_tasks ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) + ' and Module_id=' + IntToStr(IdModule) + ' and removed=''0''';

  SQL := SQL + ' order by task_start desc';
  Tasks.SQL.Add(SQL);
  Tasks.MasterSource := MasterDs;
  Tasks.MasterFields := 'ID';
  Tasks.DetailFields := 'RECORD_ID';
  Tasks.Open;
end;

procedure TTasksPanelFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TTasksPanelFrm.UpdateStrings;
begin
  strDelayed      := LinkedLanguageTasksPanel.GetTextOrDefault('strstrDelayed' (* 'Uitgesteld' *) );
  strWaitingFor   := LinkedLanguageTasksPanel.GetTextOrDefault('strstrWaitingFor' (* 'Wachten op iemand anders' *) );
  strCompleted    := LinkedLanguageTasksPanel.GetTextOrDefault('strstrCompleted' (* 'Voltooid' *) );
  strInExecution  := LinkedLanguageTasksPanel.GetTextOrDefault('strstrInExecution' (* 'Wordt uitgevoerd' *) );
  strNotStarted   := LinkedLanguageTasksPanel.GetTextOrDefault('strstrNotStarted' (* 'Niet gestart' *) );
  strTasks        := LinkedLanguageTasksPanel.GetTextOrDefault('strstrTasks' (* 'Taken' *) );
  MsgErrorDelete  := LinkedLanguageTasksPanel.GetTextOrDefault('strMsgErrorDelete' (* 'Fout bij het verwijderen: ' *) );
  MsgDeleteTask   := LinkedLanguageTasksPanel.GetTextOrDefault('strMsgDeleteTask' (* 'Verwijderen taak?' *) );
end;

end.

