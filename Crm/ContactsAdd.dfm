object ContactsAddFrm: TContactsAddFrm
  Left = 0
  Top = 0
  ClientHeight = 665
  ClientWidth = 913
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditAppelation
  Images = UniMainModule.ImageList
  ImageIndex = 27
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  TextHeight = 15
  object PageControlContacts: TUniPageControl
    Left = 16
    Top = 16
    Width = 881
    Height = 585
    Hint = ''
    ActivePage = TabSheetGeneral
    Images = UniMainModule.ImageList
    TabOrder = 0
    object TabSheetGeneral: TUniTabSheet
      Hint = ''
      ImageIndex = 22
      Caption = 'Algemene informatie'
      object EditAppelation: TUniDBLookupComboBox
        Left = 16
        Top = 16
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsAppelations
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'SALUTATION'
        DataSource = Ds_Contacts_Edit
        TabOrder = 0
        Color = clWindow
        FieldLabel = 'Aanspreektitel *'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditName: TUniDBEdit
        Left = 16
        Top = 48
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'LAST_NAME'
        DataSource = Ds_Contacts_Edit
        TabOrder = 1
        FieldLabel = 'Naam *'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object EditFirstName: TUniDBEdit
        Left = 16
        Top = 80
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'FIRST_NAME'
        DataSource = Ds_Contacts_Edit
        TabOrder = 2
        FieldLabel = 'Voornaam'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object EditAddress: TUniDBEdit
        Left = 16
        Top = 144
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'ADDRESS_STREET'
        DataSource = Ds_Contacts_Edit
        TabOrder = 4
        FieldLabel = 'Adres'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object EditZipCode: TUniDBEdit
        Left = 16
        Top = 176
        Width = 289
        Height = 22
        Hint = ''
        DataField = 'ADDRESS_POSTAL_CODE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 5
        FieldLabel = 'Postcode'
        FieldLabelWidth = 110
        SelectOnFocus = True
        OnExit = EditZipCodeExit
      end
      object EditCity: TUniDBEdit
        Left = 16
        Top = 208
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'ADDRESS_CITY'
        DataSource = Ds_Contacts_Edit
        TabOrder = 6
        FieldLabel = 'Locatie'
        FieldLabelWidth = 110
        SelectOnFocus = True
        OnExit = EditCityExit
      end
      object ComboCountryCode: TUniDBLookupComboBox
        Left = 16
        Top = 240
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsCountries
        KeyField = 'CODE'
        ListFieldIndex = 0
        DataField = 'ADDRESS_COUNTRY'
        DataSource = Ds_Contacts_Edit
        TabOrder = 7
        Color = clWindow
        FieldLabel = 'Land'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditPhone: TUniDBEdit
        Left = 16
        Top = 272
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'PHONE_HOME'
        DataSource = Ds_Contacts_Edit
        TabOrder = 8
        FieldLabel = 'Telefoon thuis'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_mobile: TUniDBEdit
        Left = 16
        Top = 304
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'PHONE_MOBILE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 9
        FieldLabel = 'Gsm'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_email: TUniDBEdit
        Left = 16
        Top = 336
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'EMAIL'
        DataSource = Ds_Contacts_Edit
        TabOrder = 10
        FieldLabel = 'Email'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Checkbox_do_not_call: TUniDBCheckBox
        Left = 448
        Top = 80
        Width = 409
        Height = 17
        Hint = ''
        DataField = 'DO_NOT_CALL'
        DataSource = Ds_Contacts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 18
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Niet opbellen'
        FieldLabelWidth = 110
      end
      object Edit_birthdate: TUniDBDateTimePicker
        Left = 16
        Top = 440
        Width = 409
        Hint = ''
        DataField = 'BIRTHDATE'
        DataSource = Ds_Contacts_Edit
        DateTime = 43589.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 13
        FieldLabel = 'Geboortedatum'
        FieldLabelWidth = 110
      end
      object Edit_Title: TUniDBLookupComboBox
        Left = 448
        Top = 16
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsTitles
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'TITLE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 16
        Color = clWindow
        FieldLabel = 'Functie'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_department: TUniDBLookupComboBox
        Left = 448
        Top = 48
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsDepartments
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'DEPARTMENT'
        DataSource = Ds_Contacts_Edit
        TabOrder = 17
        Color = clWindow
        FieldLabel = 'Afdeling'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_Assigned_User_Id: TUniDBLookupComboBox
        Left = 448
        Top = 112
        Width = 409
        Hint = ''
        ListField = 'USERFULLNAME'
        ListSource = DsUsers
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'ASSIGNED_USER_ID'
        DataSource = Ds_Contacts_Edit
        TabOrder = 19
        Color = clWindow
        FieldLabel = 'Eigenaar'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_language: TUniDBLookupComboBox
        Left = 16
        Top = 408
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsLanguages
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'LANGUAGE'
        DataSource = Ds_Contacts_Edit
        AnyMatch = True
        TabOrder = 12
        Color = clWindow
        FieldLabel = 'Taal'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_lead_source: TUniDBEdit
        Left = 448
        Top = 208
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'LEAD_SOURCE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 22
        FieldLabel = 'Bron (vrij)'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Account: TUniDBLookupComboBox
        Left = 448
        Top = 304
        Width = 409
        Hint = ''
        ListField = 'NAME'
        ListSource = DsCrmAccounts
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'CRM_ACCOUNT'
        DataSource = Ds_Contacts_Edit
        TabOrder = 25
        Color = clWindow
        FieldLabel = 'Bedrijf'
        FieldLabelWidth = 110
        ForceSelection = True
        Triggers = <
          item
            ButtonId = 0
          end
          item
            ImageIndex = 5
            ButtonId = 1
            HandleClicks = True
            Hint = 'Toevoegen'
          end
          item
            ImageIndex = 1
            ButtonId = 2
            HandleClicks = True
            Hint = 'Wijzigen'
          end>
        Images = UniMainModule.ImageButtons
        Style = csDropDown
        OnTriggerEvent = Edit_AccountTriggerEvent
      end
      object Memo_Remarks: TUniDBMemo
        Left = 448
        Top = 376
        Width = 409
        Height = 153
        Hint = ''
        DataField = 'REMARKS'
        DataSource = Ds_Contacts_Edit
        TabOrder = 27
        FieldLabel = 'Opmerkingen'
        FieldLabelWidth = 110
        FieldLabelAlign = laTop
      end
      object Edit_Contact_Type: TUniDBLookupComboBox
        Left = 448
        Top = 240
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsContactTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'CONTACT_TYPE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 23
        Color = clWindow
        FieldLabel = 'Type'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditInternalNumber: TUniDBEdit
        Left = 16
        Top = 112
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'INTERNAL_NUMBER'
        DataSource = Ds_Contacts_Edit
        TabOrder = 3
        FieldLabel = 'Intern nr.'
        FieldLabelWidth = 110
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 5
            ButtonId = 0
            HandleClicks = True
            Hint = 'Toekennen nummer'
          end>
        OnTriggerEvent = EditInternalNumberTriggerEvent
      end
      object Edit_contact_role: TUniDBLookupComboBox
        Left = 448
        Top = 272
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsRoles
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'CONTACT_ROLE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 24
        Color = clWindow
        FieldLabel = 'Rol'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditDateFirstContact: TUniDBDateTimePicker
        Left = 16
        Top = 504
        Width = 409
        Hint = ''
        DataField = 'DATE_FIRST_CONTACT'
        DataSource = Ds_Contacts_Edit
        DateTime = 43949.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 15
        FieldLabel = 'Eerste contact'
        FieldLabelWidth = 110
      end
      object EditBankAccount: TUniDBEdit
        Left = 16
        Top = 472
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'BANKACCOUNT'
        DataSource = Ds_Contacts_Edit
        TabOrder = 14
        FieldLabel = 'Bankrekening'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object EditPartner: TUniDBLookupComboBox
        Left = 448
        Top = 144
        Width = 409
        Hint = ''
        ListField = 'FULLNAME'
        ListSource = DsContactsSearch
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'PARTNER'
        DataSource = Ds_Contacts_Edit
        TabOrder = 20
        Color = clWindow
        FieldLabel = 'Partner'
        FieldLabelWidth = 110
        Style = csDropDown
      end
      object EditLeadSourceList: TUniDBLookupComboBox
        Left = 448
        Top = 176
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsContactSources
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'CONTACT_SOURCE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 21
        Color = clWindow
        FieldLabel = 'Bron'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object ComboLanguageDoc: TUniComboBox
        Left = 448
        Top = 336
        Width = 409
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Nederlands'
          'Frans'
          'Engels')
        TabOrder = 26
        FieldLabel = 'Taal doc. *'
        FieldLabelWidth = 110
        IconItems = <>
      end
      object UniDBCheckBox1: TUniDBCheckBox
        Left = 16
        Top = 376
        Width = 409
        Height = 17
        Hint = ''
        DataField = 'EMAIL_INVALID'
        DataSource = Ds_Contacts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 11
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Email niet geldig'
        FieldLabelWidth = 110
      end
    end
    object TabSheetAdditional: TUniTabSheet
      Hint = ''
      ImageIndex = 3
      Caption = 'Bijkomende informatie'
      object LblPhoto: TUniLabel
        Left = 448
        Top = 176
        Width = 27
        Height = 13
        Hint = ''
        Caption = 'Foto:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 18
      end
      object PanelPhoto: TUniPanel
        Left = 448
        Top = 208
        Width = 377
        Height = 273
        Hint = ''
        TabOrder = 19
        BorderStyle = ubsSolid
        Caption = ''
        Layout = 'border'
        object ImageLogo: TUniDBImage
          Left = 72
          Top = 64
          Width = 231
          Height = 175
          Hint = ''
          DataField = 'PICTURE'
          DataSource = Ds_Contacts_Edit
          AutoSize = True
          Transparent = True
          LayoutConfig.Region = 'center'
        end
      end
      object Edit_birth_place: TUniDBEdit
        Left = 16
        Top = 16
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'BIRTH_PLACE'
        DataSource = Ds_Contacts_Edit
        TabOrder = 0
        FieldLabel = 'Geboorteplaats'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_birth_country: TUniDBLookupComboBox
        Left = 16
        Top = 48
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsCountries
        KeyField = 'CODE'
        ListFieldIndex = 0
        DataField = 'BIRTH_COUNTRY'
        DataSource = Ds_Contacts_Edit
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Geboorteland'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_nationality: TUniDBLookupComboBox
        Left = 16
        Top = 80
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsNationalities
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'NATIONALITY'
        DataSource = Ds_Contacts_Edit
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Nationaliteit'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_nis_number: TUniDBEdit
        Left = 16
        Top = 112
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'NIS_NUMBER'
        DataSource = Ds_Contacts_Edit
        TabOrder = 3
        FieldLabel = 'Rijksregisternr.'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_id_cardnumber: TUniDBEdit
        Left = 16
        Top = 144
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'ID_CARDNUMBER'
        DataSource = Ds_Contacts_Edit
        TabOrder = 4
        FieldLabel = 'Identiteitskaartnr.'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_MaritalStatus: TUniDBLookupComboBox
        Left = 16
        Top = 272
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsMaritalStatus
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'MARITAL_STATUS'
        DataSource = Ds_Contacts_Edit
        TabOrder = 8
        Color = clWindow
        FieldLabel = 'Burgerlijke staat'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_sex: TUniDBLookupComboBox
        Left = 16
        Top = 240
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsSex
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'SEX'
        DataSource = Ds_Contacts_Edit
        TabOrder = 7
        Color = clWindow
        FieldLabel = 'Geslacht'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_MarriageSystem: TUniDBLookupComboBox
        Left = 16
        Top = 304
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsMarriageSystem
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'MARRIAGE_SYSTEM'
        DataSource = Ds_Contacts_Edit
        TabOrder = 9
        Color = clWindow
        FieldLabel = 'Huwelijksstelsel'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditNumberOfDependentChildren: TUniDBNumberEdit
        Left = 16
        Top = 368
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'NUMBER_DEPENDENT_CHILDREN'
        DataSource = Ds_Contacts_Edit
        TabOrder = 11
        SelectOnFocus = True
        FieldLabel = 'Kinderen ten laste'
        FieldLabelWidth = 110
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
      object EditIdCardValidFrom: TUniDBDateTimePicker
        Left = 16
        Top = 176
        Width = 409
        Hint = ''
        DataField = 'ID_CARD_VALID_FROM'
        DataSource = Ds_Contacts_Edit
        DateTime = 43743.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 5
        FieldLabel = 'Geldig van'
        FieldLabelWidth = 110
      end
      object EditIdCardValidUntil: TUniDBDateTimePicker
        Left = 16
        Top = 208
        Width = 409
        Hint = ''
        DataField = 'ID_CARD_VALID_UNTIL'
        DataSource = Ds_Contacts_Edit
        DateTime = 43743.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 6
        FieldLabel = 'Geldig tot'
        FieldLabelWidth = 110
      end
      object EditDateOfMarriage: TUniDBDateTimePicker
        Left = 16
        Top = 336
        Width = 409
        Hint = ''
        DataField = 'DATE_OF_MARRIAGE'
        DataSource = Ds_Contacts_Edit
        DateTime = 43743.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 10
        FieldLabel = 'Huwelijksdatum'
        FieldLabelWidth = 110
      end
      object EditNameEmployer: TUniDBEdit
        Left = 448
        Top = 16
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'NAME_EMPLOYER'
        DataSource = Ds_Contacts_Edit
        TabOrder = 13
        FieldLabel = 'Naam werkgever'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Profession: TUniDBLookupComboBox
        Left = 448
        Top = 80
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsProfessions
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'PROFESSION'
        DataSource = Ds_Contacts_Edit
        TabOrder = 15
        Color = clWindow
        FieldLabel = 'Beroep'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditTypeOfContract: TUniDBLookupComboBox
        Left = 448
        Top = 112
        Width = 409
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsContractTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'TYPE_OF_CONTRACT'
        DataSource = Ds_Contacts_Edit
        TabOrder = 16
        Color = clWindow
        FieldLabel = 'Type contract'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object EditInServiceSince: TUniDBDateTimePicker
        Left = 448
        Top = 144
        Width = 409
        Hint = ''
        DataField = 'IN_SERVICE_SINCE'
        DataSource = Ds_Contacts_Edit
        DateTime = 43743.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 17
        FieldLabel = 'In dienst sinds'
        FieldLabelWidth = 110
      end
      object EditNumberOfDependentPersons: TUniDBNumberEdit
        Left = 16
        Top = 400
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'NUMBER_OF_DEPENDENT_PERSONS'
        DataSource = Ds_Contacts_Edit
        TabOrder = 12
        SelectOnFocus = True
        FieldLabel = 'Pers. ten laste'
        FieldLabelWidth = 110
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
      object Edit_phone_work: TUniDBEdit
        Left = 448
        Top = 48
        Width = 409
        Height = 22
        Hint = ''
        DataField = 'PHONE_WORK'
        DataSource = Ds_Contacts_Edit
        TabOrder = 14
        FieldLabel = 'Telefoon werk'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
    end
    object TabSheetCreditManagement: TUniTabSheet
      Hint = ''
      ImageIndex = 21
      Caption = 'Kredietbeheer'
      object GroupBoxFutureAddress: TUniGroupBox
        Left = 16
        Top = 16
        Width = 417
        Height = 193
        Hint = ''
        Caption = 'Toekomstig adres'
        TabOrder = 0
        object Edit_Future_Street: TUniDBEdit
          Left = 16
          Top = 24
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_STREET'
          DataSource = Ds_Contacts_Edit
          TabOrder = 1
          FieldLabel = 'Straat'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Future_Postal_code: TUniDBEdit
          Left = 16
          Top = 56
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_POSTAL_CODE'
          DataSource = Ds_Contacts_Edit
          TabOrder = 2
          FieldLabel = 'Postcode'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Future_City: TUniDBEdit
          Left = 16
          Top = 88
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_CITY'
          DataSource = Ds_Contacts_Edit
          TabOrder = 3
          FieldLabel = 'Locatie'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Future_State: TUniDBEdit
          Left = 16
          Top = 120
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_STATE'
          DataSource = Ds_Contacts_Edit
          TabOrder = 4
          FieldLabel = 'Provincie'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Future_Country: TUniDBLookupComboBox
          Left = 16
          Top = 152
          Width = 385
          Hint = ''
          ListField = 'DUTCH'
          ListSource = DsCountries
          KeyField = 'CODE'
          ListFieldIndex = 0
          DataField = 'FUTURE_ADDRESS_COUNTRY'
          DataSource = Ds_Contacts_Edit
          TabOrder = 5
          Color = clWindow
          FieldLabel = 'Land'
          FieldLabelWidth = 110
          ForceSelection = True
          Style = csDropDown
        end
      end
      object GroupboxAccountCredit: TUniGroupBox
        Left = 440
        Top = 16
        Width = 417
        Height = 137
        Hint = ''
        Caption = 'Rekening krediet'
        TabOrder = 1
        object Edit_BankAccount_Credit: TUniDBEdit
          Left = 16
          Top = 24
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'BANKACCOUNT_CREDIT'
          DataSource = Ds_Contacts_Edit
          TabOrder = 1
          FieldLabel = 'Bankrekening'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Iban_Credit: TUniDBEdit
          Left = 16
          Top = 56
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'IBAN_CREDIT'
          DataSource = Ds_Contacts_Edit
          TabOrder = 2
          FieldLabel = 'Iban'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Bic_Credit: TUniDBEdit
          Left = 16
          Top = 88
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'BIC_CREDIT'
          DataSource = Ds_Contacts_Edit
          TabOrder = 3
          FieldLabel = 'Bic'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
      end
    end
    object TabSheetMifidManagement: TUniTabSheet
      Hint = ''
      ImageIndex = 97
      Caption = 'Mifid'
      object CheckBoxContactCardAvailable: TUniDBCheckBox
        Left = 16
        Top = 16
        Width = 433
        Height = 17
        Hint = ''
        DataField = 'CONTACT_CARD_AVAILABLE'
        DataSource = Ds_Contacts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 0
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Klantenfiche aanwezig'
        FieldLabelWidth = 200
      end
    end
    object TabSheetSocialMedia: TUniTabSheet
      Hint = ''
      ImageIndex = 60
      Caption = 'Social media'
      object Edit_Facebook: TUniDBEdit
        Left = 16
        Top = 16
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'FACEBOOK'
        DataSource = Ds_Contacts_Edit
        TabOrder = 0
        FieldLabel = 'Facebook'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Twitter: TUniDBEdit
        Left = 16
        Top = 48
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'TWITTER'
        DataSource = Ds_Contacts_Edit
        TabOrder = 1
        FieldLabel = 'Twitter'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_instagram: TUniDBEdit
        Left = 16
        Top = 80
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'INSTAGRAM'
        DataSource = Ds_Contacts_Edit
        TabOrder = 2
        FieldLabel = 'Instagram'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_GooglePlus: TUniDBEdit
        Left = 16
        Top = 112
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'GOOGLEPLUS'
        DataSource = Ds_Contacts_Edit
        TabOrder = 3
        FieldLabel = 'Google +'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_LinkedIn: TUniDBEdit
        Left = 16
        Top = 144
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'LINKEDIN'
        DataSource = Ds_Contacts_Edit
        TabOrder = 4
        FieldLabel = 'LinkedIn'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
    end
  end
  object BtnSave: TUniThemeButton
    Left = 632
    Top = 616
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 1
    ScreenMask.Target = Owner
    OnClick = BtnSaveClick
    ButtonTheme = uctSuccess
  end
  object BtnCancel: TUniThemeButton
    Left = 768
    Top = 616
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 2
    ScreenMask.Target = Owner
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object ImageLastModification: TUniImage
    Left = 16
    Top = 616
    Width = 16
    Height = 16
    Cursor = crHandPoint
    Hint = 'Laatste wijziging'
    ShowHint = True
    ParentShowHint = False
    Stretch = True
    Url = 'images/wv_time_gray.svg'
  end
  object LblLastModification: TUniDBText
    Left = 40
    Top = 616
    Width = 101
    Height = 13
    Cursor = crHandPoint
    Hint = ''
    DataField = 'DATE_MODIFIED'
    DataSource = Ds_Contacts_Edit
    ParentFont = False
    Font.Color = 9474192
  end
  object Appelations: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 728
    Top = 56
  end
  object DsAppelations: TDataSource
    DataSet = Appelations
    Left = 728
    Top = 111
  end
  object Countries: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from countries order by dutch')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 560
    Top = 56
  end
  object DsCountries: TDataSource
    DataSet = Countries
    Left = 560
    Top = 111
  end
  object Users: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, NAME || '#39' '#39' || FIRSTNAME AS "USERFULLNAME" from users'
      'order by USERFULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 488
    Top = 216
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 488
    Top = 272
  end
  object Titles: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 800
    Top = 56
  end
  object DsTitles: TDataSource
    DataSet = Titles
    Left = 800
    Top = 111
  end
  object Departments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 672
    Top = 216
  end
  object DsDepartments: TDataSource
    DataSet = Departments
    Left = 672
    Top = 271
  end
  object Languages: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 576
    Top = 216
  end
  object DsLanguages: TDataSource
    DataSet = Languages
    Left = 576
    Top = 271
  end
  object Nationalities: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 752
    Top = 216
  end
  object DsNationalities: TDataSource
    DataSet = Nationalities
    Left = 752
    Top = 271
  end
  object Sex: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 576
    Top = 328
  end
  object DsSex: TDataSource
    DataSet = Sex
    Left = 576
    Top = 383
  end
  object LinkedLanguageContactsAdd: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TContactsAddFrm.Layout'
      'TabSheetGeneral.Layout'
      'EditAppelation.FieldLabelSeparator'
      'EditName.FieldLabelSeparator'
      'EditFirstName.FieldLabelSeparator'
      'EditAddress.FieldLabelSeparator'
      'EditZipCode.FieldLabelSeparator'
      'EditCity.FieldLabelSeparator'
      'ComboCountryCode.FieldLabelSeparator'
      'EditPhone.FieldLabelSeparator'
      'Edit_mobile.FieldLabelSeparator'
      'Edit_email.FieldLabelSeparator'
      'Checkbox_do_not_call.FieldLabelSeparator'
      'Checkbox_do_not_call.ValueChecked'
      'Checkbox_do_not_call.ValueUnchecked'
      'Edit_birthdate.DateFormat'
      'Edit_birthdate.FieldLabelSeparator'
      'Edit_Title.FieldLabelSeparator'
      'Edit_birthdate.TimeFormat'
      'Edit_department.FieldLabelSeparator'
      'Edit_Assigned_User_Id.FieldLabelSeparator'
      'Edit_language.FieldLabelSeparator'
      'Edit_lead_source.FieldLabelSeparator'
      'TabSheetAdditional.Layout'
      'PanelPhoto.Layout'
      'Edit_birth_place.FieldLabelSeparator'
      'Edit_birth_country.FieldLabelSeparator'
      'Edit_nationality.FieldLabelSeparator'
      'Edit_nis_number.FieldLabelSeparator'
      'Edit_id_cardnumber.FieldLabelSeparator'
      'Edit_MaritalStatus.FieldLabelSeparator'
      'Edit_Account.FieldLabelSeparator'
      'Edit_sex.FieldLabelSeparator'
      'Edit_MarriageSystem.FieldLabelSeparator'
      'EditNumberOfDependentChildren.FieldLabelSeparator'
      'EditIdCardValidFrom.DateFormat'
      'EditIdCardValidFrom.FieldLabelSeparator'
      'EditIdCardValidFrom.TimeFormat'
      'EditIdCardValidUntil.DateFormat'
      'EditIdCardValidUntil.FieldLabelSeparator'
      'EditIdCardValidUntil.TimeFormat'
      'Contacts_Edit.KeyGenerator'
      'TabSheetSocialMedia.Layout'
      'TabSheetCreditManagement.Layout'
      'Edit_Facebook.FieldLabelSeparator'
      'Edit_Twitter.FieldLabelSeparator'
      'Edit_instagram.FieldLabelSeparator'
      'Edit_Bic_Credit.FieldLabelSeparator'
      'Memo_Remarks.FieldLabelSeparator'
      'Edit_GooglePlus.FieldLabelSeparator'
      'EditInServiceSince.TimeFormat'
      'EditInServiceSince.FieldLabelSeparator'
      'Edit_Iban_Credit.FieldLabelSeparator'
      'Edit_Future_Street.FieldLabelSeparator'
      'GroupBoxFutureAddress.Layout'
      'ImageLastModification.Url'
      'Edit_Future_Postal_code.FieldLabelSeparator'
      'Edit_Future_City.FieldLabelSeparator'
      'Edit_Future_State.FieldLabelSeparator'
      'Edit_Future_Country.FieldLabelSeparator'
      'GroupboxAccountCredit.Layout'
      'Edit_BankAccount_Credit.FieldLabelSeparator'
      'EditDateOfMarriage.DateFormat'
      'EditDateOfMarriage.FieldLabelSeparator'
      'EditDateOfMarriage.TimeFormat'
      'EditNameEmployer.FieldLabelSeparator'
      'Edit_Profession.FieldLabelSeparator'
      'EditTypeOfContract.FieldLabelSeparator'
      'EditInServiceSince.DateFormat'
      'Appelations.SQLDelete'
      'Appelations.SQLInsert'
      'Appelations.SQLLock'
      'Appelations.SQLRecCount'
      'Appelations.SQLRefresh'
      'Appelations.SQLUpdate'
      'Countries.SQLDelete'
      'Countries.SQLInsert'
      'Countries.SQLLock'
      'Countries.SQLRecCount'
      'Countries.SQLRefresh'
      'Countries.SQLUpdate'
      'Users.SQLDelete'
      'Users.SQLInsert'
      'Users.SQLLock'
      'Users.SQLRecCount'
      'Users.SQLRefresh'
      'Users.SQLUpdate'
      'Titles.SQLDelete'
      'Titles.SQLInsert'
      'Titles.SQLLock'
      'Titles.SQLRecCount'
      'Titles.SQLRefresh'
      'Titles.SQLUpdate'
      'Departments.SQLDelete'
      'Departments.SQLInsert'
      'Departments.SQLLock'
      'Departments.SQLRecCount'
      'Departments.SQLRefresh'
      'Departments.SQLUpdate'
      'Languages.SQLDelete'
      'Languages.SQLInsert'
      'Languages.SQLLock'
      'Languages.SQLRecCount'
      'Languages.SQLRefresh'
      'Languages.SQLUpdate'
      'Nationalities.SQLDelete'
      'Nationalities.SQLInsert'
      'Nationalities.SQLLock'
      'Nationalities.SQLRecCount'
      'Nationalities.SQLRefresh'
      'Nationalities.SQLUpdate'
      'Sex.SQLDelete'
      'Sex.SQLInsert'
      'Sex.SQLLock'
      'Sex.SQLRecCount'
      'Sex.SQLRefresh'
      'Sex.SQLUpdate'
      'CrmAccounts.SQLDelete'
      'CrmAccounts.SQLInsert'
      'CrmAccounts.SQLLock'
      'CrmAccounts.SQLRecCount'
      'CrmAccounts.SQLRefresh'
      'CrmAccounts.SQLUpdate'
      'ContractTypes.SQLUpdate'
      'ContractTypes.SQLRefresh'
      'ContractTypes.SQLRecCount'
      'ContractTypes.SQLLock'
      'ContractTypes.SQLInsert'
      'ContractTypes.SQLDelete'
      'Professions.SQLUpdate'
      'Professions.SQLRefresh'
      'MaritalStatus.SQLDelete'
      'MaritalStatus.SQLInsert'
      'MaritalStatus.SQLLock'
      'MaritalStatus.SQLRecCount'
      'MaritalStatus.SQLRefresh'
      'MaritalStatus.SQLUpdate'
      'MarriageSystem.SQLDelete'
      'MarriageSystem.SQLInsert'
      'MarriageSystem.SQLLock'
      'MarriageSystem.SQLRecCount'
      'MarriageSystem.SQLRefresh'
      'MarriageSystem.SQLUpdate'
      'Contacts_Edit.SQLDelete'
      'Contacts_Edit.SQLInsert'
      'Contacts_Edit.SQLLock'
      'Contacts_Edit.SQLRecCount'
      'Contacts_Edit.SQLRefresh'
      'Contacts_Edit.SQLUpdate'
      'UpdTr_Contacts_Edit.Params'
      'Professions.SQLDelete'
      'Professions.SQLInsert'
      'Professions.SQLLock'
      'Professions.SQLRecCount'
      'EditNumberOfDependentPersons.FieldLabelSeparator'
      'ContactTypes.SQLDelete'
      'ContactTypes.SQLInsert'
      'ContactTypes.SQLLock'
      'ContactTypes.SQLRecCount'
      'ContactTypes.SQLRefresh'
      'ContactTypes.SQLUpdate'
      'Edit_Contact_Type.FieldLabelSeparator'
      'EditInternalNumber.FieldLabelSeparator'
      'Edit_contact_role.FieldLabelSeparator'
      'EditDateFirstContact.DateFormat'
      'EditDateFirstContact.FieldLabelSeparator'
      'EditDateFirstContact.TimeFormat'
      'EditBankAccount.FieldLabelSeparator'
      'EditPartner.FieldLabelSeparator'
      'Edit_phone_work.FieldLabelSeparator'
      'EditLeadSourceList.FieldLabelSeparator'
      'ComboLanguageDoc.FieldLabelSeparator'
      'Edit_LinkedIn.FieldLabelSeparator'
      'UniDBCheckBox1.FieldLabelSeparator'
      'UniDBCheckBox1.ValueChecked'
      'UniDBCheckBox1.ValueUnchecked'
      'TabSheetMifidManagement.Layout'
      'CheckBoxContactCardAvailable.FieldLabelSeparator'
      'CheckBoxContactCardAvailable.ValueChecked'
      'CheckBoxContactCardAvailable.ValueUnchecked')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageContactsAddChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 120
    Top = 272
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A0054006100620053006800650065007400470065006E006500
      720061006C00010041006C00670065006D0065006E006500200069006E006600
      6F0072006D0061007400690065000100520065006E0073006500690067006E00
      65006D0065006E007400730020006700E9006E00E90072006100750078000100
      470065006E006500720061006C00200069006E0066006F0072006D0061007400
      69006F006E0001000D000A00420074006E00430061006E00630065006C000100
      41006E006E0075006C006500720065006E00010041006E006E0075006C006500
      72000100430061006E00630065006C0001000D000A0054006100620053006800
      6500650074004100640064006900740069006F006E0061006C00010042006900
      6A006B006F006D0065006E0064006500200069006E0066006F0072006D006100
      7400690065000100520065006E0073006500690067006E0065006D0065006E00
      74007300200063006F006D0070006C00E9006D0065006E007400610069007200
      6500730001004100640064006900740069006F006E0061006C00200069006E00
      66006F0072006D006100740069006F006E0001000D000A004C0062006C005000
      68006F0074006F00010046006F0074006F003A000100500068006F0074006F00
      3A000100500068006F0074006F003A0001000D000A00420074006E0053006100
      7600650001004F00700073006C00610061006E00010045006E00720065006700
      6900730074007200650072000100530061007600650001000D000A0054006100
      62005300680065006500740053006F006300690061006C004D00650064006900
      6100010053006F006300690061006C0020006D00650064006900610001004D00
      E9006400690061007300200073006F0063006900610075007800010053006F00
      6300690061006C0020006D00650064006900610001000D000A00540061006200
      530068006500650074004300720065006400690074004D0061006E0061006700
      65006D0065006E00740001004B00720065006400690065007400620065006800
      6500650072000100470065007300740069006F006E0020006400650073002000
      63007200E9006400690074007300010043007200650064006900740020006D00
      61006E006100670065006D0065006E00740001000D000A00470072006F007500
      700042006F007800460075007400750072006500410064006400720065007300
      7300010054006F0065006B006F006D0073007400690067002000610064007200
      6500730001004100640072006500730073006500200066007500740075007200
      6500010046007500740075007200650020006100640064007200650073007300
      01000D000A00470072006F007500700062006F0078004100630063006F007500
      6E0074004300720065006400690074000100520065006B0065006E0069006E00
      670020006B00720065006400690065007400010043006F006D00700074006500
      200063007200E90064006900740001004100630063006F0075006E0074002000
      63007200650064006900740001000D000A005400610062005300680065006500
      74004D0069006600690064004D0061006E006100670065006D0065006E007400
      01004D00690066006900640001004D00690066006900640001004D0069006600
      6900640001000D000A0073007400480069006E00740073005F0055006E006900
      63006F00640065000D000A0049006D006100670065004C006100730074004D00
      6F00640069006600690063006100740069006F006E0001004C00610061007400
      7300740065002000770069006A007A006900670069006E006700010044006500
      72006E0069006500720020006300680061006E00670065006D0065006E007400
      01004C0061007300740020006D006F0064006900660069006300610074006900
      6F006E0001000D000A007300740044006900730070006C00610079004C006100
      620065006C0073005F0055006E00690063006F00640065000D000A0073007400
      46006F006E00740073005F0055006E00690063006F00640065000D000A007300
      74004D0075006C00740069004C0069006E00650073005F0055006E0069006300
      6F00640065000D000A0043006F006D0062006F004C0061006E00670075006100
      6700650044006F0063002E004900740065006D00730001004E00650064006500
      72006C0061006E00640073002C004600720061006E0073002C0045006E006700
      65006C00730001004E00E900650072006C0061006E0064006100690073002C00
      4600720061006E00E7006100690073002C0041006E0067006C00610069007300
      0100440075007400630068002C004600720065006E00630068002C0045006E00
      67006C0069007300680001000D000A007300740053007400720069006E006700
      73005F0055006E00690063006F00640065000D000A0073007400720073007400
      72005F006E0061006D0065005F00690073005F00720065007100750069007200
      6500640001004400650020006E00610061006D00200069007300200065006500
      6E00200076006500720070006C0069006300680074006500200069006E006700
      6100760065002E0001004C00650020006E006F006D0020006500730074002000
      75006E006500200065006E0074007200E900650020006F0062006C0069006700
      610074006F006900720065002E00010054006800650020006E0061006D006500
      2000690073002000610020006D0061006E006400610074006F00720079002000
      65006E007400720079002E0001000D000A007300740072007300740072005F00
      65006D00610069006C005F00720065007100750069007200650064005F007000
      6F007200740061006C00010045006D00610069006C0020006100640072006500
      73002000690073002000650065006E0020007600650072006500690073007400
      6500200069006E006700610076006500200069006E006400690065006E002000
      640065002000720065006C006100740069006500200074006F00650067006100
      6E006700200068006500650066007400200074006F0074002000680065007400
      200070006F0072007400610061006C002E0001004C0027006100640072006500
      7300730065002000E9006C0065006300740072006F006E006900710075006500
      2000650073007400200075006E006500200065006E0074007200E90065002000
      6F0062006C0069006700610074006F0069007200650020007300690020006C00
      61002000720065006C006100740069006F006E00200061002000610063006300
      E8007300200061007500200070006F0072007400610069006C002E0001004500
      6D00610069006C00200061006400640072006500730073002000690073002000
      6100200072006500710075006900720065006400200065006E00740072007900
      20006900660020007400680065002000720065006C006100740069006F006E00
      20006800610073002000610063006300650073007300200074006F0020007400
      68006500200070006F007200740061006C002E0001000D000A00730074007200
      7300740072005F00700061007300730077006F00720064005F00720065007100
      750069007200650064005F0070006F007200740061006C000100570061006300
      6800740077006F006F00720064002000690073002000650065006E0020007600
      6500720065006900730074006500200069006E00670061007600650020006900
      6E006400690065006E002000640065002000720065006C006100740069006500
      200074006F006500670061006E00670020006800650065006600740020007400
      6F0074002000680065007400200070006F0072007400610061006C002E000100
      4C00650020006D006F0074002000640065002000700061007300730065002000
      650073007400200075006E006500200065006E0074007200E900650020006F00
      62006C0069006700610074006F0069007200650020007300690020006C006100
      2000720065006C006100740069006F006E00200061002000610063006300E800
      7300200061007500200070006F0072007400610069006C002E00010050006100
      7300730077006F00720064002000690073002000610020007200650071007500
      6900720065006400200065006E00740072007900200069006600200074006800
      65002000720065006C006100740069006F006E00200068006100730020006100
      63006300650073007300200074006F002000740068006500200070006F007200
      740061006C002E0001000D000A007300740072007300740072005F0065006D00
      610069006C005F0075006E006900710075006500010045007200200062006500
      730074006100610074002000720065006500640073002000650065006E002000
      720065006C00610074006900650020006D006500740020006400690074002000
      65006D00610069006C002000610064007200650073002C00200065006D006100
      69006C0020006100640072006500730020006D006F0065007400200075006E00
      690065006B0020007A0069006A006E002E00010049006C002000650078006900
      73007400650020006400E9006A00E000200075006E0065002000720065006C00
      6100740069006F006E0020006100760065006300200063006500740074006500
      20006100640072006500730073006500200065002D006D00610069006C002C00
      2000630065006C006C0065002D0063006900200064006F00690074002000EA00
      740072006500200075006E0069007100750065002E0001005400680065007200
      6500200069007300200061006C00720065006100640079002000610020007200
      65006C006100740069006F006E00730068006900700020007700690074006800
      20007400680069007300200065006D00610069006C0020006100640064007200
      6500730073002C00200065006D00610069006C00200061006400640072006500
      7300730020006D00750073007400200062006500200075006E00690071007500
      65002E0001000D000A007300740072007300740072005F006500720072006F00
      72005F0073006100760065005F006400610074006100010046006F0075007400
      2000620069006A00200068006500740020006F00700073006C00610061006E00
      2000760061006E0020006400650020006700650067006500760065006E007300
      3A002000010045007200720065007500720020006C006F007200730020006400
      650020006C00270065006E00720065006700690073007400720065006D006500
      6E0074002000640065007300200064006F006E006E00E900650073003A002000
      01004500720072006F007200200073006100760069006E006700200074006800
      6500200064006100740061003A00200001000D000A0073007400720073007400
      72005F006100640064005F0063006F006E007400610063007400010054006F00
      650076006F006500670065006E00200063006F006E0074006100630074000100
      41006A006F007500740065007200200075006E00200063006F006E0074006100
      630074000100410064006400200063006F006E00740061006300740001000D00
      0A007300740072007300740072005F006D006F0064006900660079005F006300
      6F006E0074006100630074000100570069006A007A006900670065006E002000
      63006F006E00740061006300740001004300680061006E006700650072002000
      75006E00200063006F006E00740061006300740001004D006F00640069006600
      7900200063006F006E00740061006300740001000D000A007300740072004D00
      730067004500720072006F0072004100640064004900740065006D0001004600
      6F00750074002000620069006A002000680065007400200074006F0065007600
      6F006500670065006E002100010045007200720065007500720020006C006F00
      7200730020006400650020006C00270061006A006F0075007400210001004500
      720072006F007200200061006400640069006E006700210001000D000A007300
      740072004D00730067005200650063006F00720064004C006F0063006B006500
      6400010049006E0020006700650062007200750069006B00200064006F006F00
      72002000650065006E00200061006E0064006500720065002000670065006200
      7200750069006B00650072002100010045006E00200063006F00750072007300
      2000640027007500740069006C00690073006100740069006F006E0020007000
      61007200200075006E0020006100750074007200650020007500740069006C00
      69007300610074006500750072002100010049006E0020007500730065002000
      62007900200061006E006F007400680065007200200075007300650072002100
      01000D000A007300740072004D00730067004E006F0041006400640072006500
      7300730001004700650065006E00200061006400720065007300200061006100
      6E00770065007A00690067000100500061007300200064002700610064007200
      6500730073006500200070007200E900730065006E007400650001004E006F00
      200061006400640072006500730073002000700072006500730065006E007400
      01000D000A007300740072004D00730067004E006F0045006900640046006F00
      75006E00640001004700650065006E00200045002D0049006400200067006500
      67006500760065006E00730020006700650076006F006E00640065006E000100
      41007500630075006E006500200064006F006E006E00E9006500200064002700
      6900640065006E00740069007400E9002000E9006C0065006300740072006F00
      6E0069007100750065002000740072006F0075007600E900650001004E006F00
      200045002D004900640020006400610074006100200066006F0075006E006400
      01000D000A007300740072004D007300670045006900640049006E0066006F00
      5200650061006400010045002D0069006400200069006E0066006F0072006D00
      6100740069006500200069006E00670065006C0065007A0065006E0001004C00
      270069006E0066006F0072006D006100740069006F006E00200065002D006900
      6400200061002000E9007400E90020006C007500650001005400680065002000
      65002D0069006400200069006E0066006F0072006D006100740069006F006E00
      200068006100730020006200650065006E002000720065006100640001000D00
      0A007300740072004D0073006700550070006400610074006500450069006400
      4400610074006100010046006F00750074002000620069006A00200068006500
      74002000620069006A007700650072006B0065006E002000760061006E002000
      64006500200045002D0069006400200064006100740061006200610073006500
      2100010045007200720065007500720020006C006F0072007300200064006500
      20006C00610020006D006900730065002000E00020006A006F00750072002000
      6400650020006C00610020006200610073006500200064006500200064006F00
      6E006E00E90065007300200045002D0069006400210001004500720072006F00
      720020007500700064006100740069006E006700200074006800650020004500
      2D0069006400200064006100740061006200610073006500210001000D000A00
      7300740072007300740072005F0065006D00610069006C005F006E006F007400
      5F00760061006C0069006400010045006D00610069006C002000610064007200
      6500730020006900730020006E00690065007400200063006F00720072006500
      630074002E0001004C0027006100640072006500730073006500200065002D00
      6D00610069006C002000650073007400200069006E0063006F00720072006500
      6300740065002E00010045006D00610069006C00200061006400640072006500
      73007300200069007300200069006E0063006F00720072006500630074002E00
      01000D000A007300740072007300740072005F00610073007300690067006E00
      5F006E0075006D00620065007200010054006F0065006B0065006E006E006500
      6E0020006E0075006D006D006500720001004100740074007200690062007500
      65007200200075006E0020006E0075006D00E90072006F000100410073007300
      690067006E0020006E0075006D0062006500720001000D000A00730074007200
      7300740072005F006E0075006D006200650072005F0070007200650073006500
      6E00740001004500720020006900730020007200650065006400730020006500
      65006E0020006E0075006D006D00650072002000610061006E00770065007A00
      690067002C002000770065006E007300740020007500200064006F006F007200
      20007400650020006700610061006E003F00010049006C002000790020006100
      20006400E9006A00E000200075006E0020006E0075006D00E90072006F002000
      64006900730070006F006E00690062006C0065002C00200076006F0075006C00
      65007A002D0076006F0075007300200063006F006E00740069006E0075006500
      72003F00010054006800650072006500200069007300200061006C0072006500
      6100640079002000610020006E0075006D006200650072002000610076006100
      69006C00610062006C0065002C00200064006F00200079006F00750020007700
      61006E007400200074006F00200063006F006E00740069006E00750065003F00
      01000D000A007300740072007300740072005F006C0061006E00670075006100
      670065005F0064006F0063005F00720065007100750069007200650064000100
      4400650020007400610061006C00200076006F006F0072002000640065002000
      64006F00630075006D0065006E00740065006E00200069007300200065006500
      6E00200076006500720065006900730074006500200069006E00670061007600
      65002E0001004C00610020006C0061006E006700750065002000640065007300
      200064006F00630075006D0065006E0074007300200065007300740020007500
      6E006500200065006E0074007200E900650020006F0062006C00690067006100
      74006F006900720065002E00010054006800650020006C0061006E0067007500
      610067006500200066006F0072002000740068006500200064006F0063007500
      6D0065006E007400730020006900730020006100200072006500710075006900
      720065006400200065006E007400720079002E0001000D000A00730074007200
      7300740072005F007700610072006E0069006E0067005F006400750070006C00
      690063006100740065005F0065006D00610069006C005F0063006F006E007400
      61006300740001004F007000670065006C00650074003A002000650072002000
      6200650073007400610061007400200072006500650064007300200065006500
      6E00200063006F006E00740061006300740020006D0065007400200068006500
      74007A0065006C00660064006500200065006D00610069006C00200061006400
      7200650073002100010041007400740065006E00740069006F006E003A002000
      69006C00200065007800690073007400650020006400E9006A00E00020007500
      6E00200063006F006E0074006100630074002000610076006500630020006C00
      610020006D00EA006D0065002000610064007200650073007300650020006500
      6D00610069006C002100010041007400740065006E00740069006F006E003A00
      200074006800650072006500200069007300200061006C007200650061006400
      790020006100200063006F006E00740061006300740020007700690074006800
      20007400680065002000730061006D006500200065006D00610069006C002000
      6100640064007200650073007300210001000D000A0073007400720073007400
      72005F0063006F006E0074006100630074005F006900640065006E0074006900
      630061006C005F006E0061006D00650001004900640065006E00740069006500
      6B00650020006E00610061006D0001004E006F006D0020006900640065006E00
      7400690071007500650001004900640065006E0074006900630061006C002000
      6E0061006D00650001000D000A007300740072007300740072005F0077006100
      72006E0069006E0067005F0063006F006E0074006100630074005F0064007500
      70006C00690063006100740065005F006E0061006D00650001004F0070006700
      65006C00650074002C00200063006F006E00740061006300740020006D006500
      740020006900640065006E007400690065006B00650020006E00610061006D00
      20006700650076006F006E00640065006E002E00200044006F006F0072006700
      610061006E003F00010041007400740065006E00740069006F006E002C002000
      75006E00200063006F006E007400610063007400200061007600650063002000
      6C00650020006D00EA006D00650020006E006F006D00200061002000E9007400
      E9002000740072006F0075007600E9002E00200043006F006E00740069006E00
      7500650072003F00010041007400740065006E00740069006F006E002C002000
      63006F006E007400610063007400200077006900740068002000690064006500
      6E0074006900630061006C0020006E0061006D00650020006800610073002000
      6200650065006E00200066006F0075006E0064002E00200043006F006E007400
      69006E00750065003F0001000D000A00730074004F0074006800650072005300
      7400720069006E00670073005F0055006E00690063006F00640065000D000A00
      450064006900740041007000700065006C006100740069006F006E002E004600
      690065006C0064004C006100620065006C000100410061006E00730070007200
      650065006B0074006900740065006C0020002A00010054006900740072006500
      20002A0001005400690074006C00650020002A0001000D000A00450064006900
      74004E0061006D0065002E004600690065006C0064004C006100620065006C00
      01004E00610061006D0020002A0001004E006F006D0020002A0001004E006100
      6D00650020002A0001000D000A00450064006900740046006900720073007400
      4E0061006D0065002E004600690065006C0064004C006100620065006C000100
      56006F006F0072006E00610061006D00010050007200E9006E006F006D000100
      4600690072007300740020004E0061006D00650001000D000A00450064006900
      740041006400640072006500730073002E004600690065006C0064004C006100
      620065006C000100410064007200650073000100410064007200650073007300
      65000100410064006400720065007300730001000D000A004500640069007400
      5A006900700043006F00640065002E004600690065006C0064004C0061006200
      65006C00010050006F007300740063006F0064006500010043006F0064006500
      200070006F007300740061006C00010050006F007300740061006C0020004300
      6F006400650001000D000A00450064006900740043006900740079002E004600
      690065006C0064004C006100620065006C0001004C006F006300610074006900
      650001004C0069006500750001004C006F0063006100740069006F006E000100
      0D000A0043006F006D0062006F0043006F0075006E0074007200790043006F00
      640065002E004600690065006C0064004C006100620065006C0001004C006100
      6E00640001005000610079007300010043006F0075006E007400720079000100
      0D000A004500640069007400500068006F006E0065002E004600690065006C00
      64004C006100620065006C000100540065006C00650066006F006F006E002000
      7400680075006900730001005400E9006C00E900700068006F006E0065002000
      7000720069007600E9000100500068006F006E006500200068006F006D006500
      01000D000A0045006400690074005F006D006F00620069006C0065002E004600
      690065006C0064004C006100620065006C000100470073006D00010047007300
      6D0001004D006F00620069006C00650001000D000A0045006400690074005F00
      65006D00610069006C002E004600690065006C0064004C006100620065006C00
      010045006D00610069006C00010045006D00610069006C00010045002D006D00
      610069006C0001000D000A0043006800650063006B0062006F0078005F006400
      6F005F006E006F0074005F00630061006C006C002E004600690065006C006400
      4C006100620065006C0001004E0069006500740020006F007000620065006C00
      6C0065006E0001004E0065002000700061007300200061007000700065006C00
      65007200010044006F006E00270074002000630061006C006C0001000D000A00
      45006400690074005F006200690072007400680064006100740065002E004600
      690065006C0064004C006100620065006C0001004700650062006F006F007200
      7400650064006100740075006D00010044006100740065002000640065002000
      6E0061006900730073002E000100440061007400650020006F00660020006200
      690072007400680001000D000A0045006400690074005F005400690074006C00
      65002E004600690065006C0064004C006100620065006C000100460075006E00
      6300740069006500010046006F006E006300740069006F006E00010050006F00
      73006900740069006F006E0001000D000A0045006400690074005F0064006500
      70006100720074006D0065006E0074002E004600690065006C0064004C006100
      620065006C00010041006600640065006C0069006E00670001004400E9007000
      61007200740065006D0065006E00740001004400650070006100720074006D00
      65006E00740001000D000A0045006400690074005F0041007300730069006700
      6E00650064005F0055007300650072005F00490064002E004600690065006C00
      64004C006100620065006C00010045006900670065006E006100610072000100
      500072006F00700072006900E9007400610069007200650001004F0077006E00
      6500720001000D000A0045006400690074005F006C0061006E00670075006100
      670065002E004600690065006C0064004C006100620065006C00010054006100
      61006C0001004C0061006E0067007500650001004C0061006E00670075006100
      6700650001000D000A0045006400690074005F006C006500610064005F007300
      6F0075007200630065002E004600690065006C0064004C006100620065006C00
      0100420072006F006E00200028007600720069006A002900010053006F007500
      720063006500200028006C0069006200720065002900010053006F0075007200
      63006500200028006600720065006500290001000D000A004500640069007400
      5F00620069007200740068005F0070006C006100630065002E00460069006500
      6C0064004C006100620065006C0001004700650062006F006F00720074006500
      70006C00610061007400730001004C0069006500750020006400650020006E00
      61006900730073002E0001004200690072007400680070006C00610063006500
      01000D000A0045006400690074005F00620069007200740068005F0063006F00
      75006E007400720079002E004600690065006C0064004C006100620065006C00
      01004700650062006F006F007200740065006C0061006E006400010050006100
      7900730020006400650020006E0061006900730073002E00010043006F007500
      6E0074007200790020006F00660020006200690072007400680001000D000A00
      45006400690074005F006E006100740069006F006E0061006C00690074007900
      2E004600690065006C0064004C006100620065006C0001004E00610074006900
      6F006E0061006C006900740065006900740001004E006100740069006F006E00
      61006C0069007400E90001004E006100740069006F006E0061006C0069007400
      790001000D000A0045006400690074005F006E00690073005F006E0075006D00
      6200650072002E004600690065006C0064004C006100620065006C0001005200
      69006A006B007300720065006700690073007400650072006E0072002E000100
      4E00B00020007200650067006900730074007200650020006E00610074002E00
      01004E00610074002E0020005200650067006900730074006500720020006E00
      6F002E0001000D000A0045006400690074005F00690064005F00630061007200
      64006E0075006D006200650072002E004600690065006C0064004C0061006200
      65006C0001004900640065006E0074006900740065006900740073006B006100
      6100720074006E0072002E0001004E002000B000200063006100720074006500
      2000690064002E000100490044002000630061007200640020006E006F002E00
      01000D000A0045006400690074005F004D00610072006900740061006C005300
      740061007400750073002E004600690065006C0064004C006100620065006C00
      01004200750072006700650072006C0069006A006B0065002000730074006100
      610074000100C90074006100740020006D0061007400720069006D006F006E00
      690061006C0001004D00610072006900740061006C0020007300740061007400
      7500730001000D000A0045006400690074005F004100630063006F0075006E00
      74002E004600690065006C0064004C006100620065006C000100420065006400
      720069006A006600010053006F0063006900E9007400E900010043006F006D00
      700061006E00790001000D000A0045006400690074005F007300650078002E00
      4600690065006C0064004C006100620065006C0001004700650073006C006100
      63006800740001005300650078006500010053006500780001000D000A004500
      6400690074005F004D0061007200720069006100670065005300790073007400
      65006D002E004600690065006C0064004C006100620065006C00010048007500
      770065006C0069006A006B0073007300740065006C00730065006C0001005300
      7900730074002E0020006400650020006D006100720069006100670065000100
      4D0061007200720069006100670065002000730079007300740065006D000100
      0D000A0045006400690074004E0075006D006200650072004F00660044006500
      700065006E00640065006E0074004300680069006C006400720065006E002E00
      4600690065006C0064004C006100620065006C0001004B0069006E0064006500
      720065006E002000740065006E0020006C006100730074006500010045006E00
      660061006E00740073002000E000200063006800610072006700650001004400
      650070002E0020006300680069006C006400720065006E0001000D000A004500
      640069007400490064004300610072006400560061006C006900640046007200
      6F006D002E004600690065006C0064004C006100620065006C00010047006500
      6C006400690067002000760061006E000100560061006C00610062006C006500
      2000E00020007000610072007400690072002000640065000100560061006C00
      690064002000660072006F006D0001000D000A00450064006900740049006400
      4300610072006400560061006C006900640055006E00740069006C002E004600
      690065006C0064004C006100620065006C000100470065006C00640069006700
      200074006F0074000100560061006C00610062006C00650020006A0075007300
      710075002700610075000100560061006C0069006400200075006E0074006900
      6C0001000D000A0045006400690074005F00460061006300650062006F006F00
      6B002E004600690065006C0064004C006100620065006C000100460061006300
      650062006F006F006B000100460061006300650062006F006F006B0001004600
      61006300650062006F006F006B0001000D000A0045006400690074005F005400
      7700690074007400650072002E004600690065006C0064004C00610062006500
      6C00010054007700690074007400650072000100540077006900740074006500
      72000100540077006900740074006500720001000D000A004500640069007400
      5F0069006E007300740061006700720061006D002E004600690065006C006400
      4C006100620065006C00010049006E007300740061006700720061006D000100
      49006E007300740061006700720061006D00010049006E007300740061006700
      720061006D0001000D000A0045006400690074005F0047006F006F0067006C00
      650050006C00750073002E004600690065006C0064004C006100620065006C00
      010047006F006F0067006C00650020002B00010047006F006F0067006C006500
      20002B00010047006F006F0067006C00650020002B0001000D000A004D006500
      6D006F005F00520065006D00610072006B0073002E004600690065006C006400
      4C006100620065006C0001004F0070006D00650072006B0069006E0067006500
      6E000100520065006D006100720071007500650073000100520065006D006100
      72006B00730001000D000A0045006400690074005F0046007500740075007200
      65005F005300740072006500650074002E004600690065006C0064004C006100
      620065006C000100530074007200610061007400010052007500650001005300
      7400720065006500740001000D000A0045006400690074005F00460075007400
      7500720065005F0050006F007300740061006C005F0063006F00640065002E00
      4600690065006C0064004C006100620065006C00010050006F00730074006300
      6F0064006500010043006F0064006500200070006F007300740061006C000100
      50006F007300740061006C00200043006F006400650001000D000A0045006400
      690074005F004600750074007500720065005F0043006900740079002E004600
      690065006C0064004C006100620065006C0001004C006F006300610074006900
      650001004C006F0063006100740069006F006E0001004C006F00630061007400
      69006F006E0001000D000A0045006400690074005F0046007500740075007200
      65005F00530074006100740065002E004600690065006C0064004C0061006200
      65006C000100500072006F00760069006E006300690065000100500072006F00
      760069006E00630065000100500072006F00760069006E006300650001000D00
      0A0045006400690074005F004600750074007500720065005F0043006F007500
      6E007400720079002E004600690065006C0064004C006100620065006C000100
      4C0061006E00640001005000610079007300010043006F0075006E0074007200
      790001000D000A0045006400690074005F00420061006E006B00410063006300
      6F0075006E0074005F004300720065006400690074002E004600690065006C00
      64004C006100620065006C000100420061006E006B00720065006B0065006E00
      69006E006700010043006F006D007000740065002000620061006E0063006100
      6900720065000100420061006E006B0020006100630063006F0075006E007400
      01000D000A0045006400690074005F004900620061006E005F00430072006500
      6400690074002E004600690065006C0064004C006100620065006C0001004900
      620061006E0001004900620061006E0001004900620061006E0001000D000A00
      45006400690074005F004200690063005F004300720065006400690074002E00
      4600690065006C0064004C006100620065006C00010042006900630001004200
      69006300010042006900630001000D000A004500640069007400440061007400
      65004F0066004D0061007200720069006100670065002E004600690065006C00
      64004C006100620065006C00010048007500770065006C0069006A006B007300
      64006100740075006D000100440061007400650020006400650020006D006100
      720069006100670065000100570065006400640069006E006700200044006100
      7400650001000D000A0045006400690074004E0061006D00650045006D007000
      6C006F007900650072002E004600690065006C0064004C006100620065006C00
      01004E00610061006D0020007700650072006B00670065007600650072000100
      4E006F006D00200065006D0070006C006F00790065007500720001004E006100
      6D006500200065006D0070006C006F0079006500720001000D000A0045006400
      690074005F00500072006F00660065007300730069006F006E002E0046006900
      65006C0064004C006100620065006C0001004200650072006F00650070000100
      45006D0070006C006F0069000100500072006F00660065007300730069006F00
      6E0001000D000A00450064006900740054007900700065004F00660043006F00
      6E00740072006100630074002E004600690065006C0064004C00610062006500
      6C0001005400790070006500200063006F006E00740072006100630074000100
      5400790070006500200064006500200063006F006E0074007200610074000100
      43006F006E00740072006100630074002000740079007000650001000D000A00
      450064006900740049006E005300650072007600690063006500530069006E00
      630065002E004600690065006C0064004C006100620065006C00010049006E00
      20006400690065006E00730074002000730069006E0064007300010045006E00
      2000730065007200760069006300650020006400650070007500690073000100
      49006E00200073006500720076006900630065002000730069006E0063006500
      01000D000A0045006400690074004E0075006D006200650072004F0066004400
      6500700065006E00640065006E00740050006500720073006F006E0073002E00
      4600690065006C0064004C006100620065006C00010050006500720073002E00
      2000740065006E0020006C006100730074006500010050006500720073006F00
      6E006E00650073002000E0002000630068006100720067006500010044006500
      700065006E00640065006E007400730001000D000A0045006400690074005F00
      43006F006E0074006100630074005F0054007900700065002E00460069006500
      6C0064004C006100620065006C00010054007900700065000100540079007000
      65000100540079007000650001000D000A00450064006900740049006E007400
      650072006E0061006C004E0075006D006200650072002E004600690065006C00
      64004C006100620065006C00010049006E007400650072006E0020006E007200
      2E0001004E00B000200069006E007400650072006E006500010049006E007400
      650072006E0061006C0020006E006F002E0001000D000A004500640069007400
      5F0063006F006E0074006100630074005F0072006F006C0065002E0046006900
      65006C0064004C006100620065006C00010052006F006C0001005200F4006C00
      6500010052006F006C00650001000D000A004500640069007400440061007400
      65004600690072007300740043006F006E0074006100630074002E0046006900
      65006C0064004C006100620065006C0001004500650072007300740065002000
      63006F006E00740061006300740001005000720065006D006900650072002000
      63006F006E007400610063007400010046006900720073007400200063006F00
      6E00740061006300740001000D000A004500640069007400420061006E006B00
      4100630063006F0075006E0074002E004600690065006C0064004C0061006200
      65006C000100420061006E006B00720065006B0065006E0069006E0067000100
      43006F006D007000740065002000620061006E00630061006900720065000100
      420061006E006B0020006100630063006F0075006E00740001000D000A004500
      64006900740050006100720074006E00650072002E004600690065006C006400
      4C006100620065006C00010050006100720074006E0065007200010050006100
      7200740065006E006100690072006500010050006100720074006E0065007200
      01000D000A0045006400690074005F00700068006F006E0065005F0077006F00
      72006B002E004600690065006C0064004C006100620065006C00010054006500
      6C00650066006F006F006E0020007700650072006B0001005400E9006C002E00
      20007400720061007600610069006C000100500068006F006E00650020007700
      6F0072006B0001000D000A0045006400690074004C0065006100640053006F00
      75007200630065004C006900730074002E004600690065006C0064004C006100
      620065006C000100420072006F006E00010053006F0075007200630065000100
      53006F00750072006300650001000D000A0043006F006D0062006F004C006100
      6E006700750061006700650044006F0063002E004600690065006C0064004C00
      6100620065006C0001005400610061006C00200064006F0063002E0020002A00
      01004C0061006E00670075006500200064006F0063002E0001004C0061006E00
      67007500610067006500200064006F0063002E0001000D000A00450064006900
      74005F004C0069006E006B006500640049006E002E004600690065006C006400
      4C006100620065006C0001004C0069006E006B006500640049006E0001004C00
      69006E006B006500640049006E0001004C0069006E006B006500640049006E00
      01000D000A0055006E0069004400420043006800650063006B0042006F007800
      31002E004600690065006C0064004C006100620065006C00010045006D006100
      69006C0020006E006900650074002000670065006C0064006900670001004500
      6D00610069006C0020006E006F006E002000760061006C006900640065000100
      45006D00610069006C0020006E006F0074002000760061006C00690064000100
      0D000A0043006800650063006B0042006F00780043006F006E00740061006300
      7400430061007200640041007600610069006C00610062006C0065002E004600
      690065006C0064004C006100620065006C0001004B006C0061006E0074006500
      6E00660069006300680065002000610061006E00770065007A00690067000100
      46006900630068006500200063006C00690065006E007400200070007200E900
      730065006E0074006500010043007500730074006F006D006500720020006300
      6100720064002000700072006500730065006E00740001000D000A0073007400
      43006F006C006C0065006300740069006F006E0073005F0055006E0069006300
      6F00640065000D000A00450064006900740049006E007400650072006E006100
      6C004E0075006D006200650072002E0054007200690067006700650072007300
      5B0030005D002E00480069006E007400010054006F0065006B0065006E006E00
      65006E0020006E0075006D006D00650072000100410074007400720069006200
      750065007200200075006E0020006E0075006D00E90072006F00010041007300
      7300690067006E0020006E0075006D0062006500720001000D000A0045006400
      690074005F004100630063006F0075006E0074002E0054007200690067006700
      6500720073005B0031005D002E00480069006E007400010054006F0065007600
      6F006500670065006E00010041006A006F007500740065007200010041006400
      640001000D000A0045006400690074005F004100630063006F0075006E007400
      2E00540072006900670067006500720073005B0032005D002E00480069006E00
      74000100570069006A007A006900670065006E0001004D006F00640069006600
      69006500720001004D006F00640069006600790001000D000A00730074004300
      68006100720053006500740073005F0055006E00690063006F00640065000D00
      0A00}
  end
  object CrmAccounts: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select id, name from crm_accounts')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 480
    Top = 56
  end
  object DsCrmAccounts: TDataSource
    DataSet = CrmAccounts
    Left = 480
    Top = 103
  end
  object MaritalStatus: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 488
    Top = 328
  end
  object DsMaritalStatus: TDataSource
    DataSet = MaritalStatus
    Left = 488
    Top = 383
  end
  object MarriageSystem: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 640
    Top = 56
  end
  object DsMarriageSystem: TDataSource
    DataSet = MarriageSystem
    Left = 640
    Top = 111
  end
  object Contacts_Edit: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_CRM_CONTACTS_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTr_Contacts_Edit
    SQL.Strings = (
      'Select * from crm_contacts')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    BeforePost = Contacts_EditBeforePost
    Left = 120
    Top = 120
  end
  object Ds_Contacts_Edit: TDataSource
    DataSet = Contacts_Edit
    Left = 120
    Top = 168
  end
  object UpdTr_Contacts_Edit: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 120
    Top = 216
  end
  object Professions: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 672
    Top = 328
  end
  object DsProfessions: TDataSource
    DataSet = Professions
    Left = 672
    Top = 376
  end
  object ContractTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 760
    Top = 320
  end
  object DsContractTypes: TDataSource
    DataSet = ContractTypes
    Left = 760
    Top = 368
  end
  object ContactTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 760
    Top = 416
  end
  object DsContactTypes: TDataSource
    DataSet = ContactTypes
    Left = 760
    Top = 463
  end
  object Roles: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 856
    Top = 56
  end
  object DsRoles: TDataSource
    DataSet = Roles
    Left = 856
    Top = 199
  end
  object ContactsSearch: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, coalesce(last_name, '#39#39') || '#39' '#39' || coalesce(first_name' +
        ', '#39#39') as FullName'
      'from crm_contacts'
      'order by FullName')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 400
    Top = 56
  end
  object DsContactsSearch: TDataSource
    DataSet = ContactsSearch
    Left = 400
    Top = 104
  end
  object ContactSources: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 304
    Top = 56
  end
  object DsContactSources: TDataSource
    DataSet = ContactSources
    Left = 304
    Top = 104
  end
end
