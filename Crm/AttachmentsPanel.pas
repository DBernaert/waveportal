unit AttachmentsPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid, uniDBNavigator, uniEdit,
  uniGUIBaseClasses, uniPanel, Data.DB, MemDS, DBAccess, IBC, uniFileUpload, siComp, siLngLnk,
  uniImage;

type
  TAttachmentsPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    GridAttachments: TUniDBGrid;
    Attachments: TIBCQuery;
    DsAttachments: TDataSource;
    LinkedLanguageAttachmentsPanel: TsiLangLinked;
    BtnAddAttachment: TUniThemeButton;
    BtnOpenAttachment: TUniThemeButton;
    procedure BtnAddAttachmentClick(Sender: TObject);
    procedure BtnOpenAttachmentClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
  private
    { Private declarations }
    ModuleId: integer;
    procedure CallBackInsertUpdateAttachment(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_attachments(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, IOUtils, Main, ServerModule, AttachmentsAdd, Utils;

{ TAttachmentsPanelFrm }

procedure TAttachmentsPanelFrm.BtnAddAttachmentClick(Sender: TObject);
begin
  if Attachments.MasterSource.DataSet.FieldByName('Id').isNull
  then exit;

  With AttachmentsAddFrm
  do begin
       if initialize_attachment_add(ModuleId, Attachments.MasterSource.DataSet.FieldByName('Id').AsInteger)
       then ShowModal(CallBackInsertUpdateAttachment)
       else Close;
     end;
end;

procedure TAttachmentsPanelFrm.CallBackInsertUpdateAttachment(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Attachments.Active
         then begin
                Attachments.Refresh;
                Attachments.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TAttachmentsPanelFrm.BtnOpenAttachmentClick(Sender: TObject);
var Destination: string;
begin
  if ((Attachments.Active) and (not Attachments.fieldbyname('Id').isNull))
  then begin
         Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
         if Trim(Attachments.FieldByName('Folder').AsString) <> ''
         then Destination := Destination + Attachments.FieldByName('Folder').AsString + '\';
         if Trim(Attachments.FieldByName('SubFolder').AsString) <> ''
         then Destination := Destination + Attachments.FieldByName('SubFolder').AsString + '\';
         Destination := Destination + Attachments.FieldByName('Destination_name').AsString;

         UniSession.SendFile(Destination, Utils.EscapeIllegalChars(Attachments.fieldbyname('Original_Name').asString));
       end;
end;

procedure TAttachmentsPanelFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' ORIGINAL_NAME LIKE ''%' + Utils.RemoveQuotes(EditSearch.Text) + '%'' OR ' +
            ' DESCRIPTION LIKE ''%'   + Utils.RemoveQuotes(EditSearch.Text) + '%''';
  Attachments.Filter := Filter;
end;

procedure TAttachmentsPanelFrm.Start_attachments(IdModule: integer; MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;

  //Set the rights

//  if (UniMainModule.portal_contr_credit_module_active = True)
//  then begin
         if UniMainModule.User_portal_rights >= 3
         then BtnAddAttachment.Visible := True
         else BtnAddAttachment.Visible := False;
//       end
//  else BtnAddAttachment.Visible := False;

  Attachments.Close;
  Attachments.SQL.Clear;
  SQL := 'Select id, original_name, destination_name, description, date_entered, folder, subfolder from files_archive ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) +
         ' and Module_id='  + IntToStr(IdModule) +
         ' and removed=''0'' ' +
         ' and visible_portal=1';
  SQL := SQL + ' order by original_name';
  Attachments.SQL.Add(SQL);
  Attachments.MasterSource := MasterDs;
  Attachments.MasterFields := 'ID';
  Attachments.DetailFields := 'RECORD_ID';
  Attachments.Open;
end;

end.
