unit AccountsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniMemo, uniDBMemo, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox,
  uniGUIBaseClasses, uniEdit, uniDBEdit, uniGroupBox, uniButton, UniThemeButton, uniImage, uniDBImage, uniPanel,
  Data.DB, MemDS, DBAccess, IBC, uniListBox, siComp, siLngLnk,
  uniPageControl, uniLabel, uniDBText, uniBitBtn,
  uniMenuButton, Vcl.Menus, uniMainMenu, uniCheckBox, uniDBCheckBox, REST.Types,
  REST.Client, REST.Authenticator.Basic, Data.Bind.Components,
  Data.Bind.ObjectScope;

var
	str_name_is_required:                    string = 'De naam is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_error_saving_data:                   string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	str_add_organisation:                    string = 'Toevoegen organisatie'; // TSI: Localized (Don't modify!)
	str_modify_organisation:                 string = 'Wijzigen organisatie'; // TSI: Localized (Don't modify!)
	MsgErrorAddItem:                         string = 'Fout bij het toevoegen!'; // TSI: Localized (Don't modify!)
	MsgRecordLocked:                         string = 'In gebruik door een andere gebruiker!'; // TSI: Localized (Don't modify!)
	MsgNoAddressAvailable:                   string = 'Geen adres aanwezig.'; // TSI: Localized (Don't modify!)
	str_invalid_company_number:              string = 'Ongeldig ondernemingsnummer.'; // TSI: Localized (Don't modify!)
	str_error_getting_data_kruispuntbank:    string = 'Fout bij het opvragen van de gegevens bij de kruispuntbank.'; // TSI: Localized (Don't modify!)
	str_company_number_not_valid:            string = 'Het ondernemingsnummer is niet geldig.'; // TSI: Localized (Don't modify!)
	str_company_number_valid:                string = 'Het ondernemingsnummer is geldig.'; // TSI: Localized (Don't modify!)
	str_data_crossroads_bank:                string = 'Gegevens kruispuntbank:'; // TSI: Localized (Don't modify!)
	str_name_crossroads_bank:                string = 'Naam: '; // TSI: Localized (Don't modify!)
	str_address_crossroads_bank:             string = 'Adres: '; // TSI: Localized (Don't modify!)
	str_email_not_valid:                     string = 'Email adres is niet correct.'; // TSI: Localized (Don't modify!)
	str_assign_number:                       string = 'Toekennen nummer'; // TSI: Localized (Don't modify!)
	str_number_present:                      string = 'Er is reeds een nummer aanwezig, wenst u door te gaan?'; // TSI: Localized (Don't modify!)
	str_language_doc_required_account:       string = 'De taal van de documenten is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_identical_name:                      string = 'Identieke naam'; // TSI: Localized (Don't modify!)
	str_warning_duplicate_name:              string = 'Opgelet, bedrijf met identieke naam gevonden. Doorgaan?'; // TSI: Localized (Don't modify!)
	str_company_number:                      string = 'Ondernemingsnummer'; // TSI: Localized (Don't modify!)
	str_no_belgian_company_number_continue:  string = 'Geen geldig Belgisch ondernemingsnummer. Doorgaan?'; // TSI: Localized (Don't modify!)

type
  TAccountsAddFrm = class(TUniForm)
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    Countries: TIBCQuery;
    DsCountries: TDataSource;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    IndustryTypes: TIBCQuery;
    DsIndustryTypes: TDataSource;
    AccountTypes: TIBCQuery;
    DsAccountTypes: TDataSource;
    LinkedLanguageAccountsAdd: TsiLangLinked;
    PageControlAccounts: TUniPageControl;
    TabSheetGeneral: TUniTabSheet;
    Edit_Name: TUniDBEdit;
    Edit_Account_Type: TUniDBLookupComboBox;
    Edit_Phone: TUniDBEdit;
    Edit_Email: TUniDBEdit;
    Edit_Website: TUniDBEdit;
    Edit_Industry_Type: TUniDBLookupComboBox;
    Edit_VatNumber: TUniDBEdit;
    Edit_Assigned_User_Id: TUniDBLookupComboBox;
    Edit_Remarks: TUniDBMemo;
    GroupBoxInvoiceAddress: TUniGroupBox;
    Edit_Billing_Address_Street: TUniDBEdit;
    Edit_Billing_Address_Postal_Code: TUniDBEdit;
    Edit_Billing_Address_City: TUniDBEdit;
    Edit_Billing_Address_Country: TUniDBLookupComboBox;
    GroupBoxShippingAddress: TUniGroupBox;
    Edit_Shipping_Address_Street: TUniDBEdit;
    Edit_Shipping_Address_Postal_Code: TUniDBEdit;
    Edit_Shipping_Address_City: TUniDBEdit;
    Edit_Shipping_Address_Country: TUniDBLookupComboBox;
    TabSheetSocialMedia: TUniTabSheet;
    TabSheetCreditManagement: TUniTabSheet;
    Accounts_Edit: TIBCQuery;
    Ds_Accounts_Edit: TDataSource;
    UpdTransAccountsEdit: TIBCTransaction;
    Edit_Facebook: TUniDBEdit;
    Edit_Twitter: TUniDBEdit;
    Edit_GooglePlus: TUniDBEdit;
    Edit_OneTime_Remarks: TUniDBMemo;
    Edit_instagram: TUniDBEdit;
    ImageLastModification: TUniImage;
    LblLastModification: TUniDBText;
    GroupBoxDemandAddress: TUniGroupBox;
    Edit_Demand_street: TUniDBEdit;
    Edit_Demand_Postal_code: TUniDBEdit;
    Edit_Demand_City: TUniDBEdit;
    Edit_Demand_State: TUniDBEdit;
    Edit_Demand_Country: TUniDBLookupComboBox;
    GroupBoxFutureAddress: TUniGroupBox;
    Edit_Future_Street: TUniDBEdit;
    Edit_Future_Postal_code: TUniDBEdit;
    Edit_Future_City: TUniDBEdit;
    Edit_Future_State: TUniDBEdit;
    Edit_Future_Country: TUniDBLookupComboBox;
    GroupboxAccountCredit: TUniGroupBox;
    Edit_BankAccount_Credit: TUniDBEdit;
    Edit_Iban_Credit: TUniDBEdit;
    Edit_Bic_Credit: TUniDBEdit;
    PopupMenuBtnDemand: TUniPopupMenu;
    BtnDemandTakeOverFutureAddress: TUniMenuItem;
    BtnDemandTakeOverInvoiceAddress: TUniMenuItem;
    BtnDemandTakeSendAddress: TUniMenuItem;
    PopupMenuFuture: TUniPopupMenu;
    BtnFutureTakeOverInvoiceAddress: TUniMenuItem;
    BtnFutureTakeSendAddress: TUniMenuItem;
    BtnFutureTakeDemandAddress: TUniMenuItem;
    TabSheetOther: TUniTabSheet;
    Edit_Annual_Revenue: TUniDBFormattedNumberEdit;
    Edit_Employee_Count: TUniDBNumberEdit;
    CheckboxCustomer: TUniDBCheckBox;
    CheckboxSupplier: TUniDBCheckBox;
    EditInternalNumber: TUniDBEdit;
    Edit_Phone_Office: TUniDBEdit;
    Edit_Fax: TUniDBEdit;
    Client: TRESTClient;
    Request: TRESTRequest;
    Response: TRESTResponse;
    Authenticator: THTTPBasicAuthenticator;
    CheckBoxInvoiceByEmail: TUniDBCheckBox;
    BtnCreateContact: TUniThemeButton;
    EditBankAccount: TUniDBEdit;
    EditBillingAddressContactName: TUniDBEdit;
    EditShippingAddressEmail: TUniDBEdit;
    EditBillingAddressEmail: TUniDBEdit;
    BtnSendTakeOverInvoicing: TUniThemeButton;
    MenuBtnDemand: TUniThemeButton;
    MenuBtnFuture: TUniThemeButton;
    ComboLanguageDoc: TUniComboBox;
    Edit_LinkedIn: TUniDBEdit;
    CheckBox_bankruptcy: TUniDBCheckBox;
    CheckBoxObsolete: TUniDBCheckBox;
    CheckBoxEmailInvalid: TUniDBCheckBox;
    CompanyTypes: TIBCQuery;
    DsCompanyTypes: TDataSource;
    EditCompanyType: TUniDBLookupComboBox;
    procedure UniFormReady(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure LinkedLanguageAccountsAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnDemandTakeOverInvoiceAddressClick(Sender: TObject);
    procedure BtnDemandTakeSendAddressClick(Sender: TObject);
    procedure BtnDemandTakeOverFutureAddressClick(Sender: TObject);
    procedure BtnFutureTakeOverInvoiceAddressClick(Sender: TObject);
    procedure BtnFutureTakeSendAddressClick(Sender: TObject);
    procedure BtnFutureTakeDemandAddressClick(Sender: TObject);
    procedure MenuBtnDemandClick(Sender: TObject);
    procedure MenuBtnFutureClick(Sender: TObject);
    procedure Edit_Billing_Address_Postal_CodeExit(Sender: TObject);
    procedure Edit_Billing_Address_CityExit(Sender: TObject);
    procedure Edit_Shipping_Address_Postal_CodeExit(Sender: TObject);
    procedure Edit_Shipping_Address_CityExit(Sender: TObject);
    procedure Edit_Demand_Postal_codeExit(Sender: TObject);
    procedure Edit_Demand_CityExit(Sender: TObject);
    procedure Edit_Future_Postal_codeExit(Sender: TObject);
    procedure Edit_Future_CityExit(Sender: TObject);
    procedure Edit_VatNumberTriggerEvent(Sender: TUniFormControl;
      AButtonId: Integer);
    procedure BtnCreateContactClick(Sender: TObject);
    procedure BtnSendTakeOverInvoicingClick(Sender: TObject);
    procedure EditInternalNumberTriggerEvent(Sender: TUniFormControl;
      AButtonId: Integer);
    procedure UniFormCreate(Sender: TObject);
    procedure Accounts_EditBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure CallBackSelectCityBilling(Sender: TComponent; AResult: Integer);
    procedure CallBackSelectCityShipping(Sender: TComponent; AResult: Integer);
    procedure CallBackSelectCityDemand(Sender: TComponent; AResult: Integer);
    procedure CallBackSelectCityFuture(Sender: TComponent; AResult: Integer);
    function  Save_account: boolean;
  public
    { Public declarations }
    function Init_account_creation: boolean;
    function Init_account_modification(Id: longint): boolean;
  end;

function AccountsAddFrm: TAccountsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ZipCodesSelect,
  Main, System.JSON.Types, System.JSON.Readers, Utils, ContactsAdd,
  System.StrUtils;

function AccountsAddFrm: TAccountsAddFrm;
begin
  Result := TAccountsAddFrm(UniMainModule.GetFormInstance(TAccountsAddFrm));
end;

{ TAccountsAddFrm }

procedure TAccountsAddFrm.Accounts_EditBeforePost(DataSet: TDataSet);
begin
  Accounts_Edit.FieldByName('Name_sndx').AsString    := Soundex(Accounts_Edit.FieldByname('Name').asString, 15);
end;

procedure TAccountsAddFrm.BtnCancelClick(Sender: TObject);
begin
  Accounts_Edit.Cancel;
  Accounts_Edit.Close;
  Close;
end;

procedure TAccountsAddFrm.BtnCreateContactClick(Sender: TObject);
begin
  if Accounts_Edit.State = dsInsert
  then begin
         if Save_Account = false
         then exit
         else Accounts_Edit.Edit;
       end;

  UniMainModule.Result_dbAction := 0;
  With ContactsAddFrm
  do begin
       if Init_contact_creation(Accounts_Edit.FieldByName('Id').AsInteger)
       then ShowModal
       else Close;
     end;
end;

procedure TAccountsAddFrm.BtnDemandTakeOverFutureAddressClick(
  Sender: TObject);
begin
  Accounts_Edit.FieldByName('Demand_address_street').AsString       := Accounts_Edit.FieldByName('Future_address_street').AsString;
  Accounts_Edit.FieldByName('Demand_address_postal_code').AsString  := Accounts_Edit.FieldByName('Future_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Demand_address_city').AsString         := Accounts_Edit.FieldByName('Future_address_city').AsString;
  Accounts_Edit.FieldByName('Demand_address_state').AsString        := Accounts_Edit.FieldByName('Future_address_state').AsString;
  Accounts_Edit.FieldByName('Demand_address_country').AsString      := Accounts_Edit.FieldByName('Future_address_country').AsString;
end;

procedure TAccountsAddFrm.BtnDemandTakeOverInvoiceAddressClick(
  Sender: TObject);
begin
  Accounts_Edit.FieldByName('Demand_address_street').AsString       := Accounts_Edit.FieldByName('Billing_address_street').AsString;
  Accounts_Edit.FieldByName('Demand_address_postal_code').AsString  := Accounts_Edit.FieldByName('Billing_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Demand_address_city').AsString         := Accounts_Edit.FieldByName('Billing_address_city').AsString;
  Accounts_Edit.FieldByName('Demand_address_state').AsString        := Accounts_Edit.FieldByName('Billing_address_state').AsString;
  Accounts_Edit.FieldByName('Demand_address_country').AsString      := Accounts_Edit.FieldByName('Billing_address_country').AsString;
end;

procedure TAccountsAddFrm.BtnDemandTakeSendAddressClick(Sender: TObject);
begin
  Accounts_Edit.FieldByName('Demand_address_street').AsString       := Accounts_Edit.FieldByName('Shipping_address_street').AsString;
  Accounts_Edit.FieldByName('Demand_address_postal_code').AsString  := Accounts_Edit.FieldByName('Shipping_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Demand_address_city').AsString         := Accounts_Edit.FieldByName('Shipping_address_city').AsString;
  Accounts_Edit.FieldByName('Demand_address_state').AsString        := Accounts_Edit.FieldByName('Shipping_address_state').AsString;
  Accounts_Edit.FieldByName('Demand_address_country').AsString      := Accounts_Edit.FieldByName('Shipping_address_country').AsString;
end;

procedure TAccountsAddFrm.BtnFutureTakeDemandAddressClick(Sender: TObject);
begin
  Accounts_Edit.FieldByName('Future_address_street').AsString       := Accounts_Edit.FieldByName('Demand_address_street').AsString;
  Accounts_Edit.FieldByName('Future_address_postal_code').AsString  := Accounts_Edit.FieldByName('Demand_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Future_address_city').AsString         := Accounts_Edit.FieldByName('Demand_address_city').AsString;
  Accounts_Edit.FieldByName('Future_address_state').AsString        := Accounts_Edit.FieldByName('Demand_address_state').AsString;
  Accounts_Edit.FieldByName('Future_address_country').AsString      := Accounts_Edit.FieldByName('Demand_address_country').AsString;
end;

procedure TAccountsAddFrm.BtnFutureTakeOverInvoiceAddressClick(
  Sender: TObject);
begin
  Accounts_Edit.FieldByName('Future_address_street').AsString       := Accounts_Edit.FieldByName('Billing_address_street').AsString;
  Accounts_Edit.FieldByName('Future_address_postal_code').AsString  := Accounts_Edit.FieldByName('Billing_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Future_address_city').AsString         := Accounts_Edit.FieldByName('Billing_address_city').AsString;
  Accounts_Edit.FieldByName('Future_address_state').AsString        := Accounts_Edit.FieldByName('Billing_address_state').AsString;
  Accounts_Edit.FieldByName('Future_address_country').AsString      := Accounts_Edit.FieldByName('Billing_address_country').AsString;
end;

procedure TAccountsAddFrm.BtnFutureTakeSendAddressClick(Sender: TObject);
begin
  Accounts_Edit.FieldByName('Future_address_street').AsString       := Accounts_Edit.FieldByName('Shipping_address_street').AsString;
  Accounts_Edit.FieldByName('Future_address_postal_code').AsString  := Accounts_Edit.FieldByName('Shipping_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Future_address_city').AsString         := Accounts_Edit.FieldByName('Shipping_address_city').AsString;
  Accounts_Edit.FieldByName('Future_address_state').AsString        := Accounts_Edit.FieldByName('Shipping_address_state').AsString;
  Accounts_Edit.FieldByName('Future_address_country').AsString      := Accounts_Edit.FieldByName('Shipping_address_country').AsString;
end;

procedure TAccountsAddFrm.CallBackSelectCityBilling(Sender: TComponent;
  AResult: Integer);
begin
  if UniMainModule.Result_zipSelection.selected = true
  then begin
         Accounts_Edit.FieldByName('BILLING_ADDRESS_POSTAL_CODE').AsString := UniMainModule.Result_zipSelection.zipcode;
         Accounts_Edit.FieldByName('BILLING_ADDRESS_CITY').AsString        := UniMainModule.Result_zipSelection.city;
       end;
end;

procedure TAccountsAddFrm.CallBackSelectCityDemand(Sender: TComponent;
  AResult: Integer);
begin
  if UniMainModule.Result_zipSelection.selected = true
  then begin
         Accounts_Edit.FieldByName('DEMAND_ADDRESS_POSTAL_CODE').AsString := UniMainModule.Result_zipSelection.zipcode;
         Accounts_Edit.FieldByName('DEMAND_ADDRESS_CITY').AsString        := UniMainModule.Result_zipSelection.city;
       end;
end;

procedure TAccountsAddFrm.CallBackSelectCityFuture(Sender: TComponent;
  AResult: Integer);
begin
  if UniMainModule.Result_zipSelection.selected = true
  then begin
         Accounts_Edit.FieldByName('FUTURE_ADDRESS_POSTAL_CODE').AsString := UniMainModule.Result_zipSelection.zipcode;
         Accounts_Edit.FieldByName('FUTURE_ADDRESS_CITY').AsString        := UniMainModule.Result_zipSelection.city;
       end;
end;

procedure TAccountsAddFrm.CallBackSelectCityShipping(Sender: TComponent;
  AResult: Integer);
begin
  if UniMainModule.Result_zipSelection.selected = true
  then begin
         Accounts_Edit.FieldByName('SHIPPING_ADDRESS_POSTAL_CODE').AsString := UniMainModule.Result_zipSelection.zipcode;
         Accounts_Edit.FieldByName('SHIPPING_ADDRESS_CITY').AsString        := UniMainModule.Result_zipSelection.city;
       end;
end;

procedure TAccountsAddFrm.EditInternalNumberTriggerEvent(
  Sender: TUniFormControl; AButtonId: Integer);
begin
  case AButtonId of
    0: begin
         if (Accounts_Edit.FieldByName('Internal_number').AsString) <> ''
         then if MainForm.WaveConfirmBlocking(str_assign_number, str_number_present) = False
              then exit;
         Accounts_Edit.FieldByName('Internal_number').AsString := IntToStr(UniMainModule.Get_document_sequence(-4));
         if UniMainModule.UpdTrans.Active
         then UniMainModule.UpdTrans.Commit;
       end;
  end;
end;

procedure TAccountsAddFrm.Edit_Billing_Address_CityExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Billing_Address_Postal_Code.Text) = '') and (Trim(Edit_Billing_Address_City.Text) <> '')
  then begin
         selectlocation := UniMainModule.Search_zipcode(UpperCase(Edit_Billing_Address_City.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('BILLING_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('BILLING_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_zipcode_selection(Edit_Billing_Address_City.Text);
                     ShowModal(CallBackSelectCityBilling);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Billing_Address_Postal_CodeExit(
  Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Billing_Address_Postal_Code.Text) <> '') and (Trim(Edit_Billing_Address_City.Text) = '')
  then begin
         selectlocation := UniMainModule.Search_city(UpperCase(Edit_Billing_Address_Postal_Code.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('BILLING_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('BILLING_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_city_selection(Edit_Billing_Address_Postal_Code.Text);
                     ShowModal(CallBackSelectCityBilling);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Demand_CityExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Demand_Postal_Code.Text) = '') and (Trim(Edit_Demand_City.Text) <> '')
  then begin
         selectlocation := UniMainModule.Search_zipcode(UpperCase(Edit_Demand_City.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('DEMAND_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('DEMAND_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_zipcode_selection(Edit_Demand_City.Text);
                     ShowModal(CallBackSelectCityDemand);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Demand_Postal_codeExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Demand_Postal_Code.Text) <> '') and (Trim(Edit_Demand_City.Text) = '')
  then begin
         selectlocation := UniMainModule.Search_city(UpperCase(Edit_Demand_Postal_Code.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('DEMAND_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('DEMAND_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_city_selection(Edit_Demand_Postal_Code.Text);
                     ShowModal(CallBackSelectCityDemand);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Future_CityExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Future_Postal_Code.Text) = '') and (Trim(Edit_Future_City.Text) <> '')
  then begin
         selectlocation := UniMainModule.Search_zipcode(UpperCase(Edit_Future_City.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('FUTURE_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('FUTURE_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_zipcode_selection(Edit_Future_City.Text);
                     ShowModal(CallBackSelectCityFuture);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Future_Postal_codeExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Future_Postal_Code.Text) <> '') and (Trim(Edit_Future_City.Text) = '')
  then begin
         selectlocation := UniMainModule.Search_city(UpperCase(Edit_Future_Postal_Code.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('FUTURE_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('FUTURE_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_city_selection(Edit_Future_Postal_Code.Text);
                     ShowModal(CallBackSelectCityFuture);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Shipping_Address_CityExit(Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Shipping_Address_Postal_Code.Text) = '') and (Trim(Edit_Shipping_Address_City.Text) <> '')
  then begin
         selectlocation := UniMainModule.Search_zipcode(UpperCase(Edit_Shipping_Address_City.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('SHIPPING_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('SHIPPING_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_zipcode_selection(Edit_Shipping_Address_City.Text);
                     ShowModal(CallBackSelectCityShipping);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_Shipping_Address_Postal_CodeExit(
  Sender: TObject);
var selectlocation: TZipSelection;
begin
  if (Trim(Edit_Shipping_Address_Postal_Code.Text) <> '') and (Trim(Edit_Shipping_Address_City.Text) = '')
  then begin
         selectlocation := UniMainModule.Search_city(UpperCase(Edit_Shipping_Address_Postal_Code.Text));
         if (selectlocation.selected = true) and (selectlocation.multiple = false)
         then begin
                Accounts_Edit.FieldByName('SHIPPING_ADDRESS_POSTAL_CODE').AsString := selectlocation.zipcode;
                Accounts_Edit.FieldByName('SHIPPING_ADDRESS_CITY').AsString        := selectlocation.city;
              end;
         if (selectlocation.selected = false) and (selectlocation.multiple = false)
         then exit;
         if (selectlocation.selected = false) and (selectlocation.multiple = true)
         then begin
                With ZipCodesSelectFrm
                do begin
                     Init_city_selection(Edit_Shipping_Address_Postal_Code.Text);
                     ShowModal(CallBackSelectCityShipping);
                   end;
              end
         else
       end;
end;

procedure TAccountsAddFrm.Edit_VatNumberTriggerEvent(Sender: TUniFormControl;
  AButtonId: Integer);
var WorkNumber:       string;
    VatNumber:        string;
    I:                integer;
    sr:               TStringReader;
    jsonreader:       TJSONTextReader;
    SuccessValue:     integer;
    IsValidValue:     boolean;
    NameValue:        string;
    AddressValue:     string;
begin
  case AButtonId of
    0: begin
         if Trim(Edit_VatNumber.Text) = ''
         then exit;

         Worknumber := '';
         VatNumber  := Edit_VatNumber.Text;

         for I := 1 to length(VatNumber)
         do begin
              if CharInSet(VatNumber[I], ['0'..'9'])
              then Worknumber := Worknumber + VatNumber[I];
            end ;

         if length(Worknumber) = 9
         then Worknumber := '0' + Worknumber;

         if length(Worknumber) <> 10
         then begin
                MainForm.WaveShowWarningMessage(str_invalid_company_number);
                exit;
              end;

         Worknumber := 'BE' + Worknumber;
         Request.Params.ParameterByName('vatNumber').Value := WorkNumber;
         Request.Execute;

         SuccessValue    := 0;
         IsValidValue    := false;
         NameValue       := '';
         AddressValue    := '';

         sr := TStringReader.Create(Response.Content);
         try
           JsonReader := TJsonTextReader.Create(sr);
           try
             while jsonReader.Read
             do begin
                  if jsonReader.TokenType = TJsonToken.PropertyName
                  then begin
                         if UpperCase(jsonReader.Value.ToString) = 'SUCCESS'
                         then begin
                                jsonReader.Read;
                                SuccessValue := jsonReader.Value.AsInteger;
                              end;
                         if UpperCase(jsonReader.Value.ToString) = 'ISVALID'
                         then begin
                                jsonReader.Read;
                                IsValidValue := jsonReader.Value.AsBoolean;
                              end;
                         if UpperCase(jsonReader.Value.ToString) = 'NAME'
                         then begin
                                jsonReader.Read;
                                NameValue := jsonReader.Value.AsString;
                              end;
                         if UpperCase(jsonReader.Value.ToString) = 'ADDRESS'
                         then begin
                                jsonReader.Read;
                                AddressValue := jsonReader.Value.AsString;
                              end;
                       end;
                end;
           finally
             jsonReader.Free;
           end;
         finally
           sr.Free;
         end;

         if SuccessValue = 0
         then begin
                MainForm.WaveShowErrorMessage(str_error_getting_data_kruispuntbank);
                Exit;
              end;

         if IsValidValue = False
         then begin
                MainForm.WaveShowWarningMessage(str_company_number_not_valid);
                Exit;
              end;

         if IsValidValue = True
         then MainForm.WaveShowInfoMessage(str_company_number_valid + '<br><br>' +
                                           '<b>' + str_data_crossroads_bank + '</b><br><br>'     +
                                           str_name_crossroads_bank         + NameValue + '<br>' +
                                           str_address_crossroads_bank      + AddressValue);
       end;
  end;
end;

procedure TAccountsAddFrm.BtnSaveClick(Sender: TObject);
begin
  if Save_account = True
  then begin
         Accounts_Edit.Close;
         Close;
       end;
end;

procedure TAccountsAddFrm.BtnSendTakeOverInvoicingClick(Sender: TObject);
begin
  Accounts_Edit.FieldByName('Shipping_address_street').AsString       := Accounts_Edit.FieldByName('Billing_address_street').AsString;
  Accounts_Edit.FieldByName('Shipping_address_postal_code').AsString  := Accounts_Edit.FieldByName('Billing_address_postal_code').AsString;
  Accounts_Edit.FieldByName('Shipping_address_city').AsString         := Accounts_Edit.FieldByName('Billing_address_city').AsString;
  Accounts_Edit.FieldByName('Shipping_address_state').AsString        := Accounts_Edit.FieldByName('Billing_address_state').AsString;
  Accounts_Edit.FieldByName('Shipping_address_country').AsString      := Accounts_Edit.FieldByName('Billing_address_country').AsString;
end;

function TAccountsAddFrm.Init_account_creation: boolean;
begin
  Caption                             := str_add_organisation;
  UniMainModule.Result_dbAction       := 0;
  ComboLanguageDoc.ItemIndex          := 0;
  TabSheetCreditManagement.TabVisible := UniMainModule.portal_contr_credit_module_active;
  Open_reference_tables;

  Try
    Accounts_Edit.Close;
    Accounts_Edit.SQL.Clear;
    Accounts_Edit.SQL.Add('Select first 0 * from crm_accounts');
    Accounts_Edit.Open;
    Accounts_Edit.Append;
    Accounts_Edit.FieldByName('CompanyId').AsInteger             := UniMainModule.Company_id;
    Accounts_Edit.FieldByName('Removed').AsString                := '0';
    Accounts_Edit.FieldByName('Account_customer').AsInteger      := 1;
    Accounts_Edit.FieldByName('Account_supplier').AsInteger      := 0;
    Accounts_Edit.FieldByName('INVOICE_BY_EMAIL').AsInteger      := 0;
    Accounts_Edit.FieldByName('DATE_ENTERED').AsDateTime         := Now;
//    Accounts_Edit.FieldByName('CREATED_USER_ID').AsInteger       := UniMainModule.User_id;
//    Accounts_Edit.FieldByName('ASSIGNED_USER_ID').AsInteger      := UniMainModule.User_id;
    Accounts_Edit.FieldByName('DATE_MODIFIED').AsDateTime        := Now;
//    Accounts_Edit.FieldByName('MODIFIED_USER_ID').AsInteger      := UniMainModule.User_id;
    Accounts_Edit.FieldByName('VAT_NUMBER').AsString             := 'BE';
    Accounts_Edit.FieldByName('BANKRUPTCY').AsInteger            := 0;
    Accounts_Edit.FieldByName('Obsolete').AsInteger              := 0;
    Accounts_Edit.FieldByName('Email_invalid').AsInteger         := 0;
    Result                                                       := True;
  Except
    Accounts_Edit.Close;
    MainForm.WaveShowErrorToast(MsgErrorAddItem);
    Result := False;
  End;
end;

function TAccountsAddFrm.Init_account_modification(Id: longint): boolean;
begin
  Caption                             := str_modify_organisation;
  UniMainModule.Result_dbAction       := 0;
  TabSheetCreditManagement.TabVisible := UniMainModule.portal_contr_credit_module_active;
  Open_reference_tables;
  Accounts_Edit.Close;
  Accounts_Edit.SQL.Clear;
  Accounts_Edit.SQL.Add('Select * from crm_accounts where id=' + IntToStr(Id));
  Accounts_Edit.Open;
  Try
    Accounts_Edit.Edit;
    ComboLanguageDoc.ItemIndex                                   := Accounts_Edit.FieldByName('Language_doc').AsInteger;
    Accounts_Edit.FieldByName('DATE_MODIFIED').AsDateTime        := Now;
    //Accounts_Edit.FieldByName('MODIFIED_USER_ID').AsInteger      := UniMainModule.User_id;
    Result := True;
  Except
    Result := False;
    Accounts_Edit.Cancel;
    Accounts_Edit.Close;
    MainForm.WaveShowWarningToast(MsgRecordLocked);
  End;
end;

procedure TAccountsAddFrm.LinkedLanguageAccountsAddChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TAccountsAddFrm.MenuBtnDemandClick(Sender: TObject);
begin
  PopupMenuBtnDemand.PopupBy(MenuBtnDemand);
end;

procedure TAccountsAddFrm.MenuBtnFutureClick(Sender: TObject);
begin
  PopupMenuFuture.PopupBy(MenuBtnFuture);
end;

procedure TAccountsAddFrm.Open_reference_tables;
var SQL: string;
begin
  Countries.Close;
  Countries.SQL.Clear;
  SQL := 'Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id);
  Case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by dutch';
       Edit_Billing_Address_Country.ListField  := 'DUTCH';
       Edit_Shipping_Address_Country.ListField := 'DUTCH';
       Edit_Demand_Country.ListField           := 'DUTCH';
       Edit_Future_Country.ListField           := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by french';
       Edit_Billing_Address_Country.ListField  := 'FRENCH';
       Edit_Shipping_Address_Country.ListField := 'FRENCH';
       Edit_Demand_Country.ListField           := 'FRENCH';
       Edit_Future_Country.ListField           := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by english';
       Edit_Billing_Address_Country.ListField  := 'ENGLISH';
       Edit_Shipping_Address_Country.ListField := 'ENGLISH';
       Edit_Demand_Country.ListField           := 'ENGLISH';
       Edit_Future_Country.ListField           := 'ENGLISH';
     end;
  End;
  Countries.SQL.Add(SQL);
  Countries.Open;

  Users.Close;
  Users.SQL.Clear;
  Users.SQL.Add('Select ID, COALESCE(NAME, '''') || '' '' || COALESCE(FIRSTNAME, '''') AS "USERFULLNAME" from users ' +
                'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by USERFULLNAME');
  Users.Open;

  IndustryTypes.Close;
  IndustryTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=5 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Industry_Type.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_industry_Type.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_industry_Type.ListField := 'ENGLISH';
     end;
  end;
  IndustryTypes.SQL.Add(SQL);
  IndustryTypes.Open;

  AccountTypes.Close;
  AccountTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=9 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       Edit_Account_Type.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       Edit_Account_Type.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       Edit_Account_Type.ListField := 'ENGLISH';
     end;
  end;
  AccountTypes.SQL.Add(SQL);
  AccountTypes.Open;

  CompanyTypes.Close;
  CompanyTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=4 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL                       := SQL + ' order by DUTCH';
       EditCompanyType.ListField := 'DUTCH';
     end;
  2: begin
       SQL                       := SQL + ' order by FRENCH';
       EditCompanyType.ListField := 'FRENCH';
     end;
  3: begin
       SQL                     := SQL + ' order by ENGLISH';
       EditCompanyType.ListField := 'ENGLISH';
     end;
  end;
  CompanyTypes.SQL.Add(SQL);
  CompanyTypes.Open;
end;

function TAccountsAddFrm.Save_account: boolean;
var SQL:               string;
    Result_vat_number: string;
begin
  BtnSave.SetFocus;

  if Trim(Accounts_Edit.FieldByName('Name').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_name_is_required);
         PageControlAccounts.ActivePage := TabSheetGeneral;
         Edit_Name.SetFocus;
         Result := False;
         Exit;
       end;

  if ComboLanguageDoc.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_language_doc_required_account);
         PageControlAccounts.ActivePage := TabSheetGeneral;
         ComboLanguageDoc.SetFocus;
         Result := False;
         Exit;
       end;

  //Check email
  if Trim(Accounts_Edit.FieldByName('Email').AsString) <> ''
  then if Utils.IsValidEmailRegEx(Accounts_Edit.FieldByName('Email').asString) = False
       then begin
              MainForm.WaveShowWarningToast(str_email_not_valid);
              PageControlAccounts.ActivePage := TabSheetGeneral;
              Edit_Email.SetFocus;
              Result := False;
              Exit;
            end;

  if Trim(Accounts_Edit.FieldByName('Vat_number').AsString) <> ''
  then begin
         Result_vat_number := Utils.BEcheck(Accounts_Edit.FieldByName('Vat_number').AsString);
         if Result_vat_number = ''
         then begin
                if MainForm.WaveConfirmBlocking(str_company_number, str_no_belgian_company_number_continue) = False
                then begin
                       PageControlAccounts.ActivePage := TabSheetGeneral;
                       Edit_VatNumber.SetFocus;
                       Result := False;
                       Exit;
                     end;
              end
         else Accounts_Edit.FieldByName('Vat_number').AsString := Result_vat_number;
       end;

  if (UniMainModule.Hypotheekwereld = True) and
     (Trim(Accounts_Edit.FieldByName('Email').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast('Email is verplicht in te geven');
         PageControlAccounts.ActivePage := TabSheetGeneral;
         Edit_email.SetFocus;
         Exit;
       end;

  if (UniMainModule.Hypotheekwereld = True) and
     (Trim(Accounts_Edit.FieldByName('Phone').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast('Telefoon is verplicht in te geven');
         PagecontrolAccounts.ActivePage := TabSheetGeneral;
         Exit;
       end;

  //Standard params
  Accounts_Edit.FieldByName('Date_Modified').AsDateTime   := Now;
//  Accounts_Edit.FieldByName('Modified_User_id').AsInteger := UniMainModule.User_id;
  Accounts_Edit.FieldByName('Language_doc').AsInteger     := ComboLanguageDoc.ItemIndex;

  //Check for duplicate names
  if Accounts_Edit.State = dsInsert
  then begin
         SQL := 'Select count(*) as counter from crm_accounts where companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
                'and removed=''0'' and name=' + QuotedStr(Accounts_Edit.FieldByName('Name').AsString);
         UniMainModule.QWork.Close;
         UniMainModule.QWork.SQL.Clear;
         UniMainModule.QWork.SQL.Add(SQL);
         UniMainModule.QWork.Open;
         if UniMainModule.QWork.FieldByName('Counter').AsInteger = 0
         then UniMainModule.QWork.Close
         else begin
                UniMainModule.QWork.Close;
                if MainForm.WaveConfirmBlocking(str_identical_name, str_warning_duplicate_name) = False
                then begin
                       Result := False;
                       Exit;
                     end;
              end;
       end;

  try
    Accounts_Edit.Post;
    if UpdTransAccountsEdit.Active
    then UpdTransAccountsEdit.Commit;
    UniMainModule.Result_dbAction := Accounts_Edit.FieldByName('Id').AsInteger;
    Result := True;
  except
    on E: Exception
    do begin
         if UpdTransAccountsEdit.Active
         then UpdTransAccountsEdit.Rollback;
         MainForm.WaveShowErrorToast(str_error_saving_data + E.Message);
         Result := False;
       end;
  end;
end;

procedure TAccountsAddFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TAccountsAddFrm.UniFormReady(Sender: TObject);
begin
  Edit_Remarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
  Edit_OneTime_Remarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){if(e.key == "i" && e.ctrlKey){} else {e.stopPropagation()}});');
end;

procedure TAccountsAddFrm.UpdateStrings;
begin
  str_no_belgian_company_number_continue  := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_no_belgian_company_number_continue' (* 'Geen geldig Belgisch ondernemingsnummer. Doorgaan?' *) );
  str_company_number                      := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_company_number' (* 'Ondernemingsnummer' *) );
  str_warning_duplicate_name              := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_warning_duplicate_name' (* 'Opgelet, bedrijf met identieke naam gevonden. Doorgaan?' *) );
  str_identical_name                      := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_identical_name' (* 'Identieke naam' *) );
  str_language_doc_required_account       := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_language_doc_required_account' (* 'De taal van de documenten is een verplichte ingave.' *) );
  str_number_present                      := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_number_present' (* 'Er is reeds een nummer aanwezig, wenst u door te gaan?' *) );
  str_assign_number                       := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_assign_number' (* 'Toekennen nummer' *) );
  str_email_not_valid                     := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_email_not_valid' (* 'Email adres is niet correct.' *) );
  str_address_crossroads_bank             := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_address_crossroads_bank' (* 'Adres: ' *) );
  str_name_crossroads_bank                := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_name_crossroads_bank' (* 'Naam: ' *) );
  str_data_crossroads_bank                := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_data_crossroads_bank' (* 'Gegevens kruispuntbank:' *) );
  str_company_number_valid                := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_company_number_valid' (* 'Het ondernemingsnummer is geldig.' *) );
  str_company_number_not_valid            := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_company_number_not_valid' (* 'Het ondernemingsnummer is niet geldig.' *) );
  str_error_getting_data_kruispuntbank    := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_error_getting_data_kruispuntbank' (* 'Fout bij het opvragen van de gegevens bij de kruispuntbank.' *) );
  str_invalid_company_number              := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_invalid_company_number' (* 'Ongeldig ondernemingsnummer.' *) );
  MsgNoAddressAvailable                   := LinkedLanguageAccountsAdd.GetTextOrDefault('strMsgNoAddressAvailable' (* 'Geen adres aanwezig.' *) );
  MsgRecordLocked                         := LinkedLanguageAccountsAdd.GetTextOrDefault('strMsgRecordLocked' (* 'In gebruik door een andere gebruiker!' *) );
  MsgErrorAddItem                         := LinkedLanguageAccountsAdd.GetTextOrDefault('strMsgErrorAddItem' (* 'Fout bij het toevoegen!' *) );
  str_modify_organisation                 := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_modify_organisation' (* 'Wijzigen organisatie' *) );
  str_add_organisation                    := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_add_organisation' (* 'Toevoegen organisatie' *) );
  str_error_saving_data                   := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens: ' *) );
  str_name_is_required                    := LinkedLanguageAccountsAdd.GetTextOrDefault('strstr_name_is_required' (* 'De naam is een verplichte ingave.' *) );
end;

end.



