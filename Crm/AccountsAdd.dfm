object AccountsAddFrm: TAccountsAddFrm
  Left = 0
  Top = 0
  ClientHeight = 729
  ClientWidth = 1016
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = Edit_Name
  Images = UniMainModule.ImageList
  ImageIndex = 20
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  TextHeight = 15
  object BtnSave: TUniThemeButton
    Left = 736
    Top = 680
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 0
    ScreenMask.Target = Owner
    OnClick = BtnSaveClick
    ButtonTheme = uctSuccess
  end
  object BtnCancel: TUniThemeButton
    Left = 872
    Top = 680
    Width = 129
    Height = 33
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 1
    ScreenMask.Target = Owner
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object PageControlAccounts: TUniPageControl
    Left = 16
    Top = 16
    Width = 985
    Height = 649
    Hint = ''
    ActivePage = TabSheetGeneral
    Images = UniMainModule.ImageList
    TabOrder = 2
    object TabSheetGeneral: TUniTabSheet
      Hint = ''
      ImageIndex = 20
      Caption = 'Algemeen'
      object Edit_Name: TUniDBEdit
        Left = 16
        Top = 16
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'NAME'
        DataSource = Ds_Accounts_Edit
        TabOrder = 0
        FieldLabel = 'Naam *'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Account_Type: TUniDBLookupComboBox
        Left = 16
        Top = 80
        Width = 449
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsAccountTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'ACCOUNT_TYPE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Type'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_Phone: TUniDBEdit
        Left = 16
        Top = 112
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'PHONE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 3
        FieldLabel = 'Telefoon'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Email: TUniDBEdit
        Left = 16
        Top = 144
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'EMAIL'
        DataSource = Ds_Accounts_Edit
        TabOrder = 4
        FieldLabel = 'Email'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Website: TUniDBEdit
        Left = 16
        Top = 176
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'WEBSITE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 5
        FieldLabel = 'Website'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Industry_Type: TUniDBLookupComboBox
        Left = 16
        Top = 208
        Width = 449
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsIndustryTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'INDUSTRY_TYPE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 6
        Color = clWindow
        FieldLabel = 'Sector'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_VatNumber: TUniDBEdit
        Left = 16
        Top = 240
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'VAT_NUMBER'
        DataSource = Ds_Accounts_Edit
        TabOrder = 7
        FieldLabel = 'Ondern. / btw-nr.'
        FieldLabelWidth = 110
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 3
            ButtonId = 0
            HandleClicks = True
            Hint = 'Controleren ondernemingsnummer kruispuntbank'
          end>
        OnTriggerEvent = Edit_VatNumberTriggerEvent
      end
      object Edit_Assigned_User_Id: TUniDBLookupComboBox
        Left = 16
        Top = 304
        Width = 449
        Hint = ''
        ListField = 'USERFULLNAME'
        ListSource = DsUsers
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'ASSIGNED_USER_ID'
        DataSource = Ds_Accounts_Edit
        TabOrder = 9
        Color = clWindow
        FieldLabel = 'Eigenaar'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
      object Edit_Remarks: TUniDBMemo
        Left = 480
        Top = 120
        Width = 449
        Height = 105
        Hint = ''
        DataField = 'REMARKS'
        DataSource = Ds_Accounts_Edit
        TabOrder = 14
        FieldLabel = 'Opmerkingen'
        FieldLabelAlign = laTop
      end
      object GroupBoxInvoiceAddress: TUniGroupBox
        Left = 16
        Top = 336
        Width = 449
        Height = 233
        Hint = ''
        Caption = 'Factuuradres'
        TabOrder = 16
        object Edit_Billing_Address_Street: TUniDBEdit
          Left = 16
          Top = 24
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'BILLING_ADDRESS_STREET'
          DataSource = Ds_Accounts_Edit
          TabOrder = 1
          FieldLabel = 'Straat'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Billing_Address_Postal_Code: TUniDBEdit
          Left = 16
          Top = 56
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'BILLING_ADDRESS_POSTAL_CODE'
          DataSource = Ds_Accounts_Edit
          TabOrder = 2
          FieldLabel = 'Postcode'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Billing_Address_Postal_CodeExit
        end
        object Edit_Billing_Address_City: TUniDBEdit
          Left = 16
          Top = 88
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'BILLING_ADDRESS_CITY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 3
          FieldLabel = 'Locatie'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Billing_Address_CityExit
        end
        object Edit_Billing_Address_Country: TUniDBLookupComboBox
          Left = 16
          Top = 120
          Width = 425
          Hint = ''
          ListField = 'DUTCH'
          ListSource = DsCountries
          KeyField = 'CODE'
          ListFieldIndex = 0
          DataField = 'BILLING_ADDRESS_COUNTRY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 4
          Color = clWindow
          FieldLabel = 'Land'
          FieldLabelWidth = 110
          ForceSelection = True
          Style = csDropDown
        end
        object EditBillingAddressContactName: TUniDBEdit
          Left = 16
          Top = 152
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'BILLING_ADDRESS_CONTACT_NAME'
          DataSource = Ds_Accounts_Edit
          TabOrder = 5
          FieldLabel = 'T.a.v.'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object EditBillingAddressEmail: TUniDBEdit
          Left = 16
          Top = 184
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'BILLING_ADDRESS_EMAIL'
          DataSource = Ds_Accounts_Edit
          TabOrder = 6
          FieldLabel = 'Email'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
      end
      object GroupBoxShippingAddress: TUniGroupBox
        Left = 480
        Top = 344
        Width = 481
        Height = 201
        Hint = ''
        Caption = 'Verzendingsadres'
        TabOrder = 17
        object Edit_Shipping_Address_Street: TUniDBEdit
          Left = 16
          Top = 24
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'SHIPPING_ADDRESS_STREET'
          DataSource = Ds_Accounts_Edit
          TabOrder = 1
          FieldLabel = 'Straat'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Shipping_Address_Postal_Code: TUniDBEdit
          Left = 16
          Top = 56
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'SHIPPING_ADDRESS_POSTAL_CODE'
          DataSource = Ds_Accounts_Edit
          TabOrder = 2
          FieldLabel = 'Postcode'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Shipping_Address_Postal_CodeExit
        end
        object Edit_Shipping_Address_City: TUniDBEdit
          Left = 16
          Top = 88
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'SHIPPING_ADDRESS_CITY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 3
          FieldLabel = 'Locatie'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Shipping_Address_CityExit
        end
        object Edit_Shipping_Address_Country: TUniDBLookupComboBox
          Left = 16
          Top = 120
          Width = 425
          Hint = ''
          ListField = 'DUTCH'
          ListSource = DsCountries
          KeyField = 'CODE'
          ListFieldIndex = 0
          DataField = 'SHIPPING_ADDRESS_COUNTRY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 4
          Color = clWindow
          FieldLabel = 'Land'
          FieldLabelWidth = 110
          ForceSelection = True
          Style = csDropDown
        end
        object EditShippingAddressEmail: TUniDBEdit
          Left = 16
          Top = 152
          Width = 425
          Height = 22
          Hint = ''
          DataField = 'SHIPPING_ADDRESS_EMAIL'
          DataSource = Ds_Accounts_Edit
          TabOrder = 5
          FieldLabel = 'Email'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object BtnSendTakeOverInvoicing: TUniThemeButton
          Left = 448
          Top = 24
          Width = 25
          Height = 25
          Hint = 'Overname gegevens factuuradres'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 6
          Images = UniMainModule.ImageList
          ImageIndex = 5
          OnClick = BtnSendTakeOverInvoicingClick
          ButtonTheme = uctSecondary
        end
      end
      object Edit_OneTime_Remarks: TUniDBMemo
        Left = 480
        Top = 240
        Width = 481
        Height = 81
        Hint = ''
        DataField = 'ONETIME_REMARK'
        DataSource = Ds_Accounts_Edit
        TabOrder = 15
        FieldLabel = 'E'#233'nmalige opmerking'
        FieldLabelAlign = laTop
      end
      object CheckboxCustomer: TUniDBCheckBox
        Left = 16
        Top = 576
        Width = 185
        Height = 17
        Hint = ''
        DataField = 'ACCOUNT_CUSTOMER'
        DataSource = Ds_Accounts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 'Klant'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 18
        ParentColor = False
        Color = clBtnFace
      end
      object CheckboxSupplier: TUniDBCheckBox
        Left = 208
        Top = 576
        Width = 185
        Height = 17
        Hint = ''
        DataField = 'ACCOUNT_SUPPLIER'
        DataSource = Ds_Accounts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 'Leverancier'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 19
        ParentColor = False
        Color = clBtnFace
      end
      object EditInternalNumber: TUniDBEdit
        Left = 16
        Top = 48
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'INTERNAL_NUMBER'
        DataSource = Ds_Accounts_Edit
        TabOrder = 1
        FieldLabel = 'Intern nr.'
        FieldLabelWidth = 110
        SelectOnFocus = True
        Triggers = <
          item
            ImageIndex = 5
            ButtonId = 0
            HandleClicks = True
            UseDefaultHandler = False
            Hint = 'Nummer toekennen'
          end>
        OnTriggerEvent = EditInternalNumberTriggerEvent
      end
      object CheckBoxInvoiceByEmail: TUniDBCheckBox
        Left = 480
        Top = 16
        Width = 481
        Height = 17
        Hint = ''
        DataField = 'INVOICE_BY_EMAIL'
        DataSource = Ds_Accounts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 10
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Wenst facturen te ontvangen via mail'
        FieldLabelWidth = 350
      end
      object BtnCreateContact: TUniThemeButton
        Left = 480
        Top = 560
        Width = 169
        Height = 33
        Hint = ''
        Caption = 'Contactpersoon'
        TabStop = False
        TabOrder = 20
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnCreateContactClick
        ButtonTheme = uctPrimary
      end
      object ComboLanguageDoc: TUniComboBox
        Left = 480
        Top = 88
        Width = 481
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Nederlands'
          'Frans'
          'Engels')
        TabOrder = 13
        FieldLabel = 'Taal doc. *'
        FieldLabelWidth = 110
        IconItems = <>
      end
      object CheckBoxObsolete: TUniDBCheckBox
        Left = 480
        Top = 40
        Width = 481
        Height = 17
        Hint = ''
        DataField = 'OBSOLETE'
        DataSource = Ds_Accounts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 11
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Bedrijfsfiche is verouderd (niet meer gebruiken)'
        FieldLabelWidth = 350
      end
      object CheckBoxEmailInvalid: TUniDBCheckBox
        Left = 480
        Top = 64
        Width = 481
        Height = 17
        Hint = ''
        DataField = 'EMAIL_INVALID'
        DataSource = Ds_Accounts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 12
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Email niet geldig'
        FieldLabelWidth = 350
      end
      object EditCompanyType: TUniDBLookupComboBox
        Left = 16
        Top = 272
        Width = 449
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsCompanyTypes
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'COMPANY_TYPE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 8
        Color = clWindow
        FieldLabel = 'Bedrijfsvorm'
        FieldLabelWidth = 110
        ForceSelection = True
        Style = csDropDown
      end
    end
    object TabSheetCreditManagement: TUniTabSheet
      Hint = ''
      ImageIndex = 21
      Caption = 'Kredietbeheer'
      object GroupBoxDemandAddress: TUniGroupBox
        Left = 16
        Top = 16
        Width = 417
        Height = 225
        Hint = ''
        Caption = 'Adres bij aanvraag'
        TabOrder = 0
        object Edit_Demand_street: TUniDBEdit
          Left = 16
          Top = 24
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'DEMAND_ADDRESS_STREET'
          DataSource = Ds_Accounts_Edit
          TabOrder = 1
          FieldLabel = 'Straat'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Demand_Postal_code: TUniDBEdit
          Left = 16
          Top = 56
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'DEMAND_ADDRESS_POSTAL_CODE'
          DataSource = Ds_Accounts_Edit
          TabOrder = 2
          FieldLabel = 'Postcode'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Demand_Postal_codeExit
        end
        object Edit_Demand_City: TUniDBEdit
          Left = 16
          Top = 88
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'DEMAND_ADDRESS_CITY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 3
          FieldLabel = 'Locatie'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Demand_CityExit
        end
        object Edit_Demand_State: TUniDBEdit
          Left = 16
          Top = 120
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'DEMAND_ADDRESS_STATE'
          DataSource = Ds_Accounts_Edit
          TabOrder = 4
          FieldLabel = 'Provincie'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Demand_Country: TUniDBLookupComboBox
          Left = 16
          Top = 152
          Width = 385
          Hint = ''
          ListField = 'DUTCH'
          ListSource = DsCountries
          KeyField = 'CODE'
          ListFieldIndex = 0
          DataField = 'DEMAND_ADDRESS_COUNTRY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 5
          Color = clWindow
          FieldLabel = 'Land'
          FieldLabelWidth = 110
          ForceSelection = True
          Style = csDropDown
        end
        object MenuBtnDemand: TUniThemeButton
          Left = 16
          Top = 184
          Width = 25
          Height = 25
          Hint = 'Overname gegevens'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 6
          Images = UniMainModule.ImageList
          ImageIndex = 5
          OnClick = MenuBtnDemandClick
          ButtonTheme = uctPrimary
        end
      end
      object GroupBoxFutureAddress: TUniGroupBox
        Left = 448
        Top = 16
        Width = 417
        Height = 225
        Hint = ''
        Caption = 'Toekomstig adres'
        TabOrder = 1
        object Edit_Future_Street: TUniDBEdit
          Left = 16
          Top = 24
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_STREET'
          DataSource = Ds_Accounts_Edit
          TabOrder = 1
          FieldLabel = 'Straat'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Future_Postal_code: TUniDBEdit
          Left = 16
          Top = 56
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_POSTAL_CODE'
          DataSource = Ds_Accounts_Edit
          TabOrder = 2
          FieldLabel = 'Postcode'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Future_Postal_codeExit
        end
        object Edit_Future_City: TUniDBEdit
          Left = 16
          Top = 88
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_CITY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 3
          FieldLabel = 'Locatie'
          FieldLabelWidth = 110
          SelectOnFocus = True
          OnExit = Edit_Future_CityExit
        end
        object Edit_Future_State: TUniDBEdit
          Left = 16
          Top = 120
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'FUTURE_ADDRESS_STATE'
          DataSource = Ds_Accounts_Edit
          TabOrder = 4
          FieldLabel = 'Provincie'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Future_Country: TUniDBLookupComboBox
          Left = 16
          Top = 152
          Width = 385
          Hint = ''
          ListField = 'DUTCH'
          ListSource = DsCountries
          KeyField = 'CODE'
          ListFieldIndex = 0
          DataField = 'FUTURE_ADDRESS_COUNTRY'
          DataSource = Ds_Accounts_Edit
          TabOrder = 5
          Color = clWindow
          FieldLabel = 'Land'
          FieldLabelWidth = 110
          ForceSelection = True
          Style = csDropDown
        end
        object MenuBtnFuture: TUniThemeButton
          Left = 16
          Top = 184
          Width = 25
          Height = 25
          Hint = 'Overname gegevens'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 6
          Images = UniMainModule.ImageList
          ImageIndex = 5
          OnClick = MenuBtnFutureClick
          ButtonTheme = uctPrimary
        end
      end
      object GroupboxAccountCredit: TUniGroupBox
        Left = 16
        Top = 248
        Width = 417
        Height = 137
        Hint = ''
        Caption = 'Rekening krediet'
        TabOrder = 2
        object Edit_BankAccount_Credit: TUniDBEdit
          Left = 16
          Top = 24
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'BANKACCOUNT_CREDIT'
          DataSource = Ds_Accounts_Edit
          TabOrder = 1
          FieldLabel = 'Bankrekening'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Iban_Credit: TUniDBEdit
          Left = 16
          Top = 56
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'IBAN_CREDIT'
          DataSource = Ds_Accounts_Edit
          TabOrder = 2
          FieldLabel = 'Iban'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
        object Edit_Bic_Credit: TUniDBEdit
          Left = 16
          Top = 88
          Width = 385
          Height = 22
          Hint = ''
          DataField = 'BIC_CREDIT'
          DataSource = Ds_Accounts_Edit
          TabOrder = 3
          FieldLabel = 'Bic'
          FieldLabelWidth = 110
          SelectOnFocus = True
        end
      end
    end
    object TabSheetSocialMedia: TUniTabSheet
      Hint = ''
      ImageIndex = 60
      Caption = 'Social media'
      object Edit_Facebook: TUniDBEdit
        Left = 16
        Top = 16
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'FACEBOOK'
        DataSource = Ds_Accounts_Edit
        TabOrder = 0
        FieldLabel = 'Facebook'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Twitter: TUniDBEdit
        Left = 16
        Top = 48
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'TWITTER'
        DataSource = Ds_Accounts_Edit
        TabOrder = 1
        FieldLabel = 'Twitter'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_GooglePlus: TUniDBEdit
        Left = 16
        Top = 112
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'GOOGLEPLUS'
        DataSource = Ds_Accounts_Edit
        TabOrder = 3
        FieldLabel = 'Google +'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_instagram: TUniDBEdit
        Left = 16
        Top = 80
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'INSTAGRAM'
        DataSource = Ds_Accounts_Edit
        TabOrder = 2
        FieldLabel = 'Instagram'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_LinkedIn: TUniDBEdit
        Left = 16
        Top = 144
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'LINKEDIN'
        DataSource = Ds_Accounts_Edit
        TabOrder = 4
        FieldLabel = 'LinkedIn'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
    end
    object TabSheetOther: TUniTabSheet
      Hint = ''
      ImageIndex = 66
      Caption = 'Overig'
      object Edit_Annual_Revenue: TUniDBFormattedNumberEdit
        Left = 16
        Top = 80
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'ANNUAL_REVENUE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 2
        SelectOnFocus = True
        FieldLabel = 'Jaarlijkse omzet'
        FieldLabelWidth = 110
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object Edit_Employee_Count: TUniDBNumberEdit
        Left = 16
        Top = 112
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'EMPLOYEE_COUNT'
        DataSource = Ds_Accounts_Edit
        TabOrder = 3
        SelectOnFocus = True
        FieldLabel = '# werknemers'
        FieldLabelWidth = 110
        DecimalSeparator = ','
      end
      object Edit_Phone_Office: TUniDBEdit
        Left = 16
        Top = 16
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'PHONE_OFFICE'
        DataSource = Ds_Accounts_Edit
        TabOrder = 0
        FieldLabel = 'Telefoon kantoor'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object Edit_Fax: TUniDBEdit
        Left = 16
        Top = 48
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'FAX'
        DataSource = Ds_Accounts_Edit
        TabOrder = 1
        FieldLabel = 'Fax'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object EditBankAccount: TUniDBEdit
        Left = 16
        Top = 144
        Width = 417
        Height = 22
        Hint = ''
        DataField = 'BANKACCOUNT'
        DataSource = Ds_Accounts_Edit
        TabOrder = 4
        FieldLabel = 'Bankrekening'
        FieldLabelWidth = 110
        SelectOnFocus = True
      end
      object CheckBox_bankruptcy: TUniDBCheckBox
        Left = 16
        Top = 184
        Width = 417
        Height = 17
        Hint = ''
        DataField = 'BANKRUPTCY'
        DataSource = Ds_Accounts_Edit
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 5
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Faillissement'
        FieldLabelWidth = 110
      end
    end
  end
  object ImageLastModification: TUniImage
    Left = 16
    Top = 680
    Width = 16
    Height = 16
    Cursor = crHandPoint
    Hint = 'Laatste wijziging'
    ShowHint = True
    ParentShowHint = False
    Stretch = True
    Url = 'images/wv_time_gray.svg'
  end
  object LblLastModification: TUniDBText
    Left = 40
    Top = 680
    Width = 101
    Height = 13
    Cursor = crHandPoint
    Hint = ''
    DataField = 'DATE_MODIFIED'
    DataSource = Ds_Accounts_Edit
    ParentFont = False
    Font.Color = 9474192
  end
  object Countries: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from countries order by dutch')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 552
    Top = 216
  end
  object DsCountries: TDataSource
    DataSet = Countries
    Left = 552
    Top = 263
  end
  object Users: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, NAME || '#39' '#39' || FIRSTNAME AS "USERFULLNAME" from users'
      'order by USERFULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 488
    Top = 320
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 488
    Top = 376
  end
  object IndustryTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 624
    Top = 216
  end
  object DsIndustryTypes: TDataSource
    DataSet = IndustryTypes
    Left = 624
    Top = 263
  end
  object AccountTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 712
    Top = 216
  end
  object DsAccountTypes: TDataSource
    DataSet = AccountTypes
    Left = 712
    Top = 263
  end
  object LinkedLanguageAccountsAdd: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'Countries.SQLDelete'
      'Countries.SQLInsert'
      'Countries.SQLLock'
      'Countries.SQLRecCount'
      'Countries.SQLRefresh'
      'Countries.SQLUpdate'
      'Users.SQLDelete'
      'Users.SQLInsert'
      'Users.SQLLock'
      'Users.SQLRecCount'
      'Users.SQLRefresh'
      'Users.SQLUpdate'
      'IndustryTypes.SQLDelete'
      'IndustryTypes.SQLInsert'
      'IndustryTypes.SQLLock'
      'IndustryTypes.SQLRecCount'
      'IndustryTypes.SQLRefresh'
      'IndustryTypes.SQLUpdate'
      'AccountTypes.SQLDelete'
      'AccountTypes.SQLInsert'
      'AccountTypes.SQLLock'
      'AccountTypes.SQLRecCount'
      'AccountTypes.SQLRefresh'
      'AccountTypes.SQLUpdate'
      'Accounts_Edit.SQLDelete'
      'Accounts_Edit.SQLInsert'
      'Accounts_Edit.SQLLock'
      'Accounts_Edit.SQLRecCount'
      'Accounts_Edit.SQLRefresh'
      'Accounts_Edit.SQLUpdate'
      'UpdTransAccountsEdit.Params'
      'ImageLastModification.Url'
      'Accounts_Edit.KeyGenerator'
      'TabSheetSocialMedia.Layout'
      'TabSheetCreditManagement.Layout'
      'GroupBoxInvoiceAddress.Layout'
      'GroupBoxShippingAddress.Layout'
      'GroupBoxDemandAddress.Layout'
      'GroupboxAccountCredit.Layout'
      'GroupBoxFutureAddress.Layout'
      'TAccountsAddFrm.Layout'
      'TabSheetGeneral.Layout'
      'Edit_Name.FieldLabelSeparator'
      'Edit_Account_Type.FieldLabelSeparator'
      'Edit_Phone.FieldLabelSeparator'
      'Edit_Email.FieldLabelSeparator'
      'Edit_Website.FieldLabelSeparator'
      'Edit_Industry_Type.FieldLabelSeparator'
      'Edit_VatNumber.FieldLabelSeparator'
      'Edit_Assigned_User_Id.FieldLabelSeparator'
      'Edit_Remarks.FieldLabelSeparator'
      'Edit_Billing_Address_Street.FieldLabelSeparator'
      'Edit_Billing_Address_Postal_Code.FieldLabelSeparator'
      'Edit_Billing_Address_City.FieldLabelSeparator'
      'Edit_Billing_Address_Country.FieldLabelSeparator'
      'Edit_Shipping_Address_Street.FieldLabelSeparator'
      'Edit_Shipping_Address_Postal_Code.FieldLabelSeparator'
      'Edit_Shipping_Address_City.FieldLabelSeparator'
      'Edit_Shipping_Address_Country.FieldLabelSeparator'
      'Edit_Facebook.FieldLabelSeparator'
      'Edit_Twitter.FieldLabelSeparator'
      'Edit_GooglePlus.FieldLabelSeparator'
      'Edit_OneTime_Remarks.FieldLabelSeparator'
      'Edit_instagram.FieldLabelSeparator'
      'Edit_Demand_street.FieldLabelSeparator'
      'Edit_Demand_Postal_code.FieldLabelSeparator'
      'Edit_Demand_City.FieldLabelSeparator'
      'Edit_Demand_State.FieldLabelSeparator'
      'Edit_Demand_Country.FieldLabelSeparator'
      'Edit_Future_Street.FieldLabelSeparator'
      'Edit_Future_Postal_code.FieldLabelSeparator'
      'Edit_Future_City.FieldLabelSeparator'
      'Edit_Future_State.FieldLabelSeparator'
      'Edit_Future_Country.FieldLabelSeparator'
      'Edit_BankAccount_Credit.FieldLabelSeparator'
      'Edit_Iban_Credit.FieldLabelSeparator'
      'Edit_Bic_Credit.FieldLabelSeparator'
      'TabSheetOther.Layout'
      'Edit_Annual_Revenue.FieldLabelSeparator'
      'Edit_Employee_Count.FieldLabelSeparator'
      'CheckboxCustomer.FieldLabelSeparator'
      'CheckboxCustomer.ValueChecked'
      'CheckboxCustomer.ValueUnchecked'
      'CheckboxSupplier.FieldLabelSeparator'
      'CheckboxSupplier.ValueChecked'
      'CheckboxSupplier.ValueUnchecked'
      'EditInternalNumber.FieldLabelSeparator'
      'Edit_Phone_Office.FieldLabelSeparator'
      'Edit_Fax.FieldLabelSeparator'
      'Client.Accept'
      'Client.AcceptCharset'
      'Client.BaseURL'
      'Client.FallbackCharsetEncoding'
      'Client.UserAgent'
      'Request.Accept'
      'Request.AcceptCharset'
      'Request.Resource'
      'Response.ContentType'
      'Authenticator.Password'
      'Authenticator.Username'
      'Request.Params'
      'CheckBoxInvoiceByEmail.FieldLabelSeparator'
      'CheckBoxInvoiceByEmail.ValueChecked'
      'CheckBoxInvoiceByEmail.ValueUnchecked'
      'EditBankAccount.FieldLabelSeparator'
      'EditBillingAddressContactName.FieldLabelSeparator'
      'EditShippingAddressEmail.FieldLabelSeparator'
      'EditBillingAddressEmail.FieldLabelSeparator'
      'ComboLanguageDoc.FieldLabelSeparator'
      'Edit_LinkedIn.FieldLabelSeparator'
      'CheckBox_bankruptcy.ValueChecked'
      'CheckBox_bankruptcy.ValueUnchecked'
      'CheckBox_bankruptcy.FieldLabelSeparator'
      'CheckBoxObsolete.FieldLabelSeparator'
      'CheckBoxObsolete.ValueChecked'
      'CheckBoxObsolete.ValueUnchecked'
      'CheckBoxEmailInvalid.FieldLabelSeparator'
      'CheckBoxEmailInvalid.ValueChecked'
      'CheckBoxEmailInvalid.ValueUnchecked'
      'EditCompanyType.FieldLabelSeparator')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageAccountsAddChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 624
    Top = 320
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00420074006E00530061007600650001004F00700073006C00
      610061006E000100530061007500760065006700610072006400650072000100
      530061007600650001000D000A00420074006E00430061006E00630065006C00
      010041006E006E0075006C006500720065006E00010041006E006E0075006C00
      650072000100430061006E00630065006C0001000D000A005400610062005300
      6800650065007400470065006E006500720061006C00010041006C0067006500
      6D00650065006E0001004700E9006E00E900720061006C000100470065006E00
      6500720061006C0001000D000A00470072006F007500700042006F0078004900
      6E0076006F006900630065004100640064007200650073007300010046006100
      6300740075007500720061006400720065007300010041006400720065007300
      7300650020006400650020006600610063007400750072006100740069006F00
      6E000100420069006C006C0069006E0067002000610064006400720065007300
      730001000D000A00470072006F007500700042006F0078005300680069007000
      700069006E006700410064006400720065007300730001005600650072007A00
      65006E00640069006E0067007300610064007200650073000100410064007200
      650073007300650020006400650020006C006900760072006100690073006F00
      6E0001005300680069007000700069006E006700200061006400640072006500
      7300730001000D000A005400610062005300680065006500740053006F006300
      690061006C004D006500640069006100010053006F006300690061006C002000
      6D00650064006900610001004D00E9006400690061007300200073006F006300
      6900610075007800010053006F006300690061006C0020006D00650064006900
      610001000D000A00540061006200530068006500650074004300720065006400
      690074004D0061006E006100670065006D0065006E00740001004B0072006500
      6400690065007400620065006800650065007200010047006500730074006900
      6F006E002000640065007300200063007200E900640069007400730001004300
      7200650064006900740020004D0061006E006100670065006D0065006E007400
      01000D000A00470072006F007500700042006F007800440065006D0061006E00
      6400410064006400720065007300730001004100640072006500730020006200
      69006A002000610061006E007600720061006100670001004100640072006500
      7300730065002000E00020006C0061002000640065006D0061006E0064006500
      010041006400640072006500730073002000750070006F006E00200072006500
      7100750065007300740001000D000A00470072006F007500700042006F007800
      4600750074007500720065004100640064007200650073007300010054006F00
      65006B006F006D00730074006900670020006100640072006500730001004100
      6400720065007300730065002000660075007400750072006500010046007500
      74007500720065002000610064006400720065007300730001000D000A004700
      72006F007500700062006F0078004100630063006F0075006E00740043007200
      65006400690074000100520065006B0065006E0069006E00670020006B007200
      65006400690065007400010043006F006D00700074006500200063007200E900
      64006900740001004100630063006F0075006E00740020006300720065006400
      6900740001000D000A00420074006E00440065006D0061006E00640054006100
      6B0065004F0076006500720049006E0076006F00690063006500410064006400
      720065007300730001004F007600650072006E0061006D006500200076006100
      6E00200066006100630074007500750072006100640072006500730001005200
      65007000720069007300650020006400650020006C0027006100640072006500
      7300730065002000640065002000660061006300740075007200610074006900
      6F006E0001005400720061006E00730066006500720020006F00660020006200
      69006C006C0069006E0067002000610064006400720065007300730001000D00
      0A00420074006E00440065006D0061006E006400540061006B00650053006500
      6E006400410064006400720065007300730001004F007600650072006E006100
      6D0065002000760061006E0020007600650072007A0065006E00640069006E00
      6700730061006400720065007300010052006500700072006900730065002000
      6400650020006C00270061006400720065007300730065002000640065002000
      6C006900760072006100690073006F006E0001005400720061006E0073006600
      6500720020006F00660020007300680069007000700069006E00670020006100
      64006400720065007300730001000D000A00420074006E00440065006D006100
      6E006400540061006B0065004F00760065007200460075007400750072006500
      410064006400720065007300730001004F007600650072006E0061006D006500
      2000760061006E00200074006F0065006B006F006D0073007400690067002000
      6100640072006500730001005200650070007200690073006500200064006500
      20006C0027006100640072006500730073006500200066007500740075007200
      65000100540061006B0065006F0076006500720020006F006600200066007500
      74007500720065002000610064006400720065007300730001000D000A004200
      74006E00460075007400750072006500540061006B0065004F00760065007200
      49006E0076006F00690063006500410064006400720065007300730001004F00
      7600650072006E0061006D0065002000760061006E0020006600610063007400
      7500750072006100640072006500730001005200650070007200690073006500
      20006400650020006C0027006100640072006500730073006500200064006500
      20006600610063007400750072006100740069006F006E000100540072006100
      6E00730066006500720020006F0066002000620069006C006C0069006E006700
      2000610064006400720065007300730001000D000A00420074006E0046007500
      7400750072006500540061006B006500530065006E0064004100640064007200
      65007300730001004F007600650072006E0061006D0065002000760061006E00
      20007600650072007A0065006E00640069006E00670073006100640072006500
      73000100520065007000720069007300650020006400650020006C0027006100
      64007200650073007300650020006400650020006C0069007600720061006900
      73006F006E0001005400720061006E00730066006500720020006F0066002000
      7300680069007000700069006E00670020006100640064007200650073007300
      01000D000A00420074006E00460075007400750072006500540061006B006500
      440065006D0061006E006400410064006400720065007300730001004F007600
      650072006E0061006D0065002000760061006E00200061006400720065007300
      2000620069006A002000610061006E0076007200610061006700010052006500
      7000720069007300650020006400650020006C00270061006400720065007300
      730065002000E00020006C0061002000640065006D0061006E00640065000100
      5400720061006E00730066006500720020006F00660020006100640064007200
      6500730073002000750070006F006E0020007200650071007500650073007400
      01000D000A00540061006200530068006500650074004F007400680065007200
      01004F007600650072006900670001004100750074007200650001004F007400
      68006500720001000D000A0043006800650063006B0062006F00780043007500
      730074006F006D006500720001004B006C0061006E007400010043006C006900
      65006E007400010043006C00690065006E00740001000D000A00430068006500
      63006B0062006F00780053007500700070006C0069006500720001004C006500
      76006500720061006E006300690065007200010046006F00750072006E006900
      73007300650075007200010053007500700070006C0069006500720001000D00
      0A00420074006E0043007200650061007400650043006F006E00740061006300
      7400010043006F006E00740061006300740070006500720073006F006F006E00
      010043006F006E007400610063007400010043006F006E007400610063007400
      01000D000A0073007400480069006E00740073005F0055006E00690063006F00
      640065000D000A0049006D006100670065004C006100730074004D006F006400
      69006600690063006100740069006F006E0001004C0061006100740073007400
      65002000770069006A007A006900670069006E00670001004400650072006E00
      69006500720020006300680061006E00670065006D0065006E00740001004C00
      61007300740020006D006F00640069006600690063006100740069006F006E00
      01000D000A00420074006E00530065006E006400540061006B0065004F007600
      6500720049006E0076006F006900630069006E00670001004F00760065007200
      6E0061006D00650020006700650067006500760065006E007300200066006100
      630074007500750072006100640072006500730001005400720061006E007300
      66006500720074002000640065007300200064006F006E006E00E90065007300
      20006400650020006C0027006100640072006500730073006500200064006500
      20006600610063007400750072006100740069006F006E000100420069006C00
      6C0069006E006700200061006400640072006500730073002000640061007400
      610020007400720061006E00730066006500720001000D000A004D0065006E00
      7500420074006E00440065006D0061006E00640001004F007600650072006E00
      61006D00650020006700650067006500760065006E0073000100520065007000
      7200690073006500200064006500200064006F006E006E00E900650073000100
      5400720061006E0073006600650072002000640061007400610001000D000A00
      4D0065006E007500420074006E0046007500740075007200650001004F007600
      650072006E0061006D00650020006700650067006500760065006E0073000100
      5200650070007200690073006500200064006500200064006F006E006E00E900
      6500730001005400720061006E00730066006500720020006400610074006100
      01000D000A007300740044006900730070006C00610079004C00610062006500
      6C0073005F0055006E00690063006F00640065000D000A007300740046006F00
      6E00740073005F0055006E00690063006F00640065000D000A00730074004D00
      75006C00740069004C0069006E00650073005F0055006E00690063006F006400
      65000D000A0043006F006D0062006F004C0061006E0067007500610067006500
      44006F0063002E004900740065006D00730001004E0065006400650072006C00
      61006E00640073002C004600720061006E0073002C0045006E00670065006C00
      730001004E00E900650072006C0061006E0064006100690073002C0046007200
      61006E00E7006100690073002C0041006E0067006C0061006900730001004400
      75007400630068002C004600720065006E00630068002C0045006E0067006C00
      69007300680001000D000A007300740053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A007300740072007300740072005F00
      6E0061006D0065005F00690073005F0072006500710075006900720065006400
      01004400650020006E00610061006D002000690073002000650065006E002000
      76006500720070006C0069006300680074006500200069006E00670061007600
      65002E0001004C00650020006E006F006D002000650073007400200075006E00
      6500200065006E0074007200E900650020006F0062006C006900670061007400
      6F006900720065002E00010054006800650020006E0061006D00650020006900
      73002000610020006D0061006E006400610074006F0072007900200065006E00
      7400720079002E0001000D000A007300740072007300740072005F0065007200
      72006F0072005F0073006100760069006E0067005F0064006100740061000100
      46006F00750074002000620069006A00200068006500740020006F0070007300
      6C00610061006E002000760061006E0020006400650020006700650067006500
      760065006E0073003A002000010045007200720065007500720020006C006F00
      7200730020006400650020006C00270065006E00720065006700690073007400
      720065006D0065006E0074002000640065007300200064006F006E006E00E900
      650073003A00200001004500720072006F007200200073006100760069006E00
      67002000740068006500200064006100740061003A00200001000D000A007300
      740072007300740072005F006100640064005F006F007200670061006E006900
      73006100740069006F006E00010054006F00650076006F006500670065006E00
      2000620065006400720069006A006600010041006A006F007500740065007200
      200075006E006500200065006E00740072006500700072006900730065000100
      410064006400200063006F006D00700061006E00790001000D000A0073007400
      72007300740072005F006D006F0064006900660079005F006F00720067006100
      6E00690073006100740069006F006E000100570069006A007A00690067006500
      6E002000620065006400720069006A00660001004D006F006400690066006900
      65007200200075006E006500200065006E007400720065007000720069007300
      650001004D006F006400690066007900200063006F006D00700061006E007900
      01000D000A007300740072004D00730067004500720072006F00720041006400
      64004900740065006D00010046006F00750074002000620069006A0020006800
      65007400200074006F00650076006F006500670065006E002100010045007200
      720065007500720020006C006F007200730020006400650020006C0027006100
      6A006F0075007400210001004500720072006F00720020007700680069006C00
      6500200061006400640069006E006700210001000D000A007300740072004D00
      730067005200650063006F00720064004C006F0063006B006500640001004900
      6E0020006700650062007200750069006B00200064006F006F00720020006500
      65006E00200061006E0064006500720065002000670065006200720075006900
      6B00650072002100010045006E00200063006F00750072007300200064002700
      7500740069006C00690073006100740069006F006E0020007000610072002000
      75006E0020006100750074007200650020007500740069006C00690073006100
      74006500750072002100010049006E0020007500730065002000620079002000
      61006E006F00740068006500720020007500730065007200210001000D000A00
      7300740072004D00730067004E006F0041006400640072006500730073004100
      7600610069006C00610062006C00650001004700650065006E00200061006400
      7200650073002000610061006E00770065007A00690067002E00010041007500
      630075006E00650020006100640072006500730073006500200070007200E900
      730065006E00740065002E0001004E006F002000610064006400720065007300
      7300200061007600610069006C00610062006C0065002E0001000D000A007300
      740072007300740072005F0069006E00760061006C00690064005F0063006F00
      6D00700061006E0079005F006E0075006D0062006500720001004F006E006700
      65006C0064006900670020006F006E006400650072006E0065006D0069006E00
      670073006E0075006D006D00650072002E0001004E0075006D00E90072006F00
      20006400270065006E007400720065007000720069007300650020006E006F00
      6E002000760061006C006900640065002E00010049006E00760061006C006900
      6400200065006E007400650072007000720069007300650020006E0075006D00
      6200650072002E0001000D000A007300740072007300740072005F0065007200
      72006F0072005F00670065007400740069006E0067005F006400610074006100
      5F006B007200750069007300700075006E007400620061006E006B0001004600
      6F00750074002000620069006A00200068006500740020006F00700076007200
      6100670065006E002000760061006E0020006400650020006700650067006500
      760065006E0073002000620069006A0020006400650020006B00720075006900
      7300700075006E007400620061006E006B002E00010045007200720065007500
      720020006C006F007200730020006400650020006C0061002000640065006D00
      61006E00640065002000640065007300200064006F006E006E00E90065007300
      2000E00020006C0061002000620061006E007100750065002000640027006900
      6E00740065007200730065006300740069006F006E0073002E00010045007200
      72006F0072002000720065007100750065007300740069006E00670020007400
      68006500200064006100740061002000660072006F006D002000740068006500
      200069006E00740065007200730065006300740069006F006E00200062006100
      6E006B002E0001000D000A007300740072007300740072005F0063006F006D00
      700061006E0079005F006E0075006D006200650072005F006E006F0074005F00
      760061006C0069006400010048006500740020006F006E006400650072006E00
      65006D0069006E00670073006E0075006D006D00650072002000690073002000
      6E006900650074002000670065006C006400690067002E0001004C0065002000
      6E0075006D00E90072006F0020006400270065006E0074007200650070007200
      69007300650020006E0027006500730074002000700061007300200076006100
      6C006900640065002E000100540068006500200065006E007400650072007000
      720069007300650020006E0075006D0062006500720020006900730020006E00
      6F0074002000760061006C00690064002E0001000D000A007300740072007300
      740072005F0063006F006D00700061006E0079005F006E0075006D0062006500
      72005F00760061006C0069006400010048006500740020006F006E0064006500
      72006E0065006D0069006E00670073006E0075006D006D006500720020006900
      73002000670065006C006400690067002E0001004C00650020006E0075006D00
      E90072006F0020006400270065006E0074007200650070007200690073006500
      20006500730074002000760061006C006900640065002E000100540068006500
      200065006E007400650072007000720069007300650020006E0075006D006200
      650072002000690073002000760061006C00690064002E0001000D000A007300
      740072007300740072005F0064006100740061005F00630072006F0073007300
      72006F006100640073005F00620061006E006B00010047006500670065007600
      65006E00730020006B007200750069007300700075006E007400620061006E00
      6B003A00010049006E0066006F0072006D006100740069006F006E0073002000
      73007500720020006C0061002000420061006E00710075006500200043006100
      72007200650066006F00750072003A000100430072006F007300730072006F00
      610064002000420061006E006B00200049006E0066006F0072006D0061007400
      69006F006E003A0001000D000A007300740072007300740072005F006E006100
      6D0065005F00630072006F007300730072006F006100640073005F0062006100
      6E006B0001004E00610061006D003A00200001004E006F006D003A0020000100
      4E0061006D0065003A00200001000D000A007300740072007300740072005F00
      61006400640072006500730073005F00630072006F007300730072006F006100
      640073005F00620061006E006B000100410064007200650073003A0020000100
      41006400720065007300730065003A0020000100410064006400720065007300
      73003A00200001000D000A007300740072007300740072005F0065006D006100
      69006C005F006E006F0074005F00760061006C0069006400010045006D006100
      69006C0020006100640072006500730020006900730020006E00690065007400
      200063006F00720072006500630074002E0001004C0027006100640072006500
      730073006500200065002D006D00610069006C00200065007300740020006900
      6E0063006F007200720065006300740065002E00010045006D00610069006C00
      20006100640064007200650073007300200069007300200069006E0063006F00
      720072006500630074002E0001000D000A007300740072007300740072005F00
      610073007300690067006E005F006E0075006D00620065007200010054006F00
      65006B0065006E006E0065006E0020006E0075006D006D006500720001004100
      74007400720069006200750065007200200075006E0020006E0075006D00E900
      72006F000100410073007300690067006E0020006E0075006D00620065007200
      01000D000A007300740072007300740072005F006E0075006D00620065007200
      5F00700072006500730065006E00740001004500720020006900730020007200
      65006500640073002000650065006E0020006E0075006D006D00650072002000
      610061006E00770065007A00690067002C002000770065006E00730074002000
      7500200064006F006F00720020007400650020006700610061006E003F000100
      49006C00200079002000610020006400E9006A00E000200075006E0020006E00
      75006D00E90072006F00200064006900730070006F006E00690062006C006500
      2C00200076006F0075006C0065007A002D0076006F0075007300200063006F00
      6E00740069006E007500650072003F0001005400680065007200650020006900
      7300200061006C00720065006100640079002000610020006E0075006D006200
      65007200200061007600610069006C00610062006C0065002C00200064006F00
      200079006F0075002000770061006E007400200074006F00200063006F006E00
      740069006E00750065003F0001000D000A007300740072007300740072005F00
      6C0061006E00670075006100670065005F0064006F0063005F00720065007100
      750069007200650064005F006100630063006F0075006E007400010044006500
      20007400610061006C002000760061006E00200064006500200064006F006300
      75006D0065006E00740065006E002000690073002000650065006E0020007600
      6500720070006C0069006300680074006500200069006E006700610076006500
      2E0001004C00610020006C0061006E0067007500650020006400650073002000
      64006F00630075006D0065006E00740073002000650073007400200075006E00
      6500200065006E0074007200E900650020006F0062006C006900670061007400
      6F006900720065002E000100540068006500200064006F00630075006D006500
      6E00740020006C0061006E006700750061006700650020006900730020006100
      200072006500710075006900720065006400200065006E007400720079002E00
      01000D000A007300740072007300740072005F006900640065006E0074006900
      630061006C005F006E0061006D00650001004900640065006E00740069006500
      6B00650020006E00610061006D0001004E006F006D0020006900640065006E00
      7400690071007500650001004900640065006E0074006900630061006C002000
      6E0061006D00650001000D000A007300740072007300740072005F0077006100
      72006E0069006E0067005F006400750070006C00690063006100740065005F00
      6E0061006D00650001004F007000670065006C00650074002C00200062006500
      6400720069006A00660020006D006500740020006900640065006E0074006900
      65006B00650020006E00610061006D0020006700650076006F006E0064006500
      6E002E00200044006F006F0072006700610061006E003F000100410074007400
      65006E00740069006F006E002C00200065006E00740072006500700072006900
      730065002000740072006F0075007600E9006500200061007600650063002000
      6C00650020006D00EA006D00650020006E006F006D002E00200043006F006E00
      740069006E007500650072003F00010041007400740065006E00740069006F00
      6E002C00200063006F006D00700061006E007900200066006F0075006E006400
      2000770069007400680020006900640065006E0074006900630061006C002000
      6E0061006D0065002E00200043006F006E00740069006E00750065003F000100
      0D000A007300740072007300740072005F0063006F006D00700061006E007900
      5F006E0075006D0062006500720001004F006E006400650072006E0065006D00
      69006E00670073006E0075006D006D006500720001004E0075006D00E9007200
      6F0020006400270065006E007400720065007000720069007300650001004300
      6F006D00700061006E00790020006E0075006D0062006500720001000D000A00
      7300740072007300740072005F006E006F005F00620065006C00670069006100
      6E005F0063006F006D00700061006E0079005F006E0075006D00620065007200
      5F0063006F006E00740069006E007500650001004700650065006E0020006700
      65006C006400690067002000420065006C006700690073006300680020006F00
      6E006400650072006E0065006D0069006E00670073006E0075006D006D006500
      72002E00200044006F006F0072006700610061006E003F000100500061007300
      200075006E0020006E0075006D00E90072006F0020006400270065006E007400
      72006500700072006900730065002000620065006C0067006500200076006100
      6C006900640065002E00200043006F006E00740069006E007500650072003F00
      01004E006F007400200061002000760061006C00690064002000420065006C00
      6700690061006E00200063006F006D00700061006E00790020006E0075006D00
      6200650072002E00200043006F006E00740069006E00750065003F0001000D00
      0A00730074004F00740068006500720053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A0045006400690074005F004E006100
      6D0065002E004600690065006C0064004C006100620065006C0001004E006100
      61006D0020002A0001004E006F006D0020002A0001004E0061006D0065002000
      2A0001000D000A0045006400690074005F004100630063006F0075006E007400
      5F0054007900700065002E004600690065006C0064004C006100620065006C00
      0100540079007000650001005400790070006500010054007900700065000100
      0D000A0045006400690074005F00500068006F006E0065002E00460069006500
      6C0064004C006100620065006C000100540065006C00650066006F006F006E00
      01005400E9006C00E900700068006F006E0065000100500068006F006E006500
      01000D000A0045006400690074005F0045006D00610069006C002E0046006900
      65006C0064004C006100620065006C00010045006D00610069006C0001004500
      6D00610069006C00010045002D006D00610069006C0001000D000A0045006400
      690074005F0057006500620073006900740065002E004600690065006C006400
      4C006100620065006C0001005700650062007300690074006500010053006900
      7400650020007700650062000100570065006200730069007400650001000D00
      0A0045006400690074005F0049006E006400750073007400720079005F005400
      7900700065002E004600690065006C0064004C006100620065006C0001005300
      6500630074006F00720001005300650063007400650075007200010053006500
      630074006F00720001000D000A0045006400690074005F005600610074004E00
      75006D006200650072002E004600690065006C0064004C006100620065006C00
      01004F006E006400650072006E002E0020002F0020006200740077002D006E00
      72002E0001004E00B000200065006E00740072002E0020002F00200074007600
      6100010045006E0074002E0020002F00200076006100740020004E006F000100
      0D000A0045006400690074005F00410073007300690067006E00650064005F00
      55007300650072005F00490064002E004600690065006C0064004C0061006200
      65006C00010045006900670065006E006100610072000100500072006F007000
      72006900E9007400610069007200650001004F0077006E006500720001000D00
      0A0045006400690074005F00520065006D00610072006B0073002E0046006900
      65006C0064004C006100620065006C0001004F0070006D00650072006B006900
      6E00670065006E000100520065006D0061007200710075006500730001005200
      65006D00610072006B00730001000D000A0045006400690074005F0042006900
      6C006C0069006E0067005F0041006400640072006500730073005F0053007400
      72006500650074002E004600690065006C0064004C006100620065006C000100
      5300740072006100610074000100520075006500010053007400720065006500
      740001000D000A0045006400690074005F00420069006C006C0069006E006700
      5F0041006400640072006500730073005F0050006F007300740061006C005F00
      43006F00640065002E004600690065006C0064004C006100620065006C000100
      50006F007300740063006F0064006500010043006F0064006500200070006F00
      7300740061006C00010050006F007300740061006C00200043006F0064006500
      01000D000A0045006400690074005F00420069006C006C0069006E0067005F00
      41006400640072006500730073005F0043006900740079002E00460069006500
      6C0064004C006100620065006C0001004C006F00630061007400690065000100
      4C0069006500750001004C006F0063006100740069006F006E0001000D000A00
      45006400690074005F00420069006C006C0069006E0067005F00410064006400
      72006500730073005F0043006F0075006E007400720079002E00460069006500
      6C0064004C006100620065006C0001004C0061006E0064000100500061007900
      7300010043006F0075006E0074007200790001000D000A004500640069007400
      5F005300680069007000700069006E0067005F00410064006400720065007300
      73005F005300740072006500650074002E004600690065006C0064004C006100
      620065006C000100530074007200610061007400010052007500650001005300
      7400720065006500740001000D000A0045006400690074005F00530068006900
      7000700069006E0067005F0041006400640072006500730073005F0050006F00
      7300740061006C005F0043006F00640065002E004600690065006C0064004C00
      6100620065006C00010050006F007300740063006F0064006500010043006F00
      64006500200070006F007300740061006C00010050006F007300740061006C00
      200043006F006400650001000D000A0045006400690074005F00530068006900
      7000700069006E0067005F0041006400640072006500730073005F0043006900
      740079002E004600690065006C0064004C006100620065006C0001004C006F00
      6300610074006900650001004C0069006500750001004C006F00630061007400
      69006F006E0001000D000A0045006400690074005F0053006800690070007000
      69006E0067005F0041006400640072006500730073005F0043006F0075006E00
      7400720079002E004600690065006C0064004C006100620065006C0001004C00
      61006E00640001005000610079007300010043006F0075006E00740072007900
      01000D000A0045006400690074005F00460061006300650062006F006F006B00
      2E004600690065006C0064004C006100620065006C0001004600610063006500
      62006F006F006B000100460061006300650062006F006F006B00010046006100
      6300650062006F006F006B0001000D000A0045006400690074005F0054007700
      690074007400650072002E004600690065006C0064004C006100620065006C00
      0100540077006900740074006500720001005400770069007400740065007200
      0100540077006900740074006500720001000D000A0045006400690074005F00
      47006F006F0067006C00650050006C00750073002E004600690065006C006400
      4C006100620065006C00010047006F006F0067006C00650020002B0001004700
      6F006F0067006C00650020002B00010047006F006F0067006C00650020002B00
      01000D000A0045006400690074005F004F006E006500540069006D0065005F00
      520065006D00610072006B0073002E004600690065006C0064004C0061006200
      65006C0001004500E9006E006D0061006C0069006700650020006F0070006D00
      650072006B0069006E0067000100520065006D00610072007100750065007300
      200070006F006E0063007400750065006C0001004F006E0065002D0074006900
      6D0065002000720065006D00610072006B00730001000D000A00450064006900
      74005F0069006E007300740061006700720061006D002E004600690065006C00
      64004C006100620065006C00010049006E007300740061006700720061006D00
      010049006E007300740061006700720061006D00010049006E00730074006100
      6700720061006D0001000D000A0045006400690074005F00440065006D006100
      6E0064005F007300740072006500650074002E004600690065006C0064004C00
      6100620065006C00010053007400720061006100740001005200750065000100
      53007400720065006500740001000D000A0045006400690074005F0044006500
      6D0061006E0064005F0050006F007300740061006C005F0063006F0064006500
      2E004600690065006C0064004C006100620065006C00010050006F0073007400
      63006F0064006500010043006F0064006500200070006F007300740061006C00
      010050006F007300740061006C00200043006F006400650001000D000A004500
      6400690074005F00440065006D0061006E0064005F0043006900740079002E00
      4600690065006C0064004C006100620065006C0001004C006F00630061007400
      6900650001004C0069006500750001004C006F0063006100740069006F006E00
      01000D000A0045006400690074005F00440065006D0061006E0064005F005300
      74006100740065002E004600690065006C0064004C006100620065006C000100
      500072006F00760069006E006300690065000100500072006F00760069006E00
      630065000100500072006F00760069006E006300650001000D000A0045006400
      690074005F00440065006D0061006E0064005F0043006F0075006E0074007200
      79002E004600690065006C0064004C006100620065006C0001004C0061006E00
      640001005000610079007300010043006F0075006E0074007200790001000D00
      0A0045006400690074005F004600750074007500720065005F00530074007200
      6500650074002E004600690065006C0064004C006100620065006C0001005300
      7400720061006100740001005200750065000100530074007200650065007400
      01000D000A0045006400690074005F004600750074007500720065005F005000
      6F007300740061006C005F0063006F00640065002E004600690065006C006400
      4C006100620065006C00010050006F007300740063006F006400650001004300
      6F0064006500200070006F007300740061006C00010050006F00730074006100
      6C00200043006F006400650001000D000A0045006400690074005F0046007500
      74007500720065005F0043006900740079002E004600690065006C0064004C00
      6100620065006C0001004C006F006300610074006900650001004C0069006500
      750001004C006F0063006100740069006F006E0001000D000A00450064006900
      74005F004600750074007500720065005F00530074006100740065002E004600
      690065006C0064004C006100620065006C000100500072006F00760069006E00
      6300690065000100500072006F00760069006E00630065000100500072006F00
      760069006E006300650001000D000A0045006400690074005F00460075007400
      7500720065005F0043006F0075006E007400720079002E004600690065006C00
      64004C006100620065006C0001004C0061006E00640001005000610079007300
      010043006F0075006E0074007200790001000D000A0045006400690074005F00
      420061006E006B004100630063006F0075006E0074005F004300720065006400
      690074002E004600690065006C0064004C006100620065006C00010042006100
      6E006B00720065006B0065006E0069006E006700010043006F006D0070007400
      65002000620061006E00630061006900720065000100420061006E006B002000
      6100630063006F0075006E00740001000D000A0045006400690074005F004900
      620061006E005F004300720065006400690074002E004600690065006C006400
      4C006100620065006C0001004900620061006E0001004900620061006E000100
      4900620061006E0001000D000A0045006400690074005F004200690063005F00
      4300720065006400690074002E004600690065006C0064004C00610062006500
      6C0001004200690063000100420069006300010042006900630001000D000A00
      45006400690074005F0041006E006E00750061006C005F005200650076006500
      6E00750065002E004600690065006C0064004C006100620065006C0001004A00
      6100610072006C0069006A006B007300650020006F006D007A00650074000100
      43006800690066002E002000640027006100660066002E00200061006E006E00
      750065006C00010041006E006E00750061006C002000730061006C0065007300
      01000D000A0045006400690074005F0045006D0070006C006F00790065006500
      5F0043006F0075006E0074002E004600690065006C0064004C00610062006500
      6C000100230020007700650072006B006E0065006D0065007200730001002300
      200065006D0070006C006F007900E900730001002300200065006D0070006C00
      6F00790065006500730001000D000A00450064006900740049006E0074006500
      72006E0061006C004E0075006D006200650072002E004600690065006C006400
      4C006100620065006C00010049006E007400650072006E0020006E0072002E00
      01004E002000B000200069006E007400650072006E006500010049006E007400
      650072006E0061006C0020006E006F002E0001000D000A004500640069007400
      5F00500068006F006E0065005F004F00660066006900630065002E0046006900
      65006C0064004C006100620065006C000100540065006C00650066006F006F00
      6E0020006B0061006E0074006F006F00720001005400E9006C00E90070006800
      6F006E006500200062007500720065006100750001004F006600660069006300
      65002000700068006F006E00650001000D000A0045006400690074005F004600
      610078002E004600690065006C0064004C006100620065006C00010046006100
      78000100460061007800010046006100780001000D000A004300680065006300
      6B0042006F00780049006E0076006F006900630065004200790045006D006100
      69006C002E004600690065006C0064004C006100620065006C00010057006500
      6E0073007400200066006100630074007500720065006E002000740065002000
      6F006E007400760061006E00670065006E00200076006900610020006D006100
      69006C00010053006F0075006800610069007400650020007200650063006500
      76006F0069007200200064006500730020006600610063007400750072006500
      73002000700061007200200065002D006D00610069006C00010057006F007500
      6C00640020006C0069006B006500200074006F00200072006500630065006900
      76006500200069006E0076006F00690063006500730020006200790020006D00
      610069006C0001000D000A004500640069007400420061006E006B0041006300
      63006F0075006E0074002E004600690065006C0064004C006100620065006C00
      0100420061006E006B00720065006B0065006E0069006E006700010043006F00
      6D007000740065002000620061006E0063006100690072006500010042006100
      6E006B0020006100630063006F0075006E00740001000D000A00450064006900
      7400420069006C006C0069006E00670041006400640072006500730073004300
      6F006E0074006100630074004E0061006D0065002E004600690065006C006400
      4C006100620065006C00010054002E0061002E0076002E000100C00020006C00
      270061007400740065006E00740069006F006E00200064006500010054006F00
      2000740068006500200061007400740065006E00740069006F006E0020006F00
      660001000D000A0045006400690074005300680069007000700069006E006700
      410064006400720065007300730045006D00610069006C002E00460069006500
      6C0064004C006100620065006C00010045006D00610069006C00010045006D00
      610069006C00010045002D006D00610069006C0001000D000A00450064006900
      7400420069006C006C0069006E00670041006400640072006500730073004500
      6D00610069006C002E004600690065006C0064004C006100620065006C000100
      45006D00610069006C00010045006D00610069006C00010045002D006D006100
      69006C0001000D000A0043006F006D0062006F004C0061006E00670075006100
      6700650044006F0063002E004600690065006C0064004C006100620065006C00
      01005400610061006C00200064006F0063002E0020002A0001004C0061006E00
      670075006500200064006F0063002E0020002A0001004C0061006E0067007500
      610067006500200064006F0063002E0020002A0001000D000A00450064006900
      74005F004C0069006E006B006500640049006E002E004600690065006C006400
      4C006100620065006C0001004C0069006E006B006500640049006E0001004C00
      69006E006B006500640049006E0001004C0069006E006B006500640049006E00
      01000D000A0043006800650063006B0042006F0078005F00620061006E006B00
      7200750070007400630079002E004600690065006C0064004C00610062006500
      6C0001004600610069006C006C0069007300730065006D0065006E0074000100
      4600610069006C006C006900740065000100420061006E006B00720075007000
      74006300790001000D000A0043006800650063006B0042006F0078004F006200
      73006F006C006500740065002E004600690065006C0064004C00610062006500
      6C000100420065006400720069006A0066007300660069006300680065002000
      6900730020007600650072006F0075006400650072006400200028006E006900
      6500740020006D0065006500720020006700650062007200750069006B006500
      6E00290001004C00650020006600690063006800690065007200200064006500
      20006C00270065006E0074007200650070007200690073006500200065007300
      740020006F00620073006F006C00E80074006500200028006E00650020007000
      6C007500730020007500740069006C0069007300650072002900010043006F00
      6D00700061006E0079002000660069006C00650020006900730020006F007500
      7400640061007400650064002000280064006F0020006E006F00740020007500
      73006500200061006E0079006D006F0072006500290001000D000A0043006800
      650063006B0042006F00780045006D00610069006C0049006E00760061006C00
      690064002E004600690065006C0064004C006100620065006C00010045006D00
      610069006C0020006E006900650074002000670065006C006400690067000100
      43006F00750072007200690065006C0020006E006F006E002000760061006C00
      690064006500010045006D00610069006C0020006E006F007400200076006100
      6C006900640001000D000A00450064006900740043006F006D00700061006E00
      790054007900700065002E004600690065006C0064004C006100620065006C00
      0100420065006400720069006A006600730076006F0072006D00010046006F00
      72006D002E00200063006F006D006D00650072006300690061006C0001004200
      7500730069006E00650073007300200066006F0072006D0001000D000A007300
      740043006F006C006C0065006300740069006F006E0073005F0055006E006900
      63006F00640065000D000A0045006400690074005F005600610074004E007500
      6D006200650072002E00540072006900670067006500720073005B0030005D00
      2E00480069006E007400010043006F006E00740072006F006C00650072006500
      6E0020006F006E006400650072006E0065006D0069006E00670073006E007500
      6D006D006500720020006B007200750069007300700075006E00740062006100
      6E006B0001005600E900720069006600690063006100740069006F006E002000
      6E0075006D00E90072006F0020006400270065006E0074007200650070007200
      6900730065002000640061006E00730020006C0061002000620061006E007100
      7500650020006400270069006E00740065007200730065006300740069006F00
      6E00010043006800650063006B00200065006E00740065007200700072006900
      7300650020006E0075006D00620065007200200069006E002000740068006500
      200069006E00740065007200730065006300740069006F006E00200062006100
      6E006B0001000D000A00450064006900740049006E007400650072006E006100
      6C004E0075006D006200650072002E0054007200690067006700650072007300
      5B0030005D002E00480069006E00740001004E0075006D006D00650072002000
      74006F0065006B0065006E006E0065006E000100410074007400720069006200
      750065007200200075006E0020006E0075006D00E90072006F00010041007300
      7300690067006E0020006E0075006D0062006500720001000D000A0073007400
      430068006100720053006500740073005F0055006E00690063006F0064006500
      0D000A00}
  end
  object Accounts_Edit: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_CRM_ACCOUNTS_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTransAccountsEdit
    SQL.Strings = (
      'Select * from crm_accounts')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    BeforePost = Accounts_EditBeforePost
    Left = 808
    Top = 216
  end
  object Ds_Accounts_Edit: TDataSource
    DataSet = Accounts_Edit
    Left = 808
    Top = 264
  end
  object UpdTransAccountsEdit: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 808
    Top = 320
  end
  object PopupMenuBtnDemand: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 344
    Top = 256
    object BtnDemandTakeOverInvoiceAddress: TUniMenuItem
      Caption = 'Overname van factuuradres'
      ImageIndex = 5
      OnClick = BtnDemandTakeOverInvoiceAddressClick
    end
    object BtnDemandTakeSendAddress: TUniMenuItem
      Caption = 'Overname van verzendingsadres'
      ImageIndex = 5
      OnClick = BtnDemandTakeSendAddressClick
    end
    object BtnDemandTakeOverFutureAddress: TUniMenuItem
      Caption = 'Overname van toekomstig adres'
      ImageIndex = 5
      OnClick = BtnDemandTakeOverFutureAddressClick
    end
  end
  object PopupMenuFuture: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 624
    Top = 376
    object BtnFutureTakeOverInvoiceAddress: TUniMenuItem
      Caption = 'Overname van factuuradres'
      ImageIndex = 5
      OnClick = BtnFutureTakeOverInvoiceAddressClick
    end
    object BtnFutureTakeSendAddress: TUniMenuItem
      Caption = 'Overname van verzendingsadres'
      ImageIndex = 5
      OnClick = BtnFutureTakeSendAddressClick
    end
    object BtnFutureTakeDemandAddress: TUniMenuItem
      Caption = 'Overname van adres bij aanvraag'
      ImageIndex = 5
      OnClick = BtnFutureTakeDemandAddressClick
    end
  end
  object Client: TRESTClient
    Authenticator = Authenticator
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'utf-8, *;q=0.8'
    BaseURL = 'https://api.kruispuntdatabank.be'
    Params = <>
    RaiseExceptionOn500 = False
    Left = 338
    Top = 328
  end
  object Request: TRESTRequest
    Client = Client
    Params = <
      item
        Name = 'vatNumber'
        Value = 'BE0681376203'
      end>
    Resource = 'vat-number/verify.json'
    Response = Response
    SynchronizedEvents = False
    Left = 338
    Top = 384
  end
  object Response: TRESTResponse
    ContentType = 'application/json'
    Left = 338
    Top = 432
  end
  object Authenticator: THTTPBasicAuthenticator
    Username = 'info@wavedesk.be'
    Password = 'enM0YWdpd0tGUjhGanlMY0NheWU2M2RzLzZZZ0Q1UnFxdHZScVZYNm1TRm1ValpJ'
    Left = 338
    Top = 480
  end
  object CompanyTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 912
    Top = 216
  end
  object DsCompanyTypes: TDataSource
    DataSet = CompanyTypes
    Left = 912
    Top = 271
  end
end
