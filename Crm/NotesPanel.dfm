object NotesPanelFrm: TNotesPanelFrm
  Left = 0
  Top = 0
  Width = 1109
  Height = 500
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1109
    Height = 30
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerFilterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 313
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      LayoutConfig.Margin = '0 0 0 8'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 297
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        FieldLabel = '<i class="fas fa-search fa-lg " ></i>'
        FieldLabelWidth = 25
        FieldLabelSeparator = ' '
        OnChange = EditSearchChange
      end
    end
  end
  object ContainerNotesBody: TUniContainerPanel
    Left = 0
    Top = 30
    Width = 1109
    Height = 470
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 1
    Layout = 'hbox'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 8 8 8'
    object GridNotes: TUniDBGrid
      Left = 0
      Top = 0
      Width = 809
      Height = 470
      Hint = ''
      DataSource = DsNotes
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      ForceFit = True
      LayoutConfig.Cls = 'customGrid'
      LayoutConfig.Height = '100%'
      LayoutConfig.Region = 'center'
      Align = alLeft
      TabOrder = 1
      ParentColor = False
      Color = clBtnFace
      Columns = <
        item
          FieldName = 'DATE_NOTE'
          Title.Caption = 'Datum'
          Width = 85
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'TITLE'
          Title.Caption = 'Omschrijving'
          Width = 100
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
    object MemoNotesPanel: TUniDBMemo
      Left = 809
      Top = 0
      Width = 300
      Height = 470
      Hint = ''
      DataField = 'CONTENT'
      DataSource = DsNotes
      Align = alClient
      ReadOnly = True
      TabOrder = 2
      TabStop = False
      LayoutConfig.Flex = 1
      LayoutConfig.Height = '100%'
      LayoutConfig.Split = True
    end
  end
  object Notes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO CRM_NOTES'
      
        '  (ID, COMPANYID, REMOVED, MODULE_ID, RECORD_ID, TITLE, CONTENT,' +
        ' DATE_ENTERED, DATE_MODIFIED, CREATED_USER_ID, MODIFIED_USER_ID,' +
        ' DATE_NOTE, VISIBLE_PORTAL)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :REMOVED, :MODULE_ID, :RECORD_ID, :TITLE, :C' +
        'ONTENT, :DATE_ENTERED, :DATE_MODIFIED, :CREATED_USER_ID, :MODIFI' +
        'ED_USER_ID, :DATE_NOTE, :VISIBLE_PORTAL)'
      'RETURNING '
      
        '  ID, COMPANYID, REMOVED, MODULE_ID, RECORD_ID, TITLE, DATE_ENTE' +
        'RED, DATE_MODIFIED, CREATED_USER_ID, MODIFIED_USER_ID, DATE_NOTE' +
        ', VISIBLE_PORTAL')
    SQLDelete.Strings = (
      'DELETE FROM CRM_NOTES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE CRM_NOTES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, REMOVED = :REMOVED, MODULE_I' +
        'D = :MODULE_ID, RECORD_ID = :RECORD_ID, TITLE = :TITLE, CONTENT ' +
        '= :CONTENT, DATE_ENTERED = :DATE_ENTERED, DATE_MODIFIED = :DATE_' +
        'MODIFIED, CREATED_USER_ID = :CREATED_USER_ID, MODIFIED_USER_ID =' +
        ' :MODIFIED_USER_ID, DATE_NOTE = :DATE_NOTE, VISIBLE_PORTAL = :VI' +
        'SIBLE_PORTAL'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, REMOVED, MODULE_ID, RECORD_ID, TITLE, CONT' +
        'ENT, DATE_ENTERED, DATE_MODIFIED, CREATED_USER_ID, MODIFIED_USER' +
        '_ID, DATE_NOTE, VISIBLE_PORTAL FROM CRM_NOTES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM CRM_NOTES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM CRM_NOTES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from crm_notes'
      '')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 448
    Top = 112
  end
  object DsNotes: TDataSource
    DataSet = Notes
    Left = 448
    Top = 168
  end
  object LinkedLanguageNotesPanel: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TNotesPanelFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerFilterLeft.Layout'
      'EditSearch.FieldLabelSeparator'
      'ContainerNotesBody.Layout'
      'MemoNotesPanel.FieldLabelSeparator'
      'Notes.SQLDelete'
      'Notes.SQLInsert'
      'Notes.SQLLock'
      'Notes.SQLRecCount'
      'Notes.SQLRefresh'
      'Notes.SQLUpdate')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 448
    Top = 224
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A0073007400480069006E00740073005F0055006E0069006300
      6F00640065000D000A007300740044006900730070006C00610079004C006100
      620065006C0073005F0055006E00690063006F00640065000D000A0073007400
      46006F006E00740073005F0055006E00690063006F00640065000D000A007300
      74004D0075006C00740069004C0069006E00650073005F0055006E0069006300
      6F00640065000D000A007300740053007400720069006E00670073005F005500
      6E00690063006F00640065000D000A00730074004F0074006800650072005300
      7400720069006E00670073005F0055006E00690063006F00640065000D000A00
      45006400690074005300650061007200630068002E004600690065006C006400
      4C006100620065006C0001003C006900200063006C006100730073003D002200
      6600610073002000660061002D00730065006100720063006800200066006100
      2D006C0067002000220020003E003C002F0069003E0001003C00690020006300
      6C006100730073003D0022006600610073002000660061002D00730065006100
      7200630068002000660061002D006C0067002000220020003E003C002F006900
      3E0001003C006900200063006C006100730073003D0022006600610073002000
      660061002D007300650061007200630068002000660061002D006C0067002000
      220020003E003C002F0069003E0001000D000A007300740043006F006C006C00
      65006300740069006F006E0073005F0055006E00690063006F00640065000D00
      0A0047007200690064004E006F007400650073002E0043006F006C0075006D00
      6E0073005B0030005D002E0043006800650063006B0042006F00780046006900
      65006C0064002E004600690065006C006400560061006C007500650073000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      01000D000A0047007200690064004E006F007400650073002E0043006F006C00
      75006D006E0073005B0030005D002E005400690074006C0065002E0043006100
      7000740069006F006E00010044006100740075006D0001004400610074006500
      0100440061007400650001000D000A0047007200690064004E006F0074006500
      73002E0043006F006C0075006D006E0073005B0031005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A0047007200690064004E006F00
      7400650073002E0043006F006C0075006D006E0073005B0031005D002E005400
      690074006C0065002E00430061007000740069006F006E0001004F006D007300
      63006800720069006A00760069006E0067000100440065007300630072006900
      7000740069006F006E0001004400650073006300720069007000740069006F00
      6E0001000D000A0073007400430068006100720053006500740073005F005500
      6E00690063006F00640065000D000A00}
  end
end
