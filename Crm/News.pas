unit News;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniURLFrame, uniPanel,
  uniImage, uniLabel, siComp, siLngLnk;

type
  TNewsFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    LinkedLanguageNews: TsiLangLinked;
    URLNews: TUniURLFrame;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_news_module;
  end;

implementation

{$R *.dfm}

uses MainModule, DateUtils;

{ TNewsFrm }

procedure TNewsFrm.Start_news_module;
var URL:    string;
    Module: string;
begin
  URL := '';
  case UniMainModule.Database_type of
    4:  Module := 'wavedesk';
    10: Module := 'wavedeskcredit';
    13: Module := 'wavedeskinsurance';
  end;

  case UniMainModule.LanguageManager.ActiveLanguage of
    1: URL := 'https://wavedesk.cloud/' + Module + '/news/' + UniMainModule.Company_account_name + '/intermediary_dutch/news.html?ts=' + IntToStr(DateTimeToUnix(Now));
    2: URL := 'https://wavedesk.cloud/' + Module + '/news/' + UniMainModule.Company_account_name + '/intermediary_french/news.html?ts=' + IntToStr(DateTimeToUnix(Now));
    3: URL := 'https://wavedesk.cloud/' + Module + '/news/' + UniMainModule.Company_account_name + '/intermediary_english/news.html?ts=' + IntToStr(DateTimeToUnix(Now));
    4: URL := 'https://wavedesk.cloud/' + Module + '/news/' + UniMainModule.Company_account_name + '/customer_dutch/news.html?ts=' + IntToStr(DateTimeToUnix(Now));
    5: URL := 'https://wavedesk.cloud/' + Module + '/news/' + UniMainModule.Company_account_name + '/customer_french/news.html?ts=' + IntToStr(DateTimeToUnix(Now));
    6: URL := 'https://wavedesk.cloud/' + Module + '/news/' + UniMainModule.Company_account_name + '/customer_english/news.html?ts=' + IntToStr(DateTimeToUnix(Now));
  end;

  if Trim(URL) <> ''
  then URLNews.URL := URL;
end;

end.
