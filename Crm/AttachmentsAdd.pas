unit AttachmentsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniButton, UniThemeButton,
  uniLabel, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox,
  uniEdit, uniFileUpload, DBAccess, IBC, Data.DB, MemDS, uniDBEdit,
  uniDBText, siComp, siLngLnk, uniImage;

var
	MsgFileIsRequired:         string = 'Het bestand is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgErrorSavingData:        string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	MsgErrorCreatingDirectory: string = 'Fout bij het aanmaken van de directory'; // TSI: Localized (Don't modify!)
	MsgErrorAddItem:           string = 'Fout bij het toevoegen!'; // TSI: Localized (Don't modify!)
	MsgRecordLocked:           string = 'In gebruik door een andere gebruiker!'; // TSI: Localized (Don't modify!)
	str_file_uploaded: string = 'Bestand opgeladen'; // TSI: Localized (Don't modify!)

type
  TAttachmentsAddFrm = class(TUniForm)
    BtnCancel: TUniThemeButton;
    ComboAttachmentType: TUniDBLookupComboBox;
    LblDescription: TUniLabel;
    LblAttachmentType: TUniLabel;
    LblFilename: TUniLabel;
    BtnSave: TUniThemeButton;
    UpdFilesArchive: TIBCQuery;
    Ds_UpdFilesArchive: TDataSource;
    EditDescription: TUniDBEdit;
    FileTypes: TIBCQuery;
    Ds_FileTypes: TDataSource;
    LblValueFilename: TUniDBText;
    LinkedLanguageAttachmentsAdd: TsiLangLinked;
    UpdTransFilesArchive: TIBCTransaction;
    UploadBtnFile: TUniFileUploadButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure LinkedLanguageAttachmentsAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UploadBtnFileCompleted(Sender: TObject;
      AStream: TFileStream);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    function initialize_attachment_add(Module_id: integer; Master_id: longint): boolean;
    function initialize_attachment_edit(AttachmentId: longint): boolean;
  end;

function AttachmentsAddFrm: TAttachmentsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, IOUtils, Main, System.DateUtils;

function AttachmentsAddFrm: TAttachmentsAddFrm;
begin
  Result := TAttachmentsAddFrm(UniMainModule.GetFormInstance(TAttachmentsAddFrm));
end;

procedure TAttachmentsAddFrm.BtnSaveClick(Sender: TObject);
begin
  if Trim(UpdFilesArchive.FieldByName('ORIGINAL_NAME').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(MsgFileIsRequired);
         Exit;
       end;

  UpdFilesArchive.FieldByName('DATE_ENTERED').asDateTime    := Now;
  UpdFilesArchive.FieldByName('DATE_MODIFIED').AsDateTime   := Now;
  UpdFilesArchive.Post;

  Try
    if UpdTransFilesArchive.Active
    then UpdTransFilesArchive.Commit;
    UniMainModule.Result_dbAction := UpdFilesArchive.FieldByName('Id').AsInteger;
    Close;
  Except
    on E: EDataBaseError
    do begin
         if UpdTransFilesArchive.Active
         then UpdTransFilesArchive.Rollback;
         UniMainModule.Result_dbAction := 0;
         MainForm.WaveShowErrorToast(MsgErrorSavingData + E.Message);
         Close;
       end;
  End;
end;

function TAttachmentsAddFrm.initialize_attachment_add(Module_id: integer; Master_id: longint): boolean;
begin
  Open_reference_tables;
  Try
    UpdFilesArchive.Close;
    UpdFilesArchive.SQL.Clear;
    UpdFilesArchive.SQL.Add('Select first 0 * from files_archive');
    UpdFilesArchive.Open;
    UpdFilesArchive.Append;
    UpdFilesArchive.FieldByName('COMPANYID').AsInteger        := UniMainModule.Company_id;
    UpdFilesArchive.FieldByName('VISIBLE_PORTAL').AsInteger   := 1;
    UpdFilesArchive.FieldByName('REMOVED').asString           := '0';
    UpdFilesArchive.FieldByName('MODULE_ID').AsInteger        := Module_id;
    UpdFilesArchive.FieldByName('RECORD_ID').AsInteger        := Master_id;
    UpdFilesArchive.FieldByName('DATE_ENTERED').asDateTime    := Now;
    UpdFilesArchive.FieldByName('DATE_MODIFIED').AsDateTime   := Now;
    Result                                                    := True;
  Except
    Result                                                    := False;
    MainForm.WaveShowErrorToast(MsgErrorAddItem);
  End;
end;

function TAttachmentsAddFrm.initialize_attachment_edit(AttachmentId: longint): boolean;
begin
  UploadBtnFile.Enabled := False;
  Open_reference_tables;
  UpdFilesArchive.Close;
  UpdFilesArchive.SQL.Clear;
  UpdFilesArchive.SQL.Add('Select * from files_archive where id=' + IntToStr(AttachmentId));
  UpdFilesArchive.Open;
  Try
    UpdFilesArchive.Edit;
    Result := True;
  Except
    Result := False;
    MainForm.WaveShowWarningToast(MsgRecordLocked);
  End;
end;

procedure TAttachmentsAddFrm.LinkedLanguageAttachmentsAddChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TAttachmentsAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UpdFilesArchive.Cancel;
  UpdFilesArchive.Close;
  Close;
end;

procedure TAttachmentsAddFrm.Open_reference_tables;
var SQL: string;
begin
  FileTypes.Close;
  FileTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2010 ' +
         ' and REMOVED=''0''';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       ComboAttachmentType.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       ComboAttachmentType.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       ComboAttachmentType.ListField := 'ENGLISH';
     end;
  end;
  FileTypes.SQL.Add(SQL);
  FileTypes.Open;
end;

procedure TAttachmentsAddFrm.UploadBtnFileCompleted(Sender: TObject;
  AStream: TFileStream);
var
  TargetDir:   string;
  Dir:         string;
  Folder:      string;
  SubFolder:   string;
  TargetName:  string;
  Destination: string;
begin
  Folder    := IntToStr(YearOf(Now));
  SubFolder := IntToStr(WeekOfTheYear(Now));
  TargetDir := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\' + Folder + '\' + SubFolder + '\';
  Dir       := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\' + Folder + '\' + SubFolder;

  if not DirectoryExists(Dir)
  then begin
         if ForceDirectories(Dir) = False
         then begin
                MainForm.WaveShowErrorToast(MsgErrorCreatingDirectory);
                exit;
              end;
       end;

  TargetName   := TPath.GetGuidFileName(False);
  Destination  := TargetDir + TargetName;
  CopyFile(PChar(AStream.FileName), PChar(Destination), False);
  UpdFilesArchive.FieldByName('ORIGINAL_NAME').AsString    := ExtractFileName(UploadBtnFile.FileName);
  UpdFilesArchive.FieldByName('FOLDER').AsString           := Folder;
  UpdFilesArchive.FieldByName('SUBFOLDER').AsString        := SubFolder;
  UpdFilesArchive.FieldByName('DESTINATION_NAME').asString := TargetName;

  LblValueFileName.Font.Style := [fsBold];
  LblValueFileName.Font.Color := ClGreen;

  MainForm.WaveShowConfirmationToast(str_file_uploaded);
end;

procedure TAttachmentsAddFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TAttachmentsAddFrm.UpdateStrings;
begin
  str_file_uploaded         := LinkedLanguageAttachmentsAdd.GetTextOrDefault('strstr_file_uploaded' (* 'Bestand opgeladen' *) );
  MsgRecordLocked           := LinkedLanguageAttachmentsAdd.GetTextOrDefault('strMsgRecordLocked' (* 'In gebruik door een andere gebruiker!' *) );
  MsgErrorAddItem           := LinkedLanguageAttachmentsAdd.GetTextOrDefault('strMsgErrorAddItem' (* 'Fout bij het toevoegen!' *) );
  MsgErrorCreatingDirectory := LinkedLanguageAttachmentsAdd.GetTextOrDefault('strMsgErrorCreatingDirectory' (* 'Fout bij het aanmaken van de directory' *) );
  MsgErrorSavingData        := LinkedLanguageAttachmentsAdd.GetTextOrDefault('strMsgErrorSavingData' (* 'Fout bij het opslaan van de gegevens: ' *) );
  MsgFileIsRequired         := LinkedLanguageAttachmentsAdd.GetTextOrDefault('strMsgFileIsRequired' (* 'Het bestand is een verplichte ingave.' *) );
end;

end.


