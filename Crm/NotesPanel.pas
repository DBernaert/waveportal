unit NotesPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, Data.DB, MemDS, DBAccess, IBC, uniBasicGrid, uniDBGrid,
  uniButton, UniThemeButton, uniDBNavigator, uniEdit, uniPanel, uniMemo, uniDBMemo,
  siComp, siLngLnk, uniImage;

type
  TNotesPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    Notes: TIBCQuery;
    DsNotes: TDataSource;
    ContainerNotesBody: TUniContainerPanel;
    GridNotes: TUniDBGrid;
    MemoNotesPanel: TUniDBMemo;
    LinkedLanguageNotesPanel: TsiLangLinked;
    procedure EditSearchChange(Sender: TObject);
  private
    { Private declarations }
    ModuleId: integer;
  public
    { Public declarations }
    procedure Start_notes(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, ServerModule, Main, Utils;



{ TNotesPanelFrm }

procedure TNotesPanelFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' TITLE LIKE ''%'   + Utils.RemoveQuotes(EditSearch.Text) + '%'' OR ' +
            ' CONTENT LIKE ''%' + Utils.RemoveQuotes(EditSearch.Text) + '%''';
  Notes.Filter := Filter;
end;

procedure TNotesPanelFrm.Start_notes(IdModule: integer; MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;
  Notes.Close;
  Notes.SQL.Clear;
  SQL := 'Select * from crm_notes '             +
         'where companyId='                     + IntToStr(UniMainModule.Company_id) +
         ' and Module_id=' + IntToStr(IdModule) +
         ' and visible_portal=1 and removed=''0''';
  SQL := SQL + ' order by date_note desc';
  Notes.SQL.Add(SQL);
  Notes.MasterSource := MasterDs;
  Notes.MasterFields := 'ID';
  Notes.DetailFields := 'RECORD_ID';
  Notes.Open;
end;

end.
