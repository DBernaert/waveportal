unit ContactsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniImage, uniGUIBaseClasses, uniPanel, uniDBNavigator, uniEdit, uniButton,
  UniThemeButton, uniBasicGrid, uniDBGrid, uniSplitter, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

var
	str_delete_contact: string = 'Verwijderen relatie?'; // TSI: Localized (Don't modify!)
	str_error_delete: string = 'Fout bij het verwijderen: '; // TSI: Localized (Don't modify!)
	str_record_locked: string = 'Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.'; // TSI: Localized (Don't modify!)
	str_export_contacts: string = 'Contacten'; // TSI: Localized (Don't modify!)

type
  TContactsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ImageTitle: TUniImage;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorContacts: TUniDBNavigator;
    ContainerBody: TUniContainerPanel;
    GridContacts: TUniDBGrid;
    Contacts: TIBCQuery;
    DsContacts: TDataSource;
    PanelJournal: TUniPanel;
    LinkedLanguageContactsMain: TsiLangLinked;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    procedure BtnAddClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure GridContactsFieldImage(const Column: TUniDBGridColumn; const AField: TField; var OutImage: TGraphic;
      var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
    procedure EditSearchChange(Sender: TObject);
    procedure UniFrameCreate(Sender: TObject);
    procedure LinkedLanguageContactsMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnExportListClick(Sender: TObject);
  private
    { Private declarations }
    JournalFrame: TUniFrame;
    procedure CallBackInsertUpdateContactsTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_contacts_management_module;
  end;

implementation

{$R *.dfm}

uses MainModule, UniFSConfirm, Journal, ContactsAdd, Main, ServerModule, xlsGrid;

procedure TContactsMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.Contacts_Edit.Close;
  UniMainModule.Contacts_Edit.SQL.Clear;
  UniMainModule.Contacts_Edit.SQL.Add('Select first 0 * from crm_contacts');
  UniMainModule.Contacts_Edit.Open;
  UniMainModule.Contacts_Edit.Append;
  UniMainModule.Contacts_Edit.FieldByName('CompanyId').AsInteger             := UniMainModule.Company_id;
  UniMainModule.Contacts_Edit.FieldByName('Removed').AsString                := '0';
  UniMainModule.Contacts_Edit.FieldByName('Do_not_call').AsInteger           := 0;
  UniMainModule.Contacts_Edit.FieldByName('Portal_active').AsInteger         := 0;
  UniMainModule.Contacts_Edit.FieldByName('DATE_ENTERED').AsDateTime         := Now;
  UniMainModule.Contacts_Edit.FieldByName('CREATED_USER_ID').AsInteger       := UniMainModule.User_id;
  UniMainModule.Contacts_Edit.FieldByName('ASSIGNED_USER_ID').AsInteger      := UniMainModule.User_id;

  With ContactsAddFrm do begin
    Init_contact_creation;
    ShowModal(CallBackInsertUpdateContactsTable);
  end;
end;

procedure TContactsMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Contacts.Active) or (Contacts.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, str_delete_contact,'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes
      then begin
             Try
               UniMainModule.Contacts_Edit.Close;
               UniMainModule.Contacts_Edit.SQL.Clear;
               UniMainModule.Contacts_Edit.SQL.Add('Select * from crm_contacts where id=' +
                                                    Contacts.FieldByName('Id').asString);
               UniMainModule.Contacts_Edit.Open;
               UniMainModule.Contacts_Edit.Edit;
               UniMainModule.Contacts_Edit.FieldByName('Removed').AsString := '1';
               UniMainModule.Contacts_Edit.Post;
               UniMainModule.UpdTr_Contacts_Edit.Commit;
               UniMainModule.Contacts_Edit.Close;
             Except on E: EDataBaseError
             do begin
                  UniMainModule.UpdTr_Contacts_Edit.Rollback;
                  UniMainModule.Contacts_Edit.Close;
                  UniMainModule.Show_Error(str_error_delete + E.Message);
                end;
             end;
             Contacts.Refresh;
           end;
    end);
end;

procedure TContactsMainFrm.BtnExportListClick(Sender: TObject);
var ExportFileName: string;
    AUrl: string;
    CurrentId: longint;
begin
  CurrentId := Contacts.FieldByName('Id').AsInteger;
  ExportFileName := UniServerModule.NewCacheFileUrl(False,'xls', FormatDateTime('yyyymmddhhmmss', Now), '', AUrl, True);
  TxlsGrid.ExportXls(GridContacts, str_export_contacts, '', ExportFileName, False, True);
  UniSession.SendFile(ExportFileName);
  Contacts.Locate('Id', CurrentId, []);
end;

procedure TContactsMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Contacts.Active) and (not Contacts.fieldbyname('Id').isNull) then begin
    UniMainModule.Result_dbAction := 0;
    UniMainModule.Contacts_Edit.Close;
    UniMainModule.Contacts_Edit.SQL.Clear;
    UniMainModule.Contacts_Edit.SQL.Add('Select * from crm_contacts where id=' +
                                         Contacts.FieldByName('Id').AsString);
    UniMainModule.Contacts_Edit.Open;
    Try
      UniMainModule.Contacts_Edit.Edit;
    Except
      UniMainModule.Contacts_Edit.Cancel;
      UniMainModule.Contacts_Edit.Close;
      UniMainModule.Show_Error(str_record_locked);
      Exit;
    End;

    With ContactsAddFrm do begin
        Init_contact_modification(Contacts.FieldByName('Id').AsInteger);
        ShowModal(CallBackInsertUpdateContactsTable);
    end;
  end;
end;

procedure TContactsMainFrm.CallBackInsertUpdateContactsTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if Contacts.Active then begin
      Contacts.Refresh;
      Contacts.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TContactsMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' LAST_NAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' FIRST_NAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' ADDRESS_CITY LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' PHONE_MOBILE LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' PHONE_WORK LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' EMAIL LIKE ''%' + EditSearch.Text + '%''';
  Contacts.Filter := Filter;
end;

procedure TContactsMainFrm.GridContactsFieldImage(const Column: TUniDBGridColumn; const AField: TField;
  var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(UpperCase(AField.FieldName), 'PORTAL_ACTIVE')
  then begin
         DoNotDispose := True; // we provide an static image so do not free it.
         Case AField.AsInteger of
         0: OutImage := MainForm.ImageEmpty.Picture.Graphic;
         1: OutImage := MainForm.ImageGreen.Picture.Graphic;
         End;
       end;
end;

procedure TContactsMainFrm.LinkedLanguageContactsMainChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContactsMainFrm.Start_contacts_management_module;
var SQL: string;
begin
  Contacts.Close;
  SQL := 'Select id, last_name, first_name, address_city, phone_mobile, phone_work, email, portal_active from crm_contacts ' +
         'where companyid=' + IntToStr(UniMainModule.Company_id) +
         ' and removed=''0''' +
         ' order by Last_name, First_name';
  Contacts.SQL.Clear;
  Contacts.SQL.Add(SQL);
  Contacts.Open;

  JournalFrame := TJournalFrm.Create(Self);
  With (JournalFrame as TJournalFrm)
  do begin
       Start_journal(2, DsContacts);
       Parent := PanelJournal;
     end;
  EditSearch.SetFocus;
end;


procedure TContactsMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  PanelJournal.JSInterface.JSConfig('stateId', ['PanelJournal_stateId_unique']);
  with PanelJournal
  do JSInterface.JSConfig('stateId', [Self.Name + Name]);
end;

procedure TContactsMainFrm.UpdateStrings;
begin
  str_export_contacts := LinkedLanguageContactsMain.GetTextOrDefault('strstr_export_contacts' (* 'Contacten' *) );
  str_record_locked := LinkedLanguageContactsMain.GetTextOrDefault('strstr_record_locked' (* 'Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.' *) );
  str_error_delete := LinkedLanguageContactsMain.GetTextOrDefault('strstr_error_delete' (* 'Fout bij het verwijderen: ' *) );
  str_delete_contact := LinkedLanguageContactsMain.GetTextOrDefault('strstr_delete_contact' (* 'Verwijderen persoon?' *) );
end;

end.


