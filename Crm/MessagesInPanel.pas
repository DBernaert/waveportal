unit MessagesInPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, uniImage, uniEdit, uniGUIBaseClasses,
  uniPanel, MemDS, DBAccess, IBC, uniBasicGrid, uniDBGrid, siComp, siLngLnk,
  uniButton, UniThemeButton, uniFileUpload;

var
	str_delete_message:                  string = 'Verwijderen bericht?'; // TSI: Localized (Don't modify!)
	str_error_deleting:                  string = 'Fout bij het verwijderen.'; // TSI: Localized (Don't modify!)
	str_message_not_found:               string = 'Bericht kon niet worden gevonden.'; // TSI: Localized (Don't modify!)
	str_messages:                        string = 'Berichten'; // TSI: Localized (Don't modify!)
	str_error_importing:                 string = 'Fout bij het importeren'; // TSI: Localized (Don't modify!)
	str_error_creating_directory:        string = 'Fout bij het aanmaken van de directory'; // TSI: Localized (Don't modify!)
	str_only_msg_or_pdf_files_supported: string = 'Enkel bestanden met extensie .msg of .pdf kunnen verwerkt worden'; // TSI: Localized (Don't modify!)
	str_letter:                          string = 'Brief'; // TSI: Localized (Don't modify!)
	str_error_modification_visibility:   string = 'Fout bij het aanpassen van de zichtbaarheid'; // TSI: Localized (Don't modify!)

type
  TMessagesInPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    GridMessages: TUniDBGrid;
    VwMessages: TIBCQuery;
    Ds_VwMessages: TDataSource;
    LinkedLanguageMessagesInPanel: TsiLangLinked;
    BtnOpenMessage: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    procedure EditSearchChange(Sender: TObject);
    procedure BtnExportListClick(Sender: TObject);
    procedure BtnOpenMessageClick(Sender: TObject);
    procedure LinkedLanguageMessagesInPanelChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure GridMessagesFieldImageURL(const Column: TUniDBGridColumn;
      const AField: TField; var OutImageURL: string);
  private
    { Private declarations }
    ModuleId: integer;
  public
    { Public declarations }
    procedure Start_messages_in(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, Utils, Main, MailViewer, IOUtils;

{ TMessagesInPanelFrm }

procedure TMessagesInPanelFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridMessages, str_messages, VwMessages, 'Id');
end;

procedure TMessagesInPanelFrm.BtnOpenMessageClick(Sender: TObject);
var Destination: string;
begin
  if ((VwMessages.Active) and (not VwMessages.fieldbyname('Id').isNull))
  then begin
         Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
         if Trim(VwMessages.FieldByName('Folder').AsString) <> ''
         then Destination := Destination + VwMessages.FieldByName('Folder').AsString + '\';
         if Trim(VwMessages.FieldByName('SubFolder').AsString) <> ''
         then Destination := Destination + VwMessages.FieldByName('SubFolder').AsString + '\';
         Destination := Destination + VwMessages.FieldByName('Destination_Name').AsString;

         if FileExists(Destination)
         then begin
                if VwMessages.FieldByName('Message_type').AsInteger = 1  // pdf-document
                then begin
                       UniSession.SendFile(Destination, Utils.EscapeIllegalChars(VwMessages.fieldbyname('Destination_Name').asString + '.pdf'))
                     end
                else begin
                       with MailViewerFrm
                       do begin
                            Start_mailviewer(Destination,
                                             VwMessages.FieldByName('Record_Id').AsInteger,
                                             VwMessages.FieldByName('Module_Id').AsInteger);
                            ShowModal();
                          end;
                     end;
              end
         else MainForm.WaveShowErrorToast(str_message_not_found);
       end;
end;

procedure TMessagesInPanelFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' MESSAGE_FROM_NAME LIKE ''%' + Utils.RemoveQuotes(EditSearch.Text) + '%'' OR ' +
            ' MESSAGE_SUBJECT LIKE ''%'   + Utils.RemoveQuotes(EditSearch.Text) + '%''';
  VwMessages.Filter := Filter;
end;

procedure TMessagesInPanelFrm.GridMessagesFieldImageURL(
  const Column: TUniDBGridColumn; const AField: TField;
  var OutImageURL: string);
begin
  if SameText(UpperCase(AField.FieldName), 'MESSAGE_TYPE')
  then begin
         case AField.AsInteger of
           0: OutImageURL := 'images/vw_mail_icon_grid_13.png';
           1: OutImageURL := 'images/vw_pdf_icon_grid_13.png';
         end;
       end;

  if SameText(UpperCase(AField.FieldName), 'DIRECTION')
  then begin
         Case AField.AsInteger of
           0: OutImageURL := 'images/wv_mail_received_12.png';
           1: OutImageURL := 'images/wv_mail_send_12.png';
         End;
       end;
end;

procedure TMessagesInPanelFrm.LinkedLanguageMessagesInPanelChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMessagesInPanelFrm.Start_messages_in(IdModule: integer;
  MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;
  VwMessages.Close;
  VwMessages.SQL.Clear;
  SQL := 'select id, destination_name, message_subject, message_from_name, message_from_email, message_date, module_id, record_id, ' +
         'direction, message_type, folder, subfolder ' +
         'from messages_archive ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) +
         ' and removed=''0'' '  +
         ' and Module_id=' + IntToStr(IdModule) +
         ' and visible_portal=1 ' +
         ' order by messages_archive.message_date desc';
  VwMessages.SQL.Add(SQL);
  VwMessages.MasterSource := MasterDs;
  VwMessages.MasterFields := 'ID';
  VwMessages.DetailFields := 'RECORD_ID';
  VwMessages.Open;
end;

procedure TMessagesInPanelFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMessagesInPanelFrm.UpdateStrings;
begin
  str_error_modification_visibility   := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_error_modification_visibility' (* 'Fout bij het aanpassen van de zichtbaarheid' *) );
  str_letter                          := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_letter' (* 'Brief' *) );
  str_only_msg_or_pdf_files_supported := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_only_msg_or_pdf_files_supported' (* 'Enkel bestanden met extensie .msg of .pdf kunnen verwerkt worden' *) );
  str_error_creating_directory        := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_error_creating_directory' (* 'Fout bij het aanmaken van de directory' *) );
  str_error_importing                 := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_error_importing' (* 'Fout bij het importeren' *) );
  str_messages                        := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_messages' (* 'Berichten' *) );
  str_message_not_found               := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_message_not_found' (* 'Bericht kon niet worden gevonden.' *) );
  str_error_deleting                  := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_error_deleting' (* 'Fout bij het verwijderen.' *) );
  str_delete_message                  := LinkedLanguageMessagesInPanel.GetTextOrDefault('strstr_delete_message' (* 'Verwijderen bericht?' *) );
end;

end.
