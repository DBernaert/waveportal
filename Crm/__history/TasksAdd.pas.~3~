unit TasksAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniDBCheckBox, uniGroupBox, uniMultiItem, uniComboBox, uniDateTimePicker, uniDBDateTimePicker, uniButton,
  UniThemeButton, uniMemo, uniDBMemo, uniGUIBaseClasses, uniEdit, uniDBEdit;

type
  TTasksAddFrm = class(TUniForm)
    EditTitle: TUniDBEdit;
    EditDescription: TUniDBMemo;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditTaskStart: TUniDBDateTimePicker;
    ComboStatus: TUniComboBox;
    EditResponsable: TUniDBLookupComboBox;
    Responsables: TIBCQuery;
    DsResponsables: TDataSource;
    EditTaskEnd: TUniDBDateTimePicker;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    function Init_task_creation(ModuleId: longint; RecordId: longint): boolean;
    function Init_task_modification(TaskId: longint): boolean;
  end;

function TasksAddFrm: TTasksAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function TasksAddFrm: TTasksAddFrm;
begin
  Result := TTasksAddFrm(UniMainModule.GetFormInstance(TTasksAddFrm));
end;

{ TTasksAddFrm }

procedure TTasksAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Tasks_Edit.Cancel;
  UniMainModule.Tasks_Edit.Close;
  Close;
end;

procedure TTasksAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if UniMainModule.Tasks_Edit.FieldByName('Task_Start').IsNull
  then begin
         UniMainModule.Show_warning('De begindatum is een verplichte ingave.');
         EditTaskStart.SetFocus;
         Exit;
       end;

  if UniMainModule.Tasks_Edit.FieldByName('Task_End').IsNull
  then begin
         UniMainModule.Show_warning('De vervaldatum is een verplichte ingave.');
         EditTaskEnd.SetFocus;
         Exit;
       end;

  if (UniMainModule.Tasks_Edit.Fieldbyname('Responsable_key').isNull)
  then begin
         UniMainModule.Show_warning('De verantwoordelijke is een verplichte ingave.');
         EditResponsable.SetFocus;
         exit;
       end;

  UniMainModule.Tasks_Edit.FieldByName('Task_status').AsInteger := ComboStatus.ItemIndex;
  UniMainModule.Tasks_Edit.FieldByName('Date_Modified').AsDateTime   := Now;
  UniMainModule.Tasks_Edit.FieldByName('Modified_User_id').AsInteger := UniMainModule.User_id;

  //Fill in key fields
  if Responsables.Locate('Responsable_key', UniMainModule.Tasks_Edit.FieldByName('Responsable_key').AsString, [])
  then begin
         UniMainModule.Tasks_Edit.FieldByName('Responsable_type').AsInteger := Responsables.FieldByName('Responsable_type').AsInteger;
         UniMainModule.Tasks_Edit.FieldByName('Responsable_id').AsInteger := Responsables.FieldByName('Id').AsInteger;
       end;

  try
    UniMainModule.Tasks_Edit.Post;
    UniMainModule.UpdTr_Tasks_Edit.Commit;
    UniMainModule.Result_dbAction := UniMainModule.Tasks_Edit.FieldByName('Id').AsInteger;
  except
    on E: Exception do begin
      UniMainModule.UpdTr_Tasks_Edit.Rollback;
      UniMainModule.Show_Error('Fout bij het opslaan van de gegevens: ' + E.Message);
    end;
  end;
  UniMainModule.Tasks_Edit.Close;
  Close;
end;

function TTasksAddFrm.Init_task_creation(ModuleId, RecordId: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  Caption := 'Toevoegen taak';
  Try
    UniMainModule.Tasks_Edit.Close;
    UniMainModule.Tasks_Edit.SQL.Clear;
    UniMainModule.Tasks_Edit.SQL.Add('Select first 0 * from crm_tasks');
    UniMainModule.Tasks_Edit.Open;
    UniMainModule.Tasks_Edit.Append;
    UniMainModule.Tasks_Edit.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
    UniMainModule.Tasks_Edit.FieldByName('Removed').AsString    := '0';
    UniMainModule.Tasks_Edit.FieldByName('Module_id').AsInteger := ModuleId;
    UniMainModule.Tasks_Edit.FieldByName('Record_id').AsInteger := RecordId;
    UniMainModule.Tasks_Edit.FieldByName('Date_entered').AsDateTime := Now;
    UniMainModule.Tasks_Edit.FieldByName('Date_modified').AsDateTime := Now;
    UniMainModule.Tasks_Edit.FieldByName('Task_start').AsDateTime      := Date;
    UniMainModule.Tasks_Edit.FieldByName('Task_end').AsDateTime        := Date;
    Open_reference_tables;
    Result := True;
  Except
    UniMainModule.Tasks_Edit.Close;
    Result := False;
  End;

end;

function TTasksAddFrm.Init_task_modification(TaskId: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  Caption := 'Wijzigen taak';
  Try
    UniMainModule.Tasks_Edit.Close;
    UniMainModule.Tasks_Edit.SQL.Clear;
    UniMainModule.Tasks_Edit.SQL.Add('Select * from crm_tasks where id=' + IntToStr(TaskId));
    UniMainModule.Tasks_Edit.Open;
    UniMainModule.Tasks_Edit.Edit;

    ComboStatus.ItemIndex    := UniMainModule.Tasks_Edit.FieldByName('Task_status').AsInteger;

    Open_reference_tables;

    Result := True;
  Except
    UniMainModule.Tasks_Edit.Cancel;
    UniMainModule.Tasks_Edit.Close;
    Result := False;
    UniMainModule.Show_warning('In gebruik door een andere gebruiker!');
  End;
end;

procedure TTasksAddFrm.Open_reference_tables;
var SQL: string;
begin
  Responsables.Close;
  Responsables.SQL.Clear;
  SQL := 'Select responsable_key, id, responsable_type, fullname, responsable_type_description from ' +
         '(Select 1 || id as responsable_key, id, 1 as responsable_type, coalesce(name, '''') || '' '' || ' +
         'coalesce(firstname, '''') as fullname, ''Medewerker'' as responsable_type_description ' +
         'from users where users.COMPANYID = ' + IntToStr(UniMainModule.Company_id) + ' and users.REMOVED=''0'' ' +
         'union all ' +
         'Select 2 || id as responsable_key, id, 2 as responable_type, coalesce(last_name, '''') || '' '' || ' +
         'coalesce(first_name, '''') as fullname, ''Aanbrenger'' as responsable_type_description ' +
         'from credit_contributors where credit_contributors.COMPANYID = ' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and credit_contributors.REMOVED=''0'' and credit_contributors.ID=' + IntToStr(UniMainModule.User_id) + ') ' +
         'order by fullname';
  Responsables.SQL.Add(SQL);
  Responsables.Open;
end;

procedure TTasksAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TTasksAddFrm.UniFormReady(Sender: TObject);
begin
  EditDescription.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

end.
