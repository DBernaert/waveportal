unit AttachmentsPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid, uniDBNavigator, uniEdit,
  uniGUIBaseClasses, uniPanel, Data.DB, MemDS, DBAccess, IBC, uniFileUpload, siComp, siLngLnk,
  uniImage;

type
  TAttachmentsPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    GridAttachments: TUniDBGrid;
    Attachments: TIBCQuery;
    DsAttachments: TDataSource;
    LinkedLanguageAttachmentsPanel: TsiLangLinked;
    ImageFind: TUniImage;
    BtnAddAttachment: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnOpenAttachment: TUniThemeButton;
    procedure BtnAddAttachmentClick(Sender: TObject);
    procedure BtnOpenAttachmentClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
  private
    { Private declarations }
    ModuleId: integer;
    procedure CallBackInsertUpdateAttachment(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_attachments(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, IOUtils, Main, ServerModule, AttachmentsAdd, Utils;

{ TAttachmentsPanelFrm }

procedure TAttachmentsPanelFrm.BtnAddAttachmentClick(Sender: TObject);
begin
  if Attachments.MasterSource.DataSet.FieldByName('Id').isNull
  then exit;

  With AttachmentsAddFrm
  do begin
       if initialize_attachment_add(ModuleId, Attachments.MasterSource.DataSet.FieldByName('Id').AsInteger)
       then ShowModal(CallBackInsertUpdateAttachment)
       else Close;
     end;
end;

procedure TAttachmentsPanelFrm.CallBackInsertUpdateAttachment(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Attachments.Active
         then begin
                Attachments.Refresh;
                Attachments.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TAttachmentsPanelFrm.BtnModifyClick(Sender: TObject);
begin
  if (not Attachments.Active) or (Attachments.fieldbyname('Id').isNull)
  then exit;

  with AttachmentsAddFrm
  do begin
       if initialize_attachment_edit(Attachments.fieldbyname('Id').asInteger)
       then ShowModal(CallBackInsertUpdateAttachment)
       else Close;
     end;
end;

procedure TAttachmentsPanelFrm.BtnOpenAttachmentClick(Sender: TObject);
begin
  if ((Attachments.Active) and (not Attachments.fieldbyname('Id').isNull))
  then UniSession.SendFile(UniMainModule.Root_archive + UniMainModule.Company_account_name + '\' +
                           Attachments.fieldbyname('Destination_Name').asString,
                           Attachments.fieldbyname('Original_Name').asString);
end;

procedure TAttachmentsPanelFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' ORIGINAL_NAME LIKE ''%' + Utils.RemoveQuotes(EditSearch.Text) + '%'' OR ' +
            ' DESCRIPTION LIKE ''%'   + Utils.RemoveQuotes(EditSearch.Text) + '%''';
  Attachments.Filter := Filter;
end;

procedure TAttachmentsPanelFrm.Start_attachments(IdModule: integer; MasterDs: TDataSource);
var SQL: string;
begin
  ModuleId := IdModule;
  Attachments.Close;
  Attachments.SQL.Clear;
  SQL := 'Select id, original_name, destination_name, description from files_archive ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) +
         ' and Module_id='  + IntToStr(IdModule) +
         ' and removed=''0'' ' +
         ' and visible_portal=1';
  SQL := SQL + ' order by original_name';
  Attachments.SQL.Add(SQL);
  Attachments.MasterSource := MasterDs;
  Attachments.MasterFields := 'ID';
  Attachments.DetailFields := 'RECORD_ID';
  Attachments.Open;
end;

end.
