unit CallsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniMemo, uniDBMemo, uniGUIBaseClasses, uniEdit, uniDBEdit,
  uniDateTimePicker, uniDBDateTimePicker, uniMultiItem, uniComboBox, uniGroupBox, uniCheckBox, uniDBCheckBox, Data.DB,
  MemDS, DBAccess, IBC, uniDBComboBox, uniDBLookupComboBox, uniBasicGrid, uniDBGrid,
  uniLabel, uniImage, siComp, siLngLnk;

type
  TCallsAddFrm = class(TUniForm)
    EditTitle: TUniDBEdit;
    EditDescription: TUniDBMemo;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditCallStart: TUniDBDateTimePicker;
    ComboStatus: TUniComboBox;
    ComboDirection: TUniComboBox;
    GroupBoxResult: TUniGroupBox;
    ComboPriority: TUniComboBox;
    ComboResult: TUniComboBox;
    EditNotes: TUniDBMemo;
    EditActualStart: TUniDBDateTimePicker;
    CheckBoxLeftVoiceMail: TUniDBCheckBox;
    EditResponsable: TUniDBLookupComboBox;
    Responsables: TIBCQuery;
    DsResponsables: TDataSource;
    Calls_Edit: TIBCQuery;
    Ds_Calls_Edit: TDataSource;
    UpdTr_Calls_Edit: TIBCTransaction;
    ImageTitle: TUniImage;
    LblTitle: TUniLabel;
    LinkedLanguageCallsAdd: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageCallsAddChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    function Init_call_creation(ModuleId: longint; RecordId: longint): boolean;
    function Init_call_modification(CallId: longint): boolean;
  end;

var
	MsgTimeRequired:          string = 'Het tijdstip is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgResponsableRequired:   string = 'De verantwoordelijke is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgErrorSavingData:       string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	MsgErrorAddItem:          string = 'Fout bij het toevoegen!'; // TSI: Localized (Don't modify!)
	StrAddPhone:              string = 'Toevoegen telefoon'; // TSI: Localized (Don't modify!)
	strModifyPhone:           string = 'Wijzigen telefoon'; // TSI: Localized (Don't modify!)
	strRecordLocked:          string = 'In gebruik door een andere gebruiker!'; // TSI: Localized (Don't modify!)
	strEmployee:              string = 'Medewerker'; // TSI: Localized (Don't modify!)
	strApplicator:            string = 'Aanbrenger'; // TSI: Localized (Don't modify!)
	MsgDirectionRequired:     string = 'De richting is een verplichte ingave.'; // TSI: Localized (Don't modify!)

function CallsAddFrm: TCallsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, DateUtils;

function CallsAddFrm: TCallsAddFrm;
begin
  Result := TCallsAddFrm(UniMainModule.GetFormInstance(TCallsAddFrm));
end;

{ TCallsAddFrm }

procedure TCallsAddFrm.BtnCancelClick(Sender: TObject);
begin
  Calls_Edit.Cancel;
  Calls_Edit.Close;
  Close;
end;

procedure TCallsAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if (Calls_Edit.FieldByName('Call_start').IsNull)
  then begin
         UniMainModule.Show_warning(MsgTimeRequired);
         EditCallStart.SetFocus;
         exit;
       end;

  if ComboDirection.ItemIndex = -1
  then begin
         UniMainModule.Show_warning(MsgDirectionRequired);
         ComboDirection.SetFocus;
         exit;
       end;

  if (Calls_Edit.Fieldbyname('Responsable_key').isNull)
  then begin
         UniMainModule.Show_warning(MsgResponsableRequired);
         EditResponsable.SetFocus;
         exit;
       end;

  Calls_Edit.FieldByName('Direction').AsInteger        := ComboDirection.ItemIndex;
  Calls_Edit.FieldByName('Call_status').AsInteger      := ComboStatus.ItemIndex;
  Calls_Edit.FieldByName('Call_result').AsInteger      := ComboResult.ItemIndex;
  Calls_Edit.FieldByName('Priority_code').AsInteger    := ComboPriority.ItemIndex;

  Calls_Edit.FieldByName('Date_Modified').AsDateTime   := Now;
  Calls_Edit.FieldByName('Modified_User_id').AsInteger := UniMainModule.User_id;

  //Fill in key fields
  if Responsables.Locate('Responsable_key', Calls_Edit.FieldByName('Responsable_key').AsString, [])
  then begin
         Calls_Edit.FieldByName('Responsable_type').AsInteger := Responsables.FieldByName('Responsable_type').AsInteger;
         Calls_Edit.FieldByName('Responsable_id').AsInteger   := Responsables.FieldByName('Id').AsInteger;
       end;

  try
    Calls_Edit.Post;
    if UpdTr_Calls_Edit.Active
    then UpdTr_Calls_Edit.Commit;
    UniMainModule.Result_dbAction := Calls_Edit.FieldByName('Id').AsInteger;
  except
    on E: Exception
    do begin
         if UpdTr_Calls_Edit.Active
         then UpdTr_Calls_Edit.Rollback;
         UniMainModule.Show_Error(MsgErrorSavingData + E.Message);
       end;
  end;
  Calls_Edit.Close;
  Close;
end;

procedure TCallsAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

function TCallsAddFrm.Init_call_creation(ModuleId, RecordId: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  LblTitle.Caption              := StrAddPhone;
  Try
    Calls_Edit.Close;
    Calls_Edit.SQL.Clear;
    Calls_Edit.SQL.Add('Select first 0 * from crm_calls');
    Calls_Edit.Open;
    Calls_Edit.Append;
    Calls_Edit.FieldByName('CompanyId').AsInteger        := UniMainModule.Company_id;
    Calls_Edit.FieldByName('Removed').AsString           := '0';
    Calls_Edit.FieldByName('Module_id').AsInteger        := ModuleId;
    Calls_Edit.FieldByName('Record_id').AsInteger        := RecordId;
    Calls_Edit.FieldByName('Date_entered').AsDateTime    := Now;
    Calls_Edit.FieldByName('Date_modified').AsDateTime   := Now;
    Calls_Edit.FieldByName('Created_User_Id').AsInteger  := UniMainModule.User_id;
    Calls_Edit.FieldByName('Modified_User_Id').AsInteger := UniMainModule.User_id;
    Calls_Edit.FieldByName('Call_start').AsDateTime      := Now;
    Calls_Edit.FieldByName('Actual_start').AsDateTime    := Now;
    Calls_Edit.FieldByName('Left_voicemail').AsInteger   := 0;
    Open_reference_tables;
    Result := True;
  Except
    Calls_Edit.Close;
    Result := False;
    UniMainModule.Show_error(MsgErrorAddItem);
  End;
end;

function TCallsAddFrm.Init_call_modification(CallId: longint): boolean;
begin
  UniMainModule.Result_dbAction := 0;
  LblTitle.Caption              := strModifyPhone;
  Try
    Calls_Edit.Close;
    Calls_Edit.SQL.Clear;
    Calls_Edit.SQL.Add('Select * from crm_calls where id=' + IntToStr(CallId));
    Calls_Edit.Open;
    Calls_Edit.Edit;
    ComboDirection.ItemIndex := Calls_Edit.FieldByName('Direction').AsInteger;
    ComboStatus.ItemIndex    := Calls_Edit.FieldByName('Call_status').AsInteger;
    ComboResult.ItemIndex    := Calls_Edit.FieldByName('Call_result').AsInteger;
    ComboPriority.ItemIndex  := Calls_Edit.FieldByName('Priority_code').AsInteger;
    Open_reference_tables;
    Result := True;
  Except
    Calls_Edit.Cancel;
    Calls_Edit.Close;
    Result := False;
    UniMainModule.Show_warning(strRecordLocked);
  End;
end;

procedure TCallsAddFrm.LinkedLanguageCallsAddChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCallsAddFrm.Open_reference_tables;
var SQL: string;
begin
  Responsables.Close;
  Responsables.SQL.Clear;
  SQL := 'Select responsable_key, id, responsable_type, fullname, responsable_type_description from ' +
         '(Select 1 || id as responsable_key, id, 1 as responsable_type, coalesce(name, '''') || '' '' || ' +
         'coalesce(firstname, '''') as fullname, ''Medewerker'' as responsable_type_description ' +
         'from users where users.COMPANYID = ' + IntToStr(UniMainModule.Company_id) + ' and users.REMOVED=''0'' ' +
         'union all ' +
         'Select 2 || id as responsable_key, id, 2 as responable_type, coalesce(last_name, '''') || '' '' || ' +
         'coalesce(first_name, '''') as fullname, ''Aanbrenger'' as responsable_type_description ' +
         'from credit_contributors where credit_contributors.COMPANYID = ' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and credit_contributors.REMOVED=''0'') ' +
         'order by fullname';
  Responsables.SQL.Add(SQL);
  Responsables.Open;
end;

procedure TCallsAddFrm.UniFormReady(Sender: TObject);
begin
  EditDescription.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  EditNotes.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

procedure TCallsAddFrm.UpdateStrings;
begin
  MsgDirectionRequired   := LinkedLanguageCallsAdd.GetTextOrDefault('strMsgDirectionRequired' (* 'De richting is een verplichte ingave.' *) );
  strApplicator          := LinkedLanguageCallsAdd.GetTextOrDefault('strstrApplicator' (* 'Aanbrenger' *) );
  strEmployee            := LinkedLanguageCallsAdd.GetTextOrDefault('strstrEmployee' (* 'Medewerker' *) );
  strRecordLocked        := LinkedLanguageCallsAdd.GetTextOrDefault('strstrRecordLocked' (* 'In gebruik door een andere gebruiker!' *) );
  strModifyPhone         := LinkedLanguageCallsAdd.GetTextOrDefault('strstrModifyPhone' (* 'Wijzigen telefoon' *) );
  StrAddPhone            := LinkedLanguageCallsAdd.GetTextOrDefault('strStrAddPhone' (* 'Toevoegen telefoon' *) );
  MsgErrorAddItem        := LinkedLanguageCallsAdd.GetTextOrDefault('strMsgErrorAddItem' (* 'Fout bij het toevoegen!' *) );
  MsgErrorSavingData     := LinkedLanguageCallsAdd.GetTextOrDefault('strMsgErrorSavingData' (* 'Fout bij het opslaan van de gegevens: ' *) );
  MsgResponsableRequired := LinkedLanguageCallsAdd.GetTextOrDefault('strMsgResponsableRequired' (* 'De verantwoordelijke is een verplichte ingave.' *) );
  MsgTimeRequired        := LinkedLanguageCallsAdd.GetTextOrDefault('strMsgTimeRequired' (* 'Het tijdstip is een verplichte ingave.' *) );
end;

end.

