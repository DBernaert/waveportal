unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniPanel, uniLabel, uniImage, uniEdit, uniRadioGroup,
  uniComboBox, siComp, siLngLnk, uniMultiItem, uniButton, UniThemeButton,
  Vcl.Imaging.pngimage;

type
  TLoginFrm = class(TUniFrame)
    ContainerBody: TUniContainerPanel;
    ImageLogo: TUniImage;
    LblWelcome2: TUniLabel;
    LblWelcome3: TUniLabel;
    GroupBoxLanguage: TUniRadioGroup;
    LblLogin: TUniLabel;
    EditLogin: TUniEdit;
    LblPassWord: TUniLabel;
    EditPassWord: TUniEdit;
    LinkedLanguageLogin: TsiLangLinked;
    LblReference: TUniLabel;
    BtnLogin: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    LblCode: TUniLabel;
    EditCode: TUniEdit;
    PnlCustomer: TUniPanel;
    PnlIntermediary: TUniPanel;
    PnlPartner: TUniPanel;
    ImageLogin: TUniImage;
    LblAccountType: TUniLabel;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnLoginClick(Sender: TObject);
    procedure EditLoginKeyPress(Sender: TObject; var Key: Char);
    procedure GroupBoxLanguageClick(Sender: TObject);
    procedure LinkedLanguageLoginChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);

    procedure CallBackSelectContributorAccount(Sender: TComponent; AResult: Integer);
    procedure PnlCustomerClick(Sender: TObject);
    procedure PnlIntermediaryClick(Sender: TObject);
    procedure PnlPartnerClick(Sender: TObject);
  private
    { Private declarations }
    SmsCode:               integer;
    SmsValidation:         boolean;
    SmsValidationRequired: boolean;
    SmsValidationOverride: boolean;

    SelectedPortal:        integer;
    procedure Color_selected_portal;
  public
    { Public declarations }
    procedure Start_login_procedure;
  end;

var
	MsgLoginRequired:            string = 'Uw login is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgPasswordRequired:         string = 'Uw wachtwoord is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	MsgNoAccess:                 string = 'U heeft geen toegang, neem contact op om toegang te krijgen.'; // TSI: Localized (Don't modify!)
	MsgPasswordNotCorrect:       string = 'Het opgegeven wachtwoord is niet correct.'; // TSI: Localized (Don't modify!)
	MsgErrorConnectingToAdminDb: string = 'Fout bij het verbinden aan de admin database'; // TSI: Localized (Don't modify!)
	MsgErrorWritingLoginData:    string = 'Fout bij wegschrijven login gegevens.'; // TSI: Localized (Don't modify!)
	MsgInvalidLogin:             string = 'Ongeldige login.'; // TSI: Localized (Don't modify!)
	str_code_not_valid:          string = 'Code niet geldig'; // TSI: Localized (Don't modify!)
	str_validation_not_possible: string = 'Controle niet mogelijk, mobiel nummer vereist, gelieve uw administrator te contacteren'; // TSI: Localized (Don't modify!)
	str_controle_code:           string = 'Controle'; // TSI: Localized (Don't modify!)
	str_error_sending_code:      string = 'Validatie code kon niet worden verstuurd, contacteer uw administrator om uw mobiel nummer te controleren in uw profiel'; // TSI: Localized (Don't modify!)
	str_sending_code_to:         string = 'Versturing login code naar '; // TSI: Localized (Don't modify!)
	str_login_code_send_to:      string = 'Login code verstuurd naar '; // TSI: Localized (Don't modify!)

implementation

{$R *.dfm}

uses MainModule, ServerModule, uniGUIApplication, Utils, Main, Db, SendSms,
     SelectContributorAccount;

{ TLoginFrm }

procedure TLoginFrm.BtnCancelClick(Sender: TObject);
begin
  UniSession.Terminate('<script>window.location.href = "' + UniMainModule.Portal_return_url + '";</script>');
end;

procedure TLoginFrm.BtnLoginClick(Sender: TObject);
begin
  if SmsValidation
  then begin
         if UpperCase(IntToStr(SmsCode)) = UpperCase(Trim(EditCode.Text))
         then begin
                MainForm.Show_contributors_form;
                Exit;
              end
         else begin
                ShowToast(str_code_not_valid);
                Exit;
              end;
       end;

  if Trim(EditLogin.Text) = ''
  then begin
         EditLogin.SetFocus;
         ShowToast(MsgLoginRequired);
         Exit;
       end;

  if Trim(EditPassWord.Text) = ''
  then begin
         EditPassWord.SetFocus;
         ShowToast(MsgPasswordRequired);
         Exit;
       end;

  if SelectedPortal = 0
  then begin
         //Search the user
         UniMainModule.QWork.Close;
         UniMainModule.QWork.SQL.Clear;
         UniMainModule.QWork.SQL.Add('Select id, email, portal_active, portal_password, last_name, first_name,' +
                                     ' portal_privacy_doc_signed, phone_mobile, partner ' +
                                     ' from crm_contacts where companyid=' + IntToStr(UniMainModule.Company_id) +
                                     ' and removed=''0'' ' +
                                     ' and email=' + QuotedStr(EditLogin.Text));
         UniMainModule.QWork.Open;
         if UniMainModule.QWork.RecordCount > 0
         then begin
                if UniMainModule.QWork.FieldByName('portal_active').AsInteger = 0
                then begin
                       UniMainModule.QWork.Close;
                       ShowToast(MsgNoAccess);
                       Exit;
                     end;

                if UpperCase(Utils.Decrypt(UniMainModule.QWork.FieldByName('portal_password').asString, 35462)) <>
                   UpperCase(EditPassWord.Text)
                then begin
                       UniMainModule.QWork.Close;
                       ShowToast(MsgPasswordNotCorrect);
                       Exit;
                     end;

                //Everything is ok, show the contacts frame
                UniMainModule.User_type := 0;
                UniMainModule.User_id   := UniMainModule.QWork.FieldByName('Id').AsInteger;

                if UniMainModule.QWork.FieldByName('Partner').isNull
                then UniMainModule.User_partner_id := 0
                else UniMainModule.User_partner_id :=
                     UniMainModule.QWork.FieldByName('Partner').AsInteger;

                UniMainModule.User_name := Trim(UniMainModule.QWork.FieldByName('Last_name').AsString + ' ' +
                                                UniMainModule.QWork.FieldByName('First_name').AsString);
                UniMainModule.User_email := UniMainModule.QWork.FieldByName('Email').AsString;
                UniMainModule.User_phone_mobile := UniMainModule.QWork.FieldByName('Phone_mobile').AsString;
                if UniMainModule.QWork.FieldByName('Portal_privacy_doc_signed').AsInteger <> 1
                then UniMainModule.user_portal_privacy_doc_signed := False
                else UniMainModule.user_portal_privacy_doc_signed := True;

                UniMainModule.QWork.Close;

                //Register the login
                Try
                  UniMainModule.DbAdmin.Connected := False;
                  UniMainModule.DbAdmin.Connected := true;
                Except on E: EDataBaseError
                do begin
                     ShowToast(MsgErrorConnectingToAdminDb);
                     Exit;
                   end;
                end;

                UniMainModule.ActivityLog.Close;
                UniMainModule.ActivityLog.SQL.Clear;
                UniMainModule.ActivityLog.SQL.Add('Select first 0 * from ActivityLog');
                UniMainModule.ActivityLog.Open;
                UniMainModule.ActivityLog.Append;
                UniMainMOdule.ActivityLog.FieldByName('Product_Id').AsInteger     := 5;
                UniMainModule.ActivityLog.FieldByName('Company_Name').AsString    := UniMainModule.Company_name;
                UniMainModule.ActivityLog.FieldByName('User_Name').AsString       := UniMainModule.User_name;
                UniMainModule.ActivityLog.FieldByName('Activity_Time').AsDateTime := Now;

                Try
                  UniMainModule.ActivityLog.Post;
                  if UniMainModule.UpdActivityLog.Active
                  then UniMainModule.UpdActivityLog.Commit;
                  UniMainMOdule.ActivityLog.Close;
                Except on E: EDataBaseError
                do begin
                     if UniMainModule.UpdActivityLog.Active
                     then UniMainModule.UpdActivityLog.Rollback;
                     UniMainModule.ActivityLog.Close;
                     ShowToast(MsgErrorWritingLoginData);
                   end;
                end;
                UniMainModule.DbAdmin.Connected := False;

                UniMainModule.Log_activity('');

                UniApplication.Cookies.SetCookie('_WavePortalType',     IntToStr(SelectedPortal), Date + 60.0);
                UniApplication.Cookies.SetCookie('_WavePortalLanguage', IntToStr(GroupBoxLanguage.ItemIndex), Date + 60.0);
                UniApplication.Cookies.SetCookie('_WavePortalLogin',    EditLogin.Text, Date + 60.0);
                MainForm.Show_contacts_form;
              end
         else begin
                UniMainModule.QWork.Close;
                ShowToast(MsgInvalidLogin);
                Exit;
              end;
       end
  else if SelectedPortal = 1
       then begin
              //Search the contributor
              UniMainModule.QWork.Close;
              UniMainModule.QWork.SQL.Clear;
              UniMainModule.QWork.SQL.Add('Select id, email from credit_contributors ' +
                                          'where companyid=' + IntToStr(UniMainModule.Company_id) + ' ' +
                                          'and removed=''0'' ' +
                                          'and email=' + QuotedStr(EditLogin.Text) + ' ' +
                                          'and portal_active=1');
              UniMainModule.QWork.Open;
              if UniMainModule.QWork.RecordCount > 0
              then begin
                     if UniMainModule.QWork.RecordCount = 1
                     then begin
                            UniMainModule.Result_dbAction := UniMainModule.QWork.FieldByName('Id').AsInteger;
                            UniMainModule.QWork.Close;
                            CallbackSelectContributorAccount(nil, 0);
                          end
                     else begin //Select account to work with
                            UniMainModule.Result_dbAction := 0;
                            With SelectContributorAccountFrm
                            do begin
                                 if Init_contributor_account_selection(EditLogin.Text)
                                 then ShowModal(CallBackSelectContributorAccount)
                                 else Close;
                               end;
                          end;
                   end
              else begin
                     UniMainModule.QWork.Close;
                     ShowToast(MsgInvalidLogin);
                     Exit;
                   end;
            end;
end;

procedure TLoginFrm.CallBackSelectContributorAccount(Sender: TComponent;
  AResult: Integer);
begin
  if UniMainModule.Result_dbAction = 0
  then exit;

  //Search the user
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select id, email, portal_active, portal_password, last_name, first_name, portal_rights, ' +
                              'show_commission_tab_portal, show_documents_tab_portal, phone_mobile, portal_reference_contributor, ' +
                              'portal_allow_credit_edit, stopped,  responsable_contributor ' +
                              'from credit_contributors ' +
                              'where id=' + IntToStr(UniMainModule.Result_dbAction));
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.RecordCount = 1
  then begin
         if UniMainModule.QWork.FieldByName('portal_active').AsInteger = 0
         then begin
                UniMainModule.QWork.Close;
                ShowToast(MsgNoAccess);
                Exit;
              end;

         if UpperCase(Utils.Decrypt(UniMainModule.QWork.FieldByName('portal_password').asString, 35462)) <>
            UpperCase(EditPassWord.Text)
         then begin
                UniMainModule.QWork.Close;
                ShowToast(MsgPasswordNotCorrect);
                Exit;
              end;

         //Everything is ok, show the contributors frame
         UniMainModule.User_type                         := 1;
         UniMainModule.User_id                           := UniMainModule.QWork.FieldByName('Id').AsInteger;
         UniMainModule.Reference_contributor_id          := UniMainModule.QWork.FieldByName('Id').AsInteger;
         if not UniMainModule.QWork.FieldByName('Portal_reference_contributor').IsNull
         then UniMainModule.Reference_contributor_id     := UniMainModule.QWork.FieldByName('Portal_reference_contributor').AsInteger;

         if not UniMainModule.QWork.FieldByName('Responsable_contributor').IsNull
         then UniMainModule.Responsable_contributor_credit := UniMainModule.QWork.FieldByName('Responsable_contributor').AsInteger
         else UniMainModule.Responsable_contributor_credit := 0;

         UniMainModule.User_name                         := Trim(UniMainModule.QWork.FieldByName('Last_name').AsString + ' ' +
                                                                 UniMainModule.QWork.FieldByName('First_name').AsString);
         UniMainModule.User_phone_mobile                 := UniMainModule.QWork.FieldByName('Phone_mobile').AsString;
         UniMainModule.User_portal_rights                := UniMainModule.QWork.FieldByName('portal_rights').AsInteger;
         if UniMainModule.QWork.FieldbyName('Portal_allow_credit_edit').AsInteger = 1
         then UniMainModule.User_allow_credit_edit       := True
         else UniMainModule.User_allow_credit_edit       := False;

         if UniMainModule.QWork.FieldByName('Show_commission_tab_portal').AsInteger = 0
         then UniMainModule.Show_commissions_contributor := False
         else UniMainModule.Show_commissions_contributor := True;

         if UniMainModule.QWork.FieldByName('Show_documents_tab_portal').AsInteger = 0
         then UniMainModule.Show_documents_contributor   := False
         else UniMainModule.Show_documents_contributor   := True;

         if UniMainModule.QWork.FieldByName('Stopped').AsInteger = 1
         then UniMainModule.Contributor_collaboration_stopped := True
         else UniMainModule.Contributor_collaboration_stopped := False;

         UniMainModule.QWork.Close;

         //Register the login
         Try
           UniMainModule.DbAdmin.Connected := False;
           UniMainModule.DbAdmin.Connected := true;
         Except on E: EDataBaseError
         do begin
              ShowToast(MsgErrorConnectingToAdminDb);
              Exit;
            end;
         end;

         UniMainModule.ActivityLog.Close;
         UniMainModule.ActivityLog.SQL.Clear;
         UniMainModule.ActivityLog.SQL.Add('Select first 0 * from ActivityLog');
         UniMainModule.ActivityLog.Open;
         UniMainModule.ActivityLog.Append;
         UniMainModule.ActivityLog.FieldByName('Product_Id').AsInteger     := 5;
         UniMainModule.ActivityLog.FieldByName('Company_Name').AsString    := UniMainModule.Company_name;
         UniMainModule.ActivityLog.FieldByName('User_Name').AsString       := UniMainModule.User_name;
         UniMainModule.ActivityLog.FieldByName('Activity_Time').AsDateTime := Now;

         Try
           UniMainModule.ActivityLog.Post;
           if UniMainModule.UpdActivityLog.Active
           then UniMainModule.UpdActivityLog.Commit;
           UniMainModule.ActivityLog.Close;
         Except on E: EDataBaseError
         do begin
              if UniMainModule.UpdActivityLog.Active
              then UniMainModule.UpdActivityLog.Rollback;
              UniMainModule.ActivityLog.Close;
              ShowToast(MsgErrorWritingLoginData);
            end;
         end;
         UniMainModule.DbAdmin.Connected := False;

         UniMainModule.Log_activity('');

         UniApplication.Cookies.SetCookie('_WavePortalType',     IntToStr(SelectedPortal), Date + 60.0);
         UniApplication.Cookies.SetCookie('_WavePortalLanguage', IntToStr(GroupBoxLanguage.ItemIndex), Date + 60.0);
         UniApplication.Cookies.SetCookie('_WavePortalLogin',    EditLogin.Text, Date + 60.0);

         //Check for sms validation
         if UniMainModule.Portal_2_factor = True
         then begin
                SmsValidationRequired := True;
                if Trim(UniMainModule.User_phone_mobile) = ''
                then begin
                       ShowToast(str_validation_not_possible);
                       Exit;
                     end;
                LblWelcome2.Caption        := str_sending_code_to + Utils.Format_mobile_phone(UniMainModule.User_phone_mobile);
                LblWelcome3.Visible        := False;
                UniSession.Synchronize;
                SmsValidation              := True;
                Randomize;
                SmsCode                    := Random(100000);
                GroupBoxLanguage.Enabled   := False;
                PnlCustomer.Enabled        := False;
                PnlIntermediary.Enabled    := False;
                PnlPartner.Enabled         := False;
                EditLogin.Enabled          := False;
                EditPassWord.Enabled       := False;
                LblCode.Visible            := True;
                EditCode.Visible           := True;
                BtnLogin.Caption           := str_controle_code;
                EditCode.SetFocus;
                if not SendSms.Send_sms('WAVEDESK', Utils.Format_mobile_phone(UniMainModule.User_phone_mobile),
                                                    UniMainModule.Company_name + ' login code: ' + IntToStr(SmsCode))
                then begin
                       ShowToast(str_error_sending_code);
                       LblWelcome2.Caption := str_error_sending_code;
                       Exit;
                     end
                else LblWelcome2.Caption   := str_login_code_send_to + Utils.Format_mobile_phone(UniMainModule.User_phone_mobile);
                Exit;
              end
         else begin
                SmsValidationRequired := False;
                MainForm.Show_contributors_form;
              end;
       end
  else begin
         UniMainModule.QWork.Close;
         ShowToast(MsgInvalidLogin);
         Exit;
       end;
end;

procedure TLoginFrm.Color_selected_portal;
begin
  case Selectedportal of
  0: begin
       PnlCustomer.Color          := $00834802;
       PnlCustomer.Font.Color     := ClWhite;
       PnlIntermediary.Color      := $00E8E8E8;
       PnlIntermediary.Font.Color := clWindowText;
       PnlPartner.Color           := $00E8E8E8;
       PnlPartner.Font.Color      := ClWindowText;
     end;
  1: begin
       PnlCustomer.Color          := $00E8E8E8;
       PnlCustomer.Font.Color     := clWindowText;
       PnlIntermediary.Color      := $00834802;
       PnlIntermediary.Font.Color := ClWhite;
       PnlPartner.Color           := $00E8E8E8;
       PnlPartner.Font.Color      := ClWindowText;
     end;
  2: begin
       PnlCustomer.Color          := $00E8E8E8;
       PnlCustomer.Font.Color     := clWindowText;
       PnlIntermediary.Color      := $00E8E8E8;
       PnlIntermediary.Font.Color := ClWindowText;
       PnlPartner.Color           := $00834802;
       PnlPartner.Font.Color      := ClWhite;
     end;
  end;
end;

procedure TLoginFrm.EditLoginKeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key) = VK_RETURN
  then begin
         Key := #0; // prevent beeping
         BtnLoginClick(Sender);
       end;
end;

procedure TLoginFrm.GroupBoxLanguageClick(Sender: TObject);
begin
  UniMainModule.LanguageDispatcher.ActiveLanguage := GroupBoxLanguage.ItemIndex + 1;
end;

procedure TLoginFrm.LinkedLanguageLoginChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TLoginFrm.PnlCustomerClick(Sender: TObject);
begin
  Selectedportal := 0;
  Color_selected_portal;
end;

procedure TLoginFrm.PnlIntermediaryClick(Sender: TObject);
begin
  Selectedportal := 1;
  Color_selected_portal;
end;

procedure TLoginFrm.PnlPartnerClick(Sender: TObject);
begin
  Selectedportal := 2;
  Color_selected_portal;
end;

procedure TLoginFrm.Start_login_procedure;
var tempvalue: string;
begin
  try
    ImageLogo.Picture.Assign(UniMainModule.Portal_logo);
  except
  end;

  //Get the cookies if available
  tempvalue := UniApplication.Cookies.Values['_WavePortalLanguage'];
  if Trim(tempvalue) <> ''
  then begin
         if tempvalue = '0'
         then GroupBoxLanguage.ItemIndex := 0
         else if tempvalue = '1'
              then GroupBoxLanguage.ItemIndex := 1
              else if tempvalue = '2'
              then GroupBoxLanguage.ItemIndex := 2;
         GroupBoxLanguageClick(nil);
       end;

  if UniMainModule.portal_only_contributors = False
  then begin
         tempvalue := UniApplication.Cookies.Values['_WavePortalType'];
         if Trim(Tempvalue) <> ''
         then begin
                if tempvalue = '0'
                then begin
                       SelectedPortal           := 0;
                       Color_selected_portal;
                     end
                else if tempvalue = '1'
                     then begin
                            SelectedPortal           := 1;
                            Color_selected_portal;
                          end
                     else if tempvalue = '2'
                          then begin
                                 SelectedPortal := 2;
                                 Color_selected_portal;
                               end;
              end
         else begin
                SelectedPortal := 0;
                Color_selected_portal;
              end;
       end;

  tempvalue := UniApplication.Cookies.Values['_WavePortalLogin'];
  if Trim(Tempvalue) <> ''
  then begin
         EditLogin.Text := TempValue;
         EditPassWord.SetFocus;
       end
  else EditLogin.SetFocus;
end;

procedure TLoginFrm.UniFrameCreate(Sender: TObject);
begin
  SmsCode               := 0;
  SmsValidation         := False;
  SmsValidationRequired := False;
  SmsValidationOverride := False;
  LblCode.Visible       := False;
  EditCode.Visible      := False;
  UpdateStrings;

  if UniMainModule.portal_only_contributors = True
  then begin
         ImageLogin.Visible := False;
         LblAccountType.Visible := False;
         PnlCustomer.Visible := False;
         PnlIntermediary.Visible := False;
         PnlPartner.Visible := False;

         LblWelcome2.Left := 206;
         LblWelcome3.Left := 206;
         GroupBoxLanguage.Left := 206;
         LblLogin.Left := 206;
         EditLogin.Left := 342;
         LblPassword.Left := 206;
         EditPassWord.Left := 342;
         LblCode.Left := 206;
         EditCode.Left := 342;
         BtnLogin.Left := 350;
         BtnCancel.Left := 486;
         LblReference.Left := 350;


         Selectedportal := 1;
         Color_selected_portal;
       end;
end;

procedure TLoginFrm.UpdateStrings;
begin
  str_login_code_send_to       := LinkedLanguageLogin.GetTextOrDefault('strstr_login_code_send_to' (* 'Login code verstuurd naar ' *) );
  str_sending_code_to          := LinkedLanguageLogin.GetTextOrDefault('strstr_sending_code_to' (* 'Versturing login code naar ' *) );
  str_error_sending_code       := LinkedLanguageLogin.GetTextOrDefault('strstr_error_sending_code' (* 'Validatie code kon niet worden verstuurd, contacteer uw administrator om uw mobiel nummer te controleren in uw profiel' *) );
  str_controle_code            := LinkedLanguageLogin.GetTextOrDefault('strstr_controle_code' (* 'Controle' *) );
  str_validation_not_possible  := LinkedLanguageLogin.GetTextOrDefault('strstr_validation_not_possible' (* 'Controle niet mogelijk, mobiel nummer vereist, gelieve uw administrator te contacteren' *) );
  str_code_not_valid           := LinkedLanguageLogin.GetTextOrDefault('strstr_code_not_valid' (* 'Code niet geldig' *) );
  MsgInvalidLogin              := LinkedLanguageLogin.GetTextOrDefault('strMsgInvalidLogin' (* 'Ongeldige login.' *) );
  MsgErrorWritingLoginData     := LinkedLanguageLogin.GetTextOrDefault('strMsgErrorWritingLoginData' (* 'Fout bij wegschrijven login gegevens.' *) );
  MsgErrorConnectingToAdminDb  := LinkedLanguageLogin.GetTextOrDefault('strMsgErrorConnectingToAdminDb' (* 'Fout bij het verbinden aan de admin database' *) );
  MsgPasswordNotCorrect        := LinkedLanguageLogin.GetTextOrDefault('strMsgPasswordNotCorrect' (* 'Het opgegeven wachtwoord is niet correct.' *) );
  MsgNoAccess                  := LinkedLanguageLogin.GetTextOrDefault('strMsgNoAccess' (* 'U heeft geen toegang, neem contact op om toegang te krijgen.' *) );
  MsgPasswordRequired          := LinkedLanguageLogin.GetTextOrDefault('strMsgPasswordRequired' (* 'Uw wachtwoord is een verplichte ingave.' *) );
  MsgLoginRequired             := LinkedLanguageLogin.GetTextOrDefault('strMsgLoginRequired' (* 'Uw login is een verplichte ingave.' *) );
end;

end.
