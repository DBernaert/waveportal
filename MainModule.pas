unit MainModule;

interface

uses
  uniGUIMainModule, SysUtils, Classes, DBAccess, IBC, Data.DB, MemDS, Graphics, uniGUIBaseClasses,
  uniGUIClasses, uniImageList, siComp;

  type
  TZipSelection = record
    selected: boolean;
    zipcode:  string;
    city:     string;
    multiple: boolean;
  end;

  type
  TUniMainModule = class(TUniGUIMainModule)
    DbModule: TIBCConnection;
    DBTrans: TIBCTransaction;
    UpdTrans: TIBCTransaction;
    QWork: TIBCQuery;
    CreditProposals_Edit: TIBCQuery;
    Ds_CreditProposals_Edit: TDataSource;
    UpdTr_CreditProposals_Edit: TIBCTransaction;
    CreditFiles_Edit: TIBCQuery;
    Ds_CreditFiles_Edit: TDataSource;
    UpdTr_CreditFiles_Edit: TIBCTransaction;
    Files_Archive_Edit: TIBCQuery;
    Ds_Files_Archive_Edit: TDataSource;
    UpdTr_Files_Archive_Edit: TIBCTransaction;
    CreditProposals_EditID: TLargeintField;
    CreditProposals_EditCOMPANYID: TLargeintField;
    CreditProposals_EditREMOVED: TWideStringField;
    CreditProposals_EditCREDIT_FILE: TLargeintField;
    CreditProposals_EditPROPOSAL_NUMBER: TIntegerField;
    CreditProposals_EditPROPOSAL_DEPOSIT_NUMBER: TIntegerField;
    CreditProposals_EditCREDIT_PROVIDER: TWideStringField;
    CreditProposals_EditTARIFICATION_NUMBER: TIntegerField;
    CreditProposals_EditTARIFICATION_DATE: TDateField;
    CreditProposals_EditPROSPECTUS_NUMBER: TIntegerField;
    CreditProposals_EditPROSPECTUS_DATE: TDateField;
    CreditProposals_EditQUOTITY: TIntegerField;
    CreditProposals_EditBASIC_INTEREST_RATE: TFloatField;
    CreditProposals_EditPRODUCT_REDUCTION: TFloatField;
    CreditProposals_EditQUALITY_REDUCTION: TFloatField;
    CreditProposals_EditEXTRA_REDUCTION: TFloatField;
    CreditProposals_EditQUOTITY_ALLOWANCE: TFloatField;
    CreditProposals_EditCREDIT_ALLOWANCE: TFloatField;
    CreditProposals_EditFORMULA: TWideStringField;
    CreditProposals_EditBORROWED_AMOUNT: TFloatField;
    CreditProposals_EditDURATION: TIntegerField;
    CreditProposals_EditSTART_REDEMPTION: TFloatField;
    CreditProposals_EditSTART_DEBT_BAL_INCL: TFloatField;
    CreditProposals_EditSTART_FIRE_INS_INCL: TFloatField;
    CreditProposals_EditMAX_REDEMPTION: TFloatField;
    CreditProposals_EditMAX_REDEMPT_DEBT_BAL_INCL: TFloatField;
    CreditProposals_EditMAX_REDEMPT_FIRE_INS_INCL: TFloatField;
    CreditProposals_EditTOTAL_INTEREST: TFloatField;
    CreditProposals_EditTOTAL_INTEREST_DEBT_BALANCE: TFloatField;
    CreditProposals_EditTOTAL_INTEREST_FIRE_INSURANCE: TFloatField;
    CreditProposals_EditGENERAL_TOTAL: TFloatField;
    CreditFile_Demander_Edit: TIBCQuery;
    Ds_CreditFile_Demander_Edit: TDataSource;
    UpdTr_CreditFile_Demander_Edit: TIBCTransaction;
    Contacts_Edit: TIBCQuery;
    Ds_Contacts_Edit: TDataSource;
    UpdTr_Contacts_Edit: TIBCTransaction;
    Accounts_Edit: TIBCQuery;
    Ds_Accounts_Edit: TDataSource;
    UpdTr_Accounts_Edit: TIBCTransaction;
    QWork2: TIBCQuery;
    Contributor_reportdata: TIBCQuery;
    Contributor_reportdataID: TLargeintField;
    Contributor_reportdataCOMPANYID: TLargeintField;
    Contributor_reportdataSALUTATION: TLargeintField;
    Contributor_reportdataLAST_NAME: TWideStringField;
    Contributor_reportdataFIRST_NAME: TWideStringField;
    Contributor_reportdataCOMPANY_NAME: TWideStringField;
    Contributor_reportdataTITLE: TLargeintField;
    Contributor_reportdataADDRESS_STREET: TWideStringField;
    Contributor_reportdataADDRESS_POSTAL_CODE: TWideStringField;
    Contributor_reportdataADDRESS_STATE: TWideStringField;
    Contributor_reportdataADDRESS_CITY: TWideStringField;
    Contributor_reportdataADDRESS_COUNTRY: TWideStringField;
    Contributor_reportdataPHONE_HOME: TWideStringField;
    Contributor_reportdataPHONE_MOBILE: TWideStringField;
    Contributor_reportdataPHONE_FAX: TWideStringField;
    Contributor_reportdataEMAIL: TWideStringField;
    Contributor_reportdataWEBSITE: TWideStringField;
    Contributor_reportdataVAT_NUMBER: TWideStringField;
    Contributor_reportdataBANK_ACCOUNT: TWideStringField;
    Contributor_reportdataIBAN_ACCOUNT: TWideStringField;
    Contributor_reportdataBIC_ACCOUNT: TWideStringField;
    Contributor_reportdataBANK_ACCOUNT2: TWideStringField;
    Contributor_reportdataIBAN_ACCOUNT2: TWideStringField;
    Contributor_reportdataBIC_ACCOUNT2: TWideStringField;
    Contributor_reportdataFSMA_NUMBER: TWideStringField;
    Contributor_reportdataLOGO: TBlobField;
    Contributor_reportdataLANGUAGE: TLargeintField;
    Contributor_reportdataREMOVED: TWideStringField;
    Contributor_reportdataREMARKS: TWideMemoField;
    Contributor_reportdataSPREADED_PAYMENT: TIntegerField;
    Contributor_reportdataPERCENTAGE_DIRECT: TFloatField;
    Contributor_reportdataNUMBER_OF_MONTHS_SPREAD: TIntegerField;
    Contributor_reportdataPLANNED_PAYMENT: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT1: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT2: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT3: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT4: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT5: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT6: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT7: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT8: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT9: TIntegerField;
    Contributor_reportdataMONTH_PLANNED_PAYMENT10: TIntegerField;
    Contributor_reportdataPERCENTAGE_PAYMENT1: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT2: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT3: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT4: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT5: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT6: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT7: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT8: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT9: TFloatField;
    Contributor_reportdataPERCENTAGE_PAYMENT10: TFloatField;
    Contributor_reportdataFIXED_AMOUNT: TIntegerField;
    Contributor_reportdataAMOUNT_FIXED_AMOUNT: TFloatField;
    Contributor_reportdataPORTAL_ACTIVE: TIntegerField;
    Contributor_reportdataPORTAL_PASSWORD: TWideStringField;
    Contributor_reportdataPORTAL_RIGHTS: TIntegerField;
    Contributor_reportdataBIRTHDATE: TDateField;
    Contributor_reportdataASSIGNED_USER_ID: TLargeintField;
    Contributor_reportdataCREATED_USER_ID: TLargeintField;
    Contributor_reportdataMODIFIED_USER_ID: TLargeintField;
    Contributor_reportdataDATE_ENTERED: TDateTimeField;
    Contributor_reportdataDATE_MODIFIED: TDateTimeField;
    Contributor_reportdetaildata: TIBCQuery;
    Contributor_reportdetaildataCONTRIBUTOR: TLargeintField;
    Contributor_reportdetaildataCREDIT_FILE: TLargeintField;
    Contributor_reportdetaildataAMOUNT: TFloatField;
    Contributor_reportdetaildataDATE_CREDIT_FINAL: TDateField;
    Contributor_reportdetaildataTOTAL_AMOUNT_CREDIT: TFloatField;
    Contributor_reportdetaildataPROPERTY_ADDRESS: TWideStringField;
    Contributor_reportdetaildataID: TLargeintField;
    Contributor_reportdetaildataDEMANDER_NAME: TWideStringField;
    DsContributor_reportdata: TDataSource;
    DsContributor_reportdetaildata: TDataSource;
    Contributor_reportdataUSE_COMPANY_DATA: TSmallintField;
    DbAdmin: TIBCConnection;
    ActivityLog: TIBCQuery;
    UpdActivityLog: TIBCTransaction;
    AdminTrans: TIBCTransaction;
    QWorkAdmin: TIBCQuery;
    LanguageManager: TsiLang;
    LanguageDispatcher: TsiLangDispatcher;
    QBatch: TIBCQuery;
    UpdTr_Batch: TIBCTransaction;
    Log: TIBCQuery;
    ImageList: TUniNativeImageList;
    SmsLog: TIBCQuery;
    UpdTrSmsLog: TIBCTransaction;
    Contributor_reportdetaildataFREE_DESCRIPTION: TWideStringField;
    Contributor_reportdetaildataFINANCIALINSTNAME: TWideStringField;
    TblDocNumbersFree: TIBCQuery;
    TblDocSequences: TIBCQuery;
    ImageButtons: TUniNativeImageList;
    QUpdate: TIBCQuery;
    Contributor_reportdetaildataWITHHOLD_COMMISSION: TSmallintField;
    QWork4: TIBCQuery;
    QBatch2: TIBCQuery;
    QWork5: TIBCQuery;
    procedure UniGUIMainModuleCreate(Sender: TObject);
    procedure UniGUIMainModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Result_zipSelection:                  TZipSelection;
    Company_id:                           integer;
    Company_name:                         string;
    Company_account_name:                 string;
    Company_account_token:                string;
    Company_account_connectionstring:     string;
    Database_type:                        integer;
    Portal_name:                          string;
    Portal_title_dutch:                   string;
    Portal_title_french:                  string;
    Portal_title_english:                 string;
    Portal_return_url:                    string;
    Portal_active_contributors:           boolean;
    Portal_active_contacts:               boolean;
    Portal_logo:                          TPicture;
    Portal_2_factor:                      boolean;
    portal_contr_credit_module_active:    boolean;
    portal_contr_insurance_module_active: boolean;
    portal_contr_show_news:               boolean;
    portal_contr_show_agenda:             boolean;
    portal_contr_show_tasks:              boolean;
    portal_contr_show_phone_calls:        boolean;
    portal_contr_show_doc_archive:        boolean;
    portal_contr_show_all_commission:     boolean;
    portal_contacts_privacy_doc:          boolean;
    portal_theme_color:                   longint;
    portal_only_contributors:             boolean;
    User_type:                            integer;
    User_id:                              longint;
    User_partner_id:                      longint;
    Reference_contributor_id:             longint;
    Ok_sign_authorization:                string;
    Hypotheekwereld:                      boolean;
    Max_percentage_comm_portal:           currency;
    Statements3Months:                    boolean;

    Contributor_collaboration_stopped:    boolean;
    Responsable_contributor_credit:       longint;

    User_name:                            string;
    User_phone_mobile:                    string;
    User_email:                           string;
    User_portal_rights:                   integer;
    User_allow_credit_edit:               boolean;
    User_portal_privacy_doc_signed:       boolean;

    Show_commissions_contributor:         boolean;
    Show_documents_contributor:           boolean;
    Credit_quotity_required:              boolean;
    Credit_date_submission_required:      boolean;
    Result_dbAction:                      longint;
    Result_dbAction_string:               string;
    Root_archive:                         string;
    Template_archive:                     string;
    Default_template_archive:             string;

    Color_green:                          integer;
    Color_blue:                           integer;
    Color_red:                            integer;
    Color_yellow:                         integer;
    Color_darkgray:                 integer;

    procedure Log_activity(Description: string);
    function  Log_sms(Receivers: string; SmsMessage: string): boolean;

    function Get_pdf_url(OriginalFile, TargetFile: string): string;

    function  Get_document_sequence(Journal: integer): longint;

    function  Search_city(zipcode: string): TZipSelection;
    function  Search_zipcode(city: string): TZipSelection;

    function  ExecuteBatch(Command: string): boolean;
  end;

function UniMainModule: TUniMainModule;

implementation

{$R *.dfm}

uses
  UniGUIVars, ServerModule, uniGUIApplication, Windows;

function UniMainModule: TUniMainModule;
begin
  Result := TUniMainModule(UniApplication.UniMainModule)
end;

function TUniMainModule.ExecuteBatch(Command: string): boolean;
begin
  QBatch.Close;
  QBatch.SQL.Clear;
  QBatch.SQL.Add(Command);
  Try
    QBatch.ExecSQL;
    if UpdTr_Batch.Active
    then UpdTr_Batch.Commit;
    QBatch.Close;
    Result := True;
  Except
    if UpdTr_Batch.Active
    then UpdTr_Batch.Rollback;
    QBatch.Close;
    Result := False;
  End;
end;

function TUniMainModule.Get_document_sequence(Journal: integer): longint;
var NextNumber: longint;
    I:          integer;
begin
  TblDocNumbersFree.Close;
  TblDocNumbersFree.SQL.Clear;
  TblDocNumbersFree.SQL.Add('Select * from doc_numbers_free where companyId=' + IntToStr(Company_id) + ' ' +
                            'and journal=' + IntToStr(Journal) + ' ' +
                            'order by free_number');
  TblDocNumbersFree.Open;
  if TblDocNumbersFree.RecordCount < 10
  then begin
         TblDocSequences.Close;
         TblDocSequences.SQL.Clear;
         TblDocSequences.SQL.Add('Select * from doc_sequences where companyId=' + IntToStr(Company_id) + ' ' +
                                 'and Journal=' + IntToStr(Journal));
         TblDocSequences.Open;
         if TblDocSequences.RecordCount = 0
         then begin
                TblDocSequences.Close;
                Result := 0;
                Exit;
              end;

         NextNumber := TblDocSequences.FieldByName('Doc_Sequence').AsInteger;
         for I := 0 to 20
         do begin
              TblDocNumbersFree.Append;
              TblDocNumbersFree.FieldByName('CompanyId').AsInteger   := Company_id;
              TblDocNumbersFree.FieldByName('Journal').AsInteger     := Journal;
              TblDocNumbersFree.FieldByName('Free_number').AsInteger := NextNumber;
              TblDocNumbersFree.Post;
              Inc(NextNumber);
            end;
         TblDocSequences.Edit;
         TblDocSequences.FieldByName('Doc_Sequence').AsInteger := NextNumber;
         TblDocSequences.Post;
       end;
  TblDocNumbersFree.First;
  Result := TblDocNumbersFree.FieldByName('Free_number').AsInteger;
  TblDocNumbersFree.Delete;
end;

function TUniMainModule.Get_pdf_url(OriginalFile, TargetFile: string): string;
var AUrl: string;
begin
  Result := UniServerModule.NewCacheFileUrl(False, '', TargetFile, '', AUrl, True);
  if CopyFile(PChar(OriginalFile), PChar(Result), False) = false
  then Result := ''
  else Result := AUrl;
end;

procedure TUniMainModule.Log_activity(Description: string);
begin
  Log.Close;
  Log.SQL.Clear;
  Log.SQL.Add('Select first 0 * from sys_portal_activity');
  Log.Open;
  Log.Append;
  Log.FieldByName('CompanyId').AsInteger        := Company_id;
  Log.FieldByName('Account_Type').AsInteger     := User_type;
  Log.FieldByName('Account_id').asInteger       := User_id;
  Log.FieldByName('Activity_Time').AsDateTime   := Now;
  Log.Post;
  Try
    if UpdTrans.Active
    then UpdTrans.Commit;
    Log.Close;
  Except
    if UpdTrans.Active
    then UpdTrans.Rollback;
    Log.Close;
  End;
end;

function TUniMainModule.Log_sms(Receivers, SmsMessage: string): boolean;
begin
  Try
    DbAdmin.Connected := True;
    SmsLog.Close;
    SmsLog.SQL.Clear;
    SmsLog.SQL.Add('Select first 0 * from SmsLog');
    SmsLog.Open;
    SmsLog.Append;
    SmsLog.FieldByName('Product').AsInteger      := 4;
    SmsLog.FieldByName('AccountName').AsString   := Company_account_name;
    SmsLog.FieldByName('CompanyId').AsInteger    := Company_id;
    SmsLog.FieldByName('SmsTime').AsDateTime     := Now;
    SmsLog.FieldByName('Invoiced').AsInteger     := 0;
    SmsLog.FieldByName('Receivers').AsString     := Receivers;
    SmsLog.FieldByName('SmsMessage').AsString    := SmsMessage;
    SmsLog.Post;
    if UpdTrSmsLog.Active
    then UpdTrSmsLog.Commit;
    SmsLog.Close;
    DbAdmin.Connected := False;
    Result := True;
  Except
    if UpdTrSmsLog.Active
    then UpdTrSmsLog.Rollback;
    SmsLog.Close;
    DbAdmin.Connected := False;
    Result := False;
  End;
end;

function TUniMainModule.Search_city(zipcode: string): TZipSelection;
begin
  QWork.Close;
  QWork.SQL.Clear;
  QWork.SQL.Add('Select * from zipcodes where companyid=' + IntToStr(Company_Id) +
                ' and UPPER(Zipcode) like ' + QuotedStr(UpperCase(zipcode) + '%'));
  QWork.Open;
  if QWork.RecordCount = 1
  then begin
         result.selected := true;
         result.zipcode  := QWork.FieldByName('ZipCode').AsString;
         result.city     := QWork.FieldByName('City').AsString;
         result.multiple := false;
       end
  else if QWork.RecordCount > 1
       then begin
              result.selected := false;
              result.zipcode  := '';
              result.city     := '';
              result.multiple := true;
            end
       else begin
              result.selected := false;
              result.zipcode  := '';
              result.city     := '';
              result.multiple := false;
            end;
  QWork.Close;
end;

function TUniMainModule.Search_zipcode(city: string): TZipSelection;
begin
  QWork.Close;
  QWork.SQL.Clear;
  QWork.SQL.Add('Select * from zipcodes where companyid=' + IntToStr(Company_Id) +
                ' and UPPER(City) like ' + QuotedStr('%' + UpperCase(city) + '%'));
  QWork.Open;
  if QWork.RecordCount = 1
  then begin
         result.selected := true;
         result.zipcode  := QWork.FieldByName('ZipCode').AsString;
         result.city     := QWork.FieldByName('City').AsString;
         result.multiple := false;
       end
  else if QWork.RecordCount > 1
       then begin
              result.selected := false;
              result.zipcode  := '';
              result.city     := '';
              result.multiple := true;
            end
       else begin
              result.selected := false;
              result.zipcode  := '';
              result.city     := '';
              result.multiple := false;
            end;
  QWork.Close;
end;

procedure TUniMainModule.UniGUIMainModuleCreate(Sender: TObject);
begin
  Portal_logo              := TPicture.Create;
  Root_archive             := 'D:\DocumentArchive\WaveDesk\';
  Template_archive         := 'C:\TemplateArchive\WaveDesk\';
  Default_Template_archive := 'C:\DefaultTemplateArchive\';
  Color_green              := $00449D44;
  Color_blue               := $00EA1B15;
  Color_red                := $002D19E3;
  Color_yellow             := $001F97EC;
  Color_darkgray           := $003E322D;
end;

procedure TUniMainModule.UniGUIMainModuleDestroy(Sender: TObject);
begin
  Portal_logo.Free;
end;

initialization
  RegisterMainModuleClass(TUniMainModule);
end.
