unit report_PremiumsPayments;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.DB, MemDS,
  DBAccess, IBC,VCL.FlexCel.Core, FlexCel.XlsAdapter, VirtualTable, siComp,
  siLngLnk;

var
	str_year: string = 'Jaar'; // TSI: Localized (Don't modify!)
	str_month: string = 'Maand'; // TSI: Localized (Don't modify!)
	str_premium_due: string = 'Verschuldigde premie (�)'; // TSI: Localized (Don't modify!)
	str_open_amount: string = 'Openstaand bedrag (�)'; // TSI: Localized (Don't modify!)
	str_assigned_amount: string = 'Toegewezen bedrag (�)'; // TSI: Localized (Don't modify!)
	str_date_payment: string = 'Datum betaling'; // TSI: Localized (Don't modify!)
	str_amount: string = 'Bedrag (�)'; // TSI: Localized (Don't modify!)
	str_assigned_amount2: string = 'Toegewezen bedrag (�)'; // TSI: Localized (Don't modify!)
	str_payment_method: string = 'Betaalwijze'; // TSI: Localized (Don't modify!)
	str_sepa_batch_number: string = 'SEPA batch nr'; // TSI: Localized (Don't modify!)
	str_comment: string = 'Commentaar'; // TSI: Localized (Don't modify!)
	str_payment: string = 'Betaling'; // TSI: Localized (Don't modify!)
	str_balance_payment: string = 'saldo betaling'; // TSI: Localized (Don't modify!)
	str_refund: string = 'terugbetaling'; // TSI: Localized (Don't modify!)
	str_balance_refund: string = 'saldo terugbetaling'; // TSI: Localized (Don't modify!)

 type
    TExcelDataRow = record
    Year: Integer;
    Month: integer;
  end;

type
  TReportPremiumsPayments = class(TDataModule)
    qryExportExcel: TIBCQuery;
    qryExportExcelINSURANCE_FILE: TLargeintField;
    qryExportExcelPERIOD_YEAR: TIntegerField;
    qryExportExcelPERIOD_MONTH: TIntegerField;
    qryExportExcelDUE_AMOUNT: TFloatField;
    qryExportExcelOPEN_AMOUNT: TFloatField;
    qryExportExcelASSIGNED_AMOUNT: TFloatField;
    qryExportExcelID: TLargeintField;
    qryExportExcelPAYMENT_DATE: TDateField;
    qryExportExcelPAYMENT_AMOUNT: TFloatField;
    qryExportExcelAMOUNT_BOOKED: TFloatField;
    qryExportExcelPAYMENT_METHOD: TSmallintField;
    qryExportExcelSEPA_BATCH_REFERENCE: TWideStringField;
    qryExportExcelPAYMENT_COMMENT: TWideStringField;
    vtHelper: TVirtualTable;
    vtHelperpaymentID: TLargeintField;
    vtHelperfirstPaymentRowNr: TIntegerField;
    vtHelperpaymentAmount: TCurrencyField;
    LinkedLanguageReportPremiums: TsiLangLinked;
    procedure LinkedLanguageReportPremiumsChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure SetExcelHeaders (xls: TXlsFile; headers: array of string; colWidths: array of integer);
    procedure SetExcelCellBackground(xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; color: TExcelColor);
    procedure SetExcelCellFormat(xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; fontStyle: TFlxFontStyleSet; fontColor: TExcelColor);
    function GetPaymentComment(paymentAmount: Currency; isFirst: boolean): string;
  public
    function CreateExcelReportPremiumsAndPayments (insuranceFileId: int64): TXlsFile;
  end;

var
  ReportPremiumsPayments: TReportPremiumsPayments;

implementation

uses enums, MainModule;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

//Create an Excel report with the premiums for the given insurance file, matching
//the different payments received ('dubbel boekhouden')
function TReportPremiumsPayments.CreateExcelReportPremiumsAndPayments(insuranceFileId: int64): TXlsFile;
var
  xls: TXlsFile;
  curRowNr: Integer;
  curRow, prevRow: TExcelDataRow;
begin
  xls := TXlsFile.Create (1, true);

  //Set header columns
  SetExcelHeaders (xls, [str_year, str_month, str_premium_due, str_open_amount,
    str_assigned_amount, '', str_date_payment, str_amount, str_assigned_amount2,
    str_payment_method, str_sepa_batch_number, str_comment],
    [7, 7, 22, 22, 22, 25, 13, 12, 25, 20, 40, 75]);

  //Add data
  vtHelper.Open;
  qryExportExcel.close;
  qryExportExcel.ParamByName('insurance_file').AsLargeInt := insuranceFileId;
  qryExportExcel.open;

  try
    curRowNr := 2;
    while not(qryExportExcel.eof) do
    begin
      curRow.Year           := qryExportExcel.FieldByName ('period_year').AsInteger;
      curRow.Month          := qryExportExcel.FieldByName ('period_month').AsInteger;

      if (curRow.Year <> prevRow.Year) or (curRow.Month <> prevRow.Month) then
      begin
        xls.SetCellValue (curRowNr,  1, curRow.Year);
        xls.SetCellValue (curRowNr,  2, curRow.Month);
        xls.SetCellValue (curRowNr,  3, qryExportExcel.FieldByName ('due_amount').AsCurrency);
        xls.SetCellValue (curRowNr,  4, qryExportExcel.FieldByName ('open_amount').AsCurrency);
        xls.SetCellValue (curRowNr,  5, qryExportExcel.FieldByName ('assigned_amount').AsCurrency);
        SetExcelCellBackground(xls, curRowNr, 1, 5,  TUIColor.FromArgb ($ef, $f6, $ea)); //light green

        SetExcelCellFormat (xls, curRowNr, 1, 5, [TFlxFontStyles.Bold], TExcelColor.FromArgb(0, 0, 0)); //bold black
        if qryExportExcel.FieldByName ('open_amount').AsCurrency > 0 then
        begin
          SetExcelCellFormat (xls, curRowNr, 4, 4, [TFlxFontStyles.Bold], TExcelColor.FromArgb($fc, 0, 0)); //bold red
        end;

        curRowNr := curRowNr + 1;
      end;

      if not(qryExportExcel.FieldByName ('id').IsNull) then
      begin
        if vtHelper.Locate ('paymentId', qryExportExcel.FieldByName ('id').asLargeInt, []) then
        begin
          vtHelper.Edit;
          vtHelper.FieldByName ('lastPaymentRowNr').AsInteger := curRowNr;
          vtHelper.Post;
        end
        else begin
          vtHelper.Append;
          vtHelper.FieldByName ('paymentId').AsInteger        := qryExportExcel.FieldByName ('id').asLargeInt;
          vtHelper.FieldByName ('paymentAmount').AsCurrency   := qryExportExcel.FieldByName ('payment_amount').AsCurrency;
          vtHelper.FieldByName ('lastPaymentRowNr').AsInteger := curRowNr;
          vtHelper.Post;
        end;
      end;

      //by default all rows are 'saldo' -> afterwards we will correct for the first payments
      if not (qryExportExcel.FieldByName ('id').IsNull) then //when there is no payment for a month -> don't show the empty data
      begin
        xls.SetCellValue (curRowNr,  6, GetPaymentComment(qryExportExcel.FieldByName ('payment_amount').AsCurrency, false));
        SetExcelCellFormat (xls, curRowNr, 6, 12, [TFlxFontStyles.Italic], TExcelColor.FromArgb(180, 180, 180));

        xls.SetCellValue (curRowNr,  7, FormatDateTime('dd/mm/yyyy', qryExportExcel.FieldByName ('payment_date').AsDateTime));
        xls.SetCellValue (curRowNr,  8, qryExportExcel.FieldByName ('payment_amount').AsCurrency);
        xls.SetCellValue (curRowNr,  9, qryExportExcel.FieldByName ('amount_booked').AsCurrency);
        xls.SetCellValue (curRowNr, 10, PaymentMethodToText (SafePaymentMethodCast (qryExportExcel.FieldByName ('payment_method').AsInteger), nil));
        xls.SetCellValue (curRowNr, 11, qryExportExcel.FieldByName ('sepa_batch_reference').AsString);
        xls.SetCellValue (curRowNr, 12, qryExportExcel.FieldByName ('payment_comment').AsString);
      end;


      qryExportExcel.next;

      prevRow := curRow;
      curRowNr := curRowNr + 1;
    end;

    //Set 'betaling' or keep 'saldo' ?
    vtHelper.first;
    while not (vtHelper.Eof) do
    begin
      xls.SetCellValue (vtHelper.FieldByName ('lastPaymentRowNr').AsInteger, 6, GetPaymentComment(vtHelper.FieldByName('paymentAmount').AsCurrency, true));
      SetExcelCellFormat (xls, vtHelper.FieldByName ('lastPaymentRowNr').AsInteger, 6, 12, [], TExcelColor.FromArgb(0, 0, 0));

      vtHelper.Next;
    end;

  finally
    qryExportExcel.close;
  end;

  result := xls;
end;

procedure TReportPremiumsPayments.DataModuleCreate(Sender: TObject);
begin
  UpdateStrings;
end;

function TReportPremiumsPayments.GetPaymentComment (paymentAmount: Currency; isFirst: boolean): string;
begin
  if paymentAmount >= 0 then
  begin
    if isFirst then result := str_payment
    else result := str_balance_payment;
  end
  else begin
    if isFirst then result := str_refund
    else result := str_balance_refund;
  end;
end;

procedure TReportPremiumsPayments.LinkedLanguageReportPremiumsChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TReportPremiumsPayments.SetExcelCellFormat (xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; fontStyle: TFlxFontStyleSet; fontColor: TExcelColor);
var
  format: TFlxFormat;
  lcv: Integer;
begin
    for lcv := fromColNr to untilColNr do
    begin
      format := xls.GetFormat(xls.GetCellFormat(rowNr, lcv));
      format.Font.Style := fontStyle;
      format.Font.Color := fontColor;
      xls.SetCellFormat(rowNr, lcv, xls.AddFormat(format));
    end;
end;

procedure TReportPremiumsPayments.SetExcelCellBackground(xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; color: TExcelColor);
var
  format: TFlxFormat;
  lcv: Integer;
begin
    for lcv := fromColNr to untilColNr do
    begin
      format := xls.GetFormat(Xls.GetCellFormat(rowNr, lcv));
      format.FillPattern.Pattern := TFlxPatternStyle.Solid;
      format.FillPattern.FgColor := color;
      xls.SetCellFormat(rowNr, lcv, xls.AddFormat(format));
    end;
end;


procedure TReportPremiumsPayments.SetExcelHeaders(xls: TXlsFile; headers: array of string; colWidths: array of integer);
var
	lcv: Integer;
begin
	for lcv := 0 to high(headers) do
  begin
    xls.SetCellValue(1, lcv + 1, headers[lcv]);
    xls.SetColWidth(lcv + 1, lcv + 1, colWidths[lcv] * 250);
    SetExcelCellBackground(xls, 1, lcv + 1, lcv + 1, TUIColor.FromArgb ($a2, $cc, $86)); //dark green
  end;
end;

procedure TReportPremiumsPayments.UpdateStrings;
begin
  str_balance_refund := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_balance_refund' (* 'saldo terugbetaling' *) );
  str_refund := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_refund' (* 'terugbetaling' *) );
  str_balance_payment := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_balance_payment' (* 'saldo betaling' *) );
  str_payment := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_payment' (* 'Betaling' *) );
  str_comment := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_comment' (* 'Commentaar' *) );
  str_sepa_batch_number := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_sepa_batch_number' (* 'SEPA batch nr' *) );
  str_payment_method := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_payment_method' (* 'Betaalwijze' *) );
  str_assigned_amount2 := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_assigned_amount2' (* 'Toegewezen bedrag (�)' *) );
  str_amount := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_amount' (* 'Bedrag (�)' *) );
  str_date_payment := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_date_payment' (* 'Datum betaling' *) );
  str_assigned_amount := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_assigned_amount' (* 'Toegewezen bedrag (�)' *) );
  str_open_amount := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_open_amount' (* 'Openstaand bedrag (�)' *) );
  str_premium_due := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_premium_due' (* 'Verschuldigde premie (�)' *) );
  str_month := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_month' (* 'Maand' *) );
  str_year := LinkedLanguageReportPremiums.GetTextOrDefault('strstr_year' (* 'Jaar' *) );
end;

end.

