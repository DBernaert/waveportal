unit report_PremiumsPayments;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.DB, MemDS,
  DBAccess, IBC,VCL.FlexCel.Core, FlexCel.XlsAdapter, VirtualTable;

 type
    TExcelDataRow = record
    Year: Integer;
    Month: integer;
  end;

type
  TReportPremiumsPayments = class(TDataModule)
    qryExportExcel: TIBCQuery;
    qryExportExcelINSURANCE_FILE: TLargeintField;
    qryExportExcelPERIOD_YEAR: TIntegerField;
    qryExportExcelPERIOD_MONTH: TIntegerField;
    qryExportExcelDUE_AMOUNT: TFloatField;
    qryExportExcelOPEN_AMOUNT: TFloatField;
    qryExportExcelASSIGNED_AMOUNT: TFloatField;
    qryExportExcelID: TLargeintField;
    qryExportExcelPAYMENT_DATE: TDateField;
    qryExportExcelPAYMENT_AMOUNT: TFloatField;
    qryExportExcelAMOUNT_BOOKED: TFloatField;
    qryExportExcelPAYMENT_METHOD: TSmallintField;
    qryExportExcelSEPA_BATCH_REFERENCE: TWideStringField;
    qryExportExcelPAYMENT_COMMENT: TWideStringField;
    vtHelper: TVirtualTable;
    vtHelperpaymentID: TLargeintField;
    vtHelperfirstPaymentRowNr: TIntegerField;
    vtHelperpaymentAmount: TCurrencyField;
  private
    procedure SetExcelHeaders (xls: TXlsFile; headers: array of string; colWidths: array of integer);
    procedure SetExcelCellBackground(xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; color: TExcelColor);
    procedure SetExcelCellFormat(xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; fontStyle: TFlxFontStyleSet; fontColor: TExcelColor);
    function GetPaymentComment(paymentAmount: Currency; isFirst: boolean): string;
  public
    function CreateExcelReportPremiumsAndPayments (insuranceFileId: int64): TXlsFile;
  end;

var
  ReportPremiumsPayments: TReportPremiumsPayments;

implementation

uses enums;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

//Create an Excel report with the premiums for the given insurance file, matching
//the different payments received ('dubbel boekhouden')
function TReportPremiumsPayments.CreateExcelReportPremiumsAndPayments(insuranceFileId: int64): TXlsFile;
var
  xls: TXlsFile;
  fn: String;
  aUrl: string;
  curRowNr: Integer;
  curRow, prevRow: TExcelDataRow;
  format: TFlxFormat;
  firstPaymentRowNr: Integer;
begin
  xls := TXlsFile.Create (1, true);

  //Set header columns
  SetExcelHeaders (xls, ['Jaar', 'Maand', 'Verschuldigde premie (�)', 'Openstaand bedrag (�)',
    'Toegewezen bedrag (�)', '', 'Datum betaling', 'Bedrag (�)', 'Toegewezen bedrag (�)',
    'Betaalwijze', 'SEPA batch nr', 'Commentaar'],
    [7, 7, 22, 22, 22, 25, 13, 12, 25, 20, 40, 75]);

  //Add data
  vtHelper.Open;
  qryExportExcel.close;
  qryExportExcel.ParamByName('insurance_file').AsLargeInt := insuranceFileId;
  qryExportExcel.open;

  try
    curRowNr := 2;
    while not(qryExportExcel.eof) do
    begin
      curRow.Year           := qryExportExcel.FieldByName ('period_year').AsInteger;
      curRow.Month          := qryExportExcel.FieldByName ('period_month').AsInteger;

      if (curRow.Year <> prevRow.Year) or (curRow.Month <> prevRow.Month) then
      begin
        xls.SetCellValue (curRowNr,  1, curRow.Year);
        xls.SetCellValue (curRowNr,  2, curRow.Month);
        xls.SetCellValue (curRowNr,  3, qryExportExcel.FieldByName ('due_amount').AsCurrency);
        xls.SetCellValue (curRowNr,  4, qryExportExcel.FieldByName ('open_amount').AsCurrency);
        xls.SetCellValue (curRowNr,  5, qryExportExcel.FieldByName ('assigned_amount').AsCurrency);
        SetExcelCellBackground(xls, curRowNr, 1, 5,  TUIColor.FromArgb ($ef, $f6, $ea)); //light green

        SetExcelCellFormat (xls, curRowNr, 1, 5, [TFlxFontStyles.Bold], TExcelColor.FromArgb(0, 0, 0)); //bold black
        if qryExportExcel.FieldByName ('open_amount').AsCurrency > 0 then
        begin
          SetExcelCellFormat (xls, curRowNr, 4, 4, [TFlxFontStyles.Bold], TExcelColor.FromArgb($fc, 0, 0)); //bold red
        end;

        curRowNr := curRowNr + 1;
      end;

      if not(qryExportExcel.FieldByName ('id').IsNull) then
      begin
        if vtHelper.Locate ('paymentId', qryExportExcel.FieldByName ('id').asLargeInt, []) then
        begin
          vtHelper.Edit;
          vtHelper.FieldByName ('lastPaymentRowNr').AsInteger := curRowNr;
          vtHelper.Post;
        end
        else begin
          vtHelper.Append;
          vtHelper.FieldByName ('paymentId').AsInteger        := qryExportExcel.FieldByName ('id').asLargeInt;
          vtHelper.FieldByName ('paymentAmount').AsCurrency   := qryExportExcel.FieldByName ('payment_amount').AsCurrency;
          vtHelper.FieldByName ('lastPaymentRowNr').AsInteger := curRowNr;
          vtHelper.Post;
        end;
      end;

      //by default all rows are 'saldo' -> afterwards we will correct for the first payments
      if not (qryExportExcel.FieldByName ('id').IsNull) then //when there is no payment for a month -> don't show the empty data
      begin
        xls.SetCellValue (curRowNr,  6, GetPaymentComment(qryExportExcel.FieldByName ('payment_amount').AsCurrency, false));
        SetExcelCellFormat (xls, curRowNr, 6, 12, [TFlxFontStyles.Italic], TExcelColor.FromArgb(180, 180, 180));

        xls.SetCellValue (curRowNr,  7, FormatDateTime('dd/mm/yyyy', qryExportExcel.FieldByName ('payment_date').AsDateTime));
        xls.SetCellValue (curRowNr,  8, qryExportExcel.FieldByName ('payment_amount').AsCurrency);
        xls.SetCellValue (curRowNr,  9, qryExportExcel.FieldByName ('amount_booked').AsCurrency);
        xls.SetCellValue (curRowNr, 10, PaymentMethodToText (SafePaymentMethodCast (qryExportExcel.FieldByName ('payment_method').AsInteger), nil));
        xls.SetCellValue (curRowNr, 11, qryExportExcel.FieldByName ('sepa_batch_reference').AsString);
        xls.SetCellValue (curRowNr, 12, qryExportExcel.FieldByName ('payment_comment').AsString);
      end;


      qryExportExcel.next;

      prevRow := curRow;
      curRowNr := curRowNr + 1;
    end;

    //Set 'betaling' or keep 'saldo' ?
    vtHelper.first;
    while not (vtHelper.Eof) do
    begin
      xls.SetCellValue (vtHelper.FieldByName ('lastPaymentRowNr').AsInteger, 6, GetPaymentComment(vtHelper.FieldByName('paymentAmount').AsCurrency, true));
      SetExcelCellFormat (xls, vtHelper.FieldByName ('lastPaymentRowNr').AsInteger, 6, 12, [], TExcelColor.FromArgb(0, 0, 0));

      vtHelper.Next;
    end;

  finally
    qryExportExcel.close;
  end;

  result := xls;
end;

function TReportPremiumsPayments.GetPaymentComment (paymentAmount: Currency; isFirst: boolean): string;
begin
  if paymentAmount >= 0 then
  begin
    if isFirst then result := 'betaling'
    else result := 'saldo betaling';
  end
  else begin
    if isFirst then result := 'terugbetaling'
    else result := 'saldo terugbetaling';
  end;
end;

procedure TReportPremiumsPayments.SetExcelCellFormat (xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; fontStyle: TFlxFontStyleSet; fontColor: TExcelColor);
var
  format: TFlxFormat;
  lcv: Integer;
begin
    for lcv := fromColNr to untilColNr do
    begin
      format := xls.GetFormat(xls.GetCellFormat(rowNr, lcv));
      format.Font.Style := fontStyle;
      format.Font.Color := fontColor;
      xls.SetCellFormat(rowNr, lcv, xls.AddFormat(format));
    end;
end;

procedure TReportPremiumsPayments.SetExcelCellBackground(xls: TXlsFile; rowNr, fromColNr, untilColNr: integer; color: TExcelColor);
var
  format: TFlxFormat;
  lcv: Integer;
begin
    for lcv := fromColNr to untilColNr do
    begin
      format := xls.GetFormat(Xls.GetCellFormat(rowNr, lcv));
      format.FillPattern.Pattern := TFlxPatternStyle.Solid;
      format.FillPattern.FgColor := color;
      xls.SetCellFormat(rowNr, lcv, xls.AddFormat(format));
    end;
end;


procedure TReportPremiumsPayments.SetExcelHeaders(xls: TXlsFile; headers: array of string; colWidths: array of integer);
var
	lcv: Integer;
begin
	for lcv := 0 to high(headers) do
  begin
    xls.SetCellValue(1, lcv + 1, headers[lcv]);
    xls.SetColWidth(lcv + 1, lcv + 1, colWidths[lcv] * 250);
    SetExcelCellBackground(xls, 1, lcv + 1, lcv + 1, TUIColor.FromArgb ($a2, $cc, $86)); //dark green
  end;
end;

end.
