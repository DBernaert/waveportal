object ReportRepositoryFrm: TReportRepositoryFrm
  Height = 360
  Width = 567
  object ReportBorderellen: TfrxReport
    Version = '2022.2.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var withhold_commission: boolean;'
      
        '    withhold_commission_percentage: real;                       ' +
        '                                                                ' +
        '              '
      '  '
      
        'procedure frxAgentsAGENTNAMEOnBeforePrint(Sender: TfrxComponent)' +
        ';'
      'begin'
      '  if <frxAgents."USE_COMPANY_DATA"> = 1'
      '  then frxAgentsAGENTNAME.Text := <frxAgents."COMPANY_NAME">'
      
        '  else frxAgentsAGENTNAME.Text := <frxAgents."LAST_NAME"> + '#39' '#39' ' +
        '+ <frxAgents."FIRST_NAME">;  '
      'end;'
      ''
      'procedure DetailData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Trim(<frxCommission."FREE_DESCRIPTION">) <> '#39#39
      '  then begin'
      '         Memo23.Visible   := False;'
      '         Line2.Visible    := False;'
      '        // Memo28.Left      := 0.20;'
      '         Memo28.DataField := '#39'FREE_DESCRIPTION'#39';  '
      '         Line4.Visible    := False;'
      
        '         Line9.Visible    := False;                             ' +
        '                                             '
      '       end'
      '  else begin'
      '         Memo23.Visible   := True;'
      '         Line2.Visible    := True;'
      '         //Memo28.Left      := 2.40;'
      
        '         Memo28.DataField := '#39'DEMANDER_NAME'#39';                   ' +
        '                                                                ' +
        '    '
      '         Line4.Visible    := True;'
      
        '         Line9.Visible    := True;                              ' +
        '                                          '
      '       end;            '
      'end;'
      ''
      'procedure ReportBorderellenOnStartReport(Sender: TfrxComponent);'
      'begin'
      '  withhold_commission := false;'
      
        '  withhold_commission_percentage := 100;                        ' +
        '                                                          '
      '  QWork.Close;'
      
        '  QWork.SQL.Text := '#39'Select withhold_commission, withhold_commis' +
        'sion_percentage from companyprofile'#39';'
      '  QWork.Open;'
      ''
      '  if Qwork.Recordcount > 0'
      '  then begin'
      
        '         if Qwork.FieldByName('#39'Withhold_commission'#39').AsInteger =' +
        ' 1'
      '         then begin'
      '                Withhold_commission := True;'
      
        '                Withhold_commission_percentage := QWork.FieldByN' +
        'ame('#39'Withhold_commission_percentage'#39').AsCurrency;'
      
        '                Withhold_commission_percentage := 100 - Withhold' +
        '_commission_percentage;'
      
        '                LblLegendWithhold.Text := '#39'(*) '#39' + FormatFloat('#39 +
        '#,##0.00 %'#39', Withhold_commission_percentage);'
      
        '                LblLegendWithhold.Visible := True;              ' +
        '                                                                ' +
        '          '
      '              end'
      '         else begin'
      '                Withhold_commission := False;'
      '                Withhold_commission_percentage := 100;'
      
        '                LblLegendWithhold.Visible := False;             ' +
        '                                                                ' +
        '                                     '
      '              end;                   '
      '       end;'
      '  Qwork.Close;  '
      'end;'
      ''
      'procedure Memo19OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if withhold_commission = True'
      '  then begin                         '
      '         if <frxCommission."Withhold_commission"> = 1'
      
        '         then Memo19.Text := '#39'(*) '#39' + FormatFloat('#39'#,##0.00 '#8364#39', ' +
        '<frxCommission."Amount">)                                       ' +
        '                                                                ' +
        '                                                                ' +
        '       '
      
        '         else Memo19.Text := FormatFloat('#39'#,##0.00 '#8364#39', <frxCommi' +
        'ssion."Amount">);'
      '      end'
      
        '  else Memo19.Text := FormatFloat('#39'#,##0.00 '#8364#39', <frxCommission."' +
        'Amount">);  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnStartReport = 'ReportBorderellenOnStartReport'
    Left = 72
    Top = 80
    Datasets = <
      item
        DataSet = frxAgents
        DataSetName = 'frxAgents'
      end
      item
        DataSet = frxCommission
        DataSetName = 'frxCommission'
      end
      item
        DataSet = ReportBorderellen.QWork
        DataSetName = 'QWork'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
      object QWork: TfrxIBDACQuery
        UserName = 'QWork'
        CloseDataSource = True
        BCDToCurrency = False
        DataSetOptions = []
        IgnoreDupParams = False
        Params = <>
        pLeft = 448
        pTop = 144
        Parameters = <>
      end
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object DataAgent: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 181.417440000000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        DataSet = frxAgents
        DataSetName = 'frxAgents'
        PrintIfDetailEmpty = True
        RowCount = 0
        StartNewPage = True
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 11.338590000000000000
          Width = 419.527830000000000000
          Height = 102.047310000000000000
          Frame.Color = clWhite
          Frame.Typ = []
        end
        object LblReportTile: TfrxMemoView
          AllowVectorExport = True
          Top = 136.063080000000000000
          Width = 1009.134510000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 5455915
          HAlign = haCenter
          Memo.UTF8W = (
            'LblReportTile')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 132.283550000000000000
          Fill.BackColor = 5455915
          Frame.Color = 15000804
          Frame.Typ = []
        end
        object LblCompanyName: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 7.559060000000000000
          Width = 415.748300000000000000
          Height = 26.456710000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCompanyName')
          ParentFont = False
          VAlign = vaCenter
        end
        object LblCompanyAddress1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCompanyAddress1')
          ParentFont = False
          VAlign = vaCenter
        end
        object LblCompanyAddress2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 52.913420000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCompanyAddress2')
          ParentFont = False
          VAlign = vaCenter
        end
        object LblCompanyContact: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 71.811070000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCompanyContact')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          Fill.BackColor = 5455915
          Frame.Color = 5455915
          Frame.Typ = []
        end
        object LblDateCredit: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 162.519790000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Datum akte')
          ParentFont = False
        end
        object LblCustomer: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 162.519790000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Klant')
          ParentFont = False
        end
        object LblLocation: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 162.519790000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Aankoop')
          ParentFont = False
        end
        object LblCreditAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 782.362710000000000000
          Top = 162.519790000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Kredietbedrag')
          ParentFont = False
        end
        object LblCommissionAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 891.969080000000000000
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Commissie')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          DataField = 'ADDRESS_STREET'
          DataSet = frxAgents
          DataSetName = 'frxAgents'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxAgents."ADDRESS_STREET"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 653.858690000000000000
          Top = 60.472480000000000000
          Width = 328.819110000000000000
          Height = 18.897650000000000000
          DataField = 'ADDRESS_CITY'
          DataSet = frxAgents
          DataSetName = 'frxAgents'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxAgents."ADDRESS_CITY"]')
          ParentFont = False
        end
        object LblFsmaNumber: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 90.708720000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblFsmaNumber')
          ParentFont = False
          VAlign = vaCenter
        end
        object LblCompanyNumber: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 109.606370000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCompanyNumber')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 83.149660000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          DataField = 'VAT_NUMBER'
          DataSet = frxAgents
          DataSetName = 'frxAgents'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxAgents."VAT_NUMBER"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 60.472480000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DataField = 'ADDRESS_POSTAL_CODE'
          DataSet = frxAgents
          DataSetName = 'frxAgents'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxAgents."ADDRESS_POSTAL_CODE"]')
          ParentFont = False
        end
        object frxAgentsAGENTNAME: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 18.897650000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'frxAgentsAGENTNAMEOnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxAgents."ID"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 653.858690000000000000
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Fin. instelling')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.763779530000000000
        Top = 222.992270000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'DetailData1OnBeforePrint'
        DataSet = frxCommission
        DataSetName = 'frxCommission'
        RowCount = 0
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'DATE_CREDIT_FINAL'
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxCommission."DATE_CREDIT_FINAL"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 238.110390000000000000
          Height = 15.118120000000000000
          DataField = 'DEMANDER_NAME'
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxCommission."DEMANDER_NAME"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 782.362710000000000000
          Width = 98.267716540000000000
          Height = 15.118120000000000000
          DataField = 'TOTAL_AMOUNT_CREDIT'
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxCommission."TOTAL_AMOUNT_CREDIT"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Height = 17.763779530000000000
          Color = clSilver
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Height = 17.763779530000000000
          Color = 5455915
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Height = 17.763779530000000000
          Color = clSilver
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Line6: TfrxLineView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Height = 17.763779530000000000
          Color = clSilver
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Line7: TfrxLineView
          AllowVectorExport = True
          Left = 1009.134510000000000000
          Height = 17.763779530000000000
          Color = clSilver
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 309.921460000000000000
          Height = 15.118120000000000000
          DataField = 'PROPERTY_ADDRESS'
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxCommission."PROPERTY_ADDRESS"]')
          ParentFont = False
        end
        object Line8: TfrxLineView
          AllowVectorExport = True
          Left = 650.079160000000000000
          Height = 17.763779530000000000
          Color = clSilver
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 653.858690000000000000
          Width = 120.944960000000000000
          Height = 15.118120000000000000
          DataField = 'FINANCIALINSTNAME'
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxCommission."FINANCIALINSTNAME"]')
          ParentFont = False
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Height = 17.763779530000000000
          Color = clSilver
          Frame.Color = 5455915
          Frame.Typ = []
          Diagonal = True
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 891.969080000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'Memo19OnBeforePrint'
          DataSet = frxCommission
          DataSetName = 'frxCommission'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 264.567100000000000000
        Width = 1009.134510000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 30.236240000000000000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          Memo.UTF8W = (
            'Created with WAVEDESK - www.wavedesk.be')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 26.456710000000000000
          Fill.BackColor = 5455915
          Frame.Color = 5455915
          Frame.Typ = []
        end
        object LblTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 793.701300000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totaal:')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Left = 891.969080000000000000
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxCommission."AMOUNT">,DetailData1)]')
          ParentFont = False
        end
        object LblFreeVat: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 476.220780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Vrijgesteld van BTW ingevolge art. 44 '#167' 3 W.Btw.')
          ParentFont = False
        end
        object LblDocDate: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 30.236240000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
        end
        object LblLegendWithhold: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 30.236240000000000000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          HAlign = haRight
          ParentFont = False
        end
      end
    end
  end
  object PdfExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    EmbedFontsIfProtected = False
    InteractiveFormsFontSubset = 'A-Z,a-z,0-9,#43-#47 '
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Adm-Concept OneDesk Lite'
    Subject = 'WAVEDESK'
    Creator = 'FastReport'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = True
    PDFStandard = psPDFA_2a
    PDFVersion = pv17
    Left = 72
    Top = 16
  end
  object frxAgents: TfrxDBDataset
    UserName = 'frxAgents'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'COMPANYID=COMPANYID'
      'SALUTATION=SALUTATION'
      'LAST_NAME=LAST_NAME'
      'FIRST_NAME=FIRST_NAME'
      'COMPANY_NAME=COMPANY_NAME'
      'TITLE=TITLE'
      'ADDRESS_STREET=ADDRESS_STREET'
      'ADDRESS_POSTAL_CODE=ADDRESS_POSTAL_CODE'
      'ADDRESS_STATE=ADDRESS_STATE'
      'ADDRESS_CITY=ADDRESS_CITY'
      'ADDRESS_COUNTRY=ADDRESS_COUNTRY'
      'PHONE_HOME=PHONE_HOME'
      'PHONE_MOBILE=PHONE_MOBILE'
      'PHONE_FAX=PHONE_FAX'
      'EMAIL=EMAIL'
      'WEBSITE=WEBSITE'
      'VAT_NUMBER=VAT_NUMBER'
      'BANK_ACCOUNT=BANK_ACCOUNT'
      'IBAN_ACCOUNT=IBAN_ACCOUNT'
      'BIC_ACCOUNT=BIC_ACCOUNT'
      'BANK_ACCOUNT2=BANK_ACCOUNT2'
      'IBAN_ACCOUNT2=IBAN_ACCOUNT2'
      'BIC_ACCOUNT2=BIC_ACCOUNT2'
      'FSMA_NUMBER=FSMA_NUMBER'
      'LOGO=LOGO'
      'LANGUAGE=LANGUAGE'
      'REMOVED=REMOVED'
      'REMARKS=REMARKS'
      'SPREADED_PAYMENT=SPREADED_PAYMENT'
      'PERCENTAGE_DIRECT=PERCENTAGE_DIRECT'
      'NUMBER_OF_MONTHS_SPREAD=NUMBER_OF_MONTHS_SPREAD'
      'PLANNED_PAYMENT=PLANNED_PAYMENT'
      'MONTH_PLANNED_PAYMENT1=MONTH_PLANNED_PAYMENT1'
      'MONTH_PLANNED_PAYMENT2=MONTH_PLANNED_PAYMENT2'
      'MONTH_PLANNED_PAYMENT3=MONTH_PLANNED_PAYMENT3'
      'MONTH_PLANNED_PAYMENT4=MONTH_PLANNED_PAYMENT4'
      'MONTH_PLANNED_PAYMENT5=MONTH_PLANNED_PAYMENT5'
      'MONTH_PLANNED_PAYMENT6=MONTH_PLANNED_PAYMENT6'
      'MONTH_PLANNED_PAYMENT7=MONTH_PLANNED_PAYMENT7'
      'MONTH_PLANNED_PAYMENT8=MONTH_PLANNED_PAYMENT8'
      'MONTH_PLANNED_PAYMENT9=MONTH_PLANNED_PAYMENT9'
      'MONTH_PLANNED_PAYMENT10=MONTH_PLANNED_PAYMENT10'
      'PERCENTAGE_PAYMENT1=PERCENTAGE_PAYMENT1'
      'PERCENTAGE_PAYMENT2=PERCENTAGE_PAYMENT2'
      'PERCENTAGE_PAYMENT3=PERCENTAGE_PAYMENT3'
      'PERCENTAGE_PAYMENT4=PERCENTAGE_PAYMENT4'
      'PERCENTAGE_PAYMENT5=PERCENTAGE_PAYMENT5'
      'PERCENTAGE_PAYMENT6=PERCENTAGE_PAYMENT6'
      'PERCENTAGE_PAYMENT7=PERCENTAGE_PAYMENT7'
      'PERCENTAGE_PAYMENT8=PERCENTAGE_PAYMENT8'
      'PERCENTAGE_PAYMENT9=PERCENTAGE_PAYMENT9'
      'PERCENTAGE_PAYMENT10=PERCENTAGE_PAYMENT10'
      'FIXED_AMOUNT=FIXED_AMOUNT'
      'AMOUNT_FIXED_AMOUNT=AMOUNT_FIXED_AMOUNT'
      'PORTAL_ACTIVE=PORTAL_ACTIVE'
      'PORTAL_PASSWORD=PORTAL_PASSWORD'
      'PORTAL_RIGHTS=PORTAL_RIGHTS'
      'BIRTHDATE=BIRTHDATE'
      'ASSIGNED_USER_ID=ASSIGNED_USER_ID'
      'CREATED_USER_ID=CREATED_USER_ID'
      'MODIFIED_USER_ID=MODIFIED_USER_ID'
      'DATE_ENTERED=DATE_ENTERED'
      'DATE_MODIFIED=DATE_MODIFIED'
      'USE_COMPANY_DATA=USE_COMPANY_DATA')
    DataSource = UniMainModule.DsContributor_reportdata
    BCDToCurrency = False
    DataSetOptions = []
    Left = 72
    Top = 136
  end
  object frxCommission: TfrxDBDataset
    UserName = 'frxCommission'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CONTRIBUTOR=CONTRIBUTOR'
      'CREDIT_FILE=CREDIT_FILE'
      'AMOUNT=AMOUNT'
      'DATE_CREDIT_FINAL=DATE_CREDIT_FINAL'
      'TOTAL_AMOUNT_CREDIT=TOTAL_AMOUNT_CREDIT'
      'PROPERTY_ADDRESS=PROPERTY_ADDRESS'
      'ID=ID'
      'DEMANDER_NAME=DEMANDER_NAME'
      'FREE_DESCRIPTION=FREE_DESCRIPTION'
      'FINANCIALINSTNAME=FINANCIALINSTNAME'
      'WITHHOLD_COMMISSION=WITHHOLD_COMMISSION')
    DataSource = UniMainModule.DsContributor_reportdetaildata
    BCDToCurrency = False
    DataSetOptions = []
    Left = 72
    Top = 192
  end
  object ReportsFiche28150: TfrxReport
    Version = '2022.2.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 224
    Top = 80
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Shape1: TfrxShapeView
        AllowVectorExport = True
        Width = 718.110700000000000000
        Height = 797.480830000000000000
        Frame.Typ = []
      end
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 3.779530000000000000
        Width = 710.551640000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'FICHE Nr. 281.50  (commissies, erelonen, enz.)')
        ParentFont = False
      end
      object Memo2: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 30.236240000000000000
        Width = 309.921460000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '1. Nr. .................... (overbrengen op 325.50)')
        ParentFont = False
      end
      object LblYear: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 52.913420000000000000
        Width = 94.488250000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '2. Jaar . . . .')
        ParentFont = False
      end
      object Line1: TfrxLineView
        AllowVectorExport = True
        Top = 26.456710000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Line2: TfrxLineView
        AllowVectorExport = True
        Top = 49.133890000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Line3: TfrxLineView
        AllowVectorExport = True
        Top = 71.811070000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo4: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 75.590600000000000000
        Width = 275.905690000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '3. Naam (of benaming) en adres van de schuldenaar ')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        AllowVectorExport = True
        Left = 15.118120000000000000
        Top = 90.708720000000000000
        Width = 264.567100000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'van de inkomsten :')
        ParentFont = False
      end
      object Line4: TfrxLineView
        AllowVectorExport = True
        Top = 385.512060000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Line5: TfrxLineView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 71.811070000000000000
        Height = 313.700990000000000000
        Color = clBlack
        Frame.Style = fsDash
        Frame.Typ = []
        Diagonal = True
      end
      object Memo6: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 75.590600000000000000
        Width = 370.393940000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'Naam, voornaam (of benaming) en adres van de verkrijger van de i' +
            'nkomsten :')
        ParentFont = False
      end
      object Line6: TfrxLineView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 306.141930000000000000
        Width = 423.307360000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo3: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 309.921460000000000000
        Width = 132.283550000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsItalic]
        Frame.Typ = []
        Memo.UTF8W = (
          'Facultatief in te vullen')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 328.819110000000000000
        Width = 396.850650000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'Nationaal nummer : .............................................' +
            '....................................................')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 347.716760000000000000
        Width = 396.850650000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'Buitenlands fiscaal identificatienummer : ......................' +
            '...........................................')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 362.834880000000000000
        Width = 396.850650000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'Geboortedatum : ................................................' +
            '......................................................')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 264.567100000000000000
        Width = 120.944960000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Ondernemingsnummer :'
          '.')
        ParentFont = False
      end
      object Memo11: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 283.464750000000000000
        Width = 396.850650000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'Uitgeoefend beroep : ...........................................' +
            '....................................................')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        AllowVectorExport = True
        Left = 15.118120000000000000
        Top = 264.567100000000000000
        Width = 166.299320000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Ondernemingsnr. of nationaal nr :')
        ParentFont = False
      end
      object Memo13: TfrxMemoView
        AllowVectorExport = True
        Top = 1031.811690000000000000
        Width = 154.960730000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          'Nr. 281.50  -  DTP  -  2011')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        AllowVectorExport = True
        Top = 941.102970000000000000
        Width = 718.110700000000000000
        Height = 83.149660000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          
            'betreffende commissies, makelaarslonen, handels- of andere resto' +
            'rno'#39's, toevallige of niet-toevallige vacatiegelden'
          
            'of erelonen,  gratificaties, vergoedingen of voordelen van alle ' +
            'aard die voor de verkrijgers al dan niet in Belgi'#235
          
            'belastbare beroepsinkomsten zijn, behoudens de bezoldigingen van' +
            ' meewerkende echtgenoten.'
          ''
          
            '(Model opgemaakt ter uitvoering van artikel 30 van het Koninklij' +
            'k besluit tot'
          'uitvoering van het Wetboek van de inkomstenbelastingen 1992)')
        ParentFont = False
      end
      object Memo15: TfrxMemoView
        AllowVectorExport = True
        Top = 910.866730000000000000
        Width = 718.110700000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Fiche nr. 281.50')
        ParentFont = False
      end
      object Line7: TfrxLineView
        AllowVectorExport = True
        Left = 317.480520000000000000
        Top = 907.087200000000000000
        Width = 86.929190000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo16: TfrxMemoView
        AllowVectorExport = True
        Top = 884.410020000000000000
        Width = 718.110700000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'INKOMSTENBELASTINGEN')
        ParentFont = False
      end
      object Line8: TfrxLineView
        AllowVectorExport = True
        Left = 317.480520000000000000
        Top = 880.630490000000000000
        Width = 86.929190000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo17: TfrxMemoView
        AllowVectorExport = True
        Top = 857.953310000000000000
        Width = 718.110700000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'ALGEMENE ADMINISTRATIE VAN DE FISCALITEIT ')
        ParentFont = False
      end
      object Line9: TfrxLineView
        AllowVectorExport = True
        Left = 317.480520000000000000
        Top = 854.173780000000000000
        Width = 86.929190000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo18: TfrxMemoView
        AllowVectorExport = True
        Top = 831.496600000000000000
        Width = 718.110700000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'FINANCIEN')
        ParentFont = False
      end
      object Memo19: TfrxMemoView
        AllowVectorExport = True
        Top = 808.819420000000000000
        Width = 718.110700000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Federale Overheidsdienst')
        ParentFont = False
      end
      object Memo20: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 771.024120000000000000
        Width = 710.551640000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            '5. Commentaar: .................................................' +
            '................................................................' +
            '................................................................' +
            '............................')
        ParentFont = False
      end
      object Line10: TfrxLineView
        AllowVectorExport = True
        Top = 755.906000000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo21: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 389.291590000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '4. Aard')
        ParentFont = False
      end
      object Memo22: TfrxMemoView
        AllowVectorExport = True
        Left = 548.031850000000000000
        Top = 389.291590000000000000
        Width = 166.299320000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Bedrag')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 419.527830000000000000
        Width = 321.260050000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'a) Commissies, makelaarslonen, handelsrestorno'#39's, enz. :')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 449.764070000000000000
        Width = 321.260050000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'b) Erelonen of vacantiegelden :')
        ParentFont = False
      end
      object Memo25: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 710.551640000000000000
        Width = 517.795610000000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'g) Indien het onder litt. e ingevulde bedrag niet overeenstemt m' +
            'et het bedrag dat in het in vak 2 vermelde'
          
            '    jaar werd uitbetaald, vermeld dan hiernaast het bedrag dat w' +
            'erkelijk in dat jaar is uitbetaald (met inbegrip'
          
            '    van de sommen die betrekking hebben op andere belastbare tij' +
            'dperken) :')
        ParentFont = False
      end
      object Line11: TfrxLineView
        AllowVectorExport = True
        Top = 699.213050000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Style = fsDash
        Frame.Typ = []
        Diagonal = True
      end
      object Line12: TfrxLineView
        AllowVectorExport = True
        Left = 544.252320000000000000
        Top = 385.512060000000000000
        Height = 370.393940000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Line13: TfrxLineView
        AllowVectorExport = True
        Top = 411.968770000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object Memo26: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 483.779840000000000000
        Width = 525.354670000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'c) Voordelen van alle aard (aard : .............................' +
            '................................................................' +
            '................. )  :')
        ParentFont = False
      end
      object Memo27: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 517.795610000000000000
        Width = 525.354670000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'd) Kosten gedaan voor rekening van de verkrijger :')
        ParentFont = False
      end
      object Memo28: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 555.590910000000000000
        Width = 525.354670000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'e) Totaal (zie ook litt. f en g) :')
        ParentFont = False
      end
      object Memo29: TfrxMemoView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 600.945270000000000000
        Width = 525.354670000000000000
        Height = 30.236240000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'f) Vermeld hier, in voorkomend geval, het in litt. e begrepen be' +
            'drag dat betrekking heeft op vergoedingen'
          '    uitgekeerd aan :')
        ParentFont = False
      end
      object Memo30: TfrxMemoView
        AllowVectorExport = True
        Left = 26.456710000000000000
        Top = 634.961040000000000000
        Width = 502.677490000000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '-  sportbeoefenaars :')
        ParentFont = False
      end
      object Memo31: TfrxMemoView
        AllowVectorExport = True
        Left = 26.456710000000000000
        Top = 668.976810000000000000
        Width = 502.677490000000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            '-  opleiders, trainers en begeleiders uit activiteiten ten behoe' +
            've van sportbeoefenaars :')
        ParentFont = False
      end
      object Shape2: TfrxShapeView
        AllowVectorExport = True
        Left = 551.811380000000000000
        Top = 721.890230000000000000
        Width = 158.740260000000000000
        Height = 26.456710000000000000
        Frame.Typ = []
      end
      object Shape3: TfrxShapeView
        AllowVectorExport = True
        Left = 551.811380000000000000
        Top = 661.417750000000000000
        Width = 158.740260000000000000
        Height = 26.456710000000000000
        Frame.Typ = []
      end
      object Shape4: TfrxShapeView
        AllowVectorExport = True
        Left = 551.811380000000000000
        Top = 627.401980000000000000
        Width = 158.740260000000000000
        Height = 26.456710000000000000
        Frame.Typ = []
      end
      object Line14: TfrxLineView
        AllowVectorExport = True
        Top = 585.827150000000000000
        Width = 718.110700000000000000
        Color = clBlack
        Frame.Style = fsDash
        Frame.Typ = []
        Diagonal = True
      end
      object Shape5: TfrxShapeView
        AllowVectorExport = True
        Left = 551.811380000000000000
        Top = 548.031850000000000000
        Width = 158.740260000000000000
        Height = 26.456710000000000000
        Frame.Typ = []
      end
      object Line15: TfrxLineView
        AllowVectorExport = True
        Left = 544.252320000000000000
        Top = 540.472790000000000000
        Width = 173.858380000000000000
        Color = clBlack
        Frame.Typ = []
        Diagonal = True
      end
      object LblCompanyName: TfrxMemoView
        AllowVectorExport = True
        Left = 49.133890000000000000
        Top = 132.283550000000000000
        Width = 241.889920000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object LblCompanyStreet: TfrxMemoView
        AllowVectorExport = True
        Left = 49.133890000000000000
        Top = 154.960730000000000000
        Width = 241.889920000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object LblCompanyStreet2: TfrxMemoView
        AllowVectorExport = True
        Left = 49.133890000000000000
        Top = 177.637910000000000000
        Width = 241.889920000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object LblCompanyNumber: TfrxMemoView
        AllowVectorExport = True
        Left = 181.417440000000000000
        Top = 264.567100000000000000
        Width = 109.606370000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object Memo36: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 631.181510000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object Memo37: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 665.197280000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object Memo38: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 725.669760000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 517.795610000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object Memo33: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 483.779840000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object Memo34: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 449.764070000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object LblAmount1: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 419.527830000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object LblAmount2: TfrxMemoView
        AllowVectorExport = True
        Left = 555.590910000000000000
        Top = 551.811380000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '0,00')
        ParentFont = False
      end
      object LblCustomerName: TfrxMemoView
        AllowVectorExport = True
        Left = 366.614410000000000000
        Top = 132.283550000000000000
        Width = 309.921460000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object LblCustomerStreet: TfrxMemoView
        AllowVectorExport = True
        Left = 366.614410000000000000
        Top = 154.960730000000000000
        Width = 309.921460000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object LblCustomerLocation: TfrxMemoView
        AllowVectorExport = True
        Left = 366.614410000000000000
        Top = 177.637910000000000000
        Width = 309.921460000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
      object LblCustomerCompanyNumber: TfrxMemoView
        AllowVectorExport = True
        Left = 438.425480000000000000
        Top = 264.567100000000000000
        Width = 264.567100000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        ParentFont = False
      end
    end
  end
  object Report: TfrxReport
    Version = '2022.2.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43910.444921006940000000
    ReportOptions.LastChange = 43910.444921006940000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 408
    Top = 16
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object FastReportIBDac: TfrxIBDACComponents
    DefaultDatabase = UniMainModule.DbModule
    Left = 224
    Top = 16
  end
  object frxRichObject1: TfrxRichObject
    Left = 224
    Top = 144
  end
end
