unit ReportRepository;
interface
uses
  SysUtils, Classes, frxClass, frxDBSet, frxExportBaseDialog, frxExportPDF, Data.DB, MemDS, DBAccess, IBC,
  frxDACComponents, frxIBDACComponents, frxRich;
type
  TReportRepositoryFrm = class(TDataModule)
    ReportBorderellen: TfrxReport;
    PdfExport: TfrxPDFExport;
    frxAgents: TfrxDBDataset;
    frxCommission: TfrxDBDataset;
    ReportsFiche28150: TfrxReport;
    Report: TfrxReport;
    FastReportIBDac: TfrxIBDACComponents;
    frxRichObject1: TfrxRichObject;
  private
    { Private declarations }
    procedure Set_report_params(Report: TFrxReport);
  public
    { Public declarations }
    function Print_insurance_statement(InsuranceStatementId: longint; Preview: boolean): string;
    function Print_invoice_v2(InvoiceId: longint; Language: integer): string;
    function Generate_privacy_statement(Digital: integer; TwoParties: boolean): string;
  end;

  function ReportRepositoryFrm: TReportRepositoryFrm;

implementation
{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, ServerModule, ReportPreview, Utils;

function ReportRepositoryFrm: TReportRepositoryFrm;
begin
  Result := TReportRepositoryFrm(UniMainModule.GetModuleInstance(TReportRepositoryFrm));
end;
{ TReportRepositoryFrm }

function TReportRepositoryFrm.Generate_privacy_statement(Digital: integer; TwoParties: boolean): string;
var FileName:  string;
    AUrl:      string;
begin
  Report.Clear;
  if FileExists(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'PrivacyChapter.fr3')
  then Report.LoadFromFile(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'PrivacyChapter.fr3')
  else exit;

  Report.Variables['Digital'] := Digital;

  //if TwoParties = True
  //then Report.Variables['Twoparties'] := 1
  //else Report.Variables['Twoparties'] := 0;

  Set_report_params(Report);
  FileName := Utils.EscapeIllegalChars('Privacychapter');

  PdfExport.FileName                                    := UniServerModule.NewCacheFileUrl(False, 'pdf', FileName, '', AUrl, True);
  Report.PrepareReport;
  Report.Export(PdfExport);
  Result                                                := PdfExport.FileName;
end;


function TReportRepositoryFrm.Print_insurance_statement(
  InsuranceStatementId: longint; Preview: boolean): string;
var AUrl: string;
begin
  Report.Clear;
  if FileExists(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'Insurance_statement.fr3')
  then Report.LoadFromFile(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'Insurance_statement.fr3')
  else if FileExists(UniMainModule.Default_Template_archive + 'Insurance_statement.fr3')
       then Report.LoadFromFile(UniMainModule.Default_Template_archive + 'Insurance_statement.fr3');
  Report.Variables['Insurance_document_Id'] := InsuranceStatementId;
  Set_report_params(Report);
  PdfExport.FileName                                    := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', AUrl, True);
  Report.PrepareReport;
  Report.Export(PdfExport);
  Result                                                := PdfExport.FileName;

  if Preview
  then begin
         if Trim(AUrl) <> ''
         then with ReportPreviewFrm
              do begin
                   Preview_document(AUrl);
                   ShowModal;
                 end;
       end;
end;

function TReportRepositoryFrm.Print_invoice_v2(InvoiceId: longint; Language: integer): string;
var FileName:  string;
    AUrl:      string;
begin
  Report.Clear;
  if FileExists(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'InvoiceV2.fr3')
  then Report.LoadFromFile(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'InvoiceV2.fr3')
  else if FileExists(UniMainModule.Default_Template_Archive + 'InvoiceV2.fr3')
       then Report.LoadFromFile(UniMainModule.Default_Template_Archive + 'InvoiceV2.fr3');

  Report.Variables['InvoiceId']       := InvoiceId;
  Report.Variables['PrintConditions'] := 1;
  Report.Variables['Duplicate']       := 0;
  Report.Variables['PreviewMode']     := 0;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select Invoice_number from fin_sales_invoices_header where id=' + IntToStr(InvoiceId));
  UniMainModule.QWork.Open;
  case Language of
    0:   FileName  := 'Factuur_' + UniMainModule.QWork.FieldByName('Invoice_number').AsString;
    1:   FileName  := 'Facture_' + UniMainModule.QWork.FieldByName('Invoice_number').AsString;
    2:   FileName  := 'Invoice_' + UniMainModule.QWork.FieldByName('Invoice_number').AsString;
    else FileName  := 'Factuur_' + UniMainModule.QWork.FieldByName('Invoice_number').AsString;
  end;
  UniMainModule.QWork.Close;

  Set_report_params(Report);
  PdfExport.FileName                                    := UniServerModule.NewCacheFileUrl(False, 'pdf', FileName, '', AUrl, True);
  Report.PrepareReport;
  Report.Export(PdfExport);
  Result                                                := AUrl;
end;

procedure TReportRepositoryFrm.Set_report_params(Report: TFrxReport);
begin
  Report.PrintOptions.ShowDialog             := False;
  Report.ShowProgress                        := False;
  Report.EngineOptions.SilentMode            := True;
  Report.EngineOptions.EnableThreadSafe      := True;
  Report.EngineOptions.DestroyForms          := False;
  Report.EngineOptions.UseGlobalDataSetList  := False;
  Report.PreviewOptions.AllowEdit            := False;
  PdfExport.Background                       := True;
  PdfExport.ShowProgress                     := False;
  PdfExport.ShowDialog                       := False;
  PdfExport.DefaultPath                      := '';
end;

Initialization
  RegisterModuleClass(TReportRepositoryFrm);
end.
