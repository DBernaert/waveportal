unit CustomersMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniLabel, uniImage, uniPanel,
  uniButton, UniThemeButton, siComp, siLngLnk;

type
  TCustomersMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerHeaderLeft: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerHeaderRight: TUniContainerPanel;
    ContainerCustomerModule: TUniContainerPanel;
    BtnExit: TUniThemeButton;
    LinkedLanguageCustomersMain: TsiLangLinked;
    ContainerMenu: TUniContainerPanel;
    PanelHeaderContributor: TUniPanel;
    ImageLogoContributor: TUniImage;
    LblUserName: TUniLabel;
    PanelMainNavigation: TUniPanel;
    LblMainNavigation: TUniLabel;
    PnlMyFiles: TUniPanel;
    BtnMyFiles: TUniThemeButton;
    LblMyFiles: TUniLabel;
    PnlUploadDocuments: TUniPanel;
    BtnUploadDocuments: TUniThemeButton;
    LblUploadDocuments: TUniLabel;
    PnlCloseSession: TUniPanel;
    BtnCloseSession: TUniThemeButton;
    LblCloseSession: TUniLabel;
    PnlChangePassword: TUniPanel;
    BtnChangePassWord: TUniThemeButton;
    LblChangePassWord: TUniLabel;
    PnlInvoices: TUniPanel;
    BtnInvoices: TUniThemeButton;
    LblInvoices: TUniLabel;
    PnlAvailableDocuments: TUniPanel;
    BtnAvailableDocuments: TUniThemeButton;
    LblAvailableDocuments: TUniLabel;
    procedure BtnExitClick(Sender: TObject);
    procedure PnlUploadDocumentsClick(Sender: TObject);
    procedure PnlMyFilesClick(Sender: TObject);
    procedure PnlChangePasswordClick(Sender: TObject);
    procedure PnlInvoicesClick(Sender: TObject);
    procedure PnlAvailableDocumentsClick(Sender: TObject);
  private
    { Private declarations }
    ActiveFrameCustomers: TUniFrame;
    procedure Check_privacy_doc_signature;
  public
    { Public declarations }
    procedure Start_customers_module;
    procedure Open_customer_introduction;
  end;

implementation

{$R *.dfm}

uses MainModule, CreditDocumentsMain, CustomerIntroduction, CustomerCreditFiles,
     CustomerChangePassword, CustomerInvoices, CustomerPrivacySignature,
  CustomerDocuments;



{ TCustomersMainFrm }

procedure TCustomersMainFrm.BtnExitClick(Sender: TObject);
begin
  UniSession.Terminate('<script>window.location.href = "' + UniMainModule.Portal_return_url + '";</script>');
end;

procedure TCustomersMainFrm.Check_privacy_doc_signature;
begin
  if (UniMainModule.portal_contacts_privacy_doc = True) and
     (UniMainModule.user_portal_privacy_doc_signed = False)
  then with CustomerPrivacySignatureFrm
       do begin
            Start_privacy_signature;
            ShowModal;
          end;
end;

procedure TCustomersMainFrm.Open_customer_introduction;
begin
  if (ActiveFrameCustomers <> nil) and (ActiveFrameCustomers.ClassType = TCustomerIntroductionFrm)
  then exit;

  if (ActiveFrameCustomers <> nil)
  then FreeAndNil(ActiveFrameCustomers);

  ActiveFrameCustomers := TCustomerIntroductionFrm.Create(Self);
  With (ActiveFrameCustomers as TCustomerIntroductionFrm)
  do begin
       Parent := ContainerCustomerModule;
     end;
end;

procedure TCustomersMainFrm.PnlAvailableDocumentsClick(Sender: TObject);
begin
  if (ActiveFrameCustomers <> nil) and (ActiveFrameCustomers.ClassType = TCustomerDocumentsFrm)
  then exit;

  if (ActiveFrameCustomers <> nil)
  then FreeAndNil(ActiveFrameCustomers);

  ActiveFrameCustomers := TCustomerDocumentsFrm.Create(Self);
  With (ActiveFrameCustomers as TCustomerDocumentsFrm)
  do begin
       Start_customer_documents;
       Parent := ContainerCustomerModule;
     end;
end;

procedure TCustomersMainFrm.PnlChangePasswordClick(Sender: TObject);
begin
  With CustomerChangePassWordFrm
  do begin
       Init_customer_change_password;
       ShowModal;
     end;
end;

procedure TCustomersMainFrm.PnlInvoicesClick(Sender: TObject);
begin
  if (ActiveFrameCustomers <> nil) and (ActiveFrameCustomers.ClassType = TCustomerInvoicesFrm)
  then exit;

  if (ActiveFrameCustomers <> nil)
  then FreeAndNil(ActiveFrameCustomers);

  ActiveFrameCustomers := TCustomerInvoicesFrm.Create(Self);
  With (ActiveFrameCustomers as TCustomerInvoicesFrm)
  do begin
       Start_customer_invoices(Self);
       Parent := ContainerCustomerModule;
     end;
end;

procedure TCustomersMainFrm.PnlMyFilesClick(Sender: TObject);
begin
  if (ActiveFrameCustomers <> nil) and (ActiveFrameCustomers.ClassType = TCustomerCreditFilesFrm)
  then exit;

  if (ActiveFrameCustomers <> nil)
  then FreeAndNil(ActiveFrameCustomers);

  ActiveFrameCustomers := TCustomerCreditFilesFrm.Create(Self);
  With (ActiveFrameCustomers as TCustomerCreditFilesFrm)
  do begin
       start_customer_creditfiles;
       Parent := ContainerCustomerModule;
     end;
end;

procedure TCustomersMainFrm.PnlUploadDocumentsClick(Sender: TObject);
begin
  if (ActiveFrameCustomers <> nil) and (ActiveFrameCustomers.ClassType = TCreditDocumentsMainFrm)
  then exit;

  if (ActiveFrameCustomers <> nil)
  then FreeAndNil(ActiveFrameCustomers);

  ActiveFrameCustomers := TCreditDocumentsMainFrm.Create(Self);
  With (ActiveFrameCustomers as TCreditDocumentsMainFrm)
  do begin
       Start_credit_documents(Self);
       Parent := ContainerCustomerModule;
     end;
end;

procedure TCustomersMainFrm.Start_customers_module;
begin
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: LblTitle.Caption    := UniMainModule.Portal_title_dutch;
  2: LblTitle.Caption    := UniMainModule.Portal_title_french;
  3: LblTitle.Caption    := UniMainModule.Portal_title_english;
  end;

  LblUserName.Caption := UniMainModule.User_name;
  Open_customer_introduction;
  Check_privacy_doc_signature;
end;

end.
