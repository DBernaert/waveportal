unit CustomerCreditFiles;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniLabel, uniBasicGrid,
  uniDBGrid, uniButton, UniThemeButton, Data.DB, MemDS, DBAccess, IBC, siComp,
  siLngLnk, uniPanel;

type
  TCustomerCreditFilesFrm = class(TUniFrame)
    LblYourFiles: TUniLabel;
    LblAvailable: TUniLabel;
    GridCreditFilesCustomer: TUniDBGrid;
    GridAttachments: TUniDBGrid;
    CreditFilesCustomer: TIBCQuery;
    DsCreditFilesCustomer: TDataSource;
    BtnDownloadDocument: TUniThemeButton;
    Attachments: TIBCQuery;
    DsAttachments: TDataSource;
    CreditFilesCustomerID: TLargeintField;
    CreditFilesCustomerTOTAL_AMOUNT_CREDIT: TFloatField;
    CreditFilesCustomerDATE_CREDIT_FINAL: TDateField;
    CreditFilesCustomerSTATUS_DESCRIPTION: TWideStringField;
    CreditFilesCustomerFINANCIAL_INSTITUTION: TWideStringField;
    LinkedLanguageCustomerCreditFiles: TsiLangLinked;
    CreditFilesCustomerDATE_SUBMISSION: TDateField;
    GridLog: TUniDBGrid;
    Log: TIBCQuery;
    DsLog: TDataSource;
    LblLog: TUniLabel;
    LblMyFiles: TUniLabel;
    LblDescription1: TUniLabel;
    LblDescription2: TUniLabel;
    LblDescription3: TUniLabel;
    LblDescription4: TUniLabel;
    procedure BtnDownloadDocumentClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure start_customer_creditfiles;
  end;

implementation

{$R *.dfm}

uses MainModule, Utils;



{ TCustomerCreditFilesFrm }

procedure TCustomerCreditFilesFrm.BtnDownloadDocumentClick(Sender: TObject);
var Destination: string;
begin
  if ((Attachments.Active) and (not Attachments.fieldbyname('Id').isNull))
  then begin
         Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
         if Trim(Attachments.FieldByName('Folder').AsString) <> ''
         then Destination := Destination + Attachments.FieldByName('Folder').AsString + '\';
         if Trim(Attachments.FieldByName('SubFolder').AsString) <> ''
         then Destination := Destination + Attachments.FieldByName('SubFolder').AsString + '\';
         Destination := Destination + Attachments.FieldByName('Destination_name').AsString;

         UniSession.SendFile(Destination, Utils.EscapeIllegalChars(Attachments.fieldbyname('Original_Name').asString));
       end;
end;

procedure TCustomerCreditFilesFrm.start_customer_creditfiles;
var SQL: string;
begin
  CreditFilesCustomer.Close;
  SQL := 'SELECT credit_files.id, credit_files.total_amount_credit, credit_files.date_credit_final, credit_files.date_submission, ';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: SQL := SQL + 'basictables.dutch as status_description, ';
  2: SQL := SQL + 'basictables.french as status_description, ';
  3: SQL := SQL + 'basictables.english as status_description, ';
  end;

  SQL := SQL +
         'credit_financialinstitutions.name as financial_institution ' +
         'from credit_files ' +
         'left outer join basictables on (credit_files.status = basictables.id) ' +
         'left outer join credit_financialinstitutions on (credit_files.financial_institution = credit_financialinstitutions.id) ' +
         'WHERE credit_files.id in ' +
         '(select distinct credit_file from credit_file_demanders where demander_type=0 and demander_id=:Demander_id) ' +
         'order by credit_files.date_credit_final desc';
  CreditFilesCustomer.SQL.Text := SQL;
  CreditFilesCustomer.ParamByName('Demander_id').AsInteger := UniMainModule.User_id;
  CreditFilesCustomer.Open;

  Attachments.Open;
  Log.Open;
end;

end.
