unit CustomerDocuments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, siComp, siLngLnk, Data.DB, MemDS, DBAccess, IBC,
  uniURLFrame, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses, uniPanel;

var
	str_error_file_not_found: string = 'Het bestand kon niet worden gevonden'; // TSI: Localized (Don't modify!)

type
  TCustomerDocumentsFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblDocuments: TUniLabel;
    LblDescription1: TUniLabel;
    LblDescription2: TUniLabel;
    ContainerBody: TUniContainerPanel;
    GridDocuments: TUniDBGrid;
    DocumentsBrowse: TIBCQuery;
    DsDocumentsBrowse: TDataSource;
    LinkedLanguageCustomerDocuments: TsiLangLinked;
    procedure GridDocumentsDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageCustomerDocumentsChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_customer_documents;
  end;

implementation

{$R *.dfm}

uses MainModule, Utils, Main;



{ TCustomerDocumentsFrm }

procedure TCustomerDocumentsFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCustomerDocumentsFrm.GridDocumentsDblClick(Sender: TObject);
var Destination:   string;
    FileName:      string;
begin
  if (DocumentsBrowse.Active = False) or (DocumentsBrowse.FieldByName('Id').IsNull)
  then exit;

  Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';

  if Trim(DocumentsBrowse.FieldByName('Folder').AsString) <> ''
  then Destination := Destination + DocumentsBrowse.FieldByName('Folder').AsString + '\';

  if Trim(DocumentsBrowse.FieldByName('SubFolder').AsString) <> ''
  then Destination := Destination + DocumentsBrowse.FieldByName('SubFolder').AsString + '\';

  Destination := Destination + DocumentsBrowse.FieldByName('Destination_Name').AsString;

  if Trim(DocumentsBrowse.FieldByName('Description').AsString) <> ''
  then begin
         FileName := DocumentsBrowse.FieldByname('Description').AsString;
         FileName := FileName + ExtractFileExt(DocumentsBrowse.FieldByName('Original_name').AsString)
       end
  else FileName := DocumentsBrowse.FieldByname('Original_name').AsString;

  if FileExists(Destination)
  then UniSession.SendFile(Destination, Utils.EscapeIllegalChars(FileName))
  else MainForm.WaveShowErrorToast(str_error_file_not_found);
end;

procedure TCustomerDocumentsFrm.LinkedLanguageCustomerDocumentsChangeLanguage(
  Sender: TObject);
begin

  UpdateStrings;
end;

procedure TCustomerDocumentsFrm.Start_customer_documents;
begin
  DocumentsBrowse.Close;
  DocumentsBrowse.ParamByName('Contact_id').AsInteger := UniMainModule.User_id;
  DocumentsBrowse.Open;
end;

procedure TCustomerDocumentsFrm.UpdateStrings;
begin
  str_error_file_not_found := LinkedLanguageCustomerDocuments.GetTextOrDefault('strstr_error_file_not_found' (* 'Het bestand kon niet worden gevonden' *) );
end;

end.

