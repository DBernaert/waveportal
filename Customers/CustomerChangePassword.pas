unit CustomerChangePassword;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniEdit, uniButton,
  UniThemeButton, uniLabel, siComp, siLngLnk;

var
	str_both_fields_required:        string = 'Beide velden zijn verplicht'; // TSI: Localized (Don't modify!)
	str_minimum_length_password:     string = 'Het wachtwoord moet minstens 6 karakters bevatten'; // TSI: Localized (Don't modify!)
	str_match_confirmation:          string = 'Het wachtwoord en de bevestiging komen niet overeen'; // TSI: Localized (Don't modify!)
	str_password_modified:           string = 'Uw wachtwoord werd bijgewerkt, u kan nu inloggen met uw nieuw wachtwoord'; // TSI: Localized (Don't modify!)
	str_error_modification_password: string = 'Er is een fout opgetreden bij het bijwerken van uw wachtwoord.'; // TSI: Localized (Don't modify!)

type
  TCustomerChangePasswordFrm = class(TUniForm)
    EditPassword: TUniEdit;
    EditConfirmation: TUniEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    LblDescription1: TUniLabel;
    LblDescription2: TUniLabel;
    LblDescription3: TUniLabel;
    LinkedLanguageCustomerChangePassWord: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure LinkedLanguageCustomerChangePassWordChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_customer_change_password;
  end;

function CustomerChangePasswordFrm: TCustomerChangePasswordFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, Utils;

function CustomerChangePasswordFrm: TCustomerChangePasswordFrm;
begin
  Result := TCustomerChangePasswordFrm(UniMainModule.GetFormInstance(TCustomerChangePasswordFrm));
end;

{ TCustomerChangePasswordFrm }

procedure TCustomerChangePasswordFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TCustomerChangePasswordFrm.BtnSaveClick(Sender: TObject);
var Password: string;
begin
  BtnSave.SetFocus;

  if (Trim(EditPassWord.Text) = '') or (Trim(EditConfirmation.Text) = '')
  then begin
         MainForm.WaveShowWarningMessage(str_both_fields_required);
         Exit;
       end;

  if (Length(Trim(EditPassWord.Text)) < 6) or (Length(Trim(EditConfirmation.Text)) < 6)
  then begin
         MainForm.WaveShowWarningMessage(str_minimum_length_password);
         Exit;
       end;

  if UpperCase(Trim(EditPassWord.Text)) <> UpperCase(Trim(EditConfirmation.Text))
  then begin
         MainForm.WaveShowWarningMessage(str_match_confirmation);
         Exit;
       end;

  PassWord := Utils.Crypt(Trim(EditPassWord.Text), 35462);
  UniMainModule.QBatch.Close;
  UniMainModule.QBatch.SQL.Text := 'Update crm_contacts set portal_password=:Portal_password where Id=:Id';
  UniMainModule.QBatch.ParamByName('Portal_password').AsString := PassWord;
  UniMainModule.QBatch.ParamByName('Id').AsInteger             := UniMainModule.User_id;

  Try
    UniMainModule.QBatch.ExecSQL;
    if UniMainModule.UpdTr_Batch.Active
    then UniMainModule.UpdTr_Batch.Commit;
    UniMainModule.QBatch.Close;
    MainForm.WaveShowConfirmationToast(str_password_modified);
    Close;
  Except
    if UniMainModule.UpdTr_Batch.Active
    then UniMainModule.UpdTr_Batch.Rollback;
    UniMainModule.QBatch.Close;
    MainForm.WaveShowErrorMessage(str_error_modification_password);
  End;
end;

procedure TCustomerChangePasswordFrm.Init_customer_change_password;
begin
  //
end;

procedure TCustomerChangePasswordFrm.LinkedLanguageCustomerChangePassWordChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCustomerChangePasswordFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCustomerChangePasswordFrm.UpdateStrings;
begin
  str_error_modification_password := LinkedLanguageCustomerChangePassWord.GetTextOrDefault('strstr_error_modification_password' (* 'Er is een fout opgetreden bij het bijwerken van uw wachtwoord.' *) );
  str_password_modified           := LinkedLanguageCustomerChangePassWord.GetTextOrDefault('strstr_password_modified' (* 'Uw wachtwoord werd bijgewerkt, u kan nu inloggen met uw nieuw wachtwoord' *) );
  str_match_confirmation          := LinkedLanguageCustomerChangePassWord.GetTextOrDefault('strstr_match_confirmation' (* 'Het wachtwoord en de bevestiging komen niet overeen' *) );
  str_minimum_length_password     := LinkedLanguageCustomerChangePassWord.GetTextOrDefault('strstr_minimum_length_password' (* 'Het wachtwoord moet minstens 6 karakters bevatten' *) );
  str_both_fields_required        := LinkedLanguageCustomerChangePassWord.GetTextOrDefault('strstr_both_fields_required' (* 'Beide velden zijn verplicht' *) );
end;

end.

