unit CustomerPrivacySignature;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniButton, UniThemeButton,
  REST.Types, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, uniLabel,
  DBAccess, IBC, Data.DB, MemDS, siComp, siLngLnk;

var
	str_error_sending_sign_request:         string = 'Er is een fout opgetreden bij het doorsturen van het te ondertekene document'; // TSI: Localized (Don't modify!)
	str_document_ready_for_signature:       string = 'Document beschikbaar voor ondertekening'; // TSI: Localized (Don't modify!)
	str_generation_digital_document:        string = 'Even geduld, aanmaak van het digitale document...'; // TSI: Localized (Don't modify!)
	str_upload_document:                    string = 'Online plaatsen van het document...'; // TSI: Localized (Don't modify!)
	str_signing:                            string = 'Ondertekening'; // TSI: Localized (Don't modify!)
	str_is_document_signed:                 string = 'Heeft u het document ondertekend?'; // TSI: Localized (Don't modify!)
	str_error_processing_result_signature:  string = 'Fout bij het verwerken van de ondertekening'; // TSI: Localized (Don't modify!)
	str_confirmation_signature:             string = 'Ondertekening goed ontvangen'; // TSI: Localized (Don't modify!)
	str_customer:                           string = ' klant'; // TSI: Localized (Don't modify!)
	str_signed_document_not_found:          string = 'Het ondertekende document kon niet gevonden worden'; // TSI: Localized (Don't modify!)

type
  TCustomerPrivacySignatureFrm = class(TUniForm)
    BtnSign: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    UploadDocumentClient: TRESTClient;
    UpLoadDocumentRequest: TRESTRequest;
    UpLoadDocumentResponse: TRESTResponse;
    UploadDescriptionClient: TRESTClient;
    UploadDescriptionRequest: TRESTRequest;
    UploadDescriptionResponse: TRESTResponse;
    LblDescriptionLine1: TUniLabel;
    LblDescriptionLine2: TUniLabel;
    LblDescriptionLine3: TUniLabel;
    LblDescriptionLine4: TUniLabel;
    LblDescriptionLine6: TUniLabel;
    LblDescriptionLine7: TUniLabel;
    LblTitle: TUniLabel;
    LblDescriptionLine8: TUniLabel;
    ContactsEdit: TIBCQuery;
    UpdTrContactsEdit: TIBCTransaction;
    LblAction: TUniLabel;
    ValidateSignatureClient: TRESTClient;
    ValidateDocumentRequest: TRESTRequest;
    ValidateDocumentResponse: TRESTResponse;
    LinkedLanguageCustomerPrivacySignature: TsiLangLinked;
    LblWarningPopup1: TUniLabel;
    LblWarningPopup2: TUniLabel;
    LblWarningPopup3: TUniLabel;
    procedure BtnSignClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageCustomerPrivacySignatureChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    StartTime_check: string;
    EndTime_check:   string;

    procedure Validate_signature(DocumentId: string);
  public
    { Public declarations }
    procedure Start_privacy_signature;
  end;

function CustomerPrivacySignatureFrm: TCustomerPrivacySignatureFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, XSuperObject, ReportRepository, Main,
  DateUtils;

function CustomerPrivacySignatureFrm: TCustomerPrivacySignatureFrm;
begin
  Result := TCustomerPrivacySignatureFrm(UniMainModule.GetFormInstance(TCustomerPrivacySignatureFrm));
end;

{ TUniForm1 }

procedure TCustomerPrivacySignatureFrm.BtnCancelClick(Sender: TObject);
begin
  UniSession.Terminate('<script>window.location.href = "' + UniMainModule.Portal_return_url + '";</script>');
end;

procedure TCustomerPrivacySignatureFrm.BtnSignClick(Sender: TObject);
var DocumentName: string;
    Document:     TFileStream;
    DocumentId:   string;
    ReturnValue:  string;
    X:            ISuperObject;
    Y:            ISuperObject;
    Status:       string;
    Reason:       string;
    URL:          string;
    StartTime:    TDateTime;
    EndTime:      TDateTime;
begin
  ContactsEdit.Close;

  ContactsEdit.SQL.Text :=
  'Select portal_privacy_doc_document_url, portal_privacy_doc_online, ' +
  'portal_privacy_doc_document_id, ' +
  'portal_privacy_doc_online_date from crm_contacts where Id=:Id';

  ContactsEdit.ParamByName('Id').AsInteger := UniMainModule.User_id;

  ContactsEdit.Open;

  if (ContactsEdit.FieldByName('Portal_privacy_doc_online').AsInteger = 1) and
     (Trim(ContactsEdit.FieldByName('Portal_privacy_doc_document_url').AsString) <> '')
  then begin
         URL := ContactsEdit.FieldByName('Portal_privacy_doc_document_url').AsString;

         DocumentId := ContactsEdit.FieldByName('Portal_privacy_doc_document_id').AsString;

         StartTime := IncMinute(ContactsEdit.FieldByName('Portal_privacy_doc_online_date').AsDateTime, -120);
         EndTime   := IncMinute(ContactsEdit.FieldByname('Portal_privacy_doc_online_date').AsDateTime, 120);

         StartTime_Check := FormatDateTime('YYYY-MM-DD', StartTime) + 'T' +
                            FormatDateTime('HH:MM:SS',   StartTime) + '+01:00';
         EndTime_Check   := FormatDateTime('YYYY-MM-DD', EndTime)   + 'T' +
                            FormatDateTime('HH:MM:SS',   EndTime)   + '+01:00';

         ContactsEdit.Close;

         LblAction.Caption := str_document_ready_for_signature;
         URL := 'window.open(' + '''' + URL + ''', ''_blank'');';
         UniSession.AddJS(URL);
         UniSession.Synchronize();
       end
  else begin
         LblAction.Caption := str_generation_digital_document;

         LblAction.Visible := True;
         UniSession.Synchronize();

         //Generate the document
         With ReportRepositoryFrm
         do DocumentName := Generate_privacy_statement(1, false);

         if Trim(DocumentName) = ''
         then exit;

         if FileExists(DocumentName)
         then begin
                LblAction.Caption := str_upload_document;
                UniSession.Synchronize();
                UploadDocumentRequest.Params.Clear;
                UploadDocumentRequest.Params.AddHeader('x-oksign-authorization', UniMainModule.Ok_sign_authorization);
                UploadDocumentRequest.Params[0].Options := [poDoNotEncode];
                UploadDocumentRequest.Params.AddHeader('x-oksign-filename', 'PrivacyChapter.pdf');
                UploadDocumentRequest.Params[1].Options := [poDoNotEncode];
                UploadDocumentRequest.Params.AddHeader('content-type', 'application/pdf');
                UploadDocumentRequest.Params[2].Options := [poDoNotEncode];

                Document := TFileStream.Create(DocumentName, fmOpenRead);

                Try
                  Document.Position := 0;
                  UploadDocumentRequest.params.AddBody(Document, ctAPPLICATION_PDF);
                Finally
                  FreeAndNil(Document);
                End;

                UploadDocumentRequest.Execute;

                if UploadDocumentRequest.Response.StatusCode <> 200
                then begin
                       LblAction.Visible := False;
                       MainForm.WaveShowErrorToast(str_error_sending_sign_request);
                       Exit;
                     end;

                ReturnValue := UploadDocumentRequest.Response.Content;
                //Process the response
                X      := SO(ReturnValue);
                Status := X['status'].AsString;
                Reason := X['reason'].AsString;

                if UpperCase(Status) <> 'OK'
                then begin
                       LblAction.Visible := False;
                       MainForm.WaveShowErrorToast(str_error_sending_sign_request + ': ' + Reason);
                       Exit;
                     end;

                DocumentId := Reason;

                //Description uploaden
                Y := SO;
                Y.B['reusable']                        := false;
                Y.A['fields'].O[0].S['marker']         := '_customersignature_';
                Y.A['fields'].O[0].S['inputtype']      := 'CanvasSIG';
                Y.A['fields'].O[0].S['name']           := 'SIG_FIELD_1';
                Y.A['fields'].O[0].B['required']       := true;
                Y.A['fields'].O[0].I['width']          := 175;
                Y.A['fields'].O[0].I['height']         := 70;
                Y.A['fields'].O[0].S['signerid']       := 'bt_00000000-0000-0000-0000-000000000100';
                Y.A['fields'].O[0].O['signingoptions'].O['eid'];
                Y.A['fields'].O[0].O['signingoptions'].O['pen'];
                Y.A['fields'].O[0].O['signingoptions'].O['tan'];
                //Y.A['fields'].O[0].O['signingoptions'].O['itsme'];

                Y.A['signersinfo'].O[0].S['name']      := UniMainModule.User_name;
                Y.A['signersinfo'].O[0].S['mobile']    := UniMainModule.User_phone_mobile;
                Y.A['signersinfo'].O[0].S['actingas']  := Trim(UniMainModule.Company_name) + str_customer;
                Y.A['signersinfo'].O[0].S['id']        := 'bt_00000000-0000-0000-0000-000000000100';
                Y.A['signersinfo'].O[0].S['email']     := UniMainModule.User_email;
                UploadDescriptionRequest.Params.Clear;

                UploadDescriptionRequest.Params.AddHeader('x-oksign-authorization', UniMainModule.Ok_sign_authorization);
                UploadDescriptionRequest.Params[0].Options := [poDoNotEncode];
                UploadDescriptionRequest.Params.AddHeader('content-type', 'application/json');
                UploadDescriptionRequest.Params[1].Options := [poDoNotEncode];
                UploadDescriptionRequest.Params.AddHeader('x-oksign-docid', DocumentId);
                UploadDescriptionRequest.Params[2].Options := [poDoNotEncode];
                UploadDescriptionRequest.ClearBody;
                UploadDescriptionRequest.AddBody(Y.AsJSON(false,false), ctAPPLICATION_JSON);

                UploadDescriptionRequest.Execute;

                ReturnValue := UploadDescriptionRequest.Response.Content;

                //Process the response
                Y      := SO(ReturnValue);
                Status := Y['status'].AsString;

                if UpperCase(Status) <> 'OK'
                then begin
                       MainForm.WaveShowErrorToast(str_error_sending_sign_request);
                       Exit;
                     end;

                Reason := Y.A['reason'].O[0].S['id'];
                Reason := Y.A['reason'].O[0].S['name'];

                Try
                  ContactsEdit.Close;
                  ContactsEdit.SQL.Text := 'Select * from crm_contacts where Id=:Id';
                  ContactsEdit.ParamByName('Id').AsInteger := UniMainModule.User_id;
                  ContactsEdit.Open;
                  ContactsEdit.Edit;

                  ContactsEdit.FieldByName('Portal_privacy_doc_document_id').AsString  :=
                  DocumentId;

                  ContactsEdit.FieldByName('Portal_privacy_doc_document_url').AsString :=
                  Y.A['reason'].O[0].S['url'];

                  ContactsEdit.FieldByName('Portal_privacy_doc_online').AsInteger := 1;
                  ContactsEdit.FieldByName('Portal_privacy_doc_online_date').AsDateTime := Now;

                  ContactsEdit.Post;

                  if UpdTrContactsEdit.Active
                  then UpdTrContactsEdit.Commit;


                  StartTime := IncMinute(ContactsEdit.FieldByName('Portal_privacy_doc_online_date').AsDateTime, -120);
                  EndTime   := IncMinute(ContactsEdit.FieldByname('Portal_privacy_doc_online_date').AsDateTime, 120);

                  StartTime_Check := FormatDateTime('YYYY-MM-DD', StartTime) + 'T' +
                                     FormatDateTime('HH:MM:SS',   StartTime) + '+01:00';
                  EndTime_Check   := FormatDateTime('YYYY-MM-DD', EndTime)   + 'T' +
                                     FormatDateTime('HH:MM:SS',   EndTime)   + '+01:00';

                  ContactsEdit.Close;
                  LblAction.Caption := str_document_ready_for_signature;

                  URL := 'window.open(' + '''' + Y.A['reason'].O[0].S['url'] + ''', ''_blank'');';

                  UniSession.AddJS(URL);

                  UniSession.Synchronize();
                Except
                  if UpdTrContactsEdit.Active
                  then UpdTrContactsEdit.Rollback;

                  ContactsEdit.Close;
                  LblAction.Visible := False;

                  MainForm.WaveShowErrorToast(str_error_sending_sign_request);
                End;
              end;
       end;

  if MainForm.WaveConfirmBlocking(str_signing, str_is_document_signed)
  then Validate_signature(DocumentId);
end;

procedure TCustomerPrivacySignatureFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCustomerPrivacySignatureFrm.LinkedLanguageCustomerPrivacySignatureChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCustomerPrivacySignatureFrm.Start_privacy_signature;
begin

end;

procedure TCustomerPrivacySignatureFrm.UpdateStrings;
begin
  str_signed_document_not_found         := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_signed_document_not_found' (* 'Het ondertekende document kon niet gevonden worden' *) );
  str_customer                          := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_customer' (* ' klant' *) );
  str_confirmation_signature            := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_confirmation_signature' (* 'Ondertekening goed ontvangen' *) );
  str_error_processing_result_signature := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_error_processing_result_signature' (* 'Fout bij het verwerken van de ondertekening' *) );
  str_is_document_signed                := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_is_document_signed' (* 'Heeft u het document ondertekend?' *) );
  str_signing                           := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_signing' (* 'Ondertekening' *) );
  str_upload_document                   := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_upload_document' (* 'Online plaatsen van het document...' *) );
  str_generation_digital_document       := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_generation_digital_document' (* 'Even geduld, aanmaak van het digitale document...' *) );
  str_document_ready_for_signature      := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_document_ready_for_signature' (* 'Document beschikbaar voor ondertekening' *) );
  str_error_sending_sign_request        := LinkedLanguageCustomerPrivacySignature.GetTextOrDefault('strstr_error_sending_sign_request' (* 'Er is een fout opgetreden bij het doorsturen van het te ondertekene document' *) );
end;

procedure TCustomerPrivacySignatureFrm.Validate_signature(DocumentId: string);
var ReturnValue:    string;
    X:              ISuperObject;
    Obj:            ISuperObject;
    Status:         string;
    J:              integer;
    NumberSigned:   integer;
    NumberRequired: integer;
    SourceDocId:    string;
    DocumentFound:  boolean;
begin
  ValidateDocumentRequest.Params.Clear;
  ValidateDocumentRequest.Params.AddHeader('x-oksign-authorization', UniMainModule.Ok_sign_authorization);
  ValidateDocumentRequest.Params[0].Options := [poDoNotEncode];
  ValidateDocumentRequest.Params.AddHeader('content-type', 'application/json');
  ValidateDocumentRequest.Params[1].Options := [poDoNotEncode];
  ValidateDocumentRequest.Params.AddHeader('x-oksign-starttime', StartTime_check);
  ValidateDocumentRequest.Params[2].Options := [poDoNotEncode];
  ValidateDocumentRequest.Params.AddHeader('x-oksign-endtime',   EndTime_check);
  ValidateDocumentRequest.Params[3].Options := [poDoNotEncode];
  ValidateDocumentRequest.Execute;

  ReturnValue := ValidateDocumentRequest.Response.Content;

  X      := SO(ReturnValue);
  Status := X['status'].AsString;

  if UpperCase(Status) <> 'OK'
  then begin
         MainForm.WaveShowErrorToast(str_error_processing_result_signature);
         Exit;
       end;

  DocumentFound := False;
  with X.A['reason'] do
    for J := 0 to Length -1 do
    begin
      Obj := O[J];
      Obj.First;
      NumberSigned    := 0;
      NumberRequired  := 0;
      SourceDocId     := '';

      while not Obj.EoF do
      begin
        if UpperCase(Obj.CurrentKey) = 'NBROFSIGATURESREQUIRED'
        then NumberRequired := Obj.CurrentValue.AsVariant;

        if UpperCase(Obj.CurrentKey) = 'NBROFSIGATURESVALID'
        then NumberSigned := Obj.CurrentValue.AsVariant;

        if UpperCase(Obj.CurrentKey)  = 'SOURCE_DOCID'
        then SourceDocId := Obj.CurrentValue.AsVariant;
        Obj.Next;
      end;

      if (SourceDocId = DocumentId) and (NumberSigned = NumberRequired)
      then begin
             DocumentFound := True;

             ContactsEdit.Close;

             ContactsEdit.SQL.Text :=
             'Select id, portal_privacy_doc_signed, portal_privacy_doc_date_signed ' +
             'from crm_contacts where Id=:Id';

             ContactsEdit.ParamByName('Id').AsInteger := UniMainModule.User_id;

             ContactsEdit.Open;

             ContactsEdit.Edit;
             ContactsEdit.FieldByName('Portal_privacy_doc_signed').AsInteger := 1;
             ContactsEdit.FieldByName('Portal_privacy_doc_date_signed').AsDateTime := Now;
             ContactsEdit.Post;

             Try
               if UpdTrContactsEdit.Active
               then UpdTrContactsEdit.Commit;
               ContactsEdit.Close;
               MainForm.WaveShowConfirmationToast(str_confirmation_signature);
               Close;
             Except
               if UpdTrContactsEdit.Active
               then UpdTrContactsEdit.Rollback;
               ContactsEdit.Close;
               LblAction.Visible := False;
               MainForm.WaveShowErrorToast(str_error_processing_result_signature);
             End;
           end;
    end;

    if DocumentFound = False
    then MainForm.WaveShowErrorToast(str_signed_document_not_found);
end;

end.


