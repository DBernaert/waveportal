object CreditDocumentsMainFrm: TCreditDocumentsMainFrm
  Left = 0
  Top = 0
  Width = 1041
  Height = 945
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerDescription: TUniContainerPanel
    Left = 16
    Top = 64
    Width = 1017
    Height = 121
    Hint = ''
    ParentColor = False
    TabOrder = 0
    LayoutConfig.Region = 'north'
    object LblTitle1: TUniLabel
      Left = 0
      Top = 0
      Width = 50
      Height = 17
      Hint = ''
      Caption = 'Geachte,'
      ParentFont = False
      Font.Height = -13
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
    object LblTitle2: TUniLabel
      Left = 0
      Top = 24
      Width = 659
      Height = 17
      Hint = ''
      Caption = 
        'Via het onderstaande formulier kan u de nodige documenten aanlev' +
        'eren die nodig zijn voor uw kredietaanvraag.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 2
      LayoutConfig.Cls = 'boldtext'
    end
    object LblTitle3: TUniLabel
      Left = 0
      Top = 48
      Width = 50
      Height = 17
      Hint = ''
      Caption = 'Werking:'
      ParentFont = False
      Font.Height = -13
      TabOrder = 3
      LayoutConfig.Cls = 'boldtext'
    end
    object LblTitle4: TUniLabel
      Left = 32
      Top = 64
      Width = 424
      Height = 17
      Hint = ''
      Caption = 
        'Links ziet u de lijst van de categorie'#235'n waar documenten voor no' +
        'dig zijn.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 4
      LayoutConfig.Cls = 'boldtext'
    end
    object LblTitle5: TUniLabel
      Left = 32
      Top = 80
      Width = 458
      Height = 17
      Hint = ''
      Caption = 
        'Selecteer de categorie waarvoor u documenten wenst toe te voegen' +
        ' in de lijst.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 5
      LayoutConfig.Cls = 'boldtext'
    end
    object LblTitle6: TUniLabel
      Left = 32
      Top = 96
      Width = 837
      Height = 17
      Hint = ''
      Caption = 
        'U kan documenten toevoegen door deze te slepen naar het vak of d' +
        'oor te klikken op het vak en de documenten te selecteren op uw c' +
        'omputer.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 6
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerUpload: TUniContainerPanel
    Left = 16
    Top = 193
    Width = 1017
    Height = 744
    Hint = ''
    ParentColor = False
    TabOrder = 1
    LayoutConfig.Region = 'center'
    object GridCategories: TUniDBGrid
      Left = 0
      Top = 24
      Width = 353
      Height = 329
      Hint = ''
      DataSource = Ds_VwDocItems
      Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      LayoutConfig.Cls = 'customGrid'
      TabOrder = 1
      OnDrawColumnCell = GridCategoriesDrawColumnCell
      Columns = <
        item
          Flex = 1
          FieldName = 'DESCRIPTION_DUTCH'
          Title.Caption = 'Omschrijving'
          Width = 1204
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'Dummy'
          Title.Caption = ' '
          Width = 25
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
    object FileUploadButton: TUniFileUploadButton
      Left = 368
      Top = 24
      Width = 489
      Height = 209
      Hint = ''
      Caption = 
        'Sleep documenten hier<br><br>of klik om documenten te selecteren' +
        ' op uw computer<br><br><b>Enkel PDF-documenten</b>'
      Messages.Uploading = 'Uploading...'
      Messages.PleaseWait = 'Please Wait'
      Messages.UploadError = 'Upload Error'
      Messages.UploadTimeout = 'Timeout occurred...'
      Messages.MaxSizeError = 'File is bigger than maximum allowed size'
      Messages.MaxFilesError = 'You can upload maximum %d files.'
      Overwrite = True
      AllowDragDrop = True
      ButtonVisible = False
      ShowUploadingMsg = False
      OnMultiCompleted = FileUploadButtonMultiCompleted
    end
    object LblCategories: TUniLabel
      Left = 0
      Top = 0
      Width = 74
      Height = 17
      Hint = ''
      Caption = 'Categorie'#235'n:'
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsUnderline]
      TabOrder = 3
      LayoutConfig.Cls = 'boldtext'
    end
    object LblExtraInformation: TUniLabel
      Left = 0
      Top = 472
      Width = 133
      Height = 17
      Hint = ''
      Caption = 'Bijkomende informatie:'
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsUnderline]
      TabOrder = 4
      LayoutConfig.Cls = 'boldtext'
    end
    object MemoExtraInfo: TUniDBMemo
      Left = 0
      Top = 496
      Width = 353
      Height = 105
      Cursor = crArrow
      Hint = ''
      DataField = 'HINT_DUTCH'
      DataSource = Ds_VwDocItems
      BorderStyle = ubsSolid
      ParentFont = False
      Font.Height = -13
      ReadOnly = True
      TabOrder = 5
      TabStop = False
      Color = clBtnFace
    end
    object LblDeliverdDocuments: TUniLabel
      Left = 368
      Top = 248
      Width = 457
      Height = 17
      Hint = ''
      TextConversion = txtHTML
      Caption = 
        'Aangeleverde documenten voor de gekozen categorie <b>(maximum 5)' +
        '</b>:'
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsUnderline]
      TabOrder = 6
      LayoutConfig.Cls = 'boldtext'
    end
    object GridDocuments: TUniDBGrid
      Left = 368
      Top = 280
      Width = 489
      Height = 177
      Hint = ''
      DataSource = DsUpdFilesArchive
      Options = [dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      WebOptions.FetchAll = True
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      LayoutConfig.Cls = 'customGrid'
      TabOrder = 7
      Columns = <
        item
          Flex = 1
          FieldName = 'ORIGINAL_NAME'
          Title.Caption = 'Naam document'
          Width = 64
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
    object BtnDeleteDocument: TUniThemeButton
      Left = 672
      Top = 464
      Width = 185
      Height = 33
      Hint = ''
      Caption = 'Document verwijderen'
      TabStop = False
      TabOrder = 8
      OnClick = BtnDeleteDocumentClick
      ButtonTheme = uctDanger
    end
    object BtnCloseCreditDocuments: TUniThemeButton
      Left = 488
      Top = 512
      Width = 369
      Height = 41
      Hint = ''
      Caption = 'Ik ben klaar met het aanleveren van documenten'
      TabStop = False
      TabOrder = 9
      OnClick = BtnCloseCreditDocumentsClick
      ButtonTheme = uctSuccess
    end
    object LegendRed: TUniContainerPanel
      Left = 0
      Top = 392
      Width = 17
      Height = 17
      Hint = ''
      ParentColor = False
      Color = clRed
      TabOrder = 10
    end
    object LegendOrange: TUniContainerPanel
      Left = 0
      Top = 416
      Width = 17
      Height = 17
      Hint = ''
      ParentColor = False
      Color = 4227327
      TabOrder = 11
    end
    object LegendGreen: TUniContainerPanel
      Left = 0
      Top = 440
      Width = 17
      Height = 17
      Hint = ''
      ParentColor = False
      Color = clGreen
      TabOrder = 12
    end
    object LblRed: TUniLabel
      Left = 24
      Top = 392
      Width = 193
      Height = 13
      Hint = ''
      Caption = 'Niet aanwezig of ongeldig (zie reden)'
      TabOrder = 13
    end
    object LblOrange: TUniLabel
      Left = 24
      Top = 416
      Width = 79
      Height = 13
      Hint = ''
      Caption = 'In behandeling'
      TabOrder = 14
    end
    object LblGreen: TUniLabel
      Left = 24
      Top = 440
      Width = 103
      Height = 13
      Hint = ''
      Caption = 'Document aanvaard'
      TabOrder = 15
    end
    object LblLegend: TUniLabel
      Left = 0
      Top = 360
      Width = 74
      Height = 17
      Hint = ''
      Caption = 'Categorie'#235'n:'
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsUnderline]
      TabOrder = 16
      LayoutConfig.Cls = 'boldtext'
    end
    object LblReasonInvalid: TUniLabel
      Left = 0
      Top = 608
      Width = 96
      Height = 17
      Hint = ''
      Caption = 'Reden ongeldig:'
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsUnderline]
      TabOrder = 17
      LayoutConfig.Cls = 'boldtext'
    end
    object MemoReasonInvalid: TUniDBMemo
      Left = 0
      Top = 632
      Width = 353
      Height = 105
      Cursor = crArrow
      Hint = ''
      DataField = 'REASON_REJECTED'
      DataSource = Ds_VwDocItems
      BorderStyle = ubsSolid
      ParentFont = False
      Font.Height = -13
      ReadOnly = True
      TabOrder = 18
      TabStop = False
      Color = clBtnFace
    end
    object BtnViewDocument: TUniThemeButton
      Left = 480
      Top = 464
      Width = 185
      Height = 33
      Hint = ''
      Caption = 'Document bekijken'
      TabStop = False
      TabOrder = 19
      OnClick = BtnViewDocumentClick
      ButtonTheme = uctPrimary
    end
  end
  object LblWelcome: TUniLabel
    Left = 16
    Top = 16
    Width = 345
    Height = 45
    Hint = ''
    Caption = 'Documenten aanleveren'
    ParentFont = False
    Font.Height = -32
    TabOrder = 2
    LayoutConfig.Cls = 'boldtext'
  end
  object VwDocItems: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    credit_doc_items.id,'
      '    sys_credit_doc_items.description_dutch,'
      '    credit_doc_items.hint_dutch, '
      '    sys_credit_doc_items.description_french,'
      '    credit_doc_items.hint_french,'
      '    sys_credit_doc_items.description_english,'
      '    credit_doc_items.hint_english,'
      '    credit_doc_items.sys_credit_doc_item,'
      '    credit_doc_items.present,'
      '    credit_doc_items.validated,'
      '    credit_doc_items.rejected,'
      '    reason_rejected'
      'from credit_doc_items'
      
        '   left outer join sys_credit_doc_items on (credit_doc_items.sys' +
        '_credit_doc_item = sys_credit_doc_items.id)')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 160
    Top = 320
    object VwDocItemsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object VwDocItemsDESCRIPTION_DUTCH: TWideStringField
      FieldName = 'DESCRIPTION_DUTCH'
      ReadOnly = True
      Size = 200
    end
    object VwDocItemsHINT_DUTCH: TWideMemoField
      FieldName = 'HINT_DUTCH'
      ReadOnly = True
      BlobType = ftWideMemo
    end
    object VwDocItemsDESCRIPTION_FRENCH: TWideStringField
      FieldName = 'DESCRIPTION_FRENCH'
      ReadOnly = True
      Size = 200
    end
    object VwDocItemsHINT_FRENCH: TWideMemoField
      FieldName = 'HINT_FRENCH'
      ReadOnly = True
      BlobType = ftWideMemo
    end
    object VwDocItemsDESCRIPTION_ENGLISH: TWideStringField
      FieldName = 'DESCRIPTION_ENGLISH'
      ReadOnly = True
      Size = 200
    end
    object VwDocItemsHINT_ENGLISH: TWideMemoField
      FieldName = 'HINT_ENGLISH'
      ReadOnly = True
      BlobType = ftWideMemo
    end
    object VwDocItemsSYS_CREDIT_DOC_ITEM: TLargeintField
      FieldName = 'SYS_CREDIT_DOC_ITEM'
      Required = True
    end
    object VwDocItemsPRESENT: TSmallintField
      FieldName = 'PRESENT'
      Required = True
    end
    object VwDocItemsVALIDATED: TSmallintField
      FieldName = 'VALIDATED'
      Required = True
    end
    object VwDocItemsREJECTED: TSmallintField
      FieldName = 'REJECTED'
    end
    object VwDocItemsTest: TStringField
      FieldKind = fkCalculated
      FieldName = 'Dummy'
      Size = 1
      Calculated = True
    end
    object VwDocItemsREASON_REJECTED: TWideMemoField
      FieldName = 'REASON_REJECTED'
      BlobType = ftWideMemo
    end
  end
  object Ds_VwDocItems: TDataSource
    DataSet = VwDocItems
    Left = 160
    Top = 376
  end
  object UpdFilesArchive: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_FILES_ARCHIVE_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTransFilesArchive
    SQL.Strings = (
      'Select * from files_archive')
    MasterFields = 'ID'
    DetailFields = 'RECORD_ID'
    MasterSource = Ds_VwDocItems
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 160
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        Value = nil
      end>
  end
  object UpdTransFilesArchive: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 160
    Top = 560
  end
  object DsUpdFilesArchive: TDataSource
    DataSet = UpdFilesArchive
    Left = 160
    Top = 497
  end
end
