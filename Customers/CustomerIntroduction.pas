unit CustomerIntroduction;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniLabel, Vcl.Imaging.pngimage,
  uniImage, siComp, siLngLnk;

type
  TCustomerIntroductionFrm = class(TUniFrame)
    LblDescription1: TUniLabel;
    LblDescription2: TUniLabel;
    LblWelcome: TUniLabel;
    ImageArrow: TUniImage;
    LinkedLanguageCustomerIntroduction: TsiLangLinked;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses MainModule;



end.
