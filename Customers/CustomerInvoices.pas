unit CustomerInvoices;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, CustomersMain, uniGUIBaseClasses, uniPanel,
  uniLabel, uniURLFrame, uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC,
  siComp, siLngLnk;

type
  TCustomerInvoicesFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblInvoices: TUniLabel;
    ContainerBody: TUniContainerPanel;
    GridInvoices: TUniDBGrid;
    LblDescription1: TUniLabel;
    LblDescription2: TUniLabel;
    InvoicesBrowse: TIBCQuery;
    InvoicesBrowseID: TLargeintField;
    InvoicesBrowseINVOICE_NUMBER: TLargeintField;
    InvoicesBrowseINVOICE_DATE: TDateField;
    InvoicesBrowseINVOICE_EXPIRATION_DATE: TDateField;
    InvoicesBrowseINVOICE_TOTAL_AMOUNT: TFloatField;
    InvoicesBrowseLANGUAGE_DOC: TSmallintField;
    InvoicesBrowseCUSTOMERNAME: TWideStringField;
    DsInvoicesBrowse: TDataSource;
    PdfPreview: TUniPDFFrame;
    LinkedLanguageCustomerInvoices: TsiLangLinked;
    procedure GridInvoicesCellClick(Column: TUniDBGridColumn);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_customer_invoices(ParentForm: TCustomersMainFrm);
  end;

implementation

{$R *.dfm}

uses MainModule, ReportRepository;



{ TCustomerInvoicesFrm }

procedure TCustomerInvoicesFrm.GridInvoicesCellClick(Column: TUniDBGridColumn);
var Url: string;
begin
  if InvoicesBrowse.FieldByName('Id').IsNull
  then exit;

  with ReportRepositoryFrm
  do begin
       Url := Print_invoice_v2(InvoicesBrowse.FieldByName('Id').AsInteger, InvoicesBrowse.FieldByName('Language_doc').AsInteger);
       if Trim(Url) <> ''
       then PdfPreview.PdfURL := Url;
     end;
end;

procedure TCustomerInvoicesFrm.Start_customer_invoices(ParentForm: TCustomersMainFrm);
begin
  InvoicesBrowse.Close;
  InvoicesBrowse.ParamByName('ContactId').AsInteger := UniMainModule.User_id;
  InvoicesBrowse.Open;
  if InvoicesBrowse.RecordCount > 0
  then GridInvoicesCellClick(nil)
  else PdfPreview.PdfURL                   := '';
end;

end.
