object CustomerInvoicesFrm: TCustomerInvoicesFrm
  Left = 0
  Top = 0
  Width = 1004
  Height = 736
  Layout = 'border'
  LayoutConfig.Margin = '16 16 16 16'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1004
    Height = 89
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    LayoutConfig.Region = 'north'
    object LblInvoices: TUniLabel
      Left = 0
      Top = 0
      Width = 187
      Height = 39
      Hint = ''
      Caption = 'Mijn facturen'
      ParentFont = False
      Font.Height = -32
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
    object LblDescription1: TUniLabel
      Left = 0
      Top = 48
      Width = 384
      Height = 16
      Hint = ''
      Caption = 
        'Hieronder kan u een overzicht vinden van uw beschikbare facturen' +
        '.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 2
      LayoutConfig.Cls = 'boldtext'
    end
    object LblDescription2: TUniLabel
      Left = 0
      Top = 64
      Width = 667
      Height = 16
      Hint = ''
      Caption = 
        'Bij het selecteren van een factuur krijgt u een voorbeeld te zie' +
        'n aan de rechterkant en kan u de factuur downloaden.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 3
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerBody: TUniContainerPanel
    Left = 8
    Top = 136
    Width = 961
    Height = 561
    Hint = ''
    ParentColor = False
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Padding = '16 0 0 0'
    LayoutConfig.Region = 'center'
    object GridInvoices: TUniDBGrid
      Left = 16
      Top = 16
      Width = 449
      Height = 361
      Hint = ''
      DataSource = DsInvoicesBrowse
      Options = [dgTitles, dgIndicator, dgColumnResize, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      LayoutConfig.Region = 'west'
      TabOrder = 1
      OnCellClick = GridInvoicesCellClick
      Columns = <
        item
          Flex = 1
          FieldName = 'INVOICE_NUMBER'
          Title.Alignment = taRightJustify
          Title.Caption = 'Factuurnr.'
          Width = 80
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'INVOICE_DATE'
          Title.Caption = 'Datum'
          Width = 80
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'INVOICE_EXPIRATION_DATE'
          Title.Caption = 'Vervaldatum'
          Width = 80
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'INVOICE_TOTAL_AMOUNT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Bedrag'
          Width = 64
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
    object PdfPreview: TUniPDFFrame
      Left = 576
      Top = 16
      Width = 369
      Height = 529
      Hint = ''
      LayoutConfig.Padding = '0 0 0 16'
      LayoutConfig.Region = 'center'
      TabOrder = 2
    end
  end
  object InvoicesBrowse: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select'
      '    fin_sales_invoices_header.id,'
      '    fin_sales_invoices_header.invoice_number,'
      '    fin_sales_invoices_header.invoice_date,'
      '    fin_sales_invoices_header.invoice_expiration_date,'
      '    fin_sales_invoices_header.invoice_total_amount,'
      '    case coalesce(fin_sales_invoices_header.account, 0)'
      
        '    when 0 then coalesce(crm_contacts.last_name, '#39#39') || '#39' '#39' || c' +
        'oalesce(crm_contacts.first_name, '#39#39')'
      '    else crm_accounts.name'
      '    end as customername,'
      '    case coalesce(fin_sales_invoices_header.account, 0)'
      '    when 0 then crm_contacts.language_doc'
      '    else crm_accounts.language_doc'
      '    end as language_doc'
      'from crm_contacts'
      
        '   right outer join fin_sales_invoices_header on (crm_contacts.i' +
        'd = fin_sales_invoices_header.invoice_contact_id)'
      
        '   left outer join crm_accounts on (fin_sales_invoices_header.ac' +
        'count = crm_accounts.id)'
      
        'where fin_sales_invoices_header.removed = '#39'0'#39' and fin_sales_invo' +
        'ices_header.visible_portal=1'
      'and fin_sales_invoices_header.invoice_contact_id=:ContactId'
      'order by fin_sales_invoices_header.invoice_date desc')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 296
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ContactId'
        Value = nil
      end>
    object InvoicesBrowseID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InvoicesBrowseINVOICE_NUMBER: TLargeintField
      FieldName = 'INVOICE_NUMBER'
      Required = True
    end
    object InvoicesBrowseINVOICE_DATE: TDateField
      FieldName = 'INVOICE_DATE'
      Required = True
    end
    object InvoicesBrowseINVOICE_EXPIRATION_DATE: TDateField
      FieldName = 'INVOICE_EXPIRATION_DATE'
    end
    object InvoicesBrowseINVOICE_TOTAL_AMOUNT: TFloatField
      FieldName = 'INVOICE_TOTAL_AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesBrowseLANGUAGE_DOC: TSmallintField
      FieldName = 'LANGUAGE_DOC'
      ReadOnly = True
    end
    object InvoicesBrowseCUSTOMERNAME: TWideStringField
      FieldName = 'CUSTOMERNAME'
      ReadOnly = True
      Size = 61
    end
  end
  object DsInvoicesBrowse: TDataSource
    DataSet = InvoicesBrowse
    Left = 296
    Top = 320
  end
  object LinkedLanguageCustomerInvoices: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'InvoicesBrowseID.DisplayLabel'
      'InvoicesBrowseINVOICE_NUMBER.DisplayLabel'
      'InvoicesBrowseINVOICE_DATE.DisplayLabel'
      'InvoicesBrowseINVOICE_EXPIRATION_DATE.DisplayLabel'
      'InvoicesBrowseINVOICE_TOTAL_AMOUNT.DisplayLabel'
      'InvoicesBrowseLANGUAGE_DOC.DisplayLabel'
      'InvoicesBrowseCUSTOMERNAME.DisplayLabel'
      'TCustomerInvoicesFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerBody.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 296
    Top = 384
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C0049006E0076006F0069006300650073000100
      4D0069006A006E00200066006100630074007500720065006E0001004D006500
      730020006600610063007400750072006500730001004D007900200069006E00
      76006F00690063006500730001000D000A004C0062006C004400650073006300
      720069007000740069006F006E003100010048006900650072006F006E006400
      6500720020006B0061006E00200075002000650065006E0020006F0076006500
      72007A0069006300680074002000760069006E00640065006E00200076006100
      6E0020007500770020006200650073006300680069006B006200610072006500
      200066006100630074007500720065006E002E00010056006F00750073002000
      740072006F00750076006500720065007A002000630069002D00640065007300
      73006F0075007300200075006E0020006100700065007200E700750020006400
      6500200076006F00730020006600610063007400750072006500730020006400
      6900730070006F006E00690062006C00650073002E000100420065006C006F00
      7700200079006F0075002000630061006E002000660069006E00640020006100
      6E0020006F00760065007200760069006500770020006F006600200079006F00
      75007200200061007600610069006C00610062006C006500200069006E007600
      6F0069006300650073002E0001000D000A004C0062006C004400650073006300
      720069007000740069006F006E0032000100420069006A002000680065007400
      2000730065006C006500630074006500720065006E002000760061006E002000
      650065006E002000660061006300740075007500720020006B00720069006A00
      67007400200075002000650065006E00200076006F006F007200620065006500
      6C00640020007400650020007A00690065006E002000610061006E0020006400
      6500200072006500630068007400650072006B0061006E007400200065006E00
      20006B0061006E00200075002000640065002000660061006300740075007500
      7200200064006F0077006E006C006F006100640065006E002E0001004C006F00
      72007300710075006500200076006F007500730020007300E9006C0065006300
      740069006F006E006E0065007A00200075006E00650020006600610063007400
      7500720065002C00200076006F00750073002000760065007200720065007A00
      200075006E0020006100700065007200E7007500200073007500720020006C00
      61002000640072006F00690074006500200065007400200076006F0075007300
      200070006F007500760065007A0020007400E9006C00E9006300680061007200
      67006500720020006C006100200066006100630074007500720065002E000100
      5700680065006E002000730065006C0065006300740069006E00670020006100
      6E00200069006E0076006F00690063006500200079006F007500200077006900
      6C006C0020007300650065002000610020007000720065007600690065007700
      20006F006E002000740068006500200072006900670068007400200061006E00
      6400200079006F0075002000630061006E00200064006F0077006E006C006F00
      610064002000740068006500200069006E0076006F006900630065002E000100
      0D000A0073007400480069006E00740073005F0055006E00690063006F006400
      65000D000A007300740044006900730070006C00610079004C00610062006500
      6C0073005F0055006E00690063006F00640065000D000A007300740046006F00
      6E00740073005F0055006E00690063006F00640065000D000A00730074004D00
      75006C00740069004C0069006E00650073005F0055006E00690063006F006400
      65000D000A007300740053007400720069006E00670073005F0055006E006900
      63006F00640065000D000A00730074004F007400680065007200530074007200
      69006E00670073005F0055006E00690063006F00640065000D000A0073007400
      43006F006C006C0065006300740069006F006E0073005F0055006E0069006300
      6F00640065000D000A00470072006900640049006E0076006F00690063006500
      73002E0043006F006C0075006D006E0073005B0030005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640049006E00
      76006F0069006300650073002E0043006F006C0075006D006E0073005B003000
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      46006100630074007500750072006E0072002E0001004E00B000200064006500
      20006600610063007400750072006500010049006E0076006F00690063006500
      20006E006F002E0001000D000A00470072006900640049006E0076006F006900
      6300650073002E0043006F006C0075006D006E0073005B0031005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      49006E0076006F0069006300650073002E0043006F006C0075006D006E007300
      5B0031005D002E005400690074006C0065002E00430061007000740069006F00
      6E00010044006100740075006D00010044006100740065000100440061007400
      650001000D000A00470072006900640049006E0076006F006900630065007300
      2E0043006F006C0075006D006E0073005B0032005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A00470072006900640049006E007600
      6F0069006300650073002E0043006F006C0075006D006E0073005B0032005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001005600
      65007200760061006C0064006100740075006D00010044006100740065002000
      64002700650078007000690072006100740069006F006E000100450078007000
      690072006100740069006F006E002000640061007400650001000D000A004700
      72006900640049006E0076006F0069006300650073002E0043006F006C007500
      6D006E0073005B0033005D002E0043006800650063006B0042006F0078004600
      690065006C0064002E004600690065006C006400560061006C00750065007300
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      650001000D000A00470072006900640049006E0076006F006900630065007300
      2E0043006F006C0075006D006E0073005B0033005D002E005400690074006C00
      65002E00430061007000740069006F006E000100420065006400720061006700
      01004D006F006E00740061006E007400010041006D006F0075006E0074000100
      0D000A0073007400430068006100720053006500740073005F0055006E006900
      63006F00640065000D000A00}
  end
end
