object CustomersMainFrm: TCustomersMainFrm
  Left = 0
  Top = 0
  Width = 1080
  Height = 742
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1080
    Height = 37
    Hint = ''
    ParentColor = False
    Color = 6367488
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerHeaderLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 553
      Height = 37
      Hint = ''
      ParentColor = False
      Color = 6367488
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object LblTitle: TUniLabel
        Left = 8
        Top = 9
        Width = 4
        Height = 21
        Hint = ''
        Caption = ''
        ParentFont = False
        Font.Color = clWhite
        Font.Height = -16
        TabOrder = 1
      end
    end
    object ContainerHeaderRight: TUniContainerPanel
      Left = 1055
      Top = 0
      Width = 25
      Height = 37
      Hint = ''
      ParentColor = False
      Color = 6367488
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      LayoutConfig.Margin = '0 8 0 0'
      object BtnExit: TUniThemeButton
        Left = 2
        Top = 6
        Width = 25
        Height = 25
        Hint = 'Sessie afsluiten'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 99
        LayoutConfig.Cls = 'icon-white'
        OnClick = BtnExitClick
        ButtonTheme = uctTertiary
      end
    end
  end
  object ContainerCustomerModule: TUniContainerPanel
    Left = 320
    Top = 72
    Width = 689
    Height = 617
    Hint = ''
    ParentColor = False
    TabOrder = 1
    Layout = 'fit'
    LayoutAttribs.Align = 'center'
    LayoutAttribs.Pack = 'center'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 8 8 8'
  end
  object ContainerMenu: TUniContainerPanel
    Left = 0
    Top = 37
    Width = 217
    Height = 705
    Hint = ''
    ParentColor = False
    Color = 15790320
    Align = alLeft
    TabOrder = 2
    Layout = 'vbox'
    LayoutConfig.Region = 'west'
    LayoutConfig.Margin = '8 0 8 8'
    object PanelHeaderContributor: TUniPanel
      Left = 0
      Top = 0
      Width = 217
      Height = 94
      Hint = ''
      Align = alTop
      TabOrder = 1
      BorderStyle = ubsNone
      Caption = ''
      Color = 6367488
      LayoutConfig.Width = '100%'
      object ImageLogoContributor: TUniImage
        Left = 83
        Top = 8
        Width = 50
        Height = 50
        Hint = ''
        Stretch = True
        Url = 'images/wv_user_account.png'
      end
      object LblUserName: TUniLabel
        Left = 13
        Top = 64
        Width = 191
        Height = 17
        Hint = ''
        Alignment = taCenter
        AutoSize = False
        Caption = ''
        ParentFont = False
        Font.Color = clWhite
        Font.Height = -13
        Font.Style = [fsBold]
        TabOrder = 2
      end
    end
    object PanelMainNavigation: TUniPanel
      Left = 3
      Top = 113
      Width = 169
      Height = 35
      Hint = ''
      TabOrder = 5
      BorderStyle = ubsNone
      Caption = ''
      Color = 15790320
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutAttribs.Pack = 'center'
      LayoutConfig.Width = '100%'
      object LblMainNavigation: TUniLabel
        Left = 32
        Top = 3
        Width = 67
        Height = 17
        Hint = ''
        Caption = 'NAVIGATIE'
        ParentFont = False
        Font.Height = -13
        Font.Style = [fsBold]
        TabOrder = 1
        LayoutConfig.Cls = 'boldtext'
      end
    end
    object PnlMyFiles: TUniPanel
      Left = 4
      Top = 149
      Width = 205
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 2
      BorderStyle = ubsNone
      Caption = ''
      Color = clWhite
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutConfig.Padding = '0 0 0 8'
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '3 3 3 3'
      OnClick = PnlMyFilesClick
      object BtnMyFiles: TUniThemeButton
        Left = 8
        Top = 5
        Width = 25
        Height = 25
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 71
        LayoutConfig.Cls = 'icon-blue'
        ButtonTheme = uctTertiary
      end
      object LblMyFiles: TUniLabel
        Left = 40
        Top = 11
        Width = 78
        Height = 17
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Mijn dossiers'
        ParentFont = False
        Font.Height = -13
        TabOrder = 1
        LayoutConfig.Margin = '0 0 0 15'
      end
    end
    object PnlUploadDocuments: TUniPanel
      Left = 4
      Top = 189
      Width = 205
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 3
      BorderStyle = ubsNone
      Caption = ''
      Color = clWhite
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutConfig.Padding = '0 0 0 8'
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '3 3 3 3'
      OnClick = PnlUploadDocumentsClick
      object BtnUploadDocuments: TUniThemeButton
        Left = 8
        Top = 5
        Width = 25
        Height = 25
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 54
        LayoutConfig.Cls = 'icon-blue'
        ButtonTheme = uctTertiary
      end
      object LblUploadDocuments: TUniLabel
        Left = 40
        Top = 11
        Width = 140
        Height = 17
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Documenten aanleveren'
        ParentFont = False
        Font.Height = -13
        TabOrder = 1
        LayoutConfig.Margin = '0 0 0 15'
      end
    end
    object PnlAvailableDocuments: TUniPanel
      Left = 4
      Top = 229
      Width = 205
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 8
      BorderStyle = ubsNone
      Caption = ''
      Color = clWhite
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutConfig.Padding = '0 0 0 8'
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '3 3 3 3'
      OnClick = PnlAvailableDocumentsClick
      object BtnAvailableDocuments: TUniThemeButton
        Left = 8
        Top = 5
        Width = 25
        Height = 25
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 72
        LayoutConfig.Cls = 'icon-blue'
        ButtonTheme = uctTertiary
      end
      object LblAvailableDocuments: TUniLabel
        Left = 40
        Top = 11
        Width = 101
        Height = 17
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Mijn documenten'
        ParentFont = False
        Font.Height = -13
        TabOrder = 1
        LayoutConfig.Margin = '0 0 0 15'
      end
    end
    object PnlInvoices: TUniPanel
      Left = 4
      Top = 277
      Width = 205
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 7
      BorderStyle = ubsNone
      Caption = ''
      Color = clWhite
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutConfig.Padding = '0 0 0 8'
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '3 3 3 3'
      OnClick = PnlInvoicesClick
      object BtnInvoices: TUniThemeButton
        Left = 8
        Top = 5
        Width = 25
        Height = 25
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 3
        LayoutConfig.Cls = 'icon-blue'
        ButtonTheme = uctTertiary
      end
      object LblInvoices: TUniLabel
        Left = 40
        Top = 11
        Width = 76
        Height = 17
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Mijn facturen'
        ParentFont = False
        Font.Height = -13
        TabOrder = 1
        LayoutConfig.Margin = '0 0 0 15'
      end
    end
    object PnlChangePassword: TUniPanel
      Left = 4
      Top = 325
      Width = 205
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 6
      BorderStyle = ubsNone
      Caption = ''
      Color = clWhite
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutConfig.Padding = '0 0 0 8'
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '3 3 3 3'
      OnClick = PnlChangePasswordClick
      object BtnChangePassWord: TUniThemeButton
        Left = 8
        Top = 5
        Width = 25
        Height = 25
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 26
        LayoutConfig.Cls = 'icon-blue'
        ButtonTheme = uctTertiary
      end
      object LblChangePassWord: TUniLabel
        Left = 40
        Top = 11
        Width = 139
        Height = 17
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Wachtwoord aanpassen'
        ParentFont = False
        Font.Height = -13
        TabOrder = 1
        LayoutConfig.Margin = '0 0 0 15'
      end
    end
    object PnlCloseSession: TUniPanel
      Left = 4
      Top = 373
      Width = 205
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 4
      BorderStyle = ubsNone
      Caption = ''
      Color = clWhite
      Layout = 'hbox'
      LayoutAttribs.Align = 'middle'
      LayoutConfig.Padding = '0 0 0 8'
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '9 3 3 3'
      OnClick = BtnExitClick
      object BtnCloseSession: TUniThemeButton
        Left = 8
        Top = 5
        Width = 25
        Height = 25
        Hint = ''
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 99
        LayoutConfig.Cls = 'icon-red'
        ButtonTheme = uctTertiary
      end
      object LblCloseSession: TUniLabel
        Left = 40
        Top = 11
        Width = 88
        Height = 17
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Sessie afsluiten'
        ParentFont = False
        Font.Height = -13
        TabOrder = 1
        LayoutConfig.Margin = '0 0 0 15'
      end
    end
  end
  object LinkedLanguageCustomersMain: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TCustomersMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerHeaderLeft.Layout'
      'ContainerHeaderRight.Layout'
      'ContainerCustomerModule.Layout'
      'ContainerMenu.Layout'
      'PanelHeaderContributor.Layout'
      'ImageLogoContributor.Url'
      'PanelMainNavigation.Layout'
      'PnlMyFiles.Layout'
      'PnlUploadDocuments.Layout'
      'PnlCloseSession.Layout'
      'PnlChangePassword.Layout'
      'PnlInvoices.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 512
    Top = 288
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C004D00610069006E004E006100760069006700
      6100740069006F006E0001004E00410056004900470041005400490045000100
      4E0041005600490047004100540049004F004E0001004E004100560049004700
      4100540049004F004E0001000D000A004C0062006C004D007900460069006C00
      6500730001004D0069006A006E00200064006F00730073006900650072007300
      01004D0065007300200064006F0073007300690065007200730001004D007900
      2000660069006C006500730001000D000A004C0062006C00550070006C006F00
      6100640044006F00630075006D0065006E0074007300010044006F0063007500
      6D0065006E00740065006E002000610061006E006C0065007600650072006500
      6E00010053006F0075006D0065007400740072006500200064006F0063007500
      6D0065006E007400730001005300750062006D0069007400200064006F006300
      75006D0065006E007400730001000D000A004C0062006C0043006C006F007300
      6500530065007300730069006F006E0001005300650073007300690065002000
      6100660073006C0075006900740065006E0001004600650072006D0065007200
      20006C0061002000730065007300730069006F006E00010043006C006F007300
      65002000730065007300730069006F006E0001000D000A004C0062006C004300
      680061006E0067006500500061007300730057006F0072006400010057006100
      63006800740077006F006F00720064002000610061006E007000610073007300
      65006E0001004300680061006E0067006500720020006D006F00740020006400
      650020007000610073007300650001004300680061006E006700650020007000
      61007300730077006F007200640001000D000A004C0062006C0049006E007600
      6F00690063006500730001004D0069006A006E00200066006100630074007500
      720065006E0001004D0065007300200066006100630074007500720065007300
      01004D007900200069006E0076006F00690063006500730001000D000A004C00
      62006C0041007600610069006C00610062006C00650044006F00630075006D00
      65006E007400730001004D0069006A006E00200064006F00630075006D006500
      6E00740065006E0001004D0065007300200064006F00630075006D0065006E00
      7400730001004D007900200064006F00630075006D0065006E00740073000100
      0D000A0073007400480069006E00740073005F0055006E00690063006F006400
      65000D000A00420074006E004500780069007400010053006500730073006900
      650020006100660073006C0075006900740065006E0001004600650072006D00
      6500720020006C0061002000730065007300730069006F006E00010043006C00
      6F00730065002000730065007300730069006F006E0001000D000A0073007400
      44006900730070006C00610079004C006100620065006C0073005F0055006E00
      690063006F00640065000D000A007300740046006F006E00740073005F005500
      6E00690063006F00640065000D000A00730074004D0075006C00740069004C00
      69006E00650073005F0055006E00690063006F00640065000D000A0073007400
      53007400720069006E00670073005F0055006E00690063006F00640065000D00
      0A00730074004F00740068006500720053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A0050006E006C004100760061006900
      6C00610062006C00650044006F00630075006D0065006E00740073002E004C00
      610079006F00750074000100680062006F0078000100010001000D000A007300
      740043006F006C006C0065006300740069006F006E0073005F0055006E006900
      63006F00640065000D000A007300740043006800610072005300650074007300
      5F0055006E00690063006F00640065000D000A00}
  end
end
