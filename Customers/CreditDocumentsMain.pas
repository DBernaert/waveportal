unit CreditDocumentsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniPanel, uniLabel,
  uniBasicGrid, uniDBGrid, uniFileUpload, Data.DB, MemDS, DBAccess, IBC,
  uniMemo, uniDBMemo, uniButton, UniThemeButton, siComp, siLngLnk,
  CustomersMain;

var
	str_delete_document:           string = 'Verwijderen document'; // TSI: Localized (Don't modify!)
	str_confirm_delete:            string = 'Wenst u het document te verwijderen?'; // TSI: Localized (Don't modify!)
	str_error_change_status:       string = 'Fout bij het bijwerken van de status'; // TSI: Localized (Don't modify!)
	str_error_delete_document:     string = 'Fout bij het verwijderen van het document'; // TSI: Localized (Don't modify!)
	str_error_creating_directory:  string = 'Fout bij het aanmaken van de directory'; // TSI: Localized (Don't modify!)
	str_error_uploading_documents: string = 'Fout bij het uploaden van de documenten'; // TSI: Localized (Don't modify!)

const
  str_file_not_found = 'Het bestand kon niet worden gevonden';
  str_max_5_files_category = 'Het maximaal aantal bestanden per categorie is 5';

type
  TCreditDocumentsMainFrm = class(TUniFrame)
    ContainerDescription: TUniContainerPanel;
    LblTitle1: TUniLabel;
    LblTitle2: TUniLabel;
    LblTitle3: TUniLabel;
    ContainerUpload: TUniContainerPanel;
    GridCategories: TUniDBGrid;
    FileUploadButton: TUniFileUploadButton;
    LblTitle4: TUniLabel;
    VwDocItems: TIBCQuery;
    Ds_VwDocItems: TDataSource;
    LblTitle5: TUniLabel;
    LblTitle6: TUniLabel;
    LblCategories: TUniLabel;
    LblExtraInformation: TUniLabel;
    MemoExtraInfo: TUniDBMemo;
    LblDeliverdDocuments: TUniLabel;
    GridDocuments: TUniDBGrid;
    VwDocItemsID: TLargeintField;
    VwDocItemsDESCRIPTION_DUTCH: TWideStringField;
    VwDocItemsHINT_DUTCH: TWideMemoField;
    VwDocItemsDESCRIPTION_FRENCH: TWideStringField;
    VwDocItemsHINT_FRENCH: TWideMemoField;
    VwDocItemsDESCRIPTION_ENGLISH: TWideStringField;
    VwDocItemsHINT_ENGLISH: TWideMemoField;
    VwDocItemsSYS_CREDIT_DOC_ITEM: TLargeintField;
    VwDocItemsPRESENT: TSmallintField;
    UpdFilesArchive: TIBCQuery;
    UpdTransFilesArchive: TIBCTransaction;
    DsUpdFilesArchive: TDataSource;
    BtnDeleteDocument: TUniThemeButton;
    BtnCloseCreditDocuments: TUniThemeButton;
    LblWelcome: TUniLabel;
    LegendRed: TUniContainerPanel;
    LegendOrange: TUniContainerPanel;
    LegendGreen: TUniContainerPanel;
    LblRed: TUniLabel;
    LblOrange: TUniLabel;
    LblGreen: TUniLabel;
    LblLegend: TUniLabel;
    VwDocItemsVALIDATED: TSmallintField;
    VwDocItemsREJECTED: TSmallintField;
    VwDocItemsTest: TStringField;
    LblReasonInvalid: TUniLabel;
    MemoReasonInvalid: TUniDBMemo;
    VwDocItemsREASON_REJECTED: TWideMemoField;
    BtnViewDocument: TUniThemeButton;
    procedure FileUploadButtonMultiCompleted(Sender: TObject;
      Files: TUniFileInfoArray);
    procedure BtnDeleteDocumentClick(Sender: TObject);
    procedure BtnCloseCreditDocumentsClick(Sender: TObject);
    procedure GridCategoriesDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
      Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure BtnViewDocumentClick(Sender: TObject);
  private
    { Private declarations }
    CallingForm: TCustomersMainFrm;
  public
    { Public declarations }
    procedure Start_credit_documents(ParentForm: TCustomersMainFrm);
  end;

implementation

{$R *.dfm}

uses MainModule, Main, IOUtils, System.DateUtils, Utils;

{ TCreditDocumentsMainFrm }

procedure TCreditDocumentsMainFrm.BtnCloseCreditDocumentsClick(Sender: TObject);
begin
  CallingForm.Open_customer_introduction;
end;

procedure TCreditDocumentsMainFrm.BtnDeleteDocumentClick(Sender: TObject);
begin
  if (UpdFilesArchive.Active = False) or (UpdFilesArchive.FieldByName('Id').IsNull) then
    exit;

  if MainForm.WaveConfirmBlocking(str_delete_document, str_confirm_delete) = True then begin
    UpdFilesArchive.Edit;
    UpdFilesArchive.FieldByname('Removed').AsString := '1';
    UpdFilesArchive.Post;

    Try
      if UpdTransFilesArchive.Active then
        UpdTransFilesArchive.Commit;

      UpdFilesArchive.Refresh;

      if UpdFilesArchive.RecordCount = 0 then begin
         UniMainModule.QBatch.Close;
         UniMainModule.QBatch.SQL.Text := 'Update credit_doc_items set present=0, validated=0, rejected=0, reason_rejected=:reason_rejected where id=:Id';
         UniMainModule.QBatch.ParamByName('Id').AsInteger := VwDocItems.FieldByName('Id').AsInteger;
         UniMainModule.QBatch.ParamByName('Reason_rejected').AsString := '';
         UniMainModule.QBatch.ExecSQL;
         Try
           if UniMainModule.UpdTr_Batch.Active then
             UniMainModule.UpdTr_Batch.Commit;
           UniMainModule.QBatch.Close;
           VwDocItems.Refresh;
         Except
           if UniMainModule.UpdTr_Batch.Active then
             UniMainModule.UpdTr_Batch.Rollback;
           UniMainModule.QBatch.Close;
           MainForm.WaveShowErrorToast(str_error_change_status);
         End;
      end;
    Except
      if UpdTransFilesArchive.Active then
        UpdTransFilesArchive.Rollback;
      MainForm.WaveShowErrorToast(str_error_delete_document);
    End;
  end;
end;

procedure TCreditDocumentsMainFrm.BtnViewDocumentClick(Sender: TObject);
var DestinationName: string;
begin
  if ((UpdFilesArchive.Active) and (not UpdFilesArchive.fieldbyname('Id').isNull))
  then begin
         DestinationName := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';

         if Trim(UpdFilesArchive.FieldByName('Folder').AsString) <> ''
         then DestinationName := DestinationName + UpdFilesArchive.FieldByName('Folder').AsString + '\';

         if Trim(UpdFilesArchive.FieldByname('SubFolder').AsString) <> ''
         then DestinationName := DestinationName + UpdFilesArchive.FieldByName('SubFolder').AsString + '\';

         DestinationName := DestinationName + UpdFilesArchive.FieldByName('Destination_name').AsString;

         if UpdFilesArchive.FieldByName('Storage_type').AsInteger = 1
         then UniSession.AddJS('window.open(' + QuotedStr(DestinationName) + ', ''_blank'')')
         else begin
                if FileExists(DestinationName)
                then UniSession.SendFile(DestinationName, Utils.EscapeIllegalChars(UpdFilesArchive.fieldbyname('Original_Name').asString))
                else MainForm.WaveShowErrorToast(str_file_not_found);
              end;
       end;
end;

procedure TCreditDocumentsMainFrm.FileUploadButtonMultiCompleted(
  Sender: TObject; Files: TUniFileInfoArray);
var
  TargetDir:       string;
  Dir:             string;
  TargetName:      string;
  TargetFolder:    string;
  TargetSubFolder: string;
  Destination:     string;
  I:               integer;

  TotalCount:      integer;
  Valid:           boolean;
begin
  if (VwDocItems.Active = False) or (VwDocItems.FieldByName('Id').IsNull)
  then exit;

  Valid := True;

  for I := Low(Files) to High(Files)
  do begin
       if ExtractFileExt(UpperCase(Files[I].FileName)) <> '.PDF'
       then Valid := False;
     end;

  if Valid = False
  then begin
    MainForm.WaveShowWarningToast('U kunt enkel Pdf-documenten uploaden');
    Exit;
  end;

  TotalCount := UpdFilesArchive.RecordCount;

  TotalCount :=
  TotalCount + Length(Files);

  if TotalCount > 5
  then begin
    MainForm.WaveShowWarningToast(str_max_5_files_category);
    Exit;
  end;

  TargetFolder    := IntToStr(YearOf(Now));
  TargetSubFolder := IntToStr(WeekOfTheYear(Now));
  TargetDir       := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\' + TargetFolder + '\' + TargetSubFolder + '\';
  Dir             := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\' + TargetFolder + '\' + TargetSubFolder;

  if not DirectoryExists(Dir)
  then begin
         if ForceDirectories(Dir) = False
         then begin
                MainForm.WaveShowErrorToast(str_error_creating_directory);
                exit;
              end;
       end;

  for I := Low(Files) to High(Files)
  do begin
       TargetName  := TPath.GetGUIDFileName(False);
       Destination := TargetDir + TargetName;
       if CopyFile(PChar(Files[I].CacheFile), PChar(Destination), False)
       then begin
              UpdFilesArchive.Append;
              UpdFilesArchive.FieldByName('COMPANYID').AsInteger        := UniMainModule.Company_id;
              UpdFilesArchive.FieldByName('REMOVED').asString           := '0';
              UpdFilesArchive.FieldByName('LANGUAGE_ID').AsInteger      := 0;
              UpdFilesArchive.FieldByName('MODULE_ID').AsInteger        := 100;   //Kredietdocumenten
              UpdFilesArchive.FieldByName('RECORD_ID').AsInteger        := VwDocItems.FieldByName('Id').AsInteger;
              UpdFilesArchive.FieldByName('VISIBLE_PORTAL').asInteger   := 0;
              UpdFilesArchive.FieldByName('DATE_ENTERED').asDateTime    := Now;
              UpdFilesArchive.FieldByName('DATE_MODIFIED').AsDateTime   := Now;
              UpdFilesArchive.FieldByName('CREATED_USER_ID').AsInteger  := UniMainModule.User_id;
              UpdFilesArchive.FieldByName('MODIFIED_USER_ID').AsInteger := UniMainModule.User_id;
              UpdFilesArchive.FieldByName('STORAGE_TYPE').AsInteger     := 0;
              UpdFilesArchive.FieldByName('ORIGINAL_NAME').AsString     := ExtractFileName(Files[I].OriginalFileName);
              UpdFilesArchive.FieldByName('DESTINATION_NAME').asString  := TargetName;
              UpdFilesArchive.FieldByName('FOLDER').AsString            := TargetFolder;
              UpdFilesArchive.FieldByName('SUBFOLDER').AsString         := TargetSubFolder;
              UpdFilesArchive.Post;
            end;

       Try
         if UpdTransFilesArchive.Active
         then UpdTransFilesArchive.Commit;

         //Bijwerken status
         UniMainModule.QBatch.Close;
         UniMainModule.QBatch.SQL.Text := 'Update credit_doc_items set present=1, rejected=0, validated=0, reason_rejected=:reason_rejected where id=:Id';
         UniMainModule.QBatch.ParamByName('Id').AsInteger := VwDocItems.FieldByName('Id').AsInteger;
         UniMainModule.QBatch.ParamByName('Reason_rejected').AsString := '';
         UniMainModule.QBatch.ExecSQL;

         Try
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Commit;

           UniMainModule.QBatch.Close;
           VwDocItems.Refresh;
         Except
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Rollback;

           UniMainModule.QBatch.Close;
           MainForm.WaveShowErrorToast(str_error_uploading_documents);
         End;
       Except
         if UpdTransFilesArchive.Active
         then UpdTransFilesArchive.Rollback;

         MainForm.WaveShowErrorToast(str_error_uploading_documents);
       End;
     end;
end;

procedure TCreditDocumentsMainFrm.GridCategoriesDrawColumnCell(Sender: TObject;
  ACol, ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if Column.Index = 1
  then begin
         if VwDocItems.FieldByName('Present').AsInteger = 0
         then Attribs.Color := ClRed;

         if VwDocItems.FieldByName('Rejected').AsInteger = 1
         then begin
                Attribs.Color := ClRed;
                Exit;
              end;

         if VwDocItems.FieldByName('Present').AsInteger = 1
         then Attribs.Color := $004080FF;

         if VwDocItems.FieldByName('Validated').AsInteger = 1
         then Attribs.Color := ClGreen;
       end;
end;

procedure TCreditDocumentsMainFrm.Start_credit_documents(ParentForm: TCustomersMainFrm);
var SQL: string;
begin
  CallingForm := ParentForm;
  VwDocItems.Close;
  SQL := 'select ' +
         'credit_doc_items.id, sys_credit_doc_items.description_dutch, ' +
         'credit_doc_items.hint_dutch, sys_credit_doc_items.description_french, ' +
         'credit_doc_items.hint_french, sys_credit_doc_items.description_english, ' +
         'credit_doc_items.hint_english, credit_doc_items.sys_credit_doc_item, ' +
         'credit_doc_items.present, ' +
         'credit_doc_items.validated, ' +
         'credit_doc_items.rejected, ' +
         'reason_rejected ' +
         'from credit_doc_items ' +
         'left outer join sys_credit_doc_items on (credit_doc_items.sys_credit_doc_item = sys_credit_doc_items.id) ' +
         'where credit_doc_items.companyId=:CompanyId ' +
         'and ((credit_doc_items.Module_id=2 and (credit_doc_items.record_id=:CustomerId or credit_doc_items.record_id=:CustomerId2)) ' +
         'or ' +
         '(credit_doc_items.Module_id=5 and (credit_doc_items.record_id in (select distinct credit_file from credit_file_demanders ' +
         'where demander_type = 0 and (demander_id=:CustomerId or (demander_id=:CustomerId2)))))) ';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + 'order by sys_credit_doc_items.description_dutch';
       GridCategories.Columns[0].FieldName := 'Description_dutch';
       MemoExtraInfo.DataField             := 'Hint_dutch';
     end;
  2: begin
       SQL := SQL + 'order by sys_credit_doc_items.description_french';
       GridCategories.Columns[0].FieldName := 'Description_french';
       MemoExtraInfo.DataField             := 'Hint_french';
     end;
  3: begin
       SQL := SQL + 'order by sys_credit_doc_items.description_english';
       GridCategories.Columns[0].FieldName := 'Description_english';
       MemoExtraInfo.DataField             := 'Hint_english';
     end;
  end;

  VwDocItems.SQL.Text := SQL;
  VwDocItems.ParamByName('CompanyId').AsInteger   := UniMainModule.Company_id;
  VwDocItems.ParamByName('CustomerId').AsInteger  := UniMainModule.User_id;

  if UniMainModule.User_partner_id = 0
  then VwDocItems.ParamByName('CustomerId2').AsInteger := UniMainModule.User_id
  else VwDocItems.ParamByName('CustomerId2').AsInteger := UniMainModule.User_partner_id;

  VwDocItems.Open;
  UpdFilesArchive.Close;
  UpdFilesArchive.SQL.Clear;
  UpdFilesArchive.SQL.Text := 'Select * from files_archive where module_id=100 and removed=''0'' ' +
                              'order by original_name';
  UpdFilesArchive.Open;
end;

end.
