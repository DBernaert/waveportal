unit Journal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniPanel, uniPageControl, uniDBNavigator, uniEdit, uniBasicGrid,
  uniDBGrid, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

var
	str_attachments:        string = 'Bijlagen'; // TSI: Localized (Don't modify!)
	str_relations:          string = 'Relaties'; // TSI: Localized (Don't modify!)
	str_propositions:       string = 'Voorstellen'; // TSI: Localized (Don't modify!)
	str_commission_schemas: string = 'Commissieschema''s'; // TSI: Localized (Don't modify!)
	strNotes:               string = 'Notities'; // TSI: Localized (Don't modify!)
	strTasks:               string = 'Taken'; // TSI: Localized (Don't modify!)
	str_payments:           string = 'Betalingen'; // TSI: Localized (Don't modify!)
	str_messages:           string = 'Berichten'; // TSI: Localized (Don't modify!)

type
  TJournalFrm = class(TUniFrame)
    PageControlJournal: TUniPageControl;
    LinkedLanguageJournal: TsiLangLinked;
    procedure LinkedLanguageJournalChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
  private
    { Private declarations }
    procedure Show_tasks(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_proposals(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_messages(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_Notes(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_attachments(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_insurance_payments(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_credit_documents(Module_id: integer; Master_datasource: TDataSource);
  public
    { Public declarations }
    procedure Start_journal(Module_id: integer; Master_datasource: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, ProposalsPanel, AttachmentsPanel, TasksPanel, NotesPanel,
     InsurancePaymentsPanel, MessagesInPanel, CreditDocumentsPanel;

{ TJournalFrm }

procedure TJournalFrm.LinkedLanguageJournalChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TJournalFrm.Show_attachments(Module_id: integer; Master_datasource: TDataSource);
var AttachmentsFrame:    TUniFrame;
    AttachmentsTabSheet: TUniTabSheet;
begin
  AttachmentsTabSheet                  := TUniTabSheet.Create(Self);
  AttachmentsTabSheet.PageControl      := PageControlJournal;
  AttachmentsTabSheet.AlignmentControl := uniAlignmentClient;
  AttachmentsTabSheet.Layout           := 'fit';
  AttachmentsTabSheet.Caption          := str_attachments;
  AttachmentsTabSheet.ImageIndex       := 38;
  AttachmentsFrame                     := TAttachmentsPanelFrm.Create(Self);
  With (AttachmentsFrame as TAttachmentsPanelFrm)
  do begin
       Start_attachments(Module_id, Master_datasource);
       Parent := AttachmentsTabSheet;
     end;
end;

procedure TJournalFrm.Show_credit_documents(Module_id: integer;
  Master_datasource: TDataSource);
var CreditDocumentsFrame: TUniFrame;
    CreditDocumentsTabSheet: TUniTabSheet;
begin
  CreditDocumentsTabSheet := TUniTabSheet.Create(Self);
  CreditDocumentsTabSheet.PageControl := PageControlJournal;
  CreditDocumentsTabSheet.AlignmentControl := uniAlignmentClient;
  CreditDocumentsTabSheet.Layout := 'fit';
  CreditDocumentsTabSheet.Caption := 'Kredietdocumenten';
  CreditDocumentsTabSheet.ImageIndex := 100;
  CreditDocumentsFrame := TCreditDocumentsPanelFrm.Create(Self);
  With (CreditDocumentsFrame as TCreditDocumentsPanelFrm)
  do begin
    Start_credit_documents(Module_id, Master_datasource);
    Parent := CreditDocumentsTabsheet;
  end;
end;

procedure TJournalFrm.Show_messages(Module_id: integer; Master_datasource: TDataSource);
var MessagesFrame:    TUniFrame;
    MessagesTabSheet: TUniTabSheet;
begin
  MessagesTabSheet                       := TUniTabSheet.Create(Self);
  MessagesTabSheet.PageControl           := PageControlJournal;
  MessagesTabSheet.AlignmentControl      := uniAlignmentClient;
  MessagesTabSheet.Layout                := 'fit';
  MessagesTabSheet.Caption               := str_messages;
  MessagesTabSheet.ImageIndex            := 68;
  MessagesFrame                          := TMessagesInPanelFrm.Create(Self);
  With (MessagesFrame as TMessagesInPanelFrm)
  do begin
       Start_messages_in(Module_id, Master_datasource);
       Parent := MessagesTabSheet;
     end;
end;

procedure TJournalFrm.Show_insurance_payments(Module_id: integer;
  Master_datasource: TDataSource);
var InsurancePaymentsFrame:    TUniFrame;
    InsurancePaymentsTabSheet: TUniTabSheet;
begin
  InsurancePaymentsTabSheet                  := TUniTabSheet.Create(Self);
  InsurancePaymentsTabSheet.PageControl      := PageControlJournal;
  InsurancePaymentsTabSheet.AlignmentControl := uniAlignmentClient;
  InsurancePaymentsTabSheet.Layout           := 'fit';
  InsurancePaymentsTabSheet.Caption          := str_payments;
  InsurancePaymentsTabSheet.ImageIndex       := 21;
  InsurancePaymentsFrame                     := TInsurancePaymentsPanelFrm.Create(Self);
  With (InsurancePaymentsFrame as TInsurancePaymentsPanelFrm)
  do begin
       Start_insurance_file_payments(Module_id, Master_datasource);
       Parent := InsurancePaymentsTabSheet;
     end;
end;

procedure TJournalFrm.Show_Notes(Module_id: integer; Master_datasource: TDataSource);
var NotesFrame:    TUniFrame;
    NotesTabSheet: TUniTabSheet;
begin
  NotesTabSheet                  := TUniTabSheet.Create(Self);
  NotesTabSheet.PageControl      := PageControlJournal;
  NotesTabSheet.AlignmentControl := uniAlignmentClient;
  NotesTabSheet.Layout           := 'fit';
  NotesTabSheet.Caption          := strNotes;
  NotesTabSheet.ImageIndex       := 49;
  NotesFrame                     := TNotesPanelFrm.Create(Self);
  With (NotesFrame as TNotesPanelFrm)
  do begin
       Start_notes(Module_id, Master_datasource);
       Parent := NotesTabSheet;
     end;
end;

procedure TJournalFrm.Show_proposals(Module_id: integer; Master_datasource: TDataSource);
var ProposalsFrame:    TUniFrame;
    ProposalsTabSheet: TUniTabSheet;
begin
  ProposalsTabSheet                  := TUniTabSheet.Create(Self);
  ProposalsTabSheet.PageControl      := PageControlJournal;
  ProposalsTabSheet.AlignmentControl := uniAlignmentClient;
  ProposalsTabSheet.Layout           := 'fit';
  ProposalsTabSheet.Caption          := str_propositions;
  ProposalsTabSheet.ImageIndex       := 3;
  ProposalsFrame                     := TProposalsPanelFrm.Create(Self);
  With (ProposalsFrame as TProposalsPanelFrm)
  do begin
       Start_proposals(Module_id, Master_datasource);
       Parent := ProposalsTabSheet;
     end;
end;

procedure TJournalFrm.Show_tasks(Module_id: integer; Master_datasource: TDataSource);
var TasksFrame:    TUniFrame;
    TasksTabSheet: TUniTabSheet;
begin
  TasksTabSheet                  := TUniTabSheet.Create(Self);
  TasksTabSheet.PageControl      := PageControlJournal;
  TasksTabSheet.AlignmentControl := uniAlignmentClient;
  TasksTabSheet.Layout           := 'fit';
  TasksTabSheet.Caption          := strTasks;
  TasksTabSheet.ImageIndex       := 8;
  TasksFrame := TTasksPanelFrm.Create(Self);
  With (TasksFrame as TTasksPanelFrm)
  do begin
       Start_tasks(Module_id, Master_datasource);
       Parent := TasksTabSheet;
     end;
end;

procedure TJournalFrm.Start_journal(Module_id: integer; Master_datasource: TDataSource);
begin
  {Overview id's:
   1: Accounts
   2: contacts
   3: contributors
   4: financial institutions
   5: credit files
   6: credit proposals
   19: insurance proposals
  }
  case Module_id of
    1:  begin //Accounts
          Show_Tasks(Module_id,    Master_datasource);
          Show_Notes(Module_id,    Master_datasource);
        end;
    5:  begin //Credit files
          Show_proposals(Module_id, Master_datasource);
          Show_tasks(Module_id, Master_datasource);
          Show_Notes(Module_id, Master_datasource);
          if UniMainModule.User_portal_rights >=2
          then Show_attachments(Module_id, Master_datasource);
          Show_credit_documents(Module_id, Master_datasource);
        end;
    10: begin //Insurance files
          Show_insurance_payments(Module_id, Master_datasource);
          Show_messages(Module_id, Master_datasource);
          //Show_tasks(Module_id, Master_datasource);
          Show_Notes(Module_id, Master_datasource);
          Show_attachments(Module_id, Master_datasource);
        end;
    19: begin //Insurance proposals
          Show_Notes(Module_id, Master_datasource);
        end;
  end;

  PageControlJournal.ActivePageIndex := 0;
end;

procedure TJournalFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TJournalFrm.UpdateStrings;
begin
  str_messages           := LinkedLanguageJournal.GetTextOrDefault('strstr_messages' (* 'Berichten' *) );
  str_payments           := LinkedLanguageJournal.GetTextOrDefault('strstr_payments' (* 'Betalingen' *) );
  strTasks               := LinkedLanguageJournal.GetTextOrDefault('strstrTasks' (* 'Taken' *) );
  strNotes               := LinkedLanguageJournal.GetTextOrDefault('strstrNotes' (* 'Notities' *) );
  str_commission_schemas := LinkedLanguageJournal.GetTextOrDefault('strstr_commission_schemas' (* 'Commissieschema's' *) );
  str_propositions       := LinkedLanguageJournal.GetTextOrDefault('strstr_propositions' (* 'Voorstellen' *) );
  str_relations          := LinkedLanguageJournal.GetTextOrDefault('strstr_relations' (* 'Relaties' *) );
  str_attachments        := LinkedLanguageJournal.GetTextOrDefault('strstr_attachments' (* 'Bijlagen' *) );
end;

end.
