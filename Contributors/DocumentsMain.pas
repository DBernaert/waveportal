unit DocumentsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniBasicGrid,
  uniDBGrid, uniButton, UniThemeButton, uniLabel, uniGUIBaseClasses, uniPanel,
  siComp, siLngLnk;

type
  TDocumentsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    BtnDownload: TUniThemeButton;
    Attachments: TIBCQuery;
    DsAttachments: TDataSource;
    GridAttachments: TUniDBGrid;
    LinkedLanguageDocumentsMain: TsiLangLinked;
    procedure BtnDownloadClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_documents;
  end;

implementation

{$R *.dfm}

uses MainModule, Utils;



{ TDocumentsMainFrm }

procedure TDocumentsMainFrm.BtnDownloadClick(Sender: TObject);
var Destination: string;
begin
  if ((Attachments.Active) and (not Attachments.fieldbyname('Id').isNull))
  then begin
         Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
         if Trim(Attachments.FieldByName('Folder').AsString) <> ''
         then Destination := Destination + Attachments.FieldByName('Folder').AsString + '\';
         if Trim(Attachments.FieldByName('SubFolder').AsString) <> ''
         then Destination := Destination + Attachments.FieldByName('SubFolder').AsString + '\';
         Destination := Destination + Attachments.FieldByName('Destination_name').AsString;

         if FileExists(Destination)
         then UniSession.SendFile(Destination, Utils.EscapeIllegalChars(Attachments.fieldbyname('Original_Name').asString));
       end;
end;

procedure TDocumentsMainFrm.Start_documents;
var SQL: string;
begin
  Attachments.Close;
  SQL :=
  'Select files_archive.id, files_archive.original_name, files_archive.destination_name, ' +
  'files_archive.description, files_archive.date_entered, files_archive.folder, ' +
  'files_archive.subfolder, files_archive.file_type, ';

  case UniMainModule.LanguageManager.ActiveLanguage of
  1: SQL := SQL + 'basictables.dutch typedescription ';
  2: SQL := SQL + 'basictables.french typedescription ';
  3: SQL := SQL + 'basictables.english typedescription ';
  end;

  SQL :=
  SQL +
  'from files_archive ' +
  'left outer join basictables on (files_archive.file_type = basictables.id) ' +
  'where files_archive.companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
  'and files_archive.module_id=3 '  +
  'and files_archive.record_id=:Contributor_id ' +
  'and files_archive.removed=''0'' ' +
  'and files_archive.visible_portal=1';

  SQL := SQL + ' order by files_archive.original_name';

  Attachments.SQL.Text := SQL;

  Attachments.ParamByName('Contributor_id').AsInteger :=
  UniMainModule.Reference_contributor_id;

  Attachments.Open;
end;

end.
