unit CreditFilesMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Vcl.Menus, uniMainMenu, siComp, siLngLnk, Data.DB, MemDS, DBAccess, IBC, uniPanel,
  uniBasicGrid, uniDBGrid, uniBitBtn, uniMenuButton, uniButton, UniThemeButton, uniDBNavigator, uniMultiItem,
  uniComboBox, uniEdit, uniLabel, uniImage, uniGUIBaseClasses, uniDateTimePicker;

type
  TCreditFilesMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ComboStatus: TUniComboBox;
    ContainerFilterRight: TUniContainerPanel;
    ContainerBody: TUniContainerPanel;
    GridCreditFiles: TUniDBGrid;
    PanelJournal: TUniPanel;
    CreditFiles: TIBCQuery;
    CreditFilesID: TLargeintField;
    CreditFilesTOTAL_INVESTMENT: TFloatField;
    CreditFilesOWN_RESOURCES: TFloatField;
    CreditFilesDEMANDED_CREDIT: TFloatField;
    CreditFilesTOTAL_AMOUNT_CREDIT: TFloatField;
    CreditFilesNAME: TWideStringField;
    CreditFilesDEMANDER_NAME: TWideStringField;
    DsCreditFiles: TDataSource;
    Status: TIBCQuery;
    DsStatus: TDataSource;
    LinkedLanguageCreditFilesMain: TsiLangLinked;
    CreditFilesCREDIT_FILE_NUMBER: TWideStringField;
    CreditFilesDATE_CREDIT_FINAL: TDateField;
    CreditFilesREFUSED: TSmallintField;
    EditStartDateRange: TUniDateTimePicker;
    EditEndDateRange: TUniDateTimePicker;
    BtnSearch: TUniThemeButton;
    PopupMenuCreditFilesMain: TUniPopupMenu;
    BtnCopyCreditFile: TUniMenuItem;
    CreditFilesCONTRIBUTOR_FULLNAME: TWideStringField;
    CreditFilesSTATUS_FILE: TWideStringField;
    CreditFilesSTATUS: TLargeintField;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    BtnMore: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    BtnSendPortalInvitation: TUniMenuItem;
    procedure UniFrameCreate(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure GridCreditFilesColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
    procedure LinkedLanguageCreditFilesMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnSearchClick(Sender: TObject);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure GridCreditFilesDrawColumnCell(Sender: TObject; ACol,
      ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnExportListClick(Sender: TObject);
    procedure BtnMoreClick(Sender: TObject);
    procedure BtnCopyCreditFileClick(Sender: TObject);
    procedure GridCreditFilesDblClick(Sender: TObject);
    procedure BtnSendPortalInvitationClick(Sender: TObject);
  private
    { Private declarations }
    JournalFrame: TUniFrame;
    procedure Load_statusses;
    procedure Open_creditFiles;
    procedure CallBackInsertUpdateDemand(Sender: TComponent; AResult: Integer);
    function  Create_copy_creditFile(CreditFile: longint): longint;
  public
    { Public declarations }
    procedure Start_credit_files_management_module;
  end;

var
	str_All_CreditFiles: string = 'Alle kredietdossiers'; // TSI: Localized (Don't modify!)

const
  str_cannot_delete_schemas_present = 'Dossier kan niet verwijderd worden, er zijn schema''s aanwezig.';
  str_delete_credit = 'Verwijderen krediet?';
	str_error_deleting = 'Fout bij het verwijderen: ';
  str_credits = 'Kredieten';
	str_make_copy = 'Kopie aanmaken';
	str_question_make_copy = 'Wenst u een kopie van dit kredietdossier aan te maken?';
  str_error_creating_copy = 'Fout bij het maken van de kopie.';
  str_error_transfer_demanders = 'Fout bij het overplaatsen van de aanvragers.';
  str_error_transfer_property_owners = 'Fout bij het overplaatsen van de pandverstrekkers.';
  str_error_transfer_guarantors = 'Fout bij het overplaatsen van de borgstellers.';

implementation

{$R *.dfm}

uses MainModule, Journal, CreditFilesAdd, Utils, DateUtils, Main, MailAdd;

{ TCreditFilesMainFrm }

procedure TCreditFilesMainFrm.BtnAddClick(Sender: TObject);
begin
  With CreditFilesAddFrm
  do begin
       if Init_creditfile_creation
       then ShowModal(CallBackInsertUpdateDemand)
       else Close;
     end;
end;

procedure TCreditFilesMainFrm.BtnCopyCreditFileClick(Sender: TObject);
var resultCreditFile: longint;
begin
  if (CreditFiles.Active) and (not CreditFiles.fieldbyname('Id').isNull)
  then if MainForm.WaveConfirmBlocking(str_make_copy, str_question_make_copy) = True
       then begin
              resultCreditFile := Create_copy_creditFile(CreditFiles.FieldByName('Id').AsInteger);
              if resultCreditFile <> 0
              then begin
                     UniMainModule.Result_dbAction := 0;
                     With CreditFilesAddFrm
                     do begin
                          if Init_creditfile_modification(resultCreditFile)
                          then ShowModal(CallBackInsertUpdateDemand)
                          else Close;
                        end;
                   end;
            end;
end;

procedure TCreditFilesMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not CreditFiles.Active) or (CreditFiles.fieldbyname('Id').isNull)
  then exit;

  //Check if commission schemes are present
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select count(*) as counter from credit_commission_schemas where credit_file=' + CreditFiles.FieldByName('Id').asString);
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.Fieldbyname('Counter').asInteger > 0
  then begin
         UniMainModule.QWork.Close;
         MainForm.WaveShowWarningToast(str_cannot_delete_schemas_present);
         Exit;
       end
  else UniMainModule.Qwork.Close;

  if MainForm.WaveConfirmBlocking(UniMainModule.Company_name, str_delete_credit)
  then begin
         Try
           UniMainModule.QBatch.Close;
           UniMainModule.QBatch.SQL.Clear;
           UniMainModule.QBatch.SQL.Add('Update credit_files set removed=''1'' where id=' + QuotedStr(CreditFiles.FieldByName('Id').asString));
           UniMainModule.QBatch.ExecSQL;
           if UniMainModule.UpdTr_Batch.Active
           then UniMainModule.UpdTr_Batch.Commit;
           UniMainModule.QBatch.Close;
         Except on E: EDataBaseError
         do begin
              if UniMainModule.UpdTr_Batch.Active
              then UniMainModule.UpdTr_Batch.Rollback;
              UniMainModule.QBatch.Close;
              MainForm.WaveShowErrorToast(str_error_deleting + E.Message);
            end;
         end;
         CreditFiles.Refresh;
       end;
end;

procedure TCreditFilesMainFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridCreditFiles, str_credits, CreditFiles, 'Id');
end;

procedure TCreditFilesMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (CreditFiles.Active) and (not CreditFiles.fieldbyname('Id').isNull)
  then begin
         With CreditFilesAddFrm
         do begin
              if Init_creditfile_modification(CreditFiles.FieldByName('Id').AsInteger)
              then ShowModal(CallBackInsertUpdateDemand)
              else Close;
            end;
       end;
end;

procedure TCreditFilesMainFrm.BtnMoreClick(Sender: TObject);
begin
  PopupMenuCreditFilesMain.PopupBy(BtnMore);
end;

procedure TCreditFilesMainFrm.BtnSearchClick(Sender: TObject);
begin
  Open_creditfiles;
end;

procedure TCreditFilesMainFrm.BtnSendPortalInvitationClick(Sender: TObject);
var ContactId: longint;
begin
  //Opzoeken van het contact
  if (CreditFiles.Active = False) or (CreditFiles.FieldByName('Id').isNull)
  then exit;

  UniMainModule.QWork.Close;

  UniMainModule.QWork.SQL.Text :=
  'Select first 1 * from credit_file_demanders where demander_type=0 and credit_file=:credit_file ' +
  'order by id';

  UniMainModule.QWork.ParamByName('Credit_file').AsInteger :=
  CreditFiles.FieldByName('Id').AsInteger;

  UniMainModule.QWork.Open;

  ContactId := UniMainModule.QWork.FieldByName('Demander_id').AsInteger;

  UniMainModule.QWork.Close;

  //Checken of er een mailadres aanwezig is + updaten status
  UniMainModule.QWork.SQL.Text :=
  'Select * from crm_contacts where id=:id';

  UniMainModule.QWork.ParamByName('Id').AsInteger := ContactId;

  UniMainModule.QWork.Open;

  if Trim(UniMainModule.QWork.FieldByName('Email').AsString) = '' then begin
    UniMainModule.QWork.Close;

    MainForm.WaveShowWarningToast('Er is geen mailadres aanwezig op de fiche van de relatie');
    Exit;
  end;

  UniMainModule.QBatch.Close;

  if Trim(UniMainModule.QWork.FieldByName('Portal_password').AsString) = ''
  then UniMainModule.QBatch.SQL.Text :=
       'Update crm_contacts set portal_active=1, portal_privacy_doc_signed=1, portal_password=:portal_password where id=:id'
  else UniMainModule.QBatch.SQL.Text :=
       'Update crm_contacts set portal_active=1, portal_privacy_doc_signed=1 where id=:id';

  if Trim(UniMainModule.QWork.FieldByName('Portal_password').AsString) = ''
  then UniMainModule.QBatch.ParamByName('Portal_password').AsString :=
       Utils.Crypt(Utils.MakeRandomString(15), 35462);

  UniMainModule.QBatch.ParamByName('Id').AsInteger := ContactId;

  UniMainModule.QBatch.Execute;

  if UniMainModule.UpdTr_Batch.Active
  then UniMainModule.UpdTr_Batch.Commit;

  UniMainModule.QBatch.Close;

  UniMainModule.QWork.Close;

  With MailAddFrm
  do begin
       if Start_mail_module(2, ContactId, ContactId, '', 1002)
       then ShowModal
       else Close;
     end;
end;

procedure TCreditFilesMainFrm.CallBackInsertUpdateDemand(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if CreditFiles.Active
         then begin
                CreditFiles.Refresh;
                CreditFiles.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

function TCreditFilesMainFrm.Create_copy_creditFile(
  CreditFile: longint): longint;
var I:                integer;
    SQL:              string;
begin
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from credit_files where id=' + IntToStr(CreditFile));
  UniMainModule.QWork.Open;

  //Copy the credit file
  UniMainModule.QUpdate.Close;
  UniMainModule.QUpdate.SQL.Clear;
  UniMainModule.QUpdate.SQL.Add('Select first 0 * from credit_files');
  UniMainModule.QUpdate.KeyFields    := 'ID';
  UniMainModule.QUpdate.KeyGenerator := 'GEN_CREDIT_FILES_ID';
  UniMainModule.QUpdate.Open;
  UniMainModule.QUpdate.Append;
  for I := 0 to UniMainModule.QWork.FieldCount -1
  do begin
       if UpperCase(UniMainModule.QWork.Fields[I].FieldName) <> 'ID'
       then UniMainModule.QUpdate.FieldByName(UniMainModule.QWork.Fields[I].FieldName).Value :=
            UniMainModule.QWork.FieldByName(UniMainModule.QWork.Fields[I].FieldName).Value;
     end;

  UniMainModule.QUpdate.FieldByName('Assigned_user_id').AsInteger := UniMainModule.User_id;
  UniMainModule.QUpdate.FieldByName('Modified_User_id').AsInteger := UniMainModule.User_id;
  UniMainModule.QUpdate.FieldByName('Date_Entered').asDateTime    := Now;
  UniMainModule.QUpdate.FieldByName('Date_modified').AsDateTime   := Now;

  UniMainModule.QUpdate.FieldByName('Created_User_id').AsInteger  := UniMainModule.User_id;
  UniMainModule.QUpdate.FieldByName('Assigned_User_id').AsInteger := UniMainModule.User_id;
  UniMainModule.QUpdate.FieldByName('Credit_file_number').Clear;
  UniMainModule.QUpdate.FieldByName('Internal_remarks').Clear;

  UniMainModule.QUpdate.Post;
  Result := UniMainModule.QUpdate.FieldByName('Id').AsInteger;
  Try
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;

    UniMainModule.QUpdate.Close;
    UniMainModule.QUpdate.KeyFields    := '';
    UniMainModule.QUpdate.KeyGenerator := '';
    UniMainModule.QWork.Close;
  except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    UniMainModule.QUpdate.Close;
    UniMainModule.QUpdate.KeyFields    := '';
    UniMainModule.QUpdate.KeyGenerator := '';
    UniMainModule.QWork.Close;
    MainForm.WaveShowErrorToast(str_error_creating_copy);
    Result := 0;
    Exit;
  end;

  //Credit demanders
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from credit_file_demanders where credit_file=' + IntToStr(CreditFile));
  UniMainModule.QWork.Open;

  UniMainModule.QUpdate.Close;
  UniMainModule.QUpdate.SQL.Clear;
  UniMainModule.QUpdate.SQL.Add('Select first 0 * from credit_file_demanders');
  UniMainModule.QUpdate.KeyFields    := 'ID';
  UniMainModule.QUpdate.KeyGenerator := 'GEN_CREDIT_FILE_DEMANDERS_ID';
  UniMainModule.QUpdate.Open;
  UniMainModule.QWork.First;
  while not UniMainModule.QWork.Eof
  do begin
       UniMainModule.QUpdate.Append;
       for I := 0 to UniMainModule.QWork.FieldCount -1
       do begin
            if UpperCase(UniMainModule.QWork.Fields[I].FieldName) <> 'ID'
            then begin
                   if UpperCase(UniMainModule.QWork.Fields[I].FieldName) = 'CREDIT_FILE'
                   then UniMainModule.QUpdate.FieldByName(UniMainModule.QWork.Fields[I].FieldName).Value := Result
                   else UniMainModule.QUpdate.FieldByName(UniMainModule.QWork.Fields[I].FieldName).Value :=
                        UniMainModule.QWork.FieldByName(UniMainModule.QWork.Fields[I].FieldName).Value;
                 end;
          end;
       UniMainModule.QUpdate.Post;
       UniMainModule.QWork.Next;
     end;

  Try
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;

    UniMainModule.QUpdate.Close;
    UniMainModule.QUpdate.KeyFields    := '';
    UniMainModule.QUpdate.KeyGenerator := '';
    UniMainModule.QWork.Close;
  except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    UniMainModule.QUpdate.Close;
    UniMainModule.QUpdate.KeyFields    := '';
    UniMainModule.QUpdate.KeyGenerator := '';
    UniMainModule.QWork.Close;
    MainForm.WaveShowErrorToast(str_error_transfer_demanders);
    Result := 0;
    Exit;
  end;

  //Credit file Lenders
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from credit_file_lenders where credit_file=' + IntToStr(CreditFile));
  UniMainModule.QWork.Open;

  UniMainModule.QWork.First;
  While not UniMainModule.QWork.Eof
  do begin
       UniMainModule.QBatch.Close;
       UniMainModule.QBatch.SQL.Clear;
       SQL := 'Insert into credit_file_lenders(companyid,credit_file,lender_id,lender_type) values(' +
              UniMainModule.QWork.FieldByName('Companyid').asString + ', ' + IntToStr(Result) + ', ' +
              UniMainModule.QWork.FieldByName('Lender_id').AsString + ', ' + UniMainModule.QWork.FieldByName('Lender_type').asString + ')';
       UniMainModule.QBatch.SQL.Add(SQL);
       Try
         UniMainModule.QBatch.ExecSQL;
         if UniMainModule.UpdTr_Batch.Active
         then UniMainModule.UpdTr_Batch.Commit;
         UniMainModule.QBatch.Close;
       Except
         if UniMainModule.UpdTr_Batch.Active
         then UniMainModule.UpdTr_Batch.Rollback;
         UniMainModule.QBatch.Close;
         UniMainModule.QWork.Close;
         MainForm.WaveShowErrorToast(str_error_transfer_property_owners);
         Exit;
       End;
       UniMainModule.QWork.Next;
     end;
  UniMainModule.QWork.Close;

  //Credit file guarantors
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from credit_file_guarantors where credit_file=' + IntToStr(CreditFile));
  UniMainModule.QWork.Open;

  UniMainModule.QWork.First;
  While not UniMainModule.QWork.Eof
  do begin
       UniMainModule.QBatch.Close;
       UniMainModule.QBatch.SQL.Clear;
       SQL := 'Insert into credit_file_guarantors(companyid,credit_file,guarantor_id,guarantor_type) values(' +
               UniMainModule.QWork.FieldByName('Companyid').asString + ', ' + IntToStr(Result) + ', ' +
               UniMainModule.QWork.FieldByName('Guarantor_id').AsString + ', ' + UniMainModule.QWork.FieldByName('Guarantor_type').asString + ')';
       UniMainModule.QBatch.SQL.Add(SQL);
       Try
         UniMainModule.QBatch.ExecSQL;
         if UniMainModule.UpdTr_Batch.Active
         then UniMainModule.UpdTr_Batch.Commit;
         UniMainModule.QBatch.Close;
       Except
         if UniMainModule.UpdTr_Batch.Active
         then UniMainModule.UpdTr_Batch.Rollback;
         UniMainModule.QBatch.Close;
         UniMainModule.QWork.Close;
         MainForm.WaveShowErrorToast(str_error_transfer_guarantors);
         Result := 0;
         Exit;
       End;
       UniMainModule.QWork.Next;
     end;
  UniMainModule.QWork.Close;
end;

procedure TCreditFilesMainFrm.EditSearchKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13
  then begin
         Key := #0;
         BtnSearchClick(Sender);
       end;
end;

procedure TCreditFilesMainFrm.GridCreditFilesColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
begin
  if Direction
  then CreditFiles.IndexFieldNames := Column.FieldName + ' asc'
  else CreditFiles.IndexFieldNames := Column.FieldName + ' desc';
end;

procedure TCreditFilesMainFrm.GridCreditFilesDblClick(Sender: TObject);
begin
  if (BtnModify.Visible) and (BtnModify.Enabled)
  then BtnModifyClick(Sender);
end;

procedure TCreditFilesMainFrm.GridCreditFilesDrawColumnCell(Sender: TObject;
  ACol, ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if CreditFiles.FieldByName('Refused').AsInteger = 1
  then begin
         Attribs.Font.Color := UniMainModule.Color_red;
         Attribs.Font.Style := [fsStrikeOut];
       end;
end;

procedure TCreditFilesMainFrm.LinkedLanguageCreditFilesMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TCreditFilesMainFrm.Load_statusses;
var SQL: string;
begin
  Status.Close;
  Status.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=2008 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SQL := SQL + ' order by DUTCH';
    2: SQL := SQL + ' order by FRENCH';
    3: SQL := SQL + ' order by ENGLISH';
  end;
  Status.SQL.Add(SQL);
  Status.Open;

  Status.First;
  ComboStatus.Items.Clear;
  ComboStatus.Items.Add(str_All_CreditFiles);
  while not Status.Eof
  do begin
       case UniMainModule.LanguageManager.ActiveLanguage of
         1: ComboStatus.Items.Add(Status.FieldByName('Dutch').asString);
         2: ComboStatus.Items.Add(Status.FieldByName('French').AsString);
         3: ComboStatus.Items.Add(Status.FieldByName('English').AsString);
       end;
       Status.Next;
     end;
  ComboStatus.ItemIndex := 0;
end;

procedure TCreditFilesMainFrm.Open_creditFiles;
var SQL: string;
begin
  GridCreditFiles.BeginUpdate;
  CreditFiles.Close;
  CreditFiles.SQL.Clear;
  SQL := 'Select t.* from ' +
         '(SELECT CREDIT_FILES.ID, CREDIT_FILES.CREDIT_FILE_NUMBER, CREDIT_FILES.TOTAL_INVESTMENT, CREDIT_FILES.OWN_RESOURCES, ' +
         'CREDIT_FILES.DEMANDED_CREDIT, CREDIT_FILES.TOTAL_AMOUNT_CREDIT, CREDIT_FILES.DATE_CREDIT_FINAL, CREDIT_FILES.REFUSED, ' +
         'CREDIT_FILES.STATUS, ' +
         'CREDIT_FINANCIALINSTITUTIONS.NAME, CREDIT_FILES.REMOVED, CREDIT_FILES.CONTRIBUTOR_ID, CREDIT_FILES.COMPANYID, ' +
         'CREDIT_FILES.AGENT_ID, ' +
         'COALESCE(CREDIT_CONTRIBUTORS.last_name, '''') || '' '' || COALESCE(CREDIT_CONTRIBUTORS.first_name, '''') AS CONTRIBUTOR_FULLNAME, ';

  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SQL := SQL + ' BASICTABLES.DUTCH AS STATUS_FILE, ';
    2: SQL := SQL + ' BASICTABLES.FRENCH AS STATUS_FILE, ';
    3: SQL := SQL + ' BASICTABLES.ENGLISH AS STATUS_FILE, ';
  end;

  SQL := SQL +
         '(Select ' +
         '  Case ' +
         '    WHEN CRDEMANDER.DEMANDER_TYPE = 0 THEN COALESCE(SP.LAST_NAME, '''') || '' '' || COALESCE(SP.FIRST_NAME, '''') ' +
         '    WHEN CRDEMANDER.DEMANDER_TYPE = 1 THEN C.NAME ' +
         '  END AS DEMANDER_NAME ' +
         'FROM Credit_file_demanders CRDEMANDER ' +
         'LEFT OUTER JOIN Crm_accounts C on CRDEMANDER.DEMANDER_ID = C.ID ' +
         'LEFT OUTER JOIN Crm_contacts SP on CRDEMANDER.DEMANDER_ID = SP.ID ' +
         'WHERE CRDEMANDER.ID = (Select first 1 id from credit_file_demanders crdemander where crdemander.CREDIT_FILE = credit_files.ID)) ' +
         'FROM ' +
         'CREDIT_FILES ' +
         'LEFT OUTER JOIN BASICTABLES ON (CREDIT_FILES.STATUS = BASICTABLES.ID) ' +
         'LEFT OUTER JOIN CREDIT_FINANCIALINSTITUTIONS ON (CREDIT_FILES.FINANCIAL_INSTITUTION = CREDIT_FINANCIALINSTITUTIONS.ID) ' +
         'LEFT OUTER JOIN credit_contributors ON (CREDIT_FILES.contributor_id = credit_contributors.ID)) as t ';

  if UniMainModule.Responsable_contributor_credit = 0
  then SQL := SQL + 'WHERE t.COMPANYID='    + IntToStr(UniMainModule.Company_Id) + ' ' +
                    'AND t.REMOVED=''0'' ' +
                    'AND (t.CONTRIBUTOR_ID=' + IntToStr(UniMainModule.Reference_contributor_id) +
                    ' OR t.AGENT_ID=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '

  else SQL := SQL + 'WHERE t.COMPANYID='    + IntToStr(UniMainModule.Company_Id) + ' ' +
                    'AND t.REMOVED=''0'' ' +
                    'AND (t.CONTRIBUTOR_ID=' + IntToStr(UniMainModule.Reference_contributor_id)    +
                    ' OR t.CONTRIBUTOR_ID=' + IntToStr(UniMainModule.Responsable_contributor_credit) +
                    ' OR t.AGENT_ID=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';


  if ComboStatus.ItemIndex > 0
  then begin
         Status.First;
         Status.MoveBy(ComboStatus.ItemIndex-1);
         SQL := SQL + ' AND t.STATUS=' + Status.fieldbyname('Id').asString;
       end;

  if EditStartDateRange.DateTime <> 0
  then SQL := SQL + ' AND t.DATE_CREDIT_FINAL >= ' + QuotedStr(FormatDateTime('YYYY-MM-DD', IncDay(EditStartDateRange.DateTime, -1)));

  if EditEndDateRange.DateTime <> 0
  then SQL := SQL + ' AND t.DATE_CREDIT_FINAL < ' + QuotedStr(FormatDateTime('YYYY-MM-DD', IncDay(EditEndDateRange.DateTime, 1)));

  if Trim(EditSearch.Text) <> ''
  then begin
         SQL :=
         SQL + ' and (UPPER(REPLACE(TRIM(t.demander_name), '' '', '''')) like '             +
                      Utils.SearchValue(EditSearch.Text) + ' OR '                           +
                     'UPPER(REPLACE(TRIM(t.name), '' '', '''')) like '                      +
                      Utils.SearchValue(EditSearch.text) + ' OR '                           +
                     'UPPER(REPLACE(TRIM(t.status), '' '', '''')) like '                    +
                      Utils.SearchValue(EditSearch.text) + ' OR '                           +
                     'UPPER(REPLACE(TRIM(t.credit_file_number), '' '', '''')) like '        +
                      Utils.SearchValue(EditSearch.Text) + ')';
       end;

  CreditFiles.SQL.Add(SQL);
  CreditFiles.Open;
  GridCreditFiles.EndUpdate;
end;

procedure TCreditFilesMainFrm.Start_credit_files_management_module;
begin
  if UniMainModule.User_allow_credit_edit = False
  then begin
         BtnAdd.Visible             := False;
         BtnDelete.Visible          := False;
         BtnMore.Visible            := False;
         BtnCopyCreditFile.Enabled  := False;
         BtnModify.Left             := 0;
         BtnExportList.Left         := 32;
         ContainerFilterRight.Width := 57;
       end;

  if (UniMainModule.User_portal_rights = 0) or
     (UniMainModule.User_portal_rights = 4)
  then begin
         BtnAdd.Visible             := False;
         BtnModify.Visible          := False;
         BtnDelete.Visible          := False;
         BtnCopyCreditFile.Enabled  := False;
         BtnExportList.Left         := 0;
         ContainerFilterRight.Width := 25;
       end;

  EditStartDateRange.DateTime := 0;
  EditEndDateRange.DateTime   := 0;
  Load_statusses;
  Open_creditFiles;
  EditSearch.SetFocus;
  JournalFrame := TJournalFrm.Create(Self);
  With (JournalFrame as TJournalFrm)
  do begin
       Start_journal(5, DsCreditFiles);
       Parent := PanelJournal;
     end;
end;

procedure TCreditFilesMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  PanelJournal.JSInterface.JSConfig('stateId', ['PanelJournal_stateId_unique']);
  with PanelJournal
  do JSInterface.JSConfig('stateId', [Self.Name + Name]);
end;

procedure TCreditFilesMainFrm.UpdateStrings;
begin
  str_All_CreditFiles := LinkedLanguageCreditFilesMain.GetTextOrDefault('strstr_All_CreditFiles' (* 'Alle kredietdossiers' *) );
end;

end.

