unit ContributorDocumentsMain;
interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniImage, uniGUIBaseClasses,
  uniPanel, uniButton, UniThemeButton, uniDBNavigator, uniBasicGrid,
  uniDBGrid, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;
type
  TContributorDocumentsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerBodyDocuments: TUniContainerPanel;
    ContainerDocuments1: TUniContainerPanel;
    ContainerFilterDocuments1: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    GridDocuments1: TUniDBGrid;
    DocumentsBrowse: TIBCQuery;
    DsDocumentsBrowse: TDataSource;
    DocumentsBrowseID: TLargeintField;
    DocumentsBrowseCREDIT_DOCUMENT_BATCH: TLargeintField;
    DocumentsBrowseDOCUMENT_DATE: TDateField;
    DocumentsBrowseDOCUMENT_TOTAL_AMOUNT: TFloatField;
    DocumentsBrowseBATCH_MONTH: TIntegerField;
    DocumentsBrowseBATCH_YEAR: TIntegerField;
    LinkedLanguageContributorDocumentsMain: TsiLangLinked;
    BtnPrintDocuments1: TUniThemeButton;
    DocumentsBrowseBATCH_QUARTER: TIntegerField;
    procedure BtnPrintDocuments1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_contributor_documents;
  end;

implementation

{$R *.dfm}

uses MainModule, frxClass, ServerModule, ReportRepository, ReportPreview;

{ TContributorDocumentsMainFrm }

procedure TContributorDocumentsMainFrm.BtnPrintDocuments1Click(
  Sender: TObject);
var Memo: TfrxMemoView;
    line: string;
    itemfound: boolean;
    SQL: string;
    AUrl: string;
begin
  if DocumentsBrowse.FieldByName('Id').isNull
  then exit;

  UniMainModule.Contributor_reportdata.Close;
  UniMainModule.Contributor_reportdata.SQL.Clear;
  SQL := 'SELECT * FROM credit_contributors where Id=' + IntToStr(UniMainModule.Reference_contributor_id);
  UniMainModule.Contributor_reportdata.SQL.Add(SQL);
  UniMainModule.Contributor_reportdata.Open;

  UniMainModule.Contributor_reportdetaildata.Close;
  UniMainModule.Contributor_reportdetaildata.SQL.Clear;
  SQL := 'SELECT CREDIT_COMMISSION_ACCOUNT.ID, CREDIT_COMMISSION_ACCOUNT.CONTRIBUTOR, CREDIT_COMMISSION_ACCOUNT.CREDIT_FILE, ' +
         'CREDIT_COMMISSION_ACCOUNT.FREE_DESCRIPTION, CREDIT_COMMISSION_ACCOUNT.AMOUNT, CREDIT_FILES.DATE_CREDIT_FINAL, ' +
         'CREDIT_FILES.TOTAL_AMOUNT_CREDIT, CREDIT_FILES.PROPERTY_ADDRESS, credit_financialinstitutions.name financialinstname, ' +
         'CREDIT_FILES.WITHHOLD_COMMISSION, ' +
         '(Select ' +
         ' Case ' +
         ' WHEN CRDEMANDER.DEMANDER_TYPE = 0 THEN COALESCE(SP.LAST_NAME, '''') || '' '' || COALESCE(SP.FIRST_NAME, '''') ' +
         ' WHEN CRDEMANDER.DEMANDER_TYPE = 1 THEN C.NAME ' +
         ' END AS DEMANDER_NAME ' +
         'FROM Credit_file_demanders CRDEMANDER ' +
         'LEFT OUTER JOIN Crm_accounts C on CRDEMANDER.DEMANDER_ID = C.ID ' +
         'LEFT OUTER JOIN Crm_contacts SP on CRDEMANDER.DEMANDER_ID = SP.ID ' +
         'WHERE CRDEMANDER.ID = (Select first 1 id from credit_file_demanders crdemander where crdemander.CREDIT_FILE = credit_commission_account.credit_file)) ' +
         'from credit_financialinstitutions ' +
         'right outer join credit_files on (credit_financialinstitutions.id = credit_files.financial_institution) ' +
         'right outer join credit_commission_account on (credit_files.id = credit_commission_account.credit_file) ' +
         'WHERE CREDIT_COMMISSION_ACCOUNT.DOCUMENT=' + DocumentsBrowse.FieldByName('Id').AsString + ' ' +
         'ORDER BY CREDIT_FILES.DATE_CREDIT_FINAL';

  UniMainModule.Contributor_reportdetaildata.SQL.add(SQL);
  UniMainModule.Contributor_reportdetaildata.Open;
  AUrl := '';
  with ReportRepositoryFrm
  do begin
       UniMainModule.QWork.Close;
       UniMainModule.QWork.SQL.Clear;
       UniMainModule.QWork.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
       UniMainModule.QWork.Open;
       Memo := ReportBorderellen.FindObject('LblCompanyName') as TfrxMemoView;
       Memo.Text := UniMainModule.QWork.FieldByName('Name').AsString;
       Memo := ReportBorderellen.FindObject('LblCompanyAddress1') as TfrxMemoView;
       Memo.Text := UniMainModule.QWork.FieldByName('Address').AsString;
       Memo := ReportBorderellen.FindObject('LblCompanyAddress2') as TfrxMemoView;
       Memo.Text := Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' +
                         UniMainModule.QWork.FieldByName('City').asString);
       Memo := ReportBorderellen.FindObject('LblCompanyContact') as TfrxMemoView;
       line      := '';

       if Trim(UniMainModule.QWork.FieldByName('Phone').AsString) <> ''
       then begin
              line := Trim(UniMainModule.QWork.FieldByName('Phone').AsString);
              itemfound := True;
            end
       else itemfound := False;

       if Trim(UniMainModule.QWork.FieldByName('Email').asString) <> ''
       then begin
              if itemfound = True
              then begin
                     line := line + ' - ' + Trim(UniMainModule.QWork.FieldByName('Email').AsString);
                     itemfound := True;
                   end
              else begin
                     line := line + Trim(UniMainModule.QWork.FieldByName('Email').AsString);
                     itemfound := True;
                   end;
            end;

       if Trim(UniMainModule.QWork.FieldByName('Website').asString) <> ''
       then begin
              if itemfound = True
              then line := line + ' - ' + Trim(UniMainModule.QWork.FieldByName('Website').AsString)
              else line := line + Trim(UniMainModule.QWork.FieldByName('Website').AsString);
            end;

       Memo.Text := Line;
       Memo := ReportBorderellen.FindObject('LblReportTile') as TfrxMemoView;

       if UniMainModule.Statements3Months = True
       then Memo.Text := 'Overzicht commissies periode ' +
                         DocumentsBrowse.FieldByName('Batch_quarter').AsString + ' - ' +
                         DocumentsBrowse.FieldByName('Batch_year').AsString
       else Memo.Text := 'Overzicht commissies periode ' +
                         DocumentsBrowse.FieldByName('Batch_month').AsString + ' - ' +
                         DocumentsBrowse.FieldByName('Batch_year').AsString;

       Memo := ReportBorderellen.FindObject('LblFsmaNumber') as TfrxMemoView;
       Memo.Text := 'Fsma nummer: ' + UniMainModule.QWork.FieldByName('Fsma_number').AsString;
       Memo := ReportBorderellen.FindObject('LblCompanyNumber') as TfrxMemoView;
       Memo.Text := 'Ondernemingsnummer: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString;
       Memo := ReportBorderellen.FindObject('LblDocDate') as TfrxMemoView;
       Memo.Text := DateToStr(DocumentsBrowse.FieldByName('Document_date').AsDateTime);
       UniMainModule.QWork.Close;
       ReportBorderellen.PrintOptions.ShowDialog            := False;
       ReportBorderellen.ShowProgress                       := False;
       ReportBorderellen.EngineOptions.SilentMode           := True;
       ReportBorderellen.EngineOptions.EnableThreadSafe     := True;
       ReportBorderellen.EngineOptions.DestroyForms         := False;
       ReportBorderellen.EngineOptions.UseGlobalDataSetList := False;
       PdfExport.Background                                 := True;
       PdfExport.ShowProgress                               := False;
       PdfExport.ShowDialog                                 := False;
       PdfExport.FileName                                   := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', AUrl, True);
       PdfExport.DefaultPath                                := '';
       ReportBorderellen.PreviewOptions.AllowEdit           := False;
       ReportBorderellen.PrepareReport;
       ReportBorderellen.Export(PdfExport);
       UniMainModule.Contributor_reportdetaildata.Close;
       UniMainModule.Contributor_reportdata.Close;
     end;
  if Trim(AUrl) <> ''
  then with ReportPreviewFrm
       do begin
            Preview_document(AUrl);
            ShowModal();
          end;
end;

procedure TContributorDocumentsMainFrm.Start_contributor_documents;
var SQL: string;
begin
  if UniMainModule.Statements3Months = True
  then begin
         GridDocuments1.Columns[1].Title.Caption := 'Kwartaal';
         GridDocuments1.Columns[1].FieldName     := 'BATCH_QUARTER';
       end;

  DocumentsBrowse.Close;
  DocumentsBrowse.SQL.Clear;
  SQL := 'SELECT CREDIT_DOCUMENT.ID, CREDIT_DOCUMENT.CREDIT_DOCUMENT_BATCH, CREDIT_DOCUMENT.DOCUMENT_DATE, ' +
         'CREDIT_DOCUMENT.DOCUMENT_TOTAL_AMOUNT, CREDIT_DOCUMENT_BATCH.BATCH_MONTH, CREDIT_DOCUMENT_BATCH.BATCH_YEAR, ' +
         'CREDIT_DOCUMENT_BATCH.BATCH_QUARTER ' +
         'FROM CREDIT_DOCUMENT ' +
         'LEFT OUTER JOIN CREDIT_DOCUMENT_BATCH ON (CREDIT_DOCUMENT.CREDIT_DOCUMENT_BATCH = CREDIT_DOCUMENT_BATCH.ID) ' +
         'WHERE CREDIT_DOCUMENT.DOCUMENT_CONTRIBUTOR=' + IntToStr(UniMainModule.Reference_contributor_id) + ' ' +
         'ORDER BY CREDIT_DOCUMENT_BATCH.BATCH_YEAR desc, CREDIT_DOCUMENT_BATCH.BATCH_MONTH desc, CREDIT_DOCUMENT.DOCUMENT_DATE desc';
  DocumentsBrowse.SQL.Add(SQL);
  DocumentsBrowse.Open;
end;

end.
