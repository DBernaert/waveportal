﻿unit CommissionsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniImage, uniGUIBaseClasses,
  uniPanel, uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC,
  uniButton, UniThemeButton, uniDBNavigator, uniMultiItem, uniComboBox,
  uniEdit, siComp, siLngLnk;

type
  TCommissionsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    CommissionBrowse: TIBCQuery;
    CommissionBrowsePeriod: TWideStringField;
    CommissionBrowseCONTRIBUTOR: TLargeintField;
    CommissionBrowseCREDIT_FILE: TLargeintField;
    CommissionBrowseAMOUNT: TFloatField;
    CommissionBrowseDATE_CREDIT_FINAL: TDateField;
    CommissionBrowseTOTAL_AMOUNT_CREDIT: TFloatField;
    CommissionBrowsePROPERTY_ADDRESS: TWideStringField;
    CommissionBrowseACCOUNT_YEAR: TIntegerField;
    CommissionBrowseACCOUNT_MONTH: TIntegerField;
    CommissionBrowseDEMANDER_NAME: TWideStringField;
    DsCommissionBrowse: TDataSource;
    GridCommissions: TUniDBGrid;
    LblYear: TUniLabel;
    LinkedLanguageCommissionsMain: TsiLangLinked;
    BtnExpand: TUniThemeButton;
    BtnCollapse: TUniThemeButton;
    BtnPreviousYear: TUniThemeButton;
    BtnNextYear: TUniThemeButton;
    procedure GridCommissionsColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure GridCommissionsColumnSummaryTotal(Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs; var Result: string);
    procedure GridCommissionsColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure GridCommissionsColumnSummaryResult(Column: TUniDBGridColumn;
      GroupFieldValue: Variant; Attribs: TUniCellAttribs;
      var Result: string);
    procedure BtnCollapseClick(Sender: TObject);
    procedure BtnExpandClick(Sender: TObject);
    procedure BtnNextYearClick(Sender: TObject);
    procedure BtnPreviousYearClick(Sender: TObject);
  private
    { Private declarations }
    Period: integer;
    procedure Load_commissions;
  public
    { Public declarations }
    procedure Start_contributors_commission;
  end;

implementation

{$R *.dfm}

uses MainModule, DateUtils;



{ TCommissionsMainFrm }

procedure TCommissionsMainFrm.BtnCollapseClick(Sender: TObject);
begin
  GridCommissions.JSInterface.JSCall('view.features[0].collapseAll', []);
end;

procedure TCommissionsMainFrm.BtnExpandClick(Sender: TObject);
begin
  GridCommissions.JSInterface.JSCall('view.features[0].expandAll', []);
end;

procedure TCommissionsMainFrm.BtnNextYearClick(Sender: TObject);
begin
  Period := Period + 1;
  Load_commissions;
end;

procedure TCommissionsMainFrm.BtnPreviousYearClick(Sender: TObject);
begin
  Period := Period - 1;
  Load_commissions;
end;

procedure TCommissionsMainFrm.GridCommissionsColumnSort(
  Column: TUniDBGridColumn; Direction: Boolean);
begin
  if SameText(Column.FieldName, 'period')
  then CommissionBrowse.IndexFieldNames := 'ACCOUNT_YEAR;ACCOUNT_MONTH';
end;

procedure TCommissionsMainFrm.GridCommissionsColumnSummary(
  Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(UpperCase(Column.FieldName), 'AMOUNT')
  then begin
         if Column.AuxValue = NULL
         then Column.AuxValue:=0.0;
         Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
       end;
end;

procedure TCommissionsMainFrm.GridCommissionsColumnSummaryResult(
  Column: TUniDBGridColumn; GroupFieldValue: Variant;
  Attribs: TUniCellAttribs; var Result: string);
var F : Currency;
begin
  if SameText(UpperCase(Column.FieldName), 'AMOUNT')
  then begin
         F := Column.AuxValue;
         if Column.AuxValues[1] = NULL
         then Column.AuxValues[1] := 0;
         Column.AuxValues[1] := Column.AuxValues[1] + F;
         Result:= FormatCurr('0,0.00 €', F);
         Attribs.Font.Style := [fsBold];
         Attribs.Color      := UniMainModule.Color_green;
         Attribs.Font.Color := ClWhite;
       end;
  Column.AuxValue := NULL;
end;

procedure TCommissionsMainFrm.GridCommissionsColumnSummaryTotal(
  Column: TUniDBGridColumn; Attribs: TUniCellAttribs; var Result: string);
var F : Currency;
begin
  if SameText(UpperCase(Column.FieldName), 'AMOUNT')
  then begin
         F := Column.AuxValues[1];
         Result:= 'Σ: ' + FormatCurr('0,0.00 €', F);
         Attribs.Font.Color := clWhite;
         Attribs.Font.Style := [fsBold];
         Attribs.Color := UniMainModule.color_darkgray;
       end;
end;

procedure TCommissionsMainFrm.Load_commissions;
begin
  LblYear.Caption := IntToStr(Period);
  CommissionBrowse.Close;
  CommissionBrowse.ParamByName('Contributor').AsInteger  := UniMainModule.Reference_contributor_id;
  CommissionBrowse.ParamByName('Contributor2').AsInteger := UniMainModule.Responsable_contributor_credit;
  CommissionBrowse.ParamByName('AccountYear').AsInteger  := Period;
  CommissionBrowse.Open;
end;

procedure TCommissionsMainFrm.Start_contributors_commission;
begin
  Period := YearOf(Now);
  Load_commissions;
end;

end.
