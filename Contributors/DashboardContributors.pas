unit DashboardContributors;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniImage, uniGUIBaseClasses, uniPanel, UniFSGoogleChart, uniBasicGrid,
  uniDBGrid, uniHTMLFrame, uniButton, UniThemeButton, Data.DB, MemDS, VirtualTable, DBAccess, IBC,
  siComp, siLngLnk;

type
  TDashboardContributorsFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerHeaderRight: TUniContainerPanel;
    LblYear: TUniLabel;
    ContainerHeaderLeft: TUniContainerPanel;
    LblTitle: TUniLabel;
    CDS: TVirtualTable;
    CDSMaand: TStringField;
    CDSProductie: TCurrencyField;
    Tasks: TIBCQuery;
    TasksID: TLargeintField;
    TasksMODULE_ID: TLargeintField;
    TasksRECORD_ID: TLargeintField;
    TasksTITLE: TWideStringField;
    TasksTASK_START: TDateField;
    TasksTASK_END: TDateField;
    TasksTASK_STATUS: TIntegerField;
    TasksTASK_STATUS_DESCRIPTION: TWideStringField;
    DsTasks: TDataSource;
    Calls: TIBCQuery;
    CallsID: TLargeintField;
    CallsMODULE_ID: TLargeintField;
    CallsRECORD_ID: TLargeintField;
    CallsTITLE: TWideStringField;
    CallsCALL_START: TDateField;
    CallsLEFT_VOICEMAIL: TIntegerField;
    CallsDIRECTION: TIntegerField;
    CallsPRIORITY_CODE: TIntegerField;
    CallsCALL_STATUS: TIntegerField;
    CallsCALL_STATUS_DESCRIPTION: TWideStringField;
    CallsCALL_RESULT_DESCRIPTION: TWideStringField;
    CallsPRIORITY_DESCRIPTION: TWideStringField;
    DsCalls: TDataSource;
    PanelContainer: TUniPanel;
    ContainerTop: TUniContainerPanel;
    ContainerGeneral: TUniContainerPanel;
    HtmlFrame: TUniHTMLFrame;
    ContainerEvolution: TUniContainerPanel;
    ContainerHeaderEvolution: TUniContainerPanel;
    ContainerEmptyHeaderEvolutions: TUniContainerPanel;
    ContainerTitleEvolution: TUniContainerPanel;
    LblEvolution: TUniLabel;
    ChartEvolution: TUniFSGoogleChart;
    ContainerBottom: TUniContainerPanel;
    ContainerTasks: TUniContainerPanel;
    ContainerHeaderTask: TUniContainerPanel;
    ContainerEmptyHeaderTasks: TUniContainerPanel;
    ContainerTitleTasks: TUniContainerPanel;
    LblTasks: TUniLabel;
    ContainerBodyTasks: TUniContainerPanel;
    GridTasks: TUniDBGrid;
    ContainerCalls: TUniContainerPanel;
    ContainerHeaderCalls: TUniContainerPanel;
    ContainerEmptyHeaderCalls: TUniContainerPanel;
    ContainerLblPhones: TUniContainerPanel;
    LblCalls: TUniLabel;
    GridCalls: TUniDBGrid;
    LinkedLanguageDashboardContributors: TsiLangLinked;
    ContainerPhoneHeaderRight: TUniContainerPanel;
    ContainerTasksHeaderRight: TUniContainerPanel;
    BtnEditTask: TUniThemeButton;
    BtnEditPhone: TUniThemeButton;
    BtnPreviousYear: TUniThemeButton;
    BtnNextYear: TUniThemeButton;
    procedure BtnNextYearClick(Sender: TObject);
    procedure BtnPreviousYearClick(Sender: TObject);
    procedure GridTasksDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
    procedure GridCallsDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
    procedure LinkedLanguageDashboardContributorsChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure BtnEditPhoneClick(Sender: TObject);
    procedure BtnEditTaskClick(Sender: TObject);
    procedure GridCallsFieldImageURL(const Column: TUniDBGridColumn;
      const AField: TField; var OutImageURL: string);
  private
    { Private declarations }
    Period: integer;
    procedure Load_statistics;
    procedure CallBackInsertUpdateCallsTable(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateTasksTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_dashboard_contributors;
  end;

var
	strNumberOfCredits:          string = '# kredieten'; // TSI: Localized (Don't modify!)
	strEuroCredits:              string = '&euro; kredieten'; // TSI: Localized (Don't modify!)
	strEuroCommission:           string = '&euro; commissie'; // TSI: Localized (Don't modify!)
	str_january:                 string = 'Januari'; // TSI: Localized (Don't modify!)
	str_february:                string = 'Februari'; // TSI: Localized (Don't modify!)
	str_march:                   string = 'Maart'; // TSI: Localized (Don't modify!)
	str_april:                   string = 'April'; // TSI: Localized (Don't modify!)
	str_may:                     string = 'Mei'; // TSI: Localized (Don't modify!)
	str_june:                    string = 'Juni'; // TSI: Localized (Don't modify!)
	str_july:                    string = 'Juli'; // TSI: Localized (Don't modify!)
	str_august:                  string = 'Augustus'; // TSI: Localized (Don't modify!)
	str_september:               string = 'September'; // TSI: Localized (Don't modify!)
	str_october:                 string = 'Oktober'; // TSI: Localized (Don't modify!)
	str_november:                string = 'November'; // TSI: Localized (Don't modify!)
	str_december:                string = 'December'; // TSI: Localized (Don't modify!)
	str_not_started:             string = 'Niet gestart'; // TSI: Localized (Don't modify!)
	str_in_progress:             string = 'Wordt uitgevoerd'; // TSI: Localized (Don't modify!)
	str_completed:               string = 'Voltooid'; // TSI: Localized (Don't modify!)
	str_waiting_for:             string = 'Wachten op iemand anders'; // TSI: Localized (Don't modify!)
	str_delayed:                 string = 'Uitgesteld'; // TSI: Localized (Don't modify!)
	str_call_open:               string = 'Open'; // TSI: Localized (Don't modify!)
	str_call_completed:          string = 'Afgewerkt'; // TSI: Localized (Don't modify!)
	str_call_result_open:        string = 'Open'; // TSI: Localized (Don't modify!)
	str_call_result_executed:    string = 'Uitgevoerd'; // TSI: Localized (Don't modify!)
	str_call_result_cancelled:   string = 'Geannuleerd'; // TSI: Localized (Don't modify!)
	str_call_result_received:    string = 'Ontvangen'; // TSI: Localized (Don't modify!)
	str_call_priority_low:       string = 'Laag'; // TSI: Localized (Don't modify!)
	str_call_priority_normal:    string = 'Normaal'; // TSI: Localized (Don't modify!)
	str_call_priority_high:      string = 'Hoog'; // TSI: Localized (Don't modify!)

implementation

{$R *.dfm}

uses DateUtils, MainModule, Main, TasksAdd, CallsAdd;

{ TDashboardContributorsFrm }

procedure TDashboardContributorsFrm.BtnEditTaskClick(Sender: TObject);
begin
  if (Tasks.Active) and (not Tasks.FieldByName('Id').IsNull)
  then begin
         With TasksAddFrm
         do begin
              if Init_task_modification(Tasks.FieldByName('Id').AsInteger) = True
              then ShowModal(CallBackInsertUpdateTasksTable)
              else Close;
            end;
       end;
end;

procedure TDashboardContributorsFrm.BtnNextYearClick(Sender: TObject);
begin
  Period := Period + 1;
  Load_statistics;
end;

procedure TDashboardContributorsFrm.BtnPreviousYearClick(Sender: TObject);
begin
  Period := Period - 1;
  Load_statistics;
end;

procedure TDashboardContributorsFrm.GridCallsDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
  Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if (UpperCase(Column.FieldName) = 'CALL_START')
  then begin
         if ((Calls.FieldByName('Call_start').AsDateTime < Date) and
             (Calls.FieldByName('Call_status').AsInteger = 0))
         then begin
                Attribs.Color      := UniMainModule.Color_red;
                Attribs.Font.Color := ClWhite;
              end;
       end;

  if UpperCase(Column.FieldName) = 'PRIORITY_DESCRIPTION'
  then begin
         Case Calls.fieldbyname('Priority_code').AsInteger of
         0: begin
              Attribs.Color      := UniMainModule.Color_yellow;
              Attribs.Font.Color := ClWhite;
            end;
         2: begin
              Attribs.Color      := UniMainModule.Color_red;
              Attribs.Font.Color := ClWhite;
            end;
         end;
       end;
end;

procedure TDashboardContributorsFrm.GridCallsFieldImageURL(
  const Column: TUniDBGridColumn; const AField: TField;
  var OutImageURL: string);
begin
  if SameText(UpperCase(AField.FieldName), 'LEFT_VOICEMAIL')
  then begin
         Case AField.AsInteger of
         0:   OutImageURL := 'images/wv_check_white.png';
         1:   OutImageURL := 'images/wv_check_green.png';
         else OutImageURL := 'images/wv_check_white.png';
         End;
       end;

  if SameText(UpperCase(AField.FieldName), 'DIRECTION')
  then begin
         Case AField.AsInteger of
         0: OutImageURL := 'images/wv_arrow_right_12.png';
         1: OutImageURL := 'images/wv_arrow_left_12.png';
         End;
       end;
end;

procedure TDashboardContributorsFrm.GridTasksDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
  Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if (UpperCase(Column.FieldName) = 'TASK_END')
  then begin
         if ((Tasks.FieldByName('Task_end').AsDateTime < Date) and
             (Tasks.FieldByName('Task_status').AsInteger <> 2))
         then begin
                Attribs.Color      := UniMainModule.Color_red;
                Attribs.Font.Color := ClWhite;
              end;
       end;
end;

procedure TDashboardContributorsFrm.LinkedLanguageDashboardContributorsChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TDashboardContributorsFrm.Load_statistics;
var SQL: string;
    DisplayValue: currency;
    DisplayText: string;
    TotalCommission: Currency;
begin
  LblYear.Caption := IntToStr(Period);

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select count(*) as numberofcredits, sum(credit_files.TOTAL_AMOUNT_CREDIT) as totalcredit, ' +
         'sum(credit_files.TOTAL_AMOUNT_COMMISSION) as totalcommission ' +
         'from credit_files ' +
         'WHERE credit_files.COMPANYID='        + IntToStr(UniMainModule.Company_id)               + ' AND ' +
         'CREDIT_FILES.DATE_CREDIT_FINAL >= ''' + IntToStr(Period) + '-01-01'' AND '               +
         'CREDIT_FILES.DATE_CREDIT_FINAL < '''  + IntToStr(Period+1) + '-01-01'' AND '             +
         '(CREDIT_FILES.CONTRIBUTOR_ID='        + IntToStr(UniMainModule.Reference_contributor_id) +
         ' OR CREDIT_FILES.CONTRIBUTOR_ID='     + IntToStr(UniMainModule.Responsable_contributor_credit) +
         ' OR CREDIT_FILES.AGENT_ID='           + IntToStr(UniMainModule.Reference_contributor_id) +  ') AND ' +
         'CREDIT_FILES.REMOVED=''0'' AND ' +
         'CREDIT_FILES.SHOW_IN_DASHBOARD=1 ';

  //Extra conditie om enkel de kredieten met de specifieke status op te nemen in de statistiek
  if UpperCase(UniMainModule.Company_name) = 'HYPOTHEEKWERELD BV'
  then SQL := SQL + 'and credit_files.status=1410 ';

  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;

  UniMainModule.QWork2.Close;
  SQL := 'select sum(amount_commission) as totalcommission from credit_commission_schemas ' +
         'where credit_commission_schemas.credit_file in ' +
         '(' +
         'Select id from credit_files ' +
         'WHERE credit_files.COMPANYID='        + IntToStr(UniMainModule.Company_id)               + ' AND ' +
         'CREDIT_FILES.DATE_CREDIT_FINAL >= ''' + IntToStr(Period) + '-01-01'' AND '               +
         'CREDIT_FILES.DATE_CREDIT_FINAL < '''  + IntToStr(Period+1) + '-01-01'' AND '             +
         '(CREDIT_FILES.CONTRIBUTOR_ID='        + IntToStr(UniMainModule.Reference_contributor_id) +
         ' OR CREDIT_FILES.CONTRIBUTOR_ID='     + IntToStr(UniMainModule.Responsable_contributor_credit) +
         ' OR CREDIT_FILES.AGENT_ID='           + IntToStr(UniMainModule.Reference_contributor_id) +  ') AND ' +
         'CREDIT_FILES.REMOVED=''0'' AND ' +
         'CREDIT_FILES.SHOW_IN_DASHBOARD=1) ' +
         'AND credit_commission_schemas.credit_contributor in ' +
         '( ' +
         'Select contributor_id from credit_files ' +
         'WHERE credit_files.COMPANYID='        + IntToStr(UniMainModule.Company_id)               + ' AND ' +
         'CREDIT_FILES.DATE_CREDIT_FINAL >= ''' + IntToStr(Period) + '-01-01'' AND '               +
         'CREDIT_FILES.DATE_CREDIT_FINAL < '''  + IntToStr(Period+1) + '-01-01'' AND '             +
         '(CREDIT_FILES.CONTRIBUTOR_ID='        + IntToStr(UniMainModule.Reference_contributor_id) +
         ' OR CREDIT_FILES.CONTRIBUTOR_ID='     + IntToStr(UniMainModule.Responsable_contributor_credit) +
         ' OR CREDIT_FILES.AGENT_ID='           + IntToStr(UniMainModule.Reference_contributor_id) +  ') AND ' +
         'CREDIT_FILES.REMOVED=''0'' AND ' +
         'CREDIT_FILES.SHOW_IN_DASHBOARD=1) ';

  UniMainModule.QWork2.SQL.Text := SQL;
  UniMainModule.QWork2.Open;

  if UniMainModule.portal_contr_show_all_commission = True
  then TotalCommission := UniMainModule.QWork.FieldByName('TotalCommission').AsCurrency
  else TotalCommission := UniMainModule.QWork2.FieldByName('TotalCommission').AsCurrency;

  if UniMainModule.Show_commissions_contributor = False
  then TotalCommission := 0;

  HtmlFrame.HTML.Clear;
  DisplayText :=
   '   <table style="width:100%"> ' +
   ' <tr> ' +
   ' <td> ' +
   ' <div class="col-lg-12 control-section"> ' +
   '     <div class="sample_container"> ' +
   '         <!-- Card Component --> ' +
   '         <div class="e-card e-custom-card"> ' +
   '             <div class="e-card-header"> ' +
   '                 <!-- xLarge Circle Avatar--> ' +
   '                 <div class="e-avatar e-avatar-circle e-avatar-xlarge"> ' +
   '                     <img src="images/wv_counter_dashboard_credit.png" alt="sum"> ' +
   '                 </div> ' +
   '                 &nbsp; ' +
   '             </div> ' +
   '             <div class="e-card-header"> ' +
   '                 <div class="e-card-header-caption center"> ' +
   '                     <div class="e-card-header-title name">' + strNumberOfCredits + '</div> ' +
   '                 </div> ' +
   '             </div> ' +
   '             <div class="e-card-content"> ' +
   '                     <p class="avatar-content">' + UniMainModule.QWork.FieldByName('NumberOfCredits').asString + '</p> ' +
   '             </div> ' +
   '         </div> ' +
   '     </div> ' +
   ' </div></td> ' +
   ' <td> ' +
   ' <div class="col-lg-12 control-section"> ' +
   '     <div class="sample_container"> ' +
   '         <!-- Card Component --> ' +
   '         <div class="e-card e-custom-card"> ' +
   '             <div class="e-card-header"> ' +
   '                 <!-- xLarge Circle Avatar--> ' +
   '                 <div class="e-avatar e-avatar-circle e-avatar-xlarge"> ' +
   '                     <img src="images/wv_euro_amount_dashboard_credit.png" alt="euro-database"> ' +
   '                 </div> ' +
   '                 &nbsp; ' +
   '             </div> ' +
   '             <div class="e-card-header"> ' +
   '                 <div class="e-card-header-caption center"> ' +
   '                     <div class="e-card-header-title name">' + strEuroCredits + '</div> ' +
   '                 </div> ' +
   '             </div> ' +
   '             <div class="e-card-content"> ' +
   '                     <p class="avatar-content">' + FormatCurr('#,##0.00 �', UniMainModule.QWork.FieldByName('TotalCredit').AsCurrency) + '</p> ' +
   '             </div> ' +
   '         </div> ' +
   '     </div> ' +
   ' </div> ' +
   ' </td> ' +
   ' <td> ' +
   ' <div class="col-lg-12 control-section"> ' +
   '     <div class="sample_container"> ' +
   '         <!-- Card Component --> ' +
   '         <div class="e-card e-custom-card"> ' +
   '             <div class="e-card-header"> ' +
   '                 <!-- xLarge Circle Avatar--> ' +
   '                 <div class="e-avatar e-avatar-circle e-avatar-xlarge"> ' +
   '                     <img src="images/wv_commission_dashboard_credit.png" alt="commission"> ' +
   '                 </div> ' +
   '                 &nbsp; ' +
   '             </div> ' +
   '             <div class="e-card-header"> ' +
   '                 <div class="e-card-header-caption center"> ' +
   '                     <div class="e-card-header-title name">' + strEuroCommission + '</div> ' +
   '                 </div> ' +
   '             </div> ' +
   '             <div class="e-card-content"> ' +
   '                     <p class="avatar-content">' + FormatCurr('#,##0.00 �', TotalCommission) + '</p> ' +
   '             </div> ' +
   '         </div> ' +
   '     </div> ' +
   ' </div> ' +
   ' </td> ' +
   ' </tr></table> ';

  HtmlFrame.HTML.Add(DisplayText);
  UniMainModule.QWork2.Close;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'SELECT EXTRACT(MONTH FROM credit_files.DATE_CREDIT_FINAL) as monthofyear, '                     +
         'SUM(credit_files.TOTAL_AMOUNT_CREDIT) as TotalAmount '                                          +
         'FROM credit_files '                                                                             +
         'WHERE credit_files.COMPANYID='         + IntToStr(UniMainModule.Company_id)                     + ' AND ' +
         'CREDIT_FILES.DATE_CREDIT_FINAL >= '''  + IntToStr(Period) + '-01-01'' AND '                     +
         'CREDIT_FILES.DATE_CREDIT_FINAL < '''   + IntToStr(Period+1) + '-01-01'' AND '                   +
         '(CREDIT_FILES.CONTRIBUTOR_ID='         + IntToStr(UniMainModule.Reference_contributor_id)       +
         ' OR CREDIT_FILES.CONTRIBUTOR_ID='      + IntToStr(UniMainModule.Responsable_contributor_credit) +
         ' OR CREDIT_FILES.AGENT_ID='            + IntToStr(UniMainModule.Reference_contributor_id)       + ') AND ' +
         'CREDIT_FILES.REMOVED=''0'' AND '                                                                +
         'CREDIT_FILES.SHOW_IN_DASHBOARD=1 ';

  //Extra conditie om enkel de kredieten met de specifieke status op te nemen in de statistiek
  if UpperCase(UniMainModule.Company_name) = 'HYPOTHEEKWERELD BV'
  then SQL := SQL + 'and credit_files.status=1410 ';

  SQL := SQL + 'GROUP BY monthofyear';

  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;

  CDS.First;
  while not CDS.EOF
  do CDS.Delete;

  if UniMainModule.QWork.Locate('MonthOfyear', 1, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;
  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_january;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 2, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_february;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 3, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_march;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 4, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_april;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 5, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_may;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 6, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_june;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 7, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_july;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 8, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_august;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 9, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_september;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 10, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_october;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 11, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_november;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  if UniMainModule.QWork.Locate('MonthOfyear', 12, [])
  then DisplayValue := UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency
  else DisplayValue := 0;

  CDS.Append;
  CDS.FieldByName('Maand').AsString       := str_december;
  CDS.FieldByName('Productie').AsCurrency := DisplayValue;
  CDS.Post;

  UniMainModule.QWork.Close;

  Tasks.Close;
  Tasks.SQL.Clear;
  SQL := 'Select id, module_id, record_id, title, task_start, task_end, task_status, ' +
         'Case '                                                                       +
         'WHEN task_status = 0 THEN ''' + str_not_started + ''' '                      +
         'WHEN task_status = 1 THEN ''' + str_in_progress + ''' '                      +
         'WHEN task_status = 2 THEN ''' + str_completed   + ''' '                      +
         'WHEN task_status = 3 THEN ''' + str_waiting_for + ''' '                      +
         'WHEN task_status = 4 THEN ''' + str_delayed     + ''' '                      +
         'END as task_status_description ' +
         'from crm_tasks ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) +
         ' and responsable_id=' + IntToStr(UniMainModule.User_id) +
         ' and responsable_type=2 ' +
         ' and removed=''0'' ' +
         ' and task_status <> 2';

  SQL := SQL + ' order by task_start asc';
  Tasks.SQL.Add(SQL);
  Tasks.Open;

  Calls.Close;
  Calls.SQL.Clear;
  SQL := 'Select id, module_id, record_id, title, call_start, left_voicemail, direction, priority_code, call_status, ' +
         'Case ' +
         'WHEN Call_status = 0 THEN '''     + str_call_open + ''' '             +
         'WHEN Call_status = 1 THEN '''     + str_call_completed + ''' '        +
         'END as call_status_description, ' +
         'Case ' +
         'WHEN Call_result = 0 THEN '''     + str_call_result_open + ''' '      +
         'WHEN Call_result = 1 THEN '''     + str_call_result_executed + ''' '  +
         'WHEN Call_result = 2 THEN '''     + str_call_result_cancelled + ''' ' +
         'WHEN Call_result = 3 THEN '''     + str_call_result_received + ''' '  +
         'END as call_result_description, ' +
         'Case ' +
         'WHEN Priority_code = 0 THEN '''   + str_call_priority_low + ''' '     +
         'WHEN Priority_code = 1 THEN '''   + str_call_priority_normal + ''' '  +
         'WHEN Priority_code = 2 THEN '''   + str_call_priority_high + ''' '    +
         'END as priority_description '     +
         'from crm_calls ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id)                +
         ' and responsable_id=' + IntToStr(UniMainModule.User_id)               +
         ' and responsable_type=2 ' +
         ' and removed=''0'' ' +
         ' and call_status=0';

  SQL := SQL + ' order by call_start asc';
  Calls.SQL.Add(SQL);
  Calls.Open;
  ChartEvolution.LoadChart;
end;

procedure TDashboardContributorsFrm.Start_dashboard_contributors;
begin
  Period := YearOf(Now);
  Load_statistics;
end;

procedure TDashboardContributorsFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TDashboardContributorsFrm.BtnEditPhoneClick(Sender: TObject);
begin
  if (Calls.Active) and (not Calls.FieldByName('Id').IsNull)
  then begin
         With CallsAddFrm
         do begin
              if Init_call_modification(Calls.FieldByName('Id').AsInteger) = True
              then ShowModal(CallBackInsertUpdateCallsTable)
              else Close;
            end;
       end;
end;

procedure TDashboardContributorsFrm.CallBackInsertUpdateCallsTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Calls.Active
         then begin
                Calls.Refresh;
                Calls.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TDashboardContributorsFrm.CallBackInsertUpdateTasksTable(
  Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if Tasks.Active
         then begin
                Tasks.Refresh;
                Tasks.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TDashboardContributorsFrm.UpdateStrings;
begin
  str_call_priority_high     := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_priority_high' (* 'Hoog' *) );
  str_call_priority_normal   := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_priority_normal' (* 'Normaal' *) );
  str_call_priority_low      := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_priority_low' (* 'Laag' *) );
  str_call_result_received   := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_result_received' (* 'Ontvangen' *) );
  str_call_result_cancelled  := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_result_cancelled' (* 'Geannuleerd' *) );
  str_call_result_executed   := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_result_executed' (* 'Uitgevoerd' *) );
  str_call_result_open       := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_result_open' (* 'Open' *) );
  str_call_completed         := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_completed' (* 'Afgewerkt' *) );
  str_call_open              := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_call_open' (* 'Open' *) );
  str_delayed                := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_delayed' (* 'Uitgesteld' *) );
  str_waiting_for            := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_waiting_for' (* 'Wachten op iemand anders' *) );
  str_completed              := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_completed' (* 'Voltooid' *) );
  str_in_progress            := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_in_progress' (* 'Wordt uitgevoerd' *) );
  str_not_started            := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_not_started' (* 'Niet gestart' *) );
  str_december               := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_december' (* 'December' *) );
  str_november               := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_november' (* 'November' *) );
  str_october                := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_october' (* 'Oktober' *) );
  str_september              := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_september' (* 'September' *) );
  str_august                 := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_august' (* 'Augustus' *) );
  str_july                   := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_july' (* 'Juli' *) );
  str_june                   := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_june' (* 'Juni' *) );
  str_may                    := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_may' (* 'Mei' *) );
  str_april                  := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_april' (* 'April' *) );
  str_march                  := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_march' (* 'Maart' *) );
  str_february               := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_february' (* 'Februari' *) );
  str_january                := LinkedLanguageDashboardContributors.GetTextOrDefault('strstr_january' (* 'Januari' *) );
  strEuroCommission          := LinkedLanguageDashboardContributors.GetTextOrDefault('strstrEuroCommission' (* '&euro; commissie' *) );
  strEuroCredits             := LinkedLanguageDashboardContributors.GetTextOrDefault('strstrEuroCredits' (* '&euro; kredieten' *) );
  strNumberOfCredits         := LinkedLanguageDashboardContributors.GetTextOrDefault('strstrNumberOfCredits' (* '# kredieten' *) );
end;

end.
