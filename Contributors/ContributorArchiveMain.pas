unit ContributorArchiveMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Vcl.Menus, uniMainMenu, Data.DB, MemDS, DBAccess,
  IBC, uniDBGrid, uniMemo, uniDBMemo, uniBasicGrid, uniDBTreeGrid, uniButton,
  UniThemeButton, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, siComp,
  siLngLnk;

var
	str_file_not_found: string = 'Bestand kon niet gevonden worden.'; // TSI: Localized (Don't modify!)

type
  TContributorArchiveMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerBody: TUniContainerPanel;
    ContainerCategories: TUniContainerPanel;
    HeaderCategories: TUniContainerPanel;
    HeaderRightCategories: TUniContainerPanel;
    TreeFolders: TUniDBTreeGrid;
    ContainerFooterFolders: TUniContainerPanel;
    MemoDescriptionFolder: TUniDBMemo;
    ContainerFiles: TUniContainerPanel;
    ContainerHeaderFiles: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    GridAttachments: TUniDBGrid;
    Folders: TIBCQuery;
    DsFolders: TDataSource;
    VwAttachments: TIBCQuery;
    Ds_VwAttachments: TDataSource;
    LinkedLanguageContributorArchiveMain: TsiLangLinked;
    BtnCollapse: TUniThemeButton;
    BtnExpand: TUniThemeButton;
    BtnOpenAttachment: TUniThemeButton;
    procedure UniFrameCreate(Sender: TObject);
    procedure BtnExpandClick(Sender: TObject);
    procedure BtnCollapseClick(Sender: TObject);
    procedure BtnOpenAttachmentClick(Sender: TObject);
    procedure LinkedLanguageContributorArchiveMainChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_contributor_archive_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Main, Utils;



{ TContributorArchiveMainFrm }

procedure TContributorArchiveMainFrm.BtnCollapseClick(Sender: TObject);
begin
  TreeFolders.FullCollapse;
end;

procedure TContributorArchiveMainFrm.BtnExpandClick(Sender: TObject);
begin
  TreeFolders.FullExpand;
end;

procedure TContributorArchiveMainFrm.BtnOpenAttachmentClick(Sender: TObject);
var Destination: string;
begin
  if ((VwAttachments.Active) and (not VwAttachments.fieldbyname('Id').isNull))
  then begin
         Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
         if Trim(VwAttachments.FieldByName('Folder').AsString) <> ''
         then Destination := Destination + VwAttachments.FieldByName('Folder').AsString + '\';
         if Trim(VwAttachments.FieldByName('SubFolder').AsString) <> ''
         then Destination := Destination + VwAttachments.FieldByName('SubFolder').AsString + '\';
         Destination := Destination + VwAttachments.FieldByName('Destination_Name').AsString;
         if FileExists(Destination)
         then UniSession.SendFile(Destination, Utils.EscapeIllegalChars(VwAttachments.fieldbyname('Original_Name').asString))
         else MainForm.WaveShowErrorToast(str_file_not_found);
       end;
end;

procedure TContributorArchiveMainFrm.LinkedLanguageContributorArchiveMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContributorArchiveMainFrm.Start_contributor_archive_module;
var SQL: string;
begin
  Folders.Close;
  Folders.SQL.Clear;
  SQL := 'Select * from crm_docarchive_folders where companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' and visible_portal=1 ';
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: begin
         SQL := SQL + ' order by folder_name_dutch';
         TreeFolders.Columns.Items[0].FieldName := 'FOLDER_NAME_DUTCH';
       end;
    2: begin
         SQL := SQL + ' order by folder_name_french';
         TreeFolders.Columns.Items[0].FieldName := 'FOLDER_NAME_FRENCH';
       end;
    3: begin
         SQL := SQL + ' order by folder_name_english';
         TreeFolders.Columns.Items[0].FieldName := 'FOLDER_NAME_ENGLISH';
       end;
  end;
  Folders.SQL.Add(SQL);
  Folders.Open;
  VwAttachments.Close;
  VwAttachments.SQL.Clear;
  SQL := 'select files_archive.id, files_archive.original_name, files_archive.destination_name, files_archive.description, ' +
         'files_archive.folder, files_archive.subfolder, ' +
         'files_archive.visible_portal, files_archive.date_entered, files_archive.folder, files_archive.subfolder, ';
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SQL := SQL + 'basictables.dutch typedescription   ';
    2: SQL := SQL + 'basictables.french typedescription  ';
    3: SQL := SQL + 'basictables.english typedescription ';
  end;

  SQL := SQL +
         'from files_archive '                                                        +
         'left outer join basictables on (files_archive.file_type = basictables.id) ' +
         'where files_archive.companyId=' + IntToStr(UniMainModule.Company_id)        +
         ' and Module_id=2000 '                                                       +
         ' and files_archive.removed=''0'' '                                          +
         ' and files_archive.visible_portal=1';
  SQL := SQL + ' order by files_archive.original_name';
  VwAttachments.SQL.Add(SQL);
  VwAttachments.MasterFields := 'ID';
  VwAttachments.DetailFields := 'RECORD_ID';
  VwAttachments.Open;
end;

procedure TContributorArchiveMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  ContainerCategories.JSInterface.JSConfig('stateId', ['ContainerCategories_stateId_unique']);
  with ContainerCategories
  do JSInterface.JSConfig('stateId', [Self.Name + Name]);
end;

procedure TContributorArchiveMainFrm.UpdateStrings;
begin
  str_file_not_found := LinkedLanguageContributorArchiveMain.GetTextOrDefault('strstr_file_not_found' (* 'Bestand kon niet gevonden worden.' *) );
end;

end.
