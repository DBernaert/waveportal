object CreditFilesMainFrm: TCreditFilesMainFrm
  Left = 0
  Top = 0
  Width = 1284
  Height = 777
  OnCreate = UniFrameCreate
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1284
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 139
      Height = 30
      Hint = ''
      Caption = 'Kredietdossiers'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1284
    Height = 30
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Cls = 'boldtext'
    LayoutConfig.Region = 'north'
    object ContainerFilterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 929
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 209
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        FieldLabel = '<i class="fas fa-search fa-lg " ></i>'
        FieldLabelWidth = 25
        FieldLabelSeparator = ' '
        OnKeyPress = EditSearchKeyPress
      end
      object ComboStatus: TUniComboBox
        Left = 216
        Top = 0
        Width = 297
        Hint = ''
        Style = csDropDownList
        Text = ''
        TabOrder = 2
        MatchFieldWidth = False
        FieldLabel = 'Status'
        FieldLabelWidth = 50
        IconItems = <>
        OnKeyPress = EditSearchKeyPress
      end
      object EditStartDateRange: TUniDateTimePicker
        Left = 520
        Top = 0
        Width = 129
        Hint = ''
        DateTime = 43922.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 3
        ClearButton = True
        OnKeyPress = EditSearchKeyPress
      end
      object EditEndDateRange: TUniDateTimePicker
        Left = 656
        Top = 0
        Width = 129
        Hint = ''
        DateTime = 43889.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 4
        ClearButton = True
        FieldLabelWidth = 0
        OnKeyPress = EditSearchKeyPress
      end
      object BtnSearch: TUniThemeButton
        Left = 792
        Top = 0
        Width = 129
        Height = 28
        Hint = ''
        Caption = 'Zoeken'
        ParentFont = False
        TabStop = False
        TabOrder = 5
        OnClick = BtnSearchClick
        ButtonTheme = uctPrimary
      end
    end
    object ContainerFilterRight: TUniContainerPanel
      Left = 1128
      Top = 0
      Width = 156
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object BtnAdd: TUniThemeButton
        Left = 0
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Toevoegen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddClick
        ButtonTheme = uctPrimary
      end
      object BtnModify: TUniThemeButton
        Left = 32
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Wijzigen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyClick
        ButtonTheme = uctPrimary
      end
      object BtnDelete: TUniThemeButton
        Left = 64
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteClick
        ButtonTheme = uctDanger
      end
      object BtnMore: TUniThemeButton
        Left = 96
        Top = 0
        Width = 28
        Height = 28
        Hint = ''
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 4
        Images = UniMainModule.ImageList
        ImageIndex = 50
        OnClick = BtnMoreClick
        ButtonTheme = uctInformation
      end
      object BtnExportList: TUniThemeButton
        Left = 128
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Exporteren lijst'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 5
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnExportListClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object ContainerBody: TUniContainerPanel
    Left = 0
    Top = 63
    Width = 1284
    Height = 364
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'center'
    object GridCreditFiles: TUniDBGrid
      Left = 0
      Top = 0
      Width = 1284
      Height = 364
      Hint = ''
      DataSource = DsCreditFiles
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.PageSize = 45
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      LayoutConfig.Cls = 'customGrid'
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 0 0 0'
      Align = alClient
      TabOrder = 1
      ParentColor = False
      Color = clBtnFace
      OnColumnSort = GridCreditFilesColumnSort
      OnDblClick = GridCreditFilesDblClick
      OnDrawColumnCell = GridCreditFilesDrawColumnCell
      Columns = <
        item
          FieldName = 'CREDIT_FILE_NUMBER'
          Title.Caption = 'Intern nr.'
          Width = 100
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'DEMANDER_NAME'
          Title.Caption = 'Naam'
          Width = 184
          ReadOnly = True
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'TOTAL_INVESTMENT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Totale investering'
          Width = 115
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'OWN_RESOURCES'
          Title.Alignment = taRightJustify
          Title.Caption = 'Eigen middelen'
          Width = 115
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'DEMANDED_CREDIT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Gevraagd krediet'
          Width = 115
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'TOTAL_AMOUNT_CREDIT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Effectief krediet'
          Width = 115
          Font.Style = [fsBold]
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'DATE_CREDIT_FINAL'
          Title.Caption = 'Ingangsdatum'
          Width = 100
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'NAME'
          Title.Caption = 'Fin. instelling'
          Width = 304
          ReadOnly = True
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'STATUS_FILE'
          Title.Caption = 'Status'
          Width = 304
          ReadOnly = True
          Sortable = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'CONTRIBUTOR_FULLNAME'
          Title.Caption = 'Tussenpersoon'
          Width = 370
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
  end
  object PanelJournal: TUniPanel
    Left = 0
    Top = 427
    Width = 1284
    Height = 350
    Hint = ''
    Align = alBottom
    TabOrder = 3
    ClientEvents.UniEvents.Strings = (
      
        'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.stat' +
        'eful = true;'#13#10'    config.defaults = {'#13#10'        stateEvents: ["co' +
        'llapse", "expand"],'#13#10'        getState: function() {'#13#10'           ' +
        ' return {'#13#10'                collapsed: this.collapsed'#13#10'          ' +
        '  };'#13#10'        }'#13#10'    };'#13#10'}')
    BorderStyle = ubsNone
    TitleVisible = True
    Title = 'Journaal'
    Caption = ''
    Collapsible = True
    CollapseDirection = cdBottom
    Layout = 'fit'
    LayoutConfig.Split = True
    LayoutConfig.Region = 'south'
  end
  object CreditFiles: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO CREDIT_FINANCIALINSTITUTIONS'
      
        '  (ID, COMPANYID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAI' +
        'L, WEBSITE, REMOVED, SPREADEDPAYMENT, PERCENTAGEDIRECT, NUMBEROF' +
        'MONTHSSPREAD, PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1, MONTHPLANNED' +
        'PAYMENT2, MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4, MONTHPLANN' +
        'EDPAYMENT5, MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7, MONTHPLA' +
        'NNEDPAYMENT8, MONTHPLANNEDPAYMENT9, MONTHPLANNEDPAYMENT10, PERCE' +
        'NTAGEPAYMENT1, PERCENTAGEPAYMENT2, PERCENTAGEPAYMENT3, PERCENTAG' +
        'EPAYMENT4, PERCENTAGEPAYMENT5, PERCENTAGEPAYMENT6, PERCENTAGEPAY' +
        'MENT7, PERCENTAGEPAYMENT8, PERCENTAGEPAYMENT9, PERCENTAGEPAYMENT' +
        '10, REMARKS)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :NAME, :ADDRESS, :ZIPCODE, :CITY, :PHONE, :F' +
        'AX, :EMAIL, :WEBSITE, :REMOVED, :SPREADEDPAYMENT, :PERCENTAGEDIR' +
        'ECT, :NUMBEROFMONTHSSPREAD, :PLANNEDPAYMENT, :MONTHPLANNEDPAYMEN' +
        'T1, :MONTHPLANNEDPAYMENT2, :MONTHPLANNEDPAYMENT3, :MONTHPLANNEDP' +
        'AYMENT4, :MONTHPLANNEDPAYMENT5, :MONTHPLANNEDPAYMENT6, :MONTHPLA' +
        'NNEDPAYMENT7, :MONTHPLANNEDPAYMENT8, :MONTHPLANNEDPAYMENT9, :MON' +
        'THPLANNEDPAYMENT10, :PERCENTAGEPAYMENT1, :PERCENTAGEPAYMENT2, :P' +
        'ERCENTAGEPAYMENT3, :PERCENTAGEPAYMENT4, :PERCENTAGEPAYMENT5, :PE' +
        'RCENTAGEPAYMENT6, :PERCENTAGEPAYMENT7, :PERCENTAGEPAYMENT8, :PER' +
        'CENTAGEPAYMENT9, :PERCENTAGEPAYMENT10, :REMARKS)'
      'RETURNING '
      
        '  ID, COMPANYID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAIL' +
        ', WEBSITE, REMOVED, SPREADEDPAYMENT, PERCENTAGEDIRECT, NUMBEROFM' +
        'ONTHSSPREAD, PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1, MONTHPLANNEDP' +
        'AYMENT2, MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4, MONTHPLANNE' +
        'DPAYMENT5, MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7, MONTHPLAN' +
        'NEDPAYMENT8, MONTHPLANNEDPAYMENT9, MONTHPLANNEDPAYMENT10, PERCEN' +
        'TAGEPAYMENT1, PERCENTAGEPAYMENT2, PERCENTAGEPAYMENT3, PERCENTAGE' +
        'PAYMENT4, PERCENTAGEPAYMENT5, PERCENTAGEPAYMENT6, PERCENTAGEPAYM' +
        'ENT7, PERCENTAGEPAYMENT8, PERCENTAGEPAYMENT9, PERCENTAGEPAYMENT1' +
        '0')
    SQLDelete.Strings = (
      'DELETE FROM CREDIT_FINANCIALINSTITUTIONS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE CREDIT_FINANCIALINSTITUTIONS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, NAME = :NAME, ADDRESS = :ADD' +
        'RESS, ZIPCODE = :ZIPCODE, CITY = :CITY, PHONE = :PHONE, FAX = :F' +
        'AX, EMAIL = :EMAIL, WEBSITE = :WEBSITE, REMOVED = :REMOVED, SPRE' +
        'ADEDPAYMENT = :SPREADEDPAYMENT, PERCENTAGEDIRECT = :PERCENTAGEDI' +
        'RECT, NUMBEROFMONTHSSPREAD = :NUMBEROFMONTHSSPREAD, PLANNEDPAYME' +
        'NT = :PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1 = :MONTHPLANNEDPAYMEN' +
        'T1, MONTHPLANNEDPAYMENT2 = :MONTHPLANNEDPAYMENT2, MONTHPLANNEDPA' +
        'YMENT3 = :MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4 = :MONTHPLA' +
        'NNEDPAYMENT4, MONTHPLANNEDPAYMENT5 = :MONTHPLANNEDPAYMENT5, MONT' +
        'HPLANNEDPAYMENT6 = :MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7 =' +
        ' :MONTHPLANNEDPAYMENT7, MONTHPLANNEDPAYMENT8 = :MONTHPLANNEDPAYM' +
        'ENT8, MONTHPLANNEDPAYMENT9 = :MONTHPLANNEDPAYMENT9, MONTHPLANNED' +
        'PAYMENT10 = :MONTHPLANNEDPAYMENT10, PERCENTAGEPAYMENT1 = :PERCEN' +
        'TAGEPAYMENT1, PERCENTAGEPAYMENT2 = :PERCENTAGEPAYMENT2, PERCENTA' +
        'GEPAYMENT3 = :PERCENTAGEPAYMENT3, PERCENTAGEPAYMENT4 = :PERCENTA' +
        'GEPAYMENT4, PERCENTAGEPAYMENT5 = :PERCENTAGEPAYMENT5, PERCENTAGE' +
        'PAYMENT6 = :PERCENTAGEPAYMENT6, PERCENTAGEPAYMENT7 = :PERCENTAGE' +
        'PAYMENT7, PERCENTAGEPAYMENT8 = :PERCENTAGEPAYMENT8, PERCENTAGEPA' +
        'YMENT9 = :PERCENTAGEPAYMENT9, PERCENTAGEPAYMENT10 = :PERCENTAGEP' +
        'AYMENT10, REMARKS = :REMARKS'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, ' +
        'EMAIL, WEBSITE, REMOVED, SPREADEDPAYMENT, PERCENTAGEDIRECT, NUMB' +
        'EROFMONTHSSPREAD, PLANNEDPAYMENT, MONTHPLANNEDPAYMENT1, MONTHPLA' +
        'NNEDPAYMENT2, MONTHPLANNEDPAYMENT3, MONTHPLANNEDPAYMENT4, MONTHP' +
        'LANNEDPAYMENT5, MONTHPLANNEDPAYMENT6, MONTHPLANNEDPAYMENT7, MONT' +
        'HPLANNEDPAYMENT8, MONTHPLANNEDPAYMENT9, MONTHPLANNEDPAYMENT10, P' +
        'ERCENTAGEPAYMENT1, PERCENTAGEPAYMENT2, PERCENTAGEPAYMENT3, PERCE' +
        'NTAGEPAYMENT4, PERCENTAGEPAYMENT5, PERCENTAGEPAYMENT6, PERCENTAG' +
        'EPAYMENT7, PERCENTAGEPAYMENT8, PERCENTAGEPAYMENT9, PERCENTAGEPAY' +
        'MENT10, REMARKS FROM CREDIT_FINANCIALINSTITUTIONS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM CREDIT_FINANCIALINSTITUTIONS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM CREDIT_FINANCIALINSTITUTIONS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  CREDIT_FILES.ID,'
      '  CREDIT_FILES.CREDIT_FILE_NUMBER,'
      '  CREDIT_FILES.TOTAL_INVESTMENT,'
      '  CREDIT_FILES.OWN_RESOURCES,'
      '  CREDIT_FILES.DEMANDED_CREDIT,'
      '  CREDIT_FILES.TOTAL_AMOUNT_CREDIT,'
      '  CREDIT_FILES.DATE_CREDIT_FINAL,'
      '  CREDIT_FILES.REFUSED,'
      '  CREDIT_FILES.STATUS, '
      '  BASICTABLES.DUTCH AS STATUS_FILE,'
      '  CREDIT_FINANCIALINSTITUTIONS.NAME,'
      
        '  COALESCE(CREDIT_CONTRIBUTORS.last_name, '#39#39') || '#39' '#39' || COALESCE' +
        '(CREDIT_CONTRIBUTORS.first_name, '#39#39') AS CONTRIBUTOR_FULLNAME,'
      '  (Select '
      '   Case'
      
        '     WHEN CRDEMANDER.DEMANDER_TYPE = 0 THEN COALESCE(SP.LAST_NAM' +
        'E, '#39#39') || '#39' '#39' || COALESCE(SP.FIRST_NAME, '#39#39')'
      '     WHEN CRDEMANDER.DEMANDER_TYPE = 1 THEN C.NAME'
      '   END AS DEMANDER_NAME'
      '   FROM Credit_file_demanders CRDEMANDER'
      
        '   LEFT OUTER JOIN Crm_accounts C on CRDEMANDER.DEMANDER_ID = C.' +
        'ID'
      
        '   LEFT OUTER JOIN Crm_contacts SP on CRDEMANDER.DEMANDER_ID = S' +
        'P.ID'
      
        '   WHERE CRDEMANDER.ID = (Select first 1 id from credit_file_dem' +
        'anders crdemander where crdemander.CREDIT_FILE = credit_files.ID' +
        ')) '
      'FROM'
      '  CREDIT_FILES'
      
        '  LEFT OUTER JOIN BASICTABLES ON (CREDIT_FILES.STATUS = BASICTAB' +
        'LES.ID)'
      
        '  LEFT OUTER JOIN CREDIT_FINANCIALINSTITUTIONS ON (CREDIT_FILES.' +
        'FINANCIAL_INSTITUTION = CREDIT_FINANCIALINSTITUTIONS.ID)'
      
        '  LEFT OUTER JOIN credit_contributors ON (CREDIT_FILES.contribut' +
        'or_id = credit_contributors.ID)')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Left = 464
    Top = 152
    object CreditFilesID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object CreditFilesTOTAL_INVESTMENT: TFloatField
      FieldName = 'TOTAL_INVESTMENT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object CreditFilesOWN_RESOURCES: TFloatField
      FieldName = 'OWN_RESOURCES'
      DisplayFormat = '#,##0.00 '#8364
    end
    object CreditFilesDEMANDED_CREDIT: TFloatField
      FieldName = 'DEMANDED_CREDIT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object CreditFilesTOTAL_AMOUNT_CREDIT: TFloatField
      FieldName = 'TOTAL_AMOUNT_CREDIT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object CreditFilesNAME: TWideStringField
      FieldName = 'NAME'
      ReadOnly = True
      Size = 50
    end
    object CreditFilesDEMANDER_NAME: TWideStringField
      FieldName = 'DEMANDER_NAME'
      ReadOnly = True
      Size = 61
    end
    object CreditFilesCREDIT_FILE_NUMBER: TWideStringField
      FieldName = 'CREDIT_FILE_NUMBER'
    end
    object CreditFilesDATE_CREDIT_FINAL: TDateField
      FieldName = 'DATE_CREDIT_FINAL'
    end
    object CreditFilesREFUSED: TSmallintField
      FieldName = 'REFUSED'
    end
    object CreditFilesCONTRIBUTOR_FULLNAME: TWideStringField
      FieldName = 'CONTRIBUTOR_FULLNAME'
      ReadOnly = True
      Size = 61
    end
    object CreditFilesSTATUS_FILE: TWideStringField
      FieldName = 'STATUS_FILE'
      ReadOnly = True
      Size = 50
    end
    object CreditFilesSTATUS: TLargeintField
      FieldName = 'STATUS'
      Required = True
    end
  end
  object DsCreditFiles: TDataSource
    DataSet = CreditFiles
    Left = 464
    Top = 208
  end
  object Status: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 592
    Top = 152
  end
  object DsStatus: TDataSource
    DataSet = Status
    Left = 592
    Top = 208
  end
  object LinkedLanguageCreditFilesMain: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'CreditFilesID.DisplayLabel'
      'CreditFilesTOTAL_INVESTMENT.DisplayLabel'
      'CreditFilesOWN_RESOURCES.DisplayLabel'
      'CreditFilesDEMANDED_CREDIT.DisplayLabel'
      'CreditFilesTOTAL_AMOUNT_CREDIT.DisplayLabel'
      'CreditFilesNAME.DisplayLabel'
      'CreditFilesDEMANDER_NAME.DisplayLabel'
      'CreditFiles.SQLDelete'
      'CreditFiles.SQLInsert'
      'CreditFiles.SQLLock'
      'CreditFiles.SQLRecCount'
      'CreditFiles.SQLRefresh'
      'CreditFiles.SQLUpdate'
      'Status.SQLDelete'
      'Status.SQLInsert'
      'Status.SQLLock'
      'Status.SQLRecCount'
      'Status.SQLRefresh'
      'Status.SQLUpdate'
      'TCreditFilesMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerFilter.Layout'
      'ContainerFilterLeft.Layout'
      'EditSearch.FieldLabelSeparator'
      'ComboStatus.FieldLabelSeparator'
      'ContainerFilterRight.Layout'
      'ContainerBody.Layout'
      'PanelJournal.Layout'
      'CreditFilesCREDIT_FILE_NUMBER.DisplayLabel'
      'CreditFilesDATE_CREDIT_FINAL.DisplayLabel'
      'CreditFilesREFUSED.DisplayLabel'
      'EditStartDateRange.DateFormat'
      'EditStartDateRange.FieldLabelSeparator'
      'EditStartDateRange.TimeFormat'
      'EditEndDateRange.DateFormat'
      'EditEndDateRange.FieldLabelSeparator'
      'EditEndDateRange.TimeFormat'
      'CreditFilesCONTRIBUTOR_FULLNAME.DisplayLabel'
      'CreditFilesSTATUS_FILE.DisplayLabel'
      'CreditFilesSTATUS.DisplayLabel')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageCreditFilesMainChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 728
    Top = 154
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C00650001004B0072006500
      640069006500740064006F00730073006900650072007300010044006F007300
      73006900650072007300200064006500200063007200E9006400690074000100
      4300720065006400690074002000660069006C006500730001000D000A004200
      74006E0053006500610072006300680001005A006F0065006B0065006E000100
      4300680065007200630068006500720001005300650061007200630068000100
      0D000A00420074006E0043006F00700079004300720065006400690074004600
      69006C00650001004B006F0070006900650020006B0072006500640069006500
      740064006F00730073006900650072002000610061006E006D0061006B006500
      6E00010043007200E90065007200200075006E006500200063006F0070006900
      6500200064007500200064006F00730073006900650072002000640065002000
      63007200E9006400690074000100430072006500610074006500200061002000
      63006F007000790020006F006600200063007200650064006900740020006600
      69006C00650001000D000A0073007400480069006E00740073005F0055006E00
      690063006F00640065000D000A00420074006E004500780070006F0072007400
      4C0069007300740001004500780070006F00720074006500720065006E002000
      6C0069006A007300740001004500780070006F00720074006500720020006C00
      610020006C00690073007400650001004500780070006F007200740020006C00
      69007300740001000D000A00420074006E00410064006400010054006F006500
      76006F006500670065006E00010041006A006F00750074006500720001004100
      6400640001000D000A00420074006E00440065006C0065007400650001005600
      65007200770069006A0064006500720065006E00010053007500700070007200
      69006D00650072000100440065006C0065007400650001000D000A0042007400
      6E004D006F0064006900660079000100570069006A007A006900670065006E00
      01004D006F0064006900660069006500720001004D006F006400690066007900
      01000D000A007300740044006900730070006C00610079004C00610062006500
      6C0073005F0055006E00690063006F00640065000D000A007300740046006F00
      6E00740073005F0055006E00690063006F00640065000D000A00730074004D00
      75006C00740069004C0069006E00650073005F0055006E00690063006F006400
      65000D000A007300740053007400720069006E00670073005F0055006E006900
      63006F00640065000D000A007300740072007300740072005F0041006C006C00
      5F00430072006500640069007400460069006C0065007300010041006C006C00
      650020006B0072006500640069006500740064006F0073007300690065007200
      7300010054006F0075007300200064006F007300730069006500720073002000
      64006500200063007200E900640069007400010041006C006C00200063007200
      65006400690074002000660069006C006500730001000D000A00730074004F00
      740068006500720053007400720069006E00670073005F0055006E0069006300
      6F00640065000D000A0043006F006D0062006F00530074006100740075007300
      2E004600690065006C0064004C006100620065006C0001005300740061007400
      7500730001005300740061007400750074000100530074006100740075007300
      01000D000A00500061006E0065006C004A006F00750072006E0061006C002E00
      5400690074006C00650001004A006F00750072006E00610061006C0001004A00
      6F00750072006E0061006C0001004A006F00750072006E0061006C0001000D00
      0A0045006400690074005300650061007200630068002E004600690065006C00
      64004C006100620065006C0001003C006900200063006C006100730073003D00
      22006600610073002000660061002D0073006500610072006300680020006600
      61002D006C0067002000220020003E003C002F0069003E0001003C0069002000
      63006C006100730073003D0022006600610073002000660061002D0073006500
      61007200630068002000660061002D006C0067002000220020003E003C002F00
      69003E0001003C006900200063006C006100730073003D002200660061007300
      2000660061002D007300650061007200630068002000660061002D006C006700
      2000220020003E003C002F0069003E0001000D000A007300740043006F006C00
      6C0065006300740069006F006E0073005F0055006E00690063006F0064006500
      0D000A004700720069006400430072006500640069007400460069006C006500
      73002E0043006F006C0075006D006E0073005B0031005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640043007200
      6500640069007400460069006C00650073002E0043006F006C0075006D006E00
      73005B0035005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A004700720069006400430072006500640069007400460069006C006500
      73002E0043006F006C0075006D006E0073005B0036005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640043007200
      6500640069007400460069006C00650073002E0043006F006C0075006D006E00
      73005B0030005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A004700720069006400430072006500640069007400460069006C006500
      73002E0043006F006C0075006D006E0073005B0033005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640043007200
      6500640069007400460069006C00650073002E0043006F006C0075006D006E00
      73005B0034005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A004700720069006400430072006500640069007400460069006C006500
      73002E0043006F006C0075006D006E0073005B0032005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640043007200
      6500640069007400460069006C00650073002E0043006F006C0075006D006E00
      73005B0031005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001004E00610061006D0001004E006F006D0001004E0061006D006500
      01000D000A004700720069006400430072006500640069007400460069006C00
      650073002E0043006F006C0075006D006E0073005B0036005D002E0054006900
      74006C0065002E00430061007000740069006F006E00010049006E0067006100
      6E006700730064006100740075006D0001004400610074006500200064006500
      20006400E90062007500740001005300740061007200740069006E0067002000
      640061007400650001000D000A00470072006900640043007200650064006900
      7400460069006C00650073002E0043006F006C0075006D006E0073005B003000
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      49006E007400650072006E0020006E0072002E0001004E00B000200069006E00
      7400650072006E006500010049006E007400650072006E0061006C0020006E00
      6F002E0001000D000A0047007200690064004300720065006400690074004600
      69006C00650073002E0043006F006C0075006D006E0073005B0033005D002E00
      5400690074006C0065002E00430061007000740069006F006E00010045006900
      670065006E0020006D0069006400640065006C0065006E000100520065007300
      73002E002000700072006F00700072006500730001004F0077006E0020007200
      650073006F007500720063006500730001000D000A0047007200690064004300
      72006500640069007400460069006C00650073002E0043006F006C0075006D00
      6E0073005B0035005D002E005400690074006C0065002E004300610070007400
      69006F006E00010045006600660065006300740069006500660020006B007200
      65006400690065007400010043007200E9006400690074002000650066006600
      6500630074006900660001004500660066006500630074006900760065002000
      63007200650064006900740001000D000A004700720069006400430072006500
      640069007400460069006C00650073002E0043006F006C0075006D006E007300
      5B0032005D002E005400690074006C0065002E00430061007000740069006F00
      6E00010054006F00740061006C006500200069006E0076006500730074006500
      720069006E006700010049006E0076006500730074006900730073002E002000
      74006F00740061006C00010054006F00740061006C00200069006E0076006500
      730074006D0065006E00740001000D000A004700720069006400430072006500
      640069007400460069006C00650073002E0043006F006C0075006D006E007300
      5B0034005D002E005400690074006C0065002E00430061007000740069006F00
      6E0001004700650076007200610061006700640020006B007200650064006900
      65007400010043007200E9006400690074002000640065006D0061006E006400
      E9000100440065006D0061006E00640065006400200063007200650064006900
      740001000D000A00470072006900640043007200650064006900740046006900
      6C00650073002E0043006F006C0075006D006E0073005B0037005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      430072006500640069007400460069006C00650073002E0043006F006C007500
      6D006E0073005B0037005D002E005400690074006C0065002E00430061007000
      740069006F006E000100460069006E002E00200069006E007300740065006C00
      6C0069006E006700010049006E0073007400690074002E002000660069006E00
      61006E0063006900E800720065000100460069006E002E00200069006E007300
      7400690074007500740069006F006E0001000D000A0047007200690064004300
      72006500640069007400460069006C00650073002E0043006F006C0075006D00
      6E0073005B0038005D002E0043006800650063006B0042006F00780046006900
      65006C0064002E004600690065006C006400560061006C007500650073000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      01000D000A004700720069006400430072006500640069007400460069006C00
      650073002E0043006F006C0075006D006E0073005B0038005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001005300740061007400
      7500730001005300740061007400750074000100530074006100740075007300
      01000D000A004700720069006400430072006500640069007400460069006C00
      650073002E0043006F006C0075006D006E0073005B0039005D002E0043006800
      650063006B0042006F0078004600690065006C0064002E004600690065006C00
      6400560061006C00750065007300010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C0073006500010074007200
      750065003B00660061006C007300650001000D000A0047007200690064004300
      72006500640069007400460069006C00650073002E0043006F006C0075006D00
      6E0073005B0039005D002E005400690074006C0065002E004300610070007400
      69006F006E000100540075007300730065006E0070006500720073006F006F00
      6E00010049006E007400650072006D00E9006400690061006900720065000100
      49006E007400650072006D0065006400690061007200790001000D000A007300
      7400430068006100720053006500740073005F0055006E00690063006F006400
      65000D000A00}
  end
  object PopupMenuCreditFilesMain: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 728
    Top = 210
    object BtnCopyCreditFile: TUniMenuItem
      Caption = 'Kopie kredietdossier aanmaken'
      ImageIndex = 80
      OnClick = BtnCopyCreditFileClick
    end
    object BtnSendPortalInvitation: TUniMenuItem
      Caption = 'Uitnodiging portaal versturen'
      ImageIndex = 74
      OnClick = BtnSendPortalInvitationClick
    end
  end
end
