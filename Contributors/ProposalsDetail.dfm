object ProposalsDetailFrm: TProposalsDetailFrm
  Left = 0
  Top = 0
  ClientHeight = 753
  ClientWidth = 1220
  Caption = 'Detailgegevens voorstel'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'border'
  Images = UniMainModule.ImageList
  ImageIndex = 7
  OnCancel = BtnCloseClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1220
    Height = 209
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    LayoutConfig.Region = 'north'
    object LblDemandedAmount: TUniLabel
      Left = 16
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Geleend bedrag'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 1
    end
    object LblStart: TUniLabel
      Left = 120
      Top = 128
      Width = 29
      Height = 13
      Hint = ''
      Caption = 'Start'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 2
    end
    object LblInclFire2: TUniLabel
      Left = 640
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Incl. brand'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 3
    end
    object LblMonthly: TUniLabel
      Left = 120
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Aflossing'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 4
    end
    object LblMaxMonthly: TUniLabel
      Left = 432
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Max. aflossing'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 5
    end
    object LblInclSSV: TUniLabel
      Left = 224
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Incl. SSV'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 6
    end
    object LblInclFire: TUniLabel
      Left = 328
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Incl. brand'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 7
    end
    object LblTotalRent: TUniLabel
      Left = 744
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Totaal rente'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 8
    end
    object LblInclSSV2: TUniLabel
      Left = 536
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Incl. SSV'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 9
    end
    object LblTotalSSV: TUniLabel
      Left = 848
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Totaal SSV'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 10
    end
    object LblMax: TUniLabel
      Left = 432
      Top = 128
      Width = 24
      Height = 13
      Hint = ''
      Caption = 'Max'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 11
    end
    object LblMaxTotal: TUniLabel
      Left = 744
      Top = 128
      Width = 61
      Height = 13
      Hint = ''
      Caption = 'Max totaal'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 12
    end
    object LblTotalFire: TUniLabel
      Left = 952
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Totaal brand'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 13
    end
    object LblTotal: TUniLabel
      Left = 1056
      Top = 152
      Width = 89
      Height = 13
      Hint = ''
      AutoSize = False
      Caption = 'Totaal'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 14
    end
    object LblDbFormula: TUniDBText
      Left = 144
      Top = 16
      Width = 64
      Height = 13
      Hint = ''
      DataField = 'FORMULA'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblFormula: TUniLabel
      Left = 16
      Top = 16
      Width = 49
      Height = 13
      Hint = ''
      Caption = 'Formule:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 16
    end
    object LblCreditProvider: TUniLabel
      Left = 16
      Top = 40
      Width = 112
      Height = 13
      Hint = ''
      Caption = 'Kredietverstrekker:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 17
    end
    object LblDbCreditProvider: TUniDBText
      Left = 144
      Top = 40
      Width = 95
      Height = 13
      Hint = ''
      DataField = 'CREDIT_PROVIDER'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblProposalNumber: TUniLabel
      Left = 16
      Top = 64
      Width = 68
      Height = 13
      Hint = ''
      Caption = 'Nr. voorstel:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 19
    end
    object LblDbProposalNumber: TUniDBText
      Left = 144
      Top = 64
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'PROPOSAL_NUMBER'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblDbBorrowedAmount: TUniDBText
      Left = 16
      Top = 176
      Width = 109
      Height = 13
      Hint = ''
      DataField = 'BORROWED_AMOUNT'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbStartRedemption: TUniDBText
      Left = 120
      Top = 176
      Width = 107
      Height = 13
      Hint = ''
      DataField = 'START_REDEMPTION'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbStartDebtBalIncl: TUniDBText
      Left = 224
      Top = 176
      Width = 104
      Height = 13
      Hint = ''
      DataField = 'START_DEBT_BAL_INCL'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbStartFireInsIncl: TUniDBText
      Left = 328
      Top = 176
      Width = 100
      Height = 13
      Hint = ''
      DataField = 'START_FIRE_INS_INCL'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbMaxRedemption: TUniDBText
      Left = 432
      Top = 176
      Width = 103
      Height = 13
      Hint = ''
      DataField = 'MAX_REDEMPTION'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbMaxRedemptDebtBalIncl: TUniDBText
      Left = 536
      Top = 176
      Width = 143
      Height = 13
      Hint = ''
      DataField = 'MAX_REDEMPT_DEBT_BAL_INCL'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbMaxRedemptFireInsIncl: TUniDBText
      Left = 640
      Top = 176
      Width = 139
      Height = 13
      Hint = ''
      DataField = 'MAX_REDEMPT_FIRE_INS_INCL'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbTotalInterest: TUniDBText
      Left = 744
      Top = 176
      Width = 89
      Height = 13
      Hint = ''
      DataField = 'TOTAL_INTEREST'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbTotalInterestDebtBalance: TUniDBText
      Left = 848
      Top = 176
      Width = 149
      Height = 13
      Hint = ''
      DataField = 'TOTAL_INTEREST_DEBT_BALANCE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbTotalInterestFireInsurance: TUniDBText
      Left = 952
      Top = 176
      Width = 155
      Height = 13
      Hint = ''
      DataField = 'TOTAL_INTEREST_FIRE_INSURANCE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
    end
    object LblDbGeneralTotal: TUniDBText
      Left = 1056
      Top = 176
      Width = 104
      Height = 13
      Hint = ''
      DataField = 'GENERAL_TOTAL'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      ParentFont = False
      Font.Style = [fsBold]
    end
    object LblDuration: TUniLabel
      Left = 984
      Top = 40
      Width = 115
      Height = 13
      Hint = ''
      Caption = 'Looptijd (maanden):'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 32
    end
    object LblDbDuration: TUniDBText
      Left = 1128
      Top = 40
      Width = 81
      Height = 13
      Hint = ''
      DataField = 'DURATION'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblCreditAllowance: TUniLabel
      Left = 984
      Top = 16
      Width = 125
      Height = 13
      Hint = ''
      Caption = 'Kredietgeverstoeslag:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 34
    end
    object LblDbCreditAllowance: TUniDBText
      Left = 1128
      Top = 16
      Width = 81
      Height = 13
      Hint = ''
      DataField = 'CREDIT_ALLOWANCE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblProposalDepositnumber: TUniLabel
      Left = 264
      Top = 64
      Width = 78
      Height = 13
      Hint = ''
      Caption = 'Nr. voorschot:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 36
    end
    object LblDbProposalDepositnumber: TUniDBText
      Left = 376
      Top = 64
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'PROPOSAL_DEPOSIT_NUMBER'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblTarificationNumber: TUniLabel
      Left = 16
      Top = 88
      Width = 51
      Height = 13
      Hint = ''
      Caption = 'Tariefnr.:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 38
    end
    object LblDbTarificationNumber: TUniDBText
      Left = 144
      Top = 88
      Width = 116
      Height = 13
      Hint = ''
      DataField = 'TARIFICATION_NUMBER'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblTarificationdate: TUniLabel
      Left = 264
      Top = 88
      Width = 73
      Height = 13
      Hint = ''
      Caption = 'Tariefdatum:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 40
    end
    object LblDbTarificationdate: TUniDBText
      Left = 376
      Top = 88
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'TARIFICATION_DATE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblProspectusnumber: TUniLabel
      Left = 504
      Top = 16
      Width = 84
      Height = 13
      Hint = ''
      Caption = 'Prospectus nr.:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 42
    end
    object LblDbProspectusnumber: TUniDBText
      Left = 624
      Top = 16
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'PROSPECTUS_NUMBER'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblProspectusDate: TUniLabel
      Left = 504
      Top = 40
      Width = 103
      Height = 13
      Hint = ''
      Caption = 'Prospectusdatum:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 44
    end
    object LblDbProspectusDate: TUniDBText
      Left = 624
      Top = 40
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'PROSPECTUS_DATE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblQuotity: TUniLabel
      Left = 504
      Top = 64
      Width = 53
      Height = 13
      Hint = ''
      Caption = 'Quotiteit:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 46
    end
    object LblDbQuotity: TUniDBText
      Left = 624
      Top = 64
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'QUOTITY'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblBasicInterestRate: TUniLabel
      Left = 504
      Top = 88
      Width = 89
      Height = 13
      Hint = ''
      Caption = 'Basisrentevoet:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 48
    end
    object LblDbBasicInterestRate: TUniDBText
      Left = 624
      Top = 88
      Width = 112
      Height = 13
      Hint = ''
      DataField = 'BASIC_INTEREST_RATE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblProductReduction: TUniLabel
      Left = 744
      Top = 16
      Width = 88
      Height = 13
      Hint = ''
      Caption = 'Productkorting:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 50
    end
    object LblDbProductReduction: TUniDBText
      Left = 864
      Top = 16
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'PRODUCT_REDUCTION'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblQualityReduction: TUniLabel
      Left = 744
      Top = 40
      Width = 99
      Height = 13
      Hint = ''
      Caption = 'Kwaliteitskorting:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 52
    end
    object LblDbQualityReduction: TUniDBText
      Left = 864
      Top = 40
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'QUALITY_REDUCTION'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblExtrareduction: TUniLabel
      Left = 744
      Top = 64
      Width = 77
      Height = 13
      Hint = ''
      Caption = 'Extra korting:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 54
    end
    object LblDbExtrareduction: TUniDBText
      Left = 864
      Top = 64
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'EXTRA_REDUCTION'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
    object LblQuotityallowance: TUniLabel
      Left = 744
      Top = 88
      Width = 101
      Height = 13
      Hint = ''
      Caption = 'Quotiteitstoeslag:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 56
    end
    object LblDbQuotityallowance: TUniDBText
      Left = 864
      Top = 88
      Width = 113
      Height = 13
      Hint = ''
      DataField = 'QUOTITY_ALLOWANCE'
      DataSource = UniMainModule.Ds_CreditProposals_Edit
      AutoSize = False
    end
  end
  object PdfFrame: TUniPDFFrame
    Left = 0
    Top = 209
    Width = 1220
    Height = 511
    Hint = ''
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '0 16 0 16'
    Align = alClient
    TabOrder = 1
    TabStop = False
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 720
    Width = 1220
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '16 16 16 0'
    object ContainerFooterRight: TUniContainerPanel
      Left = 1083
      Top = 0
      Width = 137
      Height = 33
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      ExplicitLeft = 1064
      object BtnClose: TUniThemeButton
        Left = 0
        Top = 0
        Width = 137
        Height = 33
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        ScreenMask.Target = Owner
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object LinkedLanguageProposalsDetail: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TProposalsDetailFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerFooter.Layout'
      'ContainerFooterRight.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageProposalsDetailChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 600
    Top = 353
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A005400500072006F0070006F00730061006C00730044006500
      7400610069006C00460072006D000100440065007400610069006C0067006500
      67006500760065006E007300200076006F006F0072007300740065006C000100
      44006F006E006E00E9006500730020006400E9007400610069006C006C00E900
      6500730020006400650020006C0061002000700072006F0070006F0073006900
      740069006F006E000100440065007400610069006C0065006400200064006100
      740061002000700072006F0070006F00730061006C0001000D000A004C006200
      6C00440065006D0061006E0064006500640041006D006F0075006E0074000100
      470065006C00650065006E006400200062006500640072006100670001004D00
      6F006E0074002E00200065006D007000720075006E007400E900010041006D00
      6F0075006E007400200062006F00720072006F0077002E0001000D000A004C00
      62006C005300740061007200740001005300740061007200740001004400E900
      62007500740001005300740061007200740001000D000A004C0062006C004900
      6E0063006C0046006900720065003200010049006E0063006C002E0020006200
      720061006E00640001005900200063006F006D00700072006900730020006600
      65007500010049006E0063006C007500640069006E0067002000660069007200
      650001000D000A004C0062006C004D006F006E00740068006C00790001004100
      66006C006F007300730069006E0067000100520065006D0062006F0075007200
      730065006D0065006E007400010052006500640065006D007000740069006F00
      6E0001000D000A004C0062006C004D00610078004D006F006E00740068006C00
      790001004D00610078002E002000610066006C006F007300730069006E006700
      0100520065006D0062002E0020006D00610078002E0001004D00610078002000
      72006500640065006D007000740069006F006E0001000D000A004C0062006C00
      49006E0063006C00530053005600010049006E0063006C002E00200053005300
      5600010049006E0063006C002E002000530053005600010049006E0063006C00
      2E00200053005300560001000D000A004C0062006C0049006E0063006C004600
      690072006500010049006E0063006C002E0020006200720061006E0064000100
      49006E0063006C002E002000660065007500010049006E0063006C002E002000
      660069007200650001000D000A004C0062006C0054006F00740061006C005200
      65006E007400010054006F007400610061006C002000720065006E0074006500
      010049006E007400E9007200EA007400200074006F00740061006C0001005400
      6F00740061006C00200069006E0074006500720065007300740001000D000A00
      4C0062006C0049006E0063006C005300530056003200010049006E0063006C00
      2E002000530053005600010049006E0063006C002E0020005300530056000100
      49006E0063006C002E00200053005300560001000D000A004C0062006C005400
      6F00740061006C00530053005600010054006F007400610061006C0020005300
      53005600010054006F00740061006C002000530053005600010054006F007400
      61006C00200053005300560001000D000A004C0062006C004D00610078000100
      4D006100780001004D006100780001004D006100780001000D000A004C006200
      6C004D006100780054006F00740061006C0001004D0061007800200074006F00
      7400610061006C00010054006F00740061006C0020006D006100780069006D00
      75006D0001004D0061007800200074006F00740061006C0001000D000A004C00
      62006C0054006F00740061006C004600690072006500010054006F0074006100
      61006C0020006200720061006E006400010054006F00740061006C0020006600
      65007500010054006F00740061006C002000660069007200650001000D000A00
      4C0062006C0054006F00740061006C00010054006F007400610061006C000100
      54006F00740061006C00010054006F00740061006C0001000D000A004C006200
      6C0046006F0072006D0075006C006100010046006F0072006D0075006C006500
      3A00010046006F0072006D0075006C0065003A00010046006F0072006D007500
      6C0061003A0001000D000A004C0062006C004300720065006400690074005000
      72006F007600690064006500720001004B007200650064006900650074007600
      6500720073007400720065006B006B00650072003A00010046006F0075007200
      6E002E00200064006500200063007200E9006400690074003A00010043007200
      65006400690074002000700072006F00760069006400650072003A0001000D00
      0A004C0062006C00500072006F0070006F00730061006C004E0075006D006200
      6500720001004E0072002E00200076006F006F0072007300740065006C003A00
      01004E0072002E002000700072006F0070006F0073006900740069006F006E00
      3A0001004E0072002E002000700072006F0070006F00730061006C003A000100
      0D000A004C0062006C004400750072006100740069006F006E0001004C006F00
      6F007000740069006A006400200028006D00610061006E00640065006E002900
      3A000100440075007200E9006500200028006D006F006900730029003A000100
      4400750072006100740069006F006E00200028006D006F006E00740068007300
      29003A0001000D000A004C0062006C0043007200650064006900740041006C00
      6C006F00770061006E006300650001004B007200650064006900650074006700
      6500760065007200730074006F00650073006C00610067003A00010053007500
      72007400610078006500200063007200E90061006E0063006900650072007300
      3A0001004300720065006400690074006F007200730027002000730075007200
      6300680061007200670065003A0001000D000A004C0062006C00500072006F00
      70006F00730061006C004400650070006F007300690074006E0075006D006200
      6500720001004E0072002E00200076006F006F0072007300630068006F007400
      3A0001004E0072002E0020006100760061006E00630065003A0001004E007200
      2E0020006400650070006F007300690074003A0001000D000A004C0062006C00
      54006100720069006600690063006100740069006F006E004E0075006D006200
      6500720001005400610072006900650066006E0072002E003A00010054006100
      72006900660020006E002000B0003A000100520061007400650020006E006F00
      20002E003A0001000D000A004C0062006C005400610072006900660069006300
      6100740069006F006E0064006100740065000100540061007200690065006600
      64006100740075006D003A000100440061007400650020006400750020007400
      61007200690066003A0001005200610074006500200064006100740065003A00
      01000D000A004C0062006C00500072006F007300700065006300740075007300
      6E0075006D006200650072000100500072006F00730070006500630074007500
      730020006E0072002E003A0001004E00B0002000640065002000700072006F00
      73007000650063007400750073003A000100500072006F007300700065006300
      74007500730020006E006F0020002E003A0001000D000A004C0062006C005000
      72006F0073007000650063007400750073004400610074006500010050007200
      6F00730070006500630074007500730064006100740075006D003A0001004400
      6100740065002000640075002000700072006F00730070006500630074007500
      73003A000100500072006F007300700065006300740075007300200064006100
      740065003A0001000D000A004C0062006C00510075006F007400690074007900
      0100510075006F007400690074006500690074003A000100510075006F007400
      69007400E9003A000100510075006F0074006900740079003A0001000D000A00
      4C0062006C004200610073006900630049006E00740065007200650073007400
      5200610074006500010042006100730069007300720065006E00740065007600
      6F00650074003A0001005400610075007800200069006E007400E9007200EA00
      7400200062006100730065003A00010042006100730069006300200069006E00
      740065007200650073007400200072006100740065003A0001000D000A004C00
      62006C00500072006F0064007500630074005200650064007500630074006900
      6F006E000100500072006F0064007500630074006B006F007200740069006E00
      67003A000100520065006D006900730065002000700072006F00640075006900
      74003A000100500072006F006400750063007400200064006900730063006F00
      75006E0074003A0001000D000A004C0062006C005100750061006C0069007400
      790052006500640075006300740069006F006E0001004B00770061006C006900
      740065006900740073006B006F007200740069006E0067003A00010052006500
      6D0069007300650020007100750061006C0069007400E9003A00010051007500
      61006C00690074007900200064006900730063006F0075006E0074003A000100
      0D000A004C0062006C0045007800740072006100720065006400750063007400
      69006F006E0001004500780074007200610020006B006F007200740069006E00
      67003A000100520065006D00690073006500200073007500700070006C002E00
      3A00010045007800740072006100200064006900730063006F0075006E007400
      3A0001000D000A004C0062006C00510075006F00740069007400790061006C00
      6C006F00770061006E00630065000100510075006F0074006900740065006900
      7400730074006F00650073006C00610067003A00010053007500700070006C00
      2E0020006400650020007100750061006E00740069007400E9003A0001005100
      750061006E007400690074007900200073007500720063006800610072006700
      65003A0001000D000A00420074006E0043006C006F0073006500010053006C00
      75006900740065006E0001004600650072006D0065007200010043006C006F00
      7300650001000D000A0073007400480069006E00740073005F0055006E006900
      63006F00640065000D000A007300740044006900730070006C00610079004C00
      6100620065006C0073005F0055006E00690063006F00640065000D000A007300
      740046006F006E00740073005F0055006E00690063006F00640065000D000A00
      730074004D0075006C00740069004C0069006E00650073005F0055006E006900
      63006F00640065000D000A007300740053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A007300740072007300740072005F00
      6500720072006F0072005F00720065006D006F00760069006E00670001004600
      6F00750074002000620069006A00200068006500740020007600650072007700
      69006A0064006500720065006E002E0001004500720072006500750072002000
      6C006F007200730020006400650020006C006100200073007500700070007200
      65007300730069006F006E002E0001004500720072006F007200200064006500
      6C006500740069006E0067002E0001000D000A00730074007200730074007200
      5F006500720072006F0072005F006300720065006100740069006E0067005F00
      6400690072006500630074006F0072007900010046006F007500740020006200
      69006A0020006800650074002000610061006E006D0061006B0065006E002000
      760061006E0020006400650020006400690072006500630074006F0072007900
      2C00200063006F006E0074006100630074006500650072002000570041005600
      45004400450053004B002100010045007200720065007500720020006C006F00
      7200730020006400650020006C006100200063007200E9006100740069006F00
      6E0020006400750020007200E90070006500720074006F006900720065002C00
      200063006F006E00740061006300740065007A00200057004100560045004400
      450053004B00210001004500720072006F007200200063007200650061007400
      69006E006700200074006800650020006400690072006500630074006F007200
      79002C00200063006F006E007400610063007400200057004100560045004400
      450053004B00210001000D000A007300740072007300740072005F0063006100
      7000740069006F006E000100440065007400610069006C006700650067006500
      760065006E007300200076006F006F0072007300740065006C00010044006F00
      6E006E00E9006500730020006400E9007400610069006C006C00E90065007300
      20006400650020006C0061002000700072006F0070006F007300690074006900
      6F006E000100440065007400610069006C006500640020006400610074006100
      2000700072006F0070006F00730061006C0001000D000A007300740072007300
      740072005F006500720072006F0072005F0073006100760069006E0067000100
      46006F00750074002000620069006A00200068006500740020006F0070007300
      6C00610061006E002000760061006E0020006400650020006700650067006500
      760065006E0073002100010045007200720065007500720020006C006F007200
      730020006400650020006C00270065006E007200650067006900730074007200
      65006D0065006E0074002000640065007300200064006F006E006E00E9006500
      7300210001004500720072006F007200200073006100760069006E0067002000
      74006800650020006400610074006100210001000D000A00730074004F007400
      68006500720053007400720069006E00670073005F0055006E00690063006F00
      640065000D000A007300740043006F006C006C0065006300740069006F006E00
      73005F0055006E00690063006F00640065000D000A0073007400430068006100
      720053006500740073005F0055006E00690063006F00640065000D000A00}
  end
end
