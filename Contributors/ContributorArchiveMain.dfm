object ContributorArchiveMainFrm: TContributorArchiveMainFrm
  Left = 0
  Top = 0
  Width = 1449
  Height = 896
  OnCreate = UniFrameCreate
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1449
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 182
      Height = 25
      Hint = ''
      Caption = 'Documentenarchief'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerBody: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1449
    Height = 863
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'center'
    object ContainerCategories: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 521
      Height = 863
      Hint = ''
      ParentColor = False
      Align = alLeft
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'   config.sta' +
          'teful = true;'#13#10'}')
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Split = True
      LayoutConfig.Region = 'west'
      object HeaderCategories: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 521
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Region = 'north'
        LayoutConfig.Margin = '0 0 8 0'
        object HeaderRightCategories: TUniContainerPanel
          Left = 461
          Top = 0
          Width = 60
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alRight
          TabOrder = 1
          LayoutConfig.Region = 'east'
          object BtnCollapse: TUniThemeButton
            Left = 0
            Top = 0
            Width = 28
            Height = 28
            Hint = 'Samenvouwen'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            TabStop = False
            TabOrder = 1
            Images = UniMainModule.ImageList
            ImageIndex = 82
            OnClick = BtnCollapseClick
            ButtonTheme = uctPrimary
          end
          object BtnExpand: TUniThemeButton
            Left = 32
            Top = 0
            Width = 28
            Height = 28
            Hint = 'Uitvouwen'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            TabStop = False
            TabOrder = 2
            Images = UniMainModule.ImageList
            ImageIndex = 81
            OnClick = BtnExpandClick
            ButtonTheme = uctPrimary
          end
        end
      end
      object TreeFolders: TUniDBTreeGrid
        Left = 0
        Top = 30
        Width = 521
        Height = 637
        Hint = ''
        DataSource = DsFolders
        ShowIcons = True
        ClientEvents.ExtEvents.Strings = (
          
            'store.beforeload=function store.beforeload(store, operation, eOp' +
            'ts)'#13#10'{'#13#10'    var me=this.grid;'#13#10'    me.expandedNodes=[]; '#13#10'    me' +
            '.getRootNode().cascadeBy(function(node) {'#13#10'        if (node.data' +
            '.expanded) {'#13#10'            me.expandedNodes.push(node.data.id)'#13#10' ' +
            '       }'#13#10'    });'#65279#13#10'}'
          
            'store.load=function store.load(sender, records, successful, oper' +
            'ation, eOpts)'#13#10'{'#13#10'  var me=sender;'#13#10'  me.remoteSort=false;'#13#10'  me' +
            '.sort({property:0, direction:"ASC"});'#13#10'}'
          
            'store.nodeappend=function store.nodeappend(sender, node, index, ' +
            'eOpts)'#13#10'{'#13#10'    if (node && node.data && this.grid.expandedNodes ' +
            '&& this.grid.expandedNodes.indexOf(node.data.id)!=-1) {'#13#10'       ' +
            ' if (node.parentNode && node.parentNode.data.expanded) { '#13#10'     ' +
            '       node.expand()'#13#10'        } else if (node.parentNode) {'#13#10'   ' +
            '         node.parentNode.expand()'#13#10'        }                  '#13#10 +
            '    };'#65279#65279#13#10'}')
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  sender.expa' +
            'ndedNodes=[];'#65279#13#10'}')
        Align = alClient
        TabOrder = 2
        LayoutConfig.Cls = 'customGrid'
        LayoutConfig.Region = 'center'
        LayoutConfig.Margin = '0 0 4 0'
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgPersistentSelection]
        ForceFit = True
        IdParentField = 'PARENT_ID'
        IdField = 'ID'
        Columns = <
          item
            Flex = 1
            FieldName = 'FOLDER_NAME_DUTCH'
            Title.Caption = 'Mappen'
            Width = 250
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
      object ContainerFooterFolders: TUniContainerPanel
        Left = 0
        Top = 667
        Width = 521
        Height = 196
        Hint = ''
        ParentColor = False
        Align = alBottom
        TabOrder = 3
        Layout = 'border'
        LayoutConfig.Region = 'south'
        LayoutConfig.Margin = '4 0 0 0'
        object MemoDescriptionFolder: TUniDBMemo
          Left = 0
          Top = 0
          Width = 521
          Height = 196
          Cursor = crArrow
          Hint = ''
          DataField = 'FOLDER_COMMENT'
          DataSource = DsFolders
          Align = alClient
          ReadOnly = True
          TabOrder = 1
          TabStop = False
          LayoutConfig.Region = 'center'
        end
      end
    end
    object ContainerFiles: TUniContainerPanel
      Left = 521
      Top = 0
      Width = 928
      Height = 863
      Hint = ''
      ParentColor = False
      Align = alClient
      TabOrder = 2
      Layout = 'border'
      LayoutConfig.Region = 'center'
      object ContainerHeaderFiles: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 928
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Region = 'north'
        LayoutConfig.Margin = '0 0 8 0'
        object ContainerFilterRight: TUniContainerPanel
          Left = 900
          Top = 0
          Width = 28
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alRight
          TabOrder = 1
          LayoutConfig.Region = 'east'
          object BtnOpenAttachment: TUniThemeButton
            Left = 0
            Top = 0
            Width = 28
            Height = 28
            Hint = 'Downloaden'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            TabStop = False
            TabOrder = 1
            Images = UniMainModule.ImageList
            ImageIndex = 55
            OnClick = BtnOpenAttachmentClick
            ButtonTheme = uctPrimary
          end
        end
      end
      object GridAttachments: TUniDBGrid
        Left = 0
        Top = 30
        Width = 928
        Height = 833
        Hint = ''
        DataSource = Ds_VwAttachments
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
        WebOptions.Paged = False
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        LayoutConfig.Cls = 'customGrid'
        LayoutConfig.Region = 'center'
        Align = alClient
        TabOrder = 2
        OnDblClick = BtnOpenAttachmentClick
        Columns = <
          item
            Flex = 1
            FieldName = 'DESCRIPTION'
            Title.Caption = 'Omschrijving'
            Width = 1504
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            Flex = 1
            FieldName = 'TYPEDESCRIPTION'
            Title.Caption = 'Type'
            Width = 304
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'DATE_ENTERED'
            Title.Caption = 'Tijdstip upload'
            Width = 160
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
    end
  end
  object Folders: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from crm_docarchive_folders')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 240
    Top = 168
  end
  object DsFolders: TDataSource
    DataSet = Folders
    Left = 240
    Top = 226
  end
  object VwAttachments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    files_archive.id,'
      '    files_archive.original_name,'
      '    files_archive.destination_name,'
      '    files_archive.description,'
      '    files_archive.visible_portal,'
      '    files_archive.date_entered,'
      '    files_archive.folder,'
      '    files_archive.subfolder,'
      '    basictables.dutch typedescription'
      'from files_archive'
      
        '   left outer join basictables on (files_archive.file_type = bas' +
        'ictables.id)'
      '')
    MasterSource = DsFolders
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 896
    Top = 160
  end
  object Ds_VwAttachments: TDataSource
    DataSet = VwAttachments
    Left = 896
    Top = 216
  end
  object LinkedLanguageContributorArchiveMain: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TContributorArchiveMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerBody.Layout'
      'ContainerCategories.Layout'
      'HeaderCategories.Layout'
      'HeaderRightCategories.Layout'
      'TreeFolders.IdField'
      'TreeFolders.IdParentField'
      'ContainerFooterFolders.Layout'
      'MemoDescriptionFolder.FieldLabelSeparator'
      'ContainerFiles.Layout'
      'ContainerHeaderFiles.Layout'
      'ContainerFilterRight.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageContributorArchiveMainChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 896
    Top = 280
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C006500010044006F006300
      75006D0065006E00740065006E00610072006300680069006500660001004100
      7200630068006900760065007300200064006500200064006F00630075006D00
      65006E0074007300010044006F00630075006D0065006E007400200061007200
      6300680069007600650001000D000A0073007400480069006E00740073005F00
      55006E00690063006F00640065000D000A00420074006E004F00700065006E00
      4100740074006100630068006D0065006E007400010044006F0077006E006C00
      6F006100640065006E0001005400E9006C00E900630068006100720067006500
      7200010044006F0077006E006C006F006100640001000D000A00420074006E00
      43006F006C006C0061007000730065000100530061006D0065006E0076006F00
      7500770065006E000100C900740065006E00640072006500010043006F006C00
      6C00610070007300650001000D000A00420074006E0045007800700061006E00
      6400010055006900740076006F007500770065006E0001004500660066006F00
      6E006400720065007200010045007800700061006E00640001000D000A007300
      740044006900730070006C00610079004C006100620065006C0073005F005500
      6E00690063006F00640065000D000A007300740046006F006E00740073005F00
      55006E00690063006F00640065000D000A00730074004D0075006C0074006900
      4C0069006E00650073005F0055006E00690063006F00640065000D000A007300
      740053007400720069006E00670073005F0055006E00690063006F0064006500
      0D000A007300740072007300740072005F00660069006C0065005F006E006F00
      74005F0066006F0075006E0064000100420065007300740061006E0064002000
      6B006F006E0020006E0069006500740020006700650076006F006E0064006500
      6E00200077006F007200640065006E002E0001004C0065002000660069006300
      68006900650072002000650073007400200069006E00740072006F0075007600
      610062006C0065002E000100460069006C006500200063006F0075006C006400
      20006E006F007400200062006500200066006F0075006E0064002E0001000D00
      0A00730074004F00740068006500720053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A007300740043006F006C006C006500
      6300740069006F006E0073005F0055006E00690063006F00640065000D000A00
      540072006500650046006F006C0064006500720073002E0043006F006C007500
      6D006E0073005B0030005D002E0043006800650063006B0042006F0078004600
      690065006C0064002E004600690065006C006400560061006C00750065007300
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      650001000D000A00540072006500650046006F006C0064006500720073002E00
      43006F006C0075006D006E0073005B0030005D002E005400690074006C006500
      2E00430061007000740069006F006E0001004D0061007000700065006E000100
      44006F00730073006900650072007300010046006F006C006400650072007300
      01000D000A0047007200690064004100740074006100630068006D0065006E00
      740073002E0043006F006C0075006D006E0073005B0030005D002E0043006800
      650063006B0042006F0078004600690065006C0064002E004600690065006C00
      6400560061006C00750065007300010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C0073006500010074007200
      750065003B00660061006C007300650001000D000A0047007200690064004100
      740074006100630068006D0065006E00740073002E0043006F006C0075006D00
      6E0073005B0030005D002E005400690074006C0065002E004300610070007400
      69006F006E0001004F006D00730063006800720069006A00760069006E006700
      01004400650073006300720069007000740069006F006E000100440065007300
      6300720069007000740069006F006E0001000D000A0047007200690064004100
      740074006100630068006D0065006E00740073002E0043006F006C0075006D00
      6E0073005B0031005D002E0043006800650063006B0042006F00780046006900
      65006C0064002E004600690065006C006400560061006C007500650073000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      01000D000A0047007200690064004100740074006100630068006D0065006E00
      740073002E0043006F006C0075006D006E0073005B0031005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001005400790070006500
      010054007900700065000100540079007000650001000D000A00470072006900
      64004100740074006100630068006D0065006E00740073002E0043006F006C00
      75006D006E0073005B0032005D002E0043006800650063006B0042006F007800
      4600690065006C0064002E004600690065006C006400560061006C0075006500
      7300010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      7300650001000D000A0047007200690064004100740074006100630068006D00
      65006E00740073002E0043006F006C0075006D006E0073005B0032005D002E00
      5400690074006C0065002E00430061007000740069006F006E00010054006900
      6A00640073007400690070002000750070006C006F0061006400010054006500
      6D007000730020006400650020007400E9006C00E90063006800610072006700
      65006D0065006E0074000100540069006D00650020006F006600200075007000
      6C006F006100640001000D000A00730074004300680061007200530065007400
      73005F0055006E00690063006F00640065000D000A00}
  end
end
