unit ContributorsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniPanel, uniImage, uniGUIBaseClasses,
  siComp, siLngLnk, uniTreeView, uniTreeMenu, Vcl.Menus, uniMainMenu, uniButton,
  UniThemeButton;

type
  TContributorsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerHeaderLeft: TUniContainerPanel;
    ContainerHeaderRight: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerBody: TUniContainerPanel;
    ContainerContributorModule: TUniContainerPanel;
    LinkedLanguageContributorsMain: TsiLangLinked;
    ContainerMenu: TUniContainerPanel;
    PanelHeaderContributor: TUniPanel;
    ImageLogoContributor: TUniImage;
    LblUserName: TUniLabel;
    ImageProfile: TUniImage;
    PnlNews: TUniPanel;
    LblNews: TUniLabel;
    PnlCredits: TUniPanel;
    LblCredits: TUniLabel;
    PnlDashboardCredits: TUniPanel;
    LblDashboardCredits: TUniLabel;
    PnlCreditFiles: TUniPanel;
    LblCreditFiles: TUniLabel;
    PnlDocumentsCredits: TUniPanel;
    LblDocumentsCredits: TUniLabel;
    PnlCommissionCredits: TUniPanel;
    LblCommissionCredits: TUniLabel;
    PnlInsurances: TUniPanel;
    LblInsurances: TUniLabel;
    PnlDashboardInsurances: TUniPanel;
    LblDashboardInsurances: TUniLabel;
    PnlFilesInsurances: TUniPanel;
    LblFilesInsurances: TUniLabel;
    PnlDocumentsInsurances: TUniPanel;
    LblDocumentsInsurances: TUniLabel;
    PnlBackLogInsurances: TUniPanel;
    LblBackLogInsurances: TUniLabel;
    PnlListEighteenYearInsurances: TUniPanel;
    LblListEighteenYearInsurances: TUniLabel;
    PnlCRM: TUniPanel;
    LblCRM: TUniLabel;
    PnlDocumentArchive: TUniPanel;
    LblDocumentArchive: TUniLabel;
    PnlAgenda: TUniPanel;
    LblAgenda: TUniLabel;
    PanelMainNavigation: TUniPanel;
    LblMainNavigation: TUniLabel;
    BtnExit: TUniThemeButton;
    BtnDashboardCredits: TUniThemeButton;
    BtnCreditFiles: TUniThemeButton;
    BtnDocumentsCredits: TUniThemeButton;
    BtnCommissionCredits: TUniThemeButton;
    BtnNews: TUniThemeButton;
    BtnDashboardInsurances: TUniThemeButton;
    BtnFilesInsurances: TUniThemeButton;
    BtnDocumentsInsurances: TUniThemeButton;
    BtnBackLogInsurances: TUniThemeButton;
    BtnListEighteenYearInsurances: TUniThemeButton;
    BtnDocumentArchive: TUniThemeButton;
    BtnAgenda: TUniThemeButton;
    PnlInvalidSepa: TUniPanel;
    BtnInvalidSepas: TUniThemeButton;
    LblInvalidSepas: TUniLabel;
    PnlQuotes: TUniPanel;
    BtnQuotes: TUniThemeButton;
    LblQuotes: TUniLabel;
    PnlDocuments: TUniPanel;
    BtnDocuments: TUniThemeButton;
    LblDocuments: TUniLabel;
    procedure BtnExitClick(Sender: TObject);
    procedure ImageProfileClick(Sender: TObject);
    procedure PnlNewsClick(Sender: TObject);
    procedure PnlDashboardCreditsClick(Sender: TObject);
    procedure PnlCreditFilesClick(Sender: TObject);
    procedure PnlDocumentsCreditsClick(Sender: TObject);
    procedure PnlCommissionCreditsClick(Sender: TObject);
    procedure PnlDashboardInsurancesClick(Sender: TObject);
    procedure PnlFilesInsurancesClick(Sender: TObject);
    procedure PnlDocumentsInsurancesClick(Sender: TObject);
    procedure PnlBackLogInsurancesClick(Sender: TObject);
    procedure PnlListEighteenYearInsurancesClick(Sender: TObject);
    procedure PnlDocumentArchiveClick(Sender: TObject);
    procedure PnlAgendaClick(Sender: TObject);
    procedure PnlInvalidSepaClick(Sender: TObject);
    procedure PnlQuotesClick(Sender: TObject);
    procedure PnlDocumentsClick(Sender: TObject);
  private
    { Private declarations }
    ActiveFrameContributors: TUniFrame;
    procedure CallBackContributorProfile(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure start_contributor_module;
  end;

implementation

{$R *.dfm}

uses MainModule, CreditFilesMain, DashboardContributors, CommissionsMain,
     ContributorDocumentsMain, AgendaMain, News, BackLogList, EighteenYearList,
     ContributorArchiveMain, InsuranceFilesMain, ContributorProfileMain,
     InsuranceStatementsMain, DashboardInsuranceFiles, SwissLifeDemandsMain,
  InvalidSepa, InsuranceQuotesMain, DocumentsMain;

{ TContributorsMainFrm }

procedure TContributorsMainFrm.BtnExitClick(Sender: TObject);
begin
  UniSession.Terminate('<script>window.location.href = "' + UniMainModule.Portal_return_url + '";</script>');
end;

procedure TContributorsMainFrm.CallBackContributorProfile(Sender: TComponent;
  AResult: Integer);
begin
  LblUserName.Caption    := UniMainModule.User_name;
end;

procedure TContributorsMainFrm.ImageProfileClick(Sender: TObject);
begin
  with ContributorProfileMainFrm
  do begin
       Start_contributor_profile_module;
       ShowModal(CallBackContributorProfile);
     end;
end;

procedure TContributorsMainFrm.PnlNewsClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TNewsFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TNewsFrm.Create(Self);
  With (ActiveFrameContributors as TNewsFrm)
  do begin
       Start_news_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlQuotesClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and
     (ActiveFrameContributors.ClassType = TInsuranceQuotesMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TInsuranceQuotesMainFrm.Create(Self);
  With (ActiveFrameContributors as TInsuranceQuotesMainFrm)
  do begin
       Start_insurance_quotes_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlAgendaClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TAgendaMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TAgendaMainFrm.Create(Self);
  With (ActiveFrameContributors as TAgendaMainFrm)
  do Parent := ContainerContributorModule;
end;

procedure TContributorsMainFrm.PnlBackLogInsurancesClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TBackLogListFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TBackLogListFrm.Create(Self);
  With (ActiveFrameContributors as TBackLogListFrm)
  do begin
       Start_backlog_list_insurance_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlCommissionCreditsClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TCommissionsMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TCommissionsMainFrm.Create(Self);
  With (ActiveFrameContributors as TCommissionsMainFrm)
  do begin
       Start_contributors_commission;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlCreditFilesClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TCreditFilesMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TCreditFilesMainFrm.Create(Self);
  With (ActiveFrameContributors as TCreditFilesMainFrm)
  do begin
       Start_credit_files_management_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlDashboardCreditsClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TDashboardContributorsFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TDashboardContributorsFrm.Create(Self);
  With (ActiveFrameContributors as TDashboardContributorsFrm)
  do begin
       Start_dashboard_contributors;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlDashboardInsurancesClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TDashboardInsuranceFilesFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TDashboardInsuranceFilesFrm.Create(Self);
  With (ActiveFrameContributors as TDashboardInsuranceFilesFrm)
  do begin
       ContributorsFrm := Self;
       Start_dashboard_insurancefiles;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlDocumentArchiveClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TContributorArchiveMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TContributorArchiveMainFrm.Create(Self);
  With (ActiveFrameContributors as TContributorArchiveMainFrm)
  do begin
       Start_contributor_archive_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlDocumentsClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TDocumentsMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TDocumentsMainFrm.Create(Self);

  With (ActiveFrameContributors as TDocumentsMainFrm)
  do begin
       Start_documents;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlDocumentsCreditsClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TContributorDocumentsMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TContributorDocumentsMainFrm.Create(Self);
  With (ActiveFrameContributors as TContributorDocumentsMainFrm)
  do begin
       Start_contributor_documents;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlDocumentsInsurancesClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TInsuranceStatementsMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TInsuranceStatementsMainFrm.Create(Self);
  With (ActiveFrameContributors as TInsuranceStatementsMainFrm)
  do begin
       Start_insurance_statements_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlFilesInsurancesClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TInsuranceFilesMainFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TInsuranceFilesMainFrm.Create(Self);
  With (ActiveFrameContributors as TInsuranceFilesMainFrm)
  do begin
       Start_insurance_files_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlInvalidSepaClick(Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and
     (ActiveFrameContributors.ClassType = TInvalidSepaFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TInvalidSepaFrm.Create(Self);
  With (ActiveFrameContributors as TInvalidSepaFrm)
  do begin
       Start_invalid_sepa_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.PnlListEighteenYearInsurancesClick(
  Sender: TObject);
begin
  if (ActiveFrameContributors <> nil) and (ActiveFrameContributors.ClassType = TEighteenYearListFrm)
  then exit;

  if (ActiveFrameContributors <> nil)
  then FreeAndNil(ActiveFrameContributors);

  ActiveFrameContributors := TEighteenYearListFrm.Create(Self);
  With (ActiveFrameContributors as TEighteenYearListFrm)
  do begin
       Start_eighteen_year_list_module;
       Parent := ContainerContributorModule;
     end;
end;

procedure TContributorsMainFrm.start_contributor_module;
begin
  if UniMainModule.Portal_theme_color <> 0
  then begin
         ContainerHeaderLeft.Color    := UniMainModule.Portal_theme_color;
         ContainerHeaderRight.Color   := UniMainModule.Portal_theme_color;
         ContainerHeader.Color        := UniMainModule.Portal_theme_color;
         PanelHeaderContributor.Color := UniMainModule.Portal_theme_color;
         //if UniMainModule.Portal_theme_color = 2372865
         //then begin
                //BtnNews.JSInterface.JSCall('setCls', 'icon-blue');
         //     end;
       end;


  case UniMainModule.LanguageManager.ActiveLanguage of
  1: LblTitle.Caption    := UniMainModule.Portal_title_dutch;
  2: LblTitle.Caption    := UniMainModule.Portal_title_french;
  3: LblTitle.Caption    := UniMainModule.Portal_title_english;
  end;
  LblUserName.Caption    := UniMainModule.User_name;

  //Set the menu icons in function of the active modules
  if UniMainModule.Contributor_collaboration_stopped = False
  then PnlNews.Visible                          := UniMainModule.portal_contr_show_news
  else PnlNews.Visible                          := False;

  PnlCredits.Visible                       := UniMainModule.portal_contr_credit_module_active;
  PnlDashboardCredits.Visible              := UniMainModule.portal_contr_credit_module_active;
  PnlCreditFiles.Visible                   := UniMainModule.portal_contr_credit_module_active;
  PnlDocumentsCredits.Visible              := UniMainModule.portal_contr_credit_module_active;
  if (UniMainModule.portal_contr_credit_module_active = True) and
     (UniMainModule.Show_documents_contributor = True)
  then PnlDocumentsCredits.Visible         := True
  else PnlDocumentsCredits.Visible         := False;
  PnlCommissionCredits.Visible             := UniMainModule.portal_contr_credit_module_active;
  if (UniMainModule.portal_contr_credit_module_active = True) and
     (UniMainModule.Show_commissions_contributor = True)
  then PnlCommissionCredits.Visible        := True
  else PnlCommissionCredits.Visible        := False;

  PnlInsurances.Visible                    := UniMainModule.portal_contr_insurance_module_active;
  PnlDashboardInsurances.Visible           := UniMainModule.portal_contr_insurance_module_active;
  PnlFilesInsurances.Visible               := UniMainModule.portal_contr_insurance_module_active;
  if (UniMainModule.portal_contr_insurance_module_active = True) and
     (UniMainModule.Show_commissions_contributor = True)
  then PnlDocumentsInsurances.Visible      := True
  else PnlDocumentsInsurances.Visible      := False;
  PnlBackLogInsurances.Visible             := UniMainModule.portal_contr_insurance_module_active;
  PnlListEighteenYearInsurances.Visible    := UniMainModule.portal_contr_insurance_module_active;
  PnlInvalidSepa.Visible                   := UniMainModule.portal_contr_insurance_module_active;
  PnlQuotes.Visible                        := UniMainModule.portal_contr_insurance_module_active;
  PnlDocuments.Visible                     := UniMainModule.portal_contr_insurance_module_active;

  if (UniMainModule.portal_contr_show_doc_archive = True) or
     (UniMainModule.portal_contr_show_agenda = True)      or
     (UniMainModule.portal_contr_show_tasks = True)       or
     (UniMainModule.portal_contr_show_phone_calls = True)
  then PnlCRM.Visible                      := True
  else PnlCRM.Visible                      := False;
  PnlDocumentArchive.Visible               := UniMainModule.portal_contr_show_doc_archive;
  PnlAgenda.Visible                        := UniMainModule.portal_contr_show_agenda;

  //Bijkomend recht ter hoogte van de krediettussenpersonen
  if UniMainModule.User_portal_rights = 5 then begin
    PnlDashboardCredits.Visible := True;
    PnlCreditFiles.Visible := False;
    PnlDocumentsCredits.Visible := False;
    PnlCommissionCredits.Visible := False;
    PnlInsurances.Visible := False;
    PnlDashboardInsurances.Visible := False;
    PnlFilesInsurances.Visible := False;
    PnlDocumentsInsurances.Visible := False;
    PnlBackLogInsurances.Visible := False;
    PnlListEighteenYearInsurances.Visible := False;
    PnlInvalidSepa.Visible := False;
    PnlQuotes.Visible := False;
    PnlDocumentArchive.Visible := False;
  end;


  if (UniMainModule.portal_contr_show_news = True) and (UniMainModule.Contributor_collaboration_stopped = False)
  then PnlNewsClick(nil)
  else if UniMainModule.portal_contr_credit_module_active = True
       then PnlDashboardCreditsClick(nil)
       else if UniMainModule.portal_contr_insurance_module_active = True
            then PnlDashboardInsurancesClick(nil);
end;

end.
