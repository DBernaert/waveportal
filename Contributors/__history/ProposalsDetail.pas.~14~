unit ProposalsDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniFileUpload, Data.DB, MemDS, DBAccess, IBC, uniButton, UniThemeButton,
  uniURLFrame, uniDBText, uniLabel, uniPanel, siComp, siLngLnk, uniEdit, uniDBEdit, uniDateTimePicker,
  uniDBDateTimePicker;

var
	str_error_removing:           string = 'Fout bij het verwijderen.'; // TSI: Localized (Don't modify!)
	str_error_creating_directory: string = 'Fout bij het aanmaken van de directory, contacteer WAVEDESK!'; // TSI: Localized (Don't modify!)
	str_caption:                  string = 'Detailgegevens voorstel'; // TSI: Localized (Don't modify!)
	str_error_saving:             string = 'Fout bij het opslaan van de gegevens!'; // TSI: Localized (Don't modify!)

type
  TProposalsDetailFrm = class(TUniForm)
    ContainerHeader: TUniContainerPanel;
    LblDemandedAmount: TUniLabel;
    LblStart: TUniLabel;
    LblInclFire2: TUniLabel;
    LblMonthly: TUniLabel;
    LblMaxMonthly: TUniLabel;
    LblInclSSV: TUniLabel;
    LblInclFire: TUniLabel;
    LblTotalRent: TUniLabel;
    LblInclSSV2: TUniLabel;
    LblTotalSSV: TUniLabel;
    LblMax: TUniLabel;
    LblMaxTotal: TUniLabel;
    LblTotalFire: TUniLabel;
    PdfFrame: TUniPDFFrame;
    ContainerFooter: TUniContainerPanel;
    LinkedLanguageProposalsDetail: TsiLangLinked;
    LblTotal: TUniLabel;
    LblDbFormula: TUniDBText;
    LblFormula: TUniLabel;
    LblCreditProvider: TUniLabel;
    LblDbCreditProvider: TUniDBText;
    LblProposalNumber: TUniLabel;
    LblDbProposalNumber: TUniDBText;
    LblDbBorrowedAmount: TUniDBText;
    LblDbStartRedemption: TUniDBText;
    LblDbStartDebtBalIncl: TUniDBText;
    LblDbStartFireInsIncl: TUniDBText;
    LblDbMaxRedemption: TUniDBText;
    LblDbMaxRedemptDebtBalIncl: TUniDBText;
    LblDbMaxRedemptFireInsIncl: TUniDBText;
    LblDbTotalInterest: TUniDBText;
    LblDbTotalInterestDebtBalance: TUniDBText;
    LblDbTotalInterestFireInsurance: TUniDBText;
    LblDbGeneralTotal: TUniDBText;
    LblDuration: TUniLabel;
    LblDbDuration: TUniDBText;
    LblCreditAllowance: TUniLabel;
    LblDbCreditAllowance: TUniDBText;
    LblProposalDepositnumber: TUniLabel;
    LblDbProposalDepositnumber: TUniDBText;
    LblTarificationNumber: TUniLabel;
    LblDbTarificationNumber: TUniDBText;
    LblTarificationdate: TUniLabel;
    LblDbTarificationdate: TUniDBText;
    LblProspectusnumber: TUniLabel;
    LblDbProspectusnumber: TUniDBText;
    LblProspectusDate: TUniLabel;
    LblDbProspectusDate: TUniDBText;
    LblQuotity: TUniLabel;
    LblDbQuotity: TUniDBText;
    LblBasicInterestRate: TUniLabel;
    LblDbBasicInterestRate: TUniDBText;
    LblProductReduction: TUniLabel;
    LblDbProductReduction: TUniDBText;
    LblQualityReduction: TUniLabel;
    LblDbQualityReduction: TUniDBText;
    LblExtrareduction: TUniLabel;
    LblDbExtrareduction: TUniDBText;
    LblQuotityallowance: TUniLabel;
    LblDbQuotityallowance: TUniDBText;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure LinkedLanguageProposalsDetailChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
    procedure Load_pdf;
  public
    { Public declarations }
    procedure Start_view_proposal(ProposalId: longint);
  end;

function ProposalsDetailFrm: TProposalsDetailFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, IOUtils, ServerModule;

function ProposalsDetailFrm: TProposalsDetailFrm;
begin
  Result := TProposalsDetailFrm(UniMainModule.GetFormInstance(TProposalsDetailFrm));
end;

{ TProposalsDetailFrm }

procedure TProposalsDetailFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TProposalsDetailFrm.LinkedLanguageProposalsDetailChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TProposalsDetailFrm.Load_pdf;
var Url: string;
begin
  UniMainModule.Files_Archive_Edit.Close;
  UniMainModule.Files_Archive_Edit.SQL.Clear;
  UniMainModule.Files_Archive_Edit.SQL.Add('Select * from files_archive where Module_Id=6 and Record_Id=' +
                                           UniMainModule.CreditProposals_Edit.FieldByName('Id').asString +
                                           ' and removed=''0''');
  UniMainModule.Files_Archive_Edit.Open;
  if UniMainModule.Files_Archive_Edit.Recordcount = 1
  then begin
         Url := UniMainModule.Get_pdf_url(UniMainModule.Root_archive + UniMainModule.Company_account_name + '\' +
                                          UniMainModule.Files_Archive_Edit.FieldByName('Destination_Name').asString,
                                          UniMainModule.Files_Archive_Edit.FieldByName('Original_Name').AsString);
         UniMainModule.Files_Archive_Edit.Close;
         if Trim(Url) <> ''
         then PdfFrame.PdfURL := Url
         else PdfFrame.PdfURL := '';
       end
  else begin
         UniMainModule.Files_Archive_Edit.Close;
         PdfFrame.PdfURL := '';
       end;
end;

procedure TProposalsDetailFrm.Start_view_proposal(ProposalId: longint);
begin
  Caption := str_caption;
  UniMainModule.CreditProposals_Edit.Close;
  UniMainModule.CreditProposals_Edit.SQL.Clear;
  UniMainModule.CreditProposals_Edit.SQL.Add('Select * from credit_proposals where id=' + IntToStr(ProposalId));
  UniMainModule.CreditProposals_Edit.Open;
  Load_pdf;
end;

procedure TProposalsDetailFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TProposalsDetailFrm.UpdateStrings;
begin
  str_error_saving             := LinkedLanguageProposalsDetail.GetTextOrDefault('strstr_error_saving' (* 'Fout bij het opslaan van de gegevens!' *) );
  str_caption                  := LinkedLanguageProposalsDetail.GetTextOrDefault('strstr_caption' (* 'Detailgegevens voorstel' *) );
  str_error_creating_directory := LinkedLanguageProposalsDetail.GetTextOrDefault('strstr_error_creating_directory' (* 'Fout bij het aanmaken van de directory, contacteer WAVEDESK!' *) );
  str_error_removing           := LinkedLanguageProposalsDetail.GetTextOrDefault('strstr_error_removing' (* 'Fout bij het verwijderen.' *) );
end;

end.


