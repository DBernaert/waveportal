unit ProposalsPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid,
  uniDBNavigator, uniEdit, uniGUIBaseClasses, uniPanel, uniFileUpload, siComp, siLngLnk,
  uniImage;

var
	str_delete_proposal: string     = 'Verwijderen voorstel?'; // TSI: Localized (Don't modify!)
	str_error_deleting: string      = 'Fout bij het verwijderen: '; // TSI: Localized (Don't modify!)
	str_export_propositions: string = 'Export voorstellen'; // TSI: Localized (Don't modify!)

type
  TProposalsPanelFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    GridProposals: TUniDBGrid;
    Proposals: TIBCQuery;
    DsProposals: TDataSource;
    ProposalsID: TLargeintField;
    ProposalsCOMPANYID: TLargeintField;
    ProposalsCREDIT_FILE: TLargeintField;
    ProposalsPROPOSAL_NUMBER: TIntegerField;
    ProposalsPROPOSAL_DEPOSIT_NUMBER: TIntegerField;
    ProposalsCREDIT_PROVIDER: TWideStringField;
    ProposalsFORMULA: TWideStringField;
    ProposalsBORROWED_AMOUNT: TFloatField;
    ProposalsDURATION: TIntegerField;
    ProposalsSTART_FIRE_INS_INCL: TFloatField;
    ProposalsGENERAL_TOTAL: TFloatField;
    LinkedLanguageProposalsPanel: TsiLangLinked;
    BtnViewProposal: TUniThemeButton;
    BtnExpandGrid: TUniThemeButton;
    BtnCollapseGrid: TUniThemeButton;
    procedure BtnViewProposalClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure LinkedLanguageProposalsPanelChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure GridProposalsColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
    procedure GridProposalsColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
    procedure GridProposalsColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
      Attribs: TUniCellAttribs; var Result: string);
    procedure BtnCollapseGridClick(Sender: TObject);
    procedure BtnExpandGridClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_proposals(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, ServerModule, IOUtils, Main, ProposalsDetail;

{ TProposalsPanelFrm }

procedure TProposalsPanelFrm.BtnCollapseGridClick(Sender: TObject);
begin
  GridProposals.JSInterface.JSCall('view.features[0].collapseAll', []);
end;

procedure TProposalsPanelFrm.BtnExpandGridClick(Sender: TObject);
begin
  GridProposals.JSInterface.JSCall('view.features[0].expandAll', []);
end;

procedure TProposalsPanelFrm.BtnViewProposalClick(Sender: TObject);
begin
  if (Proposals.Active) and (not Proposals.FieldByName('ID').IsNull)
  then begin
         With ProposalsDetailFrm
         do begin
              Start_view_proposal(Proposals.FieldByName('Id').AsInteger);
              ShowModal();
            end;
       end;
end;

procedure TProposalsPanelFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' CREDIT_PROVIDER LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' FORMULA LIKE ''%'         + EditSearch.Text + '%''';
  Proposals.Filter := Filter;
end;

procedure TProposalsPanelFrm.GridProposalsColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
begin
  //
end;

procedure TProposalsPanelFrm.GridProposalsColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if (SameText(UpperCase(Column.FieldName), 'GENERAL_TOTAL')) or
     (SameText(UpperCase(Column.FieldName), 'BORROWED_AMOUNT')) or
     (SameText(UpperCase(Column.FieldName), 'START_FIRE_INS_INCL'))
  then begin
         if Column.AuxValue = NULL then Column.AuxValue:=0.0;
         Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
       end;
end;

procedure TProposalsPanelFrm.GridProposalsColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
  Attribs: TUniCellAttribs; var Result: string);
var F : Real;
begin
  inherited;
  if (SameText(UpperCase(Column.FieldName), 'GENERAL_TOTAL')) or
     (SameText(UpperCase(Column.FieldName), 'BORROWED_AMOUNT')) or
     (SameText(UpperCase(Column.FieldName), 'START_FIRE_INS_INCL'))
  then begin
         F := Column.AuxValue;
         if Column.AuxValues[1] = NULL
         then Column.AuxValues[1] := 0;
         Column.AuxValues[1] := Column.AuxValues[1] + F;
         Result:= FormatCurr('#,##0.00 �', F);
         Attribs.Font.Style := [fsBold];
         Attribs.Font.Color := UniMainModule.Color_blue;
       end;
  Column.AuxValue:=NULL;
end;

procedure TProposalsPanelFrm.LinkedLanguageProposalsPanelChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TProposalsPanelFrm.Start_proposals(IdModule: integer; MasterDs: TDataSource);
var SQL: string;
begin
  Proposals.Close;
  Proposals.SQL.Clear;
  SQL := 'Select id, credit_file, companyid, proposal_number, proposal_deposit_number, credit_provider, formula, duration, ' +
         'borrowed_amount, general_total, start_fire_ins_incl from credit_proposals ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' ' +
         'order by proposal_number, proposal_deposit_number';
  Proposals.SQL.Add(SQL);
  Proposals.MasterSource := MasterDs;
  Proposals.MasterFields := 'ID';
  Proposals.DetailFields := 'CREDIT_FILE';
  Proposals.Open;
end;

procedure TProposalsPanelFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TProposalsPanelFrm.UpdateStrings;
begin
  str_export_propositions := LinkedLanguageProposalsPanel.GetTextOrDefault('strstr_export_propositions' (* 'Export voorstellen' *) );
  str_error_deleting      := LinkedLanguageProposalsPanel.GetTextOrDefault('strstr_error_deleting' (* 'Fout bij het verwijderen: ' *) );
  str_delete_proposal     := LinkedLanguageProposalsPanel.GetTextOrDefault('strstr_delete_proposal' (* 'Verwijderen voorstel?' *) );
end;

end.
