unit SelectContributorAccount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniGUIBaseClasses,
  uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

type
  TSelectContributorAccountFrm = class(TUniForm)
    GridAccounts: TUniDBGrid;
    BtnSelect: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    TblAccounts: TIBCQuery;
    DsAccounts: TDataSource;
    LinkedLanguageSelectContributorAccount: TsiLangLinked;
    TblAccountsID: TLargeintField;
    TblAccountsCONTRIBUTOR_NUMBER: TWideStringField;
    TblAccountsPORTAL_DESCRIPTION: TWideStringField;
    TblAccountsAccount_description: TStringField;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSelectClick(Sender: TObject);
    procedure TblAccountsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function Init_contributor_account_selection(ContributorEmail: string): boolean;
  end;

function SelectContributorAccountFrm: TSelectContributorAccountFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function SelectContributorAccountFrm: TSelectContributorAccountFrm;
begin
  Result := TSelectContributorAccountFrm(UniMainModule.GetFormInstance(TSelectContributorAccountFrm));
end;

{ TSelectContributorAccountFrm }

procedure TSelectContributorAccountFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TSelectContributorAccountFrm.BtnSelectClick(Sender: TObject);
begin
  if (TblAccounts.Active = False) or (TblAccounts.FieldByName('Id').IsNull)
  then exit;

  UniMainModule.Result_dbAction := TblAccounts.FieldByName('Id').AsInteger;
  Close;
end;

function TSelectContributorAccountFrm.Init_contributor_account_selection(
  ContributorEmail: string): boolean;
var SQL: string;
begin
  TblAccounts.Close;
  TblAccounts.SQL.Clear;
  SQL := 'Select id, contributor_number, portal_description from credit_contributors ' +
         'where companyid=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' ' +
         'and email=' + QuotedStr(ContributorEmail) + ' ' +
         'and portal_active=1 order by Portal_description';
  TblAccounts.SQL.Add(SQL);
  TblAccounts.Open;
  Result := True;
end;

procedure TSelectContributorAccountFrm.TblAccountsCalcFields(DataSet: TDataSet);
begin
  if Trim(TblAccounts.FieldByName('Contributor_number').AsString) <> ''
  then TblAccounts.FieldByName('Account_description').AsString :=
       TblAccounts.FieldByName('Contributor_number').AsString;

  if Trim(TblAccounts.FieldByName('Portal_description').AsString) <> ''
  then begin
    if Trim(TblAccounts.FieldByName('Contributor_number').AsString) <> ''
    then TblAccounts.FieldByName('Account_description').AsString :=
         TblAccounts.FieldByName('Account_description').AsString + ' - ' +
         TblAccounts.FieldByName('Portal_description').AsString
    else TblAccounts.FieldByName('Portal_description').AsString :=
         TblAccounts.FieldByName('Contributor_number').AsString;
  end;
end;

end.
