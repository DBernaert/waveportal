object DocumentsMainFrm: TDocumentsMainFrm
  Left = 0
  Top = 0
  Width = 1326
  Height = 818
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1326
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 120
      Height = 30
      Hint = ''
      Caption = 'Documenten'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1326
    Height = 30
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerFilterRight: TUniContainerPanel
      Left = 1298
      Top = 0
      Width = 28
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      object BtnDownload: TUniThemeButton
        Left = 0
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Downloaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 55
        OnClick = BtnDownloadClick
        ButtonTheme = uctPrimary
      end
    end
  end
  object GridAttachments: TUniDBGrid
    Left = 0
    Top = 63
    Width = 1326
    Height = 755
    Hint = ''
    DataSource = DsAttachments
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Cls = 'customGrid'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 8 8 8'
    Align = alClient
    TabOrder = 2
    OnDblClick = BtnDownloadClick
    Columns = <
      item
        Flex = 1
        FieldName = 'ORIGINAL_NAME'
        Title.Caption = 'Naam'
        Width = 350
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'TYPEDESCRIPTION'
        Title.Caption = 'Type'
        Width = 300
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'DESCRIPTION'
        Title.Caption = 'Omschrijving'
        Width = 300
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'DATE_ENTERED'
        Title.Caption = 'Tijdstip upload'
        Width = 135
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object Attachments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select files_archive.id, files_archive.original_name, files_arch' +
        'ive.destination_name,'
      
        'files_archive.description, files_archive.date_entered, files_arc' +
        'hive.folder,'
      'files_archive.subfolder, files_archive.file_type,'
      'basictables.dutch typedescription'
      'from files_archive'
      
        'left outer join basictables on (files_archive.file_type = basict' +
        'ables.id)')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 640
    Top = 208
  end
  object DsAttachments: TDataSource
    DataSet = Attachments
    Left = 640
    Top = 264
  end
  object LinkedLanguageDocumentsMain: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST'
      'TWIDESTRINGS')
    SmartExcludeProps.Strings = (
      'TDocumentsMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerFilter.Layout'
      'ContainerFilterRight.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 640
    Top = 328
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C006500010044006F006300
      75006D0065006E00740065006E00010044006F00630075006D0065006E007400
      7300010044006F00630075006D0065006E007400730001000D000A0073007400
      480069006E00740073005F0055006E00690063006F00640065000D000A004200
      74006E0044006F0077006E006C006F0061006400010044006F0077006E006C00
      6F006100640065006E0001005400E9006C00E900630068006100720067006500
      7200010044006F0077006E006C006F006100640001000D000A00730074004400
      6900730070006C00610079004C006100620065006C0073005F0055006E006900
      63006F00640065000D000A007300740046006F006E00740073005F0055006E00
      690063006F00640065000D000A00730074004D0075006C00740069004C006900
      6E00650073005F0055006E00690063006F00640065000D000A00730074005300
      7400720069006E00670073005F0055006E00690063006F00640065000D000A00
      730074004F00740068006500720053007400720069006E00670073005F005500
      6E00690063006F00640065000D000A007300740043006F006C006C0065006300
      740069006F006E0073005F0055006E00690063006F00640065000D000A004700
      7200690064004100740074006100630068006D0065006E00740073002E004300
      6F006C0075006D006E0073005B0030005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A00470072006900640041007400740061006300
      68006D0065006E00740073002E0043006F006C0075006D006E0073005B003000
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      4E00610061006D0001004E006F006D0001004E0061006D00650001000D000A00
      47007200690064004100740074006100630068006D0065006E00740073002E00
      43006F006C0075006D006E0073005B0031005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A0047007200690064004100740074006100
      630068006D0065006E00740073002E0043006F006C0075006D006E0073005B00
      31005D002E005400690074006C0065002E00430061007000740069006F006E00
      0100540079007000650001005400790070006500010054007900700065000100
      0D000A0047007200690064004100740074006100630068006D0065006E007400
      73002E0043006F006C0075006D006E0073005B0032005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640041007400
      74006100630068006D0065006E00740073002E0043006F006C0075006D006E00
      73005B0032005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001004F006D00730063006800720069006A00760069006E0067000100
      4400650073006300720069007000740069006F006E0001004400650073006300
      720069007000740069006F006E0001000D000A00470072006900640041007400
      74006100630068006D0065006E00740073002E0043006F006C0075006D006E00
      73005B0033005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A0047007200690064004100740074006100630068006D0065006E007400
      73002E0043006F006C0075006D006E0073005B0033005D002E00540069007400
      6C0065002E00430061007000740069006F006E000100540069006A0064007300
      7400690070002000750070006C006F00610064000100540065006D0070007300
      20006400650020007400E9006C00E900630068002E000100550070006C006F00
      610064002000740069006D00650001000D000A00730074004300680061007200
      53006500740073005F0055006E00690063006F00640065000D000A00}
  end
end
