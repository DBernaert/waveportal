object DashboardContributorsFrm: TDashboardContributorsFrm
  Left = 0
  Top = 0
  Width = 1302
  Height = 844
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1302
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerHeaderRight: TUniContainerPanel
      Left = 1154
      Top = 0
      Width = 148
      Height = 33
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      TabStop = False
      LayoutConfig.Region = 'east'
      object LblYear: TUniLabel
        Left = 38
        Top = 7
        Width = 73
        Height = 19
        Hint = ''
        Alignment = taCenter
        AutoSize = False
        Caption = 'LblYear'
        ParentFont = False
        Font.Height = -16
        TabOrder = 1
        LayoutConfig.Cls = 'boldtext'
      end
      object BtnPreviousYear: TUniThemeButton
        Left = 0
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Vorig jaar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 42
        OnClick = BtnPreviousYearClick
        ButtonTheme = uctPrimary
      end
      object BtnNextYear: TUniThemeButton
        Left = 120
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Volgend jaar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 43
        OnClick = BtnNextYearClick
        ButtonTheme = uctPrimary
      end
    end
    object ContainerHeaderLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 353
      Height = 33
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 2
      LayoutConfig.Region = 'west'
      object LblTitle: TUniLabel
        Left = 0
        Top = 0
        Width = 101
        Height = 30
        Hint = ''
        Caption = 'Dashboard'
        ParentFont = False
        Font.Height = -21
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
            '= "lfp-pagetitle";'#13#10'}')
        TabOrder = 1
        LayoutConfig.Cls = 'boldtext'
      end
    end
  end
  object PanelContainer: TUniPanel
    Left = 16
    Top = 56
    Width = 1265
    Height = 769
    Hint = ''
    TabOrder = 1
    Caption = ''
    Layout = 'vbox'
    LayoutConfig.Region = 'center'
    object ContainerTop: TUniContainerPanel
      Left = 16
      Top = 16
      Width = 1081
      Height = 289
      Hint = ''
      ParentColor = False
      TabOrder = 1
      Layout = 'hbox'
      LayoutConfig.Flex = 1
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '8 8 4 8'
      object ContainerGeneral: TUniContainerPanel
        Left = 24
        Top = 16
        Width = 457
        Height = 265
        Hint = ''
        ParentColor = False
        Color = clWhite
        TabOrder = 2
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 4 0 0'
        object HtmlFrame: TUniHTMLFrame
          Left = 8
          Top = 8
          Width = 441
          Height = 249
          Hint = ''
          HTML.Strings = (
            '<table style="width:100%">'
            '<tr>'
            '<td>'
            '<div class="col-lg-12 control-section">'
            '    <div class="sample_container">'
            '        <!-- Card Component -->'
            '        <div class="e-card e-custom-card">'
            '            <div class="e-card-header">'
            '                <!-- xLarge Circle Avatar-->'
            
              '                <div class="e-avatar e-avatar-circle e-avatar-xl' +
              'arge">'
            
              '                    <img src="/files/images/sigma.png" alt="sigm' +
              'a">'
            '                </div>'
            '                &nbsp;'
            '            </div>'
            '            <div class="e-card-header">'
            '                <div class="e-card-header-caption center">'
            
              '                    <div class="e-card-header-title name"># kred' +
              'ieten</div>'
            '                </div>'
            '            </div>'
            '            <div class="e-card-content">'
            '                    <p class="avatar-content">2</p>'
            '            </div>'
            '        </div>'
            '    </div>'
            '</div></td>'
            '<td>'
            '<div class="col-lg-12 control-section">'
            '    <div class="sample_container">'
            '        <!-- Card Component -->'
            '        <div class="e-card e-custom-card">'
            '            <div class="e-card-header">'
            '                <!-- xLarge Circle Avatar-->'
            
              '                <div class="e-avatar e-avatar-circle e-avatar-xl' +
              'arge">'
            
              '                    <img src="/files/images/bank-building.png" a' +
              'lt="bank-building">'
            '                </div>'
            '                &nbsp;'
            '            </div>'
            '            <div class="e-card-header">'
            '                <div class="e-card-header-caption center">'
            
              '                    <div class="e-card-header-title name">&euro;' +
              ' kredieten</div>'
            '                </div>'
            '            </div>'
            '            <div class="e-card-content">'
            
              '                    <p class="avatar-content">2.000.000,00 &euro' +
              ';</p>'
            '            </div>'
            '        </div>'
            '    </div>'
            '</div>'
            '</td>'
            '<td>'
            '<div class="col-lg-12 control-section">'
            '    <div class="sample_container">'
            '        <!-- Card Component -->'
            '        <div class="e-card e-custom-card">'
            '            <div class="e-card-header">'
            '                <!-- xLarge Circle Avatar-->'
            
              '                <div class="e-avatar e-avatar-circle e-avatar-xl' +
              'arge">'
            
              '                    <img src="/files/images/hand.png" alt="hand"' +
              '>'
            '                </div>'
            '                &nbsp;'
            '            </div>'
            '            <div class="e-card-header">'
            '                <div class="e-card-header-caption center">'
            
              '                    <div class="e-card-header-title name">&euro;' +
              ' commissie</div>'
            '                </div>'
            '            </div>'
            '            <div class="e-card-content">'
            
              '                    <p class="avatar-content">5.000,00 &euro;</p' +
              '>'
            '            </div>'
            '        </div>'
            '    </div>'
            '</div>'
            '</td>'
            '</tr></table>')
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '20 0 0 0'
        end
      end
      object ContainerEvolution: TUniContainerPanel
        Left = 536
        Top = 16
        Width = 473
        Height = 265
        Hint = ''
        ParentColor = False
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 0 0 4'
        object ContainerHeaderEvolution: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 473
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerEmptyHeaderEvolutions: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 473
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alClient
            TabOrder = 1
            Layout = 'border'
            LayoutConfig.Region = 'center'
            object ContainerTitleEvolution: TUniContainerPanel
              Left = 0
              Top = 0
              Width = 377
              Height = 30
              Hint = ''
              ParentColor = False
              Align = alLeft
              TabOrder = 1
              LayoutConfig.Region = 'west'
              object LblEvolution: TUniLabel
                Left = 0
                Top = 0
                Width = 101
                Height = 21
                Hint = ''
                Caption = 'Omzetevolutie'
                ParentFont = False
                Font.Height = -16
                TabOrder = 1
                LayoutConfig.Cls = 'boldtext'
              end
            end
          end
        end
        object ChartEvolution: TUniFSGoogleChart
          Left = 0
          Top = 30
          Width = 473
          Height = 235
          Hint = ''
          ChartType = Bar
          ChartDataSet = CDS
          ChartOptions.Strings = (
            'is3D: true,'
            'legend: { position: "none"}')
          Align = alClient
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '8 0 0 0'
        end
      end
    end
    object ContainerBottom: TUniContainerPanel
      Left = 16
      Top = 312
      Width = 1081
      Height = 289
      Hint = ''
      ParentColor = False
      TabOrder = 2
      Layout = 'hbox'
      LayoutConfig.Flex = 1
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '4 8 8 8'
      object ContainerTasks: TUniContainerPanel
        Left = 16
        Top = 8
        Width = 473
        Height = 265
        Hint = ''
        ParentColor = False
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 4 0 0'
        object ContainerHeaderTask: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 473
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerEmptyHeaderTasks: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 473
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alClient
            TabOrder = 1
            Layout = 'border'
            LayoutConfig.Region = 'center'
            object ContainerTitleTasks: TUniContainerPanel
              Left = 0
              Top = 0
              Width = 257
              Height = 30
              Hint = ''
              ParentColor = False
              Align = alLeft
              TabOrder = 1
              LayoutConfig.Region = 'west'
              object LblTasks: TUniLabel
                Left = 0
                Top = 0
                Width = 39
                Height = 21
                Hint = ''
                Caption = 'Taken'
                ParentFont = False
                Font.Height = -16
                TabOrder = 1
                LayoutConfig.Cls = 'boldtext'
              end
            end
            object ContainerTasksHeaderRight: TUniContainerPanel
              Left = 445
              Top = 0
              Width = 28
              Height = 30
              Hint = ''
              ParentColor = False
              Align = alRight
              TabOrder = 2
              LayoutConfig.Region = 'east'
              object BtnEditTask: TUniThemeButton
                Left = 0
                Top = 0
                Width = 28
                Height = 28
                Hint = 'Wijzigen'
                ShowHint = True
                ParentShowHint = False
                Caption = ''
                TabStop = False
                TabOrder = 1
                Images = UniMainModule.ImageList
                ImageIndex = 11
                OnClick = BtnEditTaskClick
                ButtonTheme = uctPrimary
              end
            end
          end
        end
        object ContainerBodyTasks: TUniContainerPanel
          Left = 0
          Top = 30
          Width = 473
          Height = 235
          Hint = ''
          ParentColor = False
          Align = alClient
          TabOrder = 2
          Layout = 'border'
          LayoutConfig.Region = 'center'
          object GridTasks: TUniDBGrid
            Left = 0
            Top = 0
            Width = 473
            Height = 235
            Hint = ''
            DataSource = DsTasks
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
            WebOptions.Paged = False
            LoadMask.Enabled = False
            LoadMask.Message = 'Loading data...'
            ForceFit = True
            LayoutConfig.Cls = 'customGrid'
            LayoutConfig.Region = 'center'
            LayoutConfig.Margin = '8 0 0 0'
            Align = alClient
            TabOrder = 1
            OnDblClick = BtnEditTaskClick
            OnDrawColumnCell = GridTasksDrawColumnCell
            Columns = <
              item
                FieldName = 'TASK_START'
                Title.Caption = 'Startdatum'
                Width = 100
                Menu.MenuEnabled = False
                Menu.ColumnHideable = False
              end
              item
                FieldName = 'TASK_END'
                Title.Caption = 'Einddatum'
                Width = 100
                Menu.MenuEnabled = False
                Menu.ColumnHideable = False
              end
              item
                Flex = 1
                FieldName = 'TITLE'
                Title.Caption = 'Titel'
                Width = 100
                Menu.MenuEnabled = False
                Menu.ColumnHideable = False
              end
              item
                FieldName = 'TASK_STATUS_DESCRIPTION'
                Title.Caption = 'Status'
                Width = 160
                ReadOnly = True
                Menu.MenuEnabled = False
                Menu.ColumnHideable = False
              end>
          end
        end
      end
      object ContainerCalls: TUniContainerPanel
        Left = 544
        Top = 8
        Width = 473
        Height = 265
        Hint = ''
        ParentColor = False
        TabOrder = 2
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 0 0 4'
        object ContainerHeaderCalls: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 473
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 1
          Layout = 'border'
          LayoutConfig.Region = 'north'
          object ContainerEmptyHeaderCalls: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 473
            Height = 30
            Hint = ''
            ParentColor = False
            Align = alClient
            TabOrder = 1
            Layout = 'border'
            LayoutConfig.Region = 'center'
            object ContainerLblPhones: TUniContainerPanel
              Left = 0
              Top = 0
              Width = 305
              Height = 30
              Hint = ''
              ParentColor = False
              Align = alLeft
              TabOrder = 1
              LayoutConfig.Region = 'west'
              object LblCalls: TUniLabel
                Left = 0
                Top = 0
                Width = 65
                Height = 21
                Hint = ''
                Caption = 'Telefoons'
                ParentFont = False
                Font.Height = -16
                TabOrder = 1
                LayoutConfig.Cls = 'boldtext'
              end
            end
            object ContainerPhoneHeaderRight: TUniContainerPanel
              Left = 445
              Top = 0
              Width = 28
              Height = 30
              Hint = ''
              ParentColor = False
              Align = alRight
              TabOrder = 2
              LayoutConfig.Region = 'east'
              object BtnEditPhone: TUniThemeButton
                Left = 0
                Top = 0
                Width = 28
                Height = 28
                Hint = 'Wijzigen'
                ShowHint = True
                ParentShowHint = False
                Caption = ''
                TabStop = False
                TabOrder = 1
                Images = UniMainModule.ImageList
                ImageIndex = 11
                OnClick = BtnEditPhoneClick
                ButtonTheme = uctPrimary
              end
            end
          end
        end
        object GridCalls: TUniDBGrid
          Left = 0
          Top = 30
          Width = 473
          Height = 235
          Hint = ''
          DataSource = DsCalls
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
          WebOptions.Paged = False
          LoadMask.Enabled = False
          LoadMask.Message = 'Loading data...'
          ForceFit = True
          LayoutConfig.Cls = 'customGrid'
          LayoutConfig.Region = 'center'
          LayoutConfig.Margin = '8 0 0 0'
          Align = alClient
          TabOrder = 2
          OnDblClick = BtnEditPhoneClick
          OnFieldImageURL = GridCallsFieldImageURL
          OnDrawColumnCell = GridCallsDrawColumnCell
          Columns = <
            item
              FieldName = 'CALL_START'
              Title.Caption = 'Tijdstip'
              Width = 80
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end
            item
              Flex = 1
              FieldName = 'TITLE'
              Title.Caption = 'Titel'
              Width = 100
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end
            item
              FieldName = 'DIRECTION'
              Title.Alignment = taCenter
              Title.Caption = 'Richting'
              Width = 80
              Alignment = taCenter
              ImageOptions.Visible = True
              ImageOptions.Width = 12
              ImageOptions.Height = 12
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end
            item
              FieldName = 'LEFT_VOICEMAIL'
              Title.Alignment = taCenter
              Title.Caption = 'Voicemail'
              Width = 80
              Alignment = taCenter
              ImageOptions.Visible = True
              ImageOptions.Width = 12
              ImageOptions.Height = 12
            end
            item
              FieldName = 'PRIORITY_DESCRIPTION'
              Title.Alignment = taCenter
              Title.Caption = 'Prioriteit'
              Width = 80
              Alignment = taCenter
              ReadOnly = True
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end
            item
              FieldName = 'CALL_STATUS_DESCRIPTION'
              Title.Alignment = taCenter
              Title.Caption = 'Status'
              Width = 100
              Alignment = taCenter
              ReadOnly = True
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end
            item
              FieldName = 'CALL_RESULT_DESCRIPTION'
              Title.Alignment = taCenter
              Title.Caption = 'Resultaat'
              Width = 100
              Alignment = taCenter
              ReadOnly = True
              Menu.MenuEnabled = False
              Menu.ColumnHideable = False
            end>
        end
      end
    end
  end
  object CDS: TVirtualTable
    Active = True
    FieldDefs = <
      item
        Name = 'Maand'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Productie'
        DataType = ftCurrency
      end>
    Left = 800
    Top = 216
    Data = {
      0400020005004D61616E640100140000000000090050726F6475637469650700
      000000000000000000000000}
    object CDSMaand: TStringField
      FieldName = 'Maand'
    end
    object CDSProductie: TCurrencyField
      FieldName = 'Productie'
    end
  end
  object Tasks: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, module_id, record_id, title, task_start, task_end, ta' +
        'sk_status,'
      'Case'
      '  WHEN task_status = 0 THEN '#39'Niet gestart'#39
      '  WHEN task_status = 1 THEN '#39'Wordt uitgevoerd'#39
      '  WHEN task_status = 2 THEN '#39'Voltooid'#39
      '  WHEN task_status = 3 THEN '#39'Wachten op iemand anders'#39
      '  WHEN task_status = 4 THEN '#39'Uitgesteld'#39
      'END as task_status_description   '
      'from crm_tasks'
      'where companyId=1 and removed=0'
      '')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 288
    Top = 504
    object TasksID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object TasksMODULE_ID: TLargeintField
      FieldName = 'MODULE_ID'
      Required = True
    end
    object TasksRECORD_ID: TLargeintField
      FieldName = 'RECORD_ID'
      Required = True
    end
    object TasksTITLE: TWideStringField
      FieldName = 'TITLE'
      Size = 50
    end
    object TasksTASK_START: TDateField
      FieldName = 'TASK_START'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TasksTASK_END: TDateField
      FieldName = 'TASK_END'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TasksTASK_STATUS: TIntegerField
      FieldName = 'TASK_STATUS'
      Required = True
    end
    object TasksTASK_STATUS_DESCRIPTION: TWideStringField
      FieldName = 'TASK_STATUS_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 24
    end
  end
  object DsTasks: TDataSource
    DataSet = Tasks
    Left = 288
    Top = 560
  end
  object Calls: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, module_id, record_id, title, call_start, left_voicema' +
        'il, direction, priority_code, call_status,'
      'Case'
      '  WHEN Call_status = 0 THEN '#39'Open'#39
      '  WHEN Call_status = 1 THEN '#39'Afgewerkt'#39
      'END as call_status_description,'
      'Case'
      '  WHEN Call_result = 0 THEN '#39'Open'#39
      '  WHEN Call_result = 1 THEN '#39'Uitgevoerd'#39
      '  WHEN Call_result = 2 THEN '#39'Geannuleerd'#39
      '  WHEN Call_result = 3 THEN '#39'Ontvangen'#39
      'END as call_result_description,'
      'Case'
      '  WHEN Priority_code = 0 THEN '#39'Laag'#39
      '  WHEN Priority_code = 1 THEN '#39'Normaal'#39
      '  WHEN Priority_code = 2 THEN '#39'Hoog'#39
      'END as priority_description'
      ' '
      'from crm_calls'
      'where companyId=1 and removed=0'
      ''
      '')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 848
    Top = 504
    object CallsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object CallsMODULE_ID: TLargeintField
      FieldName = 'MODULE_ID'
      Required = True
    end
    object CallsRECORD_ID: TLargeintField
      FieldName = 'RECORD_ID'
      Required = True
    end
    object CallsTITLE: TWideStringField
      FieldName = 'TITLE'
      Size = 50
    end
    object CallsCALL_START: TDateField
      FieldName = 'CALL_START'
    end
    object CallsLEFT_VOICEMAIL: TIntegerField
      FieldName = 'LEFT_VOICEMAIL'
      Required = True
    end
    object CallsDIRECTION: TIntegerField
      FieldName = 'DIRECTION'
      Required = True
    end
    object CallsPRIORITY_CODE: TIntegerField
      FieldName = 'PRIORITY_CODE'
      Required = True
    end
    object CallsCALL_STATUS: TIntegerField
      FieldName = 'CALL_STATUS'
      Required = True
    end
    object CallsCALL_STATUS_DESCRIPTION: TWideStringField
      FieldName = 'CALL_STATUS_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 9
    end
    object CallsCALL_RESULT_DESCRIPTION: TWideStringField
      FieldName = 'CALL_RESULT_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 11
    end
    object CallsPRIORITY_DESCRIPTION: TWideStringField
      FieldName = 'PRIORITY_DESCRIPTION'
      ReadOnly = True
      FixedChar = True
      Size = 7
    end
  end
  object DsCalls: TDataSource
    DataSet = Calls
    Left = 848
    Top = 560
  end
  object LinkedLanguageDashboardContributors: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'Tasks.SQLDelete'
      'Tasks.SQLInsert'
      'Tasks.SQLLock'
      'Tasks.SQLRecCount'
      'Tasks.SQLRefresh'
      'Tasks.SQLUpdate'
      'Calls.SQLDelete'
      'Calls.SQLInsert'
      'Calls.SQLLock'
      'Calls.SQLRecCount'
      'Calls.SQLRefresh'
      'Calls.SQLUpdate'
      'HtmlFrame.HTML'
      'ChartEvolution.ChartOptions'
      'LblYear.Caption'
      'CallsCALL_RESULT_DESCRIPTION.DisplayLabel'
      'CallsPRIORITY_DESCRIPTION.DisplayLabel'
      'CallsCALL_STATUS_DESCRIPTION.DisplayLabel'
      'CallsCALL_STATUS.DisplayLabel'
      'CallsPRIORITY_CODE.DisplayLabel'
      'CallsDIRECTION.DisplayLabel'
      'CallsLEFT_VOICEMAIL.DisplayLabel'
      'CallsCALL_START.DisplayLabel'
      'CallsTITLE.DisplayLabel'
      'CallsRECORD_ID.DisplayLabel'
      'CallsMODULE_ID.DisplayLabel'
      'CallsID.DisplayLabel'
      'TasksTASK_STATUS_DESCRIPTION.DisplayLabel'
      'TasksTASK_STATUS.DisplayLabel'
      'TasksTASK_END.DisplayLabel'
      'TasksTASK_START.DisplayLabel'
      'TasksTITLE.DisplayLabel'
      'TasksRECORD_ID.DisplayLabel'
      'TasksMODULE_ID.DisplayLabel'
      'TasksID.DisplayLabel'
      'TDashboardContributorsFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerHeaderRight.Layout'
      'ContainerHeaderLeft.Layout'
      'PanelContainer.Layout'
      'ContainerTop.Layout'
      'ContainerGeneral.Layout'
      'ContainerEvolution.Layout'
      'ContainerHeaderEvolution.Layout'
      'ContainerEmptyHeaderEvolutions.Layout'
      'ContainerTitleEvolution.Layout'
      'ContainerBottom.Layout'
      'ContainerTasks.Layout'
      'ContainerHeaderTask.Layout'
      'ContainerEmptyHeaderTasks.Layout'
      'ContainerTitleTasks.Layout'
      'ContainerBodyTasks.Layout'
      'ContainerCalls.Layout'
      'ContainerHeaderCalls.Layout'
      'ContainerEmptyHeaderCalls.Layout'
      'ContainerLblPhones.Layout'
      'ContainerPhoneHeaderRight.Layout'
      'ContainerTasksHeaderRight.Layout')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageDashboardContributorsChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 672
    Top = 512
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C0065000100440061007300
      680062006F0061007200640001005400610062006C0065006100750020006400
      6500200062006F00720064000100440061007300680062006F00610072006400
      01000D000A004C0062006C00450076006F006C007500740069006F006E000100
      4F006D007A0065007400650076006F006C0075007400690065000100C9007600
      6F006C007500740069006F006E00200064007500200063006800690066006600
      7200650020006400270061006600660061006900720065007300010054007500
      72006E006F007600650072002000650076006F006C007500740069006F006E00
      01000D000A004C0062006C005400610073006B0073000100540061006B006500
      6E0001005400E200630068006500730001005400610073006B00730001000D00
      0A004C0062006C00430061006C006C0073000100540065006C00650066006F00
      6F006E00730001005400E9006C00E900700068006F006E006500730001005000
      68006F006E0065002000630061006C006C00730001000D000A00730074004800
      69006E00740073005F0055006E00690063006F00640065000D000A0042007400
      6E0045006400690074005400610073006B000100570069006A007A0069006700
      65006E0001004D006F0064006900660069006500720001004D006F0064006900
      6600790001000D000A00420074006E004500640069007400500068006F006E00
      65000100570069006A007A006900670065006E0001004D006F00640069006600
      69006500720001004D006F00640069006600790001000D000A00420074006E00
      500072006500760069006F00750073005900650061007200010056006F007200
      6900670020006A0061006100720001004C00270061006E006E00E90065002000
      6400650072006E006900E8007200650001004C00610073007400200079006500
      6100720001000D000A00420074006E004E006500780074005900650061007200
      010056006F006C00670065006E00640020006A0061006100720001004C002700
      61006E006E00E90065002000700072006F0063006800610069006E0065000100
      4E006500780074002000790065006100720001000D000A007300740044006900
      730070006C00610079004C006100620065006C0073005F0055006E0069006300
      6F00640065000D000A004300440053004D00610061006E00640001004D006100
      61006E00640001004D006F006900730001004D006F006E007400680001000D00
      0A00430044005300500072006F00640075006300740069006500010050007200
      6F006400750063007400690065000100500072006F0064007500630074006900
      6F006E000100500072006F00640075006300740069006F006E0001000D000A00
      7300740046006F006E00740073005F0055006E00690063006F00640065000D00
      0A00730074004D0075006C00740069004C0069006E00650073005F0055006E00
      690063006F00640065000D000A007300740053007400720069006E0067007300
      5F0055006E00690063006F00640065000D000A00730074007200730074007200
      4E0075006D006200650072004F00660043007200650064006900740073000100
      230020006B0072006500640069006500740065006E0001002300200063007200
      E900640069007400730001002300200063007200650064006900740073000100
      0D000A007300740072007300740072004500750072006F004300720065006400
      690074007300010026006500750072006F003B0020006B007200650064006900
      6500740065006E00010026006500750072006F003B00200063007200E9006400
      690074007300010026006500750072006F003B00200063007200650064006900
      7400730001000D000A007300740072007300740072004500750072006F004300
      6F006D006D0069007300730069006F006E00010026006500750072006F003B00
      200063006F006D006D0069007300730069006500010026006500750072006F00
      3B00200063006F006D006D0069007300730069006F006E000100260065007500
      72006F003B00200063006F006D006D0069007300730069006F006E0001000D00
      0A007300740072007300740072005F006A0061006E0075006100720079000100
      4A0061006E00750061007200690001004A0061006E0076006900650072000100
      4A0061006E00750061007200790001000D000A00730074007200730074007200
      5F00660065006200720075006100720079000100460065006200720075006100
      7200690001004600E90076007200690065007200010046006500620072007500
      61007200790001000D000A007300740072007300740072005F006D0061007200
      6300680001004D00610061007200740001004D0061007200730001004D006100
      72006300680001000D000A007300740072007300740072005F00610070007200
      69006C00010041007000720069006C00010041007600720069006C0001004100
      7000720069006C0001000D000A007300740072007300740072005F006D006100
      790001004D006500690001004D006100690001004D006100790001000D000A00
      7300740072007300740072005F006A0075006E00650001004A0075006E006900
      01004A00750069006E0001004A0075006E00650001000D000A00730074007200
      7300740072005F006A0075006C00790001004A0075006C00690001004A007500
      69006C006C006500740001004A0075006C00790001000D000A00730074007200
      7300740072005F00610075006700750073007400010041007500670075007300
      740075007300010041006F00FB00740001004100750067007500730074000100
      0D000A007300740072007300740072005F00730065007000740065006D006200
      650072000100530065007000740065006D006200650072000100530065007000
      740065006D006200720065000100530065007000740065006D00620065007200
      01000D000A007300740072007300740072005F006F00630074006F0062006500
      720001004F006B0074006F0062006500720001004F00630074006F0062007200
      650001004F00630074006F0062006500720001000D000A007300740072007300
      740072005F006E006F00760065006D0062006500720001004E006F0076006500
      6D0062006500720001004E006F00760065006D0062007200650001004E006F00
      760065006D0062006500720001000D000A007300740072007300740072005F00
      64006500630065006D00620065007200010044006500630065006D0062006500
      720001004400E900630065006D00620072006500010044006500630065006D00
      62006500720001000D000A007300740072007300740072005F006E006F007400
      5F00730074006100720074006500640001004E00690065007400200067006500
      730074006100720074000100500061007300200063006F006D006D0065006E00
      6300E90001004E006F0074002000730074006100720074006500640001000D00
      0A007300740072007300740072005F0069006E005F00700072006F0067007200
      650073007300010057006F007200640074002000750069007400670065007600
      6F00650072006400010045006E00200063006F00750072007300010049007300
      2000630061007200720069006500640020006F007500740001000D000A007300
      740072007300740072005F0063006F006D0070006C0065007400650064000100
      56006F006C0074006F006F006900640001005400650072006D0069006E00E900
      010043006F006D0070006C00650074006500640001000D000A00730074007200
      7300740072005F00770061006900740069006E0067005F0066006F0072000100
      5700610063006800740065006E0020006F0070002000690065006D0061006E00
      6400200061006E006400650072007300010045006E0020006100740074006500
      6E00740065000100570061006900740069006E006700200066006F0072002000
      73006F006D0065006F006E006500200065006C007300650001000D000A007300
      740072007300740072005F00640065006C006100790065006400010055006900
      7400670065007300740065006C00640001005200650070006F0072007400E900
      010050006F007300740070006F006E006500640001000D000A00730074007200
      7300740072005F00630061006C006C005F006F00700065006E0001004F007000
      65006E0001004F007500760065007200740001004F00700065006E0001000D00
      0A007300740072007300740072005F00630061006C006C005F0063006F006D00
      70006C006500740065006400010041006600670065007700650072006B007400
      0100460069006E0069000100460069006E006900730068006500640001000D00
      0A007300740072007300740072005F00630061006C006C005F00720065007300
      75006C0074005F006F00700065006E0001004F00700065006E0001004F007500
      760065007200740001004F00700065006E0001000D000A007300740072007300
      740072005F00630061006C006C005F0072006500730075006C0074005F006500
      780065006300750074006500640001005500690074006700650076006F006500
      7200640001004500660066006500630074007500E90001004500780065006300
      750074006500640001000D000A007300740072007300740072005F0063006100
      6C006C005F0072006500730075006C0074005F00630061006E00630065006C00
      6C006500640001004700650061006E006E0075006C0065006500720064000100
      41006E006E0075006C00E9000100430061006E00630065006C006C0065006400
      01000D000A007300740072007300740072005F00630061006C006C005F007200
      6500730075006C0074005F007200650063006500690076006500640001004F00
      6E007400760061006E00670065006E00010052006500E7007500010052006500
      63006500690076006500640001000D000A007300740072007300740072005F00
      630061006C006C005F007000720069006F0072006900740079005F006C006F00
      770001004C00610061006700010042006100730001004C006F00770001000D00
      0A007300740072007300740072005F00630061006C006C005F00700072006900
      6F0072006900740079005F006E006F0072006D0061006C0001004E006F007200
      6D00610061006C0001004E006F0072006D0061006C0001004E006F0072006D00
      61006C0001000D000A007300740072007300740072005F00630061006C006C00
      5F007000720069006F0072006900740079005F00680069006700680001004800
      6F006F006700010048006100750074000100480069006700680001000D000A00
      730074004F00740068006500720053007400720069006E00670073005F005500
      6E00690063006F00640065000D000A007300740043006F006C006C0065006300
      740069006F006E0073005F0055006E00690063006F00640065000D000A004700
      7200690064005400610073006B0073002E0043006F006C0075006D006E007300
      5B0030005D002E0043006800650063006B0042006F0078004600690065006C00
      64002E004600690065006C006400560061006C00750065007300010074007200
      750065003B00660061006C0073006500010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001000D00
      0A0047007200690064005400610073006B0073002E0043006F006C0075006D00
      6E0073005B0030005D002E005400690074006C0065002E004300610070007400
      69006F006E0001005300740061007200740064006100740075006D0001004400
      61007400650020006400650020006400E9006200750074000100530074006100
      720074002000640061007400650001000D000A00470072006900640054006100
      73006B0073002E0043006F006C0075006D006E0073005B0031005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      5400610073006B0073002E0043006F006C0075006D006E0073005B0031005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001004500
      69006E00640064006100740075006D0001004400610074006500200064006500
      2000660069006E00010045006E0064002000640061007400650001000D000A00
      47007200690064005400610073006B0073002E0043006F006C0075006D006E00
      73005B0032005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A0047007200690064005400610073006B0073002E0043006F006C007500
      6D006E0073005B0032005D002E005400690074006C0065002E00430061007000
      740069006F006E00010054006900740065006C00010054006900740072006500
      01005400690074006C00650001000D000A004700720069006400540061007300
      6B0073002E0043006F006C0075006D006E0073005B0033005D002E0043006800
      650063006B0042006F0078004600690065006C0064002E004600690065006C00
      6400560061006C00750065007300010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C0073006500010074007200
      750065003B00660061006C007300650001000D000A0047007200690064005400
      610073006B0073002E0043006F006C0075006D006E0073005B0033005D002E00
      5400690074006C0065002E00430061007000740069006F006E00010053007400
      6100740075007300010053007400610074007500740001005300740061007400
      7500730001000D000A004700720069006400430061006C006C0073002E004300
      6F006C0075006D006E0073005B0030005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A004700720069006400430061006C006C007300
      2E0043006F006C0075006D006E0073005B0030005D002E005400690074006C00
      65002E00430061007000740069006F006E000100540069006A00640073007400
      690070000100540065006D00700073000100540069006D00650001000D000A00
      4700720069006400430061006C006C0073002E0043006F006C0075006D006E00
      73005B0031005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A004700720069006400430061006C006C0073002E0043006F006C007500
      6D006E0073005B0031005D002E005400690074006C0065002E00430061007000
      740069006F006E00010054006900740065006C00010054006900740072006500
      01005400690074006C00650001000D000A004700720069006400430061006C00
      6C0073002E0043006F006C0075006D006E0073005B0032005D002E0043006800
      650063006B0042006F0078004600690065006C0064002E004600690065006C00
      6400560061006C00750065007300010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C0073006500010074007200
      750065003B00660061006C007300650001000D000A0047007200690064004300
      61006C006C0073002E0043006F006C0075006D006E0073005B0032005D002E00
      5400690074006C0065002E00430061007000740069006F006E00010052006900
      63006800740069006E006700010044006900720065006300740069006F006E00
      010044006900720065006300740069006F006E0001000D000A00470072006900
      6400430061006C006C0073002E0043006F006C0075006D006E0073005B003300
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      720069006400430061006C006C0073002E0043006F006C0075006D006E007300
      5B0033005D002E005400690074006C0065002E00430061007000740069006F00
      6E00010056006F006900630065006D00610069006C0001004D00650073007300
      610067006500720069006500010056006F006900630065006D00610069006C00
      01000D000A004700720069006400430061006C006C0073002E0043006F006C00
      75006D006E0073005B0034005D002E0043006800650063006B0042006F007800
      4600690065006C0064002E004600690065006C006400560061006C0075006500
      7300010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      7300650001000D000A004700720069006400430061006C006C0073002E004300
      6F006C0075006D006E0073005B0034005D002E005400690074006C0065002E00
      430061007000740069006F006E0001005000720069006F007200690074006500
      6900740001005000720069006F00720069007400E90001005000720069006F00
      720069007400790001000D000A004700720069006400430061006C006C007300
      2E0043006F006C0075006D006E0073005B0035005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A004700720069006400430061006C00
      6C0073002E0043006F006C0075006D006E0073005B0035005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001005300740061007400
      7500730001005300740061007400750074000100530074006100740075007300
      01000D000A004700720069006400430061006C006C0073002E0043006F006C00
      75006D006E0073005B0036005D002E0043006800650063006B0042006F007800
      4600690065006C0064002E004600690065006C006400560061006C0075006500
      7300010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      7300650001000D000A004700720069006400430061006C006C0073002E004300
      6F006C0075006D006E0073005B0036005D002E005400690074006C0065002E00
      430061007000740069006F006E00010052006500730075006C00740061006100
      740001005200E900730075006C00740061007400010052006500730075006C00
      740001000D000A0073007400430068006100720053006500740073005F005500
      6E00690063006F00640065000D000A00}
  end
end
