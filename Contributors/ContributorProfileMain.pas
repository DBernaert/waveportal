unit ContributorProfileMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniEdit, uniGUIBaseClasses, uniDBEdit, uniButton,
  UniThemeButton, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

var
	str_name_required:     string = 'De naam is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_password_required: string = 'Het wachtwoord is een verplichte ingave.'; // TSI: Localized (Don't modify!)
	str_error_saving_data: string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	str_record_locked:     string = 'Gegevens in gebruik door een andere gebruiker.'; // TSI: Localized (Don't modify!)

type
  TContributorProfileMainFrm = class(TUniForm)
    EditName: TUniDBEdit;
    EditFirstName: TUniDBEdit;
    EditPassWordUser: TUniEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    Contributors_Edit: TIBCQuery;
    DsContributors_Edit: TDataSource;
    LinkedLanguageContributorProfileMain: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure LinkedLanguageContributorProfileMainChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_contributor_profile_module;
  end;

function ContributorProfileMainFrm: TContributorProfileMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Utils, Main;

function ContributorProfileMainFrm: TContributorProfileMainFrm;
begin
  Result := TContributorProfileMainFrm(UniMainModule.GetFormInstance(TContributorProfileMainFrm));
end;

{ TContributorProfileMainFrm }

procedure TContributorProfileMainFrm.BtnCancelClick(Sender: TObject);
begin
  Contributors_Edit.Cancel;
  Contributors_Edit.Close;
  Close;
end;

procedure TContributorProfileMainFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(Contributors_Edit.FieldByName('Last_name').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_name_required);
         EditName.SetFocus;
         Exit;
       end;

  if Trim(EditPassWordUser.Text) = ''
  then begin
         MainForm.WaveShowWarningToast(str_password_required);
         EditPassWordUser.SetFocus;
         Exit;
       end;

  UniMainModule.User_name           := Trim(Contributors_Edit.FieldByName('Last_name').AsString + ' ' +
                                            Contributors_Edit.FieldByName('First_name').AsString);

  Contributors_Edit.FieldByName('Portal_password').AsString := Utils.Crypt(EditPassWordUser.Text, 35462);

  try
    Contributors_Edit.Post;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
    UniMainModule.Result_dbAction := Contributors_Edit.FieldByName('Id').AsInteger;
  except
    on E: Exception
    do begin
         if UniMainModule.UpdTrans.Active
         then UniMainModule.UpdTrans.Rollback;
         MainForm.WaveShowErrorToast(str_error_saving_data + E.Message);
       end;
  end;
  Contributors_Edit.Close;
  Close;
end;

procedure TContributorProfileMainFrm.LinkedLanguageContributorProfileMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContributorProfileMainFrm.Start_contributor_profile_module;
var SQL: string;
begin
  Contributors_Edit.Close;
  Contributors_Edit.SQL.Clear;
  SQL := 'Select id, last_name, first_name, portal_password from credit_contributors where id=' + IntToStr(UniMainModule.User_id);
  Contributors_Edit.SQL.Add(SQL);
  Contributors_Edit.Open;
  Try
    Contributors_Edit.Edit;
    EditPassWordUser.Text           := Utils.Decrypt(Contributors_Edit.FieldByName('Portal_password').AsString, 35462);
  Except
    Contributors_Edit.Cancel;
    Contributors_Edit.Close;
    MainForm.WaveShowErrorToast(str_record_locked);
    Close;
  End;
end;

procedure TContributorProfileMainFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TContributorProfileMainFrm.UpdateStrings;
begin
  str_record_locked     := LinkedLanguageContributorProfileMain.GetTextOrDefault('strstr_record_locked' (* 'Gegevens in gebruik door een andere gebruiker.' *) );
  str_error_saving_data := LinkedLanguageContributorProfileMain.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens: ' *) );
  str_password_required := LinkedLanguageContributorProfileMain.GetTextOrDefault('strstr_password_required' (* 'Het wachtwoord is een verplichte ingave.' *) );
  str_name_required     := LinkedLanguageContributorProfileMain.GetTextOrDefault('strstr_name_required' (* 'De naam is een verplichte ingave.' *) );
end;

end.

