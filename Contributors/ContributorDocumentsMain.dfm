object ContributorDocumentsMainFrm: TContributorDocumentsMainFrm
  Left = 0
  Top = 0
  Width = 1120
  Height = 721
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1120
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 105
      Height = 30
      Hint = ''
      Caption = 'Borderellen'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerBodyDocuments: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1120
    Height = 688
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 1
    Layout = 'hbox'
    LayoutConfig.Region = 'center'
    object ContainerDocuments1: TUniContainerPanel
      Left = 16
      Top = 8
      Width = 1073
      Height = 641
      Hint = ''
      ParentColor = False
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Flex = 1
      LayoutConfig.Height = '100%'
      object ContainerFilterDocuments1: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 1073
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Region = 'north'
        object ContainerFilterRight: TUniContainerPanel
          Left = 1045
          Top = 0
          Width = 28
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alRight
          TabOrder = 1
          LayoutConfig.Region = 'east'
          object BtnPrintDocuments1: TUniThemeButton
            Left = 0
            Top = 0
            Width = 28
            Height = 28
            Hint = 'Afdrukken'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            TabStop = False
            TabOrder = 1
            Images = UniMainModule.ImageList
            ImageIndex = 51
            OnClick = BtnPrintDocuments1Click
            ButtonTheme = uctPrimary
          end
        end
      end
      object GridDocuments1: TUniDBGrid
        Left = 0
        Top = 30
        Width = 1073
        Height = 611
        Hint = ''
        DataSource = DsDocumentsBrowse
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
        WebOptions.Paged = False
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        LayoutConfig.Cls = 'customGrid'
        LayoutConfig.Region = 'center'
        LayoutConfig.Margin = '8 0 0 0'
        Align = alClient
        TabOrder = 2
        OnDblClick = BtnPrintDocuments1Click
        Columns = <
          item
            FieldName = 'BATCH_YEAR'
            Title.Alignment = taRightJustify
            Title.Caption = 'Jaar'
            Width = 64
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'BATCH_MONTH'
            Title.Alignment = taRightJustify
            Title.Caption = 'Periode'
            Width = 64
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'DOCUMENT_DATE'
            Title.Caption = 'Document datum'
            Width = 85
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'DOCUMENT_TOTAL_AMOUNT'
            Title.Alignment = taRightJustify
            Title.Caption = 'Totaal bedrag'
            Width = 71
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
    end
  end
  object DocumentsBrowse: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  CREDIT_DOCUMENT.ID,'
      '  CREDIT_DOCUMENT.CREDIT_DOCUMENT_BATCH,'
      '  CREDIT_DOCUMENT.DOCUMENT_DATE,'
      '  CREDIT_DOCUMENT.DOCUMENT_TOTAL_AMOUNT,'
      '  CREDIT_DOCUMENT_BATCH.BATCH_MONTH,'
      '  CREDIT_DOCUMENT_BATCH.BATCH_YEAR,'
      '  CREDIT_DOCUMENT_BATCH.BATCH_QUARTER'
      'FROM'
      '  CREDIT_DOCUMENT'
      
        '  LEFT OUTER JOIN CREDIT_DOCUMENT_BATCH ON (CREDIT_DOCUMENT.CRED' +
        'IT_DOCUMENT_BATCH = CREDIT_DOCUMENT_BATCH.ID)'
      
        'ORDER BY CREDIT_DOCUMENT_BATCH.BATCH_YEAR desc, CREDIT_DOCUMENT_' +
        'BATCH.BATCH_MONTH desc, CREDIT_DOCUMENT.DOCUMENT_DATE desc')
    MasterFields = 'ID'
    Left = 423
    Top = 224
    object DocumentsBrowseID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object DocumentsBrowseCREDIT_DOCUMENT_BATCH: TLargeintField
      FieldName = 'CREDIT_DOCUMENT_BATCH'
      Required = True
    end
    object DocumentsBrowseDOCUMENT_DATE: TDateField
      FieldName = 'DOCUMENT_DATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object DocumentsBrowseDOCUMENT_TOTAL_AMOUNT: TFloatField
      FieldName = 'DOCUMENT_TOTAL_AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object DocumentsBrowseBATCH_MONTH: TIntegerField
      FieldName = 'BATCH_MONTH'
      ReadOnly = True
    end
    object DocumentsBrowseBATCH_YEAR: TIntegerField
      FieldName = 'BATCH_YEAR'
      ReadOnly = True
    end
    object DocumentsBrowseBATCH_QUARTER: TIntegerField
      FieldName = 'BATCH_QUARTER'
      ReadOnly = True
    end
  end
  object DsDocumentsBrowse: TDataSource
    DataSet = DocumentsBrowse
    Left = 423
    Top = 280
  end
  object LinkedLanguageContributorDocumentsMain: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TContributorDocumentsMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerBodyDocuments.Layout'
      'ContainerDocuments1.Layout'
      'ContainerFilterDocuments1.Layout'
      'ContainerFilterRight.Layout'
      'DocumentsBrowse.SQLDelete'
      'DocumentsBrowse.SQLInsert'
      'DocumentsBrowse.SQLLock'
      'DocumentsBrowse.SQLRecCount'
      'DocumentsBrowse.SQLRefresh'
      'DocumentsBrowse.SQLUpdate'
      'DocumentsBrowseID.DisplayLabel'
      'DocumentsBrowseCREDIT_DOCUMENT_BATCH.DisplayLabel'
      'DocumentsBrowseDOCUMENT_DATE.DisplayLabel'
      'DocumentsBrowseDOCUMENT_TOTAL_AMOUNT.DisplayLabel'
      'DocumentsBrowseBATCH_MONTH.DisplayLabel'
      'DocumentsBrowseBATCH_YEAR.DisplayLabel')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 424
    Top = 344
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C006500010042006F007200
      64006500720065006C006C0065006E00010044006F00630075006D0065006E00
      74007300010044006F00630075006D0065006E007400730001000D000A007300
      7400480069006E00740073005F0055006E00690063006F00640065000D000A00
      420074006E005000720069006E00740044006F00630075006D0065006E007400
      730031000100410066006400720075006B006B0065006E00010049006D007000
      720069006D006500720001005000720069006E00740001000D000A0073007400
      44006900730070006C00610079004C006100620065006C0073005F0055006E00
      690063006F00640065000D000A007300740046006F006E00740073005F005500
      6E00690063006F00640065000D000A00730074004D0075006C00740069004C00
      69006E00650073005F0055006E00690063006F00640065000D000A0073007400
      53007400720069006E00670073005F0055006E00690063006F00640065000D00
      0A00730074004F00740068006500720053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A007300740043006F006C006C006500
      6300740069006F006E0073005F0055006E00690063006F00640065000D000A00
      470072006900640044006F00630075006D0065006E007400730031002E004300
      6F006C0075006D006E0073005B0030005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A00470072006900640044006F00630075006D00
      65006E007400730031002E0043006F006C0075006D006E0073005B0030005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001004A00
      610061007200010041006E006E00E90065000100590065006100720001000D00
      0A00470072006900640044006F00630075006D0065006E007400730031002E00
      43006F006C0075006D006E0073005B0031005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A00470072006900640044006F0063007500
      6D0065006E007400730031002E0043006F006C0075006D006E0073005B003100
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      50006500720069006F006400650001005000E900720069006F00640065000100
      50006500720069006F00640001000D000A00470072006900640044006F006300
      75006D0065006E007400730031002E0043006F006C0075006D006E0073005B00
      32005D002E0043006800650063006B0042006F0078004600690065006C006400
      2E004600690065006C006400560061006C007500650073000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C007300650001000D000A00
      470072006900640044006F00630075006D0065006E007400730031002E004300
      6F006C0075006D006E0073005B0032005D002E005400690074006C0065002E00
      430061007000740069006F006E00010044006F00630075006D0065006E007400
      200064006100740075006D000100440061007400650020006400750020006400
      6F00630075006D0065006E007400010044006F00630075006D0065006E007400
      2000640061007400650001000D000A00470072006900640044006F0063007500
      6D0065006E007400730031002E0043006F006C0075006D006E0073005B003300
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      72006900640044006F00630075006D0065006E007400730031002E0043006F00
      6C0075006D006E0073005B0033005D002E005400690074006C0065002E004300
      61007000740069006F006E00010054006F007400610061006C00200062006500
      640072006100670001004D006F006E00740061006E007400200074006F007400
      61006C00010054006F00740061006C00200061006D006F0075006E0074000100
      0D000A0073007400430068006100720053006500740073005F0055006E006900
      63006F00640065000D000A00}
  end
end
