unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniGUIBaseClasses, uniPanel, uniGUIFrame, Db, UniFSConfirm,
  Vcl.Imaging.pngimage, uniImage, uniDBGrid, uUniSyncFusionLocale, uniSweetAlert,
  uniHTMLFrame, uUniSyncFusionToast, siComp, siLngLnk, uniMemo, uniDBMemo;

var
	str_no:                 string = 'Nee'; // TSI: Localized (Don't modify!)
	str_yes:                string = 'Ja'; // TSI: Localized (Don't modify!)
	str_warning:            string = 'Waarschuwing'; // TSI: Localized (Don't modify!)
	str_error:              string = 'Fout'; // TSI: Localized (Don't modify!)
	str_ok:                 string = 'Ok'; // TSI: Localized (Don't modify!)
	str_cancel:             string = 'Annuleren'; // TSI: Localized (Don't modify!)
	str_confirmation:       string = 'Bevestiging'; // TSI: Localized (Don't modify!)
	str_information:        string = 'Informatie'; // TSI: Localized (Don't modify!)

type
  TMainForm = class(TUniForm)
    Container: TUniContainerPanel;
    Confirm: TUniFSConfirm;
    SyncLocale: TUniSyncFusionLocale;
    SweetAlert: TUniSweetAlert;
    SyncToast: TUniSyncFusionToast;
    LinkedLanguageMain: TsiLangLinked;
    procedure UniFormCreate(Sender: TObject);
    procedure LinkedLanguageMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFormShow(Sender: TObject);
  private
    { Private declarations }
    TargetMemoPopup: TComponent;   //Variabele om het component bij te houden dat een nieuw commentaar toevoegd via insert_commentlines
    ActiveModule: TUniFrame;
    procedure CallBackCommentsPopup(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Show_portal_not_found;
    procedure Show_portal_not_active;
    procedure Show_login;
    procedure Show_contacts_form;
    procedure Show_contributors_form;

    procedure WaveShowWarningMessage(MessageText: string);
    procedure WaveShowErrorMessage(MessageText: string);
    procedure WaveShowInfoMessage(MessageText: string);

    procedure WaveShowInformationToast(MessageText: string);
    procedure WaveShowWarningToast(MessageText: string);
    procedure WaveShowErrorToast(MessageText: string);
    procedure WaveShowConfirmationToast(MessageText: string);

    function WaveConfirmBlocking(Title: string; MessageText: string): boolean;
    function WaveInputText(Title: string; MessageText: string; Value: string): boolean;
    function WaveInputNumber(Title, MessageText, Value: string): boolean;

    procedure ExportGrid(GridToExport: TUniDbGrid; GridTitle: string; Ds: TDataSet; KeyField: string);

    procedure Insert_commentlines(Sender: TObject);
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication, PortalNotFound, ServerModule, PortalNotActive, Login,
  ContributorsMain, xlsGrid, CustomersMain;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

procedure TMainForm.CallBackCommentsPopup(Sender: TComponent; AResult: Integer);
var InsertText: string;
begin
  InsertText := '-- ' + Trim(Unimainmodule.User_name) + ' - ' + FormatDateTime('dd/mm/yyyy hh:mm', Now) + ' --';
  if Trim(UniMainModule.Result_dbAction_string) <> ''
  then InsertText := InsertText + chr(13) + UniMainModule.Result_dbAction_string + chr(13) + '--' + chr(13)
  else InsertText := InsertText + chr(13) + chr(13) + '--' + chr(13);

  if (Sender is TUniDbMemo)
  then (Sender as TUniDbMemo).Text := InsertText + (Sender as TUniDbMemo).Text
  else if (Sender is TUniMemo)
       then (Sender as TUniMemo).Text := InsertText + (Sender as TUniMemo).Text
end;

procedure TMainForm.ExportGrid(GridToExport: TUniDbGrid; GridTitle: string;
  Ds: TDataSet; KeyField: string);
var ExportFileName: string;
    AUrl:           string;
    CurrentId:      Variant;
begin
  if Trim(KeyField) <> ''
  then CurrentId := Ds.FieldByName(KeyField).AsVariant;
  ExportFileName := UniServerModule.NewCacheFileUrl(False,'xls', FormatDateTime('yyyymmddhhmmss', Now), '', AUrl, True);
  TxlsGrid.ExportXls(GridToExport, GridTitle, '', ExportFileName, False, True);
  UniSession.SendFile(ExportFileName);
  if Trim(KeyField) <> ''
  then Ds.Locate(KeyField, CurrentId, [])
  else Ds.First;
end;

procedure TMainForm.Insert_commentlines(Sender: TObject);
begin
  UniMainModule.Result_dbAction_string := '';
  TargetMemoPopup                      := Sender as TComponent;
  CallBackCommentsPopup(Sender as TComponent, 0);
end;

procedure TMainForm.LinkedLanguageMainChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMainForm.Show_contacts_form;
begin
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SyncLocale.SyncLang := slNL;
    2: SyncLocale.SyncLang := slFR;
    3: SyncLocale.SyncLang := slEN;
  end;

  if (ActiveModule <> nil)
  then FreeAndNil(ActiveModule);

  ActiveModule := TCustomersMainFrm.Create(Self);
  With (ActiveModule as TCustomersMainFrm)
  do begin
       Parent := Container;
       start_customers_module;
     end;
end;

procedure TMainForm.Show_contributors_form;
begin
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SyncLocale.SyncLang := slNL;
    2: SyncLocale.SyncLang := slFR;
    3: SyncLocale.SyncLang := slEN;
  end;

  if (ActiveModule <> nil)
  then FreeAndNil(ActiveModule);

  ActiveModule := TContributorsMainFrm.Create(Self);
  With (ActiveModule as TContributorsMainFrm)
  do begin
       Parent := Container;
       Start_contributor_module;
     end;
end;

procedure TMainForm.Show_login;
begin
  if (ActiveModule <> nil)
  then FreeAndNil(ActiveModule);

  ActiveModule := TLoginFrm.Create(Self);
  With (ActiveModule as TLoginFrm)
  do begin
       Parent := Container;
       Start_login_procedure;
     end;
end;

procedure TMainForm.Show_portal_not_active;
begin
  if (ActiveModule <> nil)
  then FreeAndNil(ActiveModule);

  ActiveModule := TPortalNotActiveFrm.Create(Self);
  With (ActiveModule as TPortalNotActiveFrm)
  do Parent := Container;
end;

procedure TMainForm.Show_portal_not_found;
begin
  if (ActiveModule <> nil)
  then FreeAndNil(ActiveModule);

  ActiveModule := TPortalNotFoundFrm.Create(Self);
  With (ActiveModule as TPortalNotFoundFrm)
  do Parent := Container;
end;

procedure TMainForm.UniFormCreate(Sender: TObject);
var Portal: string;
begin
  Portal   := UniApplication.Parameters.Values['Portal'];

  //Check the portal name
  if Trim(Portal) = ''
  then begin
         Show_portal_not_found;
         Exit;
       end;

  //Connect to the master database
  Try
    UniMainModule.DbAdmin.Connected := False;
    UniMainModule.DbAdmin.Connected := true;
  Except on E: EDataBaseError
  do begin
       UniMainModule.DbAdmin.Connected := False;
       WaveShowErrorMessage('No connection to the admin database!');
       ModalResult := mrCancel;
       UniSession.Terminate('<script>window.location.href = "https://www.wavedesk.be";</script>');
     end;
  end;

  //Search for the account
  UniMainModule.QWorkAdmin.Close;
  UniMainModule.QWorkAdmin.SQL.Clear;
  UniMainModule.QWorkAdmin.SQL.Add('Select * from accounts where account_name=' + QuotedStr(Portal));
  UniMainModule.QWorkAdmin.Open;

  if UniMainModule.QWorkAdmin.RecordCount = 0
  then begin
         UniMainModule.QWorkAdmin.Close;
         UniMainModule.DbAdmin.Connected := False;
         Show_portal_not_found;
         Exit;
       end;

  //Read the account information
  UniMainModule.Company_account_name             := UniMainModule.QWorkAdmin.FieldByName('Account_Name').AsString;
  UniMainModule.Company_account_token            := UniMainModule.QWorkAdmin.FieldByName('Account_Token').AsString;
  UniMainModule.Company_account_connectionstring := UniMainModule.QWorkAdmin.FieldByName('Account_database').AsString +
                                                    ';User ID=SYSDBA;Password=Zandstraat11';
  UniMainModule.QWorkAdmin.Close;

  //Read the active modules, number of users, .... ********************
  //************************ to be done *******************************

  UniMainModule.DbModule.Connected               := False;
  UniMainModule.DbModule.ConnectString           := UniMainModule.Company_account_connectionstring;

  //Connect to the database
   Try
    UniMainModule.DbModule.Connected := False;
    UniMainModule.DbModule.Connected := true;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message);
       WaveShowErrorToast('No connection to the database!');
       UniSession.Terminate();
       Exit;
     end;
  end;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select id, name, portal_name, portal_title_dutch, portal_title_french, portal_title_english, ' +
                              'portal_return_url, portal_active, portal_active_contributors, portal_active_contacts, portal_logo, ' +
                              'portal_contr_show_news, portal_2_factor, ' +
                              'credit_pro_module_active, insurance_module_active, Credit_quotity_required, ' +
                              'portal_contr_show_agenda, portal_contr_show_tasks, portal_contr_show_phone_calls, portal_contr_show_doc_archive ' +
                              'from companyprofile where portal_address=' + QuotedStr(Portal));
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.RecordCount = 0
  then begin
         UniMainModule.QWork.Close;
         Show_portal_not_found;
         exit;
       end;

  //Check if the portal is active
  if UniMainModule.QWork.FieldByName('Portal_active').asInteger = 0
  then begin
         UniMainModule.QWork.Close;
         Show_portal_not_active;
         Exit;
       end;

  //Load parameters of the portal
  UniMainModule.Company_id                      := UniMainModule.QWork.FieldByName('Id').AsInteger;
  UniMainModule.Company_name                    := UniMainModule.QWork.FieldByName('Name').AsString;
  UniMainModule.Portal_name                     := UniMainModule.QWork.FieldByName('Portal_name').AsString;
  UniMainModule.Portal_title_dutch              := UniMainModule.QWork.FieldByName('Portal_title_dutch').AsString;
  UniMainModule.Portal_title_french             := UniMainModule.QWork.FieldByName('Portal_title_french').AsString;
  UniMainModule.Portal_title_english            := UniMainModule.QWork.FieldByName('Portal_title_english').AsString;
  UniMainModule.Portal_return_url               := UniMainModule.QWork.FieldByName('Portal_return_url').AsString;

  if UniMainModule.QWork.FieldByName('Portal_2_factor').asInteger = 1
  then UniMainModule.Portal_2_factor            := True
  else UniMainModule.Portal_2_factor            := False;

  if UniMainModule.QWork.FieldByName('Portal_active_contributors').AsInteger = 1
  then UniMainModule.Portal_active_contributors := True
  else UniMainModule.Portal_active_contributors := False;

  if UniMainModule.QWork.FieldByName('Portal_active_contacts').AsInteger = 1
  then UniMainModule.Portal_active_contacts := True
  else UniMainModule.Portal_active_contacts := False;

  if UniMainModule.QWork.FieldByName('Portal_contr_show_news').AsInteger = 1
  then UniMainModule.Portal_contr_show_news := True
  else UniMainModule.Portal_contr_show_news := False;

  if UniMainModule.QWork.FieldByName('Credit_pro_module_active').AsInteger = 1
  then UniMainModule.portal_contr_credit_module_active := True
  else UniMainModule.portal_contr_credit_module_active := False;

  if UniMainModule.QWork.FieldByName('Insurance_module_active').AsInteger = 1
  then UniMainModule.portal_contr_insurance_module_active := True
  else UnIMainModule.portal_contr_insurance_module_active := False;

  if UniMainModule.QWork.FieldByName('Portal_contr_show_agenda').AsInteger = 1
  then UniMainModule.portal_contr_show_agenda := True
  else UnIMainModule.portal_contr_show_agenda := False;

  if UniMainModule.QWork.FieldByName('Portal_contr_show_tasks').AsInteger = 1
  then UniMainModule.portal_contr_show_tasks := True
  else UnIMainModule.portal_contr_show_tasks := False;

  if UniMainModule.QWork.FieldByName('Portal_contr_show_phone_calls').AsInteger = 1
  then UniMainModule.portal_contr_show_phone_calls := True
  else UnIMainModule.portal_contr_show_phone_calls := False;

  if UniMainModule.QWork.FieldByName('Portal_contr_show_doc_archive').AsInteger = 1
  then UniMainModule.portal_contr_show_doc_archive := True
  else UnIMainModule.portal_contr_show_doc_archive := False;

  //Fixed params (not changable by the user)
  if UniMainModule.QWork.FieldByName('Credit_quotity_required').AsInteger = 1
  then UniMainModule.Credit_quotity_required := True
  else UniMainModule.Credit_quotity_required := False;

  if not UniMainModule.QWork.Fieldbyname('Portal_logo').isNull
  then try
         UniMainModule.Portal_logo.Assign(UniMainModule.QWork.FieldByName('Portal_logo'));
       except
       end;

  UniMainModule.QWork.Close;
  UniServerModule.Title := UniMainModule.Portal_name;
  Show_login;
end;

procedure TMainForm.UniFormShow(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TMainForm.UpdateStrings;
begin
  str_information      := LinkedLanguageMain.GetTextOrDefault('strstr_information' (* 'Informatie' *) );
  str_confirmation     := LinkedLanguageMain.GetTextOrDefault('strstr_confirmation' (* 'Bevestiging' *) );
  str_cancel           := LinkedLanguageMain.GetTextOrDefault('strstr_cancel' (* 'Annuleren' *) );
  str_ok               := LinkedLanguageMain.GetTextOrDefault('strstr_ok' (* 'Ok' *) );
  str_error            := LinkedLanguageMain.GetTextOrDefault('strstr_error' (* 'Fout' *) );
  str_warning          := LinkedLanguageMain.GetTextOrDefault('strstr_warning' (* 'Waarschuwing' *) );
  str_yes              := LinkedLanguageMain.GetTextOrDefault('strstr_yes' (* 'Ja' *) );
  str_no               := LinkedLanguageMain.GetTextOrDefault('strstr_no' (* 'Nee' *) );
end;

function TMainForm.WaveConfirmBlocking(Title, MessageText: string): boolean;
begin
  SweetAlert.AlertType         := atQuestion;
  SweetAlert.Title             := Title;
  SweetAlert.Html              := MessageText;
  SweetAlert.TitleText         := '';
  SweetAlert.AllowEscapeKey    := False;
  SweetAlert.AllowOutsideClick := False;
  SweetAlert.CancelButtonText  := str_no;
  SweetAlert.ConfirmButtonText := str_yes;
  SweetAlert.ShowCancelButton  := True;
  SweetAlert.ShowCloseButton   := False;
  SweetAlert.FocusCancel       := True;
  SweetAlert.InputType         := ItNone;
  if SweetAlert.Execute
  then Result := True
  else Result := False;
end;

function TMainForm.WaveInputNumber(Title, MessageText, Value: string): boolean;
begin
  SweetAlert.AlertType         := atInfo;
  SweetAlert.Title             := Title;
  SweetAlert.Html              := MessageText;
  SweetAlert.TitleText         := '';
  SweetAlert.AllowEscapeKey    := False;
  SweetAlert.AllowOutsideClick := False;
  SweetAlert.CancelButtonText  := str_cancel;
  SweetAlert.ConfirmButtonText := str_ok;
  SweetAlert.ShowCancelButton  := True;
  SweetAlert.ShowCloseButton   := False;
  SweetAlert.FocusCancel       := False;
  SweetAlert.InputType         := ItNumber;
  SweetAlert.InputValue        := Value;
  if SweetAlert.Execute
  then Result := True
  else Result := False;
end;

function TMainForm.WaveInputText(Title, MessageText, Value: string): boolean;
begin
  SweetAlert.AlertType         := atInfo;
  SweetAlert.Title             := Title;
  SweetAlert.Html              := MessageText;
  SweetAlert.TitleText         := '';
  SweetAlert.AllowEscapeKey    := False;
  SweetAlert.AllowOutsideClick := False;
  SweetAlert.CancelButtonText  := str_cancel;
  SweetAlert.ConfirmButtonText := str_ok;
  SweetAlert.ShowCancelButton  := True;
  SweetAlert.ShowCloseButton   := False;
  SweetAlert.FocusCancel       := False;
  SweetAlert.InputType         := ItText;
  SweetAlert.InputValue        := Value;
  if SweetAlert.Execute
  then Result := True
  else Result := False;
end;

procedure TMainForm.WaveShowConfirmationToast(MessageText: string);
begin
  SyncToast.ToastPositions := tpTopRight;
  SyncToast.Show(str_confirmation, MessageText, tcSuccess);
end;

procedure TMainForm.WaveShowErrorMessage(MessageText: string);
begin
  SweetAlert.AlertType         := atError;
  SweetAlert.Title             := str_error;
  SweetAlert.Html              := MessageText;
  SweetAlert.TitleText         := '';
  SweetAlert.AllowEscapeKey    := False;
  SweetAlert.AllowOutsideClick := False;
  SweetAlert.ConfirmButtonText := 'Ok';
  SweetAlert.ShowCancelButton  := False;
  SweetAlert.ShowCloseButton   := False;
  SweetAlert.InputType         := ItNone;
  SweetAlert.Show();
end;

procedure TMainForm.WaveShowErrorToast(MessageText: string);
begin
  SyncToast.ToastPositions := tpTopFullWidth;
  SyncToast.Show(str_error, MessageText, tcDanger);
end;

procedure TMainForm.WaveShowInfoMessage(MessageText: string);
begin
  SweetAlert.AlertType         := atSuccess;
  SweetAlert.Title             := '';
  SweetAlert.Html              := MessageText;
  SweetAlert.TitleText         := '';
  SweetAlert.AllowEscapeKey    := True;
  SweetAlert.AllowOutsideClick := False;
  SweetAlert.ConfirmButtonText := 'Ok';
  SweetAlert.ShowCancelButton  := False;
  SweetAlert.ShowCloseButton   := False;
  SweetAlert.InputType         := ItNone;
  SweetAlert.Show();
end;

procedure TMainForm.WaveShowInformationToast(MessageText: string);
begin
  SyncToast.ToastPositions := tpTopRight;
  SyncToast.Show(str_information, MessageText, tcInfo);
end;

procedure TMainForm.WaveShowWarningMessage(MessageText: string);
begin
  SweetAlert.AlertType         := atWarning;
  SweetAlert.Title             := str_warning;
  SweetAlert.Html              := MessageText;
  SweetAlert.TitleText         := '';
  SweetAlert.AllowEscapeKey    := True;
  SweetAlert.AllowOutsideClick := False;
  SweetAlert.ConfirmButtonText := 'Ok';
  SweetAlert.ShowCancelButton  := False;
  SweetAlert.ShowCloseButton   := False;
  SweetAlert.InputType         := ItNone;
  SweetAlert.Show();
end;

procedure TMainForm.WaveShowWarningToast(MessageText: string);
begin
  SyncToast.ToastPositions := tpTopFullWidth;
  SyncToast.Show(str_warning, MessageText, tcWarning);
end;

initialization
  RegisterAppFormClass(TMainForm);

end.

