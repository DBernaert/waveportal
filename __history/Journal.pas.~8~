unit Journal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniGUIBaseClasses, uniPanel, uniPageControl, uniDBNavigator, uniEdit, uniBasicGrid,
  uniDBGrid, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

var
	str_attachments:        string = 'Bijlagen'; // TSI: Localized (Don't modify!)
	str_relations:          string = 'Relaties'; // TSI: Localized (Don't modify!)
	str_propositions:       string = 'Voorstellen'; // TSI: Localized (Don't modify!)
	str_commission_schemas: string = 'Commissieschema''s'; // TSI: Localized (Don't modify!)
	strNotes:               string = 'Notities'; // TSI: Localized (Don't modify!)
	strTasks:               string = 'Taken'; // TSI: Localized (Don't modify!)


type
  TJournalFrm = class(TUniFrame)
    PageControlJournal: TUniPageControl;
    LinkedLanguageJournal: TsiLangLinked;
    procedure LinkedLanguageJournalChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
  private
    { Private declarations }
    procedure Show_tasks(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_contacts(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_proposals(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_calls(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_Notes(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_attachments(Module_id: integer; Master_datasource: TDataSource);
    procedure Show_insurance_payments(Module_id: integer; Master_datasource: TDataSource);
  public
    { Public declarations }
    procedure Start_journal(Module_id: integer; Master_datasource: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, ProposalsPanel, AttachmentsPanel, TasksPanel, CallsPanel,
     NotesPanel, InsurancePaymentsPanel;

{ TJournalFrm }

procedure TJournalFrm.LinkedLanguageJournalChangeLanguage(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TJournalFrm.Show_attachments(Module_id: integer; Master_datasource: TDataSource);
var AttachmentsFrame:    TUniFrame;
    AttachmentsTabSheet: TUniTabSheet;
begin
  AttachmentsTabSheet                  := TUniTabSheet.Create(Self);
  AttachmentsTabSheet.PageControl      := PageControlJournal;
  AttachmentsTabSheet.AlignmentControl := uniAlignmentClient;
  AttachmentsTabSheet.Layout           := 'fit';
  AttachmentsTabSheet.Caption          := str_attachments;
  AttachmentsTabSheet.ImageIndex       := 38;
  AttachmentsFrame                     := TAttachmentsPanelFrm.Create(Self);
  With (AttachmentsFrame as TAttachmentsPanelFrm)
  do begin
       Start_attachments(Module_id, Master_datasource);
       Parent := AttachmentsTabSheet;
     end;
end;

procedure TJournalFrm.Show_contacts(Module_id: integer; Master_datasource: TDataSource);
var ContactsFrame:    TUniFrame;
    ContactsTabSheet: TUniTabSheet;
begin
{  ContactsTabSheet := TUniTabSheet.Create(Self);
  ContactsTabSheet.PageControl := PageControlJournal;
  ContactsTabSheet.AlignmentControl := uniAlignmentClient;
  ContactsTabSheet.Layout  := 'fit';
  ContactsTabSheet.Caption := str_relations;
  ContactsTabSheet.ImageIndex := 18;
  ContactsFrame := TContactsPanelFrm.Create(Self);
  With (ContactsFrame as TContactsPanelFrm)
  do begin
       Start_contacts(Module_id, Master_datasource);
       Parent := ContactsTabSheet;
     end;}
end;

procedure TJournalFrm.Show_insurance_payments(Module_id: integer;
  Master_datasource: TDataSource);
var InsurancePaymentsFrame:    TUniFrame;
    InsurancePaymentsTabSheet: TUniTabSheet;
begin
  InsurancePaymentsTabSheet                  := TUniTabSheet.Create(Self);
  InsurancePaymentsTabSheet.PageControl      := PageControlJournal;
  InsurancePaymentsTabSheet.AlignmentControl := uniAlignmentClient;
  InsurancePaymentsTabSheet.Layout           := 'fit';
  InsurancePaymentsTabSheet.Caption          := 'Betalingen';
  InsurancePaymentsTabSheet.ImageIndex       := 21;
  InsurancePaymentsFrame                     := TInsurancePaymentsPanelFrm.Create(Self);
  With (InsurancePaymentsFrame as TInsurancePaymentsPanelFrm)
  do begin
       Start_insurance_file_payments(Module_id, Master_datasource);
       Parent := InsurancePaymentsTabSheet;
     end;
end;

procedure TJournalFrm.Show_Notes(Module_id: integer; Master_datasource: TDataSource);
var NotesFrame:    TUniFrame;
    NotesTabSheet: TUniTabSheet;
begin
  NotesTabSheet                  := TUniTabSheet.Create(Self);
  NotesTabSheet.PageControl      := PageControlJournal;
  NotesTabSheet.AlignmentControl := uniAlignmentClient;
  NotesTabSheet.Layout           := 'fit';
  NotesTabSheet.Caption          := strNotes;
  NotesTabSheet.ImageIndex       := 49;
  NotesFrame                     := TNotesPanelFrm.Create(Self);
  With (NotesFrame as TNotesPanelFrm)
  do begin
       Start_notes(Module_id, Master_datasource);
       Parent := NotesTabSheet;
     end;
end;

procedure TJournalFrm.Show_proposals(Module_id: integer; Master_datasource: TDataSource);
var ProposalsFrame:    TUniFrame;
    ProposalsTabSheet: TUniTabSheet;
begin
  ProposalsTabSheet                  := TUniTabSheet.Create(Self);
  ProposalsTabSheet.PageControl      := PageControlJournal;
  ProposalsTabSheet.AlignmentControl := uniAlignmentClient;
  ProposalsTabSheet.Layout           := 'fit';
  ProposalsTabSheet.Caption          := str_propositions;
  ProposalsTabSheet.ImageIndex       := 3;
  ProposalsFrame                     := TProposalsPanelFrm.Create(Self);
  With (ProposalsFrame as TProposalsPanelFrm)
  do begin
       Start_proposals(Module_id, Master_datasource);
       Parent := ProposalsTabSheet;
     end;
end;

procedure TJournalFrm.Show_tasks(Module_id: integer; Master_datasource: TDataSource);
var TasksFrame:    TUniFrame;
    TasksTabSheet: TUniTabSheet;
begin
  TasksTabSheet                  := TUniTabSheet.Create(Self);
  TasksTabSheet.PageControl      := PageControlJournal;
  TasksTabSheet.AlignmentControl := uniAlignmentClient;
  TasksTabSheet.Layout           := 'fit';
  TasksTabSheet.Caption          := strTasks;
  TasksTabSheet.ImageIndex       := 8;
  TasksFrame := TTasksPanelFrm.Create(Self);
  With (TasksFrame as TTasksPanelFrm)
  do begin
       Start_tasks(Module_id, Master_datasource);
       Parent := TasksTabSheet;
     end;
end;

procedure TJournalFrm.Show_calls(Module_id: integer; Master_datasource: TDataSource);
var CallsFrame:    TUniFrame;
    CallsTabSheet: TUniTabSheet;
begin
{  CallsTabSheet := TUniTabSheet.Create(Self);
  CallsTabSheet.PageControl := PageControlJournal;
  CallsTabSheet.AlignmentControl := uniAlignmentClient;
  CallsTabSheet.Layout  := 'fit';
  CallsTabSheet.Caption := 'Telefoons';
  CallsTabSheet.ImageIndex := 34;
  CallsFrame := TCallsPanelFrm.Create(Self);
  With (CallsFrame as TCallsPanelFrm)
  do begin
       Start_calls(Module_id, Master_datasource);
       Parent := CallsTabSheet;
     end;  }
end;

procedure TJournalFrm.Start_journal(Module_id: integer; Master_datasource: TDataSource);
begin
  {Overview id's:
   1: Accounts
   2: contacts
   3: contributors
   4: financial institutions
   5: credit files
   6: credit proposals
  }
  case Module_id of
    1:  begin //Accounts
          Show_contacts(Module_id, Master_datasource);
        end;
    5:  begin //Credit files
          Show_proposals(Module_id, Master_datasource);
        end;
    10: begin //Insurance files
          Show_insurance_payments(Module_id, Master_datasource);
        end;
  end;

  Show_Tasks(Module_id, Master_datasource);
  case Module_id of
    1: Show_Calls(Module_id, Master_datasource);
    2: Show_Calls(Module_id, Master_datasource);
    3: Show_Calls(Module_id, Master_datasource);
  end;

  Show_Notes(Module_id, Master_datasource);
  if UniMainModule.User_portal_rights >=2
  then Show_attachments(Module_id, Master_datasource);
  PageControlJournal.ActivePageIndex := 0;
end;

procedure TJournalFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TJournalFrm.UpdateStrings;
begin
  strTasks               := LinkedLanguageJournal.GetTextOrDefault('strstrTasks' (* 'Taken' *) );
  strNotes               := LinkedLanguageJournal.GetTextOrDefault('strstrNotes' (* 'Notities' *) );
  str_commission_schemas := LinkedLanguageJournal.GetTextOrDefault('strstr_commission_schemas' (* 'Commissieschema's' *) );
  str_propositions       := LinkedLanguageJournal.GetTextOrDefault('strstr_propositions' (* 'Voorstellen' *) );
  str_relations          := LinkedLanguageJournal.GetTextOrDefault('strstr_relations' (* 'Relaties' *) );
  str_attachments        := LinkedLanguageJournal.GetTextOrDefault('strstr_attachments' (* 'Bijlagen' *) );
end;

end.



