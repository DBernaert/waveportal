unit SwissLifeDemandsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniPanel, uniScrollBox, uniEdit,
  uniDBEdit, uniImage, uniLabel, uniCheckBox, uniDBCheckBox,
  Vcl.Imaging.pngimage, uniDateTimePicker, uniDBDateTimePicker, uniMultiItem,
  uniComboBox, uniButton, UniThemeButton, Data.DB, MemDS, DBAccess, IBC, siComp,
  siLngLnk, uniDBText;

var
	str_document_already_present:                       string = 'Er is reeds een document aanwezig.'; // TSI: Localized (Don't modify!)
	str_error_adding_demand:                            string = 'Fout bij het toevoegen van de aanvraag'; // TSI: Localized (Don't modify!)
	str_demand_in_use:                                  string = 'De aanvraag is in gebruik, deze kan momenteel niet aangepast worden'; // TSI: Localized (Don't modify!)
	str_subscriptionnumber_required:                    string = 'Het abonnementsaanvraagnummer is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_title_required:                                 string = 'De titel is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_name_required:                                  string = 'De naam is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_firstname_required:                             string = 'De voornaam is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_street_required:                                string = 'De straat is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_streetnumber_required:                          string = 'Het straatnummer is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_postalcode_required:                            string = 'De postcode is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_city_required:                                  string = 'De stad is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_date_of_birth_required:                         string = 'De geboortedatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_birthplace_required:                            string = 'De geboorteplaats is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_nationality_required:                           string = 'De nationaliteit is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_sex_required:                                   string = 'Het geslacht is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_civil_state_required:                           string = 'De burgerlijke staat is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_professional_activity_required:                 string = 'De professionele activiteit is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_idcardNumberRequired:                           string = 'Nummer identiteitskaart is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_enddate_idcard_required:                        string = 'De einddatum van geldigheid van de identiteitskaart is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_phone_or_mobile_required:                       string = 'Telefoon of gsm is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_email_required:                                 string = 'E-mail adres is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_smoke_behavior_required:                        string = 'Rookgedrag is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_choice_postal_adres_required:                   string = 'De keuze van het postadres is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_type_third_party_required:                      string = 'Het type derde partij is verplicht ter hoogte van het postadres'; // TSI: Localized (Don't modify!)
	str_name_firstname_company_required_postal_address: string = 'De naam, voornaam of firmanaam is verplicht ter hoogte van het postadres'; // TSI: Localized (Don't modify!)
	str_address_required_postal_address:                string = 'De straat van het postadres is verplicht'; // TSI: Localized (Don't modify!)
	str_streetnumber_required_postal_addres:            string = 'Het straatnummer van het postadres is verplicht'; // TSI: Localized (Don't modify!)
	str_postal_code_required_postal_adres:              string = 'De postcode van het postadres is verplicht'; // TSI: Localized (Don't modify!)
	str_city_required_postal_adres:                     string = 'De stad van het postadres is verplicht'; // TSI: Localized (Don't modify!)
	str_country_required_postal_adres:                  string = 'Het land van het postadres is verplicht'; // TSI: Localized (Don't modify!)
	str_base_capital_required:                          string = 'Het basiskapitaal is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_startdate_contract_required:                    string = 'De ingangsdatum van het contract is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_enddate_contract_required:                      string = 'De vervaldatum van het contract is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_frequence_payments_required:                    string = 'De frequentie van premiebetalingen is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_clause_beneficiary_required:                    string = 'De clausule begunstigde is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_contactdata_beneficiary_required:               string = 'De contactgegevens van de begunstigde clause zijn een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_iban_number_required:                           string = 'Het IBAN-nummer is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_incasso_fifth_required:                         string = 'Incassodatum op de 5de werkdag is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_startdate_contract_not_valid:                   string = 'De ingangsdatum van het contract is niet geldig'; // TSI: Localized (Don't modify!)
	str_proposition_required:                           string = 'Het voorstel ontbreekt'; // TSI: Localized (Don't modify!)
	str_general_conditions_required:                    string = 'Het document algemene voorwaarden ontbreekt'; // TSI: Localized (Don't modify!)
	str_medical_checklist_required:                     string = 'De medische vragenlijst ontbreekt'; // TSI: Localized (Don't modify!)
	str_due_dilligence_required:                        string = 'Het document due dilligence ontbreekt'; // TSI: Localized (Don't modify!)
	str_agreement_insured_required:                     string = 'Het document akkoord van de verzekerde ontbreekt'; // TSI: Localized (Don't modify!)
	str_sepa_required:                                  string = 'Het sepa formulier ontbreekt'; // TSI: Localized (Don't modify!)
	str_copy_id_card_insured_required:                  string = 'Kopie ID kaart verzekeringsnemer ontbreekt'; // TSI: Localized (Don't modify!)
	str_length_required:                                string = 'De lengte is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_weight_required:                                string = 'Het gewicht is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_engine_missing:                                 string = 'Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar'; // TSI: Localized (Don't modify!)
	str_insured_capital_required:                       string = 'Verzekerd kapitaal is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_smoker_required:                                string = 'Roker is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_birthdate_required:                             string = 'De geboortedatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_premium_payment_required:                       string = 'De premiebetaling is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_confirm_remove_document:                        string = 'Verwijderen document?'; // TSI: Localized (Don't modify!)
	str_nis_number_not_correct:                         string = 'Het rijksregisternummer is niet geldig'; // TSI: Localized (Don't modify!)
	Str_idcard_expired:                                 string = 'De geldigheid van de identiteitskaart is verlopen'; // TSI: Localized (Don't modify!)
	str_startdate_not_valid:                            string = 'De begindatum is niet geldig'; // TSI: Localized (Don't modify!)
	str_enddate_not_valid:                              string = 'De einddatum is niet geldig'; // TSI: Localized (Don't modify!)
	str_iban_number_not_correct:                        string = 'Het iban nummer is niet correct'; // TSI: Localized (Don't modify!)
	str_close:                                          string = 'Sluiten'; // TSI: Localized (Don't modify!)
	str_proof_of_residence_required:                    string = 'Bewijs van woonst ontbreekt'; // TSI: Localized (Don't modify!)

type
  TSwissLifeDemandsAddFrm = class(TUniForm)
    ContainerHeader: TUniContainerPanel;
    ContainerBody: TUniContainerPanel;
    ContainerFooter: TUniContainerPanel;
    ScrollBoxProposal: TUniScrollBox;
    ContainerDocuments: TUniContainerPanel;
    ContainerHeaderLeft: TUniContainerPanel;
    ContainerHeaderRight: TUniContainerPanel;
    LblProposition: TUniLabel;
    LblSwissLifeProtectBelgium: TUniLabel;
    LblTitleNationalinsuranceNumber: TUniLabel;
    LblTitleIdentificationInsuredPerson: TUniLabel;
    LblTitleInsuredPerson: TUniLabel;
    LblNameInsuredPerson: TUniLabel;
    LblFirstNameInsuredPerson: TUniLabel;
    LblStreetNumberBoxInsuredPerson: TUniLabel;
    LblPostalCodeInsuredPerson: TUniLabel;
    LblCityInsuredPerson: TUniLabel;
    LblCountryInsuredPerson: TUniLabel;
    LblValueCountryInsuredPerson: TUniLabel;
    LblDateOfBirthInsuredPerson: TUniLabel;
    LblBirthPlaceInsuredPerson: TUniLabel;
    LblSexInsuredPerson: TUniLabel;
    LblNationalityInsuredPerson: TUniLabel;
    LblCivilStateInsuredPerson: TUniLabel;
    LblProfessionalActivityInsuredPerson: TUniLabel;
    LblIdentitycardnumberInsuredPerson: TUniLabel;
    LblIdentitycardValidUntilInsuredPerson: TUniLabel;
    LblPhoneNumberInsuredPerson: TUniLabel;
    LblEmailInsuredPerson: TUniLabel;
    LblInsuredSmoker: TUniLabel;
    LblTitleCapitalAndDuration: TUniLabel;
    LblAmountInsuredCapital: TUniLabel;
    LblStartDateContract: TUniLabel;
    LblEndDateContract: TUniLabel;
    LblTitlePremiumPayments: TUniLabel;
    LblFrequenceOfPayment: TUniLabel;
    LblPremiumAmount: TUniLabel;
    LblPremiumDuration: TUniLabel;
    LblTitleClauseBeneficiaries: TUniLabel;
    LblBeneficiary1: TUniLabel;
    LblBeneficiary2: TUniLabel;
    LblTitleBeneficiary1: TUniLabel;
    LblAssignmentPercentageBenef1: TUniLabel;
    LblBenef1Name: TUniLabel;
    LblBenef1FirstName: TUniLabel;
    LblBenef1StreetandNumber: TUniLabel;
    LblBenef1PostalCode: TUniLabel;
    LblBenef1Location: TUniLabel;
    LblBenef1Country: TUniLabel;
    LblBenef1DateOfBirth: TUniLabel;
    LblBenef1BirthPlace: TUniLabel;
    LblSexBenef1: TUniLabel;
    LblLinkInsuredBenef1: TUniLabel;
    LblBenef1DivergentBen: TUniLabel;
    LblTitleSepaIncasso: TUniLabel;
    LblIBan: TUniLabel;
    LblReferenceMandate: TUniLabel;
    ImageHeader: TUniImage;
    EditNationalInsuranceNumber: TUniDBEdit;
    EditNameInsuredPerson: TUniDBEdit;
    EditFirstNameInsuredPerson: TUniDBEdit;
    EditStreetInsuredPerson: TUniDBEdit;
    EditPostalCodeInsuredPerson: TUniDBEdit;
    EditCityInsuredPerson: TUniDBEdit;
    EditDateOfBirthInsuredPerson: TUniDBDateTimePicker;
    EditBirthPlaceInsuredPerson: TUniDBEdit;
    EditNationalityInsuredPerson: TUniDBEdit;
    ComboCivilstateInsuredPerson: TUniComboBox;
    EditProfessionalActivityInsuredPerson: TUniDBEdit;
    EditIdentitycardnumberInsuredPerson: TUniDBEdit;
    EditIdentitycardValidUntilInsuredPerson: TUniDBDateTimePicker;
    EditPhoneNumberInsuredPerson: TUniDBEdit;
    EditEmailInsuredPerson: TUniDBEdit;
    EditStartDateContract: TUniDBDateTimePicker;
    EditEndDateContract: TUniDBDateTimePicker;
    ContainerFooterRight: TUniContainerPanel;
    BtnSave: TUniThemeButton;
    BtnSend: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    PanelHeaderDocuments: TUniPanel;
    EditPremiumAmount: TUniDBFormattedNumberEdit;
    EditPremiumPaymentYears: TUniDBNumberEdit;
    LblRankBenef1: TUniLabel;
    LblPercentageBenef1: TUniLabel;
    EditRankBenef1: TUniDBNumberEdit;
    EditPercentageBenef1: TUniDBFormattedNumberEdit;
    LblRankBenef2: TUniLabel;
    EditRankBenef2: TUniDBNumberEdit;
    LblPercentageBenef2: TUniLabel;
    EditPercentageBenef2: TUniDBFormattedNumberEdit;
    EditBenef1Name: TUniDBEdit;
    EditBenef2Name: TUniDBEdit;
    EditBenef1Firstname: TUniDBEdit;
    EditBenef2Firstname: TUniDBEdit;
    EditBenef1StreetandNumber: TUniDBEdit;
    EditBenef2StreetandNumber: TUniDBEdit;
    EditBenef1PostalCode: TUniDBEdit;
    EditBenef2PostalCode: TUniDBEdit;
    EditBenef1City: TUniDBEdit;
    EditBenef2City: TUniDBEdit;
    EditBenef1Country: TUniDBEdit;
    EditBenef2Country: TUniDBEdit;
    EditBenef1DateOfBirth: TUniDBDateTimePicker;
    EditBenef2DateOfBirth: TUniDBDateTimePicker;
    EditBenef1BirthPlace: TUniDBEdit;
    EditBenef2BirthPlace: TUniDBEdit;
    EditBenef1LinkInsured: TUniDBEdit;
    EditBenef2LinkInsured: TUniDBEdit;
    EditBenef1DivergentBeneficiary: TUniDBEdit;
    EditBenef2DivergentBeneficiary: TUniDBEdit;
    LblBeneficiary3: TUniLabel;
    LblBeneficiary4: TUniLabel;
    LblTitleBeneficiary2: TUniLabel;
    LblAssignmentPercentageBenef2: TUniLabel;
    LblRankBenef3: TUniLabel;
    EditRankBenef3: TUniDBNumberEdit;
    LblPercentageBenef3: TUniLabel;
    EditPercentageBenef3: TUniDBFormattedNumberEdit;
    LblRankBenef4: TUniLabel;
    EditRankBenef4: TUniDBNumberEdit;
    LblPercentageBenef4: TUniLabel;
    EditPercentageBenef4: TUniDBFormattedNumberEdit;
    LblBenef3Name: TUniLabel;
    EditBenef3Name: TUniDBEdit;
    EditBenef4Name: TUniDBEdit;
    LblBenef3FirstName: TUniLabel;
    EditBenef3Firstname: TUniDBEdit;
    EditBenef4Firstname: TUniDBEdit;
    LblBenef2StreetandNumber: TUniLabel;
    EditBenef3StreetandNumber: TUniDBEdit;
    EditBenef4StreetandNumber: TUniDBEdit;
    LblBenef2PostalCode: TUniLabel;
    EditBenef3PostalCode: TUniDBEdit;
    EditBenef4PostalCode: TUniDBEdit;
    LblBenef2Location: TUniLabel;
    EditBenef3City: TUniDBEdit;
    EditBenef4City: TUniDBEdit;
    LblBenef2Country: TUniLabel;
    EditBenef3Country: TUniDBEdit;
    EditBenef4Country: TUniDBEdit;
    LblBenef3DateOfBirth: TUniLabel;
    EditBenef3DateOfBirth: TUniDBDateTimePicker;
    EditBenef4DateOfBirth: TUniDBDateTimePicker;
    LblBenef2BirthPlace: TUniLabel;
    EditBenef3BirthPlace: TUniDBEdit;
    EditBenef4BirthPlace: TUniDBEdit;
    LblSexBenef2: TUniLabel;
    LblLinkInsuredBenef3: TUniLabel;
    EditBenef3LinkInsured: TUniDBEdit;
    EditBenef4LinkInsured: TUniDBEdit;
    LblBenef3DivergentBen: TUniLabel;
    EditBenef3DivergentBeneficiary: TUniDBEdit;
    EditBenef4DivergentBeneficiary: TUniDBEdit;
    CheckBoxMoreDetailledBenefClause: TUniDBCheckBox;
    CheckBoxBenefClauseIncludedInDeed: TUniDBCheckBox;
    EditBenefClauseContactData: TUniDBEdit;
    EditIban: TUniDBEdit;
    LblUpLoadDoc1: TUniLabel;
    LblUpLoadDoc2: TUniLabel;
    Insurance_proposals: TIBCQuery;
    Ds_insurance_proposals: TDataSource;
    CheckBoxAppelationMale: TUniCheckBox;
    CheckBoxAppelationFemale: TUniCheckBox;
    CheckBoxSexFemaleInsuredPerson: TUniCheckBox;
    CheckBoxSexMaleInsuredPerson: TUniCheckBox;
    CheckBoxInsuredSmokerYes: TUniCheckBox;
    CheckBoxInsuredSmokerNo: TUniCheckBox;
    CheckBoxFrequencyMonthly: TUniCheckBox;
    CheckBoxFrequencyTrimesterly: TUniCheckBox;
    CheckBoxFrequencyYearly: TUniCheckBox;
    CheckBoxStandardClause: TUniCheckBox;
    CheckBoxOtherClause: TUniCheckBox;
    CheckBoxAppelationManBenef1: TUniCheckBox;
    CheckBoxAppelationWomenBenef1: TUniCheckBox;
    CheckBoxBenef1SexFemale: TUniCheckBox;
    CheckBoxBenef1SexMale: TUniCheckBox;
    CheckBoxAppelationManBenef2: TUniCheckBox;
    CheckBoxAppelationWomenBenef2: TUniCheckBox;
    CheckBoxBenef2SexFemale: TUniCheckBox;
    CheckBoxBenef2SexMale: TUniCheckBox;
    CheckBoxAppelationManBenef3: TUniCheckBox;
    CheckBoxAppelationWomenBenef3: TUniCheckBox;
    CheckBoxBenef3SexFemale: TUniCheckBox;
    CheckBoxBenef3SexMale: TUniCheckBox;
    CheckBoxAppelationManBenef4: TUniCheckBox;
    CheckBoxAppelationWomenBenef4: TUniCheckBox;
    CheckBoxBenef4SexFemale: TUniCheckBox;
    CheckBoxBenef4SexMale: TUniCheckBox;
    CheckBoxDomiciliationFifth: TUniCheckBox;
    LinkedLanguageSwissLifeDemandsAdd: TsiLangLinked;
    LblUpLoadDoc3: TUniLabel;
    LblUpLoadDoc4: TUniLabel;
    LblUpLoadDoc5: TUniLabel;
    LblUpLoadDoc6: TUniLabel;
    LblUpLoadDoc7: TUniLabel;
    LblDocumentRequired: TUniLabel;
    ColorDocumentPresent: TUniContainerPanel;
    LblDocumentPresent: TUniLabel;
    LblMobileNumberInsuredPerson: TUniLabel;
    EditMobileNumberInsuredPerson: TUniDBEdit;
    EditInsuredStreetNumber: TUniDBEdit;
    EditInsuredStreetBox: TUniDBEdit;
    ComboInsuredCapital: TUniComboBox;
    LblSepaReferenceMandateValue: TUniDBText;
    LblTitleCorrespondanceAddress: TUniLabel;
    CheckBoxCorrespondanceAddressInsuredPerson: TUniCheckBox;
    CheckBoxCorrespondanceOtherPerson: TUniCheckBox;
    CheckBoxCorrespondenceMale: TUniCheckBox;
    CheckBoxCorrespondanceFemale: TUniCheckBox;
    CheckBoxCorrespondanceCompany: TUniCheckBox;
    LblCorrespondanceName: TUniLabel;
    EditCorrespondanceName: TUniDBEdit;
    LblCorrespondanceAddress: TUniLabel;
    EditCorrespondanceStreet: TUniDBEdit;
    LblCorrespondanceZipCode: TUniLabel;
    EditCorrespondanceZipCode: TUniDBEdit;
    LblCorrespondanceLocation: TUniLabel;
    EditCorrespondanceLocation: TUniDBEdit;
    EditCorrespondanceStreetNumber: TUniDBEdit;
    EditCorrespondanceStreetBox: TUniDBEdit;
    LblCorrespondanceCountry: TUniLabel;
    EditCorrespondanceCountry: TUniDBEdit;
    LblPremiumGross: TUniLabel;
    EditPremiumGross: TUniDBFormattedNumberEdit;
    LblPremiumTax: TUniLabel;
    EditPremiumTax: TUniDBFormattedNumberEdit;
    EditLengthInsuredPerson: TUniDBNumberEdit;
    EditWeightInsuredPerson: TUniDBNumberEdit;
    LblLengthInsuredPerson: TUniLabel;
    LblWeightInsuredPerson: TUniLabel;
    BtnCalculate: TUniThemeButton;
    LblUpLoadDoc8: TUniLabel;
    LblLanguage: TUniLabel;
    ComboboxLanguage: TUniComboBox;
    BtnUploadDoc1: TUniThemeButton;
    BtnUploadDoc2: TUniThemeButton;
    BtnUploadDoc3: TUniThemeButton;
    BtnUploadDoc4: TUniThemeButton;
    BtnUploadDoc5: TUniThemeButton;
    BtnUploadDoc6: TUniThemeButton;
    BtnUploadDoc7: TUniThemeButton;
    BtnUploadDoc8: TUniThemeButton;
    BtnDeleteDoc1: TUniThemeButton;
    BtnDeleteDoc2: TUniThemeButton;
    BtnDeleteDoc3: TUniThemeButton;
    BtnDeleteDoc4: TUniThemeButton;
    BtnDeleteDoc5: TUniThemeButton;
    BtnDeleteDoc6: TUniThemeButton;
    BtnDeleteDoc7: TUniThemeButton;
    BtnDeleteDoc8: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure CheckBoxAppelationMaleChange(Sender: TObject);
    procedure CheckBoxAppelationFemaleChange(Sender: TObject);
    procedure CheckBoxSexFemaleInsuredPersonChange(Sender: TObject);
    procedure CheckBoxSexMaleInsuredPersonChange(Sender: TObject);
    procedure CheckBoxInsuredSmokerYesChange(Sender: TObject);
    procedure CheckBoxInsuredSmokerNoChange(Sender: TObject);
    procedure CheckBoxFrequencyMonthlyChange(Sender: TObject);
    procedure CheckBoxFrequencyTrimesterlyChange(Sender: TObject);
    procedure CheckBoxFrequencyYearlyChange(Sender: TObject);
    procedure CheckBoxStandardClauseChange(Sender: TObject);
    procedure CheckBoxOtherClauseChange(Sender: TObject);
    procedure CheckBoxAppelationManBenef1Change(Sender: TObject);
    procedure CheckBoxAppelationWomenBenef1Change(Sender: TObject);
    procedure CheckBoxBenef1SexFemaleChange(Sender: TObject);
    procedure CheckBoxBenef1SexMaleChange(Sender: TObject);
    procedure CheckBoxAppelationWomenBenef2Change(Sender: TObject);
    procedure CheckBoxAppelationManBenef2Change(Sender: TObject);
    procedure CheckBoxBenef2SexFemaleChange(Sender: TObject);
    procedure CheckBoxBenef2SexMaleChange(Sender: TObject);
    procedure CheckBoxAppelationManBenef3Change(Sender: TObject);
    procedure CheckBoxAppelationWomenBenef3Change(Sender: TObject);
    procedure CheckBoxBenef3SexFemaleChange(Sender: TObject);
    procedure CheckBoxBenef3SexMaleChange(Sender: TObject);
    procedure CheckBoxAppelationManBenef4Change(Sender: TObject);
    procedure CheckBoxAppelationWomenBenef4Change(Sender: TObject);
    procedure CheckBoxBenef4SexFemaleChange(Sender: TObject);
    procedure CheckBoxBenef4SexMaleChange(Sender: TObject);
    procedure BtnUploadDoc1Click(Sender: TObject);
    procedure LblUpLoadDoc1Click(Sender: TObject);
    procedure BtnDeleteDoc1Click(Sender: TObject);
    procedure UpdateStrings;
    procedure CheckBoxCorrespondanceAddressInsuredPersonChange(Sender: TObject);
    procedure CheckBoxCorrespondanceOtherPersonChange(Sender: TObject);
    procedure CheckBoxCorrespondenceMaleChange(Sender: TObject);
    procedure CheckBoxCorrespondanceFemaleChange(Sender: TObject);
    procedure CheckBoxCorrespondanceCompanyChange(Sender: TObject);
    procedure BtnSendClick(Sender: TObject);
    procedure BtnCalculateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    UploadDocumentIndex:   integer;
    procedure Check_color_documents;
    function  Save_demand: boolean;
    function  Validate_demand: boolean;
    procedure CallBackDocumentUpload(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Init_creation_swisslife_demand: boolean;
    function Init_creation_swisslife_demand_from_simulation(
               _Insured_BaseCapital:           integer;
               _Insured_Smoker:                integer;
               _Insured_DateOfBirth:           TDateTime;
               _Insured_Height:                integer;
               _Insured_Weight:                integer;
               _Insured_PremiumPayment:        integer): boolean;
    function Init_modification_swisslife_demand(DemandId: longint): boolean;
  end;

function SwissLifeDemandsAddFrm: TSwissLifeDemandsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, SwissLifeUpload, VCL.FlexCel.Core, FlexCel.XlsAdapter,
  Utils, UniFSConfirm, DateUtils, IBAN.Utils, IBAN.Types;

function SwissLifeDemandsAddFrm: TSwissLifeDemandsAddFrm;
begin
  Result := TSwissLifeDemandsAddFrm(UniMainModule.GetFormInstance(TSwissLifeDemandsAddFrm));
end;

procedure TSwissLifeDemandsAddFrm.BtnDeleteDoc1Click(Sender: TObject);
var DocumentName: string;
begin
  DocumentName := 'Insurance_linked_document' + IntToStr((Sender as TUniThemeButton).Tag);

  MainForm.Confirm.Question(UniMainModule.Portal_name, str_confirm_remove_document, 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Insurance_proposals.FieldByName(DocumentName).Clear;
        Check_color_documents;
      end;
    end);
end;

procedure TSwissLifeDemandsAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  //Check minimum for saving the demand
  if Trim(Insurance_proposals.FieldByName('Insured_name').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_name_required);
         Exit;
       end;

  //Capitalize the name
  Insurance_proposals.FieldByName('Insured_name').AsString :=
  Utils.Capitalize_string(Insurance_proposals.FieldByName('Insured_name').asString);

  if Trim(Insurance_proposals.FieldByName('Insured_firstname').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_firstname_required);
         Exit;
       end;

  Insurance_proposals.FieldByName('Insured_firstname').asString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_firstname').AsString);

  if ComboInsuredCapital.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_insured_capital_required);
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_dateOfBirth').IsNull
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_required);
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_height').isNull
  then begin
         MainForm.WaveShowWarningToast(str_length_required);
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_weight').IsNull
  then begin
         MainForm.WaveShowWarningToast(str_weight_required);
         Exit;
       end;

  if (CheckBoxInsuredSmokerYes.Checked = False) and (CheckBoxInsuredSmokerNo.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_smoker_required);
         Exit;
       end;

  if (CheckBoxFrequencyMonthly.Checked = False)     and
     (CheckBoxFrequencyTrimesterly.Checked = False) and
     (CheckBoxFrequencyYearly.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_premium_payment_required);
         Exit;
       end;

  BtnCalculateClick(Sender);

  if Save_demand
  then Close;
end;

procedure TSwissLifeDemandsAddFrm.BtnSendClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  BtnCalculateClick(Sender);
  if Validate_demand
  then begin
         //Change the status to Send to Cares
         Insurance_proposals.FieldByName('Proposal_status').asInteger := 1;
         if Save_demand
         then Close;
       end;
end;

procedure TSwissLifeDemandsAddFrm.BtnUploadDoc1Click(Sender: TObject);
var DocumentName: string;
begin
  UploadDocumentIndex := (Sender as TUniThemeButton).Tag;
  DocumentName := 'Insurance_linked_document' + IntToStr((Sender as TUniThemeButton).Tag);
  //Check if a document is present
  if not Insurance_proposals.FieldByName(DocumentName).isNull
  then begin
         MainForm.WaveShowErrorToast(str_document_already_present);
         Exit;
       end;

  if Save_demand
  then begin
         Insurance_proposals.Edit;
         UniMainModule.Result_dbAction := 0;
         With SwissLifeUploadFrm
         do begin
              if Initialize_swissLife_upload(Insurance_proposals.FieldByName('Id').AsInteger)
              then ShowModal(CallBackDocumentUpload)
              else Close;
            end;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CallBackDocumentUpload(Sender: TComponent; AResult: Integer);
var DocumentName: string;
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         DocumentName := 'Insurance_linked_document' + IntToStr(UploadDocumentIndex);
         Insurance_proposals.FieldByname(DocumentName).asInteger := UniMainModule.Result_dbAction;
         Check_color_documents;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationFemaleChange(
  Sender: TObject);
begin
  if CheckBoxAppelationFeMale.Checked
  then CheckBoxAppelationMale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationMaleChange(Sender: TObject);
begin
  if CheckBoxAppelationMale.Checked
  then CheckBoxAppelationFemale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationManBenef1Change(
  Sender: TObject);
begin
  if CheckBoxAppelationManBenef1.Checked
  then CheckBoxAppelationWomenBenef1.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationManBenef2Change(
  Sender: TObject);
begin
  if CheckBoxAppelationManBenef2.Checked
  then CheckBoxAppelationWomenBenef2.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationManBenef3Change(
  Sender: TObject);
begin
  if CheckBoxAppelationManBenef3.Checked
  then CheckBoxAppelationWomenBenef3.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationManBenef4Change(
  Sender: TObject);
begin
  if CheckBoxAppelationManBenef4.Checked
  then CheckBoxAppelationWomenBenef4.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationWomenBenef1Change(
  Sender: TObject);
begin
  if CheckBoxAppelationWomenBenef1.Checked
  then CheckBoxAppelationManBenef1.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationWomenBenef2Change(
  Sender: TObject);
begin
  if CheckBoxAppelationWomenBenef2.Checked
  then CheckBoxAppelationManBenef2.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationWomenBenef3Change(
  Sender: TObject);
begin
  if CheckBoxAppelationWomenBenef3.Checked
  then CheckBoxAppelationManBenef3.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxAppelationWomenBenef4Change(
  Sender: TObject);
begin
  if CheckBoxAppelationWomenBenef4.Checked
  then CheckBoxAppelationManBenef4.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef1SexFemaleChange(
  Sender: TObject);
begin
  if CheckBoxBenef1SexFemale.Checked
  then CheckBoxBenef1SexMale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef1SexMaleChange(Sender: TObject);
begin
  if CheckBoxBenef1SexMale.Checked
  then CheckBoxBenef1SexFemale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef2SexFemaleChange(
  Sender: TObject);
begin
  if CheckBoxBenef2SexFemale.Checked
  then CheckBoxBenef2SexMale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef2SexMaleChange(Sender: TObject);
begin
  if CheckBoxBenef2SexMale.Checked
  then CheckBoxBenef2SexFemale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef3SexFemaleChange(
  Sender: TObject);
begin
  if CheckBoxBenef3SexFemale.Checked
  then CheckBoxBenef3SexMale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef3SexMaleChange(Sender: TObject);
begin
  if CheckBoxBenef3SexMale.Checked
  then CheckBoxBenef3SexFemale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef4SexFemaleChange(
  Sender: TObject);
begin
  if CheckBoxBenef4SexFemale.Checked
  then CheckBoxBenef4SexMale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxBenef4SexMaleChange(Sender: TObject);
begin
  if CheckBoxBenef4SexMale.Checked
  then CheckBoxBenef4SexFemale.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxCorrespondanceAddressInsuredPersonChange(
  Sender: TObject);
begin
  if CheckBoxCorrespondanceAddressInsuredPerson.Checked
  then CheckBoxCorrespondanceOtherPerson.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxCorrespondanceCompanyChange(
  Sender: TObject);
begin
  if CheckBoxCorrespondanceCompany.Checked
  then begin
         CheckBoxCorrespondenceMale.Checked    := False;
         CheckBoxCorrespondanceFemale.Checked  := False;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxCorrespondanceFemaleChange(
  Sender: TObject);
begin
  if CheckBoxCorrespondanceFemale.Checked
  then begin
         CheckBoxCorrespondenceMale.Checked    := False;
         CheckBoxCorrespondanceCompany.Checked := False;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxCorrespondanceOtherPersonChange(
  Sender: TObject);
begin
  if CheckBoxCorrespondanceOtherPerson.Checked
  then CheckBoxCorrespondanceAddressInsuredPerson.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxCorrespondenceMaleChange(
  Sender: TObject);
begin
  if CheckBoxCorrespondenceMale.Checked
  then begin
         CheckBoxCorrespondanceFemale.Checked  := False;
         CheckBoxCorrespondanceCompany.Checked := False;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxFrequencyMonthlyChange(
  Sender: TObject);
begin
  if CheckBoxFrequencyMonthly.Checked
  then begin
         CheckBoxFrequencyTrimesterly.Checked := False;
         CheckBoxFrequencyYearly.Checked      := False;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxFrequencyTrimesterlyChange(
  Sender: TObject);
begin
  if CheckBoxFrequencyTrimesterly.Checked
  then begin
         CheckBoxFrequencyMonthly.Checked := False;
         CheckBoxFrequencyYearly.Checked  := False;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxFrequencyYearlyChange(
  Sender: TObject);
begin
  if CheckBoxFrequencyYearly.Checked
  then begin
         CheckBoxFrequencyMonthly.Checked      := False;
         CheckBoxFrequencyTrimesterly.Checked  := False;
       end;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxInsuredSmokerNoChange(
  Sender: TObject);
begin
  if CheckBoxInsuredSmokerNo.Checked
  then CheckBoxInsuredSmokerYes.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxInsuredSmokerYesChange(
  Sender: TObject);
begin
  if CheckBoxInsuredSmokerYes.Checked
  then CheckBoxInsuredSmokerNo.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxOtherClauseChange(Sender: TObject);
begin
  if CheckBoxOtherClause.Checked
  then CheckBoxStandardClause.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxSexFemaleInsuredPersonChange(
  Sender: TObject);
begin
  if CheckBoxSexFemaleInsuredPerson.Checked
  then CheckBoxSexMaleInsuredPerson.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxSexMaleInsuredPersonChange(
  Sender: TObject);
begin
  if CheckBoxSexMaleInsuredPerson.Checked
  then CheckBoxSexFemaleInsuredPerson.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.CheckBoxStandardClauseChange(Sender: TObject);
begin
  if CheckBoxStandardClause.Checked
  then CheckBoxOtherClause.Checked := False;
end;

procedure TSwissLifeDemandsAddFrm.Check_color_documents;
begin
  if not Insurance_proposals.FieldByName('Insurance_linked_document1').isNull
  then LblUploadDoc1.Font.Color := ClGreen
  else LblUploadDoc1.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document2').isNull
  then LblUploadDoc2.Font.Color := ClGreen
  else LblUploadDoc2.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document3').isNull
  then LblUploadDoc3.Font.Color := ClGreen
  else LblUploadDoc3.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document4').isNull
  then LblUploadDoc4.Font.Color := ClGreen
  else LblUploadDoc4.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document5').isNull
  then LblUploadDoc5.Font.Color := ClGreen
  else LblUploadDoc5.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document6').isNull
  then LblUploadDoc6.Font.Color := ClGreen
  else LblUploadDoc6.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document7').isNull
  then LblUploadDoc7.Font.Color := ClGreen
  else LblUploadDoc7.Font.Color := ClWindowText;
  if not Insurance_proposals.FieldByName('Insurance_linked_document8').isNull
  then LblUploadDoc8.Font.Color := ClGreen
  else LblUploadDoc8.Font.Color := ClWindowText;
end;

procedure TSwissLifeDemandsAddFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

function TSwissLifeDemandsAddFrm.Init_creation_swisslife_demand: boolean;
begin
  Try
    Insurance_proposals.Close;
    Insurance_proposals.SQL.Clear;
    Insurance_proposals.SQL.Add('Select first 0 * from insurance_proposals');
    Insurance_proposals.Open;
    Insurance_proposals.Insert;

    Insurance_proposals.FieldByName('CompanyId').AsInteger                   := UniMainModule.Company_id;
    Insurance_proposals.FieldByName('Insurance_company').AsInteger           := 4;
    Insurance_proposals.FieldByName('Contributor_id').AsInteger              := UniMainModule.User_id;
    Insurance_proposals.FieldByName('Proposal_status').AsInteger             := 0;
    Insurance_proposals.FieldByName('Removed').AsString                      := '0';
    Insurance_proposals.FieldByName('Date_entered').AsDateTime               := Now;
    Insurance_proposals.FieldByName('Date_modified').AsDateTime              := Now;
    ComboboxLanguage.ItemIndex                                               := 0;
    EditNationalInsuranceNumber.SetFocus;
    Result := True;
  Except
    Result := False;
    MainForm.WaveShowWarningToast(str_error_adding_demand);
  End;
end;

function TSwissLifeDemandsAddFrm.Init_creation_swisslife_demand_from_simulation(
         _Insured_BaseCapital:           integer;
         _Insured_Smoker:                integer;
         _Insured_DateOfBirth:           TDateTime;
         _Insured_Height:                integer;
         _Insured_Weight:                integer;
         _Insured_PremiumPayment:        integer): boolean;
begin
  Try
    Insurance_proposals.Close;
    Insurance_proposals.SQL.Clear;
    Insurance_proposals.SQL.Add('Select first 0 * from insurance_proposals');
    Insurance_proposals.Open;
    Insurance_proposals.Insert;

    Insurance_proposals.FieldByName('CompanyId').AsInteger                   := UniMainModule.Company_id;
    Insurance_proposals.FieldByName('Insurance_company').AsInteger           := 4;
    Insurance_proposals.FieldByName('Contributor_id').AsInteger              := UniMainModule.User_id;
    Insurance_proposals.FieldByName('Proposal_status').AsInteger             := 0;
    Insurance_proposals.FieldByName('Removed').AsString                      := '0';
    Insurance_proposals.FieldByName('Date_entered').AsDateTime               := Now;
    Insurance_proposals.FieldByName('Date_modified').AsDateTime              := Now;
    ComboboxLanguage.ItemIndex                                               := 0;

    //Place the variables
    if _Insured_BaseCapital <> -1
    then ComboInsuredCapital.ItemIndex                                       := _Insured_BaseCapital;
    if _Insured_Smoker = 0
    then begin
           CheckBoxInsuredSmokerYes.Checked                                  := False;
           CheckBoxInsuredSmokerNo.Checked                                   := True;
         end
    else if _Insured_Smoker = 1
         then begin
                CheckBoxInsuredSmokerYes.Checked                             := True;
                CheckBoxInsuredSmokerNo.Checked                              := False;
              end;

    if _Insured_DateOfBirth <> -1
    then Insurance_proposals.FieldByName('Insured_dateofbirth').AsDateTime   := _Insured_DateOfBirth;

    Insurance_proposals.FieldByName('Insured_height').AsInteger              := _Insured_Height;
    Insurance_proposals.FieldByName('Insured_weight').AsInteger              := _Insured_Weight;

    if _Insured_PremiumPayment = 0
    then begin
           CheckBoxFrequencyMonthly.Checked                                  := True;
           CheckBoxFrequencyTrimesterly.Checked                              := False;
           CheckBoxFrequencyYearly.Checked                                   := False;
         end
    else if _Insured_PremiumPayment = 1
         then begin
                CheckBoxFrequencyMonthly.Checked                             := False;
                CheckBoxFrequencyTrimesterly.Checked                         := True;
                CheckBoxFrequencyYearly.Checked                              := False;
              end
         else if _Insured_PremiumPayment = 2
              then begin
                     CheckBoxFrequencyMonthly.Checked                        := False;
                     CheckBoxFrequencyTrimesterly.Checked                    := False;
                     CheckBoxFrequencyYearly.Checked                         := True;
                   end;

    BtnCalculateClick(nil);
    EditNationalInsuranceNumber.SetFocus;
    Result := True;
  Except
    Result := False;
    MainForm.WaveShowWarningToast(str_error_adding_demand);
  End;
end;

function TSwissLifeDemandsAddFrm.Init_modification_swisslife_demand(
  DemandId: longint): boolean;
begin
  Try
    Insurance_proposals.Close;
    Insurance_proposals.SQL.Clear;
    Insurance_proposals.SQL.Add('Select * from insurance_proposals where id=' + IntToStr(DemandId));
    Insurance_proposals.Open;
    Insurance_proposals.Edit;
    Insurance_proposals.FieldByName('Date_modified').AsDateTime := Now;

    //Check the status to allow saving or not
    if (Insurance_proposals.FieldByName('Proposal_status').AsInteger = 1) or
       (Insurance_proposals.FieldByName('Proposal_status').AsInteger = 2) or
       (Insurance_proposals.FieldByName('Proposal_status').AsInteger = 3) or
       (Insurance_proposals.FieldByName('Proposal_status').AsInteger = 4) or
       (Insurance_proposals.FieldByName('Proposal_status').AsInteger = 99)
    then begin
           BtnSave.Enabled   := False;
           BtnSave.Visible   := False;
           BtnSend.Enabled   := False;
           BtnSend.Visible   := False;
           BtnCancel.Caption := str_close;
         end;

    //Set the fields
    if Insurance_proposals.FieldByName('Insured_appelation').asInteger = 0
    then CheckBoxAppelationMale.Checked := True
    else if Insurance_proposals.FieldByName('Insured_appelation').asInteger = 1
         then CheckBoxAppelationFemale.Checked := True;

    if Insurance_proposals.FieldByName('Insured_sex').asInteger = 1
    then CheckBoxSexFemaleInsuredPerson.Checked := True
    else if Insurance_proposals.FieldByName('Insured_sex').asInteger = 0
         then CheckboxSexMaleInsuredPerson.Checked := True;

    if not Insurance_proposals.FieldByName('Insured_civilstate').isNull
    then ComboCivilstateInsuredPerson.ItemIndex := Insurance_proposals.FieldByName('Insured_civilstate').AsInteger;

    if Insurance_proposals.FieldByName('Insured_smoker').asInteger = 1
    then CheckBoxInsuredSmokerYes.Checked := True
    else if Insurance_proposals.FieldByName('Insured_smoker').asInteger = 0
         then CheckBoxInsuredSmokerNo.Checked := True;

    if Insurance_proposals.FieldByName('Correspondance_other').asInteger = 0
    then CheckBoxCorrespondanceAddressInsuredPerson.Checked := True
    else if Insurance_proposals.FieldByName('Correspondance_other').asInteger = 1
         then CheckBoxCorrespondanceOtherPerson.Checked := True;

    if Insurance_proposals.FieldByName('Correspondance_appelation').AsInteger = 0
    then CheckBoxCorrespondenceMale.Checked := True
    else if Insurance_proposals.FieldByName('Correspondance_appelation').AsInteger = 1
         then CheckBoxCorrespondanceFemale.Checked := True
         else if Insurance_proposals.FieldByName('Correspondance_appelation').asInteger = 2
              then CheckBoxCorrespondanceCompany.Checked := True;

    if Insurance_proposals.FieldByName('Premium_payment_frequency').asInteger = 0
    then CheckBoxFrequencyMonthly.Checked := True
    else if Insurance_proposals.FieldByName('Premium_payment_frequency').AsInteger = 1
         then CheckBoxFrequencyTrimesterly.Checked := True
         else if Insurance_proposals.FieldByName('Premium_payment_frequency').AsInteger = 2
              then CheckBoxFrequencyYearly.Checked := True;

    if Insurance_proposals.FieldByName('Beneficiary_standard_clause').AsInteger = 1
    then CheckBoxStandardClause.Checked := True;

    if Insurance_proposals.FieldByName('Beneficiary_other_clause').asInteger = 1
    then CheckBoxOtherClause.Checked := True;

    if Insurance_proposals.FieldByName('Benef1_appelation').asInteger = 0
    then CheckBoxAppelationManBenef1.Checked := True
    else if Insurance_proposals.FieldByName('Benef1_appelation').AsInteger = 1
         then CheckBoxAppelationWomenBenef1.Checked := True;

    if Insurance_proposals.FieldByName('Benef1_sex').AsInteger = 0
    then CheckBoxBenef1SexMale.Checked := True
    else if Insurance_proposals.FieldByName('Benef1_sex').AsInteger = 1
         then CheckBoxBenef1SexFemale.Checked := True;

    if Insurance_proposals.FieldByName('Benef2_appelation').asInteger = 0
    then CheckBoxAppelationManBenef2.Checked := True
    else if Insurance_proposals.FieldByName('Benef2_appelation').AsInteger = 1
         then CheckBoxAppelationWomenBenef2.Checked := True;

    if Insurance_proposals.FieldByName('Benef2_sex').AsInteger = 0
    then CheckBoxBenef2SexMale.Checked := True
    else if Insurance_proposals.FieldByName('Benef2_sex').AsInteger = 1
         then CheckBoxBenef2SexFemale.Checked := True;

    if Insurance_proposals.FieldByName('Benef3_appelation').asInteger = 0
    then CheckBoxAppelationManBenef3.Checked := True
    else if Insurance_proposals.FieldByName('Benef3_appelation').AsInteger = 1
         then CheckBoxAppelationWomenBenef3.Checked := True;

    if Insurance_proposals.FieldByName('Benef3_sex').AsInteger = 0
    then CheckBoxBenef3SexMale.Checked := True
    else if Insurance_proposals.FieldByName('Benef3_sex').AsInteger = 1
         then CheckBoxBenef3SexFemale.Checked := True;

    if Insurance_proposals.FieldByName('Benef4_appelation').asInteger = 0
    then CheckBoxAppelationManBenef4.Checked := True
    else if Insurance_proposals.FieldByName('Benef4_appelation').AsInteger = 1
         then CheckBoxAppelationWomenBenef4.Checked := True;

    if Insurance_proposals.FieldByName('Benef4_sex').AsInteger = 0
    then CheckBoxBenef4SexMale.Checked := True
    else if Insurance_proposals.FieldByName('Benef4_sex').AsInteger = 1
         then CheckBoxBenef4SexFemale.Checked := True;

    if Insurance_proposals.FieldByName('Sepa_domiciliation_day').AsInteger = 1
    then CheckBoxDomiciliationFifth.Checked := True
    else CheckBoxDomiciliationFifth.Checked := False;

    //Language
    if not Insurance_proposals.FieldByName('Insured_language').isNull
    then ComboboxLanguage.ItemIndex := Insurance_proposals.FieldByName('Insured_language').AsInteger;

    //Insured Capital
    if Insurance_proposals.FieldByName('Insured_capital').asCurrency = 0
    then ComboInsuredCapital.ItemIndex := -1
    else ComboInsuredCapital.ItemIndex :=
         Trunc((Insurance_proposals.FieldByName('Insured_capital').AsCurrency - 5000) / 1000);

    EditNationalInsuranceNumber.SetFocus;
    Check_color_documents;
    Result := True;
  Except
    Result := False;
    Insurance_proposals.Cancel;
    Insurance_proposals.Close;
    MainForm.WaveShowWarningToast(str_demand_in_use);
  End;
end;

procedure TSwissLifeDemandsAddFrm.LblUpLoadDoc1Click(Sender: TObject);
var DocumentName: string;
    SQL:          string;
    Destination:  string;
begin
  DocumentName := 'Insurance_linked_document' + IntToStr((Sender as TUniLabel).Tag);
  if not Insurance_proposals.FieldByName(DocumentName).isNull
  then begin
         UniMainModule.QWork.Close;
         UniMainModule.QWork.SQL.Clear;
         SQL := 'Select id, original_name, destination_name, folder, subfolder from files_archive ' +
                'where id=' + Insurance_proposals.FieldByName(DocumentName).AsString;
         UniMainModule.QWork.SQL.Add(SQL);
         UniMainModule.QWork.Open;
         if UniMainModule.QWork.Recordcount = 1
         then begin
                Destination := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
                if Trim(UniMainModule.QWork.FieldByName('Folder').AsString) <> ''
                then Destination := Destination + UniMainModule.QWork.FieldByName('Folder').AsString + '\';
                if Trim(UniMainModule.QWork.FieldByName('SubFolder').AsString) <> ''
                then Destination := Destination + UniMainModule.QWork.FieldByName('SubFolder').AsString + '\';
                Destination := Destination + UniMainModule.QWork.FieldByName('Destination_Name').AsString;
                UniSession.SendFile(Destination, Utils.EscapeIllegalChars(UniMainModule.QWork.fieldbyname('Original_Name').asString));
                UniMainModule.QWork.Close;
              end
         else UniMainModule.QWork.Close;
       end;
end;

function TSwissLifeDemandsAddFrm.Save_demand: boolean;
begin
  //Set the fields
  if CheckBoxAppelationMale.Checked
  then Insurance_proposals.FieldByName('Insured_appelation').asInteger := 0
  else if CheckboxAppelationFemale.Checked
       then Insurance_proposals.FieldByName('Insured_appelation').AsInteger := 1;

  if CheckBoxSexFemaleInsuredPerson.Checked
  then Insurance_proposals.FieldByName('Insured_sex').asInteger := 1
  else if CheckboxSexMaleInsuredPerson.Checked
       then Insurance_proposals.FieldByName('Insured_sex').AsInteger := 0;

  Insurance_proposals.FieldByName('Insured_civilstate').AsInteger := ComboCivilstateInsuredPerson.ItemIndex;

  if CheckBoxInsuredSmokerYes.Checked
  then Insurance_proposals.FieldByName('Insured_smoker').asInteger := 1
  else if CheckBoxInsuredSmokerNo.Checked
       then Insurance_proposals.FieldByName('Insured_smoker').asInteger := 0;

  if CheckBoxCorrespondanceAddressInsuredPerson.Checked
  then Insurance_proposals.FieldByName('Correspondance_other').AsInteger := 0
  else if CheckBoxCorrespondanceOtherPerson.Checked
       then Insurance_proposals.FieldByName('Correspondance_other').AsInteger := 1;

  if CheckBoxCorrespondenceMale.Checked
  then Insurance_proposals.FieldByName('Correspondance_appelation').AsInteger := 0
  else if CheckBoxCorrespondanceFemale.Checked
       then Insurance_proposals.FieldByName('Correspondance_appelation').AsInteger := 1
       else if CheckBoxCorrespondanceCompany.Checked
            then Insurance_proposals.FieldByName('Correspondance_appelation').AsInteger := 2;

  if CheckBoxFrequencyMonthly.Checked
  then Insurance_proposals.FieldByName('Premium_payment_frequency').asInteger := 0
  else if CheckBoxFrequencyTrimesterly.Checked
       then Insurance_proposals.FieldByName('Premium_payment_frequency').AsInteger := 1
       else if CheckBoxFrequencyYearly.Checked
            then Insurance_proposals.FieldByName('Premium_payment_frequency').AsInteger := 2;

  if CheckBoxStandardClause.Checked
  then Insurance_proposals.FieldByName('Beneficiary_standard_clause').AsInteger := 1
  else Insurance_proposals.FieldByName('Beneficiary_standard_clause').AsInteger := 0;

  if CheckBoxOtherClause.Checked
  then Insurance_proposals.FieldByName('Beneficiary_other_clause').asInteger := 1
  else Insurance_proposals.FieldByName('Beneficiary_other_clause').AsInteger := 0;

  if CheckBoxAppelationManBenef1.Checked
  then Insurance_proposals.FieldByName('Benef1_appelation').asInteger := 0
  else if CheckBoxAppelationWomenBenef1.Checked
       then Insurance_proposals.FieldByName('Benef1_appelation').AsInteger := 1;

  if CheckBoxBenef1SexMale.Checked
  then Insurance_proposals.FieldByName('Benef1_sex').AsInteger := 0
  else if CheckBoxBenef1SexFemale.Checked
       then Insurance_proposals.FieldByName('Benef1_sex').AsInteger := 1;

  if CheckBoxAppelationManBenef2.Checked
  then Insurance_proposals.FieldByName('Benef2_appelation').asInteger := 0
  else if CheckBoxAppelationWomenBenef2.Checked
       then Insurance_proposals.FieldByName('Benef2_appelation').AsInteger := 1;

  if CheckBoxBenef2SexMale.Checked
  then Insurance_proposals.FieldByName('Benef2_sex').AsInteger := 0
  else if CheckBoxBenef2SexFemale.Checked
       then Insurance_proposals.FieldByName('Benef2_sex').AsInteger := 1;

  if CheckBoxAppelationManBenef3.Checked
  then Insurance_proposals.FieldByName('Benef3_appelation').asInteger := 0
  else if CheckBoxAppelationWomenBenef3.Checked
       then Insurance_proposals.FieldByName('Benef3_appelation').AsInteger := 1;

  if CheckBoxBenef3SexMale.Checked
  then Insurance_proposals.FieldByName('Benef3_sex').AsInteger := 0
  else if CheckBoxBenef3SexFemale.Checked
       then Insurance_proposals.FieldByName('Benef3_sex').AsInteger := 1;

  if CheckBoxAppelationManBenef4.Checked
  then Insurance_proposals.FieldByName('Benef4_appelation').asInteger := 0
  else if CheckBoxAppelationWomenBenef4.Checked
       then Insurance_proposals.FieldByName('Benef4_appelation').AsInteger := 1;

  if CheckBoxBenef4SexMale.Checked
  then Insurance_proposals.FieldByName('Benef4_sex').AsInteger := 0
  else if CheckBoxBenef4SexFemale.Checked
       then Insurance_proposals.FieldByName('Benef4_sex').AsInteger := 1;

  if CheckBoxDomiciliationFifth.Checked
  then Insurance_proposals.FieldByName('Sepa_domiciliation_day').AsInteger := 1
  else Insurance_proposals.FieldByName('Sepa_domiciliation_day').AsInteger := 0;

  //Language
  Insurance_proposals.FieldByName('Insured_language').AsInteger            := ComboboxLanguage.ItemIndex;

  //Insured Capital
  if ComboInsuredCapital.ItemIndex <> -1
  then Insurance_proposals.FieldByName('Insured_capital').AsCurrency :=
       5000 + (ComboInsuredCapital.Itemindex * 1000)
  else Insurance_proposals.FieldByName('Insured_capital').AsCurrency := 0;

  Try
    Insurance_proposals.Post;
    UniMainModule.Result_dbAction := Insurance_proposals.FieldByName('Id').AsInteger;
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Commit;
    Result := True;
  Except
    if UniMainModule.UpdTrans.Active
    then UniMainModule.UpdTrans.Rollback;
    Result := False;
  End;
end;

procedure TSwissLifeDemandsAddFrm.UpdateStrings;
begin
  str_proof_of_residence_required                      := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_proof_of_residence_required' (* 'Bewijs van woonst ontbreekt' *) );
  str_close                                            := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_close' (* 'Sluiten' *) );
  str_iban_number_not_correct                          := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_iban_number_not_correct' (* 'Het iban nummer is niet correct' *) );
  str_enddate_not_valid                                := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_enddate_not_valid' (* 'De einddatum is niet geldig' *) );
  str_startdate_not_valid                              := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_startdate_not_valid' (* 'De begindatum is niet geldig' *) );
  Str_idcard_expired                                   := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strStr_idcard_expired' (* 'De geldigheid van de identiteitskaart is verlopen' *) );
  str_nis_number_not_correct                           := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_nis_number_not_correct' (* 'Het rijksregisternummer is niet geldig' *) );
  str_confirm_remove_document                          := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_confirm_remove_document' (* 'Verwijderen document?' *) );
  str_premium_payment_required                         := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_premium_payment_required' (* 'De premiebetaling is een verplichte ingave' *) );
  str_birthdate_required                               := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_birthdate_required' (* 'De geboortedatum is een verplichte ingave' *) );
  str_smoker_required                                  := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_smoker_required' (* 'Roker is een verplichte ingave' *) );
  str_insured_capital_required                         := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_insured_capital_required' (* 'Verzekerd kapitaal is een verplichte ingave' *) );
  str_engine_missing                                   := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_engine_missing' (* 'Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar' *) );
  str_weight_required                                  := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_weight_required' (* 'Het gewicht is een verplichte ingave' *) );
  str_length_required                                  := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_length_required' (* 'De lengte is een verplichte ingave' *) );
  str_copy_id_card_insured_required                    := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_copy_id_card_insured_required' (* 'Kopie ID kaart verzekeringsnemer ontbreekt' *) );
  str_sepa_required                                    := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_sepa_required' (* 'Het sepa formulier ontbreekt' *) );
  str_agreement_insured_required                       := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_agreement_insured_required' (* 'Het document akkoord van de verzekerde ontbreekt' *) );
  str_due_dilligence_required                          := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_due_dilligence_required' (* 'Het document due dilligence ontbreekt' *) );
  str_medical_checklist_required                       := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_medical_checklist_required' (* 'De medische vragenlijst ontbreekt' *) );
  str_general_conditions_required                      := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_general_conditions_required' (* 'Het document algemene voorwaarden ontbreekt' *) );
  str_proposition_required                             := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_proposition_required' (* 'Het voorstel ontbreekt' *) );
  str_startdate_contract_not_valid                     := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_startdate_contract_not_valid' (* 'De ingangsdatum van het contract is niet geldig' *) );
  str_incasso_fifth_required                           := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_incasso_fifth_required' (* 'Incassodatum op de 5de werkdag is een verplichte ingave' *) );
  str_iban_number_required                             := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_iban_number_required' (* 'Het IBAN-nummer is een verplichte ingave' *) );
  str_contactdata_beneficiary_required                 := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_contactdata_beneficiary_required' (* 'De contactgegevens van de begunstigde clause zijn een verplichte ingave' *) );
  str_clause_beneficiary_required                      := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_clause_beneficiary_required' (* 'De clausule begunstigde is een verplichte ingave' *) );
  str_frequence_payments_required                      := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_frequence_payments_required' (* 'De frequentie van premiebetalingen is een verplichte ingave' *) );
  str_enddate_contract_required                        := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_enddate_contract_required' (* 'De vervaldatum van het contract is een verplichte ingave' *) );
  str_startdate_contract_required                      := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_startdate_contract_required' (* 'De ingangsdatum van het contract is een verplichte ingave' *) );
  str_base_capital_required                            := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_base_capital_required' (* 'Het basiskapitaal is een verplichte ingave' *) );
  str_country_required_postal_adres                    := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_country_required_postal_adres' (* 'Het land van het postadres is verplicht' *) );
  str_city_required_postal_adres                       := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_city_required_postal_adres' (* 'De stad van het postadres is verplicht' *) );
  str_postal_code_required_postal_adres                := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_postal_code_required_postal_adres' (* 'De postcode van het postadres is verplicht' *) );
  str_streetnumber_required_postal_addres              := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_streetnumber_required_postal_addres' (* 'Het straatnummer van het postadres is verplicht' *) );
  str_address_required_postal_address                  := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_address_required_postal_address' (* 'De straat van het postadres is verplicht' *) );
  str_name_firstname_company_required_postal_address   := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_name_firstname_company_required_postal_address' (* 'De naam, voornaam of firmanaam is verplicht ter hoogte van het postadres' *) );
  str_type_third_party_required                        := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_type_third_party_required' (* 'Het type derde partij is verplicht ter hoogte van het postadres' *) );
  str_choice_postal_adres_required                     := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_choice_postal_adres_required' (* 'De keuze van het postadres is een verplichte ingave' *) );
  str_smoke_behavior_required                          := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_smoke_behavior_required' (* 'Rookgedrag is een verplichte ingave' *) );
  str_email_required                                   := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_email_required' (* 'E-mail adres is een verplichte ingave' *) );
  str_phone_or_mobile_required                         := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_phone_or_mobile_required' (* 'Telefoon of gsm is een verplichte ingave' *) );
  str_enddate_idcard_required                          := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_enddate_idcard_required' (* 'De einddatum van geldigheid van de identiteitskaart is een verplichte ingave' *) );
  str_idcardNumberRequired                             := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_idcardNumberRequired' (* 'Nummer identiteitskaart is een verplichte ingave' *) );
  str_professional_activity_required                   := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_professional_activity_required' (* 'De professionele activiteit is een verplichte ingave' *) );
  str_civil_state_required                             := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_civil_state_required' (* 'De burgerlijke staat is een verplichte ingave' *) );
  str_sex_required                                     := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_sex_required' (* 'Het geslacht is een verplichte ingave' *) );
  str_nationality_required                             := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_nationality_required' (* 'De nationaliteit is een verplichte ingave' *) );
  str_birthplace_required                              := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_birthplace_required' (* 'De geboorteplaats is een verplichte ingave' *) );
  str_date_of_birth_required                           := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_date_of_birth_required' (* 'De geboortedatum is een verplichte ingave' *) );
  str_city_required                                    := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_city_required' (* 'De stad is een verplichte ingave' *) );
  str_postalcode_required                              := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_postalcode_required' (* 'De postcode is een verplichte ingave' *) );
  str_streetnumber_required                            := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_streetnumber_required' (* 'Het straatnummer is een verplichte ingave' *) );
  str_street_required                                  := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_street_required' (* 'De straat is een verplichte ingave' *) );
  str_firstname_required                               := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_firstname_required' (* 'De voornaam is een verplichte ingave' *) );
  str_name_required                                    := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_name_required' (* 'De naam is een verplichte ingave' *) );
  str_title_required                                   := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_title_required' (* 'De titel is een verplichte ingave' *) );
  str_subscriptionnumber_required                      := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_subscriptionnumber_required' (* 'Het abonnementsaanvraagnummer is een verplichte ingave' *) );
  str_demand_in_use                                    := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_demand_in_use' (* 'De aanvraag is in gebruik, deze kan momenteel niet aangepast worden' *) );
  str_error_adding_demand                              := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_error_adding_demand' (* 'Fout bij het toevoegen van de aanvraag' *) );
  str_document_already_present                         := LinkedLanguageSwissLifeDemandsAdd.GetTextOrDefault('strstr_document_already_present' (* 'Er is reeds een document aanwezig.' *) );
end;

function TSwissLifeDemandsAddFrm.Validate_demand: boolean;
begin
  //Check if all fields are filled in
  if Trim(Insurance_proposals.FieldByName('Nis_number').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_subscriptionnumber_required);
         Result := False;
         Exit;
       end;

  //Check if the nis number is correct
  if Utils.Check_nis_number(Insurance_proposals.FieldByName('Nis_number').asString) = False
  then begin
         MainForm.WaveShowWarningToast(str_nis_number_not_correct);
         Result := False;
         Exit;
       end;

  //Format the nis number
  Insurance_proposals.FieldByName('Nis_number').AsString :=
  Utils.Format_nis_number(Insurance_proposals.FieldByName('Nis_number').asString);

  if (CheckBoxAppelationMale.Checked = False) and (CheckBoxAppelationFemale.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_title_required);
         Result := False;
         Exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Insured_name').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_name_required);
         Result := False;
         Exit;
       end;

  //Capitalize the name
  Insurance_proposals.FieldByName('Insured_name').AsString :=
  Utils.Capitalize_string(Insurance_proposals.FieldByName('Insured_name').asString);

  if Trim(Insurance_proposals.FieldByName('Insured_firstname').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_firstname_required);
         Result := False;
         Exit;
       end;

  //Capitalize the first letter of the name
  Insurance_proposals.FieldByName('Insured_firstname').asString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_firstname').AsString);

  if Trim(Insurance_proposals.FieldByName('Insured_street').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_street_required);
         Result := False;
         Exit;
       end;

  //Capitalize the first letter of the street
  Insurance_proposals.FieldByName('Insured_street').AsString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_street').asString);

  if Trim(Insurance_proposals.FieldByName('Insured_street_number').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_streetnumber_required);
         Result := False;
         Exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Insured_postalcode').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_postalcode_required);
         Result := False;
         Exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Insured_city').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_city_required);
         Result := False;
         Exit;
       end;

  //Capitalize the first letter of the city
  Insurance_proposals.FieldByName('Insured_city').asString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_city').asString);

  if Insurance_proposals.FieldByName('Insured_dateofbirth').IsNull
  then begin
         MainForm.WaveShowWarningToast(str_date_of_birth_required);
         Result := False;
         Exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Insured_birthplace').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_birthplace_required);
         Result := False;
         Exit;
       end;

  //Capitalize the first letter of the birthplace
  Insurance_proposals.FieldByName('Insured_birthplace').AsString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_birthplace').asString);

  if Trim(Insurance_proposals.FieldByName('Insured_nationality').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_nationality_required);
         Result := False;
         Exit;
       end;

  //Capitalize the nationality
  Insurance_proposals.FieldByName('Insured_nationality').AsString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_nationality').AsString);

  if (CheckBoxSexFemaleInsuredPerson.Checked = False) and (CheckBoxSexMaleInsuredPerson.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_sex_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_height').isNull
  then begin
         MainForm.WaveShowWarningToast(str_length_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_weight').isNull
  then begin
         MainForm.WaveShowWarningToast(str_weight_required);
         Result := False;
         Exit;
       end;

  if ComboCivilStateInsuredPerson.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_civil_state_required);
         Result := False;
         exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Insured_professional_activity').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_professional_activity_required);
         Result := False;
         Exit;
       end;

  //Capitalize the first letter of the activity
  Insurance_proposals.FieldByName('Insured_professional_activity').AsString :=
  Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Insured_professional_activity').asString);

  if Trim(Insurance_proposals.FieldByName('Insured_identitycard_number').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_idcardNumberRequired);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_identitycard_val_until').isNull
  then begin
         MainForm.WaveShowWarningToast(Str_enddate_idcard_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_identitycard_val_until').asDateTime < Now
  then begin
         MainForm.WaveShowWarningToast(Str_idcard_expired);
         Result := False;
         Exit;
       end;


  if (Trim(Insurance_proposals.FieldByName('Insured_phone').asString) = '') and
     (Trim(Insurance_proposals.FieldByName('Insured_mobile').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast(str_phone_or_mobile_required);
         Result := False;
         Exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Insured_phone').asString) <> ''
  then Insurance_proposals.FieldByName('Insured_phone').AsString :=
       Utils.Format_belgium_phone(Insurance_proposals.FieldByName('Insured_phone').AsString);

  if Trim(Insurance_proposals.FieldByName('Insured_mobile').asString) <> ''
  then Insurance_proposals.FieldByName('Insured_mobile').AsString :=
       Utils.Format_belgium_phone(Insurance_proposals.FieldByName('Insured_mobile').asString);

  if Trim(Insurance_proposals.FieldByName('Insured_email').asString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_email_required);
         Result := False;
         Exit;
       end;

  if (CheckBoxInsuredSmokerYes.Checked = False) and (CheckBoxInsuredSmokerNo.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_smoke_behavior_required);
         Result := False;
         Exit;
       end;

  if (CheckBoxCorrespondanceAddressInsuredPerson.Checked = False) and
     (CheckBoxCorrespondanceOtherPerson.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_choice_postal_adres_required);
         Result := False;
         Exit;
       end;

  if CheckBoxCorrespondanceOtherPerson.Checked
  then begin
         if (CheckBoxCorrespondenceMale.Checked = False)     and
            (CheckBoxCorrespondanceFemale.Checked = False)   and
            (CheckBoxCorrespondanceCompany.Checked = False)
         then begin
                MainForm.WaveShowWarningToast(str_type_third_party_required);
                Result := False;
                Exit;
              end;

         if Trim(Insurance_proposals.FieldByName('Correspondance_name').asString) = ''
         then begin
                MainForm.WaveShowWarningToast(str_name_firstname_company_required_postal_address);
                Result := False;
                exit;
              end;

         Insurance_proposals.FieldByName('Correspondance_name').AsString :=
         Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Correspondance_name').asString);

        if Trim(Insurance_proposals.FieldByName('Correspondance_street').AsString) = ''
        then begin
               MainForm.WaveShowWarningToast(str_address_required_postal_address);
               Result := False;
               Exit;
             end;

        Insurance_proposals.FieldByName('Correspondance_street').asString :=
        Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Correspondance_street').asString);

        if Trim(Insurance_proposals.FieldByName('Correspondance_street_number').AsString) = ''
        then begin
               MainForm.WaveShowWarningToast(str_streetnumber_required_postal_addres);
               Result := False;
               Exit;
             end;

        Insurance_proposals.FieldByName('Correspondance_street_number').AsString :=
        Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Correspondance_street_number').asString);

        if Trim(Insurance_proposals.FieldByName('Correspondance_postalcode').asString) = ''
        then begin
               MainForm.WaveShowWarningToast(str_postal_code_required_postal_adres);
               Result := False;
               Exit;
             end;

        if Trim(Insurance_proposals.FieldByName('Correspondance_city').AsString) = ''
        then begin
               MainForm.WaveShowWarningToast(str_city_required_postal_adres);
               Result := False;
               Exit;
             end;

        Insurance_proposals.FieldByName('Correspondance_city').asString :=
        Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Correspondance_city').asString);

        if Trim(Insurance_proposals.FieldByName('Correspondance_country').AsString) = ''
        then begin
               MainForm.WaveShowWarningToast(str_country_required_postal_adres);
               Result := False;
               exit;
             end;

        Insurance_proposals.FieldByName('Correspondance_country').asString :=
        Utils.Capitalize_first_letter(Insurance_proposals.FieldByName('Correspondance_country').asString);
       end;

  if ComboInsuredCapital.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_base_capital_required);
         Result := False;
         Exit;
       end;

  if (Insurance_proposals.FieldByName('Start_date_contract').isNull) or
     (Insurance_proposals.FieldByName('Start_date_contract').asDateTime = 0)
  then begin
         MainForm.WaveShowWarningToast(str_startdate_contract_required);
         Result := False;
         Exit;
       end;

//  if Insurance_proposals.FieldByName('Start_date_contract').asDateTime <= Now
//  then begin
//         MainForm.WaveShowWarningToast(str_startdate_not_valid);
//         Result := False;
//         Exit;
//       end;


  if (Insurance_proposals.FieldByName('End_date_contract').isNull) or
     (Insurance_proposals.FieldByName('End_date_contract').asDateTime = 0)
  then begin
         MainForm.WaveShowWarningToast(str_enddate_contract_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('End_date_contract').asDateTime <= Now
  then begin
         MainForm.WaveShowWarningToast(str_enddate_not_valid);
         Result := False;
         Exit;
       end;


  if (CheckBoxFrequencyMonthly.Checked      = False) and
     (CheckBoxFrequencyTrimesterly.Checked  = False) and
     (CheckBoxFrequencyYearly.Checked       = False)
  then begin
         MainForm.WaveShowWarningToast(str_frequence_payments_required);
         Result := False;
         Exit;
       end;

  if (CheckBoxStandardClause.Checked = False) and
     (CheckBoxOtherClause.Checked    = False)
  then begin
         MainForm.WaveShowWarningToast(str_clause_beneficiary_required);
         Result := False;
         Exit;
       end;

  if (CheckBoxBenefClauseIncludedInDeed.Checked) and
     (Trim(Insurance_proposals.FieldByName('Benef_clause_contact_data').AsString) = '')
  then begin
         MainForm.WaveShowWarningToast(str_contactdata_beneficiary_required);
         Result := False;
         exit;
       end;

  if Trim(Insurance_proposals.FieldByName('Sepa_iban_number').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_iban_number_required);
         Result := False;
         Exit;
       end;

  if TIBANUtils.IsValidIBAN(Insurance_proposals.FieldByName('Sepa_iban_number').AsString, nil) = False
  then begin
         MainForm.WaveShowWarningToast(str_iban_number_not_correct);
         Result := False;
         Exit;
       end;

  if CheckBoxDomiciliationFifth.Checked = False
  then begin
         MainForm.WaveShowWarningToast(str_incasso_fifth_required);
         Result := False;
         exit;
       end;

  if (Insurance_proposals.FieldByName('Start_date_contract').asDateTime < IncMonth(Now, -1)) or
     (Insurance_proposals.FieldByName('Start_date_contract').asDateTime > IncMonth(Now, 1))
  then begin
         MainForm.WaveShowWarningToast(str_startdate_contract_not_valid);
         Result := False;
         Exit;
       end;

  //Check the documents
  if Insurance_proposals.FieldByName('Insurance_linked_document1').isNull
  then begin
         MainForm.WaveShowWarningToast(str_proposition_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document2').isNull
  then begin
         MainForm.WaveShowWarningToast(str_general_conditions_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document3').isNull
  then begin
         MainForm.WaveShowWarningToast(str_medical_checklist_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document4').isNull
  then begin
         MainForm.WaveShowWarningToast(str_due_dilligence_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document5').isNull
  then begin
         MainForm.WaveShowWarningToast(str_agreement_insured_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document6').isNull
  then begin
         MainForm.WaveShowWarningToast(str_sepa_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document7').isNull
  then begin
         MainForm.WaveShowWarningToast(str_copy_id_card_insured_required);
         Result := False;
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insurance_linked_document8').isNull
  then begin
         MainForm.WaveShowWarningToast(str_proof_of_residence_required);
         Result := False;
         Exit;
       end;

  Result := True;
end;

procedure TSwissLifeDemandsAddFrm.BtnCalculateClick(Sender: TObject);
var xls:                 TXlsFile;
    cell:                TCellValue;
    WorkValue:           Currency;
    TotalTaxes:          Currency;
    EndDateContract:     TDateTime;
    LastDayExtraCapital: TDateTime;
begin
  //Clear the results
  Insurance_proposals.FieldByName('End_date_contract').Clear;
  Insurance_proposals.FieldByName('Premium_amount_gross').Clear;
  Insurance_proposals.FieldByName('Premium_amount_tax').Clear;
  Insurance_proposals.FieldByName('Premium_amount').Clear;
  Insurance_proposals.FieldByName('Premium_payment_years').Clear;
  Insurance_proposals.FieldByName('Age_at_inception').Clear;
  Insurance_proposals.FieldByName('Loading_bmi').Clear;
  Insurance_proposals.FieldByName('Last_day_extra_capital').Clear;
  Insurance_proposals.FieldByName('Gross_premium').Clear;
  Insurance_proposals.FieldByName('Taxes_monthly').Clear;
  Insurance_proposals.FieldByName('Insurance_premium_tax').Clear;


  if not FileExists('c:\DefaultTemplateArchive\SwissLifeCalculator4.xlsm')
  then begin
         MainForm.WaveShowWarningToast(str_engine_missing);
         Exit;
       end;

  //Validate the fields
  if ComboInsuredCapital.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_insured_capital_required);
         Exit;
       end;

  if (CheckBoxInsuredSmokerYes.Checked = False) and (CheckBoxInsuredSmokerNo.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_smoker_required);
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_dateOfBirth').IsNull
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_required);
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_height').isNull
  then begin
         MainForm.WaveShowWarningToast(str_length_required);
         Exit;
       end;

  if Insurance_proposals.FieldByName('Insured_weight').IsNull
  then begin
         MainForm.WaveShowWarningToast(str_weight_required);
         Exit;
       end;

  if (CheckBoxFrequencyMonthly.Checked = False)     and
     (CheckBoxFrequencyTrimesterly.Checked = False) and
     (CheckBoxFrequencyYearly.Checked = False)
  then begin
         MainForm.WaveShowWarningToast(str_premium_payment_required);
         Exit;
       end;

  xls := TXlsFile.Create('c:\DefaultTemplateArchive\SwissLifeCalculator4.xlsm');
  try
    xls.ActiveSheetByName := 'Premium Tool';
    xls.SetCellValue(5, 4, StrToCurr(ComboInsuredCapital.Text));
    if CheckBoxInsuredSmokerNo.Checked
    then xls.SetCellValue(6, 4, 'no')
    else if CheckBoxInsuredSmokerYes.Checked
         then xls.SetCellValue(6, 4, 'yes');
    xls.SetCellValue(7, 4, FormatDateTime('DD/MM/YYYY', Insurance_proposals.FieldByName('Insured_DateOfBirth').AsDateTime));
    xls.SetCellValue(8, 4, Insurance_proposals.FieldByName('Insured_height').asString);
    xls.SetCellValue(9, 4, Insurance_proposals.FieldByName('Insured_weight').AsString);
    if CheckBoxFrequencyMonthly.Checked
    then xls.SetCellValue(10, 4, 'Monthly')
    else if CheckBoxFrequencyTrimesterly.Checked
         then xls.SetCellValue(10, 4, 'Quarterly')
         else if CheckBoxFrequencyYearly.Checked
              then xls.SetCellValue(10, 4, 'Annually');
    xls.SetCellValue(11, 4, FormatDateTime('DD/MM/YYYY', Insurance_proposals.FieldByName('Start_date_contract').AsDateTime));
    xls.Recalc;

    //Enddate contract
    Try
      cell                                                                    := xls.GetCellValue(12, 4);
      EndDateContract                                                         := cell.ToDateTime(xls.OptionsDates1904);
      Insurance_proposals.FieldByName('End_date_contract').asDateTime         := EndDateContract;
    Except
      MainForm.WaveShowErrorToast('Fout bij het berekenen van de vervaldatum!');
    End;

    //Age at inception
    cell := xls.GetCellValue(13, 4);
    if cell.IsNumber
    then WorkValue                                                            :=  cell.AsNumber
    else try
           WorkValue                                                          := StrToInt(cell.ToString);
         except
           WorkValue                                                          := 0;
         end;
    Insurance_proposals.FieldByName('Age_at_inception').AsInteger             := Trunc(WorkValue);

    //Gross premium
    cell := xls.GetCellValue(16, 4);
    if cell.IsNumber
    then WorkValue                                                            := cell.AsNumber
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
         except
           WorkValue                                                          := 0;
         end;
    Insurance_proposals.FieldByName('Premium_amount_gross').AsCurrency        := Utils.RoundTo2dp(WorkValue);

    //Loading for bmi
    cell := xls.GetCellValue(17, 4);
    if cell.IsNumber
    then WorkValue                                                            := cell.AsNumber
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
         except
           WorkValue                                                          := 0;
         end;
    Insurance_proposals.FieldByName('Loading_bmi').AsCurrency                 := Utils.RoundTo2dp(WorkValue);

    //Insurance premium tax
    cell := xls.GetCellValue(18, 4);
    if cell.IsNumber
    then WorkValue                                                            := cell.AsNumber
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
         except
           WorkValue                                                          := 0;
         end;
    TotalTaxes                                                                := WorkValue;
    Insurance_proposals.FieldByName('Insurance_premium_tax').AsCurrency       := WorkValue;

    cell := xls.GetCellValue(19, 4);
    if cell.IsNumber
    then WorkValue                                                            := cell.AsNumber
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
           WorkValue                                                          := WorkValue;
         except
           WorkValue                                                          := 0;
         end;
    TotalTaxes                                                                := TotalTaxes + WorkValue;
    Insurance_proposals.FieldByName('Premium_amount_tax').AsCurrency          := Utils.RoundTo2dp(TotalTaxes);

    cell := xls.GetCellValue(20, 4);
    if cell.IsNumber
    then begin
           WorkValue                                                          := Utils.RoundTo2dp(cell.AsNumber);
           Insurance_proposals.FieldByName('Premium_amount').AsCurrency       := WorkValue;
         end
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
           WorkValue                                                          := Utils.RoundTo2dp(WorkValue);
           Insurance_proposals.FieldByName('Premium_amount').AsCurrency       := WorkValue;
         except
           Insurance_proposals.FieldByName('Premium_amount').Clear;
         end;

    Try
      cell := xls.GetCellValue(22, 4);
      Insurance_proposals.FieldByName('Premium_payment_years').AsInteger      := StrToInt(cell.ToString);
    except
      Insurance_proposals.FieldByName('Premium_payment_years').Clear;
    End;

    //Last date entitlement extra capital
    try
      cell                                                                    := xls.GetCellValue(24, 4);
      LastDayExtraCapital                                                     := cell.ToDateTime(xls.OptionsDates1904);
      Insurance_proposals.FieldByName('Last_day_extra_capital').AsDateTime    := LastDayExtraCapital;
    except
    end;

    //Gross premium - yearly
    cell := xls.GetCellValue(26, 4);
    if cell.IsNumber
    then begin
           WorkValue                                                          := Utils.RoundTo2dp(cell.AsNumber);
           Insurance_proposals.FieldByName('Gross_premium').AsCurrency        := WorkValue;
         end
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
           WorkValue                                                          := Utils.RoundTo2dp(WorkValue);
           Insurance_proposals.FieldByName('Gross_premium').AsCurrency        := WorkValue;
         except
           Insurance_proposals.FieldByName('Gross_premium').Clear;
         end;

    //Taxes monthly
    cell := xls.GetCellValue(30, 4);
    if cell.IsNumber
    then begin
           WorkValue                                                          := Utils.RoundTo2dp(cell.AsNumber);
           Insurance_proposals.FieldByName('Taxes_monthly').AsCurrency        := WorkValue;
         end
    else try
           WorkValue                                                          := StrToCurr(cell.ToString);
           WorkValue                                                          := Utils.RoundTo2dp(WorkValue);
           Insurance_proposals.FieldByName('Taxes_monthly').AsCurrency        := WorkValue;
         except
           Insurance_proposals.FieldByName('Taxes_monthly').Clear;
         end;
  finally
    xls.Free;
  end;
end;

procedure TSwissLifeDemandsAddFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

end.
