object SwissLifeDemandsAddFrm: TSwissLifeDemandsAddFrm
  Left = 0
  Top = 0
  ClientHeight = 645
  ClientWidth = 1312
  Caption = 'Voorstel Swiss Life Protect Belgium'
  Color = clWhite
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'border'
  Images = UniMainModule.ImageList
  ImageIndex = 11
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1312
    Height = 89
    Hint = ''
    ParentColor = False
    Color = clWhite
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerHeaderLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 513
      Height = 89
      Hint = ''
      ParentColor = False
      Color = clWhite
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object LblProposition: TUniLabel
        Left = 16
        Top = 8
        Width = 114
        Height = 39
        Hint = ''
        Caption = 'Voorstel'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -32
        TabOrder = 1
      end
      object LblSwissLifeProtectBelgium: TUniLabel
        Left = 16
        Top = 48
        Width = 277
        Height = 25
        Hint = ''
        Caption = 'Swiss Life Protect Belgium'
        ParentFont = False
        Font.Height = -21
        Font.Style = [fsBold]
        TabOrder = 2
      end
    end
    object ContainerHeaderRight: TUniContainerPanel
      Left = 1212
      Top = 0
      Width = 100
      Height = 89
      Hint = ''
      ParentColor = False
      Color = clWhite
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      LayoutConfig.Margin = '0 16 0 0'
      object ImageHeader: TUniImage
        Left = 0
        Top = 8
        Width = 100
        Height = 75
        Hint = ''
        Url = 'images/wv_swiss_life_logo.png'
      end
    end
  end
  object ContainerBody: TUniContainerPanel
    Left = 0
    Top = 89
    Width = 1312
    Height = 521
    Hint = ''
    ParentColor = False
    Color = clWhite
    Align = alClient
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'center'
    ExplicitHeight = 511
    object ScrollBoxProposal: TUniScrollBox
      Left = 0
      Top = 0
      Width = 968
      Height = 521
      Hint = ''
      Align = alClient
      TabOrder = 1
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '0 8 0 0'
      ExplicitHeight = 511
      ScrollHeight = 2760
      ScrollWidth = 889
      object LblTitleNationalinsuranceNumber: TUniLabel
        Left = 16
        Top = 16
        Width = 315
        Height = 13
        Hint = ''
        Caption = 'Abonnementsaanvraagnummer (rijksregisternummer):'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 0
      end
      object LblTitleIdentificationInsuredPerson: TUniLabel
        Left = 16
        Top = 64
        Width = 556
        Height = 19
        Hint = ''
        Caption = 
          'Identificatie van de verzekeringnemer (identiek aan de verzekerd' +
          'e)'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 2
      end
      object LblTitleInsuredPerson: TUniLabel
        Left = 16
        Top = 104
        Width = 28
        Height = 13
        Hint = ''
        Caption = 'Titel:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 3
      end
      object LblNameInsuredPerson: TUniLabel
        Left = 16
        Top = 136
        Width = 35
        Height = 13
        Hint = ''
        Caption = 'Naam:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 6
      end
      object LblFirstNameInsuredPerson: TUniLabel
        Left = 16
        Top = 168
        Width = 61
        Height = 13
        Hint = ''
        Caption = 'Voornaam:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 8
      end
      object LblStreetNumberBoxInsuredPerson: TUniLabel
        Left = 16
        Top = 200
        Width = 131
        Height = 13
        Hint = ''
        Caption = 'Straat / nummer / bus:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 10
      end
      object LblPostalCodeInsuredPerson: TUniLabel
        Left = 16
        Top = 232
        Width = 55
        Height = 13
        Hint = ''
        Caption = 'Postcode:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 14
      end
      object LblCityInsuredPerson: TUniLabel
        Left = 16
        Top = 264
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Stad:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 16
      end
      object LblCountryInsuredPerson: TUniLabel
        Left = 16
        Top = 296
        Width = 30
        Height = 13
        Hint = ''
        Caption = 'Land:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 18
      end
      object LblValueCountryInsuredPerson: TUniLabel
        Left = 368
        Top = 296
        Width = 34
        Height = 13
        Hint = ''
        Caption = 'Belgi'#235
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 19
      end
      object LblDateOfBirthInsuredPerson: TUniLabel
        Left = 16
        Top = 328
        Width = 93
        Height = 13
        Hint = ''
        Caption = 'Geboortedatum:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 20
      end
      object LblBirthPlaceInsuredPerson: TUniLabel
        Left = 16
        Top = 360
        Width = 91
        Height = 13
        Hint = ''
        Caption = 'Geboorteplaats:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 24
      end
      object LblSexInsuredPerson: TUniLabel
        Left = 16
        Top = 424
        Width = 52
        Height = 13
        Hint = ''
        Caption = 'Geslacht:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 28
      end
      object LblNationalityInsuredPerson: TUniLabel
        Left = 16
        Top = 392
        Width = 72
        Height = 13
        Hint = ''
        Caption = 'Nationaliteit:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 26
      end
      object LblCivilStateInsuredPerson: TUniLabel
        Left = 16
        Top = 520
        Width = 98
        Height = 13
        Hint = ''
        Caption = 'Burgerlijke staat:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 35
      end
      object LblProfessionalActivityInsuredPerson: TUniLabel
        Left = 16
        Top = 552
        Width = 133
        Height = 13
        Hint = ''
        Caption = 'Professionele activiteit:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 37
      end
      object LblIdentitycardnumberInsuredPerson: TUniLabel
        Left = 16
        Top = 584
        Width = 143
        Height = 13
        Hint = ''
        Caption = 'Nummer identiteitskaart:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 39
      end
      object LblIdentitycardValidUntilInsuredPerson: TUniLabel
        Left = 552
        Top = 584
        Width = 58
        Height = 13
        Hint = ''
        Caption = 'Geldig tot:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 41
      end
      object LblPhoneNumberInsuredPerson: TUniLabel
        Left = 16
        Top = 616
        Width = 52
        Height = 13
        Hint = ''
        Caption = 'Telefoon:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 43
      end
      object LblEmailInsuredPerson: TUniLabel
        Left = 16
        Top = 680
        Width = 73
        Height = 13
        Hint = ''
        Caption = 'E-mail adres:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 47
      end
      object LblInsuredSmoker: TUniLabel
        Left = 16
        Top = 712
        Width = 72
        Height = 13
        Hint = ''
        Caption = 'Rookgedrag:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 49
      end
      object LblTitleCapitalAndDuration: TUniLabel
        Left = 16
        Top = 1056
        Width = 290
        Height = 19
        Hint = ''
        Caption = 'Verzekerd kapitaal en contractduur'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 70
      end
      object LblAmountInsuredCapital: TUniLabel
        Left = 16
        Top = 1096
        Width = 78
        Height = 13
        Hint = ''
        Caption = 'Basiskapitaal:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 71
      end
      object LblStartDateContract: TUniLabel
        Left = 16
        Top = 1128
        Width = 183
        Height = 13
        Hint = ''
        Caption = 'Ingangsdatum van het contract:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 73
      end
      object LblEndDateContract: TUniLabel
        Left = 16
        Top = 1160
        Width = 173
        Height = 13
        Hint = ''
        Caption = 'Vervaldatum van het contract:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 75
      end
      object LblTitlePremiumPayments: TUniLabel
        Left = 16
        Top = 1208
        Width = 197
        Height = 19
        Hint = ''
        Caption = 'Betaling van de premies'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 77
      end
      object LblFrequenceOfPayment: TUniLabel
        Left = 16
        Top = 1248
        Width = 208
        Height = 13
        Hint = ''
        Caption = 'Frequentie van de premiebetalingen:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 78
      end
      object LblPremiumAmount: TUniLabel
        Left = 16
        Top = 1347
        Width = 369
        Height = 13
        Hint = ''
        Caption = 'Bedrag van de premie met inbegrip van belastingen en bijdragen:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 87
      end
      object LblPremiumDuration: TUniLabel
        Left = 16
        Top = 1379
        Width = 168
        Height = 13
        Hint = ''
        Caption = 'Duur in jaren betaling premie:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 89
      end
      object LblTitleClauseBeneficiaries: TUniLabel
        Left = 16
        Top = 1424
        Width = 174
        Height = 19
        Hint = ''
        Caption = 'Clausule begunstigde'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 91
      end
      object LblBeneficiary1: TUniLabel
        Left = 368
        Top = 1544
        Width = 70
        Height = 13
        Hint = ''
        Caption = 'Begunstigde'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 94
      end
      object LblBeneficiary2: TUniLabel
        Left = 584
        Top = 1544
        Width = 70
        Height = 13
        Hint = ''
        Caption = 'Begunstigde'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 95
      end
      object LblTitleBeneficiary1: TUniLabel
        Left = 16
        Top = 1576
        Width = 28
        Height = 13
        Hint = ''
        Caption = 'Titel:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 96
      end
      object LblAssignmentPercentageBenef1: TUniLabel
        Left = 16
        Top = 1608
        Width = 230
        Height = 13
        Hint = ''
        Caption = 'Volgorde van toewijzging en percentage:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 99
      end
      object LblBenef1Name: TUniLabel
        Left = 16
        Top = 1672
        Width = 122
        Height = 13
        Hint = ''
        Caption = 'Naam / sociale reden:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 104
      end
      object LblBenef1FirstName: TUniLabel
        Left = 16
        Top = 1704
        Width = 61
        Height = 13
        Hint = ''
        Caption = 'Voornaam:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 106
      end
      object LblBenef1StreetandNumber: TUniLabel
        Left = 16
        Top = 1736
        Width = 107
        Height = 13
        Hint = ''
        Caption = 'Straat en nummer:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 108
      end
      object LblBenef1PostalCode: TUniLabel
        Left = 16
        Top = 1768
        Width = 55
        Height = 13
        Hint = ''
        Caption = 'Postcode:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 110
      end
      object LblBenef1Location: TUniLabel
        Left = 16
        Top = 1800
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Stad:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 112
      end
      object LblBenef1Country: TUniLabel
        Left = 16
        Top = 1832
        Width = 30
        Height = 13
        Hint = ''
        Caption = 'Land:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 114
      end
      object LblBenef1DateOfBirth: TUniLabel
        Left = 16
        Top = 1864
        Width = 93
        Height = 13
        Hint = ''
        Caption = 'Geboortedatum:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 116
      end
      object LblBenef1BirthPlace: TUniLabel
        Left = 16
        Top = 1896
        Width = 91
        Height = 13
        Hint = ''
        Caption = 'Geboorteplaats:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 118
      end
      object LblSexBenef1: TUniLabel
        Left = 16
        Top = 1928
        Width = 52
        Height = 13
        Hint = ''
        Caption = 'Geslacht:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 120
      end
      object LblLinkInsuredBenef1: TUniLabel
        Left = 16
        Top = 1960
        Width = 183
        Height = 13
        Hint = ''
        Caption = 'Link met de verzekeringsnemer:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 123
      end
      object LblBenef1DivergentBen: TUniLabel
        Left = 16
        Top = 1992
        Width = 139
        Height = 13
        Hint = ''
        Caption = 'Afwijkende begunstigde:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 125
      end
      object LblTitleSepaIncasso: TUniLabel
        Left = 16
        Top = 2633
        Width = 219
        Height = 19
        Hint = ''
        Caption = 'Sepa automatische incasso'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 199
      end
      object LblIBan: TUniLabel
        Left = 16
        Top = 2681
        Width = 30
        Height = 13
        Hint = ''
        Caption = 'IBAN:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 200
      end
      object LblReferenceMandate: TUniLabel
        Left = 16
        Top = 2747
        Width = 117
        Height = 13
        Hint = ''
        Caption = 'Referentie mandaat:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 203
      end
      object EditNationalInsuranceNumber: TUniDBEdit
        Left = 368
        Top = 16
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'NIS_NUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 1
        SelectOnFocus = True
      end
      object EditNameInsuredPerson: TUniDBEdit
        Left = 368
        Top = 136
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_NAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 7
        SelectOnFocus = True
      end
      object EditFirstNameInsuredPerson: TUniDBEdit
        Left = 368
        Top = 168
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_FIRSTNAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 9
        SelectOnFocus = True
      end
      object EditStreetInsuredPerson: TUniDBEdit
        Left = 368
        Top = 200
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_STREET'
        DataSource = Ds_insurance_proposals
        TabOrder = 11
        SelectOnFocus = True
      end
      object EditPostalCodeInsuredPerson: TUniDBEdit
        Left = 368
        Top = 232
        Width = 153
        Height = 22
        Hint = ''
        DataField = 'INSURED_POSTALCODE'
        DataSource = Ds_insurance_proposals
        TabOrder = 15
        SelectOnFocus = True
      end
      object EditCityInsuredPerson: TUniDBEdit
        Left = 368
        Top = 264
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_CITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 17
        SelectOnFocus = True
      end
      object EditDateOfBirthInsuredPerson: TUniDBDateTimePicker
        Left = 368
        Top = 320
        Width = 153
        Hint = ''
        DataField = 'INSURED_DATEOFBIRTH'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 21
      end
      object EditBirthPlaceInsuredPerson: TUniDBEdit
        Left = 368
        Top = 352
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_BIRTHPLACE'
        DataSource = Ds_insurance_proposals
        TabOrder = 25
        SelectOnFocus = True
      end
      object EditNationalityInsuredPerson: TUniDBEdit
        Left = 368
        Top = 384
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_NATIONALITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 27
        SelectOnFocus = True
      end
      object ComboCivilstateInsuredPerson: TUniComboBox
        Left = 368
        Top = 520
        Width = 313
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Vrijgezel'
          'Gehuwd'
          'Weduwe'
          'Gescheiden'
          'Wettelijk samenwonend'
          'Feitelijk samenwonend')
        TabOrder = 36
        IconItems = <>
      end
      object EditProfessionalActivityInsuredPerson: TUniDBEdit
        Left = 368
        Top = 552
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_PROFESSIONAL_ACTIVITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 38
        SelectOnFocus = True
      end
      object EditIdentitycardnumberInsuredPerson: TUniDBEdit
        Left = 368
        Top = 584
        Width = 169
        Height = 22
        Hint = ''
        DataField = 'INSURED_IDENTITYCARD_NUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 40
        SelectOnFocus = True
      end
      object EditIdentitycardValidUntilInsuredPerson: TUniDBDateTimePicker
        Left = 688
        Top = 584
        Width = 153
        Hint = ''
        DataField = 'INSURED_IDENTITYCARD_VAL_UNTIL'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 42
      end
      object EditPhoneNumberInsuredPerson: TUniDBEdit
        Left = 368
        Top = 616
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_PHONE'
        DataSource = Ds_insurance_proposals
        TabOrder = 44
        SelectOnFocus = True
      end
      object EditEmailInsuredPerson: TUniDBEdit
        Left = 368
        Top = 680
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_EMAIL'
        DataSource = Ds_insurance_proposals
        TabOrder = 48
        SelectOnFocus = True
      end
      object EditStartDateContract: TUniDBDateTimePicker
        Left = 368
        Top = 1124
        Width = 153
        Hint = ''
        DataField = 'START_DATE_CONTRACT'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 74
      end
      object EditEndDateContract: TUniDBDateTimePicker
        Left = 368
        Top = 1164
        Width = 153
        Hint = ''
        DataField = 'END_DATE_CONTRACT'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        ReadOnly = True
        TabOrder = 76
        TabStop = False
      end
      object EditPremiumAmount: TUniDBFormattedNumberEdit
        Left = 592
        Top = 1347
        Width = 153
        Height = 22
        Hint = ''
        DataField = 'PREMIUM_AMOUNT'
        DataSource = Ds_insurance_proposals
        TabOrder = 88
        TabStop = False
        ReadOnly = True
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditPremiumPaymentYears: TUniDBNumberEdit
        Left = 592
        Top = 1379
        Width = 153
        Height = 22
        Hint = ''
        DataField = 'PREMIUM_PAYMENT_YEARS'
        DataSource = Ds_insurance_proposals
        TabOrder = 90
        TabStop = False
        ReadOnly = True
        SelectOnFocus = True
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
      object LblRankBenef1: TUniLabel
        Left = 368
        Top = 1608
        Width = 53
        Height = 13
        Hint = ''
        Caption = 'Volgorde:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 100
      end
      object LblPercentageBenef1: TUniLabel
        Left = 368
        Top = 1640
        Width = 68
        Height = 13
        Hint = ''
        Caption = 'Percentage:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 102
      end
      object EditRankBenef1: TUniDBNumberEdit
        Left = 456
        Top = 1606
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF1_RANK'
        DataSource = Ds_insurance_proposals
        TabOrder = 101
        SelectOnFocus = True
        DecimalSeparator = ','
      end
      object EditPercentageBenef1: TUniDBFormattedNumberEdit
        Left = 456
        Top = 1638
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF1_PERCENTAGE'
        DataSource = Ds_insurance_proposals
        TabOrder = 103
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object LblRankBenef2: TUniLabel
        Left = 584
        Top = 1608
        Width = 53
        Height = 13
        Hint = ''
        Caption = 'Volgorde:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 129
      end
      object EditRankBenef2: TUniDBNumberEdit
        Left = 672
        Top = 1606
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF2_RANK'
        DataSource = Ds_insurance_proposals
        TabOrder = 130
        SelectOnFocus = True
        DecimalSeparator = ','
      end
      object LblPercentageBenef2: TUniLabel
        Left = 584
        Top = 1640
        Width = 68
        Height = 13
        Hint = ''
        Caption = 'Percentage:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 131
      end
      object EditPercentageBenef2: TUniDBFormattedNumberEdit
        Left = 672
        Top = 1638
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF2_PERCENTAGE'
        DataSource = Ds_insurance_proposals
        TabOrder = 132
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditBenef1Name: TUniDBEdit
        Left = 368
        Top = 1670
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_NAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 105
        SelectOnFocus = True
      end
      object EditBenef2Name: TUniDBEdit
        Left = 584
        Top = 1670
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_NAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 133
        SelectOnFocus = True
      end
      object EditBenef1Firstname: TUniDBEdit
        Left = 368
        Top = 1702
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_FIRSTNAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 107
        SelectOnFocus = True
      end
      object EditBenef2Firstname: TUniDBEdit
        Left = 584
        Top = 1702
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_FIRSTNAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 134
        SelectOnFocus = True
      end
      object EditBenef1StreetandNumber: TUniDBEdit
        Left = 368
        Top = 1734
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_STREETANDNUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 109
        SelectOnFocus = True
      end
      object EditBenef2StreetandNumber: TUniDBEdit
        Left = 584
        Top = 1734
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_STREETANDNUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 135
        SelectOnFocus = True
      end
      object EditBenef1PostalCode: TUniDBEdit
        Left = 368
        Top = 1766
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_POSTALCODE'
        DataSource = Ds_insurance_proposals
        TabOrder = 111
        SelectOnFocus = True
      end
      object EditBenef2PostalCode: TUniDBEdit
        Left = 584
        Top = 1766
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_POSTALCODE'
        DataSource = Ds_insurance_proposals
        TabOrder = 136
        SelectOnFocus = True
      end
      object EditBenef1City: TUniDBEdit
        Left = 368
        Top = 1798
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_CITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 113
        SelectOnFocus = True
      end
      object EditBenef2City: TUniDBEdit
        Left = 584
        Top = 1798
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_CITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 137
        SelectOnFocus = True
      end
      object EditBenef1Country: TUniDBEdit
        Left = 368
        Top = 1830
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_COUNTRY'
        DataSource = Ds_insurance_proposals
        TabOrder = 115
        SelectOnFocus = True
      end
      object EditBenef2Country: TUniDBEdit
        Left = 584
        Top = 1830
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_COUNTRY'
        DataSource = Ds_insurance_proposals
        TabOrder = 138
        SelectOnFocus = True
      end
      object EditBenef1DateOfBirth: TUniDBDateTimePicker
        Left = 368
        Top = 1862
        Width = 177
        Hint = ''
        DataField = 'BENEF1_DATEOFBIRTH'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 117
      end
      object EditBenef2DateOfBirth: TUniDBDateTimePicker
        Left = 584
        Top = 1862
        Width = 177
        Hint = ''
        DataField = 'BENEF2_DATEOFBIRTH'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 139
      end
      object EditBenef1BirthPlace: TUniDBEdit
        Left = 368
        Top = 1894
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_BIRTHPLACE'
        DataSource = Ds_insurance_proposals
        TabOrder = 119
        SelectOnFocus = True
      end
      object EditBenef2BirthPlace: TUniDBEdit
        Left = 584
        Top = 1894
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_BIRTHPLACE'
        DataSource = Ds_insurance_proposals
        TabOrder = 140
        SelectOnFocus = True
      end
      object EditBenef1LinkInsured: TUniDBEdit
        Left = 368
        Top = 1958
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_LINK_INSURED_PERSON'
        DataSource = Ds_insurance_proposals
        TabOrder = 124
        SelectOnFocus = True
      end
      object EditBenef2LinkInsured: TUniDBEdit
        Left = 584
        Top = 1958
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_LINK_INSURED_PERSON'
        DataSource = Ds_insurance_proposals
        TabOrder = 143
        SelectOnFocus = True
      end
      object EditBenef1DivergentBeneficiary: TUniDBEdit
        Left = 368
        Top = 1990
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF1_DIVERGENT_BENEFICIARY'
        DataSource = Ds_insurance_proposals
        TabOrder = 126
        SelectOnFocus = True
      end
      object EditBenef2DivergentBeneficiary: TUniDBEdit
        Left = 584
        Top = 1990
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF2_DIVERGENT_BENEFICIARY'
        DataSource = Ds_insurance_proposals
        TabOrder = 144
        SelectOnFocus = True
      end
      object LblBeneficiary3: TUniLabel
        Left = 368
        Top = 2040
        Width = 70
        Height = 13
        Hint = ''
        Caption = 'Begunstigde'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 145
      end
      object LblBeneficiary4: TUniLabel
        Left = 584
        Top = 2040
        Width = 70
        Height = 13
        Hint = ''
        Caption = 'Begunstigde'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 146
      end
      object LblTitleBeneficiary2: TUniLabel
        Left = 16
        Top = 2072
        Width = 28
        Height = 13
        Hint = ''
        Caption = 'Titel:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 147
      end
      object LblAssignmentPercentageBenef2: TUniLabel
        Left = 16
        Top = 2104
        Width = 230
        Height = 13
        Hint = ''
        Caption = 'Volgorde van toewijzging en percentage:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 150
      end
      object LblRankBenef3: TUniLabel
        Left = 368
        Top = 2104
        Width = 53
        Height = 13
        Hint = ''
        Caption = 'Volgorde:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 151
      end
      object EditRankBenef3: TUniDBNumberEdit
        Left = 456
        Top = 2102
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF3_RANK'
        DataSource = Ds_insurance_proposals
        TabOrder = 152
        SelectOnFocus = True
        DecimalSeparator = ','
      end
      object LblPercentageBenef3: TUniLabel
        Left = 368
        Top = 2136
        Width = 68
        Height = 13
        Hint = ''
        Caption = 'Percentage:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 153
      end
      object EditPercentageBenef3: TUniDBFormattedNumberEdit
        Left = 456
        Top = 2134
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF3_PERCENTAGE'
        DataSource = Ds_insurance_proposals
        TabOrder = 154
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object LblRankBenef4: TUniLabel
        Left = 584
        Top = 2104
        Width = 53
        Height = 13
        Hint = ''
        Caption = 'Volgorde:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 180
      end
      object EditRankBenef4: TUniDBNumberEdit
        Left = 672
        Top = 2102
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF4_RANK'
        DataSource = Ds_insurance_proposals
        TabOrder = 181
        SelectOnFocus = True
        DecimalSeparator = ','
      end
      object LblPercentageBenef4: TUniLabel
        Left = 584
        Top = 2136
        Width = 68
        Height = 13
        Hint = ''
        Caption = 'Percentage:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 182
      end
      object EditPercentageBenef4: TUniDBFormattedNumberEdit
        Left = 672
        Top = 2134
        Width = 89
        Height = 22
        Hint = ''
        DataField = 'BENEF4_PERCENTAGE'
        DataSource = Ds_insurance_proposals
        TabOrder = 183
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object LblBenef3Name: TUniLabel
        Left = 16
        Top = 2168
        Width = 122
        Height = 13
        Hint = ''
        Caption = 'Naam / sociale reden:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 155
      end
      object EditBenef3Name: TUniDBEdit
        Left = 368
        Top = 2166
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_NAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 156
        SelectOnFocus = True
      end
      object EditBenef4Name: TUniDBEdit
        Left = 584
        Top = 2166
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_NAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 184
        SelectOnFocus = True
      end
      object LblBenef3FirstName: TUniLabel
        Left = 16
        Top = 2200
        Width = 61
        Height = 13
        Hint = ''
        Caption = 'Voornaam:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 157
      end
      object EditBenef3Firstname: TUniDBEdit
        Left = 368
        Top = 2198
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_FIRSTNAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 158
        SelectOnFocus = True
      end
      object EditBenef4Firstname: TUniDBEdit
        Left = 584
        Top = 2198
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_FIRSTNAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 185
        SelectOnFocus = True
      end
      object LblBenef2StreetandNumber: TUniLabel
        Left = 16
        Top = 2232
        Width = 107
        Height = 13
        Hint = ''
        Caption = 'Straat en nummer:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 159
      end
      object EditBenef3StreetandNumber: TUniDBEdit
        Left = 368
        Top = 2230
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_STREETANDNUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 160
        SelectOnFocus = True
      end
      object EditBenef4StreetandNumber: TUniDBEdit
        Left = 584
        Top = 2230
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_STREETANDNUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 186
        SelectOnFocus = True
      end
      object LblBenef2PostalCode: TUniLabel
        Left = 16
        Top = 2264
        Width = 55
        Height = 13
        Hint = ''
        Caption = 'Postcode:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 161
      end
      object EditBenef3PostalCode: TUniDBEdit
        Left = 368
        Top = 2262
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_POSTALCODE'
        DataSource = Ds_insurance_proposals
        TabOrder = 162
        SelectOnFocus = True
      end
      object EditBenef4PostalCode: TUniDBEdit
        Left = 584
        Top = 2262
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_POSTALCODE'
        DataSource = Ds_insurance_proposals
        TabOrder = 187
        SelectOnFocus = True
      end
      object LblBenef2Location: TUniLabel
        Left = 16
        Top = 2296
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Stad:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 163
      end
      object EditBenef3City: TUniDBEdit
        Left = 368
        Top = 2294
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_CITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 164
        SelectOnFocus = True
      end
      object EditBenef4City: TUniDBEdit
        Left = 584
        Top = 2294
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_CITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 188
        SelectOnFocus = True
      end
      object LblBenef2Country: TUniLabel
        Left = 16
        Top = 2328
        Width = 30
        Height = 13
        Hint = ''
        Caption = 'Land:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 165
      end
      object EditBenef3Country: TUniDBEdit
        Left = 368
        Top = 2326
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_COUNTRY'
        DataSource = Ds_insurance_proposals
        TabOrder = 166
        SelectOnFocus = True
      end
      object EditBenef4Country: TUniDBEdit
        Left = 584
        Top = 2326
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_COUNTRY'
        DataSource = Ds_insurance_proposals
        TabOrder = 189
        SelectOnFocus = True
      end
      object LblBenef3DateOfBirth: TUniLabel
        Left = 16
        Top = 2360
        Width = 93
        Height = 13
        Hint = ''
        Caption = 'Geboortedatum:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 167
      end
      object EditBenef3DateOfBirth: TUniDBDateTimePicker
        Left = 368
        Top = 2358
        Width = 177
        Hint = ''
        DataField = 'BENEF3_DATEOFBIRTH'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 168
      end
      object EditBenef4DateOfBirth: TUniDBDateTimePicker
        Left = 584
        Top = 2358
        Width = 177
        Hint = ''
        DataField = 'BENEF4_DATEOFBIRTH'
        DataSource = Ds_insurance_proposals
        DateTime = 44077.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 190
      end
      object LblBenef2BirthPlace: TUniLabel
        Left = 16
        Top = 2392
        Width = 91
        Height = 13
        Hint = ''
        Caption = 'Geboorteplaats:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 169
      end
      object EditBenef3BirthPlace: TUniDBEdit
        Left = 368
        Top = 2390
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_BIRTHPLACE'
        DataSource = Ds_insurance_proposals
        TabOrder = 170
        SelectOnFocus = True
      end
      object EditBenef4BirthPlace: TUniDBEdit
        Left = 584
        Top = 2390
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_BIRTHPLACE'
        DataSource = Ds_insurance_proposals
        TabOrder = 191
        SelectOnFocus = True
      end
      object LblSexBenef2: TUniLabel
        Left = 16
        Top = 2424
        Width = 52
        Height = 13
        Hint = ''
        Caption = 'Geslacht:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 171
      end
      object LblLinkInsuredBenef3: TUniLabel
        Left = 16
        Top = 2456
        Width = 183
        Height = 13
        Hint = ''
        Caption = 'Link met de verzekeringsnemer:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 174
      end
      object EditBenef3LinkInsured: TUniDBEdit
        Left = 368
        Top = 2454
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_LINK_INSURED_PERSON'
        DataSource = Ds_insurance_proposals
        TabOrder = 175
        SelectOnFocus = True
      end
      object EditBenef4LinkInsured: TUniDBEdit
        Left = 584
        Top = 2454
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_LINK_INSURED_PERSON'
        DataSource = Ds_insurance_proposals
        TabOrder = 194
        SelectOnFocus = True
      end
      object LblBenef3DivergentBen: TUniLabel
        Left = 16
        Top = 2488
        Width = 139
        Height = 13
        Hint = ''
        Caption = 'Afwijkende begunstigde:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 176
      end
      object EditBenef3DivergentBeneficiary: TUniDBEdit
        Left = 368
        Top = 2486
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF3_DIVERGENT_BENEFICIARY'
        DataSource = Ds_insurance_proposals
        TabOrder = 177
        SelectOnFocus = True
      end
      object EditBenef4DivergentBeneficiary: TUniDBEdit
        Left = 584
        Top = 2486
        Width = 177
        Height = 22
        Hint = ''
        DataField = 'BENEF4_DIVERGENT_BENEFICIARY'
        DataSource = Ds_insurance_proposals
        TabOrder = 195
        SelectOnFocus = True
      end
      object CheckBoxMoreDetailledBenefClause: TUniDBCheckBox
        Left = 16
        Top = 2537
        Width = 745
        Height = 17
        Hint = ''
        DataField = 'MORE_DETAILED_BENEF_CLAUSE'
        DataSource = Ds_insurance_proposals
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 'Meer gedetailleerde begunstigdeclausule'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 196
        ParentColor = False
        Color = clBtnFace
      end
      object CheckBoxBenefClauseIncludedInDeed: TUniDBCheckBox
        Left = 16
        Top = 2569
        Width = 745
        Height = 17
        Hint = ''
        DataField = 'BENEF_CLAUSE_INCLUDED_IN_DEED'
        DataSource = Ds_insurance_proposals
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 
          'De begunstigde clausule is opgenomen in een onderhandse akte of ' +
          'een authentieke akte die is ingediend bij:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 197
        ParentColor = False
        Color = clBtnFace
      end
      object EditBenefClauseContactData: TUniDBEdit
        Left = 368
        Top = 2593
        Width = 393
        Height = 22
        Hint = ''
        DataField = 'BENEF_CLAUSE_CONTACT_DATA'
        DataSource = Ds_insurance_proposals
        TabOrder = 198
        SelectOnFocus = True
      end
      object EditIban: TUniDBEdit
        Left = 368
        Top = 2681
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'SEPA_IBAN_NUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 201
        SelectOnFocus = True
      end
      object CheckBoxAppelationMale: TUniCheckBox
        Left = 368
        Top = 104
        Width = 153
        Height = 17
        Hint = ''
        Caption = 'De heer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 4
        OnChange = CheckBoxAppelationMaleChange
      end
      object CheckBoxAppelationFemale: TUniCheckBox
        Left = 528
        Top = 104
        Width = 153
        Height = 17
        Hint = ''
        Caption = 'Mevrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 5
        OnChange = CheckBoxAppelationFemaleChange
      end
      object CheckBoxSexFemaleInsuredPerson: TUniCheckBox
        Left = 368
        Top = 424
        Width = 153
        Height = 17
        Hint = ''
        Caption = 'Vrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 29
        OnChange = CheckBoxSexFemaleInsuredPersonChange
      end
      object CheckBoxSexMaleInsuredPerson: TUniCheckBox
        Left = 528
        Top = 424
        Width = 153
        Height = 17
        Hint = ''
        Caption = 'Man'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 30
        OnChange = CheckBoxSexMaleInsuredPersonChange
      end
      object CheckBoxInsuredSmokerYes: TUniCheckBox
        Left = 368
        Top = 713
        Width = 65
        Height = 17
        Hint = ''
        Caption = 'Ja'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 50
        OnChange = CheckBoxInsuredSmokerYesChange
      end
      object CheckBoxInsuredSmokerNo: TUniCheckBox
        Left = 456
        Top = 713
        Width = 65
        Height = 17
        Hint = ''
        Caption = 'Nee'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 51
        OnChange = CheckBoxInsuredSmokerNoChange
      end
      object CheckBoxFrequencyMonthly: TUniCheckBox
        Left = 368
        Top = 1246
        Width = 121
        Height = 17
        Hint = ''
        Caption = 'Maandelijks'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 79
        OnChange = CheckBoxFrequencyMonthlyChange
      end
      object CheckBoxFrequencyTrimesterly: TUniCheckBox
        Left = 496
        Top = 1246
        Width = 121
        Height = 17
        Hint = ''
        Caption = 'Trimestrieel'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 80
        OnChange = CheckBoxFrequencyTrimesterlyChange
      end
      object CheckBoxFrequencyYearly: TUniCheckBox
        Left = 624
        Top = 1246
        Width = 121
        Height = 17
        Hint = ''
        Caption = 'Jaarlijks'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 81
        OnChange = CheckBoxFrequencyYearlyChange
      end
      object CheckBoxStandardClause: TUniCheckBox
        Left = 16
        Top = 1462
        Width = 729
        Height = 17
        Hint = ''
        ParentShowHint = False
        Caption = 'Standaard clausule'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 92
        OnChange = CheckBoxStandardClauseChange
      end
      object CheckBoxOtherClause: TUniCheckBox
        Left = 16
        Top = 1494
        Width = 729
        Height = 17
        Hint = ''
        Caption = 'Andere begunstigde clausule voor het verzekerde kapitaal'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 93
        OnChange = CheckBoxOtherClauseChange
      end
      object CheckBoxAppelationManBenef1: TUniCheckBox
        Left = 368
        Top = 1574
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'De heer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 97
        OnChange = CheckBoxAppelationManBenef1Change
      end
      object CheckBoxAppelationWomenBenef1: TUniCheckBox
        Left = 464
        Top = 1574
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'Mevrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 98
        OnChange = CheckBoxAppelationWomenBenef1Change
      end
      object CheckBoxBenef1SexFemale: TUniCheckBox
        Left = 368
        Top = 1925
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'V'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 121
        OnChange = CheckBoxBenef1SexFemaleChange
      end
      object CheckBoxBenef1SexMale: TUniCheckBox
        Left = 464
        Top = 1925
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'M'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 122
        OnChange = CheckBoxBenef1SexMaleChange
      end
      object CheckBoxAppelationManBenef2: TUniCheckBox
        Left = 584
        Top = 1574
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'De heer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 127
        OnChange = CheckBoxAppelationManBenef2Change
      end
      object CheckBoxAppelationWomenBenef2: TUniCheckBox
        Left = 680
        Top = 1574
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'Mevrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 128
        OnChange = CheckBoxAppelationWomenBenef2Change
      end
      object CheckBoxBenef2SexFemale: TUniCheckBox
        Left = 584
        Top = 1925
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'V'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 141
        OnChange = CheckBoxBenef2SexFemaleChange
      end
      object CheckBoxBenef2SexMale: TUniCheckBox
        Left = 680
        Top = 1925
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'M'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 142
        OnChange = CheckBoxBenef2SexMaleChange
      end
      object CheckBoxAppelationManBenef3: TUniCheckBox
        Left = 368
        Top = 2070
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'De heer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 148
        OnChange = CheckBoxAppelationManBenef3Change
      end
      object CheckBoxAppelationWomenBenef3: TUniCheckBox
        Left = 464
        Top = 2070
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'Mevrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 149
        OnChange = CheckBoxAppelationWomenBenef3Change
      end
      object CheckBoxBenef3SexFemale: TUniCheckBox
        Left = 368
        Top = 2421
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'V'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 172
        OnChange = CheckBoxBenef3SexFemaleChange
      end
      object CheckBoxBenef3SexMale: TUniCheckBox
        Left = 464
        Top = 2421
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'M'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 173
        OnChange = CheckBoxBenef3SexMaleChange
      end
      object CheckBoxAppelationManBenef4: TUniCheckBox
        Left = 584
        Top = 2070
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'De heer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 178
        OnChange = CheckBoxAppelationManBenef4Change
      end
      object CheckBoxAppelationWomenBenef4: TUniCheckBox
        Left = 680
        Top = 2070
        Width = 89
        Height = 17
        Hint = ''
        Caption = 'Mevrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 179
        OnChange = CheckBoxAppelationWomenBenef4Change
      end
      object CheckBoxBenef4SexFemale: TUniCheckBox
        Left = 584
        Top = 2421
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'V'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 192
        OnChange = CheckBoxBenef4SexFemaleChange
      end
      object CheckBoxBenef4SexMale: TUniCheckBox
        Left = 680
        Top = 2421
        Width = 81
        Height = 17
        Hint = ''
        Caption = 'M'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 193
        OnChange = CheckBoxBenef4SexMaleChange
      end
      object CheckBoxDomiciliationFifth: TUniCheckBox
        Left = 368
        Top = 2717
        Width = 313
        Height = 17
        Hint = ''
        Caption = 'Incassodatum op de 5de werkdag'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 202
      end
      object LblMobileNumberInsuredPerson: TUniLabel
        Left = 16
        Top = 648
        Width = 28
        Height = 13
        Hint = ''
        Caption = 'Gsm:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 45
      end
      object EditMobileNumberInsuredPerson: TUniDBEdit
        Left = 368
        Top = 648
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_MOBILE'
        DataSource = Ds_insurance_proposals
        TabOrder = 46
        SelectOnFocus = True
      end
      object EditInsuredStreetNumber: TUniDBEdit
        Left = 688
        Top = 200
        Width = 97
        Height = 22
        Hint = ''
        DataField = 'INSURED_STREET_NUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 12
        SelectOnFocus = True
      end
      object EditInsuredStreetBox: TUniDBEdit
        Left = 792
        Top = 200
        Width = 97
        Height = 22
        Hint = ''
        DataField = 'INSURED_STREET_BOX'
        DataSource = Ds_insurance_proposals
        TabOrder = 13
        SelectOnFocus = True
      end
      object ComboInsuredCapital: TUniComboBox
        Left = 368
        Top = 1092
        Width = 313
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          '5000'
          '6000'
          '7000'
          '8000'
          '9000'
          '10000'
          '11000'
          '12000'
          '13000'
          '14000'
          '15000'
          '16000'
          '17000'
          '18000'
          '19000'
          '20000'
          '21000'
          '22000'
          '23000'
          '24000'
          '25000')
        TabOrder = 72
        IconItems = <>
      end
      object LblSepaReferenceMandateValue: TUniDBText
        Left = 368
        Top = 2747
        Width = 183
        Height = 13
        Hint = ''
        DataField = 'SEPA_REFERENCE_MANDATE'
        DataSource = Ds_insurance_proposals
        ParentFont = False
        Font.Style = [fsBold]
      end
      object LblTitleCorrespondanceAddress: TUniLabel
        Left = 16
        Top = 752
        Width = 81
        Height = 19
        Hint = ''
        Caption = 'Postadres'
        ParentFont = False
        Font.Color = 3546273
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 52
      end
      object CheckBoxCorrespondanceAddressInsuredPerson: TUniCheckBox
        Left = 368
        Top = 783
        Width = 313
        Height = 17
        Hint = ''
        Caption = 'Adres van de verzekeringsnemer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 53
        OnChange = CheckBoxCorrespondanceAddressInsuredPersonChange
      end
      object CheckBoxCorrespondanceOtherPerson: TUniCheckBox
        Left = 368
        Top = 815
        Width = 521
        Height = 17
        Hint = ''
        Caption = 
          'Stuur correspondentie naar het adres van de onderstaande derde p' +
          'artij:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 54
        OnChange = CheckBoxCorrespondanceOtherPersonChange
      end
      object CheckBoxCorrespondenceMale: TUniCheckBox
        Left = 488
        Top = 848
        Width = 129
        Height = 17
        Hint = ''
        Caption = 'De heer'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 55
        OnChange = CheckBoxCorrespondenceMaleChange
      end
      object CheckBoxCorrespondanceFemale: TUniCheckBox
        Left = 624
        Top = 848
        Width = 129
        Height = 17
        Hint = ''
        Caption = 'Mevrouw'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 56
        OnChange = CheckBoxCorrespondanceFemaleChange
      end
      object CheckBoxCorrespondanceCompany: TUniCheckBox
        Left = 760
        Top = 848
        Width = 129
        Height = 17
        Hint = ''
        Caption = 'Bedrijf'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 57
        OnChange = CheckBoxCorrespondanceCompanyChange
      end
      object LblCorrespondanceName: TUniLabel
        Left = 368
        Top = 879
        Width = 178
        Height = 13
        Hint = ''
        Caption = 'Naam, voornaam of firmanaam:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 58
      end
      object EditCorrespondanceName: TUniDBEdit
        Left = 576
        Top = 879
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_NAME'
        DataSource = Ds_insurance_proposals
        TabOrder = 59
      end
      object LblCorrespondanceAddress: TUniLabel
        Left = 368
        Top = 911
        Width = 131
        Height = 13
        Hint = ''
        Caption = 'Straat / nummer / bus:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 60
      end
      object EditCorrespondanceStreet: TUniDBEdit
        Left = 576
        Top = 911
        Width = 198
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_STREET'
        DataSource = Ds_insurance_proposals
        TabOrder = 61
      end
      object LblCorrespondanceZipCode: TUniLabel
        Left = 368
        Top = 943
        Width = 55
        Height = 13
        Hint = ''
        Caption = 'Postcode:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 64
      end
      object EditCorrespondanceZipCode: TUniDBEdit
        Left = 576
        Top = 943
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_POSTALCODE'
        DataSource = Ds_insurance_proposals
        TabOrder = 65
      end
      object LblCorrespondanceLocation: TUniLabel
        Left = 368
        Top = 975
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Stad:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 66
      end
      object EditCorrespondanceLocation: TUniDBEdit
        Left = 576
        Top = 975
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_CITY'
        DataSource = Ds_insurance_proposals
        TabOrder = 67
      end
      object EditCorrespondanceStreetNumber: TUniDBEdit
        Left = 784
        Top = 911
        Width = 49
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_STREET_NUMBER'
        DataSource = Ds_insurance_proposals
        TabOrder = 62
      end
      object EditCorrespondanceStreetBox: TUniDBEdit
        Left = 840
        Top = 911
        Width = 49
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_STREET_BOX'
        DataSource = Ds_insurance_proposals
        TabOrder = 63
      end
      object LblCorrespondanceCountry: TUniLabel
        Left = 368
        Top = 1007
        Width = 30
        Height = 13
        Hint = ''
        Caption = 'Land:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 68
      end
      object EditCorrespondanceCountry: TUniDBEdit
        Left = 576
        Top = 1007
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'CORRESPONDANCE_COUNTRY'
        DataSource = Ds_insurance_proposals
        TabOrder = 69
      end
      object LblPremiumGross: TUniLabel
        Left = 16
        Top = 1283
        Width = 323
        Height = 13
        Hint = ''
        Caption = 'Bedrag van de premie exclusief belastingen en bijdragen:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 83
      end
      object EditPremiumGross: TUniDBFormattedNumberEdit
        Left = 592
        Top = 1283
        Width = 153
        Height = 22
        Hint = ''
        DataField = 'PREMIUM_AMOUNT_GROSS'
        DataSource = Ds_insurance_proposals
        TabOrder = 84
        TabStop = False
        ReadOnly = True
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object LblPremiumTax: TUniLabel
        Left = 16
        Top = 1315
        Width = 227
        Height = 13
        Hint = ''
        Caption = 'Bedrag van de belastingen en bijdragen:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 85
      end
      object EditPremiumTax: TUniDBFormattedNumberEdit
        Left = 592
        Top = 1315
        Width = 153
        Height = 22
        Hint = ''
        DataField = 'PREMIUM_AMOUNT_TAX'
        DataSource = Ds_insurance_proposals
        TabOrder = 86
        TabStop = False
        ReadOnly = True
        SelectOnFocus = True
        DecimalSeparator = ','
        ThousandSeparator = '.'
      end
      object EditLengthInsuredPerson: TUniDBNumberEdit
        Left = 368
        Top = 456
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_HEIGHT'
        DataSource = Ds_insurance_proposals
        TabOrder = 32
        SelectOnFocus = True
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
      object EditWeightInsuredPerson: TUniDBNumberEdit
        Left = 368
        Top = 488
        Width = 313
        Height = 22
        Hint = ''
        DataField = 'INSURED_WEIGHT'
        DataSource = Ds_insurance_proposals
        TabOrder = 34
        SelectOnFocus = True
        DecimalPrecision = 0
        DecimalSeparator = ','
      end
      object LblLengthInsuredPerson: TUniLabel
        Left = 16
        Top = 456
        Width = 72
        Height = 13
        Hint = ''
        Caption = 'Lengte (cm):'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 31
      end
      object LblWeightInsuredPerson: TUniLabel
        Left = 16
        Top = 488
        Width = 75
        Height = 13
        Hint = ''
        Caption = 'Gewicht (kg):'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 33
      end
      object BtnCalculate: TUniThemeButton
        Left = 760
        Top = 1244
        Width = 129
        Height = 33
        Hint = ''
        Caption = 'Berekenen'
        TabStop = False
        TabOrder = 82
        OnClick = BtnCalculateClick
        ButtonTheme = uctSuccess
      end
      object LblLanguage: TUniLabel
        Left = 616
        Top = 320
        Width = 27
        Height = 13
        Hint = ''
        Caption = 'Taal:'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 22
      end
      object ComboboxLanguage: TUniComboBox
        Left = 688
        Top = 320
        Width = 201
        Hint = ''
        Style = csDropDownList
        Text = 'Nederlands'
        Items.Strings = (
          'Nederlands'
          'Frans')
        ItemIndex = 0
        TabOrder = 23
        IconItems = <>
      end
    end
    object ContainerDocuments: TUniContainerPanel
      Left = 968
      Top = 0
      Width = 344
      Height = 521
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      LayoutConfig.Margin = '0 16 0 8'
      ExplicitHeight = 511
      object PanelHeaderDocuments: TUniPanel
        Left = 0
        Top = 0
        Width = 344
        Height = 25
        Hint = ''
        Align = alTop
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 1
        Caption = 'Documenten'
      end
      object LblUpLoadDoc1: TUniLabel
        Tag = 1
        Left = 8
        Top = 40
        Width = 67
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Voorstel (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 2
        OnClick = LblUpLoadDoc1Click
      end
      object LblUpLoadDoc2: TUniLabel
        Tag = 2
        Left = 8
        Top = 72
        Width = 155
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Algemene voorwaarden (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 3
        OnClick = LblUpLoadDoc1Click
      end
      object LblUpLoadDoc3: TUniLabel
        Tag = 3
        Left = 8
        Top = 104
        Width = 137
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Medische vragenlijst (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 4
        OnClick = LblUpLoadDoc1Click
      end
      object LblUpLoadDoc4: TUniLabel
        Tag = 4
        Left = 8
        Top = 136
        Width = 253
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Aanvaardingsformulier + (Due dilligence) (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 5
        OnClick = LblUpLoadDoc1Click
      end
      object LblUpLoadDoc5: TUniLabel
        Tag = 5
        Left = 8
        Top = 168
        Width = 195
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Akkoord voor gegevensbeheer (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 6
        OnClick = LblUpLoadDoc1Click
      end
      object LblUpLoadDoc6: TUniLabel
        Tag = 6
        Left = 8
        Top = 200
        Width = 103
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Sepa formulier (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 7
        OnClick = LblUpLoadDoc1Click
      end
      object LblUpLoadDoc7: TUniLabel
        Tag = 7
        Left = 8
        Top = 232
        Width = 215
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Kopie ID kaart verzekeringsnemer (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 8
        OnClick = LblUpLoadDoc1Click
      end
      object LblDocumentRequired: TUniLabel
        Left = 8
        Top = 659
        Width = 268
        Height = 13
        Hint = ''
        Caption = '(*) Document vereist om te kunnen doorsturen'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 9
      end
      object ColorDocumentPresent: TUniContainerPanel
        Left = 8
        Top = 681
        Width = 16
        Height = 16
        Hint = ''
        ParentColor = False
        Color = clGreen
        TabOrder = 10
      end
      object LblDocumentPresent: TUniLabel
        Left = 32
        Top = 683
        Width = 114
        Height = 13
        Hint = ''
        Caption = 'Document aanwezig'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 11
      end
      object LblUpLoadDoc8: TUniLabel
        Tag = 8
        Left = 8
        Top = 264
        Width = 124
        Height = 13
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Bewijs van woonst (*)'
        ParentFont = False
        Font.Style = [fsBold, fsUnderline]
        TabOrder = 12
        OnClick = LblUpLoadDoc1Click
      end
      object BtnUploadDoc1: TUniThemeButton
        Tag = 1
        Left = 280
        Top = 40
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 13
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc2: TUniThemeButton
        Tag = 2
        Left = 280
        Top = 72
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 14
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc3: TUniThemeButton
        Tag = 3
        Left = 280
        Top = 104
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 15
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc4: TUniThemeButton
        Tag = 4
        Left = 280
        Top = 136
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 16
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc5: TUniThemeButton
        Tag = 5
        Left = 280
        Top = 168
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 17
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc6: TUniThemeButton
        Tag = 6
        Left = 280
        Top = 200
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 18
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc7: TUniThemeButton
        Tag = 7
        Left = 280
        Top = 232
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 19
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnUploadDoc8: TUniThemeButton
        Tag = 8
        Left = 280
        Top = 264
        Width = 25
        Height = 25
        Hint = 'Uploaden'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 20
        Images = UniMainModule.ImageList
        ImageIndex = 54
        OnClick = BtnUploadDoc1Click
        ButtonTheme = uctPrimary
      end
      object BtnDeleteDoc1: TUniThemeButton
        Tag = 1
        Left = 312
        Top = 40
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 21
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc2: TUniThemeButton
        Tag = 2
        Left = 312
        Top = 72
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 22
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc3: TUniThemeButton
        Tag = 3
        Left = 312
        Top = 104
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 23
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc4: TUniThemeButton
        Tag = 4
        Left = 312
        Top = 136
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 24
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc5: TUniThemeButton
        Tag = 5
        Left = 312
        Top = 168
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 25
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc6: TUniThemeButton
        Tag = 6
        Left = 312
        Top = 200
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 26
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc7: TUniThemeButton
        Tag = 7
        Left = 312
        Top = 232
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 27
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
      object BtnDeleteDoc8: TUniThemeButton
        Tag = 8
        Left = 312
        Top = 264
        Width = 25
        Height = 25
        Hint = 'Verwijderen'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 28
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteDoc1Click
        ButtonTheme = uctDanger
      end
    end
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 610
    Width = 1312
    Height = 35
    Hint = ''
    ParentColor = False
    Color = clWhite
    Align = alBottom
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '8 16 16 16'
    ExplicitTop = 598
    object ContainerFooterRight: TUniContainerPanel
      Left = 904
      Top = 0
      Width = 408
      Height = 35
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      ExplicitHeight = 33
      object BtnSave: TUniThemeButton
        Left = 0
        Top = 0
        Width = 129
        Height = 33
        Hint = ''
        Caption = 'Opslaan'
        ParentFont = False
        TabOrder = 1
        OnClick = BtnSaveClick
        ButtonTheme = uctPrimary
      end
      object BtnSend: TUniThemeButton
        Left = 136
        Top = 0
        Width = 129
        Height = 33
        Hint = ''
        Caption = 'Doorsturen'
        ParentFont = False
        TabOrder = 2
        OnClick = BtnSendClick
        ButtonTheme = uctSuccess
      end
      object BtnCancel: TUniThemeButton
        Left = 272
        Top = 0
        Width = 129
        Height = 33
        Hint = ''
        Caption = 'Annuleren'
        ParentFont = False
        TabOrder = 3
        OnClick = BtnCancelClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object Insurance_proposals: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_INSURANCE_PROPOSALS_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UniMainModule.UpdTrans
    SQL.Strings = (
      'Select * from insurance_proposals')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 1160
    Top = 416
  end
  object Ds_insurance_proposals: TDataSource
    DataSet = Insurance_proposals
    Left = 1160
    Top = 472
  end
  object LinkedLanguageSwissLifeDemandsAdd: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TSwissLifeDemandsAddFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerHeaderLeft.Layout'
      'ContainerHeaderRight.Layout'
      'ImageHeader.Url'
      'ContainerBody.Layout'
      'EditNationalInsuranceNumber.FieldLabelSeparator'
      'EditNameInsuredPerson.FieldLabelSeparator'
      'EditFirstNameInsuredPerson.FieldLabelSeparator'
      'EditStreetInsuredPerson.FieldLabelSeparator'
      'EditPostalCodeInsuredPerson.FieldLabelSeparator'
      'EditCityInsuredPerson.FieldLabelSeparator'
      'EditDateOfBirthInsuredPerson.DateFormat'
      'EditDateOfBirthInsuredPerson.FieldLabelSeparator'
      'EditDateOfBirthInsuredPerson.TimeFormat'
      'EditBirthPlaceInsuredPerson.FieldLabelSeparator'
      'EditNationalityInsuredPerson.FieldLabelSeparator'
      'ComboCivilstateInsuredPerson.FieldLabelSeparator'
      'EditProfessionalActivityInsuredPerson.FieldLabelSeparator'
      'EditIdentitycardnumberInsuredPerson.FieldLabelSeparator'
      'EditIdentitycardValidUntilInsuredPerson.DateFormat'
      'EditIdentitycardValidUntilInsuredPerson.FieldLabelSeparator'
      'EditIdentitycardValidUntilInsuredPerson.TimeFormat'
      'EditPhoneNumberInsuredPerson.FieldLabelSeparator'
      'EditEmailInsuredPerson.FieldLabelSeparator'
      'EditStartDateContract.DateFormat'
      'EditStartDateContract.FieldLabelSeparator'
      'EditStartDateContract.TimeFormat'
      'EditEndDateContract.DateFormat'
      'EditEndDateContract.FieldLabelSeparator'
      'EditEndDateContract.TimeFormat'
      'EditPremiumAmount.FieldLabelSeparator'
      'EditPremiumPaymentYears.FieldLabelSeparator'
      'EditRankBenef1.FieldLabelSeparator'
      'EditPercentageBenef1.FieldLabelSeparator'
      'EditRankBenef2.FieldLabelSeparator'
      'EditPercentageBenef2.FieldLabelSeparator'
      'EditBenef1Name.FieldLabelSeparator'
      'EditBenef2Name.FieldLabelSeparator'
      'EditBenef1Firstname.FieldLabelSeparator'
      'EditBenef2Firstname.FieldLabelSeparator'
      'EditBenef1StreetandNumber.FieldLabelSeparator'
      'EditBenef2StreetandNumber.FieldLabelSeparator'
      'EditBenef1PostalCode.FieldLabelSeparator'
      'EditBenef2PostalCode.FieldLabelSeparator'
      'EditBenef1City.FieldLabelSeparator'
      'EditBenef2City.FieldLabelSeparator'
      'EditBenef1Country.FieldLabelSeparator'
      'EditBenef2Country.FieldLabelSeparator'
      'EditBenef1DateOfBirth.DateFormat'
      'EditBenef1DateOfBirth.FieldLabelSeparator'
      'EditBenef1DateOfBirth.TimeFormat'
      'EditBenef2DateOfBirth.DateFormat'
      'EditBenef2DateOfBirth.FieldLabelSeparator'
      'EditBenef2DateOfBirth.TimeFormat'
      'EditBenef1BirthPlace.FieldLabelSeparator'
      'EditBenef2BirthPlace.FieldLabelSeparator'
      'EditBenef1LinkInsured.FieldLabelSeparator'
      'EditBenef2LinkInsured.FieldLabelSeparator'
      'EditBenef1DivergentBeneficiary.FieldLabelSeparator'
      'EditBenef2DivergentBeneficiary.FieldLabelSeparator'
      'EditRankBenef3.FieldLabelSeparator'
      'EditPercentageBenef3.FieldLabelSeparator'
      'EditRankBenef4.FieldLabelSeparator'
      'EditPercentageBenef4.FieldLabelSeparator'
      'EditBenef3Name.FieldLabelSeparator'
      'EditBenef4Name.FieldLabelSeparator'
      'EditBenef3Firstname.FieldLabelSeparator'
      'EditBenef4Firstname.FieldLabelSeparator'
      'EditBenef3StreetandNumber.FieldLabelSeparator'
      'EditBenef4StreetandNumber.FieldLabelSeparator'
      'EditBenef3PostalCode.FieldLabelSeparator'
      'EditBenef4PostalCode.FieldLabelSeparator'
      'EditBenef3City.FieldLabelSeparator'
      'EditBenef4City.FieldLabelSeparator'
      'EditBenef3Country.FieldLabelSeparator'
      'EditBenef4Country.FieldLabelSeparator'
      'EditBenef3DateOfBirth.DateFormat'
      'EditBenef3DateOfBirth.FieldLabelSeparator'
      'EditBenef3DateOfBirth.TimeFormat'
      'EditBenef4DateOfBirth.DateFormat'
      'EditBenef4DateOfBirth.FieldLabelSeparator'
      'EditBenef4DateOfBirth.TimeFormat'
      'EditBenef3BirthPlace.FieldLabelSeparator'
      'EditBenef4BirthPlace.FieldLabelSeparator'
      'EditBenef3LinkInsured.FieldLabelSeparator'
      'EditBenef4LinkInsured.FieldLabelSeparator'
      'EditBenef3DivergentBeneficiary.FieldLabelSeparator'
      'EditBenef4DivergentBeneficiary.FieldLabelSeparator'
      'CheckBoxMoreDetailledBenefClause.FieldLabelSeparator'
      'CheckBoxMoreDetailledBenefClause.ValueChecked'
      'CheckBoxMoreDetailledBenefClause.ValueUnchecked'
      'CheckBoxBenefClauseIncludedInDeed.FieldLabelSeparator'
      'CheckBoxBenefClauseIncludedInDeed.ValueChecked'
      'CheckBoxBenefClauseIncludedInDeed.ValueUnchecked'
      'EditBenefClauseContactData.FieldLabelSeparator'
      'EditIban.FieldLabelSeparator'
      'ContainerDocuments.Layout'
      'PanelHeaderDocuments.Layout'
      'ContainerFooter.Layout'
      'ContainerFooterRight.Layout'
      'Insurance_proposals.KeyGenerator'
      'ColorDocumentPresent.Layout'
      'EditMobileNumberInsuredPerson.FieldLabelSeparator'
      'EditInsuredStreetNumber.FieldLabelSeparator'
      'EditInsuredStreetBox.FieldLabelSeparator'
      'ComboInsuredCapital.FieldLabelSeparator'
      'EditCorrespondanceName.FieldLabelSeparator'
      'EditCorrespondanceStreet.FieldLabelSeparator'
      'EditCorrespondanceZipCode.FieldLabelSeparator'
      'EditCorrespondanceLocation.FieldLabelSeparator'
      'EditCorrespondanceStreetNumber.FieldLabelSeparator'
      'EditCorrespondanceStreetBox.FieldLabelSeparator'
      'EditCorrespondanceCountry.FieldLabelSeparator'
      'EditPremiumGross.FieldLabelSeparator'
      'EditPremiumTax.FieldLabelSeparator'
      'EditLengthInsuredPerson.FieldLabelSeparator'
      'EditWeightInsuredPerson.FieldLabelSeparator'
      'ComboboxLanguage.FieldLabelSeparator')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 1160
    Top = 520
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A005400530077006900730073004C0069006600650044006500
      6D0061006E0064007300410064006400460072006D00010056006F006F007200
      7300740065006C0020005300770069007300730020004C006900660065002000
      500072006F0074006500630074002000420065006C006700690075006D000100
      500072006F0070006F0073006900740069006F006E0020005300770069007300
      730020004C006900660065002000500072006F00740065006300740020004200
      65006C006700690075006D0001005300770069007300730020004C0069006600
      65002000500072006F0074006500630074002000420065006C00670069007500
      6D002000700072006F0070006F00730061006C0001000D000A004C0062006C00
      44006F00630075006D0065006E00740052006500710075006900720065006400
      010028002A002900200044006F00630075006D0065006E007400200076006500
      7200650069007300740020006F006D0020007400650020006B0075006E006E00
      65006E00200064006F006F007200730074007500720065006E00010028002A00
      2900200044006F00630075006D0065006E007400200072006500710075006900
      7300200070006F0075007200200070006F00750076006F006900720020007400
      720061006E0073006D0065007400740072006500010028002A00290020004400
      6F00630075006D0065006E007400200072006500710075006900720065006400
      200074006F002000620065002000610062006C006500200074006F0020006600
      6F007200770061007200640001000D000A004C0062006C00550070004C006F00
      6100640044006F00630034000100410061006E00760061006100720064006900
      6E006700730066006F0072006D0075006C0069006500720020002B0020002800
      4400750065002000640069006C006C006900670065006E006300650029002000
      28002A002900010046006F0072006D0075006C00610069007200650020006100
      630063006500700074006100740069006F006E0020002B002000280044007500
      65002000640069006C006C006900670065006E00630065002900200028002A00
      2900010041006300630065007000740061006E0063006500200066006F007200
      6D0020002B00200028004400750065002000640069006C006C00690067006500
      6E00630065002900200028002A00290001000D000A004C0062006C0054006900
      74006C0065004E006100740069006F006E0061006C0069006E00730075007200
      61006E00630065004E0075006D006200650072000100410062006F006E006E00
      65006D0065006E0074007300610061006E00760072006100610067006E007500
      6D006D006500720020002800720069006A006B00730072006500670069007300
      7400650072006E0075006D006D006500720029003A000100440065006D006100
      6E0064006500200064006500200073006F007500730063007200690070007400
      69006F006E0020006E0075006D00E90072006F00200028006E00B00020006400
      650020007200650067006900730074007200650020006E006100740069006F00
      6E0061006C0029003A0001005300750062007300630072006900700074006900
      6F006E002000720065007100750065007300740020006E0075006D0062006500
      7200200028006E006100740069006F006E0061006C0020007200650067006900
      730074006500720020006E0075006D0062006500720029003A0001000D000A00
      43006800650063006B0042006F00780043006F00720072006500730070006F00
      6E00640061006E0063006500410064006400720065007300730049006E007300
      750072006500640050006500720073006F006E00010041006400720065007300
      2000760061006E0020006400650020007600650072007A0065006B0065007200
      69006E00670073006E0065006D00650072000100410064007200650073007300
      650020006400750020007000720065006E006500750072000100410064006400
      7200650073007300200070006F006C0069006300790068006F006C0064006500
      720001000D000A004C0062006C00420065006E00650066003300440069007600
      65007200670065006E007400420065006E00010041006600770069006A006B00
      65006E0064006500200062006500670075006E00730074006900670064006500
      3A0001004200E9006E00E900660069006300690061006900720065002000E000
      2000740069007400720065002000730075006200730069006400690061006900
      720065003A000100530075006200730069006400690061007200790020006200
      65006E00650066006900630069006100720079003A0001000D000A004C006200
      6C00420065006E0065006600310044006900760065007200670065006E007400
      420065006E00010041006600770069006A006B0065006E006400650020006200
      6500670075006E007300740069006700640065003A0001004200E9006E00E900
      660069006300690061006900720065002000E000200074006900740072006500
      2000730075006200730069006400690061006900720065003A00010053007500
      620073006900640069006100720079002000620065006E006500660069006300
      69006100720079003A0001000D000A004C0062006C00550070004C006F006100
      640044006F0063003500010041006B006B006F006F0072006400200076006F00
      6F00720020006700650067006500760065006E00730062006500680065006500
      7200200028002A00290001004100630063006F00720064002000670065007300
      740069006F006E002000640065007300200064006F006E006E00E90065007300
      200028002A0029000100410067007200650065006D0065006E00740020006400
      61007400610020006D0061006E006100670065006D0065006E00740020002800
      2A00290001000D000A004C0062006C00550070004C006F006100640044006F00
      63003200010041006C00670065006D0065006E006500200076006F006F007200
      7700610061007200640065006E00200028002A002900010043006F006E006400
      6900740069006F006E00730020006700E9006E00E900720061006C0065007300
      200028002A00290001005400650072006D007300200061006E00640020006300
      6F006E0064006900740069006F006E007300200028002A00290001000D000A00
      43006800650063006B0042006F0078004F00740068006500720043006C006100
      750073006500010041006E006400650072006500200062006500670075006E00
      730074006900670064006500200063006C0061007500730075006C0065002000
      76006F006F007200200068006500740020007600650072007A0065006B006500
      72006400650020006B006100700069007400610061006C000100410075007400
      72006500200063006C00610075007300650020006200E9006E00E90066006900
      630069006100690072006500200070006F007500720020006C00650020006300
      610070006900740061006C00200061007300730075007200E90001004F007400
      6800650072002000620065006E00650066006900630069006100720079002000
      63006C006100750073006500200066006F007200200074006800650020006900
      6E007300750072006500640020006300610070006900740061006C0001000D00
      0A00420074006E00430061006E00630065006C00010041006E006E0075006C00
      6500720065006E00010041006E006E0075006C00650072000100430061006E00
      630065006C0001000D000A004C0062006C0041006D006F0075006E0074004900
      6E00730075007200650064004300610070006900740061006C00010042006100
      7300690073006B006100700069007400610061006C003A000100430061007000
      6900740061006C00200064006500200062006100730065003A00010042006100
      73006900630020006300610070006900740061006C003A0001000D000A004C00
      62006C005000720065006D00690075006D005400610078000100420065006400
      7200610067002000760061006E002000640065002000620065006C0061007300
      740069006E00670065006E00200065006E002000620069006A00640072006100
      670065006E003A0001004D006F006E00740061006E0074002000640065007300
      200074006100780065007300200065007400200063006F006E00740072006900
      62007500740069006F006E0073002000E0002000700061007900650072003A00
      010041006D006F0075006E00740020006F006600200074006100780065007300
      200061006E006400200063006F006E0074007200690062007500740069006F00
      6E0073002000700061007900610062006C0065003A0001000D000A004C006200
      6C005000720065006D00690075006D00470072006F0073007300010042006500
      64007200610067002000760061006E0020006400650020007000720065006D00
      6900650020006500780063006C00750073006900650066002000620065006C00
      61007300740069006E00670065006E00200065006E002000620069006A006400
      72006100670065006E003A0001004D006F006E00740061006E00740020006400
      650020006C00610020007000720069006D006500200063006F006D006D006500
      72006300690061006C0065002000E0002000700061007900650072002C002000
      68006F0072007300200074006100780065007300200065007400200063006F00
      6E0074007200690062007500740069006F006E0073003A00010041006D006F00
      75006E00740020006F0066002000740068006500200063006F006D006D006500
      72006300690061006C0020007000720065006D00690075006D00200074006F00
      200062006500200070006100690064002C0020006500780063006C0075006400
      69006E006700200074006100780065007300200061006E006400200063006F00
      6E0074007200690062007500740069006F006E0073003A0001000D000A004C00
      62006C005000720065006D00690075006D0041006D006F0075006E0074000100
      4200650064007200610067002000760061006E00200064006500200070007200
      65006D006900650020006D0065007400200069006E0062006500670072006900
      70002000760061006E002000620065006C0061007300740069006E0067006500
      6E00200065006E002000620069006A00640072006100670065006E003A000100
      4D006F006E00740061006E00740020006400650020006C006100200070007200
      69006D006500200063006F006D006D00650072006300690061006C0065002C00
      200074006100780065007300200065007400200063006F006E00740072006900
      62007500740069006F006E007300200063006F006D0070007200690073006500
      73003A00010041006D006F0075006E00740020006F0066002000740068006500
      200063006F006D006D00650072006300690061006C0020007000720065006D00
      690075006D002C00200074006100780065007300200061006E00640020006300
      6F006E0074007200690062007500740069006F006E007300200069006E006300
      6C0075006400650064003A0001000D000A0043006800650063006B0042006F00
      780043006F00720072006500730070006F006E00640061006E00630065004300
      6F006D00700061006E0079000100420065006400720069006A00660001005000
      6500720073006F006E006E00650020006D006F00720061006C00650001004300
      6F00720070006F0072006100740069006F006E0001000D000A004C0062006C00
      420065006E006500660069006300690061007200790034000100420065006700
      75006E0073007400690067006400650001004200E9006E00E900660069006300
      690061006900720065000100420065006E006500660069006300690061007200
      790001000D000A004C0062006C00420065006E00650066006900630069006100
      720079003300010042006500670075006E007300740069006700640065000100
      4200E9006E00E900660069006300690061006900720065000100420065006E00
      6500660069006300690061007200790001000D000A004C0062006C0042006500
      6E00650066006900630069006100720079003100010042006500670075006E00
      73007400690067006400650001004200E9006E00E90066006900630069006100
      6900720065000100420065006E00650066006900630069006100720079000100
      0D000A004C0062006C00420065006E0065006600690063006900610072007900
      3200010042006500670075006E0073007400690067006400650001004200E900
      6E00E900660069006300690061006900720065000100420065006E0065006600
      69006300690061007200790001000D000A004C0062006C00560061006C007500
      650043006F0075006E0074007200790049006E00730075007200650064005000
      6500720073006F006E000100420065006C0067006900EB000100420065006C00
      670069007100750065000100420065006C006700690075006D0001000D000A00
      420074006E00430061006C00630075006C006100740065000100420065007200
      65006B0065006E0065006E000100430061006C00630075006C00650072000100
      430061006C00630075006C0061007400650001000D000A004C0062006C005400
      690074006C0065005000720065006D00690075006D005000610079006D006500
      6E0074007300010042006500740061006C0069006E0067002000760061006E00
      20006400650020007000720065006D0069006500730001005000610069006500
      6D0065006E007400200064006500730020007000720069006D00650073000100
      5000610079006D0065006E00740020006F006600200074006800650020007000
      720065006D00690075006D00730001000D000A004C0062006C00430069007600
      69006C005300740061007400650049006E007300750072006500640050006500
      720073006F006E0001004200750072006700650072006C0069006A006B006500
      2000730074006100610074003A000100C9007400610074002000630069007600
      69006C003A00010043006900760069006C002000730074006100740075007300
      3A0001000D000A004C0062006C005400690074006C00650043006C0061007500
      73006500420065006E0065006600690063006900610072006900650073000100
      43006C0061007500730075006C006500200062006500670075006E0073007400
      6900670064006500010043006C00610075007300650020006200E9006E00E900
      660069006300690061006900720065000100420065006E006500660069006300
      6900610072007900200063006C00610075007300650001000D000A0043006800
      650063006B0042006F007800420065006E006500660043006C00610075007300
      650049006E0063006C00750064006500640049006E0044006500650064000100
      44006500200062006500670075006E0073007400690067006400650020006300
      6C0061007500730075006C00650020006900730020006F007000670065006E00
      6F006D0065006E00200069006E002000650065006E0020006F006E0064006500
      7200680061006E00640073006500200061006B007400650020006F0066002000
      650065006E002000610075007400680065006E007400690065006B0065002000
      61006B00740065002000640069006500200069007300200069006E0067006500
      6400690065006E0064002000620069006A003A0001004C006100200063006C00
      610075007300650020006200E9006E00E9006600690063006900610069007200
      6500200065007300740020007200650070007200690073006500200064006100
      6E007300200075006E0020006100630074006500200073006F00750073002000
      7300650069006E00670020007000720069007600E90020006F00750020007500
      6E00200061006300740065002000610075007400680065006E00740069007100
      7500650020006400E90070006F007300E90020006100750070007200E8007300
      2000640065003A0001005400680065002000620065006E006500660069006300
      6900610072007900200063006C00610075007300650020006900730020006900
      6E0063006C007500640065006400200069006E00200061002000700072006900
      76006100740065002000640065006500640020006F007200200061006E002000
      610075007400680065006E007400690063002000640065006500640020006600
      69006C0065006400200077006900740068003A0001000D000A00430068006500
      63006B0042006F00780041007000700065006C006100740069006F006E004D00
      61006E00420065006E0065006600330001004400650020006800650065007200
      01004D006F006E007300690065007500720001004D0072002E0001000D000A00
      43006800650063006B0042006F00780041007000700065006C00610074006900
      6F006E004D0061006C0065000100440065002000680065006500720001004D00
      6F006E007300690065007500720001004D0072002E0001000D000A0043006800
      650063006B0042006F00780043006F00720072006500730070006F006E006400
      65006E00630065004D0061006C00650001004400650020006800650065007200
      01004D006F006E007300690065007500720001004D0072002E0001000D000A00
      43006800650063006B0042006F00780041007000700065006C00610074006900
      6F006E004D0061006E00420065006E0065006600310001004400650020006800
      65006500720001004D006F006E007300690065007500720001004D0072002E00
      01000D000A0043006800650063006B0042006F00780041007000700065006C00
      6100740069006F006E004D0061006E00420065006E0065006600320001004400
      65002000680065006500720001004D006F006E00730069006500750072000100
      4D0072002E0001000D000A0043006800650063006B0042006F00780041007000
      700065006C006100740069006F006E004D0061006E00420065006E0065006600
      34000100440065002000680065006500720001004D006F006E00730069006500
      7500720001004D0072002E0001000D000A004C0062006C0044006F0063007500
      6D0065006E007400500072006500730065006E007400010044006F0063007500
      6D0065006E0074002000610061006E00770065007A0069006700010044006F00
      630075006D0065006E007400200070007200E900730065006E00740001004400
      6F00630075006D0065006E0074002000700072006500730065006E0074000100
      0D000A00500061006E0065006C0048006500610064006500720044006F006300
      75006D0065006E0074007300010044006F00630075006D0065006E0074006500
      6E00010044006F00630075006D0065006E0074007300010044006F0063007500
      6D0065006E007400730001000D000A00420074006E00530065006E0064000100
      44006F006F007200730074007500720065006E00010045006E0076006F007900
      650072000100530065006E00640001000D000A004C0062006C00500072006500
      6D00690075006D004400750072006100740069006F006E000100440075007500
      7200200069006E0020006A006100720065006E00200062006500740061006C00
      69006E00670020007000720065006D00690065003A000100440075007200E900
      6500200065006E00200061006E006E00E9006500730020006400650020007000
      6100690065006D0065006E00740020006400650020006C006100200070007200
      69006D0065003A0001004400750072006100740069006F006E00200069006E00
      20007900650061007200730020006F00660020007000720065006D0069007500
      6D0020007000610079006D0065006E0074003A0001000D000A004C0062006C00
      45006D00610069006C0049006E00730075007200650064005000650072007300
      6F006E00010045002D006D00610069006C002000610064007200650073003A00
      01004100640072006500730073006500200065002D006D00610069006C003A00
      010045002D006D00610069006C00200061006400640072006500730073003A00
      01000D000A004C0062006C004600720065007100750065006E00630065004F00
      66005000610079006D0065006E00740001004600720065007100750065006E00
      7400690065002000760061006E0020006400650020007000720065006D006900
      650062006500740061006C0069006E00670065006E003A00010046007200E900
      7100750065006E0063006500200064006500200070006100690065006D006500
      6E007400200064006500730020007000720069006D00650073003A0001005000
      720065006D00690075006D0020007000610079006D0065006E00740020006600
      720065007100750065006E00630079003A0001000D000A004C0062006C004200
      65006E0065006600310044006100740065004F00660042006900720074006800
      01004700650062006F006F0072007400650064006100740075006D003A000100
      440061007400650020006400650020006E00610069007300730061006E006300
      65003A000100440061007400650020006F006600200062006900720074006800
      3A0001000D000A004C0062006C00420065006E00650066003300440061007400
      65004F0066004200690072007400680001004700650062006F006F0072007400
      650064006100740075006D003A00010044006100740065002000640065002000
      6E00610069007300730061006E00630065003A00010044006100740065002000
      6F0066002000620069007200740068003A0001000D000A004C0062006C004400
      6100740065004F0066004200690072007400680049006E007300750072006500
      640050006500720073006F006E0001004700650062006F006F00720074006500
      64006100740075006D003A000100440061007400650020006400650020006E00
      610069007300730061006E00630065003A000100440061007400650020006F00
      66002000620069007200740068003A0001000D000A004C0062006C0042006500
      6E006500660031004200690072007400680050006C0061006300650001004700
      650062006F006F0072007400650070006C0061006100740073003A0001004C00
      69006500750020006400650020006E00610069007300730061006E0063006500
      3A0001004200690072007400680070006C006100630065003A0001000D000A00
      4C0062006C00420065006E006500660032004200690072007400680050006C00
      61006300650001004700650062006F006F0072007400650070006C0061006100
      740073003A0001004C0069006500750020006400650020006E00610069007300
      730061006E00630065003A0001004200690072007400680070006C0061006300
      65003A0001000D000A004C0062006C004200690072007400680050006C006100
      6300650049006E007300750072006500640050006500720073006F006E000100
      4700650062006F006F0072007400650070006C0061006100740073003A000100
      4C0069006500750020006400650020006E00610069007300730061006E006300
      65003A0001004200690072007400680070006C006100630065003A0001000D00
      0A004C0062006C004900640065006E0074006900740079006300610072006400
      560061006C006900640055006E00740069006C0049006E007300750072006500
      640050006500720073006F006E000100470065006C0064006900670020007400
      6F0074003A000100560061006C00610062006C00650020006A00750073007100
      75002700610075003A000100560061006C0069006400200075006E0074006900
      6C003A0001000D000A004C0062006C00530065007800420065006E0065006600
      320001004700650073006C0061006300680074003A0001005300650078006500
      3A0001005300650078003A0001000D000A004C0062006C005300650078004900
      6E007300750072006500640050006500720073006F006E000100470065007300
      6C0061006300680074003A00010053006500780065003A000100530065007800
      3A0001000D000A004C0062006C00530065007800420065006E00650066003100
      01004700650073006C0061006300680074003A00010053006500780065003A00
      01005300650078003A0001000D000A004C0062006C0057006500690067006800
      740049006E007300750072006500640050006500720073006F006E0001004700
      650077006900630068007400200028006B00670029003A00010050006F006900
      64007300200028006B00670029003A0001005700650069006700680074002000
      28006B00670029003A0001000D000A004C0062006C004D006F00620069006C00
      65004E0075006D0062006500720049006E007300750072006500640050006500
      720073006F006E000100470073006D003A000100470073006D003A0001004700
      73006D003A0001000D000A004C0062006C004900420061006E00010049004200
      41004E003A0001004900420041004E003A0001004900420041004E003A000100
      0D000A004C0062006C005400690074006C0065004900640065006E0074006900
      6600690063006100740069006F006E0049006E00730075007200650064005000
      6500720073006F006E0001004900640065006E00740069006600690063006100
      7400690065002000760061006E0020006400650020007600650072007A006500
      6B006500720069006E0067006E0065006D006500720020002800690064006500
      6E007400690065006B002000610061006E002000640065002000760065007200
      7A0065006B006500720064006500290001004900640065006E00740069006600
      690063006100740069006F006E0020006400750020007000720065006E006500
      75007200200028006900640065006E00740069007100750065002000E0002000
      6C00270061007300730075007200E900290001004900640065006E0074006900
      6600690063006100740069006F006E0020006F00660020007400680065002000
      70006F006C0069006300790068006F006C006400650072002000280069006400
      65006E0074006900630061006C00200074006F00200074006800650020006900
      6E0073007500720065006400290001000D000A0043006800650063006B004200
      6F00780044006F006D006900630069006C0069006100740069006F006E004600
      6900660074006800010049006E0063006100730073006F006400610074007500
      6D0020006F007000200064006500200035006400650020007700650072006B00
      6400610067000100440061007400650020006400270065006E006C00E8007600
      65006D0065006E00740020006C00650020003500E8006D00650020006A006F00
      7500720020006F00750076007200610062006C006500010043006F006C006C00
      65006300740069006F006E002000640061007400650020006F006E0020007400
      680065002000350074006800200077006F0072006B0069006E00670020006400
      6100790001000D000A004C0062006C0053007400610072007400440061007400
      650043006F006E0074007200610063007400010049006E00670061006E006700
      730064006100740075006D002000760061006E00200068006500740020006300
      6F006E00740072006100630074003A0001004400610074006500200064002700
      65006E0074007200E9006500200065006E002000760069006700750065007500
      7200200064007500200063006F006E0074007200610074003A00010045006600
      66006500630074006900760065002000640061007400650020006F0066002000
      740068006500200063006F006E00740072006100630074003A0001000D000A00
      43006800650063006B0042006F00780049006E00730075007200650064005300
      6D006F006B006500720059006500730001004A00610001004F00750069000100
      59006500730001000D000A0043006800650063006B0042006F00780046007200
      65007100750065006E006300790059006500610072006C00790001004A006100
      610072006C0069006A006B007300010041006E006E00750065006C0001004100
      6E006E00750061006C0001000D000A004C0062006C00550070004C006F006100
      640044006F006300370001004B006F0070006900650020004900440020006B00
      610061007200740020007600650072007A0065006B006500720069006E006700
      73006E0065006D0065007200200028002A002900010043006F00700069006500
      2000630061007200740065002000640027006900640065006E00740069007400
      E90020006400750020007000720065006E00650075007200200028002A002900
      010043006F007000790020006F006600200070006F006C006900630079006800
      6F006C0064006500720020004900440020006300610072006400200028002A00
      290001000D000A004C0062006C00420065006E0065006600320043006F007500
      6E0074007200790001004C0061006E0064003A00010050006100790073003A00
      010043006F0075006E007400720079003A0001000D000A004C0062006C004300
      6F00720072006500730070006F006E00640061006E006300650043006F007500
      6E0074007200790001004C0061006E0064003A00010050006100790073003A00
      010043006F0075006E007400720079003A0001000D000A004C0062006C004300
      6F0075006E0074007200790049006E0073007500720065006400500065007200
      73006F006E0001004C0061006E0064003A00010050006100790073003A000100
      43006F0075006E007400720079003A0001000D000A004C0062006C0042006500
      6E0065006600310043006F0075006E0074007200790001004C0061006E006400
      3A00010050006100790073003A00010043006F0075006E007400720079003A00
      01000D000A004C0062006C004C0065006E0067007400680049006E0073007500
      72006500640050006500720073006F006E0001004C0065006E00670074006500
      2000280063006D0029003A0001005400610069006C006C006500200028006300
      6D0029003A0001004C0065006E006700740068002000280063006D0029003A00
      01000D000A004C0062006C004C0069006E006B0049006E007300750072006500
      6400420065006E0065006600310001004C0069006E006B0020006D0065007400
      20006400650020007600650072007A0065006B006500720069006E0067007300
      6E0065006D00650072003A0001004C00690065006E0020006100760065006300
      20006C00650020007000720065006E006500750072003A0001004C0069006E00
      6B00200077006900740068002000740068006500200070006F006C0069006300
      790068006F006C006400650072003A0001000D000A004C0062006C004C006900
      6E006B0049006E0073007500720065006400420065006E006500660033000100
      4C0069006E006B0020006D006500740020006400650020007600650072007A00
      65006B006500720069006E00670073006E0065006D00650072003A0001004C00
      690065006E002000610076006500630020006C00650020007000720065006E00
      6500750072003A0001004C0069006E006B002000770069007400680020007400
      68006500200070006F006C006900630079006F006C006400650072003A000100
      0D000A0043006800650063006B0042006F007800420065006E00650066003100
      5300650078004D0061006C00650001004D0001004D0001004D0001000D000A00
      43006800650063006B0042006F007800420065006E0065006600320053006500
      78004D0061006C00650001004D0001004D0001004D0001000D000A0043006800
      650063006B0042006F007800420065006E006500660034005300650078004D00
      61006C00650001004D0001004D0001004D0001000D000A004300680065006300
      6B0042006F007800420065006E006500660033005300650078004D0061006C00
      650001004D0001004D0001004D0001000D000A0043006800650063006B004200
      6F0078004600720065007100750065006E00630079004D006F006E0074006800
      6C00790001004D00610061006E00640065006C0069006A006B00730001004D00
      65006E007300750065006C0001004D006F006E00740068006C00790001000D00
      0A0043006800650063006B0042006F0078005300650078004D0061006C006500
      49006E007300750072006500640050006500720073006F006E0001004D006100
      6E00010048006F006D006D00650001004D0061006E0001000D000A004C006200
      6C00550070004C006F006100640044006F006300330001004D00650064006900
      73006300680065002000760072006100670065006E006C0069006A0073007400
      200028002A00290001005100750065007300740069006F006E006E0061006900
      7200650020006D00E90064006900630061006C00200028002A00290001004D00
      650064006900630061006C0020007100750065007300740069006F006E006E00
      6100690072006500200028002A00290001000D000A0043006800650063006B00
      42006F0078004D006F0072006500440065007400610069006C006C0065006400
      420065006E006500660043006C00610075007300650001004D00650065007200
      200067006500640065007400610069006C006C00650065007200640065002000
      62006500670075006E0073007400690067006400650063006C00610075007300
      75006C006500010043006C00610075007300650020006200E9006E00E9006600
      6900630069006100690072006500200070006C007500730020006400E9007400
      610069006C006C00E900650001004D006F007200650020006400650074006100
      69006C00650064002000620065006E0065006600690063006900610072007900
      200063006C00610075007300650001000D000A0043006800650063006B004200
      6F00780041007000700065006C006100740069006F006E0057006F006D006500
      6E00420065006E0065006600340001004D006500760072006F00750077000100
      4D006D00650001004D007200730001000D000A0043006800650063006B004200
      6F00780041007000700065006C006100740069006F006E00460065006D006100
      6C00650001004D006500760072006F007500770001004D006D00650001004D00
      7200730001000D000A0043006800650063006B0042006F00780043006F007200
      72006500730070006F006E00640061006E0063006500460065006D0061006C00
      650001004D006500760072006F007500770001004D006D00650001004D007200
      730001000D000A0043006800650063006B0042006F0078004100700070006500
      6C006100740069006F006E0057006F006D0065006E00420065006E0065006600
      310001004D006500760072006F007500770001004D006D00650001004D007200
      730001000D000A0043006800650063006B0042006F0078004100700070006500
      6C006100740069006F006E0057006F006D0065006E00420065006E0065006600
      320001004D006500760072006F007500770001004D006D00650001004D007200
      730001000D000A0043006800650063006B0042006F0078004100700070006500
      6C006100740069006F006E0057006F006D0065006E00420065006E0065006600
      330001004D006500760072006F007500770001004D006D00650001004D007200
      730001000D000A004C0062006C00420065006E006500660031004E0061006D00
      650001004E00610061006D0020002F00200073006F006300690061006C006500
      200072006500640065006E003A0001004E006F006D0020002F00200072006100
      690073006F006E00200073006F006300690061006C0065003A0001004E006100
      6D00650020002F00200073006F006300690061006C0020007200650061007300
      6F006E003A0001000D000A004C0062006C00420065006E006500660033004E00
      61006D00650001004E00610061006D0020002F00200073006F00630069006100
      6C006500200072006500640065006E003A0001004E006F006D0020002F002000
      72006100690073006F006E00200073006F006300690061006C0065003A000100
      4E0061006D00650020002F00200073006F006300690061006C00200072006500
      610073006F006E003A0001000D000A004C0062006C0043006F00720072006500
      730070006F006E00640061006E00630065004E0061006D00650001004E006100
      61006D002C00200076006F006F0072006E00610061006D0020006F0066002000
      6600690072006D0061006E00610061006D003A0001004E006F006D002C002000
      70007200E9006E006F006D0020006F007500200072006100690073006F006E00
      200073006F006300690061006C0065003A0001005300750072006E0061006D00
      65002C0020006600690072007300740020006E0061006D00650020006F007200
      200063006F006D00700061006E00790020006E0061006D0065003A0001000D00
      0A004C0062006C004E0061006D00650049006E00730075007200650064005000
      6500720073006F006E0001004E00610061006D003A0001004E006F006D003A00
      01004E0061006D0065003A0001000D000A004C0062006C004E00610074006900
      6F006E0061006C0069007400790049006E007300750072006500640050006500
      720073006F006E0001004E006100740069006F006E0061006C00690074006500
      690074003A0001004E006100740069006F006E0061006C0069007400E9003A00
      01004E006100740069006F006E0061006C006900740079003A0001000D000A00
      43006800650063006B0042006F00780049006E00730075007200650064005300
      6D006F006B00650072004E006F0001004E006500650001004E006F006E000100
      4E006F0001000D000A004C0062006C004900640065006E007400690074007900
      63006100720064006E0075006D0062006500720049006E007300750072006500
      640050006500720073006F006E0001004E0075006D006D006500720020006900
      640065006E0074006900740065006900740073006B0061006100720074003A00
      01004E0075006D00E90072006F00200064006500200063006100720074006500
      2000640027006900640065006E00740069007400E9003A000100490044002000
      630061007200640020006E0075006D006200650072003A0001000D000A004200
      74006E00530061007600650001004F00700073006C00610061006E0001005300
      6100750076006500670061007200640065007200010053006100760065000100
      0D000A004C0062006C00500065007200630065006E0074006100670065004200
      65006E006500660032000100500065007200630065006E007400610067006500
      3A00010050006F0075007200630065006E0074006100670065003A0001005000
      65007200630065006E0074006100670065003A0001000D000A004C0062006C00
      500065007200630065006E007400610067006500420065006E00650066003100
      0100500065007200630065006E0074006100670065003A00010050006F007500
      7200630065006E0074006100670065003A000100500065007200630065006E00
      74006100670065003A0001000D000A004C0062006C0050006500720063006500
      6E007400610067006500420065006E0065006600340001005000650072006300
      65006E0074006100670065003A00010050006F0075007200630065006E007400
      6100670065003A000100500065007200630065006E0074006100670065003A00
      01000D000A004C0062006C00500065007200630065006E007400610067006500
      420065006E006500660033000100500065007200630065006E00740061006700
      65003A00010050006F0075007200630065006E0074006100670065003A000100
      500065007200630065006E0074006100670065003A0001000D000A004C006200
      6C005400690074006C00650043006F00720072006500730070006F006E006400
      61006E00630065004100640064007200650073007300010050006F0073007400
      6100640072006500730001004100640072006500730073006500200064006500
      200063006F00720072006500730070006F006E00640061006E00630065000100
      4D00610069006C0069006E006700200061006400640072006500730073000100
      0D000A004C0062006C0050006F007300740061006C0043006F00640065004900
      6E007300750072006500640050006500720073006F006E00010050006F007300
      740063006F00640065003A00010043006F0064006500200070006F0073007400
      61006C003A00010050006F007300740061006C00200043006F00640065003A00
      01000D000A004C0062006C00420065006E0065006600310050006F0073007400
      61006C0043006F0064006500010050006F007300740063006F00640065003A00
      010043006F0064006500200070006F007300740061006C003A00010050006F00
      7300740061006C00200043006F00640065003A0001000D000A004C0062006C00
      420065006E0065006600320050006F007300740061006C0043006F0064006500
      010050006F007300740063006F00640065003A00010043006F00640065002000
      70006F007300740061006C003A00010050006F007300740061006C0020004300
      6F00640065003A0001000D000A004C0062006C0043006F007200720065007300
      70006F006E00640061006E00630065005A006900700043006F00640065000100
      50006F007300740063006F00640065003A00010043006F006400650020007000
      6F007300740061006C003A00010050006F007300740061006C00200043006F00
      640065003A0001000D000A004C0062006C00500072006F006600650073007300
      69006F006E0061006C004100630074006900760069007400790049006E007300
      750072006500640050006500720073006F006E000100500072006F0066006500
      7300730069006F006E0065006C00650020006100630074006900760069007400
      6500690074003A0001004100630074006900760069007400E900200070007200
      6F00660065007300730069006F006E006E0065006C006C0065003A0001005000
      72006F00660065007300730069006F006E0061006C0020006100630074006900
      76006900740079003A0001000D000A004C0062006C0052006500660065007200
      65006E00630065004D0061006E00640061007400650001005200650066006500
      720065006E0074006900650020006D0061006E0064006100610074003A000100
      5200E9006600E900720065006E006300650020006D0061006E00640061007400
      3A0001005200650066006500720065006E006300650020006D0061006E006400
      6100740065003A0001000D000A004C0062006C0049006E007300750072006500
      640053006D006F006B0065007200010052006F006F006B006700650064007200
      610067003A00010043006F006D0070006F007200740065006D0065006E007400
      2000660075006D006500750072003A00010053006D006F006B0069006E006700
      20006200650068006100760069006F0072003A0001000D000A004C0062006C00
      5400690074006C006500530065007000610049006E0063006100730073006F00
      0100530065007000610020006100750074006F006D0061007400690073006300
      68006500200069006E0063006100730073006F00010050007200E9006C00E800
      760065006D0065006E0074002000530065007000610001005300650070006100
      200064006900720065006300740020006400650062006900740001000D000A00
      4C0062006C00550070004C006F006100640044006F0063003600010053006500
      70006100200066006F0072006D0075006C00690065007200200028002A002900
      010046006F0072006D0075006C00610069007200650020005300650070006100
      200028002A00290001005300650070006100200066006F0072006D0020002800
      2A00290001000D000A004C0062006C0043006F00720072006500730070006F00
      6E00640061006E00630065004C006F0063006100740069006F006E0001005300
      7400610064003A000100560069006C006C0065003A0001004300690074007900
      3A0001000D000A004C0062006C00420065006E006500660032004C006F006300
      6100740069006F006E00010053007400610064003A000100560069006C006C00
      65003A00010043006900740079003A0001000D000A004C0062006C0043006900
      7400790049006E007300750072006500640050006500720073006F006E000100
      53007400610064003A000100560069006C006C0065003A000100430069007400
      79003A0001000D000A004C0062006C00420065006E006500660031004C006F00
      63006100740069006F006E00010053007400610064003A000100560069006C00
      6C0065003A00010043006900740079003A0001000D000A004300680065006300
      6B0042006F0078005300740061006E00640061007200640043006C0061007500
      7300650001005300740061006E0064006100610072006400200063006C006100
      7500730075006C006500010043006C0061007500730065002000730074006100
      6E00640061007200640001005300740061006E00640061007200640020006300
      6C00610075007300650001000D000A004C0062006C0043006F00720072006500
      730070006F006E00640061006E00630065004100640064007200650073007300
      010053007400720061006100740020002F0020006E0075006D006D0065007200
      20002F0020006200750073003A00010052007500650020002F0020006E007500
      6D00E90072006F0020002F00200062006F00EE00740065003A00010053007400
      720065006500740020002F0020006E0075006D0062006500720020002F002000
      62006F0078003A0001000D000A004C0062006C00530074007200650065007400
      4E0075006D0062006500720042006F00780049006E0073007500720065006400
      50006500720073006F006E00010053007400720061006100740020002F002000
      6E0075006D006D006500720020002F0020006200750073003A00010052007500
      650020002F0020006E0075006D00E90072006F0020002F00200062006F00EE00
      740065003A00010053007400720065006500740020002F0020006E0075006D00
      62006500720020002F00200062006F0078003A0001000D000A004C0062006C00
      420065006E0065006600320053007400720065006500740061006E0064004E00
      75006D006200650072000100530074007200610061007400200065006E002000
      6E0075006D006D00650072003A00010052007500650020006500740020006E00
      75006D00E90072006F003A000100530074007200650065007400200061006E00
      640020006E0075006D006200650072003A0001000D000A004C0062006C004200
      65006E0065006600310053007400720065006500740061006E0064004E007500
      6D006200650072000100530074007200610061007400200065006E0020006E00
      75006D006D00650072003A00010052007500650020006500740020006E007500
      6D00E90072006F003A000100530074007200650065007400200061006E006400
      20006E0075006D006200650072003A0001000D000A0043006800650063006B00
      42006F00780043006F00720072006500730070006F006E00640061006E006300
      65004F00740068006500720050006500720073006F006E000100530074007500
      75007200200063006F00720072006500730070006F006E00640065006E007400
      6900650020006E00610061007200200068006500740020006100640072006500
      73002000760061006E0020006400650020006F006E0064006500720073007400
      610061006E006400650020006400650072006400650020007000610072007400
      69006A003A000100500072006900E8007200650020006400270065006E007600
      6F0079006500720020006C006100200063006F00720072006500730070006F00
      6E00640061006E00630065002000E00020006C00270061006400720065007300
      7300650020006400750020007400690065007200730020006400E90073006900
      67006E00E9002000630069002D0064006500730073006F00750073003A000100
      50006C0065006100730065002000730065006E006400200063006F0072007200
      6500730070006F006E00640065006E0063006500200074006F00200074006800
      65002000610064006400720065007300730020006F0066002000740068006500
      20007400680069007200640020007000610072007400790020006E0061006D00
      650064002000620065006C006F0077003A0001000D000A004C0062006C005300
      77006900730073004C00690066006500500072006F0074006500630074004200
      65006C006700690075006D0001005300770069007300730020004C0069006600
      65002000500072006F0074006500630074002000420065006C00670069007500
      6D0001005300770069007300730020004C006900660065002000500072006F00
      74006500630074002000420065006C006700690075006D000100530077006900
      7300730020004C006900660065002000500072006F0074006500630074002000
      420065006C006700690075006D0001000D000A004C0062006C00500068006F00
      6E0065004E0075006D0062006500720049006E00730075007200650064005000
      6500720073006F006E000100540065006C00650066006F006F006E003A000100
      5400E9006C00E900700068006F006E0065003A000100500068006F006E006500
      3A0001000D000A004C0062006C005400690074006C006500420065006E006500
      66006900630069006100720079003200010054006900740065006C003A000100
      540069007400720065003A0001005400690074006C0065003A0001000D000A00
      4C0062006C005400690074006C00650049006E00730075007200650064005000
      6500720073006F006E00010054006900740065006C003A000100540069007400
      720065003A0001005400690074006C0065003A0001000D000A004C0062006C00
      5400690074006C006500420065006E0065006600690063006900610072007900
      3100010054006900740065006C003A000100540069007400720065003A000100
      5400690074006C0065003A0001000D000A0043006800650063006B0042006F00
      78004600720065007100750065006E00630079005400720069006D0065007300
      7400650072006C00790001005400720069006D00650073007400720069006500
      65006C0001005400720069006D006500730074007200690065006C0001005100
      7500610072007400650072006C00790001000D000A0043006800650063006B00
      42006F007800420065006E00650066003300530065007800460065006D006100
      6C00650001005600010046000100460001000D000A0043006800650063006B00
      42006F007800420065006E00650066003100530065007800460065006D006100
      6C00650001005600010046000100460001000D000A0043006800650063006B00
      42006F007800420065006E00650066003400530065007800460065006D006100
      6C00650001005600010046000100460001000D000A0043006800650063006B00
      42006F007800420065006E00650066003200530065007800460065006D006100
      6C00650001005600010046000100460001000D000A004C0062006C0045006E00
      6400440061007400650043006F006E0074007200610063007400010056006500
      7200760061006C0064006100740075006D002000760061006E00200068006500
      7400200063006F006E00740072006100630074003A0001004400610074006500
      200064002700650078007000690072006100740069006F006E00200064007500
      200063006F006E0074007200610074003A000100450078007000690072007900
      2000640061007400650020006F0066002000740068006500200063006F006E00
      740072006100630074003A0001000D000A004C0062006C005400690074006C00
      65004300610070006900740061006C0041006E00640044007500720061007400
      69006F006E0001005600650072007A0065006B0065007200640020006B006100
      700069007400610061006C00200065006E00200063006F006E00740072006100
      63007400640075007500720001004300610070006900740061006C0020006100
      7300730075007200E9002000650074002000640075007200E900650020006400
      7500200063006F006E007400720061007400010049006E007300750072006500
      640020006300610070006900740061006C00200061006E006400200063006F00
      6E007400720061006300740020006400750072006100740069006F006E000100
      0D000A004C0062006C00410073007300690067006E006D0065006E0074005000
      65007200630065006E007400610067006500420065006E006500660031000100
      56006F006C0067006F007200640065002000760061006E00200074006F006500
      770069006A007A00670069006E006700200065006E0020007000650072006300
      65006E0074006100670065003A0001004F007200640072006500200064002700
      6100740074007200690062007500740069006F006E0020006500740020007000
      6F0075007200630065006E0074006100670065003A0001004F00720064006500
      720020006F006600200061006C006C006F0063006100740069006F006E002000
      61006E0064002000700065007200630065006E0074006100670065003A000100
      0D000A004C0062006C00410073007300690067006E006D0065006E0074005000
      65007200630065006E007400610067006500420065006E006500660032000100
      56006F006C0067006F007200640065002000760061006E00200074006F006500
      770069006A007A00670069006E006700200065006E0020007000650072006300
      65006E0074006100670065003A0001004F007200640072006500200064002700
      6100740074007200690062007500740069006F006E0020006500740020007000
      6F0075007200630065006E0074006100670065003A0001004F00720064006500
      720020006F006600200061006C006C006F0063006100740069006F006E002000
      61006E0064002000700065007200630065006E0074006100670065003A000100
      0D000A004C0062006C00520061006E006B00420065006E006500660033000100
      56006F006C0067006F007200640065003A0001004F0072006400720065003A00
      01004F0072006400650072003A0001000D000A004C0062006C00520061006E00
      6B00420065006E00650066003400010056006F006C0067006F00720064006500
      3A0001004F0072006400720065003A0001004F0072006400650072003A000100
      0D000A004C0062006C00520061006E006B00420065006E006500660032000100
      56006F006C0067006F007200640065003A0001004F0072006400720065003A00
      01004F0072006400650072003A0001000D000A004C0062006C00520061006E00
      6B00420065006E00650066003100010056006F006C0067006F00720064006500
      3A0001004F0072006400720065003A0001004F0072006400650072003A000100
      0D000A004C0062006C00460069007200730074004E0061006D00650049006E00
      7300750072006500640050006500720073006F006E00010056006F006F007200
      6E00610061006D003A00010050007200E9006E006F006D003A00010046006900
      72007300740020004E0061006D0065003A0001000D000A004C0062006C004200
      65006E00650066003300460069007200730074004E0061006D00650001005600
      6F006F0072006E00610061006D003A00010050007200E9006E006F006D003A00
      01004600690072007300740020004E0061006D0065003A0001000D000A004C00
      62006C00420065006E00650066003100460069007200730074004E0061006D00
      6500010056006F006F0072006E00610061006D003A00010050007200E9006E00
      6F006D003A0001004600690072007300740020004E0061006D0065003A000100
      0D000A004C0062006C00500072006F0070006F0073006900740069006F006E00
      010056006F006F0072007300740065006C000100500072006F0070006F007300
      6900740069006F006E000100500072006F0070006F00730061006C0001000D00
      0A004C0062006C00550070004C006F006100640044006F006300310001005600
      6F006F0072007300740065006C00200028002A0029000100500072006F007000
      6F0073006900740069006F006E00200028002A0029000100500072006F007000
      6F00730061006C00200028002A00290001000D000A0043006800650063006B00
      42006F007800530065007800460065006D0061006C00650049006E0073007500
      72006500640050006500720073006F006E000100560072006F00750077000100
      460065006D006D006500010057006F006D0061006E0001000D000A004C006200
      6C00550070004C006F006100640044006F006300380001004200650077006900
      6A0073002000760061006E00200077006F006F006E0073007400200028002A00
      2900010050007200650075007600650020006400650020007200E90073006900
      640065006E0063006500200028002A0029000100500072006F006F0066002000
      6F00660020007200650073006900640065006E0063006500200028002A002900
      01000D000A004C0062006C004C0061006E006700750061006700650001005400
      610061006C003A0001004C0061006E006700750065003A0001004C0061006E00
      670075006100670065003A0001000D000A0073007400480069006E0074007300
      5F0055006E00690063006F00640065000D000A00420074006E00550070006C00
      6F006100640044006F00630031000100550070006C006F006100640065006E00
      01005400E9006C00E90063006800610072006700650072000100550070006C00
      6F006100640001000D000A00420074006E00550070006C006F00610064004400
      6F00630032000100550070006C006F006100640065006E0001005400E9006C00
      E90063006800610072006700650072000100550070006C006F00610064000100
      0D000A00420074006E00550070006C006F006100640044006F00630033000100
      550070006C006F006100640065006E0001005400E9006C00E900630068006100
      72006700650072000100550070006C006F006100640001000D000A0042007400
      6E00550070006C006F006100640044006F00630034000100550070006C006F00
      6100640065006E0001005400E9006C00E9006300680061007200670065007200
      0100550070006C006F006100640001000D000A00420074006E00550070006C00
      6F006100640044006F00630035000100550070006C006F006100640065006E00
      01005400E9006C00E90063006800610072006700650072000100550070006C00
      6F006100640001000D000A00420074006E00550070006C006F00610064004400
      6F00630036000100550070006C006F006100640065006E0001005400E9006C00
      E90063006800610072006700650072000100550070006C006F00610064000100
      0D000A00420074006E00550070006C006F006100640044006F00630037000100
      550070006C006F006100640065006E0001005400E9006C00E900630068006100
      72006700650072000100550070006C006F006100640001000D000A0042007400
      6E00550070006C006F006100640044006F00630038000100550070006C006F00
      6100640065006E0001005400E9006C00E9006300680061007200670065007200
      0100550070006C006F006100640001000D000A00420074006E00440065006C00
      65007400650044006F00630031000100560065007200770069006A0064006500
      720065006E0001005300750070007000720069006D0065007200010044006500
      6C0065007400650001000D000A00420074006E00440065006C00650074006500
      44006F00630032000100560065007200770069006A0064006500720065006E00
      01005300750070007000720069006D00650072000100440065006C0065007400
      650001000D000A00420074006E00440065006C0065007400650044006F006300
      33000100560065007200770069006A0064006500720065006E00010053007500
      70007000720069006D00650072000100440065006C0065007400650001000D00
      0A00420074006E00440065006C0065007400650044006F006300340001005600
      65007200770069006A0064006500720065006E00010053007500700070007200
      69006D00650072000100440065006C0065007400650001000D000A0042007400
      6E00440065006C0065007400650044006F006300350001005600650072007700
      69006A0064006500720065006E0001005300750070007000720069006D006500
      72000100440065006C0065007400650001000D000A00420074006E0044006500
      6C0065007400650044006F00630036000100560065007200770069006A006400
      6500720065006E0001005300750070007000720069006D006500720001004400
      65006C0065007400650001000D000A00420074006E00440065006C0065007400
      650044006F00630037000100560065007200770069006A006400650072006500
      6E0001005300750070007000720069006D00650072000100440065006C006500
      7400650001000D000A00420074006E00440065006C0065007400650044006F00
      630038000100560065007200770069006A0064006500720065006E0001005300
      750070007000720069006D00650072000100440065006C006500740065000100
      0D000A007300740044006900730070006C00610079004C006100620065006C00
      73005F0055006E00690063006F00640065000D000A007300740046006F006E00
      740073005F0055006E00690063006F00640065000D000A00730074004D007500
      6C00740069004C0069006E00650073005F0055006E00690063006F0064006500
      0D000A0043006F006D0062006F0043006900760069006C007300740061007400
      650049006E007300750072006500640050006500720073006F006E002E004900
      740065006D00730001005600720069006A00670065007A0065006C002C004700
      650068007500770064002C005700650064007500770065002C00470065007300
      6300680065006900640065006E002C002200570065007400740065006C006900
      6A006B002000730061006D0065006E0077006F006E0065006E00640022002C00
      2200460065006900740065006C0069006A006B002000730061006D0065006E00
      77006F006E0065006E006400220001004300E9006C0069006200610074006100
      6900720065002C004D00610072006900E9002C0056006500750066002C004400
      690076006F0072006300E9002C00220043006F00680061006200690074006100
      740069006F006E0020006C00E900670061006C00650022002C00220043006F00
      680061006200690074006100740069006F006E00200064006500200066006100
      6900740022000100530069006E0067006C0065002C004D006100720072006900
      650064002C005700690064006F007700650072002C004400690076006F007200
      6300650064002C0022004C006500670061006C00200063006F00680061006200
      690074006100740069006F006E0022002C002200440065002000660061006300
      74006F00200063006F00680061006200690074006100740069006F006E002200
      01000D000A0043006F006D0062006F0049006E00730075007200650064004300
      610070006900740061006C002E004900740065006D0073000100350030003000
      30002C0036003000300030002C0037003000300030002C003800300030003000
      2C0039003000300030002C00310030003000300030002C003100310030003000
      30002C00310032003000300030002C00310033003000300030002C0031003400
      3000300030002C00310035003000300030002C00310036003000300030002C00
      310037003000300030002C00310038003000300030002C003100390030003000
      30002C00320030003000300030002C00320031003000300030002C0032003200
      3000300030002C00320033003000300030002C00320034003000300030002C00
      32003500300030003000010035003000300030002C0036003000300030002C00
      37003000300030002C0038003000300030002C0039003000300030002C003100
      30003000300030002C00310031003000300030002C0031003200300030003000
      2C00310033003000300030002C00310034003000300030002C00310035003000
      300030002C00310036003000300030002C00310037003000300030002C003100
      38003000300030002C00310039003000300030002C0032003000300030003000
      2C00320031003000300030002C00320032003000300030002C00320033003000
      300030002C00320034003000300030002C003200350030003000300001003500
      3000300030002C0036003000300030002C0037003000300030002C0038003000
      300030002C0039003000300030002C00310030003000300030002C0031003100
      3000300030002C00310032003000300030002C00310033003000300030002C00
      310034003000300030002C00310035003000300030002C003100360030003000
      30002C00310037003000300030002C00310038003000300030002C0031003900
      3000300030002C00320030003000300030002C00320031003000300030002C00
      320032003000300030002C00320033003000300030002C003200340030003000
      30002C003200350030003000300001000D000A0043006F006D0062006F006200
      6F0078004C0061006E00670075006100670065002E004900740065006D007300
      01004E0065006400650072006C0061006E00640073002C004600720061006E00
      730001004E00E900650072006C0061006E0064006100690073002C0046007200
      61006E00E7006100690073000100440075007400630068002C00460072006500
      6E006300680001000D000A007300740053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A007300740072007300740072005F00
      64006F00630075006D0065006E0074005F0061006C0072006500610064007900
      5F00700072006500730065006E00740001004500720020006900730020007200
      65006500640073002000650065006E00200064006F00630075006D0065006E00
      74002000610061006E00770065007A0069006700010055006E00200064006F00
      630075006D0065006E007400200065007300740020006400E9006A00E0002000
      64006900730070006F006E00690062006C00650001004100200064006F006300
      75006D0065006E007400200069007300200061006C0072006500610064007900
      200061007600610069006C00610062006C00650001000D000A00730074007200
      7300740072005F006500720072006F0072005F0061006400640069006E006700
      5F00640065006D0061006E006400010046006F00750074002000620069006A00
      2000680065007400200074006F00650076006F006500670065006E0020007600
      61006E002000640065002000610061006E007600720061006100670001004500
      7200720065007500720020006C006F007200730020006400650020006C002700
      61006A006F007500740020006400650020006C0061002000640065006D006100
      6E006400650001004500720072006F007200200061006400640069006E006700
      200074006800650020006100700070006C00690063006100740069006F006E00
      01000D000A007300740072007300740072005F00640065006D0061006E006400
      5F0069006E005F007500730065000100440065002000610061006E0076007200
      610061006700200069007300200069006E002000670065006200720075006900
      6B002C002000640065007A00650020006B0061006E0020006D006F006D006500
      6E007400650065006C0020006E006900650074002000610061006E0067006500
      7000610073007400200077006F007200640065006E0001004C00610020006400
      65006D0061006E00640065002000650073007400200065006E00200063006F00
      7500720073002000640027007500740069006C00690073006100740069006F00
      6E002C00200065006C006C00650020006E006500200070006500750074002000
      7000610073002000EA0074007200650020006D006F006400690066006900E900
      6500200070006F007500720020006C00650020006D006F006D0065006E007400
      010054006800650020006100700070006C00690063006100740069006F006E00
      200069007300200069006E0020007500730065002C0020006900740020006300
      61006E006E006F00740020006200650020006500640069007400650064002000
      61007400200074006800650020006D006F006D0065006E00740001000D000A00
      7300740072007300740072005F00730075006200730063007200690070007400
      69006F006E006E0075006D006200650072005F00720065007100750069007200
      6500640001004800650074002000610062006F006E006E0065006D0065006E00
      74007300610061006E00760072006100610067006E0075006D006D0065007200
      2000690073002000650065006E00200076006500720070006C00690063006800
      74006500200069006E00670061007600650001004C00650020006E0075006D00
      E90072006F002000640065002000640065006D0061006E006400650020006400
      6500200073006F00750073006300720069007000740069006F006E0020006500
      73007400200075006E006500200073006100690073006900650020006F006200
      6C0069006700610074006F006900720065000100540068006500200073007500
      620073006300720069007000740069006F006E00200072006500710075006500
      7300740020006E0075006D006200650072002000690073002000610020006D00
      61006E006400610074006F0072007900200065006E0074007200790001000D00
      0A007300740072007300740072005F007400690074006C0065005F0072006500
      710075006900720065006400010044006500200074006900740065006C002000
      690073002000650065006E00200076006500720070006C006900630068007400
      6500200069006E00670061007600650001004C00650020007400690074007200
      65002000650073007400200075006E0065002000730061006900730069006500
      20006F0062006C0069006700610074006F006900720065000100540068006500
      20007400690074006C0065002000690073002000610020006D0061006E006400
      610074006F0072007900200065006E0074007200790001000D000A0073007400
      72007300740072005F006E0061006D0065005F00720065007100750069007200
      6500640001004400650020006E00610061006D00200069007300200065006500
      6E00200076006500720070006C0069006300680074006500200069006E006700
      61007600650001004C00650020006E006F006D00200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F00690072006500010054006800650020006E0061006D00650020006900
      73002000610020006D0061006E006400610074006F0072007900200065006E00
      74007200790001000D000A007300740072007300740072005F00660069007200
      730074006E0061006D0065005F00720065007100750069007200650064000100
      44006500200076006F006F0072006E00610061006D0020006900730020006500
      65006E00200076006500720070006C0069006300680074006500200069006E00
      670061007600650001004C006500200070007200E9006E006F006D0020006500
      73007400200075006E006500200073006100690073006900650020006F006200
      6C0069006700610074006F006900720065000100540068006500200066006900
      72007300740020006E0061006D0065002000690073002000610020006D006100
      6E006400610074006F0072007900200065006E0074007200790001000D000A00
      7300740072007300740072005F007300740072006500650074005F0072006500
      7100750069007200650064000100440065002000730074007200610061007400
      2000690073002000650065006E00200076006500720070006C00690063006800
      74006500200069006E00670061007600650001004C0061002000720075006500
      2000650073007400200075006E00650020007300610069007300690065002000
      6F0062006C0069006700610074006F0069007200650001005400680065002000
      7300740072006500650074002000690073002000610020006D0061006E006400
      610074006F0072007900200065006E0074007200790001000D000A0073007400
      72007300740072005F007300740072006500650074006E0075006D0062006500
      72005F0072006500710075006900720065006400010048006500740020007300
      740072006100610074006E0075006D006D006500720020006900730020006500
      65006E00200076006500720070006C0069006300680074006500200069006E00
      670061007600650001004C00650020006E0075006D00E90072006F0020006400
      650020007200750065002000650073007400200075006E006500200073006100
      690073006900650020006F0062006C0069006700610074006F00690072006500
      0100540068006500200073007400720065006500740020006E0075006D006200
      650072002000690073002000610020006D0061006E006400610074006F007200
      7900200065006E0074007200790001000D000A00730074007200730074007200
      5F0070006F007300740061006C0063006F00640065005F007200650071007500
      6900720065006400010044006500200070006F007300740063006F0064006500
      2000690073002000650065006E00200076006500720070006C00690063006800
      74006500200069006E00670061007600650001004C006500200063006F006400
      6500200070006F007300740061006C002000650073007400200075006E006500
      200073006100690073006900650020006F0062006C0069006700610074006F00
      6900720065000100540068006500200070006F007300740061006C0020006300
      6F00640065002000690073002000610020006D0061006E006400610074006F00
      72007900200065006E0074007200790001000D000A0073007400720073007400
      72005F0063006900740079005F00720065007100750069007200650064000100
      44006500200073007400610064002000690073002000650065006E0020007600
      6500720070006C0069006300680074006500200069006E006700610076006500
      01004C0061002000760069006C006C0065002000650073007400200075006E00
      6500200073006100690073006900650020006F0062006C006900670061007400
      6F00690072006500010054006800650020006300690074007900200069007300
      2000610020006D0061006E006400610074006F0072007900200065006E007400
      7200790001000D000A007300740072007300740072005F006400610074006500
      5F006F0066005F00620069007200740068005F00720065007100750069007200
      6500640001004400650020006700650062006F006F0072007400650064006100
      740075006D002000690073002000650065006E00200076006500720070006C00
      69006300680074006500200069006E00670061007600650001004C0061002000
      640061007400650020006400650020006E00610069007300730061006E006300
      65002000650073007400200075006E0065002000730061006900730069006500
      20006F0062006C0069006700610074006F006900720065000100540068006500
      2000640061007400650020006F00660020006200690072007400680020006900
      73002000610020006D0061006E006400610074006F0072007900200065006E00
      74007200790001000D000A007300740072007300740072005F00620069007200
      7400680070006C006100630065005F0072006500710075006900720065006400
      01004400650020006700650062006F006F0072007400650070006C0061006100
      740073002000690073002000650065006E00200076006500720070006C006900
      6300680074006500200069006E00670061007600650001004C00650020006C00
      69006500750020006400650020006E00610069007300730061006E0063006500
      2000650073007400200075006E00650020007300610069007300690065002000
      6F0062006C0069006700610074006F0069007200650001005400680065002000
      70006C0061006300650020006F00660020006200690072007400680020006900
      73002000610020006D0061006E006400610074006F0072007900200065006E00
      74007200790001000D000A007300740072007300740072005F006E0061007400
      69006F006E0061006C006900740079005F007200650071007500690072006500
      640001004400650020006E006100740069006F006E0061006C00690074006500
      690074002000690073002000650065006E00200076006500720070006C006900
      6300680074006500200069006E00670061007600650001004C00610020006E00
      6100740069006F006E0061006C0069007400E900200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F0069007200650001004E006100740069006F006E0061006C0069007400
      79002000690073002000610020006D0061006E006400610074006F0072007900
      200065006E0074007200790001000D000A007300740072007300740072005F00
      7300650078005F00720065007100750069007200650064000100480065007400
      20006700650073006C0061006300680074002000690073002000650065006E00
      200076006500720070006C0069006300680074006500200069006E0067006100
      7600650001004C00650020007300650078006500200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F006900720065000100470065006E006400650072002000690073002000
      610020006D0061006E006400610074006F0072007900200065006E0074007200
      790001000D000A007300740072007300740072005F0063006900760069006C00
      5F00730074006100740065005F00720065007100750069007200650064000100
      4400650020006200750072006700650072006C0069006A006B00650020007300
      74006100610074002000690073002000650065006E0020007600650072007000
      6C0069006300680074006500200069006E00670061007600650001004C002700
      E900740061007400200063006900760069006C00200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F0069007200650001004D00610072006900740061006C00200073007400
      61007400750073002000690073002000610020006D0061006E00640061007400
      6F0072007900200065006E0074007200790001000D000A007300740072007300
      740072005F00700072006F00660065007300730069006F006E0061006C005F00
      610063007400690076006900740079005F007200650071007500690072006500
      64000100440065002000700072006F00660065007300730069006F006E006500
      6C00650020006100630074006900760069007400650069007400200069007300
      2000650065006E00200076006500720070006C00690063006800740065002000
      69006E00670061007600650001004100630074006900760069007400E9002000
      700072006F00660065007300730069006F006E006E0065006C006C0065002000
      650073007400200075006E006500200073006100690073006900650020006F00
      62006C0069006700610074006F00690072006500010054006800650020007000
      72006F00660065007300730069006F006E0061006C0020006100630074006900
      76006900740079002000690073002000610020006D0061006E00640061007400
      6F0072007900200065006E0074007200790001000D000A007300740072007300
      740072005F006900640063006100720064004E0075006D006200650072005200
      650071007500690072006500640001004E0075006D006D006500720020006900
      640065006E0074006900740065006900740073006B0061006100720074002000
      690073002000650065006E00200076006500720070006C006900630068007400
      6500200069006E00670061007600650001004C00650020006E0075006D00E900
      72006F0020006400650020006300610072007400650020006400270069006400
      65006E00740069007400E9002000650073007400200075006E00650020007300
      6100690073006900650020006F0062006C0069006700610074006F0069007200
      650001004900640065006E007400690074007900200063006100720064002000
      6E0075006D006200650072002000690073002000610020006D0061006E006400
      610074006F0072007900200065006E0074007200790001000D000A0073007400
      72007300740072005F0065006E00640064006100740065005F00690064006300
      6100720064005F00720065007100750069007200650064000100440065002000
      650069006E00640064006100740075006D002000760061006E00200067006500
      6C0064006900670068006500690064002000760061006E002000640065002000
      6900640065006E0074006900740065006900740073006B006100610072007400
      2000690073002000650065006E00200076006500720070006C00690063006800
      74006500200069006E00670061007600650001004C0061002000640061007400
      65002000640065002000660069006E002000640065002000760061006C006900
      640069007400E90020006400650020006C006100200063006100720074006500
      2000640027006900640065006E00740069007400E90020006500730074002000
      75006E006500200073006100690073006900650020006F0062006C0069006700
      610074006F006900720065000100540068006500200065006E00640020006400
      61007400650020006F0066002000760061006C00690064006900740079002000
      6F006600200074006800650020006900640065006E0074006900740079002000
      63006100720064002000690073002000610020006D0061006E00640061007400
      6F0072007900200065006E0074007200790001000D000A007300740072007300
      740072005F00700068006F006E0065005F006F0072005F006D006F0062006900
      6C0065005F00720065007100750069007200650064000100540065006C006500
      66006F006F006E0020006F0066002000670073006D0020006900730020006500
      65006E00200076006500720070006C0069006300680074006500200069006E00
      670061007600650001004C00650020007400E9006C00E900700068006F006E00
      650020006F0075002000670073006D002000650073007400200075006E006500
      200073006100690073006900650020006F0062006C0069006700610074006F00
      6900720065000100540065006C006500700068006F006E00650020006F007200
      20006D006F00620069006C0065002000700068006F006E006500200069007300
      2000610020006D0061006E006400610074006F0072007900200065006E007400
      7200790001000D000A007300740072007300740072005F0065006D0061006900
      6C005F0072006500710075006900720065006400010045002D006D0061006900
      6C002000610064007200650073002000690073002000650065006E0020007600
      6500720070006C0069006300680074006500200069006E006700610076006500
      01004100640072006500730073006500200065002D006D00610069006C002000
      650073007400200075006E006500200073006100690073006900650020006F00
      62006C0069006700610074006F00690072006500010045002D006D0061006900
      6C00200061006400640072006500730073002000690073002000610020006D00
      61006E006400610074006F0072007900200065006E0074007200790001000D00
      0A007300740072007300740072005F0073006D006F006B0065005F0062006500
      68006100760069006F0072005F00720065007100750069007200650064000100
      52006F006F006B00670065006400720061006700200069007300200065006500
      6E00200076006500720070006C0069006300680074006500200069006E006700
      61007600650001004C006500200063006F006D0070006F007200740065006D00
      65006E0074002000660075006D00650075007200200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F00690072006500010053006D006F006B0069006E006700200062006500
      68006100760069006F0072002000690073002000610020006D0061006E006400
      610074006F0072007900200065006E0074007200790001000D000A0073007400
      72007300740072005F00630068006F006900630065005F0070006F0073007400
      61006C005F00610064007200650073005F007200650071007500690072006500
      640001004400650020006B00650075007A0065002000760061006E0020006800
      65007400200070006F0073007400610064007200650073002000690073002000
      650065006E00200076006500720070006C006900630068007400650020006900
      6E00670061007600650001004C0065002000630068006F006900780020006400
      650020006C0027006100640072006500730073006500200063006F0072007200
      6500730070006F006E00640061006E0063006500200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F0069007200650001005400680065002000630068006F00690063006500
      20006F0066002000740068006500200070006F007300740061006C0020006100
      6400640072006500730073002000690073002000610020006D0061006E006400
      610074006F0072007900200065006E0074007200790001000D000A0073007400
      72007300740072005F0074007900700065005F00740068006900720064005F00
      700061007200740079005F007200650071007500690072006500640001004800
      6500740020007400790070006500200064006500720064006500200070006100
      7200740069006A00200069007300200076006500720070006C00690063006800
      74002000740065007200200068006F006F006700740065002000760061006E00
      2000680065007400200070006F00730074006100640072006500730001004C00
      6500200074007900700065002000640065002000740069006500720073002000
      65007300740020006F0062006C0069006700610074006F006900720065002000
      70006F007500720020006C002700610064007200650073007300650020006400
      6500200063006F00720072006500730070006F006E00640061006E0063006500
      01005400680065002000740079007000650020006F0066002000740068006900
      7200640020007000610072007400790020006900730020006D0061006E006400
      610074006F0072007900200066006F0072002000740068006500200070006F00
      7300740061006C002000610064006400720065007300730001000D000A007300
      740072007300740072005F006E0061006D0065005F0066006900720073007400
      6E0061006D0065005F0063006F006D00700061006E0079005F00720065007100
      750069007200650064005F0070006F007300740061006C005F00610064006400
      720065007300730001004400650020006E00610061006D002C00200076006F00
      6F0072006E00610061006D0020006F00660020006600690072006D0061006E00
      610061006D00200069007300200076006500720070006C006900630068007400
      2000740065007200200068006F006F006700740065002000760061006E002000
      680065007400200070006F00730074006100640072006500730001004C006500
      20006E006F006D002C00200070007200E9006E006F006D0020006F0075002000
      72006100690073006F006E00200073006F006300690061006C00650020006500
      7300740020006F0062006C0069006700610074006F0069007200650020007000
      6F007500720020006C0027006100640072006500730073006500200064006500
      200063006F00720072006500730070006F006E00640061006E00630065000100
      54006800650020007300750072006E0061006D0065002C002000660069007200
      7300740020006E0061006D00650020006F007200200063006F006D0070006100
      6E00790020006E0061006D00650020006900730020006D0061006E0064006100
      74006F0072007900200066006F0072002000740068006500200070006F007300
      740061006C002000610064006400720065007300730001000D000A0073007400
      72007300740072005F0061006400640072006500730073005F00720065007100
      750069007200650064005F0070006F007300740061006C005F00610064006400
      7200650073007300010044006500200073007400720061006100740020007600
      61006E002000680065007400200070006F007300740061006400720065007300
      200069007300200076006500720070006C00690063006800740001004C006100
      200072007500650020006400650020006C002700610064007200650073007300
      6500200064006500200063006F00720072006500730070006F006E0064006100
      6E0063006500200065007300740020006F0062006C0069006700610074006F00
      6900720065000100540068006500200073007400720065006500740020006F00
      66002000740068006500200070006F007300740061006C002000610064006400
      7200650073007300200069007300200072006500710075006900720065006400
      01000D000A007300740072007300740072005F00730074007200650065007400
      6E0075006D006200650072005F00720065007100750069007200650064005F00
      70006F007300740061006C005F00610064006400720065007300010048006500
      740020007300740072006100610074006E0075006D006D006500720020007600
      61006E002000680065007400200070006F007300740061006400720065007300
      200069007300200076006500720070006C00690063006800740001004C006500
      20006E0075006D00E90072006F00200064006500200072007500650020006400
      650020006C002700610064007200650073007300650020006400650020006300
      6F00720072006500730070006F006E00640061006E0063006500200065007300
      740020006F0062006C0069006700610074006F00690072006500010054006800
      6500200073007400720065006500740020006E0075006D006200650072002000
      6F0066002000740068006500200070006F007300740061006C00200061006400
      6400720065007300730020006900730020006D0061006E006400610074006F00
      7200790001000D000A007300740072007300740072005F0070006F0073007400
      61006C005F0063006F00640065005F0072006500710075006900720065006400
      5F0070006F007300740061006C005F0061006400720065007300010044006500
      200070006F007300740063006F00640065002000760061006E00200068006500
      7400200070006F00730074006100640072006500730020006900730020007600
      6500720070006C00690063006800740001004C006500200063006F0064006500
      200070006F007300740061006C0020006400650020006C002700610064007200
      6500730073006500200064006500200063006F00720072006500730070006F00
      6E00640061006E0063006500200065007300740020006F0062006C0069006700
      610074006F006900720065000100540068006500200070006F00730074006100
      6C00200063006F006400650020006F0066002000740068006500200070006F00
      7300740061006C00200061006400640072006500730073002000690073002000
      7200650071007500690072006500640001000D000A0073007400720073007400
      72005F0063006900740079005F00720065007100750069007200650064005F00
      70006F007300740061006C005F00610064007200650073000100440065002000
      73007400610064002000760061006E002000680065007400200070006F007300
      740061006400720065007300200069007300200076006500720070006C006900
      63006800740001004C0061002000760069006C006C0065002000640065002000
      6C0027006100640072006500730073006500200064006500200063006F007200
      72006500730070006F006E00640061006E006300650020006500730074002000
      6F0062006C0069006700610074006F0069007200650001005400680065002000
      630069007400790020006F0066002000740068006500200070006F0073007400
      61006C0020006100640064007200650073007300200069007300200072006500
      71007500690072006500640001000D000A007300740072007300740072005F00
      63006F0075006E007400720079005F0072006500710075006900720065006400
      5F0070006F007300740061006C005F0061006400720065007300010048006500
      740020006C0061006E0064002000760061006E00200068006500740020007000
      6F00730074006100640072006500730020006900730020007600650072007000
      6C00690063006800740001004C00650020007000610079007300200064006500
      20006C0027006100640072006500730073006500200064006500200063006F00
      720072006500730070006F006E00640061006E00630065002000650073007400
      20006F0062006C0069006700610074006F006900720065000100540068006500
      200063006F0075006E0074007200790020006F00660020007400680065002000
      70006F007300740061006C002000610064006400720065007300730020006900
      730020006D0061006E006400610074006F007200790001000D000A0073007400
      72007300740072005F0062006100730065005F00630061007000690074006100
      6C005F0072006500710075006900720065006400010048006500740020006200
      61007300690073006B006100700069007400610061006C002000690073002000
      650065006E00200076006500720070006C006900630068007400650020006900
      6E00670061007600650001004C00650020006300610070006900740061006C00
      200064006500200062006100730065002000650073007400200075006E006500
      200073006100690073006900650020006F0062006C0069006700610074006F00
      6900720065000100540068006500200062006100730069006300200063006100
      70006900740061006C002000690073002000610020006D0061006E0064006100
      74006F0072007900200065006E0074007200790001000D000A00730074007200
      7300740072005F007300740061007200740064006100740065005F0063006F00
      6E00740072006100630074005F00720065007100750069007200650064000100
      44006500200069006E00670061006E006700730064006100740075006D002000
      760061006E002000680065007400200063006F006E0074007200610063007400
      2000690073002000650065006E00200076006500720070006C00690063006800
      74006500200069006E00670061007600650001004C0061002000640061007400
      650020006400270065006E0074007200E9006500200065006E00200076006900
      67007500650075007200200064007500200063006F006E007400720061007400
      2000650073007400200075006E00650020007300610069007300690065002000
      6F0062006C0069006700610074006F0069007200650001005400680065002000
      73007400610072007400640061007400650020006F0066002000740068006500
      200063006F006E00740072006100630074002000690073002000610020006D00
      61006E006400610074006F0072007900200065006E0074007200790001000D00
      0A007300740072007300740072005F0065006E00640064006100740065005F00
      63006F006E00740072006100630074005F007200650071007500690072006500
      64000100440065002000760065007200760061006C0064006100740075006D00
      2000760061006E002000680065007400200063006F006E007400720061006300
      74002000690073002000650065006E00200076006500720070006C0069006300
      680074006500200069006E00670061007600650001004C006100200064006100
      74006500200064002700650078007000690072006100740069006F006E002000
      64007500200063006F006E007400720061007400200065007300740020007500
      6E006500200073006100690073006900650020006F0062006C00690067006100
      74006F0069007200650001005400680065002000650078007000690072007900
      2000640061007400650020006F0066002000740068006500200063006F006E00
      740072006100630074002000690073002000610020006D0061006E0064006100
      74006F0072007900200065006E0074007200790001000D000A00730074007200
      7300740072005F006600720065007100750065006E00630065005F0070006100
      79006D0065006E00740073005F00720065007100750069007200650064000100
      4400650020006600720065007100750065006E00740069006500200076006100
      6E0020007000720065006D006900650062006500740061006C0069006E006700
      65006E002000690073002000650065006E00200076006500720070006C006900
      6300680074006500200069006E00670061007600650001004C00610020006600
      7200E9007100750065006E006300650020006400650020007000610069006500
      6D0065006E007400200064006500730020007000720069006D00650073002000
      650073007400200075006E006500200065006E0074007200E900650020006F00
      62006C0069006700610074006F00690072006500010054006800650020006600
      720065007100750065006E006300790020006F00660020007000720065006D00
      690075006D0020007000610079006D0065006E00740073002000690073002000
      610020006D0061006E006400610074006F0072007900200065006E0074007200
      790001000D000A007300740072007300740072005F0063006C00610075007300
      65005F00620065006E00650066006900630069006100720079005F0072006500
      710075006900720065006400010044006500200063006C006100750073007500
      6C006500200062006500670075006E0073007400690067006400650020006900
      73002000650065006E00200076006500720070006C0069006300680074006500
      200069006E00670061007600650001004C006100200063006C00610075007300
      650020006200E9006E00E9006600690063006900610069007200650020006500
      73007400200075006E006500200073006100690073006900650020006F006200
      6C0069006700610074006F006900720065000100540068006500200062006500
      6E0065006600690063006900610072007900200063006C006100750073006500
      2000690073002000610020006D0061006E006400610074006F00720079002000
      65006E0074007200790001000D000A007300740072007300740072005F006300
      6F006E00740061006300740064006100740061005F00620065006E0065006600
      6900630069006100720079005F00720065007100750069007200650064000100
      44006500200063006F006E007400610063007400670065006700650076006500
      6E0073002000760061006E00200064006500200062006500670075006E007300
      74006900670064006500200063006C00610075007300650020007A0069006A00
      6E002000650065006E00200076006500720070006C0069006300680074006500
      200069006E00670061007600650001004C0065007300200063006F006F007200
      64006F006E006E00E9006500730020006400650020006C006100200063006C00
      610075007300650020006200E9006E00E9006600690063006900610069007200
      6500200073006F006E00740020006F0062006C0069006700610074006F006900
      7200650073000100540068006500200063006F006E0074006100630074002000
      640065007400610069006C00730020006F006600200074006800650020006200
      65006E0065006600690063006900610072007900200063006C00610075007300
      6500200061007200650020006D0061006E006400610074006F00720079000100
      0D000A007300740072007300740072005F006900620061006E005F006E007500
      6D006200650072005F0072006500710075006900720065006400010048006500
      740020004900420041004E002D006E0075006D006D0065007200200069007300
      2000650065006E00200076006500720070006C00690063006800740065002000
      69006E00670061007600650001004C00650020006E0075006D00E90072006F00
      20004900420041004E002000650073007400200075006E006500200073006100
      690073006900650020006F0062006C0069006700610074006F00690072006500
      010054006800650020004900420041004E0020006E0075006D00620065007200
      20006900730020006D0061006E006400610074006F007200790001000D000A00
      7300740072007300740072005F0069006E0063006100730073006F005F006600
      69006600740068005F0072006500710075006900720065006400010049006E00
      63006100730073006F0064006100740075006D0020006F007000200064006500
      200035006400650020007700650072006B006400610067002000690073002000
      650065006E00200076006500720070006C006900630068007400650020006900
      6E00670061007600650001004C00610020006400610074006500200064006500
      200063006F006C006C00650063007400650020006C00650020003500E8006D00
      650020006A006F007500720020006F00750076007200610062006C0065002000
      650073007400200075006E006500200065006E0074007200E900650020006F00
      62006C0069006700610074006F00690072006500010043006F006C006C006500
      6300740069006F006E002000640061007400650020006F006E00200074006800
      65002000350074006800200077006F0072006B0069006E006700200064006100
      79002000690073002000610020006D0061006E006400610074006F0072007900
      200065006E0074007200790001000D000A007300740072007300740072005F00
      7300740061007200740064006100740065005F0063006F006E00740072006100
      630074005F006E006F0074005F00760061006C00690064000100440065002000
      69006E00670061006E006700730064006100740075006D002000760061006E00
      2000680065007400200063006F006E0074007200610063007400200069007300
      20006E006900650074002000670065006C0064006900670001004C0061002000
      640061007400650020006400650020006400E900620075007400200064007500
      200063006F006E00740072006100740020006E00270065007300740020007000
      610073002000760061006C006900640065000100540068006500200073007400
      610072007400640061007400650020006F006600200074006800650020006300
      6F006E007400720061006300740020006900730020006E006F00740020007600
      61006C006900640001000D000A007300740072007300740072005F0070007200
      6F0070006F0073006900740069006F006E005F00720065007100750069007200
      650064000100480065007400200076006F006F0072007300740065006C002000
      6F006E00740062007200650065006B00740001004C0061002000700072006F00
      70006F0073006900740069006F006E0020006D0061006E007100750065000100
      5400680065002000700072006F0070006F00730061006C002000690073002000
      6D0069007300730069006E00670001000D000A00730074007200730074007200
      5F00670065006E006500720061006C005F0063006F006E006400690074006900
      6F006E0073005F00720065007100750069007200650064000100480065007400
      200064006F00630075006D0065006E007400200061006C00670065006D006500
      6E006500200076006F006F0072007700610061007200640065006E0020006F00
      6E00740062007200650065006B00740001004C006500200064006F0063007500
      6D0065006E007400200063006F006E0064006900740069006F006E0073002000
      6700E9006E00E900720061006C0065007300200065007300740020006D006100
      6E007100750061006E0074000100540068006500200064006F00630075006D00
      65006E0074002000670065006E006500720061006C00200063006F006E006400
      6900740069006F006E00730020006900730020006D0069007300730069006E00
      670001000D000A007300740072007300740072005F006D006500640069006300
      61006C005F0063006800650063006B006C006900730074005F00720065007100
      7500690072006500640001004400650020006D00650064006900730063006800
      65002000760072006100670065006E006C0069006A007300740020006F006E00
      740062007200650065006B00740001004C006500200071007500650073007400
      69006F006E006E00610069007200650020006D00E90064006900630061006C00
      200065007300740020006D0061006E007100750061006E007400010054006800
      650020006D00650064006900630061006C002000710075006500730074006900
      6F006E006E00610069007200650020006900730020006D006900730073006900
      6E00670001000D000A007300740072007300740072005F006400750065005F00
      640069006C006C006900670065006E00630065005F0072006500710075006900
      7200650064000100480065007400200064006F00630075006D0065006E007400
      20006400750065002000640069006C006C006900670065006E00630065002000
      6F006E00740062007200650065006B00740001004C006500200064006F006300
      75006D0065006E00740020006400750065002000640069006C006C0069006700
      65006E0063006500200065007300740020006D0061006E007100750061006E00
      7400010054006800650020006400750065002000640069006C00690067006500
      6E0063006500200064006F00630075006D0065006E0074002000690073002000
      6D0069007300730069006E00670001000D000A00730074007200730074007200
      5F00610067007200650065006D0065006E0074005F0069006E00730075007200
      650064005F007200650071007500690072006500640001004800650074002000
      64006F00630075006D0065006E007400200061006B006B006F006F0072006400
      2000760061006E0020006400650020007600650072007A0065006B0065007200
      6400650020006F006E00740062007200650065006B00740001004C0065002000
      64006F00630075006D0065006E0074002000640027006100630063006F007200
      640020006400650020006C00270061007300730075007200E900200065007300
      740020006D0061006E007100750061006E007400010054006800650020006400
      6F00630075006D0065006E00740020006F006600200074006800650020006100
      67007200650065006D0065006E00740020006F00660020007400680065002000
      69006E007300750072006500640020006900730020006D006900730073006900
      6E00670001000D000A007300740072007300740072005F007300650070006100
      5F00720065007100750069007200650064000100480065007400200073006500
      70006100200066006F0072006D0075006C0069006500720020006F006E007400
      62007200650065006B00740001004C006500200066006F0072006D0075006C00
      610069007200650020007300650070006100200065007300740020006D006100
      6E007100750061006E0074000100540068006500200073006500700061002000
      66006F0072006D0020006900730020006D0069007300730069006E0067000100
      0D000A007300740072007300740072005F0063006F00700079005F0069006400
      5F0063006100720064005F0069006E00730075007200650064005F0072006500
      71007500690072006500640001004B006F007000690065002000490044002000
      6B00610061007200740020007600650072007A0065006B006500720069006E00
      670073006E0065006D006500720020006F006E00740062007200650065006B00
      7400010055006E006500200063006F0070006900650020006400650020006C00
      61002000630061007200740065002000640027006900640065006E0074006900
      7400E90020006400750020007000720065006E00650075007200200064002700
      6100730073007500720061006E0063006500200065007300740020006D006100
      6E007100750061006E0074006500010043006F007000790020006F0066002000
      70006F006C0069006300790068006F006C006400650072002000490044002000
      630061007200640020006900730020006D0069007300730069006E0067000100
      0D000A007300740072007300740072005F006C0065006E006700740068005F00
      7200650071007500690072006500640001004400650020006C0065006E006700
      740065002000690073002000650065006E00200076006500720070006C006900
      6300680074006500200069006E00670061007600650001004C00610020007400
      610069006C006C0065002000650073007400200075006E006500200073006100
      690073006900650020006F0062006C0069006700610074006F00690072006500
      010054006800650020006C0065006E0067007400680020006900730020006100
      20006D0061006E006400610074006F0072007900200065006E00740072007900
      01000D000A007300740072007300740072005F00770065006900670068007400
      5F00720065007100750069007200650064000100480065007400200067006500
      770069006300680074002000690073002000650065006E002000760065007200
      70006C0069006300680074006500200069006E00670061007600650001004C00
      6500200070006F006900640073002000650073007400200075006E0065002000
      73006100690073006900650020006F0062006C0069006700610074006F006900
      7200650001005400680065002000770065006900670068007400200069007300
      2000610020006D0061006E006400610074006F0072007900200065006E007400
      7200790001000D000A007300740072007300740072005F0065006E0067006900
      6E0065005F006D0069007300730069006E0067000100530069006D0075006C00
      610074006900650020006B0061006E0020006E00690065007400200075006900
      74006700650076006F00650072006400200077006F007200640065006E002C00
      200062006500720065006B0065006E0069006E006700730065006E0067006900
      6E00650020006E0069006500740020006200650073006300680069006B006200
      61006100720001004C0061002000730069006D0075006C006100740069006F00
      6E0020006E0065002000700065007500740020007000610073002000EA007400
      7200650020006500660066006500630074007500E90065002C0020006C006500
      20006D006F0074006500750072002000640065002000630061006C0063007500
      6C0020006E002700650073007400200070006100730020006400690073007000
      6F006E00690062006C0065000100530069006D0075006C006100740069006F00
      6E002000630061006E006E006F00740020006200650020007000650072006600
      6F0072006D00650064002C002000630061006C00630075006C00610074006900
      6F006E00200065006E00670069006E00650020006E006F007400200061007600
      610069006C00610062006C00650001000D000A00730074007200730074007200
      5F0069006E00730075007200650064005F006300610070006900740061006C00
      5F007200650071007500690072006500640001005600650072007A0065006B00
      65007200640020006B006100700069007400610061006C002000690073002000
      650065006E00200076006500720070006C006900630068007400650020006900
      6E00670061007600650001004C00650020006300610070006900740061006C00
      200061007300730075007200E9002000650073007400200075006E0065002000
      73006100690073006900650020006F0062006C0069006700610074006F006900
      72006500010049006E0073007500720065006400200063006100700069007400
      61006C002000690073002000610020006D0061006E006400610074006F007200
      7900200065006E0074007200790001000D000A00730074007200730074007200
      5F0073006D006F006B00650072005F0072006500710075006900720065006400
      010052006F006B00650072002000690073002000650065006E00200076006500
      720070006C0069006300680074006500200069006E0067006100760065000100
      460075006D006500750072002000650073007400200075006E00650020007300
      6100690073006900650020006F0062006C0069006700610074006F0069007200
      6500010053006D006F006B00650072002000690073002000610020006D006100
      6E006400610074006F0072007900200065006E0074007200790001000D000A00
      7300740072007300740072005F00620069007200740068006400610074006500
      5F00720065007100750069007200650064000100440065002000670065006200
      6F006F0072007400650064006100740075006D00200069007300200065006500
      6E00200076006500720070006C0069006300680074006500200069006E006700
      61007600650001004C0061002000640061007400650020006400650020006E00
      610069007300730061006E00630065002000650073007400200075006E006500
      200073006100690073006900650020006F0062006C0069006700610074006F00
      69007200650001005400680065002000640061007400650020006F0066002000
      620069007200740068002000690073002000610020006D0061006E0064006100
      74006F0072007900200065006E0074007200790001000D000A00730074007200
      7300740072005F007000720065006D00690075006D005F007000610079006D00
      65006E0074005F00720065007100750069007200650064000100440065002000
      7000720065006D006900650062006500740061006C0069006E00670020006900
      73002000650065006E00200076006500720070006C0069006300680074006500
      200069006E00670061007600650001004C006500200070006100690065006D00
      65006E00740020006400650020006C00610020007000720069006D0065002000
      650073007400200075006E006500200073006100690073006900650020006F00
      62006C0069006700610074006F00690072006500010054006800650020007000
      720065006D00690075006D0020007000610079006D0065006E00740020006900
      73002000610020006D0061006E006400610074006F0072007900200065006E00
      74007200790001000D000A007300740072007300740072005F0063006F006E00
      6600690072006D005F00720065006D006F00760065005F0064006F0063007500
      6D0065006E0074000100560065007200770069006A0064006500720065006E00
      200064006F00630075006D0065006E0074003F00010053007500700070007200
      69006D006500720020006C006500200064006F00630075006D0065006E007400
      3F000100440065006C00650074006500200064006F00630075006D0065006E00
      74003F0001000D000A007300740072007300740072005F006E00690073005F00
      6E0075006D006200650072005F006E006F0074005F0063006F00720072006500
      6300740001004800650074002000720069006A006B0073007200650067006900
      73007400650072006E0075006D006D006500720020006900730020006E006900
      650074002000670065006C0064006900670001004C00650020006E0075006D00
      E90072006F002000640065002000720065006700690073007400720065002000
      6E006100740069006F006E0061006C0020006E00270065007300740020007000
      610073002000760061006C00690064006500010054006800650020006E006100
      740069006F006E0061006C002000720065006700690073007400650072002000
      6E0075006D0062006500720020006900730020006E006F007400200076006100
      6C006900640001000D000A007300740072005300740072005F00690064006300
      6100720064005F00650078007000690072006500640001004400650020006700
      65006C0064006900670068006500690064002000760061006E00200064006500
      20006900640065006E0074006900740065006900740073006B00610061007200
      740020006900730020007600650072006C006F00700065006E0001004C006100
      2000760061006C006900640069007400E90020006400650020006C0061002000
      630061007200740065002000640027006900640065006E00740069007400E900
      20006100200065007800700069007200E9000100540068006500200076006100
      6C006900640069007400790020006F0066002000740068006500200069006400
      65006E0074006900740079002000630061007200640020006800610073002000
      650078007000690072006500640001000D000A00730074007200730074007200
      5F007300740061007200740064006100740065005F006E006F0074005F007600
      61006C0069006400010044006500200062006500670069006E00640061007400
      75006D0020006900730020006E006900650074002000670065006C0064006900
      670001004C0061002000640061007400650020006400650020006400E9006200
      7500740020006E00270065007300740020007000610073002000760061006C00
      6900640065000100540068006500200073007400610072007400200064006100
      7400650020006900730020006E006F0074002000760061006C00690064000100
      0D000A007300740072007300740072005F0065006E0064006400610074006500
      5F006E006F0074005F00760061006C0069006400010044006500200065006900
      6E00640064006100740075006D0020006900730020006E006900650074002000
      670065006C0064006900670001004C0061002000640061007400650020006400
      65002000660069006E0020006E00270065007300740020007000610073002000
      760061006C006900640065000100540068006500200065006E00640020006400
      61007400650020006900730020006E006F0074002000760061006C0069006400
      01000D000A007300740072007300740072005F006900620061006E005F006E00
      75006D006200650072005F006E006F0074005F0063006F007200720065006300
      7400010048006500740020006900620061006E0020006E0075006D006D006500
      720020006900730020006E00690065007400200063006F007200720065006300
      740001004C00650020006E0075006D00E90072006F0020006900620061006E00
      2000650073007400200069006E0063006F007200720065006300740001005400
      6800650020006900620061006E0020006E0075006D0062006500720020006900
      7300200069006E0063006F007200720065006300740001000D000A0073007400
      72007300740072005F0063006C006F0073006500010053006C00750069007400
      65006E0001004600650072006D0065007200010043006C006F00730065000100
      0D000A007300740072007300740072005F00700072006F006F0066005F006F00
      66005F007200650073006900640065006E00630065005F007200650071007500
      6900720065006400010042006500770069006A0073002000760061006E002000
      77006F006F006E007300740020006F006E00740062007200650065006B000100
      50007200650075007600650020006400650020007200E9007300690064006500
      6E0063006500200065007300740020006D0061006E007100750061006E007400
      65000100500072006F006F00660020006F006600200072006500730069006400
      65006E006300650020006900730020006D0069007300730069006E0067000100
      0D000A00730074004F00740068006500720053007400720069006E0067007300
      5F0055006E00690063006F00640065000D000A0043006F006D0062006F006200
      6F0078004C0061006E00670075006100670065002E0054006500780074000100
      4E0065006400650072006C0061006E006400730001004E00E900650072006C00
      61006E00640061006900730001004400750074006300680001000D000A007300
      740043006F006C006C0065006300740069006F006E0073005F0055006E006900
      63006F00640065000D000A007300740043006800610072005300650074007300
      5F0055006E00690063006F00640065000D000A00}
  end
end
