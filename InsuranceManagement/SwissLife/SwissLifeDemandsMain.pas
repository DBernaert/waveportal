unit SwissLifeDemandsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniButton, UniThemeButton, uniEdit, uniImage,
  uniLabel, uniGUIBaseClasses, uniPanel, uniBasicGrid, uniDBGrid, Data.DB,
  MemDS, DBAccess, IBC, uniDateTimePicker, uniMultiItem, uniComboBox, siComp,
  siLngLnk;

var
	str_engine_missing:                    string = 'Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar'; // TSI: Localized (Don't modify!)
	str_insured_capital_required:          string = 'Verzekerd kapitaal is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_smoker_required:                   string = 'Roker is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_birthdate_required:                string = 'De geboortedatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_length_required:                   string = 'De lenge is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_weight_required:                   string = 'Het gewicht is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_premium_payment_required:          string = 'De premiebetaling is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_month:                             string = 'Maand'; // TSI: Localized (Don't modify!)
	str_quarter:                           string = 'Kwartaal'; // TSI: Localized (Don't modify!)
	str_year:                              string = 'Jaar'; // TSI: Localized (Don't modify!)
	str_no:                                string = 'Nee'; // TSI: Localized (Don't modify!)
	str_yes:                               string = 'Ja'; // TSI: Localized (Don't modify!)
	str_refused:                           string = 'GEWEIGERD'; // TSI: Localized (Don't modify!)
	str_draft:                             string = 'In opmaak'; // TSI: Localized (Don't modify!)
	str_send_validation:                   string = 'Doorgestuurd - validatie'; // TSI: Localized (Don't modify!)
	str_validated:                         string = 'Gevalideerd'; // TSI: Localized (Don't modify!)
	str_reception_police:                  string = 'Ontvangst polis'; // TSI: Localized (Don't modify!)
	str_police_refused:                    string = 'Geweigerd'; // TSI: Localized (Don't modify!)
	str_administrative_not_okay:           string = 'Administrief niet in orde'; // TSI: Localized (Don't modify!)
	str_stopped:                           string = 'Stopgezet'; // TSI: Localized (Don't modify!)
	str_already_send_cannot_be_stopped:    string = 'Aanvraag werd reeds doorgestuurd, deze aanvraag kan niet meer gestopt worden'; // TSI: Localized (Don't modify!)
	str_confirm_stop_demand:               string = 'Wenst u deze aanvraag te stoppen?'; // TSI: Localized (Don't modify!)
	str_error_stopping_proposal:           string = 'Fout bij het stopzetten van de aanvraag: '; // TSI: Localized (Don't modify!)
	str_demand_is_already_stopped:         string = 'Deze aanvraag is reeds gestop'; // TSI: Localized (Don't modify!)

type
  TSwissLifeDemandsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    SwissLife_proposals: TIBCQuery;
    Ds_SwissLife_proposals: TDataSource;
    SwissLife_proposalsID: TLargeintField;
    SwissLife_proposalsINSURED_NAME: TWideStringField;
    SwissLife_proposalsINSURED_FIRSTNAME: TWideStringField;
    SwissLife_proposalsINSURED_CITY: TWideStringField;
    SwissLife_proposalsINSURED_PHONE: TWideStringField;
    SwissLife_proposalsINSURED_EMAIL: TWideStringField;
    SwissLife_proposalsPROPOSAL_STATUS: TSmallintField;
    SwissLife_proposalsStatus_description: TStringField;
    ContainerBodySwissLife: TUniContainerPanel;
    ContainerBodyRight: TUniContainerPanel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    BtnSearch: TUniThemeButton;
    GridProposals: TUniDBGrid;
    LinkedLanguageSwissLifeDemandsMain: TsiLangLinked;
    PanelJournal: TUniPanel;
    SwissLife_proposalsLAST_NAME: TWideStringField;
    SwissLife_proposalsFIRST_NAME: TWideStringField;
    SwissLife_proposalsCOMPANY_NAME: TWideStringField;
    procedure BtnSearchClick(Sender: TObject);
    procedure SwissLife_proposalsCalcFields(DataSet: TDataSet);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure LinkedLanguageSwissLifeDemandsMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
  private
    { Private declarations }
    JournalFrame:                  TUniFrame;
  public
    { Public declarations }
    procedure Start_swisslife_demands_management;
  end;

implementation

{$R *.dfm}

uses SwissLifeDemandsAdd, MainModule, VCL.FlexCel.Core, FlexCel.XlsAdapter, Utils,
     Journal, Main, UniFSConfirm;

{ TSwissLifeDemandsMainFrm }

procedure TSwissLifeDemandsMainFrm.BtnSearchClick(Sender: TObject);
var SQL: string;
begin
  SwissLife_proposals.Close;
  SwissLife_proposals.SQL.Clear;
  SQL := 'select insurance_proposals.id, insurance_proposals.insured_name, insurance_proposals.insured_firstname, ' +
         'insurance_proposals.insured_city, insurance_proposals.insured_phone, insurance_proposals.insured_email, ' +
         'insurance_proposals.proposal_status, credit_contributors.last_name, credit_contributors.first_name, ' +
         'credit_contributors.company_name ' +
         'from insurance_proposals ' +
         'left outer join credit_contributors on (insurance_proposals.contributor_id = credit_contributors.id) ' +
         'where insurance_proposals.companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and insurance_proposals.removed=''0'' ' +
         'and (insurance_proposals.contributor_id =' + IntToStr(UniMainModule.Reference_contributor_id) + ' or ' +
         'insurance_proposals.contributor_id in (Select distinct id from credit_contributors ' +
         'where responsable_contributor=' + IntToStr(UniMainModule.Reference_contributor_id) + ' ' +
         'and removed=''0'')) ';

  if Trim(EditSearch.Text) <> ''
  then begin
         SQL :=
         SQL + ' and (UPPER(REPLACE(TRIM(insured_name), '' '', '''')) like '                 +
                Utils.SearchValue(EditSearch.Text) + ' OR '                                  +
               'UPPER(REPLACE(TRIM(insured_firstname), '' '', '''')) like '                  +
                Utils.SearchValue(EditSearch.text) + ' OR '                                  +
               'UPPER(REPLACE(TRIM(insured_city), '' '', '''')) like '                       +
                Utils.SearchValue(EditSearch.Text)  + ' OR '                                 +
               'UPPER(REPLACE(TRIM(insured_phone), '' '', '''')) like '                      +
                Utils.SearchValue(EditSearch.text) + ' OR '                                  +
               'UPPER(REPLACE(TRIM(credit_contributors.last_name), '' '', '''')) like '      +
                Utils.SearchValue(EditSearch.text) + ' OR '                                  +
               'UPPER(REPLACE(TRIM(credit_contributors.first_name), '' '', '''')) like '     +
                Utils.SearchValue(EditSearch.text) + ' OR '                                  +
               'UPPER(REPLACE(TRIM(credit_contributors.company_name), '' '', '''')) like '   +
                Utils.SearchValue(EditSearch.text) + ' OR '                                  +
               'UPPER(REPLACE(TRIM(insured_email), '' '', '''')) like '                      +
                Utils.SearchValue(EditSearch.text) + ')';
       end;

  SQL := SQL + ' order by insured_name, insured_firstname';

  SwissLife_proposals.SQL.Add(SQL);
  SwissLife_proposals.Open;
end;

procedure TSwissLifeDemandsMainFrm.EditSearchKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13
  then begin
         Key := #0;
         BtnSearchClick(Sender);
       end;
end;

procedure TSwissLifeDemandsMainFrm.LinkedLanguageSwissLifeDemandsMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TSwissLifeDemandsMainFrm.Start_swisslife_demands_management;
begin
  JournalFrame := TJournalFrm.Create(Self);
  With (JournalFrame as TJournalFrm)
  do begin
       Start_journal(19, Ds_SwissLife_Proposals);
       Parent := PanelJournal;
     end;
  BtnSearchClick(nil);
  EditSearch.SetFocus;
end;

procedure TSwissLifeDemandsMainFrm.SwissLife_proposalsCalcFields(
  DataSet: TDataSet);
begin
  case SwissLife_proposals.FieldByName('Proposal_status').asInteger of
    0:   SwissLife_proposals.FieldByName('Status_description').AsString := str_draft;
    1:   SwissLife_proposals.FieldByName('Status_description').AsString := str_send_validation;
    2:   SwissLife_proposals.FieldByName('Status_description').AsString := str_validated;
    3:   SwissLife_proposals.FieldByName('Status_description').AsString := str_reception_police;
    4:   SwissLife_proposals.FieldByName('Status_description').AsString := str_police_refused;
    5:   SwissLife_proposals.FieldByName('Status_description').AsString := str_administrative_not_okay;
    99:  SwissLife_proposals.FieldByName('Status_description').AsString := str_stopped;
  end;
end;

procedure TSwissLifeDemandsMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  PanelJournal.JSInterface.JSConfig('stateId', ['PanelJournal_stateId_unique']);
  with PanelJournal
  do JSInterface.JSConfig('stateId', [Self.Name + Name]);
end;

procedure TSwissLifeDemandsMainFrm.UpdateStrings;
begin
  str_demand_is_already_stopped      := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_demand_is_already_stopped' (* 'Deze aanvraag is reeds gestop' *) );
  str_error_stopping_proposal        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_error_stopping_proposal' (* 'Fout bij het stopzetten van de aanvraag: ' *) );
  str_confirm_stop_demand            := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_confirm_stop_demand' (* 'Wenst u deze aanvraag te stoppen?' *) );
  str_already_send_cannot_be_stopped := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_already_send_cannot_be_stopped' (* 'Aanvraag werd reeds doorgestuurd, deze aanvraag kan niet meer gestopt worden' *) );
  str_stopped                        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_stopped' (* 'Stopgezet' *) );
  str_administrative_not_okay        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_administrative_not_okay' (* 'Administrief niet in orde' *) );
  str_police_refused                 := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_police_refused' (* 'Geweigerd' *) );
  str_reception_police               := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_reception_police' (* 'Ontvangst polis' *) );
  str_validated                      := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_validated' (* 'Gevalideerd' *) );
  str_send_validation                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_send_validation' (* 'Doorgestuurd - validatie' *) );
  str_draft                          := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_draft' (* 'In opmaak' *) );
  str_refused                        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_refused' (* 'GEWEIGERD' *) );
  str_yes                            := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_yes' (* 'Ja' *) );
  str_no                             := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_no' (* 'Nee' *) );
  str_year                           := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_year' (* 'Jaar' *) );
  str_quarter                        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_quarter' (* 'Kwartaal' *) );
  str_month                          := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_month' (* 'Maand' *) );
  str_premium_payment_required       := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_premium_payment_required' (* 'De premiebetaling is een verplichte ingave' *) );
  str_weight_required                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_weight_required' (* 'Het gewicht is een verplichte ingave' *) );
  str_length_required                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_length_required' (* 'De lenge is een verplichte ingave' *) );
  str_birthdate_required             := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_birthdate_required' (* 'De geboortedatum is een verplichte ingave' *) );
  str_smoker_required                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_smoker_required' (* 'Roker is een verplichte ingave' *) );
  str_insured_capital_required       := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_insured_capital_required' (* 'Verzekerd kapitaal is een verplichte ingave' *) );
  str_engine_missing                 := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_engine_missing' (* 'Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar' *) );
end;

end.
