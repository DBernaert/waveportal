unit SwissLifeUpload;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, siComp, siLngLnk, Data.DB, MemDS,
  uniFileUpload, uniEdit, uniDBEdit, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniGUIBaseClasses, uniButton, UniThemeButton;

var
	str_error_creating_directory: string = 'Fout bij het aanmaken van de directory.'; // TSI: Localized (Don't modify!)
	str_error_saving_data:        string = 'Fout bij het opslaan van de gegevens: '; // TSI: Localized (Don't modify!)
	str_error_adding:             string = 'Fout bij het toevoegen'; // TSI: Localized (Don't modify!)

type
  TSwissLifeUploadFrm = class(TUniForm)
    BtnCancel: TUniThemeButton;
    FileUploadButton: TUniFileUploadButton;
    UpdFilesArchive: TIBCQuery;
    Ds_UpdFilesArchive: TDataSource;
    UpdTransFilesArchive: TIBCTransaction;
    LinkedLanguageSwissLifeUpload: TsiLangLinked;
    procedure BtnCancelClick(Sender: TObject);
    procedure FileUploadButtonCompleted(Sender: TObject; AStream: TFileStream);
    procedure LinkedLanguageSwissLifeUploadChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Initialize_swissLife_upload(ProposalId: longint): boolean;
  end;

function SwissLifeUploadFrm: TSwissLifeUploadFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, IOUtils;

function SwissLifeUploadFrm: TSwissLifeUploadFrm;
begin
  Result := TSwissLifeUploadFrm(UniMainModule.GetFormInstance(TSwissLifeUploadFrm));
end;

{ TSwissLifeUploadFrm }

procedure TSwissLifeUploadFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UpdFilesArchive.Cancel;
  UpdFilesArchive.Close;
  Close;
end;

procedure TSwissLifeUploadFrm.FileUploadButtonCompleted(Sender: TObject;
  AStream: TFileStream);
var TargetDir:   string;
    Dir:         string;
    TargetName:  string;
    Destination: string;
begin
  TargetDir := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';
  Dir       := UniMainModule.Root_archive + UniMainModule.Company_account_name;
  if not DirectoryExists(Dir)
  then begin
         if ForceDirectories(Dir) = False
         then begin
                UniMainModule.Show_error(str_error_creating_directory);
                Exit;
              end;
       end;
  TargetName   := TPath.GetGuidFileName(False);
  Destination  := TargetDir + TargetName;
  CopyFile(PChar(AStream.FileName), PChar(Destination), False);
  UpdFilesArchive.FieldByName('ORIGINAL_NAME').AsString    := ExtractFileName(FileUploadButton.FileName);
  UpdFilesArchive.FieldByName('DESTINATION_NAME').asString := TargetName;
  UpdFilesArchive.Post;

  Try
    if UpdTransFilesArchive.Active
    then UpdTransFilesArchive.Commit;
    UniMainModule.Result_dbAction := UpdFilesArchive.FieldByName('Id').AsInteger;
    Close;
  Except
    on E: EDataBaseError
    do begin
         if UpdTransFilesArchive.Active
         then UpdTransFilesArchive.Rollback;
         UniMainModule.Result_dbAction := 0;
         UniMainModule.Show_error(str_error_saving_data + E.Message);
         Close;
       end;
  End;
end;

function TSwissLifeUploadFrm.Initialize_swissLife_upload(
  ProposalId: longint): boolean;
begin
  Try
    UpdFilesArchive.Close;
    UpdFilesArchive.SQL.Clear;
    UpdFilesArchive.SQL.Add('Select first 0 * from files_archive');
    UpdFilesArchive.Open;
    UpdFilesArchive.Append;
    UpdFilesArchive.FieldByName('COMPANYID').AsInteger        := UniMainModule.Company_id;
    UpdFilesArchive.FieldByName('REMOVED').asString           := '0';
    UpdFilesArchive.FieldByName('LANGUAGE_ID').AsInteger      := 0;
    UpdFilesArchive.FieldByName('MODULE_ID').AsInteger        := 3000;
    UpdFilesArchive.FieldByName('RECORD_ID').AsInteger        := ProposalId;
    UpdFilesArchive.FieldByName('VISIBLE_PORTAL').asInteger   := 0;
    UpdFilesArchive.FieldByName('DATE_ENTERED').asDateTime    := Now;
    UpdFilesArchive.FieldByName('DATE_MODIFIED').AsDateTime   := Now;
    UpdFilesArchive.FieldByName('STORAGE_TYPE').AsInteger     := 0;
    Result := True;
  Except
    Result := False;
    UniMainModule.Show_error(str_error_adding);
  End;
end;

procedure TSwissLifeUploadFrm.LinkedLanguageSwissLifeUploadChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TSwissLifeUploadFrm.UniFormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TSwissLifeUploadFrm.UpdateStrings;
begin
  str_error_adding             := LinkedLanguageSwissLifeUpload.GetTextOrDefault('strstr_error_adding' (* 'Fout bij het toevoegen' *) );
  str_error_saving_data        := LinkedLanguageSwissLifeUpload.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens: ' *) );
  str_error_creating_directory := LinkedLanguageSwissLifeUpload.GetTextOrDefault('strstr_error_creating_directory' (* 'Fout bij het aanmaken van de directory.' *) );
end;

end.

