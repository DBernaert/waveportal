unit SwissLifeSimulation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniImage, uniGUIBaseClasses, uniLabel, uniButton,
  UniThemeButton, uniMultiItem, uniComboBox, uniMemo, uniPanel,
  uniDateTimePicker, uniEdit;

type
  TSwissLifeSimulationFrm = class(TUniForm)
    LblSimulation: TUniLabel;
    LblSwissLifeProtectBelgium: TUniLabel;
    ImageLogo: TUniImage;
    BtnCalculate: TUniThemeButton;
    UniMemo1: TUniMemo;
    ComboboxBaseCapital: TUniComboBox;
    ContainerResults: TUniContainerPanel;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    BtnClose: TUniThemeButton;
    LblResultInsuredCapital: TUniLabel;
    ComboboxSmoker: TUniComboBox;
    UniLabel3: TUniLabel;
    LblResultSmoker: TUniLabel;
    EditDateOfBirth: TUniDateTimePicker;
    UniLabel4: TUniLabel;
    LblResultDateOfBirth: TUniLabel;
    EditHeight: TUniNumberEdit;
    UniLabel5: TUniLabel;
    LblResultLength: TUniLabel;
    EditWeight: TUniNumberEdit;
    UniLabel6: TUniLabel;
    LblResultWeight: TUniLabel;
    ComboboxPremiumPayment: TUniComboBox;
    UniLabel7: TUniLabel;
    LblResultPremiumPayment: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniLabel10: TUniLabel;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    LblResultGrossPremium: TUniLabel;
    procedure BtnCalculateClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
  private
    { Private declarations }
    procedure ClearResults;
  public
    { Public declarations }
    procedure Start_swisslife_simulation;
  end;

function SwissLifeSimulationFrm: TSwissLifeSimulationFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, VCL.FlexCel.Core, FlexCel.XlsAdapter, Utils;

function SwissLifeSimulationFrm: TSwissLifeSimulationFrm;
begin
  Result := TSwissLifeSimulationFrm(UniMainModule.GetFormInstance(TSwissLifeSimulationFrm));
end;

{ TSwissLifeSimulationFrm }

procedure TSwissLifeSimulationFrm.BtnCalculateClick(Sender: TObject);
var xls:       TXlsFile;
    cell:      TCellValue;
    WorkValue: Currency;
begin
  ClearResults;

  if not FileExists('c:\DefaultTemplateArchive\SLProtectBelgiumBroker.xlsm')
  then begin
         UniMainModule.Show_warning('Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar');
         Exit;
       end;

  //Validate the fields
  if ComboboxBaseCapital.ItemIndex = -1
  then begin
         UniMainModule.Show_warning('Verzekerd kapitaal is een verplichte ingave');
         ComboboxBaseCapital.SetFocus;
         Exit;
       end;

  if ComboboxSmoker.ItemIndex = -1
  then begin
         UniMainModule.Show_warning('Roker is een verplichte ingave');
         ComboboxSmoker.SetFocus;
         Exit;
       end;

  if EditDateOfBirth.DateTime = 0
  then begin
         UniMainModule.Show_warning('De geboortedatum is een verplichte ingave');
         EditDateOfBirth.Setfocus;
         Exit;
       end;

  if EditHeight.Value = -1
  then begin
         UniMainModule.Show_warning('De lenge is een verplichte ingave');
         EditHeight.SetFocus;
         Exit;
       end;

  if EditWeight.Value = -1
  then begin
         UniMainModule.Show_warning('Het gewicht is een verplichte ingave');
         EditWeight.SetFocus;
         Exit;
       end;

  if ComboboxPremiumPayment.ItemIndex = -1
  then begin
         UniMainModule.Show_warning('De premiebetaling is een verplichte ingave');
         ComboboxPremiumPayment.SetFocus;
         Exit;
       end;

  xls := TXlsFile.Create('c:\DefaultTemplateArchive\SLProtectBelgiumBroker.xlsm');
  try
    UniMemo1.Lines.Clear;
    xls.ActiveSheetByName := 'Premium Tool';
    xls.SetCellValue(5, 4, StrToCurr(ComboboxBaseCapital.Text));
    if ComboboxSmoker.ItemIndex = 0
    then xls.SetCellValue(6, 4, 'no')
    else if ComboboxSmoker.ItemIndex = 1
         then xls.SetCellValue(6, 4, 'yes');
    xls.SetCellValue(7, 4, FormatDateTime('DD/MM/YYYY', EditDateOfBirth.DateTime));
    xls.SetCellValue(8, 4, EditHeight.Text);
    xls.SetCellValue(9, 4, EditWeight.Text);
    case ComboboxPremiumPayment.ItemIndex of
      0: xls.SetCellValue(10, 4, 'Monthly');
      1: xls.SetCellValue(10, 4, 'Quarterly');
      2: xls.SetCellValue(10, 4, 'Annually');
    end;
    xls.Recalc;

    cell := xls.GetCellValue(5, 4);
    if cell.IsNumber
    then begin
           WorkValue := Utils.RoundTo2dp(cell.AsNumber);
           LblResultInsuredCapital.Caption := FormatCurr('#,##0.00 �', WorkValue);
         end;
    cell := xls.GetCellValue(6, 4);
    if UpperCase(Trim(cell.ToString)) = 'NO'
    then LblResultSmoker.Caption := 'Nee'
    else if UpperCase(Trim(cell.ToString)) = 'YES'
         then LblResultSmoker.Caption := 'Ja';
    cell := xls.GetCellValue(7, 4);
    LblResultDateOfBirth.Caption := DateTimeToStr(cell.ToDateTime(false));
    cell := xls.GetCellValue(8, 4);
    LblResultLength.Caption := cell.ToString;
    cell := xls.GetCellValue(9, 4);
    LblResultWeight.Caption := cell.ToString;
    cell := xls.GetCellValue(10, 4);
    if UpperCase(Trim(cell.ToString)) = 'MONTHLY'
    then LblResultPremiumPayment.Caption := 'Maand'
    else if UpperCase(Trim(cell.ToString)) = 'QUARTERLY'
         then LblResultPremiumPayment.Caption := 'Kwartaal'
         else if UpperCase(Trim(cell.ToString)) = 'ANNUALLY'
              then LblResultPremiumPayment.Caption := 'Jaar';

    cell := xls.GetCellValue(13, 4);
    if cell.IsNumber
    then begin
           WorkValue := Utils.RoundTo2dp(cell.AsNumber);
           LblResultGrossPremium.Caption := FormatCurr('#,##0.00 �', WorkValue);
         end;
    UniMemo1.Lines.Add('Total Annual Premium: ' + cell.ToString);
    cell := xls.GetCellValue(19, 4);
    UniMemo1.Lines.Add('Premium payable regularly: ' + cell.ToString);
    cell := xls.GetCellValue(21, 4);
    UniMemo1.Lines.Add('Premium Payment Term (years): ' + cell.ToString);
  finally
    xls.Free;
  end;
end;

procedure TSwissLifeSimulationFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TSwissLifeSimulationFrm.ClearResults;
begin
  LblResultInsuredCapital.Caption := '';
end;

procedure TSwissLifeSimulationFrm.Start_swisslife_simulation;
begin
  ClearResults;
end;

end.
