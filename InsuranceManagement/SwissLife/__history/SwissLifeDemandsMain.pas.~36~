unit SwissLifeDemandsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniButton, UniThemeButton, uniEdit, uniImage,
  uniLabel, uniGUIBaseClasses, uniPanel, uniBasicGrid, uniDBGrid, Data.DB,
  MemDS, DBAccess, IBC, uniDateTimePicker, uniMultiItem, uniComboBox, siComp,
  siLngLnk;

var
	str_engine_missing:                    string = 'Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar'; // TSI: Localized (Don't modify!)
	str_insured_capital_required:          string = 'Verzekerd kapitaal is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_smoker_required:                   string = 'Roker is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_birthdate_required:                string = 'De geboortedatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_length_required:                   string = 'De lenge is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_weight_required:                   string = 'Het gewicht is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_premium_payment_required:          string = 'De premiebetaling is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_month:                             string = 'Maand'; // TSI: Localized (Don't modify!)
	str_quarter:                           string = 'Kwartaal'; // TSI: Localized (Don't modify!)
	str_year:                              string = 'Jaar'; // TSI: Localized (Don't modify!)
	str_no:                                string = 'Nee'; // TSI: Localized (Don't modify!)
	str_yes:                               string = 'Ja'; // TSI: Localized (Don't modify!)
	str_refused:                           string = 'GEWEIGERD'; // TSI: Localized (Don't modify!)
	str_draft:                             string = 'In opmaak'; // TSI: Localized (Don't modify!)
	str_send_validation:                   string = 'Doorgestuurd - validatie'; // TSI: Localized (Don't modify!)
	str_validated:                         string = 'Gevalideerd'; // TSI: Localized (Don't modify!)
	str_reception_police:                  string = 'Ontvangst polis'; // TSI: Localized (Don't modify!)
	str_police_refused:                    string = 'Geweigerd'; // TSI: Localized (Don't modify!)
	str_administrative_not_okay:           string = 'Administrief niet in orde'; // TSI: Localized (Don't modify!)
	str_stopped:                           string = 'Stopgezet'; // TSI: Localized (Don't modify!)
	str_already_send_cannot_be_stopped:    string = 'Aanvraag werd reeds doorgestuurd, deze aanvraag kan niet meer gestopt worden'; // TSI: Localized (Don't modify!)
	str_confirm_stop_demand:               string = 'Wenst u deze aanvraag te stoppen?'; // TSI: Localized (Don't modify!)
	str_error_stopping_proposal:           string = 'Fout bij het stopzetten van de aanvraag: '; // TSI: Localized (Don't modify!)
	str_demand_is_already_stopped:         string = 'Deze aanvraag is reeds gestop'; // TSI: Localized (Don't modify!)

type
  TSwissLifeDemandsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageHeaderSwissLifeProtect: TUniImage;
    SwissLife_proposals: TIBCQuery;
    Ds_SwissLife_proposals: TDataSource;
    SwissLife_proposalsID: TLargeintField;
    SwissLife_proposalsINSURED_NAME: TWideStringField;
    SwissLife_proposalsINSURED_FIRSTNAME: TWideStringField;
    SwissLife_proposalsINSURED_CITY: TWideStringField;
    SwissLife_proposalsINSURED_PHONE: TWideStringField;
    SwissLife_proposalsINSURED_EMAIL: TWideStringField;
    SwissLife_proposalsPROPOSAL_STATUS: TSmallintField;
    SwissLife_proposalsStatus_description: TStringField;
    ContainerBodySwissLife: TUniContainerPanel;
    ContainerBodyLeft: TUniContainerPanel;
    ContainerBodyRight: TUniContainerPanel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ImageFind: TUniImage;
    BtnSearch: TUniThemeButton;
    ContainerFilterRight: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    GridProposals: TUniDBGrid;
    BtnCalculate: TUniThemeButton;
    ComboboxBaseCapital: TUniComboBox;
    ContainerResults: TUniContainerPanel;
    LblCalculation: TUniLabel;
    LblInsuredCapital: TUniLabel;
    LblResultInsuredCapital: TUniLabel;
    LblSmoker: TUniLabel;
    LblResultSmoker: TUniLabel;
    LblDateOfBirth: TUniLabel;
    LblResultDateOfBirth: TUniLabel;
    LblLength: TUniLabel;
    LblResultLength: TUniLabel;
    LblWeight: TUniLabel;
    LblResultWeight: TUniLabel;
    LblPremiumPayment: TUniLabel;
    LblResultPremiumPayment: TUniLabel;
    LblPremium: TUniLabel;
    LblGrossPremium: TUniLabel;
    LblResultGrossPremium: TUniLabel;
    LblBmi: TUniLabel;
    LblResultBmi: TUniLabel;
    LblPremiumTax: TUniLabel;
    LblResultPremiumTax: TUniLabel;
    LblServicePremium: TUniLabel;
    LblResultServicePremium: TUniLabel;
    ContainerSplitter: TUniContainerPanel;
    LblPayableRegularly: TUniLabel;
    LblPaymentTerm: TUniLabel;
    LblResultPayableRegularly: TUniLabel;
    LblResultPaymentTerm: TUniLabel;
    ComboboxSmoker: TUniComboBox;
    EditDateOfBirth: TUniDateTimePicker;
    EditHeight: TUniNumberEdit;
    EditWeight: TUniNumberEdit;
    ComboboxPremiumPayment: TUniComboBox;
    BtnCreateDemand: TUniThemeButton;
    LinkedLanguageSwissLifeDemandsMain: TsiLangLinked;
    PanelJournal: TUniPanel;
    BtnStop: TUniThemeButton;
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnSearchClick(Sender: TObject);
    procedure SwissLife_proposalsCalcFields(DataSet: TDataSet);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure BtnCalculateClick(Sender: TObject);
    procedure LinkedLanguageSwissLifeDemandsMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure BtnCreateDemandClick(Sender: TObject);
    procedure BtnStopClick(Sender: TObject);
  private
    { Private declarations }
    JournalFrame:                  TUniFrame;
    Insured_BaseCapital:           integer;
    Insured_Smoker:                integer;
    Insured_DateOfBirth:           TDateTime;
    Insured_Height:                integer;
    Insured_Weight:                integer;
    Insured_PremiumPayment:        integer;
    Insured_GrossPremium:          currency;
    Insured_Bmi:                   currency;
    Insured_PremiumTax:            currency;
    Insured_ServicePremium:        currency;
    Insured_AnnualPremium:         currency;
    Insured_PeriodicPremium:       currency;
    Insured_TermPremium:           integer;

    procedure ClearResults;
    procedure CallBackInsertUpdateSwissLifeDemands(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_swisslife_demands_management;
  end;

implementation

{$R *.dfm}

uses SwissLifeDemandsAdd, MainModule, VCL.FlexCel.Core, FlexCel.XlsAdapter, Utils,
     Journal, Main, UniFSConfirm;

{ TSwissLifeDemandsMainFrm }

procedure TSwissLifeDemandsMainFrm.BtnAddClick(Sender: TObject);
begin
  With SwisslifeDemandsAddFrm
  do begin
       if Init_creation_swisslife_demand
       then ShowModal(CallBackInsertUpdateSwissLifeDemands)
       else Close;
     end;
end;

procedure TSwissLifeDemandsMainFrm.BtnCalculateClick(Sender: TObject);
var xls:       TXlsFile;
    cell:      TCellValue;
    WorkValue: Currency;
begin
  ClearResults;

  if not FileExists('c:\DefaultTemplateArchive\SwissLifeCalculator2.xlsm')
  then begin
         MainForm.WaveShowWarningToast(str_engine_missing);
         Exit;
       end;

  //Validate the fields
  if ComboboxBaseCapital.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_insured_capital_required);
         ComboboxBaseCapital.SetFocus;
         Exit;
       end;

  if ComboboxSmoker.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_smoker_required);
         ComboboxSmoker.SetFocus;
         Exit;
       end;

  if EditDateOfBirth.DateTime = 0
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_required);
         EditDateOfBirth.Setfocus;
         Exit;
       end;

  if EditHeight.Value = -1
  then begin
         MainForm.WaveShowWarningToast(str_length_required);
         EditHeight.SetFocus;
         Exit;
       end;

  if EditWeight.Value = -1
  then begin
         MainForm.WaveShowWarningToast(str_weight_required);
         EditWeight.SetFocus;
         Exit;
       end;

  if ComboboxPremiumPayment.ItemIndex = -1
  then begin
         MainForm.WaveShowWarningToast(str_premium_payment_required);
         ComboboxPremiumPayment.SetFocus;
         Exit;
       end;

  xls := TXlsFile.Create('c:\DefaultTemplateArchive\SwissLifeCalculator2.xlsm');
  try
    xls.ActiveSheetByName := 'Premium Tool';
    xls.SetCellValue(5, 4, StrToCurr(ComboboxBaseCapital.Text));
    Insured_BaseCapital := ComboboxBaseCapital.ItemIndex;
    if ComboboxSmoker.ItemIndex = 0
    then xls.SetCellValue(6, 4, 'no')
    else if ComboboxSmoker.ItemIndex = 1
         then xls.SetCellValue(6, 4, 'yes');
    Insured_Smoker := ComboboxSmoker.ItemIndex;
    xls.SetCellValue(7, 4, FormatDateTime('DD/MM/YYYY', EditDateOfBirth.DateTime));
    Insured_DateOfBirth := EditDateOfBirth.DateTime;
    xls.SetCellValue(8, 4, EditHeight.Text);
    Insured_Height := Trunc(EditHeight.Value);
    xls.SetCellValue(9, 4, EditWeight.Text);
    Insured_Weight := Trunc(EditWeight.Value);
    case ComboboxPremiumPayment.ItemIndex of
      0: xls.SetCellValue(10, 4, 'Monthly');
      1: xls.SetCellValue(10, 4, 'Quarterly');
      2: xls.SetCellValue(10, 4, 'Annually');
    end;
    Insured_PremiumPayment := ComboboxPremiumPayment.ItemIndex;
    xls.Recalc;

    cell := xls.GetCellValue(5, 4);
    if cell.IsNumber
    then begin
           WorkValue := Utils.RoundTo2dp(cell.AsNumber);
           LblResultInsuredCapital.Caption := FormatCurr('#,##0.00 �', WorkValue);
         end;
    cell := xls.GetCellValue(6, 4);
    if UpperCase(Trim(cell.ToString)) = 'NO'
    then LblResultSmoker.Caption := str_no
    else if UpperCase(Trim(cell.ToString)) = 'YES'
         then LblResultSmoker.Caption := str_yes;
    cell := xls.GetCellValue(7, 4);
    LblResultDateOfBirth.Caption := DateTimeToStr(cell.ToDateTime(false));
    cell := xls.GetCellValue(8, 4);
    LblResultLength.Caption := cell.ToString;
    cell := xls.GetCellValue(9, 4);
    LblResultWeight.Caption := cell.ToString;
    cell := xls.GetCellValue(10, 4);
    if UpperCase(Trim(cell.ToString)) = 'MONTHLY'
    then LblResultPremiumPayment.Caption := str_month
    else if UpperCase(Trim(cell.ToString)) = 'QUARTERLY'
         then LblResultPremiumPayment.Caption := str_quarter
         else if UpperCase(Trim(cell.ToString)) = 'ANNUALLY'
              then LblResultPremiumPayment.Caption := str_year;

    cell := xls.GetCellValue(16, 4);
    if cell.IsNumber
    then begin
           WorkValue                     := Utils.RoundTo2dp(cell.AsNumber);
           LblResultGrossPremium.Caption := FormatCurr('#,##0.00 �', WorkValue);
           Insured_GrossPremium          := WorkValue;
         end
    else try
           WorkValue                     := StrToCurr(cell.ToString);
           WorkValue                     := Utils.RoundTo2dp(WorkValue);
           LblResultGrossPremium.Caption := FormatCurr('#,##0.00 �', WorkValue);
           Insured_GrossPremium          := WorkValue;
         except
           LblResultGrossPremium.Caption := str_refused;
           Insured_GrossPremium          := -1;
         end;

    cell := xls.GetCellValue(17, 4);
    if cell.IsNumber
    then begin
           WorkValue                     := Utils.RoundTo2dp(cell.AsNumber);
           LblResultBmi.Caption          := FormatCurr('#,##0.00 �', WorkValue);
           Insured_Bmi                   := WorkValue;
         end
    else try
           WorkValue                     := StrToCurr(cell.ToString);
           WorkValue                     := Utils.RoundTo2dp(WorkValue);
           LblResultBmi.Caption          := FormatCurr('#,##0.00 �', WorkValue);
           Insured_Bmi                   := WorkValue;
         except
           LblResultBmi.Caption          := str_refused;
           Insured_Bmi                   := -1;
         end;

    cell := xls.GetCellValue(18, 4);
    if cell.IsNumber
    then begin
           WorkValue                     := Utils.RoundTo2dp(cell.AsNumber);
           LblResultPremiumTax.Caption   := FormatCurr('#,##0.00 �', WorkValue);
           Insured_PremiumTax            := WorkValue;
         end
    else try
           WorkValue                     := StrToCurr(cell.ToString);
           WorkValue                     := Utils.RoundTo2dp(WorkValue);
           LblResultPremiumTax.Caption   := FormatCurr('#,##0.00 �', WorkValue);
           Insured_PremiumTax            := WorkValue;
         except
           LblResultPremiumTax.Caption   := str_refused;
           Insured_PremiumTax            := -1;
         end;

    cell := xls.GetCellValue(19, 4);
    if cell.IsNumber
    then begin
           WorkValue                       := Utils.RoundTo2dp(cell.AsNumber);
           LblResultServicePremium.Caption := FormatCurr('#,##0.00 �', WorkValue);
           Insured_ServicePremium          := WorkValue;
         end
    else try
           WorkValue                       := StrToCurr(cell.ToString);
           WorkValue                       := Utils.RoundTo2dp(WorkValue);
           LblResultServicePremium.Caption := FormatCurr('#,##0.00 �', WorkValue);
           Insured_ServicePremium          := WorkValue;
         except
           LblResultServicePremium.Caption := str_refused;
           Insured_ServicePremium          := -1;
         end;

    cell := xls.GetCellValue(20, 4);
    if cell.IsNumber
    then begin
           WorkValue                         := Utils.RoundTo2dp(cell.AsNumber);
           LblResultPayableRegularly.Caption := FormatCurr('#,##0.00 �', WorkValue);
           Insured_PeriodicPremium           := WorkValue;
         end
    else try
           WorkValue                         := StrToCurr(cell.ToString);
           WorkValue                         := Utils.RoundTo2dp(WorkValue);
           LblResultPayableRegularly.Caption := FormatCurr('#,##0.00 �', WorkValue);
           Insured_PeriodicPremium           := WorkValue;
         except
           LblResultPayableRegularly.Caption := str_refused;
           Insured_PeriodicPremium           := -1;
         end;

    Try
      cell := xls.GetCellValue(22, 4);
      LblResultPaymentTerm.Caption := cell.ToString;
      Insured_TermPremium          := StrToInt(cell.ToString);
    except
      Insured_TermPremium          := 0;
    End;
  finally
    xls.Free;
  end;
end;

procedure TSwissLifeDemandsMainFrm.BtnCreateDemandClick(Sender: TObject);
begin
  With SwisslifeDemandsAddFrm
  do begin
       if Init_creation_swisslife_demand_from_simulation(Insured_BaseCapital, Insured_Smoker, Insured_DateOfBirth,
          Insured_Height, Insured_Weight, Insured_PremiumPayment)
       then ShowModal(CallBackInsertUpdateSwissLifeDemands)
       else Close;
     end;
end;

procedure TSwissLifeDemandsMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (SwissLife_proposals.Active) and (not SwissLife_proposals.fieldbyname('Id').isNull)
  then begin
         UniMainModule.Result_dbAction := 0;
         With SwisslifeDemandsAddFrm
         do begin
              if Init_modification_swisslife_demand(SwissLife_proposals.FieldByName('Id').AsInteger)
              then ShowModal(CallBackInsertUpdateSwissLifeDemands)
              else Close;
            end;
       end;
end;

procedure TSwissLifeDemandsMainFrm.BtnSearchClick(Sender: TObject);
var SQL: string;
begin
  SwissLife_proposals.Close;
  SwissLife_proposals.SQL.Clear;
  SQL := 'Select id, insured_name, insured_firstname, insured_city, insured_phone, ' +
         'insured_email, proposal_status ' +
         'from insurance_proposals ' +
         'where insurance_proposals.companyId=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' ' +
         'and contributor_id=' + IntToStr(UniMainModule.User_id) + ' ';

  if Trim(EditSearch.Text) <> ''
  then begin
         SQL :=
         SQL + ' and (UPPER(REPLACE(TRIM(insured_name), '' '', '''')) like '         +
                Utils.SearchValue(EditSearch.Text) + ' OR '                          +
               'UPPER(REPLACE(TRIM(insured_firstname), '' '', '''')) like '          +
                Utils.SearchValue(EditSearch.text) + ' OR '                          +
               'UPPER(REPLACE(TRIM(insured_city), '' '', '''')) like '               +
                Utils.SearchValue(EditSearch.Text)  + ' OR '                         +
               'UPPER(REPLACE(TRIM(insured_phone), '' '', '''')) like '              +
                Utils.SearchValue(EditSearch.text) + ' OR '                          +
               'UPPER(REPLACE(TRIM(insured_email), '' '', '''')) like '              +
                Utils.SearchValue(EditSearch.text) + ')';
       end;

  SQL := SQL + ' order by insured_name, insured_firstname';

  SwissLife_proposals.SQL.Add(SQL);
  SwissLife_proposals.Open;
end;

procedure TSwissLifeDemandsMainFrm.BtnStopClick(Sender: TObject);
begin
  if (SwissLife_proposals.Active = False) or (SwissLife_proposals.FieldByName('Id').IsNull)
  then exit;

  //Only drafts can be stopped
  if (SwissLife_proposals.FieldByName('Proposal_status').asInteger <> 0) and
     (SwissLife_proposals.FieldByName('Proposal_status').asInteger <> 99)
  then begin
         MainForm.WaveShowWarningToast(str_already_send_cannot_be_stopped);
         exit;
       end;

  if (SwissLife_proposals.FieldByName('Proposal_status').asInteger = 99)
  then begin
         MainForm.WaveShowWarningToast(str_demand_is_already_stopped);
         exit;
       end;

  MainForm.Confirm.Question(UniMainModule.Portal_name, str_confirm_stop_demand, 'fa fa-question-circle',
  procedure(Button: TConfirmButton)
  begin
    if Button = Yes then
    begin
      Try
        UniMainModule.QBatch.Close;
        UniMainModule.QBatch.SQL.Clear;
        UniMainModule.QBatch.SQL.Add('Update insurance_proposals set Proposal_status=99 where id=' + SwissLife_proposals.fieldbyname('Id').asString);
        UniMainModule.QBatch.ExecSQL;
        if UniMainModule.UpdTr_Batch.Active
        then UniMainModule.UpdTr_Batch.Commit;
        UniMainModule.QBatch.Close;
      Except
        on E: EDataBaseError
        do begin
            if UniMainModule.UpdTr_Batch.Active
            then UniMainModule.UpdTr_Batch.Rollback;
            UniMainModule.QBatch.Close;
            MainForm.WaveShowErrorToast(str_error_stopping_proposal + E.Message);
           end;
      end;
      SwissLife_proposals.Refresh;
    end;
  end);
end;

procedure TSwissLifeDemandsMainFrm.CallBackInsertUpdateSwissLifeDemands(
  Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         if SwissLife_proposals.Active
         then begin
                SwissLife_proposals.Refresh;
                SwissLife_proposals.Locate('Id', UniMainModule.Result_dbAction, []);
              end;
       end;
end;

procedure TSwissLifeDemandsMainFrm.ClearResults;
begin
  Insured_BaseCapital                        := -1;
  Insured_Smoker                             := -1;
  Insured_DateOfBirth                        := -1;
  Insured_Height                             :=  0;
  Insured_Weight                             :=  0;
  Insured_PremiumPayment                     := -1;
  Insured_GrossPremium                       := -1;
  Insured_Bmi                                := -1;
  Insured_PremiumTax                         := -1;
  Insured_ServicePremium                     := -1;
  Insured_AnnualPremium                      := -1;
  Insured_PeriodicPremium                    := -1;
  Insured_TermPremium                        :=  0;

  LblResultInsuredCapital.Caption            := '';
  LblResultSmoker.Caption                    := '';
  LblResultDateOfBirth.Caption               := '';
  LblResultLength.Caption                    := '';
  LblResultWeight.Caption                    := '';
  LblResultPremiumPayment.Caption            := '';
  LblResultGrossPremium.Caption              := '';
  LblResultBmi.Caption                       := '';
  LblResultPremiumTax.Caption                := '';
  LblResultServicePremium.Caption            := '';
  LblResultPayableRegularly.Caption          := '';
  LblResultPaymentTerm.Caption               := '';
end;

procedure TSwissLifeDemandsMainFrm.EditSearchKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13
  then begin
         Key := #0;
         BtnSearchClick(Sender);
       end;
end;

procedure TSwissLifeDemandsMainFrm.LinkedLanguageSwissLifeDemandsMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TSwissLifeDemandsMainFrm.Start_swisslife_demands_management;
begin
  JournalFrame := TJournalFrm.Create(Self);
  With (JournalFrame as TJournalFrm)
  do begin
       Start_journal(19, Ds_SwissLife_Proposals);
       Parent := PanelJournal;
     end;
  ClearResults;
  BtnSearchClick(nil);
  EditSearch.SetFocus;
end;

procedure TSwissLifeDemandsMainFrm.SwissLife_proposalsCalcFields(
  DataSet: TDataSet);
begin
  case SwissLife_proposals.FieldByName('Proposal_status').asInteger of
    0:   SwissLife_proposals.FieldByName('Status_description').AsString := str_draft;
    1:   SwissLife_proposals.FieldByName('Status_description').AsString := str_send_validation;
    2:   SwissLife_proposals.FieldByName('Status_description').AsString := str_validated;
    3:   SwissLife_proposals.FieldByName('Status_description').AsString := str_reception_police;
    4:   SwissLife_proposals.FieldByName('Status_description').AsString := str_police_refused;
    5:   SwissLife_proposals.FieldByName('Status_description').AsString := str_administrative_not_okay;
    99:  SwissLife_proposals.FieldByName('Status_description').AsString := str_stopped;
  end;
end;

procedure TSwissLifeDemandsMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  PanelJournal.JSInterface.JSConfig('stateId', ['PanelJournal_stateId_unique']);
  with PanelJournal
  do JSInterface.JSConfig('stateId', [Self.Name + Name]);
end;

procedure TSwissLifeDemandsMainFrm.UpdateStrings;
begin
  str_demand_is_already_stopped      := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_demand_is_already_stopped' (* 'Deze aanvraag is reeds gestop' *) );
  str_error_stopping_proposal        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_error_stopping_proposal' (* 'Fout bij het stopzetten van de aanvraag: ' *) );
  str_confirm_stop_demand            := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_confirm_stop_demand' (* 'Wenst u deze aanvraag te stoppen?' *) );
  str_already_send_cannot_be_stopped := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_already_send_cannot_be_stopped' (* 'Aanvraag werd reeds doorgestuurd, deze aanvraag kan niet meer gestopt worden' *) );
  str_stopped                        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_stopped' (* 'Stopgezet' *) );
  str_administrative_not_okay        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_administrative_not_okay' (* 'Administrief niet in orde' *) );
  str_police_refused                 := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_police_refused' (* 'Geweigerd' *) );
  str_reception_police               := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_reception_police' (* 'Ontvangst polis' *) );
  str_validated                      := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_validated' (* 'Gevalideerd' *) );
  str_send_validation                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_send_validation' (* 'Doorgestuurd - validatie' *) );
  str_draft                          := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_draft' (* 'In opmaak' *) );
  str_refused                        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_refused' (* 'GEWEIGERD' *) );
  str_yes                            := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_yes' (* 'Ja' *) );
  str_no                             := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_no' (* 'Nee' *) );
  str_year                           := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_year' (* 'Jaar' *) );
  str_quarter                        := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_quarter' (* 'Kwartaal' *) );
  str_month                          := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_month' (* 'Maand' *) );
  str_premium_payment_required       := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_premium_payment_required' (* 'De premiebetaling is een verplichte ingave' *) );
  str_weight_required                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_weight_required' (* 'Het gewicht is een verplichte ingave' *) );
  str_length_required                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_length_required' (* 'De lenge is een verplichte ingave' *) );
  str_birthdate_required             := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_birthdate_required' (* 'De geboortedatum is een verplichte ingave' *) );
  str_smoker_required                := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_smoker_required' (* 'Roker is een verplichte ingave' *) );
  str_insured_capital_required       := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_insured_capital_required' (* 'Verzekerd kapitaal is een verplichte ingave' *) );
  str_engine_missing                 := LinkedLanguageSwissLifeDemandsMain.GetTextOrDefault('strstr_engine_missing' (* 'Simulatie kan niet uitgevoerd worden, berekeningsengine niet beschikbaar' *) );
end;

end.
