object BackLogListFrm: TBackLogListFrm
  Left = 0
  Top = 0
  Width = 1232
  Height = 791
  OnCreate = UniFrameCreate
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1232
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 152
      Height = 30
      Hint = ''
      Caption = 'Achterstandslijst'
      ParentFont = False
      Font.Height = -21
      ParentColor = False
      Color = clBtnFace
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object HeaderList: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1232
    Height = 30
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerFilterRightBackLog: TUniContainerPanel
      Left = 868
      Top = 0
      Width = 364
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      object LblLastIncasso: TUniLabel
        Left = 0
        Top = 3
        Width = 321
        Height = 18
        Hint = ''
        Alignment = taRightJustify
        AutoSize = False
        Caption = ''
        ParentFont = False
        Font.Color = clRed
        Font.Height = -15
        Font.Style = [fsBold]
        TabOrder = 1
      end
      object BtnExportListBackLog: TUniThemeButton
        Left = 336
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Exporteren lijst'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnExportListBackLogClick
        ButtonTheme = uctSecondary
      end
    end
    object ContainerFilterLeftBackLog: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 2
      LayoutConfig.Region = 'west'
      object EditSearchBacklog: TUniEdit
        Left = 0
        Top = 0
        Width = 313
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        FieldLabel = '<i class="fas fa-search fa-lg " ></i>'
        FieldLabelWidth = 25
        FieldLabelSeparator = ' '
        SelectOnFocus = True
        OnChange = EditSearchBacklogChange
      end
    end
  end
  object GridBackLogList: TUniDBGrid
    Left = 0
    Top = 63
    Width = 1232
    Height = 728
    Hint = ''
    DataSource = DsBackLogList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    WebOptions.PageSize = 45
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Cls = 'customGrid'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 0 0 0'
    Align = alClient
    TabOrder = 2
    OnDrawColumnCell = GridBackLogListDrawColumnCell
    Columns = <
      item
        FieldName = 'INSURANCE_FILE_NUMBER'
        Title.Caption = 'Polisnummer'
        Width = 130
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'START_DATE_INSURANCE'
        Title.Alignment = taCenter
        Title.Caption = 'Ingangsdatum'
        Width = 100
        Alignment = taCenter
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'LASTPAYMENT'
        Title.Alignment = taCenter
        Title.Caption = 'Laatste betaling'
        Width = 130
        Alignment = taCenter
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'DELTA'
        Title.Alignment = taCenter
        Title.Caption = 'Termijnen achterstand'
        Width = 150
        Font.Style = [fsBold]
        Alignment = taCenter
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'INSURANCE_TAKER'
        Title.Caption = 'Verzekeringsnemer'
        Width = 184
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'PHONE_MOBILE'
        Title.Caption = 'Gsm'
        Width = 135
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'INSURED_PERSON'
        Title.Caption = 'Verzekerde'
        Width = 184
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'PREMIUM'
        Title.Alignment = taRightJustify
        Title.Caption = 'Premie'
        Width = 85
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'STRUCTURED_STATEMENT'
        Title.Caption = 'Mededeling'
        Width = 160
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object BackLogList: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select'
      '    insf.id,'
      '    insf.contributor_id,'
      '    insf.start_date_insurance,'
      '    insf.insurance_file_number,'
      '    insf.structured_statement,'
      '    insc.name insurance_company_name,'
      
        '    coalesce(agent.last_name, '#39#39') || '#39' '#39' || coalesce(agent.first' +
        '_name, '#39#39') agent_name,'
      '    CASE'
      '      WHEN insf.premium_payment = 0 THEN insf.premium_amount'
      '      WHEN insf.premium_payment = 1 THEN insf.premium_quarterly'
      '      WHEN insf.premium_payment = 2 THEN insf.premium_semesterly'
      '      WHEN insf.premium_payment = 3 THEN insf.premium_yearly'
      '    end premium,'
      
        '    coalesce(crmcontact.last_name, '#39#39') || '#39' '#39' || coalesce(crmcon' +
        'tact.first_name, '#39#39') insurance_taker,'
      '    crmcontact.phone_mobile,'
      
        '    (select first 1 coalesce(inspcontact.last_name, '#39#39') || '#39' '#39' |' +
        '| coalesce(inspcontact.first_name, '#39#39') insured_person from insur' +
        'ance_insured_persons insperson'
      
        '     left outer join crm_contacts inspcontact on (insperson.insu' +
        'red_person_contact_id = inspcontact.id)'
      
        '     where insperson.insurance_file = insf.id and inspcontact.bi' +
        'rthdate < dateadd(year, -18, current_date)),'
      
        '    (select count(*) as premiumpayments from insurance_premium_s' +
        'tatus where insurance_premium_status.insurance_file = insf.id an' +
        'd insurance_premium_status.open_amount = 0),'
      
        '    (select first 1 period_date from insurance_premium_status wh' +
        'ere insurance_premium_status.insurance_file = insf.id and insura' +
        'nce_premium_status.open_amount = 0 order by insurance_premium_st' +
        'atus.period_date desc ) lastpayment,'
      '    (select count(*) as delta from insurance_premium_status'
      '     where open_amount > 0'
      '     and insurance_premium_status.insurance_file = insf.id)'
      ''
      'from insurance_files insf'
      
        '   left outer join insurance_institutions insc on (insf.insuranc' +
        'e_company = insc.id)'
      
        '   left outer join crm_contacts crmcontact on (insf.insurance_ta' +
        'ker = crmcontact.id)'
      
        '   left outer join credit_contributors agent on (insf.agent_id =' +
        ' agent.id)'
      'where'
      '    insf.CompanyId=:CompanyId'
      '  and'
      '    (insc.id=:InsuranceCompanyId or :InsuranceCompanyId is NULL)'
      '  and'
      '    insf.removed='#39'0'#39
      '  and'
      '    insf.start_date_insurance <= current_date'
      '  and'
      '    insf.first_monthly_premium_payed = 1'
      '  and'
      '    insf.required_doc_okay = 1'
      '  and'
      '    insf.cancelled = 0'
      '  and '
      
        '    (insf.premium_payment=0 or insf.premium_payment=1 or insf.pr' +
        'emium_payment=2 or insf.premium_payment=3)'
      '  and'
      '    insc.include_in_backlog_lists = 1'
      '  and'
      '    insf.id in'
      '    (SELECT DISTINCT t.insurance_file'
      '     FROM insurance_premium_status t'
      '     JOIN ('
      '           SELECT insurance_file'
      '           FROM insurance_premium_status'
      
        '           WHERE open_amount <> 0 and period_date <= current_dat' +
        'e'
      '           GROUP BY insurance_file'
      '          ) g'
      '     ON t.insurance_file = g.insurance_file'
      '     WHERE t.open_amount <> 0 and t.period_date <= current_date)'
      'and &condition'
      'order by'
      '(select count(*) as delta from insurance_premium_status'
      '     where open_amount > 0'
      '     and insurance_premium_status.insurance_file = insf.id) desc')
    MasterFields = 'ID'
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 600
    Top = 224
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'CompanyId'
        ParamType = ptInput
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'InsuranceCompanyId'
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'InsuranceCompanyId'
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'contributorId'
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'agentId'
        Value = nil
      end>
    MacroData = <
      item
        Name = 'condition'
        Value = ' (contributor_id=:contributorId or insf.agent_id=:agentId)'
      end>
    object BackLogListID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object BackLogListCONTRIBUTOR_ID: TLargeintField
      FieldName = 'CONTRIBUTOR_ID'
    end
    object BackLogListSTART_DATE_INSURANCE: TDateField
      FieldName = 'START_DATE_INSURANCE'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object BackLogListINSURANCE_FILE_NUMBER: TWideStringField
      FieldName = 'INSURANCE_FILE_NUMBER'
    end
    object BackLogListPHONE_MOBILE: TWideStringField
      FieldName = 'PHONE_MOBILE'
      ReadOnly = True
    end
    object BackLogListLASTPAYMENT: TDateField
      FieldName = 'LASTPAYMENT'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object BackLogListDELTA: TLargeintField
      FieldName = 'DELTA'
      ReadOnly = True
    end
    object BackLogListPREMIUM: TFloatField
      FieldName = 'PREMIUM'
      ReadOnly = True
      DisplayFormat = '#,##0.00 '#8364
    end
    object BackLogListINSURANCE_TAKER: TWideStringField
      FieldName = 'INSURANCE_TAKER'
      ReadOnly = True
      Size = 61
    end
    object BackLogListINSURED_PERSON: TWideStringField
      FieldName = 'INSURED_PERSON'
      ReadOnly = True
      Size = 61
    end
    object BackLogListSTRUCTURED_STATEMENT: TWideStringField
      FieldName = 'STRUCTURED_STATEMENT'
      Size = 30
    end
  end
  object DsBackLogList: TDataSource
    DataSet = BackLogList
    Left = 600
    Top = 280
  end
  object LinkedLanguageBackLogList: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TBackLogListFrm.Layout'
      'ContainerHeader.Layout'
      'HeaderList.Layout'
      'ContainerFilterRightBackLog.Layout'
      'ContainerFilterLeftBackLog.Layout'
      'EditSearchBacklog.FieldLabelSeparator'
      'BackLogListID.DisplayLabel'
      'BackLogListCONTRIBUTOR_ID.DisplayLabel'
      'BackLogListSTART_DATE_INSURANCE.DisplayLabel'
      'BackLogListINSURANCE_FILE_NUMBER.DisplayLabel'
      'BackLogListPHONE_MOBILE.DisplayLabel'
      'BackLogListLASTPAYMENT.DisplayLabel'
      'BackLogListDELTA.DisplayLabel'
      'BackLogListPREMIUM.DisplayLabel'
      'BackLogListINSURANCE_TAKER.DisplayLabel'
      'BackLogListINSURED_PERSON.DisplayLabel'
      'LblLastIncasso.Caption'
      'BackLogListSTRUCTURED_STATEMENT.DisplayLabel')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageBackLogListChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 600
    Top = 336
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C0065000100410063006800
      7400650072007300740061006E00640073006C0069006A007300740001004C00
      6900730074006500200064006500730020006100720072006900E9007200E900
      730001004200610063006B006C006F00670020006C0069007300740001000D00
      0A0073007400480069006E00740073005F0055006E00690063006F0064006500
      0D000A00420074006E004500780070006F00720074004C006900730074004200
      610063006B004C006F00670001004500780070006F0072007400650072006500
      6E0020006C0069006A007300740001004500780070006F007200740065007200
      20006C00610020006C00690073007400650001004500780070006F0072007400
      20006C0069007300740001000D000A007300740044006900730070006C006100
      79004C006100620065006C0073005F0055006E00690063006F00640065000D00
      0A007300740046006F006E00740073005F0055006E00690063006F0064006500
      0D000A00730074004D0075006C00740069004C0069006E00650073005F005500
      6E00690063006F00640065000D000A007300740053007400720069006E006700
      73005F0055006E00690063006F00640065000D000A0073007400720073007400
      72005F006200610063006B006C006F0067005F006C0069007300740001004100
      630068007400650072007300740061006E00640073006C0069006A0073007400
      01004C006900730074006500200064006500730020006100720072006900E900
      7200E900730001004200610063006B006C006F00670020006C00690073007400
      01000D000A007300740072007300740072005F006C006100730074005F006900
      6E0063006100730073006F005F00700072006F00630065007300730069006E00
      670001004C0061006100730074006500200069006E0063006100730073006F00
      7600650072007700650072006B0069006E0067003A0020000100440065007200
      6E00690065007200200065006E006300610069007300730065006D0065006E00
      74003A00200001004C006100730074002000700072006F006300650073007300
      69006E0067002000640069007200650063007400200064006500620074003A00
      200001000D000A00730074004F00740068006500720053007400720069006E00
      670073005F0055006E00690063006F00640065000D000A004500640069007400
      5300650061007200630068004200610063006B006C006F0067002E0046006900
      65006C0064004C006100620065006C0001003C006900200063006C0061007300
      73003D0022006600610073002000660061002D00730065006100720063006800
      2000660061002D006C0067002000220020003E003C002F0069003E0001003C00
      6900200063006C006100730073003D0022006600610073002000660061002D00
      7300650061007200630068002000660061002D006C0067002000220020003E00
      3C002F0069003E0001003C006900200063006C006100730073003D0022006600
      610073002000660061002D007300650061007200630068002000660061002D00
      6C0067002000220020003E003C002F0069003E0001000D000A00730074004300
      6F006C006C0065006300740069006F006E0073005F0055006E00690063006F00
      640065000D000A0047007200690064004200610063006B004C006F0067004C00
      6900730074002E0043006F006C0075006D006E0073005B0030005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      4200610063006B004C006F0067004C006900730074002E0043006F006C007500
      6D006E0073005B0030005D002E005400690074006C0065002E00430061007000
      740069006F006E00010050006F006C00690073006E0075006D006D0065007200
      01004E00B000200064006500200070006F006C00690063006500010050006F00
      6C0069006300790020006E0075006D0062006500720001000D000A0047007200
      690064004200610063006B004C006F0067004C006900730074002E0043006F00
      6C0075006D006E0073005B0031005D002E0043006800650063006B0042006F00
      78004600690065006C0064002E004600690065006C006400560061006C007500
      65007300010074007200750065003B00660061006C0073006500010074007200
      750065003B00660061006C0073006500010074007200750065003B0066006100
      6C007300650001000D000A0047007200690064004200610063006B004C006F00
      67004C006900730074002E0043006F006C0075006D006E0073005B0031005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001004900
      6E00670061006E006700730064006100740075006D0001004400610074006500
      20006400650020006400E9006200750074000100530074006100720074002000
      640061007400650001000D000A0047007200690064004200610063006B004C00
      6F0067004C006900730074002E0043006F006C0075006D006E0073005B003200
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      7200690064004200610063006B004C006F0067004C006900730074002E004300
      6F006C0075006D006E0073005B0032005D002E005400690074006C0065002E00
      430061007000740069006F006E0001004C006100610074007300740065002000
      62006500740061006C0069006E00670001004400650072006E00690065007200
      200070006100690065006D0065006E00740001004C0061007300740020007000
      610079006D0065006E00740001000D000A004700720069006400420061006300
      6B004C006F0067004C006900730074002E0043006F006C0075006D006E007300
      5B0033005D002E0043006800650063006B0042006F0078004600690065006C00
      64002E004600690065006C006400560061006C00750065007300010074007200
      750065003B00660061006C0073006500010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001000D00
      0A0047007200690064004200610063006B004C006F0067004C00690073007400
      2E0043006F006C0075006D006E0073005B0033005D002E005400690074006C00
      65002E00430061007000740069006F006E0001005400650072006D0069006A00
      6E0065006E0020006100630068007400650072007300740061006E0064000100
      5400650072006D00650073002000720065007400610072006400010054006500
      72006D0073002000640065006C006100790001000D000A004700720069006400
      4200610063006B004C006F0067004C006900730074002E0043006F006C007500
      6D006E0073005B0034005D002E0043006800650063006B0042006F0078004600
      690065006C0064002E004600690065006C006400560061006C00750065007300
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      650001000D000A0047007200690064004200610063006B004C006F0067004C00
      6900730074002E0043006F006C0075006D006E0073005B0034005D002E005400
      690074006C0065002E00430061007000740069006F006E000100560065007200
      7A0065006B006500720069006E00670073006E0065006D006500720001005000
      720065006E0065007500720020006100730073007500720061006E0063006500
      010050006F006C0069006300790068006F006C0064006500720001000D000A00
      47007200690064004200610063006B004C006F0067004C006900730074002E00
      43006F006C0075006D006E0073005B0035005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A0047007200690064004200610063006B00
      4C006F0067004C006900730074002E0043006F006C0075006D006E0073005B00
      35005D002E005400690074006C0065002E00430061007000740069006F006E00
      0100470073006D000100470073006D0001004D006F00620069006C0065000100
      0D000A0047007200690064004200610063006B004C006F0067004C0069007300
      74002E0043006F006C0075006D006E0073005B0036005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640042006100
      63006B004C006F0067004C006900730074002E0043006F006C0075006D006E00
      73005B0036005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001005600650072007A0065006B006500720064006500010041007300
      730075007200E900010049006E007300750072006500640001000D000A004700
      7200690064004200610063006B004C006F0067004C006900730074002E004300
      6F006C0075006D006E0073005B0037005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A0047007200690064004200610063006B004C00
      6F0067004C006900730074002E0043006F006C0075006D006E0073005B003700
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      5000720065006D006900650001005000720069006D0065000100500072006500
      6D00690075006D0001000D000A0047007200690064004200610063006B004C00
      6F0067004C006900730074002E0043006F006C0075006D006E0073005B003800
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      7200690064004200610063006B004C006F0067004C006900730074002E004300
      6F006C0075006D006E0073005B0038005D002E005400690074006C0065002E00
      430061007000740069006F006E0001004D00650064006500640065006C006900
      6E00670001004D0065007300730061006700650001004D006500730073006100
      6700650001000D000A0073007400430068006100720053006500740073005F00
      55006E00690063006F00640065000D000A00}
  end
end
