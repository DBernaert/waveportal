unit FuneralCareCalculator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, VirtualTable, uniBasicGrid,
  uniDBGrid, uniMultiItem, uniComboBox, uniDateTimePicker,
  uniGUIBaseClasses, uniButton, UniThemeButton, uniLabel, siComp, siLngLnk,
  uniGroupBox, uniEdit, DBAccess, IBC;

var
	str_calculation_engine_not_found: string = 'De berekeningsengine kon niet gevonden worden'; // TSI: Localized (Don't modify!)
	str_birthdate_required:           string = 'De geboortedatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_birthdate_not_valid:          string = 'De geboortedatum is niet geldig'; // TSI: Localized (Don't modify!)
	str_startdate_required:           string = 'De aanvangsdatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_startdate_not_valid:          string = 'De aanvangsdatum is niet geldig'; // TSI: Localized (Don't modify!)
	str_select_three_capitals:        string = 'Gelieve 3 kapitalen te kiezen'; // TSI: Localized (Don't modify!)
	str_enter_name:                   string = 'Gelieve de naam op te geven'; // TSI: Localized (Don't modify!)
	str_enter_firstname:              string = 'Gelieve de voornaam op te geven'; // TSI: Localized (Don't modify!)
	str_enter_companyname:            string = 'Gelieve de bedrijfsnaam op te geven'; // TSI: Localized (Don't modify!)
	str_enter_email_address:          string = 'Gelieve het e-mailadres op geven'; // TSI: Localized (Don't modify!)
	str_error_creating_quote:         string = 'Fout bij het aanmaken van de offerte'; // TSI: Localized (Don't modify!)

const
  str_email_not_valid = 'Het opgegeven e-mailadres is niet geldig';

type
  TFuneralCareCalculatorFrm = class(TUniForm)
    BtnCalculate: TUniThemeButton;
    EditBirthDate: TUniDateTimePicker;
    EditStartDate: TUniDateTimePicker;
    ComboTypeOfInsuranceTaker: TUniComboBox;
    ComboboxFileCosts: TUniComboBox;
    TblTarifs: TVirtualTable;
    DsTarifs: TDataSource;
    GridTarifs: TUniDBGrid;
    BtnClose: TUniThemeButton;
    LblAge: TUniLabel;
    LblAgeValue: TUniLabel;
    LinkedLanguageFuneralCareCalculator: TsiLangLinked;
    BtnGenerateOffer: TUniThemeButton;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    Insurance_quotes_edit: TIBCQuery;
    UpdTr_Insurance_quotes_edit: TIBCTransaction;
    procedure BtnCalculateClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageFuneralCareCalculatorChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure BtnGenerateOfferClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_funeral_care_calculator;
  end;

function FuneralCareCalculatorFrm: TFuneralCareCalculatorFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, VCL.FlexCel.Core, FlexCel.XlsAdapter, Main,
  Utils, FuneralCareOffer;

function FuneralCareCalculatorFrm: TFuneralCareCalculatorFrm;
begin
  Result := TFuneralCareCalculatorFrm(UniMainModule.GetFormInstance(TFuneralCareCalculatorFrm));
end;

{ TFuneralCareCalculatorFrm }

procedure TFuneralCareCalculatorFrm.BtnCalculateClick(Sender: TObject);
var xls:                   TXlsFile;
    cell:                  TCellValue;
    Capital:               Integer;
    PurchasePrice:         Currency;
    GrossPremiumMonthly:   Currency;
    GrossPremiumQuarterly: Currency;
    GrossPremiumYearly:    Currency;
begin
  if not FileExists('c:\DefaultTemplateArchive\FuneralCareCalculator.xlsx')
  then begin
         MainForm.WaveShowWarningToast(str_calculation_engine_not_found);
         Exit;
       end;

  if EditBirthDate.DateTime = 0
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_required);
         EditBirthDate.SetFocus;
         Exit;
       end;

  if EditBirthDate.DateTime >= Now
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_not_valid);
         EditBirthDate.SetFocus;
         Exit;
       end;

  if EditStartDate.DateTime = 0
  then begin
         MainForm.WaveShowWarningToast(str_startdate_required);
         EditStartDate.SetFocus;
         Exit;
       end;

  if EditStartDate.DateTime < Now
  then begin
         MainForm.WaveShowWarningToast(str_startdate_not_valid);
         EditStartDate.SetFocus;
         Exit;
       end;

  TblTarifs.First;

  while not TblTarifs.Eof
  do TblTarifs.Delete;

  xls := TXlsFile.Create('c:\DefaultTemplateArchive\FuneralCareCalculator.xlsx');

  try
    xls.ActiveSheet := 1;
    xls.SetCellValue(5, 2, FormatDateTime('DD/MM/YYYY', EditBirthDate.DateTime));
    xls.SetCellValue(6, 2, FormatDateTime('DD/MM/YYYY', EditStartDate.DateTime));

    case ComboTypeOfInsuranceTaker.ItemIndex of
      0: xls.SetCellValue(8, 2, 'Personne physique');
      1: xls.SetCellValue(8, 2, 'Personne morale');
    end;

    xls.SetCellValue(9, 2, ComboboxFileCosts.ItemIndex * 10);

    Capital := 3000;

    while Capital < 16000
    do begin
         xls.SetCellValue(7, 2, Capital);
         xls.Recalc;
         cell                                                          := xls.GetCellValue(6, 4);
         LblAgeValue.Caption                                           := cell.ToString;
         cell                                                          := xls.GetCellValue(13, 10);
         PurchasePrice                                                 := Cell.ToNumberInvariant;
         cell                                                          := xls.GetCellValue(21, 12);
         GrossPremiumMonthly                                           := cell.ToNumberInvariant;
         cell                                                          := xls.GetCellValue(21, 14);
         GrossPremiumQuarterly                                         := cell.ToNumberInvariant;
         cell                                                          := xls.GetCellValue(21, 10);
         GrossPremiumYearly                                            := cell.ToNumberInvariant;
         TblTarifs.Append;
         TblTarifs.FieldByName('Capital').AsCurrency                   := Capital;
         TblTarifs.FieldByName('Purchaseprice').AsCurrency             := Purchaseprice;
         TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency       := GrossPremiumMonthly;
         TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency     := GrossPremiumQuarterly;
         TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency        := GrossPremiumYearly;
         TblTarifs.Post;
         Inc(Capital, 1000);
       end;
    TblTarifs.First;
    BtnGenerateOffer.Enabled := True;
  finally
    xls.Free;
  end;
end;

procedure TFuneralCareCalculatorFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFuneralCareCalculatorFrm.BtnGenerateOfferClick(Sender: TObject);
var B: TBookmark;
    QuoteId: longint;
begin
  if GridTarifs.SelectedRows.Count <> 3
  then begin
         MainForm.WaveShowWarningToast(str_select_three_capitals);
         Exit;
       end;

  Try
    Insurance_quotes_edit.Close;

    Insurance_quotes_edit.SQL.Text :=
    'Select first 0 * from insurance_quotes';

    Insurance_quotes_edit.Open;

    Insurance_quotes_edit.Append;

    Insurance_quotes_edit.FieldByName('CompanyId').AsInteger :=
    UniMainModule.Company_id;

    Insurance_quotes_edit.FieldByName('Quote_creation_time').AsDateTime :=
    Now;

    Insurance_quotes_edit.FieldByName('Contributor_id').AsInteger :=
    UniMainModule.User_id;

    Insurance_quotes_edit.FieldByName('Demander_date_of_birth').AsDateTime :=
    EditBirthDate.DateTime;

    Insurance_quotes_edit.FieldByName('Demander_type').AsInteger :=
    ComboTypeOfInsuranceTaker.ItemIndex;

    Insurance_quotes_edit.FieldByName('Start_date_insurance').AsDateTime :=
    EditStartDate.DateTime;

    Insurance_quotes_edit.FieldByName('Flag_mailed').AsInteger := 0;

    case ComboboxFileCosts.ItemIndex of
    0  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 0;
    1  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 10;
    2  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 20;
    3  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 30;
    4  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 40;
    5  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 50;
    6  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 60;
    7  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 70;
    8  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 80;
    9  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 90;
    10 : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 100;
    end;

    TblTarifs.DisableControls;

    B := TblTarifs.GetBookmark;

    TblTarifs.Bookmark := GridTarifs.SelectedRows[0];

    Insurance_quotes_edit.FieldByName('Capital_1').AsCurrency :=
    TblTarifs.FieldByName('Capital').AsCurrency;

    Insurance_quotes_edit.FieldByName('Purchase_price_1').AsCurrency :=
    TblTarifs.FieldByName('PurchasePrice').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_monthly_1').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_quarterly_1').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_yearly_1').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency;

    TblTarifs.Bookmark := GridTarifs.SelectedRows[1];

    Insurance_quotes_edit.FieldByName('Capital_2').AsCurrency :=
    TblTarifs.FieldByName('Capital').AsCurrency;

    Insurance_quotes_edit.FieldByName('Purchase_price_2').AsCurrency :=
    TblTarifs.FieldByName('PurchasePrice').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_monthly_2').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_quarterly_2').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_yearly_2').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency;

    TblTarifs.Bookmark := GridTarifs.SelectedRows[2];

    Insurance_quotes_edit.FieldByName('Capital_3').AsCurrency :=
    TblTarifs.FieldByName('Capital').AsCurrency;

    Insurance_quotes_edit.FieldByName('Purchase_price_3').AsCurrency :=
    TblTarifs.FieldByName('PurchasePrice').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_monthly_3').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_quarterly_3').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_yearly_3').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency;

    Insurance_quotes_edit.Post;

    if UpdTr_Insurance_quotes_edit.Active
    then UpdTr_insurance_quotes_edit.Commit;

    QuoteId := Insurance_quotes_edit.FieldByName('Id').AsInteger;

    Insurance_quotes_Edit.Close;

    if TblTarifs.BookmarkValid(B)
    then TblTarifs.GotoBookmark(B);

    TblTarifs.FreeBookmark(B);

    TblTarifs.EnableControls;

    Close;

    With FuneralCareOfferFrm
    do begin
         Start_funeral_care_offer(QuoteId);
         ShowModal;
       end;
  Except
    if UpdTr_insurance_quotes_edit.Active
    then UpdTr_insurance_quotes_edit.Rollback;

    MainForm.WaveShowErrorToast(str_error_creating_quote);
  End;
end;

procedure TFuneralCareCalculatorFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TFuneralCareCalculatorFrm.LinkedLanguageFuneralCareCalculatorChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TFuneralCareCalculatorFrm.Start_funeral_care_calculator;
begin
  EditBirthDate.DateTime := 0;
  EditStartDate.DateTime := 0;
end;

procedure TFuneralCareCalculatorFrm.UpdateStrings;
begin
  str_error_creating_quote          := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_error_creating_quote' (* 'Fout bij het aanmaken van de offerte' *) );
  str_enter_email_address           := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_enter_email_address' (* 'Gelieve het e-mailadres op geven' *) );
  str_enter_companyname             := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_enter_companyname' (* 'Gelieve de bedrijfsnaam op te geven' *) );
  str_enter_firstname               := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_enter_firstname' (* 'Gelieve de voornaam op te geven' *) );
  str_enter_name                    := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_enter_name' (* 'Gelieve de naam op te geven' *) );
  str_select_three_capitals         := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_select_three_capitals' (* 'Gelieve 3 kapitalen te kiezen' *) );
  str_startdate_not_valid           := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_startdate_not_valid' (* 'De aanvangsdatum is niet geldig' *) );
  str_startdate_required            := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_startdate_required' (* 'De aanvangsdatum is een verplichte ingave' *) );
  str_birthdate_not_valid           := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_birthdate_not_valid' (* 'De geboortedatum is niet geldig' *) );
  str_birthdate_required            := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_birthdate_required' (* 'De geboortedatum is een verplichte ingave' *) );
  str_calculation_engine_not_found  := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_calculation_engine_not_found' (* 'De berekeningsengine kon niet gevonden worden' *) );
end;

end.

