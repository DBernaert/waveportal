unit FuneralCareCalculator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, VirtualTable, uniBasicGrid,
  uniDBGrid, uniMultiItem, uniComboBox, uniDateTimePicker,
  uniGUIBaseClasses, uniButton, UniThemeButton, uniLabel, siComp, siLngLnk,
  uniGroupBox, uniEdit, DBAccess, IBC;

var
	str_calculation_engine_not_found: string = 'De berekeningsengine kon niet gevonden worden'; // TSI: Localized (Don't modify!)
	str_birthdate_required:           string = 'De geboortedatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_birthdate_not_valid:          string = 'De geboortedatum is niet geldig'; // TSI: Localized (Don't modify!)
	str_startdate_required:           string = 'De aanvangsdatum is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_startdate_not_valid:          string = 'De aanvangsdatum is niet geldig'; // TSI: Localized (Don't modify!)

type
  TFuneralCareCalculatorFrm = class(TUniForm)
    BtnCalculate: TUniThemeButton;
    EditBirthDate: TUniDateTimePicker;
    EditStartDate: TUniDateTimePicker;
    ComboTypeOfInsuranceTaker: TUniComboBox;
    ComboboxFileCosts: TUniComboBox;
    TblTarifs: TVirtualTable;
    DsTarifs: TDataSource;
    GridTarifs: TUniDBGrid;
    BtnClose: TUniThemeButton;
    LblAge: TUniLabel;
    LblAgeValue: TUniLabel;
    LinkedLanguageFuneralCareCalculator: TsiLangLinked;
    BtnGenerateOffer: TUniThemeButton;
    GroupboxParamsQuote: TUniGroupBox;
    EditName: TUniEdit;
    EditFirstName: TUniEdit;
    EditCompanyName: TUniEdit;
    EditMailAddress: TUniEdit;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    Insurance_quotes_edit: TIBCQuery;
    UpdTr_Insurance_quotes_edit: TIBCTransaction;
    ComboboxLanguage: TUniComboBox;
    procedure BtnCalculateClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageFuneralCareCalculatorChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure BtnGenerateOfferClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_funeral_care_calculator;
  end;

function FuneralCareCalculatorFrm: TFuneralCareCalculatorFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, VCL.FlexCel.Core, FlexCel.XlsAdapter, Main;

function FuneralCareCalculatorFrm: TFuneralCareCalculatorFrm;
begin
  Result := TFuneralCareCalculatorFrm(UniMainModule.GetFormInstance(TFuneralCareCalculatorFrm));
end;

{ TFuneralCareCalculatorFrm }

procedure TFuneralCareCalculatorFrm.BtnCalculateClick(Sender: TObject);
var xls:                   TXlsFile;
    cell:                  TCellValue;
    Capital:               Integer;
    PurchasePrice:         Currency;
    GrossPremiumMonthly:   Currency;
    GrossPremiumQuarterly: Currency;
    GrossPremiumYearly:    Currency;
begin
  if not FileExists('c:\DefaultTemplateArchive\FuneralCareCalculator.xlsx')
  then begin
         MainForm.WaveShowWarningToast(str_calculation_engine_not_found);
         Exit;
       end;

  if EditBirthDate.DateTime = 0
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_required);
         EditBirthDate.SetFocus;
         Exit;
       end;

  if EditBirthDate.DateTime >= Now
  then begin
         MainForm.WaveShowWarningToast(str_birthdate_not_valid);
         EditBirthDate.SetFocus;
         Exit;
       end;

  if EditStartDate.DateTime = 0
  then begin
         MainForm.WaveShowWarningToast(str_startdate_required);
         EditStartDate.SetFocus;
         Exit;
       end;

  if EditStartDate.DateTime < Now
  then begin
         MainForm.WaveShowWarningToast(str_startdate_not_valid);
         EditStartDate.SetFocus;
         Exit;
       end;

  TblTarifs.First;

  while not TblTarifs.Eof
  do TblTarifs.Delete;

  xls := TXlsFile.Create('c:\DefaultTemplateArchive\FuneralCareCalculator.xlsx');

  try
    xls.ActiveSheet := 1;
    xls.SetCellValue(5, 2, FormatDateTime('DD/MM/YYYY', EditBirthDate.DateTime));
    xls.SetCellValue(6, 2, FormatDateTime('DD/MM/YYYY', EditStartDate.DateTime));

    case ComboTypeOfInsuranceTaker.ItemIndex of
      0: xls.SetCellValue(8, 2, 'Personne physique');
      1: xls.SetCellValue(8, 2, 'Personne morale');
    end;

    xls.SetCellValue(9, 2, ComboboxFileCosts.ItemIndex * 10);

    Capital := 3000;

    while Capital < 16000
    do begin
         xls.SetCellValue(7, 2, Capital);
         xls.Recalc;
         cell                                                          := xls.GetCellValue(6, 4);
         LblAgeValue.Caption                                           := cell.ToString;
         cell                                                          := xls.GetCellValue(13, 10);
         PurchasePrice                                                 := Cell.ToNumberInvariant;
         cell                                                          := xls.GetCellValue(21, 12);
         GrossPremiumMonthly                                           := cell.ToNumberInvariant;
         cell                                                          := xls.GetCellValue(21, 14);
         GrossPremiumQuarterly                                         := cell.ToNumberInvariant;
         cell                                                          := xls.GetCellValue(21, 10);
         GrossPremiumYearly                                            := cell.ToNumberInvariant;
         TblTarifs.Append;
         TblTarifs.FieldByName('Capital').AsCurrency                   := Capital;
         TblTarifs.FieldByName('Purchaseprice').AsCurrency             := Purchaseprice;
         TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency       := GrossPremiumMonthly;
         TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency     := GrossPremiumQuarterly;
         TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency        := GrossPremiumYearly;
         TblTarifs.Post;
         Inc(Capital, 1000);
       end;
    TblTarifs.First;
    BtnGenerateOffer.Enabled := True;
  finally
    xls.Free;
  end;
end;

procedure TFuneralCareCalculatorFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFuneralCareCalculatorFrm.BtnGenerateOfferClick(Sender: TObject);
var B: TBookmark;
begin
  if GridTarifs.SelectedRows.Count <> 3
  then begin
         MainForm.WaveShowWarningToast('Gelieve 3 kapitalen te kiezen');
         Exit;
       end;

  if Trim(EditName.Text) = ''
  then begin
         EditName.SetFocus;
         MainForm.WaveShowWarningToast('Gelieve de naam op te geven');
         Exit;
       end;

  if Trim(EditFirstName.Text) = ''
  then begin
         EditFirstName.SetFocus;
         MainForm.WaveShowWarningToast('Gelieve de voornaam op te geven');
         Exit;
       end;

  if (Trim(EditCompanyName.Text) = '') and
     (ComboTypeOfInsuranceTaker.ItemIndex = 1)
  then begin
         EditCompanyName.SetFocus;
         MainForm.WaveShowWarningToast('Gelieve de bedrijfsnaam op te geven');
         Exit;
       end;

  if Trim(EditMailAddress.Text) = ''
  then begin
         EditMailAddress.SetFocus;
         MainForm.WaveShowWarningToast('Gelieve het e-mailadres op geven');
         Exit;
       end;

  Try
    Insurance_quotes_edit.Close;

    Insurance_quotes_edit.SQL.Text :=
    'Select first 0 * from insurance_quotes';

    Insurance_quotes_edit.Open;

    Insurance_quotes_edit.Append;

    Insurance_quotes_edit.FieldByName('CompanyId').AsInteger :=
    UniMainModule.Company_id;

    Insurance_quotes_edit.FieldByName('Quote_creation_time').AsDateTime :=
    Now;

    Insurance_quotes_edit.FieldByName('Contributor_id').AsInteger :=
    UniMainModule.User_id;

    Insurance_quotes_edit.FieldByName('Demander_name').AsString :=
    EditName.Text;

    Insurance_quotes_edit.FieldByName('Demander_firstname').AsString :=
    EditFirstName.Text;

    Insurance_quotes_edit.FieldByName('Demander_company_name').AsString :=
    EditCompanyName.Text;

    Insurance_quotes_edit.FieldByName('Demander_date_of_birth').AsDateTime :=
    EditBirthDate.DateTime;

    Insurance_quotes_edit.FieldByName('Demander_type').AsInteger :=
    ComboTypeOfInsuranceTaker.ItemIndex;

    Insurance_quotes_edit.FieldByName('Demander_email').AsString :=
    EditMailAddress.Text;

    Insurance_quotes_edit.FieldByName('Start_date_insurance').AsDateTime :=
    EditStartDate.DateTime;

    Insurance_quotes_edit.FieldByName('Flag_mailed').AsInteger := 0;

    case ComboboxFileCosts.ItemIndex of
    0  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 10;
    1  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 20;
    2  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 30;
    3  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 40;
    4  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 50;
    5  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 60;
    6  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 70;
    7  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 80;
    8  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 90;
    9  : Insurance_quotes_edit.FieldByName('File_costs').AsInteger := 100;
    end;

    TblTarifs.DisableControls;

    B := TblTarifs.GetBookmark;

    TblTarifs.Bookmark := GridTarifs.SelectedRows[0];

    Insurance_quotes_edit.FieldByName('Capital_1').AsCurrency :=
    TblTarifs.FieldByName('Capital').AsCurrency;

    Insurance_quotes_edit.FieldByName('Purchase_price_1').AsCurrency :=
    TblTarifs.FieldByName('PurchasePrice').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_monthly_1').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_quarterly_1').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_yearly_1').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency;

    TblTarifs.Bookmark := GridTarifs.SelectedRows[1];

    Insurance_quotes_edit.FieldByName('Capital_2').AsCurrency :=
    TblTarifs.FieldByName('Capital').AsCurrency;

    Insurance_quotes_edit.FieldByName('Purchase_price_2').AsCurrency :=
    TblTarifs.FieldByName('PurchasePrice').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_monthly_2').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_quarterly_2').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_yearly_2').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency;

    TblTarifs.Bookmark := GridTarifs.SelectedRows[2];

    Insurance_quotes_edit.FieldByName('Capital_3').AsCurrency :=
    TblTarifs.FieldByName('Capital').AsCurrency;

    Insurance_quotes_edit.FieldByName('Purchase_price_3').AsCurrency :=
    TblTarifs.FieldByName('PurchasePrice').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_monthly_3').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumMonthly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_quarterly_3').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumQuarterly').AsCurrency;

    Insurance_quotes_edit.FieldByName('Gross_premium_yearly_3').AsCurrency :=
    TblTarifs.FieldByName('GrossPremiumYearly').AsCurrency;

    Insurance_quotes_edit.Post;

    if UpdTr_Insurance_quotes_edit.Active
    then UpdTr_insurance_quotes_edit.Commit;

    if TblTarifs.BookmarkValid(B)
    then TblTarifs.GotoBookmark(B);

    TblTarifs.FreeBookmark(B);

    TblTarifs.EnableControls;

    MainForm.WaveShowConfirmationToast('Record created');
  Except
    if UpdTr_insurance_quotes_edit.Active
    then UpdTr_insurance_quotes_edit.Rollback;

    MainForm.WaveShowErrorToast('Fout bij het aanmaken van de offerte');
  End;
end;

procedure TFuneralCareCalculatorFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TFuneralCareCalculatorFrm.LinkedLanguageFuneralCareCalculatorChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TFuneralCareCalculatorFrm.Start_funeral_care_calculator;
begin
  EditBirthDate.DateTime := 0;
  EditStartDate.DateTime := 0;
end;

procedure TFuneralCareCalculatorFrm.UpdateStrings;
begin
  str_startdate_not_valid           := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_startdate_not_valid' (* 'De aanvangsdatum is niet geldig' *) );
  str_startdate_required            := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_startdate_required' (* 'De aanvangsdatum is een verplichte ingave' *) );
  str_birthdate_not_valid           := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_birthdate_not_valid' (* 'De geboortedatum is niet geldig' *) );
  str_birthdate_required            := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_birthdate_required' (* 'De geboortedatum is een verplichte ingave' *) );
  str_calculation_engine_not_found  := LinkedLanguageFuneralCareCalculator.GetTextOrDefault('strstr_calculation_engine_not_found' (* 'De berekeningsengine kon niet gevonden worden' *) );
end;

end.
