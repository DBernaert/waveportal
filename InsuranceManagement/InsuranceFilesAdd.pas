unit InsuranceFilesAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, siComp, siLngLnk, Data.DB, DBAccess, IBC, MemDS,
  uniComboBox, uniCheckBox, uniDBCheckBox, uniDBEdit, uniBasicGrid, uniDBGrid,
  uniLabel, uniPanel, uniMemo, uniDBMemo, uniDateTimePicker,
  uniDBDateTimePicker, uniEdit, uniMultiItem, uniDBComboBox,
  uniDBLookupComboBox, uniGUIBaseClasses, uniButton, UniThemeButton,
  uniRadioButton;

type
  TInsuranceFilesAddFrm = class(TUniForm)
    BtnClose: TUniThemeButton;
    EditUser: TUniDBLookupComboBox;
    Edit_Contributor: TUniDBLookupComboBox;
    EditStatus: TUniDBLookupComboBox;
    Edit_Insurance_institution: TUniDBLookupComboBox;
    EditInsuranceType: TUniDBLookupComboBox;
    EditTotalAmountInsurance: TUniDBFormattedNumberEdit;
    EditStartDateInsurance: TUniDBDateTimePicker;
    ContainerDemanders: TUniContainerPanel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    LblInsuredPersons: TUniLabel;
    GridInsuredPersons: TUniDBGrid;
    EditInternalNumber: TUniDBEdit;
    EditInsuranceFileNumber: TUniDBEdit;
    EditCommissionAmount: TUniDBFormattedNumberEdit;
    CheckBoxFirstMonthlyPremium: TUniDBCheckBox;
    CheckBoxSepaActive: TUniDBCheckBox;
    CheckBoxDocOkay: TUniDBCheckBox;
    ComboCommissionSystem: TUniComboBox;
    ComboSepaDocDay: TUniComboBox;
    EditSepaStartDate: TUniDBDateTimePicker;
    CheckBoxFileNumberFinal: TUniDBCheckBox;
    CheckboxIndexation: TUniDBCheckBox;
    CheckBoxCommissionReceived: TUniDBCheckBox;
    EditSalesDateInsurance: TUniDBDateTimePicker;
    Edit_Agent: TUniDBLookupComboBox;
    CheckBoxAdministrationOkay: TUniDBCheckBox;
    InsuranceFiles_Edit: TIBCQuery;
    Ds_InsuranceFiles_Edit: TDataSource;
    UpdTr_InsuranceFiles_Edit: TIBCTransaction;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    Status: TIBCQuery;
    DsStatus: TDataSource;
    Contributors: TIBCQuery;
    DsContributors: TDataSource;
    InsuranceCompanies: TIBCQuery;
    DsInsuranceCompanies: TDataSource;
    InsuranceTypes: TIBCQuery;
    DsInsuranceTypes: TDataSource;
    Contacts: TIBCQuery;
    DsContacts: TDataSource;
    InsuredPersons: TIBCQuery;
    InsuredPersonsID: TLargeintField;
    InsuredPersonsINSURED_CAPITAL: TFloatField;
    InsuredPersonsINSURED_PERSON_CONTACT_ID: TLargeintField;
    InsuredPersonsINSURED_NAME: TWideStringField;
    DsInsuredPersons: TDataSource;
    LinkedLanguageInsuranceFileAdd: TsiLangLinked;
    CheckBoxCancelled: TUniDBCheckBox;
    ComboPremiumPayment: TUniComboBox;
    CrmAccounts: TIBCQuery;
    DsCrmAccounts: TDataSource;
    Edit_insurance_taker: TUniDBLookupComboBox;
    RadiobuttonNaturalPerson: TUniRadioButton;
    RadiobuttonCompany: TUniRadioButton;
    Edit_insurance_taker_2: TUniDBLookupComboBox;
    EditStructuredStatement: TUniDBEdit;
    EditTotalAmountInsuranceEx: TUniDBFormattedNumberEdit;
    ContainerRegular: TUniContainerPanel;
    EditNetMonthPremiumAmount: TUniDBFormattedNumberEdit;
    EditPremiumAmount: TUniDBFormattedNumberEdit;
    EditPremiumQuarterly: TUniDBFormattedNumberEdit;
    EditPremiumSemesterly: TUniDBFormattedNumberEdit;
    EditPremiumYearly: TUniDBFormattedNumberEdit;
    EditGrossPremium: TUniDBFormattedNumberEdit;
    ContainerFamilyCare: TUniContainerPanel;
    EditPremiumTemporaryDeathCovers: TUniDBFormattedNumberEdit;
    EditPremiumAdditionalDeathCover: TUniDBFormattedNumberEdit;
    EditPremiumUKZT: TUniDBFormattedNumberEdit;
    EditPremiumAssistance: TUniDBFormattedNumberEdit;
    EditGrossPremium2: TUniDBFormattedNumberEdit;
    EditPremiumYearly2: TUniDBFormattedNumberEdit;
    EditPercentageIndexation: TUniDBFormattedNumberEdit;
    procedure BtnCloseClick(Sender: TObject);
    procedure Edit_Insurance_institutionChange(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    Function Init_insurance_file_viewing(InsuranceFileId: longint): boolean;
  end;

function InsuranceFilesAddFrm: TInsuranceFilesAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function InsuranceFilesAddFrm: TInsuranceFilesAddFrm;
begin
  Result := TInsuranceFilesAddFrm(UniMainModule.GetFormInstance(TInsuranceFilesAddFrm));
end;

{ TInsuranceFilesAddFrm }

procedure TInsuranceFilesAddFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TInsuranceFilesAddFrm.Edit_Insurance_institutionChange(
  Sender: TObject);
begin
  if InsuranceFiles_Edit.FieldByName('Insurance_company').isNull
  then begin
         ContainerFamilyCare.Visible := False;
         ContainerRegular.Visible := True;
       end
  else begin
         if InsuranceFiles_Edit.FieldByName('Insurance_company').AsInteger = 9
         then begin
                ContainerRegular.Visible := False;
                ContainerFamilyCare.Visible := True;
              end
         else begin
                ContainerFamilyCare.Visible := False;
                ContainerRegular.Visible := True;
              end;
       end;
end;

function TInsuranceFilesAddFrm.Init_insurance_file_viewing(InsuranceFileId: longint): boolean;
begin
  Try
    InsuranceFiles_Edit.Close;
    InsuranceFiles_Edit.SQL.Clear;
    InsuranceFiles_Edit.SQL.Add('Select * from insurance_files where id=' + IntToStr(InsuranceFileId));
    InsuranceFiles_Edit.Open;
    if not InsuranceFiles_Edit.FieldByName('Commission_system').IsNull
    then ComboCommissionSystem.ItemIndex := InsuranceFiles_Edit.FieldByName('Commission_system').AsInteger;
    if not InsuranceFiles_Edit.Fieldbyname('Sepa_document_day').IsNull
    then ComboSepaDocDay.ItemIndex := InsuranceFiles_Edit.FieldByName('Sepa_document_day').AsInteger;
    if not InsuranceFiles_Edit.FieldByName('Premium_payment').IsNull
    then ComboPremiumPayment.ItemIndex := InsuranceFiles_Edit.FieldByName('Premium_payment').AsInteger;

    if InsuranceFiles_Edit.FieldByName('Insurance_taker_type').asInteger = 0
    then RadiobuttonNaturalPerson.Checked      := True
    else if InsuranceFiles_Edit.FieldByName('Insurance_taker_type').asInteger = 1
         then RadiobuttonCompany.Checked       := True
         else RadiobuttonNaturalPerson.Checked := True;

    Edit_Insurance_institutionChange(nil);

    Open_reference_tables;
    InsuredPersons.Close;
    InsuredPersons.ParamByName('IdInsuranceFile').AsInteger := InsuranceFiles_Edit.FieldByName('Id').AsInteger;
    InsuredPersons.Open;
    Result := True;
  Except
    Result := False;
    InsuranceFiles_Edit.Cancel;
    InsuranceFiles_Edit.Close;
    MainForm.WaveShowWarningToast('Could not open insurance file.');
  End;

end;

procedure TInsuranceFilesAddFrm.Open_reference_tables;
var SQL: string;
begin
  Users.Close;
  Users.SQL.Clear;
  SQL := 'Select ID, coalesce(NAME,'''') || '' '' || coalesce(FIRSTNAME,'''') AS "USERFULLNAME" from users ' +
         'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by USERFULLNAME';
  Users.SQL.Add(SQL);
  Users.Open;

  Status.Close;
  Status.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=6000 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       EditStatus.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       EditStatus.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       EditStatus.ListField := 'ENGLISH';
     end;
  end;
  Status.SQL.Add(SQL);
  Status.Open;

  InsuranceTypes.Close;
  InsuranceTypes.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=6001 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
  1: begin
       SQL := SQL + ' order by DUTCH';
       EditInsuranceType.ListField := 'DUTCH';
     end;
  2: begin
       SQL := SQL + ' order by FRENCH';
       EditInsuranceType.ListField := 'FRENCH';
     end;
  3: begin
       SQL := SQL + ' order by ENGLISH';
       EditInsuranceType.ListField := 'ENGLISH';
     end;
  end;
  InsuranceTypes.SQL.Add(SQL);
  InsuranceTypes.Open;

  Contributors.Close;
  Contributors.SQL.Clear;
  SQL := 'Select ID, coalesce(LAST_NAME,'''') || '' '' || coalesce(FIRST_NAME,'''') AS "FULLNAME", contributor_number from credit_contributors ' +
         'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' ' +
         'order by FULLNAME';
  Contributors.SQL.Add(SQL);
  Contributors.Open;

  InsuranceCompanies.Close;
  InsuranceCompanies.SQL.Clear;
  InsuranceCompanies.SQL.Add('Select id, Name from insurance_institutions where companyid=' + IntTostr(UniMainModule.Company_id) +
                             ' and removed=''0'' order by name');
  InsuranceCompanies.Open;

  Contacts.Close;
  Contacts.SQL.Clear;
  SQL := 'Select id, coalesce(last_name, '''') || '' '' || coalesce(first_name, '''') as FullName ' +
         'from crm_contacts where CompanyID=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' order by FullName';
  Contacts.SQL.Add(SQL);
  Contacts.Open;

  CrmAccounts.Close;
  CrmAccounts.SQL.Clear;
  SQL := 'Select id, name from crm_accounts where companyid=' + IntToStr(UniMainModule.Company_id) +
         ' and removed=''0'' order by name';
  CrmAccounts.SQL.Add(SQL);
  CrmAccounts.Open;
end;

end.
