unit InsurancePaymentsPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, siComp, siLngLnk, Data.DB, MemDS, DBAccess, IBC,
  uniBasicGrid, uniDBGrid, uniImage, uniEdit, uniGUIBaseClasses, uniPanel,
  uniButton, UniThemeButton, uniDBText, uniLabel, VCL.FlexCel.Core, FlexCel.XlsAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

var
	str_delete_payment:        string = 'Verwijderen betaling?'; // TSI: Localized (Don't modify!)
	str_error_deleting:        string = 'Fout bij het verwijderen van de betaling.'; // TSI: Localized (Don't modify!)
	str_payments:              string = 'Betalingen polis'; // TSI: Localized (Don't modify!)
	str_month_premium:         string = 'Maandpremie'; // TSI: Localized (Don't modify!)
	str_6month_premium:        string = '6-maand premie'; // TSI: Localized (Don't modify!)
	str_year_premium:          string = 'Jaarpremie'; // TSI: Localized (Don't modify!)
	str_purchase_price:        string = 'Koopsom'; // TSI: Localized (Don't modify!)
	str_3month_premium:        string = 'TrimestriŽle premie'; // TSI: Localized (Don't modify!)
	str_no_rights:             string = 'U heeft niet de nodige rechten'; // TSI: Localized (Don't modify!)

type
  TViewFilterMode = (vfmAll, vfmFixedPeriod, vfmFixedPayment);
  TExcelDataRow = record
    Year: Integer;
    Month: integer;
    DueAmount: Currency;
    OpenAmount: Currency;
    AssignedAmount: Currency;
    PaymentID: int64;
    PaymentDate: string;
    PaymentAmount: currency;
    AmountBooked: Currency;
    PaymentType: String;
    SEPABatchNr: string;
    Comment: string;
  end;

type
  TInsurancePaymentsPanelFrm = class(TUniFrame)
    Payments: TIBCQuery;
    DsPayments: TDataSource;
    LinkedLanguageInsurancePaymentsPanel: TsiLangLinked;
    ContainerPaymentsLeft: TUniContainerPanel;
    ContainerHeaderPaymentsLeft: TUniContainerPanel;
    GridPaymentStatus: TUniDBGrid;
    Status: TIBCQuery;
    DsStatus: TDataSource;
    StatusID: TLargeintField;
    StatusCOMPANYID: TLargeintField;
    StatusINSURANCE_FILE: TLargeintField;
    StatusPERIOD_YEAR: TIntegerField;
    StatusPERIOD_MONTH: TIntegerField;
    StatusDUE_AMOUNT: TFloatField;
    StatusOPEN_AMOUNT: TFloatField;
    lblTitleOpenPremiumAmount: TUniLabel;
    lblOpenPremiumAmount: TUniDBText;
    qryPremiumsToPayments: TIBCQuery;
    qryPremiumsToPaymentsPAYMENT_AMOUNT: TFloatField;
    qryPremiumsToPaymentsPAYMENT_DATE: TDateField;
    qryPremiumsToPaymentsPAYMENT_METHOD: TSmallintField;
    qryPremiumsToPaymentsSEPA_BATCH_REFERENCE: TWideStringField;
    PaymentsPAYMENT_AMOUNT: TFloatField;
    PaymentsPAYMENT_DATE: TDateField;
    PaymentsPAYMENT_METHOD: TSmallintField;
    PaymentsSEPA_BATCH_REFERENCE: TWideStringField;
    PaymentsPAYMENT_METHOD_DESCRIPTION: TStringField;
    qryPremiumsToPaymentsPAYMENT_METHOD_DESCRIPTION: TStringField;
    qryPremiumsToPaymentsID: TLargeintField;
    qryPremiumsToPaymentsAMOUNT_BOOKED: TFloatField;
    PaymentsAMOUNT_BOOKED: TFloatField;
    PaymentsID: TLargeintField;
    dsPremiumsToPayments: TDataSource;
    qryPremiumsToPaymentsPAYMENT_COMMENT: TWideStringField;
    PaymentsPAYMENT_COMMENT: TWideStringField;
    qryPremiumsToPaymentsTRANSFID: TLargeintField;
    PaymentsTRANSFID: TLargeintField;
    qryPremiumsToPaymentsPERIOD_YEAR: TIntegerField;
    qryPremiumsToPaymentsPERIOD_MONTH: TIntegerField;
    PaymentsPERIOD_YEAR: TIntegerField;
    PaymentsPERIOD_MONTH: TIntegerField;
    qryPremiumsToPaymentsINSURANCE_PAYMENTS_ID: TLargeintField;
    PaymentsPAYMENT_ID: TLargeintField;
    qryPremiumsToPaymentsPAYMENT_ID: TLargeintField;
    StatusAMOUNT_BOOKED: TFloatField;
    qryOpenPremiumAmount: TIBCQuery;
    qryOpenPremiumAmountINSURANCE_FILE: TLargeintField;
    qryOpenPremiumAmountOPEN_AMOUNT: TFloatField;
    dsOpenPremiumAmount: TDataSource;
    qryNumberOfPayments: TIBCQuery;
    dsNumberOfPayments: TDataSource;
    qryNumberOfPaymentsINSURANCE_FILE: TLargeintField;
    qryNumberOfPaymentsPAYMENTS_COUNTER: TLargeintField;
    ContainerPaymentsRight: TUniContainerPanel;
    ContainerHeaderPaymentsRight: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    lblTitleNumberOfPayments: TUniLabel;
    lblNumberOfPayments: TUniDBText;
    GridPayments: TUniDBGrid;
    BtnExportList: TUniThemeButton;
    btnViewFixedPayment: TUniThemeButton;
    btnViewFixedPeriod: TUniThemeButton;
    btnViewAll: TUniThemeButton;
    procedure BtnAddPaymentClick(Sender: TObject);
    procedure BtnExportListClick(Sender: TObject);
    procedure PaymentsCalcFields(DataSet: TDataSet);
    procedure LinkedLanguageInsurancePaymentsPanelChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure GridPaymentsDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
      Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure GridPaymentStatusDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
    procedure btnViewAllClick(Sender: TObject);
    procedure btnViewFixedPaymentClick(Sender: TObject);
    procedure btnViewFixedPeriodClick(Sender: TObject);
    procedure DsPaymentsDataChange(Sender: TObject; Field: TField);
    procedure dsOpenPremiumAmountDataChange(Sender: TObject; Field: TField);
    procedure DsStatusDataChange(Sender: TObject; Field: TField);
  private
    FPaymentsDataSource: TDataSource;
    FMasterDs: TDataSource;
    FPinnedPaymentId: longint;
    FViewFilterMode: TViewFilterMode;
    { Private declarations }
    procedure CreateStatusQuery;
    procedure SetViewFilterMode(const Value: TViewFilterMode);
    procedure ColourLabels;
  public
    { Public declarations }
    property ViewFilterMode: TViewFilterMode read FViewFilterMode write SetViewFilterMode;
    procedure Start_insurance_file_payments(IdModule: integer; MasterDs: TDataSource);
  end;

implementation

{$R *.dfm}

uses MainModule, Main, DateUtils, enums, ServerModule, report_PremiumsPayments;


{ TInsurancePaymentsPanelFrm }

procedure TInsurancePaymentsPanelFrm.BtnAddPaymentClick(Sender: TObject);
begin
end;

//The Status query (left grid) can be used to display all months (FPinnedPaymentId = -1) (ViewFilterModus = vfmAll)
//or only months that use a preselected insurance_payments_id (ViewFilterModus = vfmFixedPayment)

procedure TInsurancePaymentsPanelFrm.CreateStatusQuery;
begin
  with Status do
  begin
  	Close;

    if ViewFilterMode = vfmFixedPayment then
    begin
      SQL.Text :=
          'Select  a.*, b.amount_booked from insurance_premium_status a left outer join insurance_payments_to_premiums b on (b.insurance_premium_status_id = a.id) ';
    	SQL.Text := SQL.Text + 'where b.insurance_payments_id = ' + IntToStr (FPinnedPaymentId) + ' ';
      SQL.Text := SQL.Text + 'order by a.period_year desc, a.period_month desc';
    end else begin //vfmAll
      SQL.Text := 'Select a.*, a.due_amount - a.open_amount as amount_booked from insurance_premium_status a ';
      SQL.Text := SQL.Text + 'order by a.period_year desc, a.period_month desc';
    end;

  	MasterSource := FMasterDs;
  	MasterFields := 'ID';
  	DetailFields := 'INSURANCE_FILE';

  	Open;
  end;
end;

procedure TInsurancePaymentsPanelFrm.dsOpenPremiumAmountDataChange(
  Sender: TObject; Field: TField);
begin
  GridPayments.Refresh; //it seems there is a refresh issue in the payments (right) grid when running through the different status records
  ColourLabels;
end;

procedure TInsurancePaymentsPanelFrm.ColourLabels;
var
  OpenAmount: double;
begin
  OpenAmount := qryOpenPremiumAmount.FieldByName ('OPEN_AMOUNT').AsFloat;

  if OpenAmount > 0 then lblOpenPremiumAmount.Font.Color := clRed;
  if OpenAmount = 0 then lblOpenPremiumAmount.Font.Color := clBlack;
  if OpenAmount < 0 then lblOpenPremiumAmount.Font.Color := clGreen;
end;

procedure TInsurancePaymentsPanelFrm.btnViewAllClick(Sender: TObject);
begin
  ViewFilterMode := vfmAll;
end;

procedure TInsurancePaymentsPanelFrm.btnViewFixedPeriodClick(Sender: TObject);
begin
  ViewFilterMode := vfmFixedPeriod;
end;

procedure TInsurancePaymentsPanelFrm.btnViewFixedPaymentClick(
  Sender: TObject);
begin
  FPinnedPaymentId := GridPayments.DataSource.DataSet.FieldByName('payment_id').AsLargeInt;
  ViewFilterMode := vfmFixedPayment;
end;

procedure TInsurancePaymentsPanelFrm.SetViewFilterMode(
  const Value: TViewFilterMode);
begin
  FViewFilterMode := Value;

  //Show buttons in different modes
  case FViewFilterMode of
    vfmAll:
      begin
        btnViewAll.Visible          := false;
        btnViewFixedPeriod.Visible  := true;
        btnViewFixedPayment.Visible := true;

  			FPinnedPaymentId := -1;
        CreateStatusQuery;
        FPaymentsDataSource := dsPayments;
        GridPayments.ColumnByName('amount_booked').Visible := false;
			end;
    vfmFixedPeriod:
      begin
        btnViewAll.Visible          := true;
        btnViewFixedPeriod.Visible  := false;
        btnViewFixedPayment.Visible := false;

        FPaymentsDataSource := dsPremiumsToPayments;
        GridPayments.ColumnByName('amount_booked').Visible := true;
        qryPremiumsToPayments.close;
        qryPremiumsToPayments.ParamByName ('insurance_premium_status_id').AsLargeInt := status.FieldByName('id').AsLargeInt;
        qryPremiumsToPayments.open;
      end;
    vfmFixedPayment:
    	begin
        btnViewAll.Visible          := true;
        btnViewFixedPeriod.Visible  := false;
        btnViewFixedPayment.Visible := false;
        CreateStatusQuery;
      end;
  end;

  GridPayments.DataSource := FPaymentsDataSource;
end;

procedure TInsurancePaymentsPanelFrm.Start_insurance_file_payments(
  IdModule: integer; MasterDs: TDataSource);
var
	curMonth: Integer;
begin
  FMasterDs := MasterDS;

  curMonth := YearOf(Now) * 100 + MonthOf(Now);
  qryOpenPremiumAmount.ParamByName('curMonth1').AsInteger := curMonth;
  qryOpenPremiumAmount.ParamByName('curMonth2').AsInteger := curMonth;
  qryOpenPremiumAmount.MasterSource := FMasterDS;
  qryOpenPremiumAmount.MasterFields := 'ID';
  qryOpenPremiumAmount.DetailFields := 'INSURANCE_FILE';
  qryOpenPremiumAmount.Open;
  ColourLabels;

  qryNumberOfPayments.MasterSource := FMasterDS;
  qryNumberOfPayments.MasterFields := 'ID';
  qryNumberOfPayments.DetailFields := 'INSURANCE_FILE';
  qryNumberOfPayments.Open;

  //option 1: all payments regardless of the selected status record
  Payments.Close;
  Payments.MasterSource := FMasterDs;
  Payments.MasterFields := 'ID';
  Payments.DetailFields := 'INSURANCE_FILE';
  Payments.Open;

  //option 2: only payments for the selected status record
	with qryPremiumsToPayments do
  begin
  	close;
		sql.text :=
        'select  a.insurance_premium_status_id,' +
        '  		   a.insurance_payments_id,' +
        '				 a.amount_booked,' +
        ' 			 c.id as transfid,' +
        '  			 d.period_year,' +
        '  			 d.period_month,' +
        '  			 b.*,' +
        '        b.id as payment_id ' +
        'from    insurance_payments_to_premiums a' +
        '        inner join insurance_payments b on (b.id = a.insurance_payments_id)' +
        '        inner join insurance_premium_status d on (d.id = a.insurance_premium_status_id)' +
        '        left outer join insurance_payments_transf c on (c.id = b.id) ' +
        'where   insurance_premium_status_id = :insurance_premium_status_id ' +
        'order by b.payment_date desc, b.id desc';

    ParamByName ('insurance_premium_status_id').AsLargeInt := status.FieldByName('id').AsLargeInt;
    open;
  end;

  ViewFilterMode := vfmAll;
end;

procedure TInsurancePaymentsPanelFrm.GridPaymentStatusDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
  Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
var
  startCurrentMonth: TDate;
  startCurrentPaymentPeriod, startNextPaymentPeriod: TDate;
  isFullyPaid: Boolean;
  interval: integer;
  premiumpayment: TPremiumPayment;

begin
  if (SameText(UpperCase(Column.FieldName), 'OPEN_AMOUNT'))
  then begin
    isFullyPaid               := (Status.FieldByName('Open_amount').AsCurrency <= 0);
    startCurrentMonth         := EnCodeDate(YearOf(Date), MonthOf(Date), 1);
    startCurrentPaymentPeriod := EncodeDate(Status.FieldByName('Period_year').AsInteger, Status.FieldByName('Period_month').AsInteger, 1);

    premiumpayment := SafePremiumPaymentCast(status.MasterSource.DataSet.FieldByName('premium_payment').AsInteger);
    case premiumpayment of
            ppMonthly:     Interval := 1;
            ppQuarterly:   Interval := 3;
            ppHalfYearly:  Interval := 6;
            ppYearly:      Interval := 12;
            else           Interval := 1; //koopsom
    end;
    startNextPaymentPeriod := IncMonth (startCurrentPaymentPeriod, interval);


    //Current month: orange/yellow (not fully paid) & green (fully paid)
    if ((startCurrentPaymentPeriod <= startCurrentMonth) and (startCurrentMonth < startNextPaymentPeriod)) or (premiumPayment = ppLumpSum) then
    begin
      if isFullyPaid then
      begin
        Attribs.Color       := UniMainModule.Color_green;
        Attribs.Font.Color  := ClWhite;
      end
      else begin
        Attribs.Color       := UniMainModule.Color_yellow;
        Attribs.Font.Color  := ClWhite;
      end;
    end;

    //Past months: red (not fully paid) & no color (fully paid)
    if (startCurrentMonth >= startNextPaymentPeriod) or (premiumPayment = ppLumpSum) then
    begin
      if not(isFullyPaid) then
      begin
        Attribs.Color       := UniMainModule.Color_red;
        Attribs.Font.Color  := ClWhite;
      end;
    end;


    //Future months: no color (not fully paid) & green (fully paid)
    if (startCurrentMonth < startCurrentPaymentPeriod) then
    begin
      if (isFullyPaid) then
      begin
        Attribs.Color       := UniMainModule.Color_green;
        Attribs.Font.Color  := ClWhite;
      end;
    end;
  end;
end;

procedure TInsurancePaymentsPanelFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  ViewFilterMode := vfmAll;
end;

procedure TInsurancePaymentsPanelFrm.UpdateStrings;
begin
  str_no_rights := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_no_rights' (* 'U heeft niet de nodige rechten' *) );
  str_3month_premium   := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_3month_premium' (* 'TrimestriŽle premie' *) );
  str_purchase_price   := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_purchase_price' (* 'Koopsom' *) );
  str_year_premium     := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_year_premium' (* 'Jaarpremie' *) );
  str_6month_premium   := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_6month_premium' (* '6-maand premie' *) );
  str_month_premium    := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_month_premium' (* 'Maandpremie' *) );
  str_payments         := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_payments' (* 'Betalingen polis' *) );
  str_error_deleting   := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_error_deleting' (* 'Fout bij het verwijderen van de betaling.' *) );
  str_delete_payment   := LinkedLanguageInsurancePaymentsPanel.GetTextOrDefault('strstr_delete_payment' (* 'Verwijderen betaling?' *) );
end;

procedure TInsurancePaymentsPanelFrm.BtnExportListClick(Sender: TObject);
var
  report: TReportPremiumsPayments;
  xls: TXlsFile;
  fn: string;
  AUrl: string;
begin
  report := TReportPremiumsPayments.Create(nil);
  try
    xls := report.CreateExcelReportPremiumsAndPayments(Status.FieldByName('insurance_file').AsLargeInt);
    try
      fn := UniServerModule.NewCacheFileUrl(False,'xlsx', FormatDateTime('yyyymmddhhmmss', Now), '', AUrl, True);
      xls.Save(fn);
      UniSession.SendFile(fn);
    finally
      FreeAndNil (xls);
    end;
  finally
    FreeAndnil (report);
  end;
end;

procedure TInsurancePaymentsPanelFrm.DsPaymentsDataChange(Sender: TObject;
  Field: TField);
begin
  if FViewFilterMode = vfmFixedPayment then
  begin
    if FPinnedPaymentId <> GridPayments.DataSource.DataSet.FieldByName('payment_id').AsLargeInt then
    begin
      FPinnedPaymentId := GridPayments.DataSource.DataSet.FieldByName('payment_id').AsLargeInt;
      ViewFilterMode := vfmFixedPayment;
    end;
  end;
end;

procedure TInsurancePaymentsPanelFrm.DsStatusDataChange(Sender: TObject;
  Field: TField);
begin
  if ViewFilterMode = vfmFixedPeriod then
  begin
    qryPremiumsToPayments.close;
    qryPremiumsToPayments.ParamByName ('insurance_premium_status_id').AsLargeInt := status.FieldByName('id').AsLargeInt;
    qryPremiumsToPayments.open;
  end;
end;

procedure TInsurancePaymentsPanelFrm.GridPaymentsDrawColumnCell(Sender: TObject;
  ACol, ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if (UpperCase(Column.FieldName) = 'PAYMENT_AMOUNT') and (GridPayments.datasource.dataset.FieldByName('Payment_amount').AsCurrency < 0)
  then begin
    Attribs.Color := UniMainModule.Color_red;
    Attribs.Font.Color := ClWhite;
  end;

  if  (GridPayments.datasource.dataset.FieldByName('Payment_method').asInteger = integer (pmDifference))
  then begin
    Attribs.Font.Style := Attribs.Font.Style + [fsBold];
  end;

  if  not(GridPayments.datasource.dataset.FieldByName('TransfId').IsNull)
  then begin
    Attribs.Font.Style := Attribs.Font.Style + [fsItalic];
  end;
end;

procedure TInsurancePaymentsPanelFrm.LinkedLanguageInsurancePaymentsPanelChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInsurancePaymentsPanelFrm.PaymentsCalcFields(DataSet: TDataSet);
begin
  if dataset.FieldByName('Payment_method').isNull then
  	dataset.FieldByName('Payment_method_description').AsString := ''
  else
  	dataset.FieldByName('Payment_method_description').AsString := PaymentMethodToText(SafePaymentMethodCast(dataset.FieldByName('Payment_method').AsInteger), LinkedLanguageInsurancePaymentsPanel);
end;

end.

