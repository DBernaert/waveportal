object DashboardInsuranceFilesFrm: TDashboardInsuranceFilesFrm
  Left = 0
  Top = 0
  Width = 1462
  Height = 819
  OnCreate = UniFrameCreate
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1462
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerHeaderLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 353
      Height = 33
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object LblTitle: TUniLabel
        Left = 0
        Top = 0
        Width = 101
        Height = 30
        Hint = ''
        Caption = 'Dashboard'
        ParentFont = False
        Font.Height = -21
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
            '= "lfp-pagetitle";'#13#10'}')
        TabOrder = 1
        LayoutConfig.Cls = 'boldtext'
      end
    end
    object ContainerHeaderRight: TUniContainerPanel
      Left = 1077
      Top = 0
      Width = 385
      Height = 33
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object ComboboxProduct: TUniComboBox
        Left = 0
        Top = 0
        Width = 385
        Height = 23
        Hint = ''
        Style = csDropDownList
        Text = 'Volledige portefeuille'
        Items.Strings = (
          'Volledige portefeuille')
        ItemIndex = 0
        TabOrder = 1
        TabStop = False
        FieldLabel = 'Product'
        IconItems = <>
        OnChange = ComboboxProductChange
      end
    end
  end
  object ContainerBody: TUniContainerPanel
    Left = 24
    Top = 64
    Width = 1417
    Height = 737
    Hint = ''
    ParentColor = False
    TabOrder = 1
    Layout = 'vbox'
    LayoutConfig.Region = 'center'
    object ContainerGeneralNumbers: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 1417
      Height = 33
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      LayoutConfig.Width = '100%'
      object LblTitleAverageInsuredCapital: TUniLabel
        Left = 0
        Top = 2
        Width = 298
        Height = 30
        Hint = ''
        Caption = 'Gemiddeld verzekerd kapitaal:'
        ParentFont = False
        Font.Height = -21
        Font.Style = [fsBold]
        TabOrder = 1
        LayoutConfig.Cls = 'boldtext'
      end
    end
    object ContainerStatistics: TUniContainerPanel
      Left = 8
      Top = 48
      Width = 1401
      Height = 313
      Hint = ''
      ParentColor = False
      TabOrder = 2
      Layout = 'hbox'
      LayoutConfig.Flex = 1
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '8 40 8 40'
      object ContainerTotals: TUniContainerPanel
        Left = 8
        Top = 16
        Width = 449
        Height = 281
        Hint = ''
        ParentColor = False
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Padding = '1'
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 20 0 0'
        object ContainerHeaderTotals: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 449
          Height = 89
          Hint = ''
          ParentColor = False
          Color = 6367488
          Align = alTop
          TabOrder = 1
          LayoutConfig.Region = 'north'
          object LblTitleTotals: TUniLabel
            Left = 8
            Top = 8
            Width = 57
            Height = 21
            Hint = ''
            Caption = 'Totalen'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -16
            Font.Style = [fsBold]
            TabOrder = 1
          end
          object LblTitleTotalNumberInsuranceFiles: TUniLabel
            Left = 8
            Top = 32
            Width = 77
            Height = 17
            Hint = ''
            Caption = 'Totaal aantal:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 2
          end
          object LblValueTotalNumberInsuranceFiles: TUniLabel
            Left = 104
            Top = 32
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 3
          end
          object LblTitleNumberOfFilesStopped: TUniLabel
            Left = 8
            Top = 48
            Width = 62
            Height = 17
            Hint = ''
            Caption = 'Stopgezet:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 4
          end
          object LblValueNumberOfFilesStopped: TUniLabel
            Left = 104
            Top = 48
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 5
          end
          object LblValuePercentageOfFilesStopped: TUniLabel
            Left = 232
            Top = 48
            Width = 57
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0,00 %'
            ParentFont = False
            Font.Color = clYellow
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 6
          end
          object LblTitleNetTotalNumberInsuranceFiles: TUniLabel
            Left = 8
            Top = 64
            Width = 81
            Height = 17
            Hint = ''
            Caption = 'Netto aantal:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 7
          end
          object LblValueNumberOfFilesActive: TUniLabel
            Left = 104
            Top = 64
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 8
          end
        end
        object ChartTotals: TUniFSGoogleChart
          Left = 0
          Top = 89
          Width = 449
          Height = 192
          Hint = ''
          ChartType = PieChart
          ChartDataSet = TblTotals
          ChartOptions.Strings = (
            'legend:{position: '#39'bottom'#39',},'
            'pieSliceText: '#39'value'#39','
            'slices: {  '
            '  0: {color: '#39'#DC3912'#39'},'
            '  1: {color: '#39'#0F9618'#39'},'
            '},'
            'pieHole: 0.4,'
            'pieStartAngle: 75,'
            'is3D:true,')
          Align = alClient
          LayoutConfig.Padding = '1 1 1 1'
          LayoutConfig.Region = 'center'
        end
      end
      object ContainerIndexation: TUniContainerPanel
        Left = 472
        Top = 16
        Width = 433
        Height = 281
        Hint = ''
        ParentColor = False
        TabOrder = 2
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Padding = '1'
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 20 0 20'
        object ContainerHeaderIndexation: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 433
          Height = 89
          Hint = ''
          ParentColor = False
          Color = 6367488
          Align = alTop
          TabOrder = 1
          LayoutConfig.Region = 'north'
          object LblTitleIndexation: TUniLabel
            Left = 8
            Top = 8
            Width = 72
            Height = 21
            Hint = ''
            Caption = 'Indexatie'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -16
            Font.Style = [fsBold]
            TabOrder = 1
          end
          object LblTitleIndexedInsuranceFiles: TUniLabel
            Left = 8
            Top = 32
            Width = 77
            Height = 17
            Hint = ''
            Caption = 'Ge'#239'ndexeerd:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 2
          end
          object LblTitleNonIndexedInsuranceFiles: TUniLabel
            Left = 8
            Top = 48
            Width = 105
            Height = 17
            Hint = ''
            Caption = 'Niet-ge'#239'ndexeerd:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 3
          end
          object LblValueNumberOfFilesNotIndexed: TUniLabel
            Left = 104
            Top = 48
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 4
          end
          object LblValueNumberOfFilesIndexed: TUniLabel
            Left = 104
            Top = 32
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 5
          end
          object LblValuePercentageOfFilesIndexed: TUniLabel
            Left = 232
            Top = 32
            Width = 57
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0,00 %'
            ParentFont = False
            Font.Color = clYellow
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 6
          end
          object LblValuePercentageOfFilesNotIndexed: TUniLabel
            Left = 232
            Top = 48
            Width = 57
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0,00 %'
            ParentFont = False
            Font.Color = clYellow
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 7
          end
        end
        object ChartIndexation: TUniFSGoogleChart
          Left = 0
          Top = 89
          Width = 433
          Height = 192
          Hint = ''
          ChartType = PieChart
          ChartDataSet = TblIndexation
          ChartOptions.Strings = (
            'legend:{position: '#39'bottom'#39',},'
            'pieSliceText: '#39'value'#39','
            'slices: {  '
            '  0: {color: '#39'#DC3912'#39'},'
            '  1: {color: '#39'#0F9618'#39'},'
            '},'
            'pieHole: 0.4,'
            'pieStartAngle: 75,'
            'is3D:true,')
          Align = alClient
          LayoutConfig.Padding = '1 1 1 1'
          LayoutConfig.Region = 'center'
        end
      end
      object ContainerRetard: TUniContainerPanel
        Left = 920
        Top = 16
        Width = 457
        Height = 281
        Hint = ''
        ParentColor = False
        TabOrder = 3
        Layout = 'border'
        LayoutConfig.Flex = 1
        LayoutConfig.Padding = '1'
        LayoutConfig.Height = '100%'
        LayoutConfig.Margin = '0 0 0 20'
        object ContainerHeaderRetard: TUniContainerPanel
          Left = 0
          Top = 0
          Width = 457
          Height = 89
          Hint = ''
          ParentColor = False
          Color = 6367488
          Align = alTop
          TabOrder = 1
          LayoutConfig.Region = 'north'
          object LblTitleBehindInsuranceFiles: TUniLabel
            Left = 8
            Top = 8
            Width = 92
            Height = 21
            Hint = ''
            Caption = 'Achterstand'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -16
            Font.Style = [fsBold]
            TabOrder = 1
          end
          object LblTitleBehindNumberInsuranceFiles: TUniLabel
            Left = 8
            Top = 32
            Width = 74
            Height = 17
            Hint = ''
            Caption = 'Achterstallig:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 2
          end
          object LblTitleNotBehindNumberInsuranceFiles: TUniLabel
            Left = 8
            Top = 48
            Width = 102
            Height = 17
            Hint = ''
            Caption = 'Niet-achterstallig:'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 3
          end
          object LblValueNumberOfFilesBackLog: TUniLabel
            Left = 104
            Top = 32
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 4
          end
          object LblValueNumberOfFilesNotBackLog: TUniLabel
            Left = 104
            Top = 48
            Width = 113
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Font.Color = clWhite
            Font.Height = -13
            TabOrder = 5
          end
          object LblValuePercentageOfFilesBackLog: TUniLabel
            Left = 232
            Top = 32
            Width = 57
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0,00 %'
            ParentFont = False
            Font.Color = clYellow
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 6
          end
          object LblValuePercentageOfFilesNotBackLog: TUniLabel
            Left = 232
            Top = 48
            Width = 57
            Height = 13
            Hint = ''
            Alignment = taRightJustify
            AutoSize = False
            Caption = '0,00 %'
            ParentFont = False
            Font.Color = clYellow
            Font.Height = -13
            Font.Style = [fsBold]
            TabOrder = 7
          end
        end
        object ChartBackLog: TUniFSGoogleChart
          Left = 0
          Top = 89
          Width = 457
          Height = 192
          Hint = ''
          ChartType = PieChart
          ChartDataSet = TblBackLog
          ChartOptions.Strings = (
            'legend:{position: '#39'bottom'#39',},'
            'pieSliceText: '#39'value'#39','
            'slices: {  '
            '  0: {color: '#39'#DC3912'#39'},'
            '  1: {color: '#39'#0F9618'#39'},'
            '},'
            'pieHole: 0.4,'
            'pieStartAngle: 75,'
            'is3D:true,')
          Align = alClient
          LayoutConfig.Padding = '1 1 1 1'
          LayoutConfig.Region = 'center'
        end
      end
    end
    object ContainerProducts: TUniContainerPanel
      Left = 8
      Top = 376
      Width = 1401
      Height = 337
      Hint = ''
      ParentColor = False
      TabOrder = 3
      Layout = 'hbox'
      LayoutAttribs.Align = 'top'
      LayoutAttribs.Pack = 'center'
      LayoutConfig.Flex = 1
      LayoutConfig.Width = '100%'
      LayoutConfig.Margin = '8 0 0 0'
      object PanelFamilyCare: TUniPanel
        Left = 152
        Top = 8
        Width = 449
        Height = 257
        Hint = ''
        TabOrder = 1
        BorderStyle = ubsSolid
        Caption = ''
        Color = clWhite
        LayoutConfig.Margin = '24 24 0 0'
        object LogoPatronale: TUniImage
          Left = 15
          Top = 17
          Width = 251
          Height = 55
          Hint = ''
          AutoSize = True
          Url = 'images/logo_patronale_life.png'
        end
        object LblShortCutPortalFamilyCare: TUniLabel
          Left = 288
          Top = 80
          Width = 152
          Height = 23
          Cursor = crHandPoint
          Hint = ''
          Alignment = taCenter
          AutoSize = False
          Caption = 'Portaal'
          ParentFont = False
          Font.Color = 16744448
          Font.Height = -16
          Font.Style = [fsUnderline]
          TabOrder = 2
          LayoutConfig.Cls = 'boldtext'
          OnClick = LblShortCutPortalFamilyCareClick
        end
        object LblTitleFamilyCare: TUniLabel
          Left = 288
          Top = 24
          Width = 152
          Height = 25
          Hint = ''
          Alignment = taCenter
          AutoSize = False
          Caption = 'FAMILY CARE'
          ParentFont = False
          Font.Height = -19
          Font.Style = [fsBold]
          TabOrder = 3
        end
        object ImagePatronale: TUniImage
          Left = 16
          Top = 80
          Width = 250
          Height = 141
          Hint = ''
          AutoSize = True
          Url = 'images/family_care_logo.jpg'
        end
        object AxaImage1: TUniImage
          Left = 248
          Top = 184
          Width = 50
          Height = 60
          Hint = ''
          AutoSize = True
          Url = 'images/assistance_by_axa.png'
        end
      end
      object PanelNucleus: TUniPanel
        Left = 784
        Top = 8
        Width = 449
        Height = 257
        Hint = ''
        TabOrder = 2
        BorderStyle = ubsSolid
        Caption = ''
        Color = clWhite
        LayoutConfig.Margin = '24 0 0 24'
        object ImagePrimaLifeProduction: TUniImage
          Left = 15
          Top = 17
          Width = 250
          Height = 44
          Hint = ''
          AutoSize = True
          Url = 'images/PrimaLife.png'
        end
        object LblShortCutSimulation: TUniLabel
          Left = 288
          Top = 112
          Width = 152
          Height = 23
          Cursor = crHandPoint
          Hint = ''
          Alignment = taCenter
          AutoSize = False
          Caption = 'Simulatie'
          ParentFont = False
          Font.Color = 16744448
          Font.Height = -16
          Font.Style = [fsUnderline]
          TabOrder = 2
          LayoutConfig.Cls = 'boldtext'
          OnClick = LblShortCutSimulationClick
        end
        object LblShortCutPortal: TUniLabel
          Left = 288
          Top = 80
          Width = 152
          Height = 33
          Cursor = crHandPoint
          Hint = ''
          Alignment = taCenter
          AutoSize = False
          Caption = 'Portaal'
          ParentFont = False
          Font.Color = 16744448
          Font.Height = -16
          Font.Style = [fsUnderline]
          TabOrder = 3
          LayoutConfig.Cls = 'boldtext'
          OnClick = LblShortCutPortalClick
        end
        object LblTitleFuneralCare: TUniLabel
          Left = 288
          Top = 24
          Width = 152
          Height = 25
          Hint = ''
          Alignment = taCenter
          AutoSize = False
          Caption = 'FUNERAL CARE'
          ParentFont = False
          Font.Height = -19
          Font.Style = [fsBold]
          TabOrder = 4
        end
        object ImageFuneralCare: TUniImage
          Left = 16
          Top = 80
          Width = 250
          Height = 141
          Hint = ''
          AutoSize = True
          Url = 'images/funeral_care_logo.jpg'
        end
        object AxaImage2: TUniImage
          Left = 248
          Top = 184
          Width = 50
          Height = 60
          Hint = ''
          AutoSize = True
          Url = 'images/assistance_by_axa.png'
        end
      end
    end
  end
  object Products: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select distinct'
      'insurance_institutions.id, '
      'insurance_institutions.name'
      'from insurance_institutions'
      
        'where insurance_institutions.id in (select insurance_institution' +
        ' from contr_insur_comission_schemes)'
      'and insurance_institutions.removed='#39'0'#39
      'order by insurance_institutions.name')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 608
    Top = 72
  end
  object TblTotals: TVirtualTable
    Active = True
    FieldDefs = <
      item
        Name = 'Description'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Quantity'
        DataType = ftCurrency
      end>
    Left = 208
    Top = 273
    Data = {
      040002000B004465736372697074696F6E010064000000000008005175616E74
      6974790700000000000000000000000000}
    object TblTotalsDescription: TStringField
      FieldName = 'Description'
      Size = 100
    end
    object TblTotalsQuantity: TCurrencyField
      FieldName = 'Quantity'
    end
  end
  object TblIndexation: TVirtualTable
    Active = True
    FieldDefs = <
      item
        Name = 'Description'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Quantity'
        DataType = ftCurrency
      end>
    Left = 568
    Top = 273
    Data = {
      040002000B004465736372697074696F6E010064000000000008005175616E74
      6974790700000000000000000000000000}
    object StringField1: TStringField
      FieldName = 'Description'
      Size = 100
    end
    object CurrencyField1: TCurrencyField
      FieldName = 'Quantity'
    end
  end
  object TblBackLog: TVirtualTable
    Active = True
    FieldDefs = <
      item
        Name = 'Description'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Quantity'
        DataType = ftCurrency
      end>
    Left = 1232
    Top = 273
    Data = {
      040002000B004465736372697074696F6E010064000000000008005175616E74
      6974790700000000000000000000000000}
    object StringField3: TStringField
      FieldName = 'Description'
      Size = 100
    end
    object CurrencyField3: TCurrencyField
      FieldName = 'Quantity'
    end
  end
  object LinkedLanguageDashboardInsuranceFiles: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'LblValueTotalNumberInsuranceFiles.Caption'
      'LblValueNumberOfFilesStopped.Caption'
      'LblValuePercentageOfFilesStopped.Caption'
      'LblValueNumberOfFilesActive.Caption'
      'LblValueNumberOfFilesNotIndexed.Caption'
      'LblValueNumberOfFilesIndexed.Caption'
      'LblValuePercentageOfFilesIndexed.Caption'
      'LblValuePercentageOfFilesNotIndexed.Caption'
      'LblValueNumberOfFilesBackLog.Caption'
      'LblValueNumberOfFilesNotBackLog.Caption'
      'LblValuePercentageOfFilesBackLog.Caption'
      'LblValuePercentageOfFilesNotBackLog.Caption'
      'TDashboardInsuranceFilesFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerHeaderLeft.Layout'
      'ContainerHeaderRight.Layout'
      'ComboboxProduct.FieldLabelSeparator'
      'ContainerBody.Layout'
      'ContainerGeneralNumbers.Layout'
      'ContainerStatistics.Layout'
      'ContainerTotals.Layout'
      'ContainerHeaderTotals.Layout'
      'ContainerIndexation.Layout'
      'ContainerHeaderIndexation.Layout'
      'ContainerRetard.Layout'
      'ContainerHeaderRetard.Layout'
      'ContainerProducts.Layout'
      'ChartTotals.ChartOptions'
      'ChartIndexation.ChartOptions'
      'ChartBackLog.ChartOptions'
      'TblTotalsDescription.DisplayLabel'
      'TblTotalsQuantity.DisplayLabel'
      'StringField1.DisplayLabel'
      'CurrencyField1.DisplayLabel'
      'StringField3.DisplayLabel'
      'CurrencyField3.DisplayLabel'
      'PanelFamilyCare.Layout'
      'LogoPatronale.Url'
      'PanelNucleus.Layout'
      'ImagePrimaLifeProduction.Url'
      'ImagePatronale.Url'
      'ImageFuneralCare.Url'
      'AxaImage1.Url'
      'AxaImage2.Url')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageDashboardInsuranceFilesChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 792
    Top = 336
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C0065000100440061007300
      680062006F0061007200640001005400610062006C0065006100750020006400
      6500200062006F00720064000100440061007300680062006F00610072006400
      01000D000A004C0062006C005400690074006C00650041007600650072006100
      6700650049006E00730075007200650064004300610070006900740061006C00
      0100470065006D0069006400640065006C00640020007600650072007A006500
      6B0065007200640020006B006100700069007400610061006C003A0001004300
      610070006900740061006C00200061007300730075007200E90020006D006F00
      790065006E003A0001004100760065007200610067006500200069006E007300
      750072006500640020006300610070006900740061006C003A0001000D000A00
      4C0062006C005400690074006C00650054006F00740061006C00730001005400
      6F00740061006C0065006E00010054006F007400610075007800010054006F00
      740061006C00730001000D000A004C0062006C005400690074006C0065005400
      6F00740061006C004E0075006D0062006500720049006E007300750072006100
      6E0063006500460069006C0065007300010054006F007400610061006C002000
      610061006E00740061006C003A0001004D006F006E00740061006E0074002000
      74006F00740061006C003A00010054006F00740061006C00200061006D006F00
      75006E0074003A0001000D000A004C0062006C005400690074006C0065004E00
      75006D006200650072004F006600460069006C0065007300530074006F007000
      7000650064000100530074006F007000670065007A00650074003A0001004100
      72007200EA007400E9003A00010044006900730063006F006E00740069006E00
      7500650064003A0001000D000A004C0062006C005400690074006C0065004E00
      6500740054006F00740061006C004E0075006D0062006500720049006E007300
      7500720061006E0063006500460069006C006500730001004E00650074007400
      6F002000610061006E00740061006C003A0001004D006F006E00740061006E00
      740020006E00650074003A0001004E006500740020006E0075006D0062006500
      72003A0001000D000A004C0062006C005400690074006C00650049006E006400
      650078006100740069006F006E00010049006E00640065007800610074006900
      6500010049006E006400650078006100740069006F006E00010049006E006400
      650078006100740069006F006E0001000D000A004C0062006C00540069007400
      6C00650049006E006400650078006500640049006E0073007500720061006E00
      63006500460069006C0065007300010047006500EF006E006400650078006500
      6500720064003A00010049006E00640065007800E9003A00010049006E006400
      65007800650064003A0001000D000A004C0062006C005400690074006C006500
      4E006F006E0049006E006400650078006500640049006E007300750072006100
      6E0063006500460069006C006500730001004E006900650074002D0067006500
      EF006E0064006500780065006500720064003A0001004E006F006E0020006900
      6E00640065007800E9003A0001004E006F007400200069006E00640065007800
      650064003A0001000D000A004C0062006C005400690074006C00650042006500
      680069006E00640049006E0073007500720061006E0063006500460069006C00
      6500730001004100630068007400650072007300740061006E00640001005200
      6500740061007200640001004200610063006B006C006F00670001000D000A00
      4C0062006C005400690074006C00650042006500680069006E0064004E007500
      6D0062006500720049006E0073007500720061006E0063006500460069006C00
      6500730001004100630068007400650072007300740061006C006C0069006700
      3A00010045006E0020007200650074006100720064003A0001004F0076006500
      72006400750065003A0001000D000A004C0062006C005400690074006C006500
      4E006F00740042006500680069006E0064004E0075006D006200650072004900
      6E0073007500720061006E0063006500460069006C006500730001004E006900
      650074002D006100630068007400650072007300740061006C006C0069006700
      3A000100500061007300200065006E0020007200650074006100720064003A00
      01004E006F00740020006F007600650072006400750065003A0001000D000A00
      4C0062006C00530068006F007200740043007500740050006F00720074006100
      6C00460061006D0069006C0079004300610072006500010050006F0072007400
      610061006C00010050006F0072007400610069006C00010050006F0072007400
      61006C0001000D000A004C0062006C00530068006F0072007400430075007400
      530069006D0075006C006100740069006F006E000100530069006D0075006C00
      61007400690065000100530069006D0075006C006100740069006F006E000100
      530069006D0075006C006100740069006F006E0001000D000A004C0062006C00
      530068006F007200740043007500740050006F007200740061006C0001005000
      6F0072007400610061006C00010050006F0072007400610069006C0001005000
      6F007200740061006C0001000D000A004C0062006C005400690074006C006500
      460061006D0069006C00790043006100720065000100460041004D0049004C00
      5900200043004100520045000100460041004D0049004C005900200043004100
      520045000100460041004D0049004C0059002000430041005200450001000D00
      0A004C0062006C005400690074006C006500460075006E006500720061006C00
      43006100720065000100460055004E004500520041004C002000430041005200
      45000100460055004E004500520041004C002000430041005200450001004600
      55004E004500520041004C002000430041005200450001000D000A0073007400
      480069006E00740073005F0055006E00690063006F00640065000D000A007300
      740044006900730070006C00610079004C006100620065006C0073005F005500
      6E00690063006F00640065000D000A007300740046006F006E00740073005F00
      55006E00690063006F00640065000D000A00730074004D0075006C0074006900
      4C0069006E00650073005F0055006E00690063006F00640065000D000A004300
      6F006D0062006F0062006F007800500072006F0064007500630074002E004900
      740065006D0073000100220056006F006C006C00650064006900670065002000
      70006F0072007400650066006500750069006C006C0065002200010022005000
      6F007200740066006F006C0069006F00200063006F006D0070006C0065007400
      220001002200460075006C006C00200070006F007200740066006F006C006900
      6F00220001000D000A007300740053007400720069006E00670073005F005500
      6E00690063006F00640065000D000A007300740072007300740072005F007300
      74006F0070007000650064000100530074006F007000670065007A0065007400
      010049006E0074006500720072006F006D007000750001004400690073006300
      6F006E00740069006E0075006500640001000D000A0073007400720073007400
      72005F006E006F0074005F00730074006F00700070006500640001004E006900
      650074002D00730074006F007000670065007A006500740001004E006F006E00
      200069006E0074006500720072006F006D007000750001004E006F006E002D00
      64006900730063006F006E00740069006E0075006500640001000D000A007300
      740072007300740072005F006E006F0074005F0069006E006400650078006500
      640001004E006900650074002D0067006500EF006E0064006500780065006500
      7200640001004E006F006E00200069006E00640065007800E90001004E006F00
      7400200069006E006400650078006500640001000D000A007300740072007300
      740072005F0069006E0064006500780065006400010047006500EF006E006400
      650078006500650072006400010049006E00640065007800E900010049006E00
      6400650078006500640001000D000A007300740072007300740072005F006D00
      650064006900630061006C0001004D0065006400690073006300680001004D00
      E90064006900630061006C0001004D00650064006900630061006C0001000D00
      0A007300740072007300740072005F006E006F0074005F006D00650064006900
      630061006C0001004E006900650074002D006D00650064006900730063006800
      01004E006F006E0020006D00E90064006900630061006C0001004E006F006E00
      2D006D00650064006900630061006C0001000D000A0073007400720073007400
      72005F006200610063006B006C006F0067000100410063006800740065007200
      7300740061006C006C0069006700010045006E00200072006500740061007200
      640001004F0076006500720064007500650001000D000A007300740072007300
      740072005F006E006F0074005F006200610063006B006C006F00670001004E00
      6900650074002D006100630068007400650072007300740061006C006C006900
      67000100500061007300200065006E0020007200650074006100720064000100
      4E006F00740020006F0076006500720064007500650001000D000A0073007400
      72007300740072005F0061007600650072006100670065005F0069006E007300
      75007200650064005F006300610070006900740061006C000100470065006D00
      69006400640065006C00640020007600650072007A0065006B00650072006400
      20006B006100700069007400610061006C003A00200001004300610070006900
      740061006C00200061007300730075007200E90020006D006F00790065006E00
      3A00200001004100760065007200610067006500200069006E00730075007200
      6500640020006300610070006900740061006C003A00200001000D000A007300
      74004F00740068006500720053007400720069006E00670073005F0055006E00
      690063006F00640065000D000A0043006F006D0062006F0062006F0078005000
      72006F0064007500630074002E004600690065006C0064004C00610062006500
      6C000100500072006F0064007500630074000100500072006F00640075006900
      74000100500072006F00640075006300740001000D000A0043006F006D006200
      6F0062006F007800500072006F0064007500630074002E005400650078007400
      010056006F006C006C0065006400690067006500200070006F00720074006500
      66006500750069006C006C006500010050006F007200740066006F006C006900
      6F00200063006F006D0070006C00650074000100460075006C006C0020007000
      6F007200740066006F006C0069006F0001000D000A007300740043006F006C00
      6C0065006300740069006F006E0073005F0055006E00690063006F0064006500
      0D000A0073007400430068006100720053006500740073005F0055006E006900
      63006F00640065000D000A00}
  end
end
