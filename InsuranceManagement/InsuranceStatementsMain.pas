unit InsuranceStatementsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniBasicGrid, uniDBGrid, uniButton,
  UniThemeButton, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, Data.DB,
  MemDS, DBAccess, IBC, siComp, siLngLnk;

type
  TInsuranceStatementsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerBodyDocuments: TUniContainerPanel;
    ContainerInsuranceStatements: TUniContainerPanel;
    ContainerFilterDocuments1: TUniContainerPanel;
    ContainerFilterRight: TUniContainerPanel;
    GridInsuranceStatements: TUniDBGrid;
    InsuranceStatements: TIBCQuery;
    InsuranceStatementsID: TLargeintField;
    InsuranceStatementsDOCUMENT_DATE: TDateField;
    InsuranceStatementsDOCUMENT_TOTAL_AMOUNT: TFloatField;
    InsuranceStatementsBATCH_MONTH: TIntegerField;
    InsuranceStatementsBATCH_YEAR: TIntegerField;
    DsInsuranceStatements: TDataSource;
    InsuranceStatementsINSURANCE_DOCUMENT_BATCH: TLargeintField;
    LinkedLanguageInsuranceStatementsMain: TsiLangLinked;
    InsuranceStatementsPRO_FORMA: TSmallintField;
    BtnPrintDocument: TUniThemeButton;
    procedure BtnPrintDocumentClick(Sender: TObject);
    procedure GridInsuranceStatementsFieldImageURL(
      const Column: TUniDBGridColumn; const AField: TField;
      var OutImageURL: string);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_insurance_statements_module;
  end;

implementation

{$R *.dfm}

uses MainModule, ReportRepository;



{ TInsuranceStatementsMainFrm }

procedure TInsuranceStatementsMainFrm.BtnPrintDocumentClick(Sender: TObject);
begin
  if (InsuranceStatements.Active = False) or (InsuranceStatements.FieldByName('Id').isNull)
  then exit;

  with ReportRepositoryFrm
  do Print_insurance_statement(InsuranceStatements.FieldByName('Id').AsInteger, True);
end;

procedure TInsuranceStatementsMainFrm.GridInsuranceStatementsFieldImageURL(
  const Column: TUniDBGridColumn; const AField: TField;
  var OutImageURL: string);
begin
  if SameText(UpperCase(AField.FieldName), 'PRO_FORMA')
  then begin
         Case AField.AsInteger of
           1:   OutImageURL := 'images/wv_check_green.png';
           else OutImageURL := 'images/wv_check_white.png';
         End;
       end;
end;

procedure TInsuranceStatementsMainFrm.Start_insurance_statements_module;
var SQL: string;
begin
  InsuranceStatements.Close;
  InsuranceStatements.SQL.Clear;
  SQL := 'SELECT INSURANCE_DOCUMENT.ID, INSURANCE_DOCUMENT.INSURANCE_DOCUMENT_BATCH, INSURANCE_DOCUMENT.DOCUMENT_DATE, ' +
         'INSURANCE_DOCUMENT.DOCUMENT_TOTAL_AMOUNT, INSURANCE_DOCUMENT_BATCH.BATCH_MONTH, INSURANCE_DOCUMENT_BATCH.BATCH_YEAR, ' +
         'INSURANCE_DOCUMENT.PRO_FORMA ' +
         'FROM INSURANCE_DOCUMENT ' +
         'LEFT OUTER JOIN INSURANCE_DOCUMENT_BATCH ON (INSURANCE_DOCUMENT.INSURANCE_DOCUMENT_BATCH = INSURANCE_DOCUMENT_BATCH.ID) ' +
         'WHERE INSURANCE_DOCUMENT.DOCUMENT_CONTRIBUTOR=' + IntToStr(UniMainModule.Reference_contributor_id) + ' ' +
         'ORDER BY INSURANCE_DOCUMENT_BATCH.BATCH_YEAR desc, INSURANCE_DOCUMENT_BATCH.BATCH_MONTH desc, INSURANCE_DOCUMENT.DOCUMENT_DATE desc';
  InsuranceStatements.SQL.Add(SQL);
  InsuranceStatements.Open;
end;

end.
