object InsuranceFilesAddFrm: TInsuranceFilesAddFrm
  Left = 0
  Top = 0
  ClientHeight = 713
  ClientWidth = 1033
  Caption = 'Raadplegen verzekeringspolis'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Images = UniMainModule.ImageList
  ImageIndex = 17
  OnCancel = BtnCloseClick
  TextHeight = 15
  object BtnClose: TUniThemeButton
    Left = 880
    Top = 664
    Width = 137
    Height = 33
    Hint = ''
    Caption = 'Sluiten'
    ParentFont = False
    TabOrder = 0
    ScreenMask.Target = Owner
    OnClick = BtnCloseClick
    ButtonTheme = uctSecondary
  end
  object EditUser: TUniDBLookupComboBox
    Left = 16
    Top = 144
    Width = 489
    Height = 23
    Hint = ''
    ListField = 'USERFULLNAME'
    ListSource = DsUsers
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'ASSIGNED_USER_ID'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 1
    ReadOnly = True
    Color = clWindow
    FieldLabel = 'Eigenaar'
    FieldLabelWidth = 160
    ForceSelection = True
    Style = csDropDown
  end
  object Edit_Contributor: TUniDBLookupComboBox
    Left = 16
    Top = 176
    Width = 489
    Height = 23
    Hint = ''
    ListFormat = '%s - %s'
    ListField = 'FULLNAME;Contributor_number'
    ListSource = DsContributors
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'CONTRIBUTOR_ID'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 2
    ReadOnly = True
    Color = clWindow
    FieldLabel = 'Tussenpersoon'
    FieldLabelWidth = 160
    ForceSelection = True
    Style = csDropDown
  end
  object EditStatus: TUniDBLookupComboBox
    Left = 16
    Top = 208
    Width = 489
    Height = 23
    Hint = ''
    ListField = 'DUTCH'
    ListSource = DsStatus
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'STATUS'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 3
    ReadOnly = True
    Color = clWindow
    FieldLabel = 'Status *'
    FieldLabelWidth = 160
    Style = csDropDown
  end
  object Edit_Insurance_institution: TUniDBLookupComboBox
    Left = 16
    Top = 240
    Width = 489
    Height = 23
    Hint = ''
    ListField = 'NAME'
    ListSource = DsInsuranceCompanies
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'INSURANCE_COMPANY'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 4
    ReadOnly = True
    Color = clWindow
    FieldLabel = 'Maatschappij'
    FieldLabelWidth = 160
    ForceSelection = True
    Style = csDropDown
    OnChange = Edit_Insurance_institutionChange
  end
  object EditInsuranceType: TUniDBLookupComboBox
    Left = 16
    Top = 272
    Width = 489
    Height = 23
    Hint = ''
    ListField = 'DUTCH'
    ListSource = DsInsuranceTypes
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'TYPE_OF_INSURANCE'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 5
    ReadOnly = True
    Color = clWindow
    FieldLabel = 'Type verzekering'
    FieldLabelWidth = 160
    Style = csDropDown
  end
  object EditTotalAmountInsurance: TUniDBFormattedNumberEdit
    Left = 16
    Top = 304
    Width = 489
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'TOTAL_AMOUNT_INSURANCE'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 6
    TabStop = False
    ReadOnly = True
    SelectOnFocus = True
    FieldLabel = 'Totaal verz. kapitaal'
    FieldLabelWidth = 160
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object EditStartDateInsurance: TUniDBDateTimePicker
    Left = 16
    Top = 592
    Width = 489
    Hint = ''
    DataField = 'START_DATE_INSURANCE'
    DataSource = Ds_InsuranceFiles_Edit
    DateTime = 43637.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    ReadOnly = True
    TabOrder = 7
    TabStop = False
    FieldLabel = 'Ingangsdatum'
    FieldLabelWidth = 160
  end
  object ContainerDemanders: TUniContainerPanel
    Left = 520
    Top = 400
    Width = 497
    Height = 249
    Hint = ''
    ParentColor = False
    TabOrder = 8
    Layout = 'border'
    object ContainerFilter: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 497
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Region = 'north'
      object ContainerFilterRight: TUniContainerPanel
        Left = 408
        Top = 0
        Width = 89
        Height = 25
        Hint = ''
        ParentColor = False
        Align = alRight
        TabOrder = 1
        LayoutConfig.Region = 'east'
      end
      object ContainerFilterLeft: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 177
        Height = 25
        Hint = ''
        ParentColor = False
        Align = alLeft
        TabOrder = 2
        LayoutConfig.Region = 'west'
        object LblInsuredPersons: TUniLabel
          Left = 0
          Top = 6
          Width = 66
          Height = 13
          Hint = ''
          Caption = 'Verzekerden:'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 1
        end
      end
    end
    object GridInsuredPersons: TUniDBGrid
      Left = 0
      Top = 25
      Width = 497
      Height = 224
      Hint = ''
      DataSource = DsInsuredPersons
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      ForceFit = True
      LayoutConfig.Cls = 'customGrid'
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 0 0 0'
      Align = alClient
      TabOrder = 2
      Columns = <
        item
          Flex = 1
          FieldName = 'INSURED_NAME'
          Title.Caption = 'Naam'
          Width = 370
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'INSURED_CAPITAL'
          Title.Alignment = taRightJustify
          Title.Caption = 'Verzekerd kapitaal'
          Width = 120
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
  end
  object EditInternalNumber: TUniDBEdit
    Left = 16
    Top = 48
    Width = 489
    Height = 22
    Hint = ''
    DataField = 'INTERNAL_NUMBER'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 9
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Intern nummer'
    FieldLabelWidth = 160
    SelectOnFocus = True
  end
  object EditInsuranceFileNumber: TUniDBEdit
    Left = 16
    Top = 16
    Width = 297
    Height = 22
    Hint = ''
    DataField = 'INSURANCE_FILE_NUMBER'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 10
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Polisnummer'
    FieldLabelWidth = 160
    SelectOnFocus = True
  end
  object EditCommissionAmount: TUniDBFormattedNumberEdit
    Left = 16
    Top = 496
    Width = 489
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'COMMISSION_AMOUNT'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 11
    TabStop = False
    ReadOnly = True
    SelectOnFocus = True
    FieldLabel = 'Commissie'
    FieldLabelWidth = 160
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object CheckBoxFirstMonthlyPremium: TUniDBCheckBox
    Left = 520
    Top = 144
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'FIRST_MONTHLY_PREMIUM_PAYED'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 14
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Eerste maandpremie in orde'
    FieldLabelWidth = 250
  end
  object CheckBoxSepaActive: TUniDBCheckBox
    Left = 520
    Top = 168
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'SEPA_DOMICILIATION_ACTIVE'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 15
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Sepa domicili'#235'ring actief'
    FieldLabelWidth = 250
  end
  object CheckBoxDocOkay: TUniDBCheckBox
    Left = 520
    Top = 192
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'REQUIRED_DOC_OKAY'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 16
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Documenten in orde'
    FieldLabelWidth = 250
  end
  object ComboCommissionSystem: TUniComboBox
    Left = 520
    Top = 80
    Width = 497
    Hint = ''
    Style = csDropDownList
    Text = ''
    Items.Strings = (
      'Aanbreng'
      'Incasso')
    TabOrder = 17
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Commissiesysteem'
    FieldLabelWidth = 160
    IconItems = <>
  end
  object ComboSepaDocDay: TUniComboBox
    Left = 520
    Top = 16
    Width = 497
    Hint = ''
    Style = csDropDownList
    Text = ''
    Items.Strings = (
      '5de werkdag'
      '10de werkdag')
    TabOrder = 18
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Sepa werkdag'
    FieldLabelWidth = 160
    IconItems = <>
  end
  object EditSepaStartDate: TUniDBDateTimePicker
    Left = 520
    Top = 48
    Width = 497
    Hint = ''
    DataField = 'SEPA_STARTDATE'
    DataSource = Ds_InsuranceFiles_Edit
    DateTime = 43889.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    ReadOnly = True
    TabOrder = 19
    TabStop = False
    FieldLabel = 'Sepa ingangsdatum'
    FieldLabelWidth = 160
  end
  object CheckBoxFileNumberFinal: TUniDBCheckBox
    Left = 320
    Top = 16
    Width = 185
    Height = 17
    Hint = ''
    DataField = 'INSURANCE_FILE_NUMBER_FINAL'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 20
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Definitief nummer'
    FieldLabelWidth = 150
  end
  object CheckboxIndexation: TUniDBCheckBox
    Left = 520
    Top = 256
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'INDEXATION'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 21
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Indexatie'
    FieldLabelWidth = 250
  end
  object CheckBoxCommissionReceived: TUniDBCheckBox
    Left = 520
    Top = 280
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'COMMISSION_RECEIVED'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 22
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Commissie ontvangen'
    FieldLabelWidth = 250
  end
  object EditSalesDateInsurance: TUniDBDateTimePicker
    Left = 16
    Top = 624
    Width = 489
    Hint = ''
    DataField = 'SALES_DATE_INSURANCE'
    DataSource = Ds_InsuranceFiles_Edit
    DateTime = 43637.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    ReadOnly = True
    TabOrder = 23
    TabStop = False
    FieldLabel = 'Verkoopdatum'
    FieldLabelWidth = 160
  end
  object Edit_Agent: TUniDBLookupComboBox
    Left = 520
    Top = 112
    Width = 497
    Height = 23
    Hint = ''
    ListFormat = '%s - %s'
    ListField = 'FULLNAME;Contributor_number'
    ListSource = DsContributors
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'AGENT_ID'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 24
    ReadOnly = True
    Color = clWindow
    FieldLabel = 'Agent'
    FieldLabelWidth = 160
    ForceSelection = True
    Style = csDropDown
  end
  object CheckBoxAdministrationOkay: TUniDBCheckBox
    Left = 520
    Top = 216
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'ADMINISTRATION_NOT_OKAY'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    ParentFont = False
    TabStop = False
    TabOrder = 25
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = 'Administratie niet in orde'
    FieldLabelWidth = 250
  end
  object CheckBoxCancelled: TUniDBCheckBox
    Left = 520
    Top = 336
    Width = 497
    Height = 17
    Hint = ''
    DataField = 'CANCELLED'
    DataSource = Ds_InsuranceFiles_Edit
    ValueChecked = '1'
    ValueUnchecked = '0'
    Caption = ''
    TabStop = False
    TabOrder = 26
    ParentColor = False
    Color = clBtnFace
    ReadOnly = True
    FieldLabel = '<font color="Red">Stopgezet</font>'
    FieldLabelWidth = 250
  end
  object ComboPremiumPayment: TUniComboBox
    Left = 16
    Top = 560
    Width = 489
    Hint = ''
    Style = csDropDownList
    Text = ''
    Items.Strings = (
      'Maandelijks'
      'Trimestrieel'
      'Semestrieel'
      'Jaarlijks')
    TabOrder = 13
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Premiebetaling *'
    FieldLabelWidth = 160
    IconItems = <>
  end
  object Edit_insurance_taker: TUniDBLookupComboBox
    Left = 200
    Top = 80
    Width = 305
    Height = 23
    Hint = ''
    ListField = 'FULLNAME'
    ListSource = DsContacts
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'INSURANCE_TAKER'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 27
    ReadOnly = True
    Color = clWindow
    FieldLabelWidth = 180
    Style = csDropDown
  end
  object RadiobuttonNaturalPerson: TUniRadioButton
    Left = 16
    Top = 80
    Width = 177
    Height = 17
    Hint = ''
    Checked = True
    Caption = 'Natuurlijk persoon:'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 28
    TabStop = False
    Group = 1
  end
  object RadiobuttonCompany: TUniRadioButton
    Left = 16
    Top = 112
    Width = 177
    Height = 17
    Hint = ''
    Caption = 'Bedrijf:'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 29
    TabStop = False
    Group = 1
  end
  object Edit_insurance_taker_2: TUniDBLookupComboBox
    Left = 200
    Top = 112
    Width = 305
    Height = 23
    Hint = ''
    ListField = 'NAME'
    ListSource = DsCrmAccounts
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'INSURANCE_TAKER'
    DataSource = Ds_InsuranceFiles_Edit
    TabStop = False
    TabOrder = 30
    ReadOnly = True
    Color = clWindow
    Style = csDropDown
  end
  object EditStructuredStatement: TUniDBEdit
    Left = 520
    Top = 368
    Width = 497
    Height = 22
    Hint = ''
    DataField = 'STRUCTURED_STATEMENT'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 31
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Mededeling'
    FieldLabelWidth = 160
  end
  object EditTotalAmountInsuranceEx: TUniDBFormattedNumberEdit
    Left = 16
    Top = 336
    Width = 489
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'TOTAL_AMOUNT_INSURANCE_EX'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 32
    TabStop = False
    ReadOnly = True
    SelectOnFocus = True
    FieldLabel = 'Aanvullend kapitaal'
    FieldLabelWidth = 160
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object ContainerRegular: TUniContainerPanel
    Left = 16
    Top = 368
    Width = 489
    Height = 121
    Hint = ''
    ParentColor = False
    TabOrder = 33
    object EditNetMonthPremiumAmount: TUniDBFormattedNumberEdit
      Left = 0
      Top = 0
      Width = 561
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'NET_MONTH_PREMIUM_AMOUNT'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 0
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Netto zonder taksen'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumAmount: TUniDBFormattedNumberEdit
      Left = 0
      Top = 32
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_AMOUNT'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 1
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumQuarterly: TUniDBFormattedNumberEdit
      Left = 248
      Top = 32
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_QUARTERLY'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 2
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Trimestri'#235'le premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumSemesterly: TUniDBFormattedNumberEdit
      Left = 0
      Top = 64
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_SEMESTERLY'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 3
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Semestri'#235'le premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumYearly: TUniDBFormattedNumberEdit
      Left = 248
      Top = 64
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_YEARLY'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 4
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Jaarlijkse premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditGrossPremium: TUniDBFormattedNumberEdit
      Left = 0
      Top = 96
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'GROSS_PREMIUM'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 5
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Bruto premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
  end
  object ContainerFamilyCare: TUniContainerPanel
    Left = 16
    Top = 368
    Width = 489
    Height = 121
    Hint = ''
    Visible = False
    ParentColor = False
    TabOrder = 34
    object EditPremiumTemporaryDeathCovers: TUniDBFormattedNumberEdit
      Left = 0
      Top = 0
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_TEMPORARY_DEATH_COVER'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 0
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Premie tijd. dekking'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumAdditionalDeathCover: TUniDBFormattedNumberEdit
      Left = 0
      Top = 32
      Width = 489
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_ADDITIONAL_DEATH_COVER'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 1
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Premie tijd. aanv. dekking'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumUKZT: TUniDBFormattedNumberEdit
      Left = 0
      Top = 64
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_UKZT'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 2
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Premie UKZT'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumAssistance: TUniDBFormattedNumberEdit
      Left = 248
      Top = 64
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_ASSISTANCE'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 3
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Premie bijstand'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditGrossPremium2: TUniDBFormattedNumberEdit
      Left = 0
      Top = 96
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'GROSS_PREMIUM'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 4
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Bruto premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object EditPremiumYearly2: TUniDBFormattedNumberEdit
      Left = 248
      Top = 96
      Width = 241
      Height = 22
      Hint = ''
      FormattedInput.ShowCurrencySign = True
      FormattedInput.DefaultCurrencySign = False
      FormattedInput.CurrencySign = #8364
      DataField = 'PREMIUM_YEARLY'
      DataSource = Ds_InsuranceFiles_Edit
      TabOrder = 5
      TabStop = False
      ReadOnly = True
      SelectOnFocus = True
      FieldLabel = 'Jaarlijkse bruto premie'
      FieldLabelWidth = 160
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
  end
  object EditPercentageIndexation: TUniDBFormattedNumberEdit
    Left = 16
    Top = 528
    Width = 489
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.CurrencySignPos = cpsRight
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = '%'
    DataField = 'PERCENTAGE_INDEXATION'
    DataSource = Ds_InsuranceFiles_Edit
    TabOrder = 12
    TabStop = False
    ReadOnly = True
    SelectOnFocus = True
    FieldLabel = 'Percentage indexatie'
    FieldLabelWidth = 160
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object InsuranceFiles_Edit: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_INSURANCE_FILES_ID'
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTr_InsuranceFiles_Edit
    SQL.Strings = (
      'Select * from insurance_files')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 672
    Top = 168
  end
  object Ds_InsuranceFiles_Edit: TDataSource
    DataSet = InsuranceFiles_Edit
    Left = 672
    Top = 213
  end
  object UpdTr_InsuranceFiles_Edit: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 672
    Top = 261
  end
  object Users: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, coalesce(NAME,'#39#39') || '#39' '#39' || coalesce(FIRSTNAME,'#39#39') AS' +
        ' "USERFULLNAME" from users'
      'order by USERFULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 880
    Top = 168
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 880
    Top = 216
  end
  object Status: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 880
    Top = 272
  end
  object DsStatus: TDataSource
    DataSet = Status
    Left = 880
    Top = 320
  end
  object Contributors: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, LAST_NAME || '#39' '#39' || FIRST_NAME AS "FULLNAME", contrib' +
        'utor_number from credit_contributors'
      'order by FULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 672
    Top = 312
  end
  object DsContributors: TDataSource
    DataSet = Contributors
    Left = 672
    Top = 360
  end
  object InsuranceCompanies: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, NAME from insurance_institutions')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 736
    Top = 32
  end
  object DsInsuranceCompanies: TDataSource
    DataSet = InsuranceCompanies
    Left = 736
    Top = 77
  end
  object InsuranceTypes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 784
    Top = 272
  end
  object DsInsuranceTypes: TDataSource
    DataSet = InsuranceTypes
    Left = 784
    Top = 320
  end
  object Contacts: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select id, coalesce(last_name, '#39#39') || '#39' '#39' || coalesce(first_name' +
        ', '#39#39') as FullName'
      'from crm_contacts'
      'order by FullName')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 624
    Top = 32
  end
  object DsContacts: TDataSource
    DataSet = Contacts
    Left = 624
    Top = 80
  end
  object InsuredPersons: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'SELECT insurance_insured_persons.id, insurance_insured_persons.i' +
        'nsured_capital, '
      'insurance_insured_persons.insured_person_contact_id,'
      
        'coalesce(SP.LAST_NAME, '#39#39') || '#39' '#39' || COALESCE(SP.FIRST_NAME, '#39#39')' +
        ' as insured_name'
      'FROM insurance_insured_persons'
      
        'LEFT OUTER JOIN crm_contacts SP ON insurance_insured_persons.ins' +
        'ured_person_contact_id = SP.ID'
      'WHERE insurance_insured_persons.insurance_file=:IDInsuranceFile '
      'ORDER BY insured_name')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 680
    Top = 416
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'IDInsuranceFile'
        ParamType = ptInput
        Value = nil
      end>
    object InsuredPersonsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InsuredPersonsINSURED_CAPITAL: TFloatField
      FieldName = 'INSURED_CAPITAL'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InsuredPersonsINSURED_PERSON_CONTACT_ID: TLargeintField
      FieldName = 'INSURED_PERSON_CONTACT_ID'
      Required = True
    end
    object InsuredPersonsINSURED_NAME: TWideStringField
      FieldName = 'INSURED_NAME'
      ReadOnly = True
      Size = 61
    end
  end
  object DsInsuredPersons: TDataSource
    DataSet = InsuredPersons
    Left = 672
    Top = 456
  end
  object LinkedLanguageInsuranceFileAdd: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TInsuranceFilesAddFrm.Layout'
      'EditUser.FieldLabelSeparator'
      'Edit_Contributor.FieldLabelSeparator'
      'EditCommissionAmount.FieldLabelSeparator'
      'InsuranceFiles_Edit.KeyGenerator'
      'EditInsuranceFileNumber.FieldLabelSeparator'
      'EditInternalNumber.FieldLabelSeparator'
      'ContainerDemanders.Layout'
      'ContainerFilter.Layout'
      'ContainerFilterRight.Layout'
      'ContainerFilterLeft.Layout'
      'EditStartDateInsurance.TimeFormat'
      'EditStartDateInsurance.FieldLabelSeparator'
      'EditStartDateInsurance.DateFormat'
      'EditTotalAmountInsurance.FieldLabelSeparator'
      'EditInsuranceType.FieldLabelSeparator'
      'Edit_Insurance_institution.FieldLabelSeparator'
      'EditStatus.FieldLabelSeparator'
      'InsuranceFiles_Edit.SQLDelete'
      'InsuranceFiles_Edit.SQLInsert'
      'InsuranceFiles_Edit.SQLLock'
      'InsuranceFiles_Edit.SQLRecCount'
      'InsuranceFiles_Edit.SQLRefresh'
      'InsuranceFiles_Edit.SQLUpdate'
      'UpdTr_InsuranceFiles_Edit.Params'
      'Users.SQLDelete'
      'Users.SQLInsert'
      'Users.SQLLock'
      'Users.SQLRecCount'
      'Users.SQLRefresh'
      'Users.SQLUpdate'
      'Status.SQLDelete'
      'Status.SQLInsert'
      'Status.SQLLock'
      'Status.SQLRecCount'
      'Status.SQLRefresh'
      'Status.SQLUpdate'
      'Contributors.SQLDelete'
      'Contributors.SQLInsert'
      'Contributors.SQLLock'
      'Contributors.SQLRecCount'
      'Contributors.SQLRefresh'
      'Contributors.SQLUpdate'
      'InsuranceCompanies.SQLDelete'
      'InsuranceCompanies.SQLInsert'
      'InsuranceCompanies.SQLLock'
      'InsuranceCompanies.SQLRecCount'
      'InsuranceCompanies.SQLRefresh'
      'InsuranceCompanies.SQLUpdate'
      'InsuranceTypes.SQLDelete'
      'InsuranceTypes.SQLInsert'
      'InsuranceTypes.SQLLock'
      'InsuranceTypes.SQLRecCount'
      'InsuranceTypes.SQLRefresh'
      'InsuranceTypes.SQLUpdate'
      'Contacts.SQLDelete'
      'Contacts.SQLInsert'
      'Contacts.SQLLock'
      'Contacts.SQLRecCount'
      'Contacts.SQLRefresh'
      'Contacts.SQLUpdate'
      'InsuredPersons.SQLDelete'
      'InsuredPersons.SQLInsert'
      'InsuredPersons.SQLLock'
      'InsuredPersons.SQLRecCount'
      'InsuredPersons.SQLRefresh'
      'InsuredPersons.SQLUpdate'
      'InsuredPersonsID.DisplayLabel'
      'InsuredPersonsINSURED_CAPITAL.DisplayLabel'
      'InsuredPersonsINSURED_PERSON_CONTACT_ID.DisplayLabel'
      'InsuredPersonsINSURED_NAME.DisplayLabel'
      'CheckBoxFirstMonthlyPremium.FieldLabelSeparator'
      'CheckBoxFirstMonthlyPremium.ValueChecked'
      'CheckBoxFirstMonthlyPremium.ValueUnchecked'
      'CheckBoxSepaActive.FieldLabelSeparator'
      'CheckBoxSepaActive.ValueChecked'
      'CheckBoxSepaActive.ValueUnchecked'
      'CheckBoxDocOkay.FieldLabelSeparator'
      'CheckBoxDocOkay.ValueChecked'
      'CheckBoxDocOkay.ValueUnchecked'
      'ComboCommissionSystem.FieldLabelSeparator'
      'ComboSepaDocDay.FieldLabelSeparator'
      'EditSepaStartDate.FieldLabelSeparator'
      'EditSepaStartDate.TimeFormat'
      'EditSepaStartDate.DateFormat'
      'CheckBoxFileNumberFinal.FieldLabelSeparator'
      'CheckBoxFileNumberFinal.ValueChecked'
      'CheckBoxFileNumberFinal.ValueUnchecked'
      'CheckboxIndexation.FieldLabelSeparator'
      'CheckboxIndexation.ValueChecked'
      'CheckboxIndexation.ValueUnchecked'
      'CheckBoxCommissionReceived.FieldLabelSeparator'
      'CheckBoxCommissionReceived.ValueChecked'
      'CheckBoxCommissionReceived.ValueUnchecked'
      'EditSalesDateInsurance.FieldLabelSeparator'
      'EditSalesDateInsurance.TimeFormat'
      'EditSalesDateInsurance.DateFormat'
      'Edit_Contributor.ListFormat'
      'Edit_Agent.ListFormat'
      'Edit_Agent.FieldLabelSeparator'
      'CheckBoxAdministrationOkay.FieldLabelSeparator'
      'CheckBoxAdministrationOkay.ValueChecked'
      'CheckBoxAdministrationOkay.ValueUnchecked'
      'CheckBoxCancelled.FieldLabelSeparator'
      'CheckBoxCancelled.ValueChecked'
      'CheckBoxCancelled.ValueUnchecked'
      'ComboPremiumPayment.FieldLabelSeparator'
      'Edit_insurance_taker.FieldLabelSeparator'
      'Edit_insurance_taker_2.FieldLabelSeparator'
      'EditStructuredStatement.FieldLabelSeparator'
      'EditTotalAmountInsuranceEx.FieldLabelSeparator'
      'EditNetMonthPremiumAmount.FieldLabelSeparator'
      'ContainerRegular.Layout'
      'EditPremiumAmount.FieldLabelSeparator'
      'EditPremiumQuarterly.FieldLabelSeparator'
      'EditPremiumSemesterly.FieldLabelSeparator'
      'EditPremiumYearly.FieldLabelSeparator'
      'EditGrossPremium.FieldLabelSeparator'
      'ContainerFamilyCare.Layout'
      'EditPremiumTemporaryDeathCovers.FieldLabelSeparator'
      'EditPremiumAdditionalDeathCover.FieldLabelSeparator'
      'EditPremiumUKZT.FieldLabelSeparator'
      'EditPremiumAssistance.FieldLabelSeparator'
      'EditGrossPremium2.FieldLabelSeparator'
      'EditPremiumYearly2.FieldLabelSeparator'
      'EditPercentageIndexation.FieldLabelSeparator')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 832
    Top = 464
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00540049006E0073007500720061006E006300650046006900
      6C0065007300410064006400460072006D000100520061006100640070006C00
      6500670065006E0020007600650072007A0065006B006500720069006E006700
      730070006F006C0069007300010043006F006E00730075006C00740065007200
      200070006F006C00690063006500200064002700610073007300750072006100
      6E0063006500010043006F006E00730075006C007400200069006E0073007500
      720061006E0063006500200070006F006C0069006300790001000D000A004200
      74006E0043006C006F0073006500010053006C0075006900740065006E000100
      4600650072006D0065007200010043006C006F007300650001000D000A004C00
      62006C0049006E007300750072006500640050006500720073006F006E007300
      01005600650072007A0065006B0065007200640065006E003A00010041007300
      730075007200E900650073003A00010049006E00730075007200650064003A00
      01000D000A0052006100640069006F0062007500740074006F006E004E006100
      74007500720061006C0050006500720073006F006E0001004E00610074007500
      750072006C0069006A006B00200070006500720073006F006F006E003A000100
      50006500720073006F006E006E00650020007000680079007300690071007500
      65003A0001004E00610074007500720061006C00200070006500720073006F00
      6E003A0001000D000A0052006100640069006F0062007500740074006F006E00
      43006F006D00700061006E0079000100420065006400720069006A0066003A00
      010053006F0063006900E9007400E9003A00010043006F006D00700061006E00
      79003A0001000D000A0073007400480069006E00740073005F0055006E006900
      63006F00640065000D000A007300740044006900730070006C00610079004C00
      6100620065006C0073005F0055006E00690063006F00640065000D000A007300
      740046006F006E00740073005F0055006E00690063006F00640065000D000A00
      730074004D0075006C00740069004C0069006E00650073005F0055006E006900
      63006F00640065000D000A0043006F006D0062006F0043006F006D006D006900
      7300730069006F006E00530079007300740065006D002E004900740065006D00
      73000100410061006E006200720065006E0067002C0049006E00630061007300
      73006F0001004100700070006C00690063006100740069006F006E002C004900
      6E0063006100730073006F0001004100700070006C0069006300610074006900
      6F006E002C0049006E0063006100730073006F0001000D000A0043006F006D00
      62006F00530065007000610044006F0063004400610079002E00490074006500
      6D0073000100220035006400650020007700650072006B006400610067002200
      2C002200310030006400650020007700650072006B0064006100670022000100
      22003500E8006D00650020006A006F007500720020006F007500760072006100
      62006C00650022002C00220031003000E8006D00650020006A006F0075007200
      20006F00750076007200610062006C0065002200010022003500740068002000
      77006F0072006B0069006E006700200064006100790022002C00220031003000
      74006800200077006F0072006B0069006E006700200064006100790022000100
      0D000A0043006F006D0062006F005000720065006D00690075006D0050006100
      79006D0065006E0074002E004900740065006D00730001004D00610061006E00
      640065006C0069006A006B0073002C005400720069006D006500730074007200
      6900650065006C002C00530065006D0065007300740072006900650065006C00
      2C004A006100610072006C0069006A006B00730001004D0065006E0073007500
      65006C002C005400720069006D006500730074007200690065006C002C005300
      65006D0069002D0061006E006E00750065006C002C0041006E006E0075006500
      6C006C0065006D0065006E00740001004D006F006E00740068006C0079002C00
      51007500610072007400650072006C0079002C00530065006D0069002D006100
      6E006E00750061006C002C0041006E006E00750061006C006C00790001000D00
      0A007300740053007400720069006E00670073005F0055006E00690063006F00
      640065000D000A007300740072007300740072005F00640065006C0065007400
      65005F0069006E00730075007200650064005F0070006500720073006F006E00
      0100560065007200770069006A0064006500720065006E002000760065007200
      7A0065006B0065007200640065003F0001005300750070007000720069006D00
      6500720020006C00270061007300730075007200E9003F000100440065006C00
      650074006500200069006E00730075007200650064003F0001000D000A007300
      740072007300740072005F006500720072006F0072005F00640065006C006500
      740069006E006700010046006F00750074002000620069006A00200068006500
      74002000760065007200770069006A0064006500720065006E002E0001004500
      7200720065007500720020006C006F007200730020006400650020006C006100
      20007300750070007000720065007300730069006F006E002E00010045007200
      72006F0072002000640065006C006500740069006E0067002E0001000D000A00
      7300740072007300740072005F007300740061007400750073005F0072006500
      7100750069007200650064000100440065002000730074006100740075007300
      2000690073002000650065006E00200076006500720070006C00690063006800
      74006500200069006E0067006100760065002E0001004C006500200073007400
      61007400750074002000650073007400200075006E006500200065006E007400
      7200E900650020006F0062006C0069006700610074006F006900720065002E00
      0100540068006500200073007400610074007500730020006900730020006100
      20006D0061006E006400610074006F0072007900200065006E00740072007900
      2E0001000D000A007300740072007300740072005F0069006E00730075007200
      61006E00630065005F0061006C00720065006100640079005F00650078006900
      7300740073000100440069007400200070006F006C00690073006E0075006D00
      6D00650072002000620065007300740061006100740020007200650065006400
      73002E0001004300650020006E0075006D00E90072006F002000640065002000
      70006F006C00690063006500200065007800690073007400650020006400E900
      6A00E0002E0001005400680069007300200070006F006C006900630079002000
      6E0075006D00620065007200200061006C007200650061006400790020006500
      780069007300740073002E0001000D000A007300740072007300740072005F00
      6500720072006F0072005F0073006100760069006E0067005F00640061007400
      6100010046006F00750074002000620069006A00200068006500740020006F00
      700073006C00610061006E002000760061006E00200064006500200067006500
      67006500760065006E007300010045007200720065007500720020006C006F00
      7200730020006400650020006C00270065006E00720065006700690073007400
      720065006D0065006E0074002000640065007300200064006F006E006E00E900
      6500730001004500720072006F007200200073006100760069006E0067002000
      7400680065002000640061007400610001000D000A0073007400720073007400
      72005F006100640064005F0069006E0073007500720061006E00630065000100
      54006F00650076006F006500670065006E0020007600650072007A0065006B00
      6500720069006E006700730070006F006C0069007300010041006A006F007500
      740065007200200070006F006C00690063006500200064002700610073007300
      7500720061006E00630065000100410064006400200069006E00730075007200
      61006E0063006500200070006F006C0069006300790001000D000A0073007400
      72007300740072005F006D006F0064006900660079005F0069006E0073007500
      720061006E00630065000100570069006A007A006900670065006E0020007600
      650072007A0065006B006500720069006E006700730070006F006C0069007300
      01004D006F00640069006600690065007200200070006F006C00690063006500
      2000640027006100730073007500720061006E006300650001004D006F006400
      690066007900200069006E0073007500720061006E0063006500200070006F00
      6C0069006300790001000D000A007300740072007300740072005F0073006100
      760065005F006300680061006E006700650073000100570065006E0073007400
      200075002000640065002000770069006A007A006900670069006E0067006500
      6E00200064006F006F007200200074006500200076006F006500720065006E00
      3F00010056006F0075006C0065007A002D0076006F0075007300200069006D00
      70006C00E9006D0065006E0074006500720020006C006500730020006D006F00
      640069006600690063006100740069006F006E0073003F00010044006F002000
      79006F0075002000770061006E007400200074006F00200069006D0070006C00
      65006D0065006E007400200074006800650020006300680061006E0067006500
      73003F0001000D000A007300740072007300740072005F006500720072006F00
      72005F0061006400640069006E006700010046006F0075007400200062006900
      6A002000680065007400200074006F00650076006F006500670065006E002E00
      010045007200720065007500720020006C006F00720073002000640065002000
      6C00270061006A006F00750074002E0001004500720072006F00720020006100
      6400640069006E0067002E0001000D000A007300740072007300740072005F00
      6C006F0063006B006500640001004B0061006E00200067006500670065007600
      65006E00730020006E006900650074002000770069006A007A00690067006500
      6E002C00200069006E0020006700650062007200750069006B00200064006F00
      6F0072002000650065006E00200061006E006400650072006500200067006500
      62007200750069006B0065007200010049006D0070006F007300730069006200
      6C00650020006400650020006D006F0064006900660069006500720020006C00
      65007300200064006F006E006E00E900650073002C0020007500740069006C00
      69007300E900650073002000700061007200200075006E002000610075007400
      7200650020007500740069006C00690073006100740065007500720001004300
      61006E006E006F00740020006300680061006E00670065002000640061007400
      61002C00200069006E002000750073006500200062007900200061006E006F00
      74006800650072002000750073006500720001000D000A00730074004F007400
      68006500720053007400720069006E00670073005F0055006E00690063006F00
      640065000D000A0043006800650063006B0042006F007800430061006E006300
      65006C006C00650064002E004600690065006C0064004C006100620065006C00
      01003C0066006F006E007400200063006F006C006F0072003D00220052006500
      640022003E00530074006F007000670065007A00650074003C002F0066006F00
      6E0074003E0001003C0066006F006E007400200063006F006C006F0072003D00
      220052006500640022003E00410072007200EA007400E9003C002F0066006F00
      6E0074003E0001003C0066006F006E007400200063006F006C006F0072003D00
      220052006500640022003E00530074006F0070007000650064003C002F006600
      6F006E0074003E0001000D000A0043006800650063006B0042006F0078004100
      64006D0069006E0069007300740072006100740069006F006E004F006B006100
      79002E004600690065006C0064004C006100620065006C000100410064006D00
      69006E006900730074007200610074006900650020006E006900650074002000
      69006E0020006F007200640065000100410064006D0069006E00690073007400
      72006100740069006F006E002000700061007300200065006E0020006F007200
      6400720065000100410064006D0069006E006900730074007200610074006900
      6F006E0020006E006F007400200069006E0020006F0072006400650072000100
      0D000A0045006400690074005F004100670065006E0074002E00460069006500
      6C0064004C006100620065006C0001004100670065006E007400010041006700
      65006E00740001004100670065006E00740001000D000A004500640069007400
      54006F00740061006C0041006D006F0075006E00740049006E00730075007200
      61006E00630065002E004600690065006C0064004C006100620065006C000100
      54006F007400610061006C0020007600650072007A002E0020006B0061007000
      69007400610061006C0001004300610070006900740061006C00200061007300
      730075007200E900200074006F00740061006C00010054006F00740061006C00
      200069006E007300750072006500640020006300610070006900740061006C00
      01000D000A00450064006900740054006F00740061006C0041006D006F007500
      6E00740049006E0073007500720061006E0063006500450078002E0046006900
      65006C0064004C006100620065006C000100410061006E00760075006C006C00
      65006E00640020006B006100700069007400610061006C000100430061007000
      6900740061006C00200073007500700070006C002E0001004100640064006900
      740069006F006E0061006C0020006300610070006900740061006C0001000D00
      0A00450064006900740043006F006D006D0069007300730069006F006E004100
      6D006F0075006E0074002E004600690065006C0064004C006100620065006C00
      010043006F006D006D0069007300730069006500010043006F006D006D006900
      7300730069006F006E00010043006F006D006D0069007300730069006F006E00
      01000D000A0043006800650063006B0042006F00780043006F006D006D006900
      7300730069006F006E00520065006300650069007600650064002E0046006900
      65006C0064004C006100620065006C00010043006F006D006D00690073007300
      6900650020006F006E007400760061006E00670065006E00010043006F006D00
      6D0069007300730069006F006E00200072006500E70075006500010043006F00
      6D006D0069007300730069006F006E0020007200650063006500690076006500
      640001000D000A00450064006900740053007400610072007400440061007400
      650049006E0073007500720061006E00630065002E004600690065006C006400
      4C006100620065006C00010049006E00670061006E0067007300640061007400
      75006D000100440061007400650020006400270065006E0074007200E9006500
      01005300740061007200740069006E0067002000640061007400650001000D00
      0A00450064006900740053006500700061005300740061007200740044006100
      740065002E004600690065006C0064004C006100620065006C00010053006500
      70006100200069006E00670061006E006700730064006100740075006D000100
      440061007400650020006400650020006400E900620075007400200064007500
      2000530045005000410001005300650070006100200073007400610072007400
      2000640061007400650001000D000A004500640069007400530061006C006500
      7300440061007400650049006E0073007500720061006E00630065002E004600
      690065006C0064004C006100620065006C0001005600650072006B006F006F00
      700064006100740075006D000100440061007400650020006400650020007600
      65006E00740065000100530061006C0065002000640061007400650001000D00
      0A0043006800650063006B0042006F00780044006F0063004F006B0061007900
      2E004600690065006C0064004C006100620065006C00010044006F0063007500
      6D0065006E00740065006E00200069006E0020006F0072006400650001004400
      6F00630075006D0065006E0074007300200065006E0020006F00720064007200
      6500010044006F00630075006D0065006E0074007300200069006E0020006F00
      720064006500720001000D000A0043006800650063006B0042006F0078005300
      6500700061004100630074006900760065002E004600690065006C0064004C00
      6100620065006C0001005300650070006100200064006F006D00690063006900
      6C006900EB00720069006E006700200061006300740069006500660001004400
      E900620069007400200064006900720065006300740020005300450050004100
      2000610063007400690066000100530065007000610020006400690072006500
      6300740020006400650062006900740020006100630074006900760065000100
      0D000A0043006800650063006B0062006F00780049006E006400650078006100
      740069006F006E002E004600690065006C0064004C006100620065006C000100
      49006E006400650078006100740069006500010049006E006400650078006100
      740069006F006E00010049006E006400650078006100740069006F006E000100
      0D000A0045006400690074005F0043006F006E00740072006900620075007400
      6F0072002E004600690065006C0064004C006100620065006C00010054007500
      7300730065006E0070006500720073006F006F006E00010049006E0074006500
      72006D00E900640069006100690072006500010049006E007400650072006D00
      65006400690061007200790001000D000A0043006F006D0062006F0053006500
      7000610044006F0063004400610079002E004600690065006C0064004C006100
      620065006C000100530065007000610020007700650072006B00640061006700
      01004A006F007500720020006F00750076007200610062006C00650020005300
      45005000410001005300650070006100200077006F0072006B0069006E006700
      200064006100790001000D000A00450064006900740053007400720075006300
      74007500720065006400530074006100740065006D0065006E0074002E004600
      690065006C0064004C006100620065006C0001004D0065006400650064006500
      6C0069006E006700010043006F006D006D0075006E0069006300610074006900
      6F006E0001004D0065007300730061006700650001000D000A00450064006900
      74004E00650074004D006F006E00740068005000720065006D00690075006D00
      41006D006F0075006E0074002E004600690065006C0064004C00610062006500
      6C0001004E006500740074006F0020007A006F006E0064006500720020007400
      61006B00730065006E0001004E00650074002000730061006E00730020007400
      610078006500730001004E0065007400200077006900740068006F0075007400
      20007400610078006500730001000D000A0043006800650063006B0042006F00
      7800460069006C0065004E0075006D00620065007200460069006E0061006C00
      2E004600690065006C0064004C006100620065006C0001004400650066006900
      6E006900740069006500660020006E0075006D006D006500720001004E007500
      6D00E90072006F0020006400E900660069006E00690074006900660001004400
      6500660069006E006900740069007600650020006E0075006D00620065007200
      01000D000A00450064006900740049006E007400650072006E0061006C004E00
      75006D006200650072002E004600690065006C0064004C006100620065006C00
      010049006E007400650072006E0020006E0075006D006D006500720001004E00
      75006D00E90072006F00200069006E007400650072006E006500010049006E00
      7400650072006E0061006C0020006E0075006D0062006500720001000D000A00
      450064006900740049006E0073007500720061006E0063006500460069006C00
      65004E0075006D006200650072002E004600690065006C0064004C0061006200
      65006C00010050006F006C00690073006E0075006D006D006500720001004E00
      B000200070006F006C00690063006500010050006F006C006900630079002000
      6E0075006D0062006500720001000D000A0043006F006D0062006F0050007200
      65006D00690075006D005000610079006D0065006E0074002E00460069006500
      6C0064004C006100620065006C0001005000720065006D006900650062006500
      740061006C0069006E00670020002A00010050006100690065006D0065006E00
      740020007000720069006D00650020002A0001005000720065006D0069007500
      6D0020007000610079006D0065006E00740020002A0001000D000A0043006800
      650063006B0042006F007800460069007200730074004D006F006E0074006800
      6C0079005000720065006D00690075006D002E004600690065006C0064004C00
      6100620065006C00010045006500720073007400650020006D00610061006E00
      64007000720065006D0069006500200069006E0020006F007200640065000100
      5000720065006D006900E8007200650020007000720069006D00650020006D00
      65006E007300750065006C006C006500200065006E0020006F00720064007200
      650001004600690072007300740020006D006F006E00740068006C0079002000
      7000720065006D00690075006D00200069006E0020006F007200640065007200
      01000D000A0045006400690074005000720065006D00690075006D0041006D00
      6F0075006E0074002E004600690065006C0064004C006100620065006C000100
      5000720065006D006900650001005000720069006D0065000100500072006500
      6D00690075006D0001000D000A0045006400690074005000720065006D006900
      75006D0059006500610072006C0079002E004600690065006C0064004C006100
      620065006C0001004A006100610072006C0069006A006B007300650020007000
      720065006D006900650001005000720069006D006500200061006E006E007500
      65006C006C006500010041006E006E00750061006C0020007000720065006D00
      690075006D0001000D000A0045006400690074005000720065006D0069007500
      6D0041007300730069007300740061006E00630065002E004600690065006C00
      64004C006100620065006C0001005000720065006D0069006500200062006900
      6A007300740061006E00640001005000720069006D0065002000610073007300
      69007300740061006E006300650001005000720065006D00690075006D002000
      61007300730069007300740061006E006300650001000D000A00450064006900
      7400470072006F00730073005000720065006D00690075006D002E0046006900
      65006C0064004C006100620065006C00010042007200750074006F0020007000
      720065006D006900650001005000720069006D00650020006200720075007400
      65000100470072006F007300730020007000720065006D00690075006D000100
      0D000A004500640069007400470072006F00730073005000720065006D006900
      75006D0032002E004600690065006C0064004C006100620065006C0001004200
      7200750074006F0020007000720065006D006900650001005000720069006D00
      65002000620072007500740065000100470072006F0073007300200070007200
      65006D00690075006D0001000D000A0045006400690074005000720065006D00
      690075006D0059006500610072006C00790032002E004600690065006C006400
      4C006100620065006C0001004A006100610072006C0069006A006B0073006500
      200062007200750074006F0020007000720065006D0069006500010050007200
      69006D006500200062007200750074006500200061006E006E00750065006C00
      6C006500010041006E006E00750061006C002000670072006F00730073002000
      7000720065006D00690075006D0001000D000A00450064006900740050007200
      65006D00690075006D00540065006D0070006F00720061007200790044006500
      61007400680043006F0076006500720073002E004600690065006C0064004C00
      6100620065006C0001005000720065006D00690065002000740069006A006400
      2E002000640065006B006B0069006E00670001005000720069006D0065002000
      63006F00750076006500720074002E002000740065006D0070002E0001005000
      720065006D00690075006D002000740065006D0070002E00200063006F007600
      6500720001000D000A0045006400690074005000720065006D00690075006D00
      4100640064006900740069006F006E0061006C00440065006100740068004300
      6F007600650072002E004600690065006C0064004C006100620065006C000100
      5000720065006D00690065002000740069006A0064002E002000610061006E00
      76002E002000640065006B006B0069006E00670001005000720069006D006500
      200063006F00750076006500720074002E002000740065006D0070002E002000
      6100640064002E0001005000720065006D00690075006D002000740065006D00
      700020006100640064002E00200063006F006E0076006500720001000D000A00
      45006400690074005000720065006D00690075006D00530065006D0065007300
      7400650072006C0079002E004600690065006C0064004C006100620065006C00
      0100530065006D0065007300740072006900EB006C0065002000700072006500
      6D006900650001005000720069006D0065002000730065006D0069002D007300
      E9006D0069006E0061006900720065000100530065006D0069002D0073006500
      6D0069006E006100720020007000720065006D00690075006D0001000D000A00
      45006400690074005000720065006D00690075006D0051007500610072007400
      650072006C0079002E004600690065006C0064004C006100620065006C000100
      5400720069006D0065007300740072006900EB006C0065002000700072006500
      6D006900650001005000720069006D00650020007400720069006D0065007300
      74007200690065006C006C006500010051007500610072007400650072006C00
      790020007000720065006D00690075006D0001000D000A004500640069007400
      5000720065006D00690075006D0055004B005A0054002E004600690065006C00
      64004C006100620065006C0001005000720065006D0069006500200055004B00
      5A00540001005000720069006D00650020004300440053005200010055004B00
      5A00540020007000720065006D00690075006D0001000D000A00450064006900
      740055007300650072002E004600690065006C0064004C006100620065006C00
      010045006900670065006E006100610072000100500072006F00700072006900
      E9007400610069007200650001004F0077006E006500720001000D000A004500
      6400690074005F0049006E0073007500720061006E00630065005F0069006E00
      73007400690074007500740069006F006E002E004600690065006C0064004C00
      6100620065006C0001004D006100610074007300630068006100700070006900
      6A00010053006F0063006900E9007400E900010043006F006D00700061006E00
      790001000D000A0045006400690074005300740061007400750073002E004600
      690065006C0064004C006100620065006C000100530074006100740075007300
      20002A00010053007400610074007500740020002A0001005300740061007400
      7500730020002A0001000D000A0043006F006D0062006F0043006F006D006D00
      69007300730069006F006E00530079007300740065006D002E00460069006500
      6C0064004C006100620065006C00010043006F006D006D006900730073006900
      65007300790073007400650065006D0001005300790073007400E8006D006500
      200064006500200063006F006D006D0069007300730069006F006E0001004300
      6F006D006D0069007300730069006F006E002000730079007300740065006D00
      01000D000A00450064006900740049006E0073007500720061006E0063006500
      54007900700065002E004600690065006C0064004C006100620065006C000100
      540079007000650020007600650072007A0065006B006500720069006E006700
      010054007900700065002000640027006100730073007500720061006E006300
      65000100540079007000650020006F006600200069006E007300750072006100
      6E006300650001000D000A004500640069007400500065007200630065006E00
      740061006700650049006E006400650078006100740069006F006E002E004600
      690065006C0064004C006100620065006C000100500065007200630065006E00
      7400610067006500200069006E00640065007800610074006900650001005000
      6F0075007200630065006E007400610067006500200069006E00640065007800
      6100740069006F006E000100500065007200630065006E007400610067006500
      200069006E006400650078006100740069006F006E0001000D000A0073007400
      43006F006C006C0065006300740069006F006E0073005F0055006E0069006300
      6F00640065000D000A00470072006900640049006E0073007500720065006400
      50006500720073006F006E0073002E0043006F006C0075006D006E0073005B00
      30005D002E0043006800650063006B0042006F0078004600690065006C006400
      2E004600690065006C006400560061006C007500650073000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C007300650001000D000A00
      470072006900640049006E007300750072006500640050006500720073006F00
      6E0073002E0043006F006C0075006D006E0073005B0030005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001004E00610061006D00
      01004E006F006D0001004E0061006D00650001000D000A004700720069006400
      49006E007300750072006500640050006500720073006F006E0073002E004300
      6F006C0075006D006E0073005B0031005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A00470072006900640049006E00730075007200
      6500640050006500720073006F006E0073002E0043006F006C0075006D006E00
      73005B0031005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001005600650072007A0065006B0065007200640020006B0061007000
      69007400610061006C0001004300610070006900740061006C00200061007300
      730075007200E900010049006E00730075007200650064002000630061007000
      6900740061006C0001000D000A00730074004300680061007200530065007400
      73005F0055006E00690063006F00640065000D000A00}
  end
  object CrmAccounts: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select id, name from crm_accounts')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 904
    Top = 32
  end
  object DsCrmAccounts: TDataSource
    DataSet = CrmAccounts
    Left = 904
    Top = 79
  end
end
