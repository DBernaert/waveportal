unit InsuranceFilesMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniImage, uniLabel, uniGUIBaseClasses, uniPanel,
  uniButton, UniThemeButton, uniMultiItem, uniComboBox, uniEdit, uniBasicGrid,
  uniDBGrid, Data.DB, MemDS, DBAccess, IBC, siComp, siLngLnk;

var
	str_insurancefiles: string = 'Verzekeringspolissen'; // TSI: Localized (Don't modify!)
	str_all_insurances: string = 'Alle verzekeringen'; // TSI: Localized (Don't modify!)
	str_all_companies: string = 'Alle maatschappijen'; // TSI: Localized (Don't modify!)

type
  TInsuranceFilesMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    InsuranceFiles: TIBCQuery;
    InsuranceFilesID: TLargeintField;
    InsuranceFilesINSURANCE_FILE_NUMBER: TWideStringField;
    InsuranceFilesINTERNAL_NUMBER: TWideStringField;
    InsuranceFilesTOTAL_AMOUNT_INSURANCE: TFloatField;
    InsuranceFilesSTART_DATE_INSURANCE: TDateField;
    InsuranceFilesSTATUS: TWideStringField;
    InsuranceFilesCONTRIBUTOR_FULLNAME: TWideStringField;
    InsuranceFilesNAME: TWideStringField;
    InsuranceFilesCONTACT_FULLNAME: TWideStringField;
    InsuranceFilesFIRST_MONTHLY_PREMIUM_PAYED: TSmallintField;
    InsuranceFilesSEPA_DOMICILIATION_ACTIVE: TSmallintField;
    InsuranceFilesREQUIRED_DOC_OKAY: TSmallintField;
    InsuranceFilesINSURANCE_TAKER: TLargeintField;
    InsuranceFilesCONTRIBUTOR_ID: TLargeintField;
    InsuranceFilesINSURANCE_FILE_NUMBER_FINAL: TSmallintField;
    InsuranceFilesCOMPANY_NAME: TWideStringField;
    InsuranceFilesAGENT_FULLNAME: TWideStringField;
    DsInsuranceFiles: TDataSource;
    GridInsuranceFiles: TUniDBGrid;
    LinkedLanguageInsuranceFilesMain: TsiLangLinked;
    PanelJournal: TUniPanel;
    InsuranceFilesCANCELLED: TSmallintField;
    BtnSearch: TUniThemeButton;
    ComboboxPremie: TUniComboBox;
    InsuranceFilesPHONE_MOBILE: TWideStringField;
    InsuranceFilesPREMIUM_AMOUNT: TFloatField;
    InsuranceFilesSTRUCTURED_STATEMENT: TWideStringField;
    BtnViewFile: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    ComboStatus: TUniComboBox;
    ComboInsuranceCompany: TUniComboBox;
    Status: TIBCQuery;
    DsStatus: TDataSource;
    InsuranceCompanies: TIBCQuery;
    InsuranceFilesCOMMISSION_RECEIVED: TSmallintField;
    InsuranceFilesPREMIUM_PAYMENT: TSmallintField;
    procedure BtnExportListClick(Sender: TObject);
    procedure BtnViewFileClick(Sender: TObject);
    procedure UniFrameCreate(Sender: TObject);
    procedure LinkedLanguageInsuranceFilesMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure GridInsuranceFilesDrawColumnCell(Sender: TObject; ACol,
      ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure BtnSearchClick(Sender: TObject);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure InsuranceFilesFIRST_MONTHLY_PREMIUM_PAYEDGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure InsuranceFilesSEPA_DOMICILIATION_ACTIVEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure InsuranceFilesREQUIRED_DOC_OKAYGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure InsuranceFilesCOMMISSION_RECEIVEDGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  private
    { Private declarations }
    JournalFrame: TUniFrame;
    procedure Open_insurance_files;
  public
    { Public declarations }
    procedure Start_insurance_files_module;
  end;

implementation

{$R *.dfm}

uses Main, MainModule, Utils, InsuranceFilesAdd, Journal;


{ TInsuranceFilesMainFrm }

procedure TInsuranceFilesMainFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridInsuranceFiles, str_insurancefiles, InsuranceFiles, 'Id');
end;

procedure TInsuranceFilesMainFrm.Start_insurance_files_module;
var SQL: string;
begin
  JournalFrame := TJournalFrm.Create(Self);
  With (JournalFrame as TJournalFrm)
  do begin
       Start_journal(10, DsInsuranceFiles);
       Parent := PanelJournal;
     end;

  Status.Close;
  Status.SQL.Clear;
  SQL := 'Select ID, DUTCH, FRENCH from BASICTABLES' +
         ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
         ' and TABLEID=6000 ' +
         ' and REMOVED=''0''';
  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SQL := SQL + ' order by DUTCH';
    2: SQL := SQL + ' order by FRENCH';
    3: SQL := SQL + ' order by ENGLISH';
  end;
  Status.SQL.Add(SQL);
  Status.Open;
  Status.First;

  ComboStatus.Items.Clear;
  ComboStatus.Items.Add(str_all_insurances);
  while not Status.Eof
  do begin
       case UniMainModule.LanguageManager.ActiveLanguage of
         1: ComboStatus.Items.Add(Status.FieldByName('Dutch').asString);
         2: ComboStatus.Items.Add(Status.FieldByName('French').AsString);
         3: ComboStatus.Items.Add(Status.FieldByName('English').AsString);
       end;
       Status.Next;
     end;
  ComboStatus.ItemIndex := 0;

  InsuranceCompanies.Close;
  InsuranceCompanies.SQL.Clear;
  SQL := 'Select ID, NAME from insurance_institutions ' +
         'where companyId=' + intToStr(UniMainModule.Company_id) + ' ' +
         'and removed=''0'' order by name';
  InsuranceCompanies.SQL.Add(SQL);
  InsuranceCompanies.Open;
  InsuranceCompanies.First;
  ComboInsuranceCompany.Items.Clear;
  ComboInsuranceCompany.Items.Add(str_all_companies);
  while not InsuranceCompanies.Eof
  do begin
       ComboInsuranceCompany.Items.Add(InsuranceCompanies.FieldByName('Name').asString);
       InsuranceCompanies.Next;
     end;
  ComboInsuranceCompany.ItemIndex := 0;

  Open_insurance_files;
  EditSearch.SetFocus;
end;

procedure TInsuranceFilesMainFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
  PanelJournal.JSInterface.JSConfig('stateId', ['PanelJournal_stateId_unique']);
  with PanelJournal
  do JSInterface.JSConfig('stateId', [Self.Name + Name]);
end;

procedure TInsuranceFilesMainFrm.UpdateStrings;
begin
  str_all_companies := LinkedLanguageInsuranceFilesMain.GetTextOrDefault('strstr_all_companies' (* 'Alle maatschappijen' *) );
  str_all_insurances := LinkedLanguageInsuranceFilesMain.GetTextOrDefault('strstr_all_insurances' (* 'Alle verzekeringen' *) );
  str_insurancefiles := LinkedLanguageInsuranceFilesMain.GetTextOrDefault('strstr_insurancefiles' (* 'Verzekeringspolissen' *) );
end;

procedure TInsuranceFilesMainFrm.BtnSearchClick(Sender: TObject);
begin
  Open_insurance_files;
end;

procedure TInsuranceFilesMainFrm.BtnViewFileClick(Sender: TObject);
begin
  if (InsuranceFiles.Active = False) or (InsuranceFiles.FieldByName('Id').isNull)
  then exit;

  With InsuranceFilesAddFrm
  do begin
       if Init_insurance_file_viewing(InsuranceFiles.FieldByName('Id').AsInteger)
       then ShowModal
       else Close;
  end;
end;

procedure TInsuranceFilesMainFrm.EditSearchKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13
  then begin
         Key := #0;
         BtnSearchClick(Sender);
       end;
end;

procedure TInsuranceFilesMainFrm.GridInsuranceFilesDrawColumnCell(
  Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
  Attribs: TUniCellAttribs);
begin
  if InsuranceFiles.FieldByName('Cancelled').AsInteger = 1
  then begin
         Attribs.Font.Color := UniMainModule.Color_red;
         Attribs.Font.Style := [fsStrikeOut];
       end;
end;

procedure TInsuranceFilesMainFrm.InsuranceFilesCOMMISSION_RECEIVEDGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-times-circle fa-lg" style=color:#B71C1C; ></i>'
  end;
end;

procedure TInsuranceFilesMainFrm.InsuranceFilesFIRST_MONTHLY_PREMIUM_PAYEDGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-times-circle fa-lg" style=color:#B71C1C; ></i>'
  end;
end;

procedure TInsuranceFilesMainFrm.InsuranceFilesREQUIRED_DOC_OKAYGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-times-circle fa-lg" style=color:#B71C1C; ></i>'
  end;
end;

procedure TInsuranceFilesMainFrm.InsuranceFilesSEPA_DOMICILIATION_ACTIVEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.AsInteger = 1
    then Text := '<i class="fas fa-check-circle fa-lg" style=color:#1B5E20; ></i>'
    else Text := '<i class="fas fa-times-circle fa-lg" style=color:#B71C1C; ></i>'
  end;
end;

procedure TInsuranceFilesMainFrm.LinkedLanguageInsuranceFilesMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInsuranceFilesMainFrm.Open_insurance_files;
var SQL: string;
begin
  InsuranceFiles.Close;
  InsuranceFiles.SQL.Clear;

  SQL := 'SELECT INSURANCE_FILES.id, INSURANCE_FILES.insurance_file_number, INSURANCE_FILES.internal_number, ' +
         'INSURANCE_FILES.total_amount_insurance, INSURANCE_FILES.start_date_insurance, ' +
         'INSURANCE_FILES.first_monthly_premium_payed, ' +
         'INSURANCE_FILES.sepa_domiciliation_active, ' +
         'INSURANCE_FILES.required_doc_okay, ' +
         'INSURANCE_FILES.Insurance_taker, ' +
         'INSURANCE_FILES.Contributor_id, '  +
         'INSURANCE_FILES.Insurance_file_number_final, ' +
         'INSURANCE_FILES.Cancelled, ' +
         'INSURANCE_FILES.premium_amount, ' +
         'INSURANCE_FILES.premium_payment, ' +
         'INSURANCE_FILES.structured_statement, ' +
         'INSURANCE_FILES.commission_received, ';

  case UniMainModule.LanguageManager.ActiveLanguage of
    1: SQL := SQL + ' BASICTABLES.DUTCH AS STATUS, ';
    2: SQL := SQL + ' BASICTABLES.FRENCH AS STATUS, ';
    3: SQL := SQL + ' BASICTABLES.ENGLISH AS STATUS, ';
  end;

  SQL := SQL + 'CASE ' +
               'WHEN INSURANCE_FILES.INSURANCE_TAKER_TYPE = 0 THEN coalesce(CRM_CONTACTS.last_name, '''') || '' '' || coalesce(CRM_CONTACTS.first_name, '''') ' +
               'WHEN INSURANCE_FILES.INSURANCE_TAKER_TYPE = 1 THEN CRM_ACCOUNTS.name ' +
               'END AS CONTACT_FULLNAME, ' +
               'CRM_CONTACTS.phone_mobile, ';


  SQL := SQL + 'COALESCE(CREDIT_CONTRIBUTORS.last_name, '''') || '' '' || COALESCE(CREDIT_CONTRIBUTORS.first_name, '''') AS CONTRIBUTOR_FULLNAME, ' +
               'CREDIT_CONTRIBUTORS.COMPANY_NAME, ' +
               'INSURANCE_INSTITUTIONS.NAME, ' +
               'COALESCE(agents.last_name, '''') || '' '' || COALESCE(agents.first_name, '''') AS AGENT_FULLNAME ' +
               'FROM INSURANCE_FILES ' +
               'LEFT OUTER JOIN BASICTABLES ON (insurance_files.status = BASICTABLES.ID) ' +
               'LEFT OUTER JOIN insurance_institutions ON (insurance_files.insurance_company = insurance_institutions.id) ' +
               'LEFT OUTER JOIN credit_contributors ON (insurance_files.contributor_id = credit_contributors.ID) ' +
               'LEFT OUTER JOIN crm_contacts on (insurance_files.insurance_taker = crm_contacts.id) ' +
               'LEFT OUTER JOIN crm_accounts on (insurance_files.insurance_taker = crm_accounts.id) ' +
               'LEFT OUTER JOIN credit_contributors agents ON (insurance_files.agent_id = agents.id)';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then begin
         SQL := SQL + 'WHERE INSURANCE_FILES.COMPANYID=' + IntToStr(UniMainModule.Company_Id) + ' ' +
                      'AND INSURANCE_FILES.REMOVED=''0'' ' +
                      'AND (INSURANCE_FILES.CONTRIBUTOR_ID=' + IntToStr(UniMainModule.Reference_contributor_id) + ' OR ' +
                      'INSURANCE_FILES.AGENT_ID=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';
       end
  else begin
         SQL := SQL + 'WHERE INSURANCE_FILES.COMPANYID=' + IntToStr(UniMainModule.Company_Id) + ' ' +
                      'AND INSURANCE_FILES.REMOVED=''0'' ' +
                      'AND (INSURANCE_FILES.CONTRIBUTOR_ID=' + IntToStr(UniMainModule.Responsable_contributor_credit) + ' AND ' +
                      'INSURANCE_FILES.AGENT_ID=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';

       end;

  case ComboboxPremie.ItemIndex of
    1: SQL := SQL + ' AND INSURANCE_FILES.first_monthly_premium_payed=1 ';
    2: SQL := SQL + ' AND INSURANCE_FILES.first_monthly_premium_payed=0 ';
  end;

  if ComboStatus.ItemIndex > 0
  then begin
         Status.First;
         Status.MoveBy(ComboStatus.ItemIndex-1);
         SQL := SQL + ' AND INSURANCE_FILES.STATUS=' + Status.fieldbyname('Id').asString;
       end;

  if ComboInsuranceCompany.ItemIndex > 0
  then begin
         InsuranceCompanies.First;
         InsuranceCompanies.MoveBy(ComboInsuranceCompany.ItemIndex-1);
         SQL := SQL + ' AND INSURANCE_FILES.INSURANCE_COMPANY=' + InsuranceCompanies.FieldByName('Id').AsString;
       end;

  if Trim(EditSearch.Text) <> ''
  then begin
         SQL :=
         SQL + ' and (UPPER(REPLACE(TRIM(insurance_files.internal_number), '' '', '''')) like '                                                                          +
                Utils.SearchValue(EditSearch.Text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(insurance_files.insurance_file_number), '' '', '''')) like '                                                                 +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(coalesce(CRM_CONTACTS.last_name, '''')) || TRIM(coalesce(CRM_CONTACTS.first_name, '''')), '' '', '''')) like '               +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_accounts.name), '' '', '''')) like '                                                                                     +
                Utils.SearchValue(EditSearch.Text)  + ' OR '                                                                                                    +
               'UPPER(REPLACE(TRIM(insurance_institutions.name), '' '', '''')) like '                                                                           +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(COALESCE(CREDIT_CONTRIBUTORS.last_name, '''')) || TRIM(COALESCE(CREDIT_CONTRIBUTORS.first_name, '''')), '' '', '''')) like ' +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(credit_contributors.contributor_number), '' '', '''')) like '                                                                +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(credit_contributors.company_name), '' '', '''')) like '                                                                      +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(COALESCE(agents.last_name, '''')) || TRIM(COALESCE(agents.first_name, '''')), '' '', '''')) like '                           +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_CONTACTS.ADDRESS_STREET), '' '', '''')) like '                                                                           +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_CONTACTS.ADDRESS_CITY), '' '', '''')) like '                                                                             +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_CONTACTS.ADDRESS_POSTAL_CODE), '' '', '''')) like '                                                                      +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_CONTACTS.BANKACCOUNT), '' '', '''')) like '                                                                              +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_CONTACTS.PHONE_HOME), '' '', '''')) like '                                                                               +
                Utils.SearchValue(EditSearch.text) + ' OR '                                                                                                     +
               'UPPER(REPLACE(TRIM(CRM_CONTACTS.EMAIL), '' '', '''')) like '                                                                                    +
                Utils.SearchValue(EditSearch.text) + ')';
       end;

  SQL := SQL + ' ORDER BY INSURANCE_FILES.START_DATE_INSURANCE DESC';
  InsuranceFiles.SQL.Add(SQL);
  InsuranceFiles.Open;
end;

end.


