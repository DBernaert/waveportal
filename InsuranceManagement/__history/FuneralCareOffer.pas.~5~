unit FuneralCareOffer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniMultiItem, uniComboBox, uniEdit,
  uniGUIBaseClasses, uniGroupBox, DBAccess, IBC, Data.DB, MemDS, uniPanel,
  uniButton, UniThemeButton, uniDBEdit, uniLabel, siComp, siLngLnk;

var
	str_additional_email_address_not_correct: string = 'Het bijkomend e-mailadres is niet correct'; // TSI: Localized (Don't modify!)
	str_name_is_required: string = 'Naam is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_firstname_is_required: string = 'Voornaam is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_company_name_is_required: string = 'Bedrijfsnaam is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_email_address_required: string = 'Emailadres is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_email_address_not_valid: string = 'Emailadres is niet correct'; // TSI: Localized (Don't modify!)
	str_name_insured_person_required: string = 'Naam verzekerde is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_firstname_insured_person_required: string = 'Voornaam verzekerde is een verplichte ingave'; // TSI: Localized (Don't modify!)
	str_error_saving_data: string = 'Fout bij het opslaan van de gegevens'; // TSI: Localized (Don't modify!)

type
  TFuneralCareOfferFrm = class(TUniForm)
    Insurance_quotes_params: TIBCQuery;
    UpdTr_Insurance_quotes_params: TIBCTransaction;
    PnlOptions: TUniPanel;
    PnlInsuranceTaker: TUniPanel;
    PnlInsuredPerson: TUniPanel;
    BtnGenerateOffer: TUniThemeButton;
    ComboboxPremiumPayment: TUniComboBox;
    ComboboxLanguage: TUniComboBox;
    EditAdditionalEmailAddress: TUniDBEdit;
    DsInsurance_quotes_params: TDataSource;
    BtnCancel: TUniThemeButton;
    EditDemanderName: TUniDBEdit;
    EditDemanderFirstName: TUniDBEdit;
    EditDemanderCompanyName: TUniDBEdit;
    EditDemanderEmail: TUniDBEdit;
    EditInsuredName: TUniDBEdit;
    EditInsuredFirstName: TUniDBEdit;
    LblAction: TUniLabel;
    LinkedLanguageFuneralCareOffer: TsiLangLinked;
    procedure BtnGenerateOfferClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure PnlInsuredPersonToolClick(Sender: TUniCustomButtonItem);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageFuneralCareOfferChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_funeral_care_offer(QuoteId: longint);
  end;

function FuneralCareOfferFrm: TFuneralCareOfferFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, InsuranceQuotePreview, Main, Utils;

function FuneralCareOfferFrm: TFuneralCareOfferFrm;
begin
  Result := TFuneralCareOfferFrm(UniMainModule.GetFormInstance(TFuneralCareOfferFrm));
end;

{ TFuneralCareOfferFrm }

procedure TFuneralCareOfferFrm.Start_funeral_care_offer(QuoteId: longint);
begin
  ComboboxLanguage.ItemIndex :=
  UniMainModule.LanguageManager.ActiveLanguage - 1;

  Insurance_quotes_params.Close;

  Insurance_quotes_params.SQL.Text :=
  'Select * from insurance_quotes where id=:id';

  Insurance_quotes_params.ParamByName('Id').AsInteger :=
  QuoteId;

  Insurance_quotes_params.Open;

  Insurance_quotes_params.Edit;
end;

procedure TFuneralCareOfferFrm.UpdateStrings;
begin
  str_error_saving_data := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_error_saving_data' (* 'Fout bij het opslaan van de gegevens' *) );
  str_firstname_insured_person_required := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_firstname_insured_person_required' (* 'Voornaam verzekerde is een verplichte ingave' *) );
  str_name_insured_person_required := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_name_insured_person_required' (* 'Naam verzekerde is een verplichte ingave' *) );
  str_email_address_not_valid := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_email_address_not_valid' (* 'Emailadres is niet correct' *) );
  str_email_address_required := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_email_address_required' (* 'Emailadres is een verplichte ingave' *) );
  str_company_name_is_required := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_company_name_is_required' (* 'Bedrijfsnaam is een verplichte ingave' *) );
  str_firstname_is_required := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_firstname_is_required' (* 'Voornaam is een verplichte ingave' *) );
  str_name_is_required := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_name_is_required' (* 'Naam is een verplichte ingave' *) );
  str_additional_email_address_not_correct := LinkedLanguageFuneralCareOffer.GetTextOrDefault('strstr_additional_email_address_not_correct' (* 'Het bijkomend e-mailadres is niet correct' *) );
end;

procedure TFuneralCareOfferFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFuneralCareOfferFrm.BtnGenerateOfferClick(Sender: TObject);
begin
  BtnGenerateOffer.SetFocus;

  if Trim(Insurance_quotes_params.FieldByName('Additional_email').AsString) <> ''
  then begin
         if Utils.IsValidEmailRegEx(Insurance_quotes_params.FieldByName('Additional_email').AsString) = False
         then begin
                MainForm.WaveShowWarningToast(str_additional_email_address_not_correct);
                EditAdditionalEmailAddress.SetFocus;
                Exit;
              end;
       end;

  if Insurance_quotes_params.FieldByName('Demander_type').AsInteger = 0
  then begin
         if Trim(Insurance_quotes_params.FieldByName('Demander_name').AsString) = ''
         then begin
                MainForm.WaveShowWarningToast(str_name_is_required);
                EditDemanderName.SetFocus;
                Exit;
              end;

         if Trim(Insurance_quotes_params.FieldByName('Demander_firstname').asString) = ''
         then begin
                MainForm.WaveShowWarningToast(str_firstname_is_required);
                EditDemanderFirstName.SetFocus;
                Exit;
              end;
       end
  else if Insurance_quotes_params.FieldByName('Demander_type').AsInteger = 1
       then begin
              if Trim(Insurance_quotes_params.FieldByName('Demander_company_name').AsString) = ''
              then begin
                     MainForm.WaveShowWarningToast(str_company_name_is_required);
                     EditDemanderCompanyName.SetFocus;
                     Exit;
                   end;
            end;

  if Trim(Insurance_quotes_params.FieldByName('Demander_email').AsString) = ''
  then begin
         MainForm.WaveShowWarningToast(str_email_address_required);
         EditDemanderEmail.SetFocus;
         Exit;
       end;

  if Utils.IsValidEmailRegEx(Insurance_quotes_params.FieldByName('Demander_email').AsString) = False
  then begin
         EditDemanderEmail.SetFocus;
         MainForm.WaveShowWarningToast(str_email_address_not_valid);
         Exit;
       end;

  if Trim(Insurance_quotes_params.FieldByName('Insured_name').AsString) = ''
  then begin
         EditInsuredName.SetFocus;
         MainForm.WaveShowWarningToast(str_name_insured_person_required);
         Exit;
       end;

  if Trim(Insurance_quotes_params.FieldByName('Insured_firstname').AsString) = ''
  then begin
         EditInsuredFirstName.SetFocus;
         MainForm.WaveShowWarningToast(str_firstname_insured_person_required);
         Exit;
       end;

  Insurance_quotes_params.FieldByName('Language').AsInteger :=
  ComboboxLanguage.ItemIndex;

  Insurance_quotes_params.FieldByName('Premium_payment').AsInteger :=
  ComboboxPremiumPayment.ItemIndex;

  Try
    Insurance_quotes_params.Post;
    if UpdTr_Insurance_quotes_params.Active
    then UpdTr_Insurance_quotes_params.Commit;
    Close;
  Except
    if UpdTr_Insurance_quotes_params.Active
    then UpdTr_Insurance_quotes_params.Rollback;

    MainForm.WaveShowErrorToast(str_error_saving_data);
    Exit;
  End;

  LblAction.Visible := true;
  UniSession.Synchronize();

  with InsuranceQuotePreviewFrm
  do begin
       Generate_quote(Insurance_quotes_params.FieldByName('Id').AsInteger);
       ShowModal;
     end;
end;

procedure TFuneralCareOfferFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TFuneralCareOfferFrm.LinkedLanguageFuneralCareOfferChangeLanguage(
  Sender: TObject);
begin

  UpdateStrings;
end;

procedure TFuneralCareOfferFrm.PnlInsuredPersonToolClick(
  Sender: TUniCustomButtonItem);
begin
  Insurance_quotes_params.FieldByName('Insured_name').AsString :=
  Insurance_quotes_params.FieldByName('Demander_name').AsString;

  Insurance_quotes_params.FieldByName('Insured_firstname').AsString :=
  Insurance_quotes_params.FieldByName('Demander_firstname').AsString;
end;

end.

