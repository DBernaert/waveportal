object InsuranceQuotesMainFrm: TInsuranceQuotesMainFrm
  Left = 0
  Top = 0
  Width = 1274
  Height = 860
  OnCreate = FormCreate
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1274
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 75
      Height = 30
      Hint = ''
      Caption = 'Offertes'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
    end
  end
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1274
    Height = 30
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerFilterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 401
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 265
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        FieldLabel = '<i class="fas fa-search fa-lg " ></i>'
        FieldLabelWidth = 25
        FieldLabelSeparator = ' '
        SelectOnFocus = True
        OnKeyPress = EditSearchKeyPress
      end
      object BtnSearch: TUniThemeButton
        Left = 272
        Top = 0
        Width = 129
        Height = 28
        Hint = ''
        Caption = 'Zoeken'
        ParentFont = False
        TabStop = False
        TabOrder = 2
        OnClick = BtnSearchClick
        ButtonTheme = uctPrimary
      end
    end
    object ContainerFilterRight: TUniContainerPanel
      Left = 1214
      Top = 0
      Width = 60
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object BtnPrint: TUniThemeButton
        Left = 0
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Afdrukken voorstel'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 51
        OnClick = BtnPrintClick
        ButtonTheme = uctPrimary
      end
      object BtnExportList: TUniThemeButton
        Left = 32
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Exporteren lijst'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnExportListClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object GridQuotes: TUniDBGrid
    Left = 0
    Top = 63
    Width = 1274
    Height = 569
    Hint = ''
    DataSource = DsQuotes
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.PageSize = 45
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Cls = 'customGrid'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 0 0 0'
    Align = alClient
    TabOrder = 2
    Columns = <
      item
        FieldName = 'ID'
        Title.Alignment = taRightJustify
        Title.Caption = 'Nr.'
        Width = 50
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'QUOTE_CREATION_TIME'
        Title.Caption = 'Tijdstip creatie'
        Width = 145
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'DEMANDER_FULLNAME'
        Title.Caption = 'Naam aanvrager'
        Width = 604
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'DEMANDER_COMPANY_NAME'
        Title.Caption = 'Bedrijfsnaam aanvrager'
        Width = 604
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'INSURED_NAME'
        Title.Caption = 'Naam verzekerde'
        Width = 604
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'INSURED_FIRSTNAME'
        Title.Caption = 'Voornaam verzekerde'
        Width = 604
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'START_DATE_INSURANCE'
        Title.Caption = 'Ingangsdatum'
        Width = 105
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'CAPITAL_1'
        Title.Alignment = taRightJustify
        Title.Caption = 'Kapitaal 1'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'CAPITAL_2'
        Title.Alignment = taRightJustify
        Title.Caption = 'Kapitaal 2'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'CAPITAL_3'
        Title.Alignment = taRightJustify
        Title.Caption = 'Kapitaal 3'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object PnlDetails: TUniPanel
    Left = 0
    Top = 632
    Width = 1274
    Height = 228
    Hint = ''
    BodyRTL = True
    Align = alBottom
    TabOrder = 3
    TitleVisible = True
    Title = 'Details'
    Caption = ''
    Images = UniMainModule.ImageList
    ImageIndex = 66
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '8 0 0 0'
    object LblTitleName: TUniLabel
      Left = 16
      Top = 24
      Width = 32
      Height = 13
      Hint = ''
      Caption = 'Naam:'
      TabOrder = 1
    end
    object LblTitleCompanyName: TUniLabel
      Left = 16
      Top = 40
      Width = 69
      Height = 13
      Hint = ''
      Caption = 'Bedrijfsnaam:'
      TabOrder = 2
    end
    object LblTitleLanguage: TUniLabel
      Left = 16
      Top = 56
      Width = 23
      Height = 13
      Hint = ''
      Caption = 'Taal:'
      TabOrder = 3
    end
    object LblTitleEmail: TUniLabel
      Left = 16
      Top = 72
      Width = 30
      Height = 13
      Hint = ''
      Caption = 'Email:'
      TabOrder = 4
    end
    object LblValueLastName: TUniDBText
      Left = 136
      Top = 24
      Width = 92
      Height = 13
      Hint = ''
      DataField = 'DEMANDER_FULLNAME'
      DataSource = DsQuotes
    end
    object LblValueCompanyName: TUniDBText
      Left = 136
      Top = 40
      Width = 120
      Height = 13
      Hint = ''
      DataField = 'DEMANDER_COMPANY_NAME'
      DataSource = DsQuotes
    end
    object LblValueLanguage: TUniDBText
      Left = 136
      Top = 56
      Width = 94
      Height = 13
      Hint = ''
      DataField = 'LanguageDescription'
      DataSource = DsQuotes
    end
    object LblValueEmail: TUniDBText
      Left = 136
      Top = 72
      Width = 70
      Height = 13
      Hint = ''
      DataField = 'DEMANDER_EMAIL'
      DataSource = DsQuotes
    end
    object LblTitleDateOfBirth: TUniLabel
      Left = 368
      Top = 24
      Width = 85
      Height = 13
      Hint = ''
      Caption = 'Geboortedatum:'
      TabOrder = 9
    end
    object LblTitleStartDateInsurance: TUniLabel
      Left = 368
      Top = 40
      Width = 78
      Height = 13
      Hint = ''
      Caption = 'Ingangsdatum:'
      TabOrder = 10
    end
    object LblTitleFileCosts: TUniLabel
      Left = 368
      Top = 56
      Width = 76
      Height = 13
      Hint = ''
      Caption = 'Dossierkosten:'
      TabOrder = 11
    end
    object LblTitleDemanderType: TUniLabel
      Left = 368
      Top = 72
      Width = 26
      Height = 13
      Hint = ''
      Caption = 'Type:'
      TabOrder = 12
    end
    object LblValueDateOfBirth: TUniDBText
      Left = 512
      Top = 24
      Width = 104
      Height = 13
      Hint = ''
      DataField = 'DEMANDER_DATE_OF_BIRTH'
      DataSource = DsQuotes
    end
    object LblValueStartDateInsurance: TUniDBText
      Left = 512
      Top = 40
      Width = 141
      Height = 13
      Hint = ''
      DataField = 'START_DATE_INSURANCE'
      DataSource = DsQuotes
    end
    object LblValueFileCosts: TUniDBText
      Left = 512
      Top = 56
      Width = 89
      Height = 13
      Hint = ''
      DataField = 'FILE_COSTS'
      DataSource = DsQuotes
    end
    object LblValueDemanderType: TUniDBText
      Left = 512
      Top = 72
      Width = 119
      Height = 13
      Hint = ''
      DataField = 'DemanderType'
      DataSource = DsQuotes
    end
    object LblTitleTimeCreation: TUniLabel
      Left = 760
      Top = 24
      Width = 78
      Height = 13
      Hint = ''
      Caption = 'Tijdstip creatie:'
      TabOrder = 17
    end
    object LblTitleTimeEmail: TUniLabel
      Left = 760
      Top = 40
      Width = 75
      Height = 13
      Hint = ''
      Caption = 'Tijdstip e-mail:'
      TabOrder = 18
    end
    object LblTitleSendByMail: TUniLabel
      Left = 760
      Top = 56
      Width = 94
      Height = 13
      Hint = ''
      Caption = 'Verstuurd via mail:'
      TabOrder = 19
    end
    object LblValueTimeCreation: TUniDBText
      Left = 888
      Top = 24
      Width = 111
      Height = 13
      Hint = ''
      DataField = 'QUOTE_CREATION_TIME'
      DataSource = DsQuotes
    end
    object LblValueTimeEmail: TUniDBText
      Left = 888
      Top = 40
      Width = 94
      Height = 13
      Hint = ''
      DataField = 'QUOTE_MAIL_TIME'
      DataSource = DsQuotes
    end
    object LblValueSendByMail: TUniDBText
      Left = 888
      Top = 56
      Width = 102
      Height = 13
      Hint = ''
      DataField = 'SendByMail'
      DataSource = DsQuotes
    end
    object LblTitlePropositions: TUniLabel
      Left = 16
      Top = 104
      Width = 61
      Height = 13
      Hint = ''
      Caption = 'Voorstellen:'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 23
    end
    object LblTitleCapital: TUniLabel
      Left = 104
      Top = 104
      Width = 121
      Height = 13
      Hint = ''
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Kapitaal'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 24
    end
    object LblValueCapital1: TUniDBText
      Left = 104
      Top = 128
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'CAPITAL_1'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueCapital2: TUniDBText
      Left = 104
      Top = 144
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'CAPITAL_2'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueCapital3: TUniDBText
      Left = 104
      Top = 160
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'CAPITAL_3'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblTitlePurchasePrice: TUniLabel
      Left = 272
      Top = 104
      Width = 121
      Height = 13
      Hint = ''
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Koopsom'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 28
    end
    object LblValuePurchasePrice1: TUniDBText
      Left = 272
      Top = 128
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'PURCHASE_PRICE_1'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValuePurchasePrice2: TUniDBText
      Left = 272
      Top = 144
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'PURCHASE_PRICE_2'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValuePurchasePrice3: TUniDBText
      Left = 272
      Top = 160
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'PURCHASE_PRICE_3'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblTitleGrossMonthPremium: TUniLabel
      Left = 440
      Top = 104
      Width = 129
      Height = 13
      Hint = ''
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Bruto maandpremie'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 32
    end
    object LblValueGrossMonthPremium1: TUniDBText
      Left = 472
      Top = 128
      Width = 97
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_MONTHLY_1'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueGrossMonthPremium2: TUniDBText
      Left = 472
      Top = 144
      Width = 97
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_MONTHLY_2'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueGrossMonthPremium3: TUniDBText
      Left = 472
      Top = 160
      Width = 97
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_MONTHLY_3'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblTitleGrossSemesterPremium: TUniLabel
      Left = 608
      Top = 104
      Width = 137
      Height = 13
      Hint = ''
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Bruto semestri'#235'le premie'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 36
    end
    object LblValueGrossSemesterPremium1: TUniDBText
      Left = 608
      Top = 128
      Width = 137
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_QUARTERLY_1'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueGrossSemesterPremium2: TUniDBText
      Left = 608
      Top = 144
      Width = 137
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_QUARTERLY_2'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueGrossSemesterPremium3: TUniDBText
      Left = 608
      Top = 160
      Width = 137
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_QUARTERLY_3'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblTitleGrossYearPremium: TUniLabel
      Left = 768
      Top = 104
      Width = 121
      Height = 13
      Hint = ''
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Bruto jaarpremie'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 40
    end
    object LblValueGrossYearPremium1: TUniDBText
      Left = 768
      Top = 128
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_YEARLY_1'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueGrossYearPremium2: TUniDBText
      Left = 768
      Top = 144
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_YEARLY_2'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
    object LblValueGrossYearPremium3: TUniDBText
      Left = 768
      Top = 160
      Width = 121
      Height = 13
      Hint = ''
      DataField = 'GROSS_PREMIUM_YEARLY_3'
      DataSource = DsQuotes
      Alignment = taRightJustify
      AutoSize = False
    end
  end
  object TblQuotes: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select '
      '    insurance_quotes.id,'
      '    insurance_quotes.companyid,'
      '    insurance_quotes.quote_creation_time,'
      '    insurance_quotes.quote_mail_time,'
      '    insurance_quotes.flag_mailed,'
      '    insurance_quotes.contributor_id,'
      '    insurance_quotes.demander_name,'
      '    insurance_quotes.demander_firstname,'
      
        '    coalesce(insurance_quotes.demander_name, '#39#39') || '#39' '#39' || coale' +
        'sce(insurance_quotes.demander_firstname, '#39#39') demander_fullname,'
      '    insurance_quotes.demander_company_name,'
      '    insurance_quotes.demander_date_of_birth,'
      '    insurance_quotes.demander_type,'
      '    insurance_quotes.demander_email,'
      '    insurance_quotes.file_costs,'
      '    insurance_quotes.start_date_insurance,'
      '    insurance_quotes.capital_1,'
      '    insurance_quotes.purchase_price_1,'
      '    insurance_quotes.gross_premium_monthly_1,'
      '    insurance_quotes.gross_premium_quarterly_1,'
      '    insurance_quotes.gross_premium_yearly_1,'
      '    insurance_quotes.capital_2,'
      '    insurance_quotes.purchase_price_2,'
      '    insurance_quotes.gross_premium_monthly_2,'
      '    insurance_quotes.gross_premium_quarterly_2,'
      '    insurance_quotes.gross_premium_yearly_2,'
      '    insurance_quotes.capital_3,'
      '    insurance_quotes.purchase_price_3,'
      '    insurance_quotes.gross_premium_monthly_3,'
      '    insurance_quotes.gross_premium_quarterly_3,'
      '    insurance_quotes.gross_premium_yearly_3,'
      '    insurance_quotes.language,'
      '    insurance_quotes.insured_name,'
      '    insurance_quotes.insured_firstname,'
      
        '    coalesce(credit_contributors.last_name, '#39#39') || '#39' '#39' || coales' +
        'ce(credit_contributors.first_name, '#39#39') contributor_fullname,'
      '    credit_contributors.company_name'
      'from credit_contributors'
      
        '   right outer join insurance_quotes on (credit_contributors.id ' +
        '= insurance_quotes.contributor_id)'
      'order by insurance_quotes.quote_creation_time desc')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = TblQuotesCalcFields
    Left = 560
    Top = 168
    object TblQuotesID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object TblQuotesCOMPANYID: TLargeintField
      FieldName = 'COMPANYID'
      Required = True
    end
    object TblQuotesQUOTE_CREATION_TIME: TDateTimeField
      FieldName = 'QUOTE_CREATION_TIME'
      Required = True
    end
    object TblQuotesQUOTE_MAIL_TIME: TDateTimeField
      FieldName = 'QUOTE_MAIL_TIME'
    end
    object TblQuotesFLAG_MAILED: TSmallintField
      FieldName = 'FLAG_MAILED'
      Required = True
    end
    object TblQuotesCONTRIBUTOR_ID: TLargeintField
      FieldName = 'CONTRIBUTOR_ID'
      Required = True
    end
    object TblQuotesDEMANDER_COMPANY_NAME: TWideStringField
      FieldName = 'DEMANDER_COMPANY_NAME'
      Size = 100
    end
    object TblQuotesDEMANDER_DATE_OF_BIRTH: TDateField
      FieldName = 'DEMANDER_DATE_OF_BIRTH'
    end
    object TblQuotesDEMANDER_TYPE: TSmallintField
      FieldName = 'DEMANDER_TYPE'
    end
    object TblQuotesDEMANDER_EMAIL: TWideStringField
      FieldName = 'DEMANDER_EMAIL'
      Size = 100
    end
    object TblQuotesFILE_COSTS: TSmallintField
      FieldName = 'FILE_COSTS'
    end
    object TblQuotesSTART_DATE_INSURANCE: TDateField
      FieldName = 'START_DATE_INSURANCE'
    end
    object TblQuotesCAPITAL_1: TFloatField
      FieldName = 'CAPITAL_1'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesPURCHASE_PRICE_1: TFloatField
      FieldName = 'PURCHASE_PRICE_1'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_MONTHLY_1: TFloatField
      FieldName = 'GROSS_PREMIUM_MONTHLY_1'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_QUARTERLY_1: TFloatField
      FieldName = 'GROSS_PREMIUM_QUARTERLY_1'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_YEARLY_1: TFloatField
      FieldName = 'GROSS_PREMIUM_YEARLY_1'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesCAPITAL_2: TFloatField
      FieldName = 'CAPITAL_2'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesPURCHASE_PRICE_2: TFloatField
      FieldName = 'PURCHASE_PRICE_2'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_MONTHLY_2: TFloatField
      FieldName = 'GROSS_PREMIUM_MONTHLY_2'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_QUARTERLY_2: TFloatField
      FieldName = 'GROSS_PREMIUM_QUARTERLY_2'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_YEARLY_2: TFloatField
      FieldName = 'GROSS_PREMIUM_YEARLY_2'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesCAPITAL_3: TFloatField
      FieldName = 'CAPITAL_3'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesPURCHASE_PRICE_3: TFloatField
      FieldName = 'PURCHASE_PRICE_3'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_MONTHLY_3: TFloatField
      FieldName = 'GROSS_PREMIUM_MONTHLY_3'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_QUARTERLY_3: TFloatField
      FieldName = 'GROSS_PREMIUM_QUARTERLY_3'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesGROSS_PREMIUM_YEARLY_3: TFloatField
      FieldName = 'GROSS_PREMIUM_YEARLY_3'
      DisplayFormat = '#,##0.00'
    end
    object TblQuotesDemanderType: TStringField
      FieldKind = fkCalculated
      FieldName = 'DemanderType'
      Calculated = True
    end
    object TblQuotesSendByMail: TStringField
      FieldKind = fkCalculated
      FieldName = 'SendByMail'
      Size = 10
      Calculated = True
    end
    object TblQuotesDEMANDER_FULLNAME: TWideStringField
      FieldName = 'DEMANDER_FULLNAME'
      ReadOnly = True
      Size = 201
    end
    object TblQuotesLANGUAGE: TSmallintField
      FieldName = 'LANGUAGE'
    end
    object TblQuotesLanguageDescription: TStringField
      FieldKind = fkCalculated
      FieldName = 'LanguageDescription'
      Calculated = True
    end
    object TblQuotesCONTRIBUTOR_FULLNAME: TWideStringField
      FieldName = 'CONTRIBUTOR_FULLNAME'
      ReadOnly = True
      Size = 61
    end
    object TblQuotesCOMPANY_NAME: TWideStringField
      FieldName = 'COMPANY_NAME'
      ReadOnly = True
      Size = 50
    end
    object TblQuotesDEMANDER_NAME: TWideStringField
      FieldName = 'DEMANDER_NAME'
      Size = 100
    end
    object TblQuotesDEMANDER_FIRSTNAME: TWideStringField
      FieldName = 'DEMANDER_FIRSTNAME'
      Size = 100
    end
    object TblQuotesINSURED_NAME: TWideStringField
      FieldName = 'INSURED_NAME'
      Size = 100
    end
    object TblQuotesINSURED_FIRSTNAME: TWideStringField
      FieldName = 'INSURED_FIRSTNAME'
      Size = 100
    end
  end
  object DsQuotes: TDataSource
    DataSet = TblQuotes
    Left = 560
    Top = 224
  end
  object LinkedLanguageInsuranceQuotesMain: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST'
      'TWIDESTRINGS')
    SmartExcludeProps.Strings = (
      'TblQuotesID.DisplayLabel'
      'TblQuotesCOMPANYID.DisplayLabel'
      'TblQuotesQUOTE_CREATION_TIME.DisplayLabel'
      'TblQuotesQUOTE_MAIL_TIME.DisplayLabel'
      'TblQuotesFLAG_MAILED.DisplayLabel'
      'TblQuotesCONTRIBUTOR_ID.DisplayLabel'
      'TblQuotesDEMANDER_COMPANY_NAME.DisplayLabel'
      'TblQuotesDEMANDER_DATE_OF_BIRTH.DisplayLabel'
      'TblQuotesDEMANDER_TYPE.DisplayLabel'
      'TblQuotesDEMANDER_EMAIL.DisplayLabel'
      'TblQuotesFILE_COSTS.DisplayLabel'
      'TblQuotesSTART_DATE_INSURANCE.DisplayLabel'
      'TblQuotesCAPITAL_1.DisplayLabel'
      'TblQuotesPURCHASE_PRICE_1.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_MONTHLY_1.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_QUARTERLY_1.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_YEARLY_1.DisplayLabel'
      'TblQuotesCAPITAL_2.DisplayLabel'
      'TblQuotesPURCHASE_PRICE_2.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_MONTHLY_2.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_QUARTERLY_2.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_YEARLY_2.DisplayLabel'
      'TblQuotesCAPITAL_3.DisplayLabel'
      'TblQuotesPURCHASE_PRICE_3.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_MONTHLY_3.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_QUARTERLY_3.DisplayLabel'
      'TblQuotesGROSS_PREMIUM_YEARLY_3.DisplayLabel'
      'TblQuotesDemanderType.DisplayLabel'
      'TblQuotesSendByMail.DisplayLabel'
      'TblQuotesDEMANDER_FULLNAME.DisplayLabel'
      'TblQuotesLANGUAGE.DisplayLabel'
      'TblQuotesLanguageDescription.DisplayLabel'
      'TblQuotesCONTRIBUTOR_FULLNAME.DisplayLabel'
      'TblQuotesCOMPANY_NAME.DisplayLabel'
      'TInsuranceQuotesMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerFilter.Layout'
      'ContainerFilterLeft.Layout'
      'EditSearch.FieldLabel'
      'EditSearch.FieldLabelSeparator'
      'ContainerFilterRight.Layout'
      'PnlDetails.Layout'
      'TblQuotesDEMANDER_NAME.DisplayLabel'
      'TblQuotesDEMANDER_FIRSTNAME.DisplayLabel'
      'TblQuotesINSURED_NAME.DisplayLabel'
      'TblQuotesINSURED_FIRSTNAME.DisplayLabel')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageInsuranceQuotesMainChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 560
    Top = 288
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C00650001004F0066006600
      6500720074006500730001004F00660066007200650073000100510075006F00
      74006500730001000D000A00420074006E005300650061007200630068000100
      5A006F0065006B0065006E000100430068006500720063006800650072000100
      53006500610072006300680001000D000A004C0062006C005400690074006C00
      65004E0061006D00650001004E00610061006D003A0001004E006F006D003A00
      01004E0061006D0065003A0001000D000A004C0062006C005400690074006C00
      650043006F006D00700061006E0079004E0061006D0065000100420065006400
      720069006A00660073006E00610061006D003A0001004E006F006D0020006400
      650020006C00270065006E00740072006500700072006900730065003A000100
      43006F006D00700061006E00790020004E0061006D0065003A0001000D000A00
      4C0062006C005400690074006C0065004C0061006E0067007500610067006500
      01005400610061006C003A0001004C0061006E006700750065003A0001004C00
      61006E00670075006100670065003A0001000D000A004C0062006C0054006900
      74006C00650045006D00610069006C00010045006D00610069006C003A000100
      45002D006D00610069006C003A00010045002D006D00610069006C003A000100
      0D000A004C0062006C005400690074006C00650044006100740065004F006600
      4200690072007400680001004700650062006F006F0072007400650064006100
      740075006D003A000100440061007400650020006400650020006E0061006900
      7300730061006E00630065003A000100440061007400650020006F0066002000
      620069007200740068003A0001000D000A004C0062006C005400690074006C00
      650053007400610072007400440061007400650049006E007300750072006100
      6E0063006500010049006E00670061006E006700730064006100740075006D00
      3A000100440061007400650020006400650020006400E9006200750074003A00
      01005300740061007200740069006E006700200064006100740065003A000100
      0D000A004C0062006C005400690074006C006500460069006C00650043006F00
      730074007300010044006F00730073006900650072006B006F00730074006500
      6E003A00010046007200610069007300200064006500200064006F0073007300
      69006500720020003A000100460069006C006500200063006F00730074007300
      3A0001000D000A004C0062006C005400690074006C006500440065006D006100
      6E006400650072005400790070006500010054007900700065003A0001005400
      7900700065003A00010054007900700065003A0001000D000A004C0062006C00
      5400690074006C006500540069006D0065004300720065006100740069006F00
      6E000100540069006A0064007300740069007000200063007200650061007400
      690065003A00010048006500750072006500200064006500200063007200E900
      6100740069006F006E00A0003A000100540069006D00650020006F0066002000
      6300720065006100740069006F006E003A0001000D000A004C0062006C005400
      690074006C006500540069006D00650045006D00610069006C00010054006900
      6A0064007300740069007000200065002D006D00610069006C003A0001004800
      650075007200650020006400650020006C00270065002D006D00610069006C00
      A0003A000100540069006D00650020006F006600200065006D00610069006C00
      3A0001000D000A004C0062006C005400690074006C006500530065006E006400
      420079004D00610069006C000100560065007200730074007500750072006400
      200076006900610020006D00610069006C003A00010045006E0076006F007900
      E9002000700061007200200065002D006D00610069006C00A0003A0001005300
      65006E007400200062007900200065006D00610069006C003A0001000D000A00
      4C0062006C005400690074006C006500500072006F0070006F00730069007400
      69006F006E007300010056006F006F0072007300740065006C006C0065006E00
      3A000100500072006F0070006F0073006900740069006F006E0073003A000100
      500072006F0070006F00730061006C0073003A0001000D000A004C0062006C00
      5400690074006C0065004300610070006900740061006C0001004B0061007000
      69007400610061006C0001004300610070006900740061006C00010043006100
      70006900740061006C0001000D000A004C0062006C005400690074006C006500
      500075007200630068006100730065005000720069006300650001004B006F00
      6F00700073006F006D000100500072006900780020000B200B20640027006100
      6300680061007400010050007500720063006800610073006500200070007200
      69006300650001000D000A004C0062006C005400690074006C00650047007200
      6F00730073004D006F006E00740068005000720065006D00690075006D000100
      42007200750074006F0020006D00610061006E0064007000720065006D006900
      650001005000720069006D00650020006D0065006E007300750065006C006C00
      65002000620072007500740065000100470072006F007300730020006D006F00
      6E00740068006C00790020007000720065006D00690075006D0001000D000A00
      4C0062006C005400690074006C006500470072006F0073007300530065006D00
      650073007400650072005000720065006D00690075006D000100420072007500
      74006F002000730065006D0065007300740072006900EB006C00650020007000
      720065006D006900650001005000720069006D0065002000730065006D006500
      730074007200690065006C006C00650020006200720075007400650001004700
      72006F00730073002000730065006D0069002D0061006E006E00750061006C00
      20007000720065006D00690075006D0001000D000A004C0062006C0054006900
      74006C006500470072006F007300730059006500610072005000720065006D00
      690075006D00010042007200750074006F0020006A0061006100720070007200
      65006D006900650001005000720069006D006500200061006E006E0075006500
      6C006C0065002000620072007500740065000100470072006F00730073002000
      61006E006E00750061006C0020007000720065006D00690075006D0001000D00
      0A0073007400480069006E00740073005F0055006E00690063006F0064006500
      0D000A00420074006E005000720069006E007400010041006600640072007500
      6B006B0065006E00200076006F006F0072007300740065006C00010049006D00
      7000720069006D006500720020006F0066006600720065000100500072006900
      6E0074002000700072006F0070006F00730061006C0001000D000A0042007400
      6E004500780070006F00720074004C0069007300740001004500780070006F00
      720074006500720065006E0020006C0069006A00730074000100450078007000
      6F00720074006500720020006C00610020006C00690073007400650001004500
      780070006F007200740020006C0069007300740001000D000A00730074004400
      6900730070006C00610079004C006100620065006C0073005F0055006E006900
      63006F00640065000D000A007300740046006F006E00740073005F0055006E00
      690063006F00640065000D000A00730074004D0075006C00740069004C006900
      6E00650073005F0055006E00690063006F00640065000D000A00730074005300
      7400720069006E00670073005F0055006E00690063006F00640065000D000A00
      7300740072007300740072005F006E00610074007500720061006C005F007000
      6500720073006F006E0001004E00610074007500750072006C0069006A006B00
      200070006500720073006F006F006E00010050006500720073006F006E006E00
      650020007000680079007300690071007500650001004E006100740075007200
      61006C00200070006500720073006F006E0001000D000A007300740072007300
      740072005F0063006F006D00700061006E007900010042006500640072006900
      6A006600010053006F0063006900E9007400E900010043006F006D0070006100
      6E00790001000D000A007300740072007300740072005F006E006F0001004E00
      6500650001004E006F006E0001004E006F0001000D000A007300740072007300
      740072005F0079006500730001004A00610001004F0075006900010059006500
      730001000D000A007300740072007300740072005F0064007500740063006800
      01004E0065006400650072006C0061006E006400730001004E00E90065007200
      6C0061006E00640061006900730001004400750074006300680001000D000A00
      7300740072007300740072005F006600720065006E0063006800010046007200
      61006E00730001004600720061006E00E7006100690073000100460072006500
      6E006300680001000D000A007300740072007300740072005F00710075006F00
      74006500730001004F00660066006500720074006500730001004F0066006600
      7200650073000100510075006F0074006500730001000D000A00730074004F00
      740068006500720053007400720069006E00670073005F0055006E0069006300
      6F00640065000D000A0050006E006C00440065007400610069006C0073002E00
      5400690074006C0065000100440065007400610069006C00730001004400E900
      7400610069006C0073000100440065007400610069006C00730001000D000A00
      7300740043006F006C006C0065006300740069006F006E0073005F0055006E00
      690063006F00640065000D000A004700720069006400510075006F0074006500
      73002E0043006F006C0075006D006E0073005B0030005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640051007500
      6F007400650073002E0043006F006C0075006D006E0073005B0030005D002E00
      5400690074006C0065002E00430061007000740069006F006E0001004E007200
      2E0001004E006F006E002E0001004E006F002E0001000D000A00470072006900
      6400510075006F007400650073002E0043006F006C0075006D006E0073005B00
      31005D002E0043006800650063006B0042006F0078004600690065006C006400
      2E004600690065006C006400560061006C007500650073000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C007300650001000D000A00
      4700720069006400510075006F007400650073002E0043006F006C0075006D00
      6E0073005B0031005D002E005400690074006C0065002E004300610070007400
      69006F006E000100540069006A00640073007400690070002000630072006500
      610074006900650001004D006F006D0065006E00740020006400650020006C00
      6100200063007200E9006100740069006F006E000100540069006D0065002000
      6F00660020006300720065006100740069006F006E0001000D000A0047007200
      69006400510075006F007400650073002E0043006F006C0075006D006E007300
      5B0032005D002E0043006800650063006B0042006F0078004600690065006C00
      64002E004600690065006C006400560061006C00750065007300010074007200
      750065003B00660061006C0073006500010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001000D00
      0A004700720069006400510075006F007400650073002E0043006F006C007500
      6D006E0073005B0032005D002E005400690074006C0065002E00430061007000
      740069006F006E0001004E00610061006D002000610061006E00760072006100
      67006500720001004E006F006D002000640075002000640065006D0061006E00
      640065007500720001004100700070006C006900630061006E00740020006E00
      61006D00650001000D000A004700720069006400510075006F00740065007300
      2E0043006F006C0075006D006E0073005B0033005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A004700720069006400510075006F00
      7400650073002E0043006F006C0075006D006E0073005B0033005D002E005400
      690074006C0065002E00430061007000740069006F006E000100420065006400
      720069006A00660073006E00610061006D002000610061006E00760072006100
      67006500720001004E006F006D0020006400650020006C00270065006E007400
      72006500700072006900730065002000640065006D0061006E00640065007500
      7200010043006F006D00700061006E00790020006E0061006D00650020006100
      700070006C006900630061006E00740001000D000A0047007200690064005100
      75006F007400650073002E0043006F006C0075006D006E0073005B0034005D00
      2E0043006800650063006B0042006F0078004600690065006C0064002E004600
      690065006C006400560061006C00750065007300010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001000D000A0047007200
      69006400510075006F007400650073002E0043006F006C0075006D006E007300
      5B0034005D002E005400690074006C0065002E00430061007000740069006F00
      6E0001004E00610061006D0020007600650072007A0065006B00650072006400
      650001004E006F006D00200061007300730075007200E90001004E0061006D00
      6500200069006E007300750072006500640001000D000A004700720069006400
      510075006F007400650073002E0043006F006C0075006D006E0073005B003500
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      720069006400510075006F007400650073002E0043006F006C0075006D006E00
      73005B0035005D002E005400690074006C0065002E0043006100700074006900
      6F006E00010056006F006F0072006E00610061006D0020007600650072007A00
      65006B006500720064006500010050007200E9006E006F006D00200061007300
      730075007200E90001004600690072007300740020006E0061006D0065002000
      69006E007300750072006500640001000D000A00470072006900640051007500
      6F007400650073002E0043006F006C0075006D006E0073005B0036005D002E00
      43006800650063006B0042006F0078004600690065006C0064002E0046006900
      65006C006400560061006C00750065007300010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C007300650001000D000A00470072006900
      6400510075006F007400650073002E0043006F006C0075006D006E0073005B00
      36005D002E005400690074006C0065002E00430061007000740069006F006E00
      010049006E00670061006E006700730064006100740075006D00010044006100
      7400650020006400650020006400E90062007500740001005300740061007200
      740069006E0067002000640061007400650001000D000A004700720069006400
      510075006F007400650073002E0043006F006C0075006D006E0073005B003700
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      720069006400510075006F007400650073002E0043006F006C0075006D006E00
      73005B0037005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001004B006100700069007400610061006C0020003100010043006100
      70006900740061006C0065002000310001004300610070006900740061006C00
      2000310001000D000A004700720069006400510075006F007400650073002E00
      43006F006C0075006D006E0073005B0038005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A004700720069006400510075006F007400
      650073002E0043006F006C0075006D006E0073005B0038005D002E0054006900
      74006C0065002E00430061007000740069006F006E0001004B00610070006900
      7400610061006C002000320001004300610070006900740061006C0065002000
      320001004300610070006900740061006C002000320001000D000A0047007200
      69006400510075006F007400650073002E0043006F006C0075006D006E007300
      5B0039005D002E0043006800650063006B0042006F0078004600690065006C00
      64002E004600690065006C006400560061006C00750065007300010074007200
      750065003B00660061006C0073006500010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001000D00
      0A004700720069006400510075006F007400650073002E0043006F006C007500
      6D006E0073005B0039005D002E005400690074006C0065002E00430061007000
      740069006F006E0001004B006100700069007400610061006C00200033000100
      4300610070006900740061006C00650020003300010043006100700069007400
      61006C002000330001000D000A00730074004300680061007200530065007400
      73005F0055006E00690063006F00640065000D000A00}
  end
end
