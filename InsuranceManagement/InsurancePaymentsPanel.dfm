object InsurancePaymentsPanelFrm: TInsurancePaymentsPanelFrm
  Left = 0
  Top = 0
  Width = 1259
  Height = 531
  OnCreate = UniFrameCreate
  Layout = 'hbox'
  LayoutConfig.Margin = '0 8 8 8'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerPaymentsLeft: TUniContainerPanel
    Left = 8
    Top = 8
    Width = 617
    Height = 513
    Hint = ''
    ParentColor = False
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Flex = 1
    LayoutConfig.Height = '100%'
    LayoutConfig.Margin = '0 4 0 0'
    object ContainerHeaderPaymentsLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 617
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      LayoutConfig.Region = 'north'
      ExplicitTop = 8
      ExplicitWidth = 601
      object lblTitleOpenPremiumAmount: TUniLabel
        Left = 0
        Top = 8
        Width = 128
        Height = 13
        Hint = ''
        Caption = 'Openstaande premie ('#8364'):'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 1
        LayoutConfig.Cls = 'boldtext'
      end
      object lblOpenPremiumAmount: TUniDBText
        Left = 144
        Top = 8
        Width = 65
        Height = 13
        Hint = ''
        DataField = 'OPEN_AMOUNT'
        DataSource = dsOpenPremiumAmount
        AutoSize = False
        ParentFont = False
        Font.Style = [fsBold]
        ParentColor = False
        Color = clBtnFace
        LayoutConfig.Cls = 'boldtext'
      end
    end
    object GridPaymentStatus: TUniDBGrid
      Left = 0
      Top = 30
      Width = 617
      Height = 483
      Hint = ''
      DataSource = DsStatus
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      ForceFit = True
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 0 0 0'
      Align = alClient
      TabOrder = 2
      ParentColor = False
      Color = clBtnFace
      OnDrawColumnCell = GridPaymentStatusDrawColumnCell
      Columns = <
        item
          FieldName = 'PERIOD_YEAR'
          Title.Alignment = taRightJustify
          Title.Caption = 'Jaar'
          Width = 85
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'PERIOD_MONTH'
          Title.Alignment = taRightJustify
          Title.Caption = 'Maand'
          Width = 85
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'DUE_AMOUNT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Verschuldigde premie'
          Width = 140
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'OPEN_AMOUNT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Openstaande premie'
          Width = 140
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'AMOUNT_BOOKED'
          Title.Alignment = taRightJustify
          Title.Caption = 'Toegewezen'
          Width = 69
          ReadOnly = True
        end>
    end
  end
  object ContainerPaymentsRight: TUniContainerPanel
    Left = 632
    Top = 8
    Width = 619
    Height = 513
    Hint = ''
    ParentColor = False
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Flex = 1
    LayoutConfig.Height = '100%'
    LayoutConfig.Margin = '0 0 0 4'
    object ContainerHeaderPaymentsRight: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 619
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Region = 'north'
      ExplicitLeft = 40
      ExplicitTop = 8
      ExplicitWidth = 555
      object ContainerFilterRight: TUniContainerPanel
        Left = 527
        Top = 0
        Width = 92
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alRight
        Anchors = []
        TabOrder = 1
        LayoutConfig.Region = 'east'
        ExplicitLeft = 504
        object BtnExportList: TUniThemeButton
          Left = 64
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Exporteren lijst'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 1
          Images = UniMainModule.ImageList
          ImageIndex = 31
          OnClick = BtnExportListClick
          ButtonTheme = uctSecondary
        end
        object btnViewFixedPayment: TUniThemeButton
          Left = 32
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Toon alle periodes voor de geselecteerde betaling'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 2
          Images = UniMainModule.ImageList
          ImageIndex = 42
          OnClick = btnViewFixedPaymentClick
          ButtonTheme = uctPrimary
        end
        object btnViewFixedPeriod: TUniThemeButton
          Left = 0
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Toon alle betalingen voor de geselecteerde periode'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 3
          Images = UniMainModule.ImageList
          ImageIndex = 43
          OnClick = btnViewFixedPeriodClick
          ButtonTheme = uctPrimary
        end
        object btnViewAll: TUniThemeButton
          Left = 0
          Top = 0
          Width = 28
          Height = 28
          Hint = 'Toon alle periodes en alle betalingen'
          ShowHint = True
          ParentShowHint = False
          Caption = ''
          TabStop = False
          TabOrder = 4
          Images = UniMainModule.ImageList
          ImageIndex = 9
          OnClick = btnViewAllClick
          ButtonTheme = uctPrimary
        end
      end
      object ContainerFilterLeft: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 527
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alClient
        TabOrder = 2
        LayoutConfig.Region = 'center'
        ExplicitWidth = 289
        ExplicitHeight = 40
        object lblTitleNumberOfPayments: TUniLabel
          Left = 0
          Top = 8
          Width = 156
          Height = 13
          Hint = ''
          Caption = 'Aantal ontvangen betalingen:'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 1
          LayoutConfig.Cls = 'boldtext'
        end
        object lblNumberOfPayments: TUniDBText
          Left = 168
          Top = 8
          Width = 33
          Height = 13
          Hint = ''
          DataField = 'PAYMENTS_COUNTER'
          DataSource = dsNumberOfPayments
          AutoSize = False
          ParentFont = False
          Font.Style = [fsBold]
          LayoutConfig.Cls = 'boldtext'
        end
      end
    end
    object GridPayments: TUniDBGrid
      Left = 0
      Top = 30
      Width = 619
      Height = 483
      Hint = ''
      DataSource = dsPremiumsToPayments
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      ForceFit = True
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 0 0 0'
      Align = alClient
      TabOrder = 2
      OnDrawColumnCell = GridPaymentsDrawColumnCell
      Columns = <
        item
          FieldName = 'PAYMENT_DATE'
          Title.Caption = 'Datum betaling'
          Width = 115
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'PAYMENT_AMOUNT'
          Title.Alignment = taRightJustify
          Title.Caption = 'Betaalde premie'
          Width = 115
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'AMOUNT_BOOKED'
          Title.Alignment = taRightJustify
          Title.Caption = 'Toegewezen'
          Width = 100
        end
        item
          Flex = 1
          FieldName = 'PAYMENT_METHOD_DESCRIPTION'
          Title.Caption = 'Betalingswijze'
          Width = 184
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'PAYMENT_COMMENT'
          Title.Caption = 'Commentaar'
          Width = 1504
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'SEPA_BATCH_REFERENCE'
          Title.Caption = 'Sepa batch nr.'
          Width = 285
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
  end
  object Payments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select'
      '  a.*,'
      '  a.id as payment_id,'
      '  a.payment_amount as amount_booked,'
      '  b.id as TransfId,'
      '  0 as period_year,'
      '  0 as period_month'
      'from'
      '  insurance_payments a'
      '  left outer join insurance_payments_transf b on (b.id = a.id)'
      'order by'
      '  payment_date desc, id desc')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = PaymentsCalcFields
    Left = 984
    Top = 192
    object PaymentsPAYMENT_AMOUNT: TFloatField
      FieldName = 'PAYMENT_AMOUNT'
    end
    object PaymentsPAYMENT_DATE: TDateField
      FieldName = 'PAYMENT_DATE'
    end
    object PaymentsPAYMENT_METHOD: TSmallintField
      FieldName = 'PAYMENT_METHOD'
    end
    object PaymentsSEPA_BATCH_REFERENCE: TWideStringField
      FieldName = 'SEPA_BATCH_REFERENCE'
      Size = 50
    end
    object PaymentsPAYMENT_METHOD_DESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'PAYMENT_METHOD_DESCRIPTION'
      Size = 30
      Calculated = True
    end
    object PaymentsAMOUNT_BOOKED: TFloatField
      FieldName = 'AMOUNT_BOOKED'
    end
    object PaymentsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object PaymentsPAYMENT_COMMENT: TWideStringField
      FieldName = 'PAYMENT_COMMENT'
      Size = 250
    end
    object PaymentsTRANSFID: TLargeintField
      FieldName = 'TRANSFID'
      ProviderFlags = []
      ReadOnly = True
    end
    object PaymentsPERIOD_YEAR: TIntegerField
      DisplayLabel = 'Jaar'
      FieldName = 'PERIOD_YEAR'
      ReadOnly = True
    end
    object PaymentsPERIOD_MONTH: TIntegerField
      DisplayLabel = 'Maand'
      FieldName = 'PERIOD_MONTH'
      ReadOnly = True
    end
    object PaymentsPAYMENT_ID: TLargeintField
      FieldName = 'PAYMENT_ID'
      Required = True
      Visible = False
    end
  end
  object DsPayments: TDataSource
    DataSet = Payments
    OnDataChange = DsPaymentsDataChange
    Left = 984
    Top = 254
  end
  object LinkedLanguageInsurancePaymentsPanel: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TInsurancePaymentsPanelFrm.Layout'
      'PaymentsPAYMENT_AMOUNT.DisplayLabel'
      'PaymentsPAYMENT_DATE.DisplayLabel'
      'PaymentsPAYMENT_METHOD.DisplayLabel'
      'PaymentsSEPA_BATCH_REFERENCE.DisplayLabel'
      'PaymentsPAYMENT_METHOD_DESCRIPTION.DisplayLabel'
      'PaymentsAMOUNT_BOOKED.DisplayLabel'
      'PaymentsID.DisplayLabel'
      'PaymentsPAYMENT_COMMENT.DisplayLabel'
      'StatusID.DisplayLabel'
      'ContainerPaymentsLeft.Layout'
      'ContainerHeaderPaymentsLeft.Layout'
      'ContainerPaymentsRight.Layout'
      'ContainerHeaderPaymentsRight.Layout'
      'ContainerFilterRight.Layout'
      'ContainerFilterLeft.Layout'
      'StatusCOMPANYID.DisplayLabel'
      'StatusINSURANCE_FILE.DisplayLabel'
      'StatusPERIOD_YEAR.DisplayLabel'
      'StatusPERIOD_MONTH.DisplayLabel'
      'StatusDUE_AMOUNT.DisplayLabel'
      'StatusOPEN_AMOUNT.DisplayLabel'
      'qryPremiumsToPaymentsPAYMENT_AMOUNT.DisplayLabel'
      'qryPremiumsToPaymentsPAYMENT_DATE.DisplayLabel'
      'qryPremiumsToPaymentsPAYMENT_METHOD.DisplayLabel'
      'qryPremiumsToPaymentsSEPA_BATCH_REFERENCE.DisplayLabel'
      'qryPremiumsToPaymentsPAYMENT_METHOD_DESCRIPTION.DisplayLabel'
      'qryPremiumsToPaymentsID.DisplayLabel'
      'qryPremiumsToPaymentsAMOUNT_BOOKED.DisplayLabel'
      'qryPremiumsToPaymentsPAYMENT_COMMENT.DisplayLabel'
      'PaymentsTRANSFID.DisplayLabel'
      'PaymentsPERIOD_YEAR.DisplayLabel'
      'PaymentsPERIOD_MONTH.DisplayLabel'
      'PaymentsPAYMENT_ID.DisplayLabel'
      'StatusAMOUNT_BOOKED.DisplayLabel'
      'qryPremiumsToPaymentsTRANSFID.DisplayLabel'
      'qryPremiumsToPaymentsPERIOD_YEAR.DisplayLabel'
      'qryPremiumsToPaymentsPERIOD_MONTH.DisplayLabel'
      'qryPremiumsToPaymentsINSURANCE_PAYMENTS_ID.DisplayLabel'
      'qryPremiumsToPaymentsPAYMENT_ID.DisplayLabel'
      'qryOpenPremiumAmountINSURANCE_FILE.DisplayLabel'
      'qryOpenPremiumAmountOPEN_AMOUNT.DisplayLabel'
      'qryNumberOfPaymentsINSURANCE_FILE.DisplayLabel'
      'qryNumberOfPaymentsPAYMENTS_COUNTER.DisplayLabel')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageInsurancePaymentsPanelChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 872
    Top = 320
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A006C0062006C005400690074006C0065004F00700065006E00
      5000720065006D00690075006D0041006D006F0075006E00740001004F007000
      65006E0073007400610061006E006400650020007000720065006D0069006500
      20002800AC2029003A0001005000720069006D006500200069006D0070006100
      7900E900650020002800AC202900A0003A0001004F0075007400730074006100
      6E00640069006E00670020007000720065006D00690075006D0020002800AC20
      29003A0001000D000A006C0062006C005400690074006C0065004E0075006D00
      6200650072004F0066005000610079006D0065006E0074007300010041006100
      6E00740061006C0020006F006E007400760061006E00670065006E0020006200
      6500740061006C0069006E00670065006E003A0001004E006F006D0062007200
      6500200064006500200070006100690065006D0065006E007400730020007200
      6500E70075007300A0003A0001004E0075006D0062006500720020006F006600
      20007000610079006D0065006E00740073002000720065006300650069007600
      650064003A0001000D000A0073007400480069006E00740073005F0055006E00
      690063006F00640065000D000A00420074006E004500780070006F0072007400
      4C0069007300740001004500780070006F00720074006500720065006E002000
      6C0069006A007300740001004500780070006F00720074006500720020006C00
      610020006C00690073007400650001004500780070006F007200740020006C00
      69007300740001000D000A00620074006E005600690065007700460069007800
      650064005000610079006D0065006E007400010054006F006F006E0020006100
      6C006C006500200070006500720069006F00640065007300200076006F006F00
      7200200064006500200067006500730065006C00650063007400650065007200
      64006500200062006500740061006C0069006E00670001004100660066006900
      6300680065007200200074006F00750074006500730020006C00650073002000
      7000E900720069006F00640065007300200070006F007500720020006C006500
      200070006100690065006D0065006E00740020007300E9006C00650063007400
      69006F006E006E00E9000100530068006F007700200061006C006C0020007000
      6500720069006F0064007300200066006F007200200074006800650020007300
      65006C006500630074006500640020007000610079006D0065006E0074000100
      0D000A00620074006E0056006900650077004600690078006500640050006500
      720069006F006400010054006F006F006E00200061006C006C00650020006200
      6500740061006C0069006E00670065006E00200076006F006F00720020006400
      6500200067006500730065006C00650063007400650065007200640065002000
      70006500720069006F0064006500010041006600660069006300680065007200
      200074006F007500730020006C0065007300200070006100690065006D006500
      6E0074007300200070006F007500720020006C00610020007000E90072006900
      6F006400650020007300E9006C0065006300740069006F006E006E00E9006500
      0100530068006F007700200061006C006C0020007000610079006D0065006E00
      74007300200066006F00720020007400680065002000730065006C0065006300
      740065006400200070006500720069006F00640001000D000A00620074006E00
      560069006500770041006C006C00010054006F006F006E00200061006C006C00
      6500200070006500720069006F00640065007300200065006E00200061006C00
      6C006500200062006500740061006C0069006E00670065006E00010041006600
      660069006300680065007200200074006F00750074006500730020006C006500
      730020007000E900720069006F00640065007300200065007400200074006F00
      7500730020006C0065007300200070006100690065006D0065006E0074007300
      0100530068006F007700200061006C006C00200070006500720069006F006400
      7300200061006E006400200061006C006C0020007000610079006D0065006E00
      7400730001000D000A007300740044006900730070006C00610079004C006100
      620065006C0073005F0055006E00690063006F00640065000D000A0073007400
      46006F006E00740073005F0055006E00690063006F00640065000D000A007300
      74004D0075006C00740069004C0069006E00650073005F0055006E0069006300
      6F00640065000D000A007300740053007400720069006E00670073005F005500
      6E00690063006F00640065000D000A007300740072007300740072005F003300
      6D006F006E00740068005F007000720065006D00690075006D00010054007200
      69006D0065007300740072006900EB006C00650020007000720065006D006900
      650001005000720069006D00650020007400720069006D006500730074007200
      690065006C006C006500010051007500610072007400650072006C0079002000
      7000720065006D00690075006D0001000D000A00730074007200730074007200
      5F0036006D006F006E00740068005F007000720065006D00690075006D000100
      530065006D0065007300740072006900EB006C00650020007000720065006D00
      6900650001005000720069006D0065002000730065006D006500730074007200
      690065006C006C0065000100530065006D0069002D0061006E006E0075006100
      6C0020007000720065006D00690075006D0001000D000A007300740072007300
      740072005F00640065006C006500740065005F007000610079006D0065006E00
      74000100560065007200770069006A0064006500720065006E00200062006500
      740061006C0069006E0067003F0001005300750070007000720069006D006500
      720020006C006500200070006100690065006D0065006E0074003F0001004400
      65006C0065007400650020007000610079006D0065006E0074003F0001000D00
      0A007300740072007300740072005F006500720072006F0072005F0064006500
      6C006500740069006E006700010046006F00750074002000620069006A002000
      6800650074002000760065007200770069006A0064006500720065006E002000
      760061006E00200064006500200062006500740061006C0069006E0067002E00
      010045007200720065007500720020006C006F00720073002000640065002000
      6C00610020007300750070007000720065007300730069006F006E0020006400
      7500200070006100690065006D0065006E0074002E0001004500720072006F00
      72002000640065006C006500740069006E00670020007000610079006D006500
      6E0074002E0001000D000A007300740072007300740072005F006D006F006E00
      740068005F007000720065006D00690075006D0001004D00610061006E006400
      65006C0069006A006B007300650020007000720065006D006900650001005000
      720069006D00650020006D0065006E007300750065006C006C00650001004D00
      6F006E00740068006C00790020007000720065006D00690075006D0001000D00
      0A007300740072007300740072005F007000610079006D0065006E0074007300
      010042006500740061006C0069006E00670065006E00200070006F006C006900
      7300010050006100690065006D0065006E0074007300200070006F006C006900
      63006500730001005000610079006D0065006E0074007300200070006F006C00
      6900630069006500730001000D000A007300740072007300740072005F007000
      6D005F0064006900660066006500720065006E00630065005F00700061007900
      6D0065006E007400010042006500740061006C0069006E006700730076006500
      720073006300680069006C00010050006100690065006D0065006E0074002000
      6400650020006400690066006600E900720065006E0063006500010044006900
      660066006500720065006E006300650020007000610079006D0065006E007400
      01000D000A007300740072007300740072005F0070006D005F006D0061006E00
      750061006C005F007000610079006D0065006E00740001004D0061006E007500
      65006C006500200062006500740061006C0069006E0067000100500061006900
      65006D0065006E00740020006D0061006E00750065006C0001004D0061006E00
      750061006C0020007000610079006D0065006E00740001000D000A0073007400
      72007300740072005F0070006D005F006F006E006C0069006E0065005F007000
      610079006D0065006E00740001004F006E006C0069006E006500200062006500
      740061006C0069006E006700010050006100690065006D0065006E0074002000
      65006E0020006C00690067006E00650001004F006E006C0069006E0065002000
      7000610079006D0065006E00740001000D000A00730074007200730074007200
      5F0070006D005F007200650069006D00620075007200730065006D0065006E00
      74005F007000610079006D0065006E0074000100540065007200750067006200
      6500740061006C0069006E0067000100520065006D0062006F00750072007300
      65006D0065006E00740001005200650069006D00620075007200730065006D00
      65006E00740001000D000A007300740072007300740072005F0070006D005F00
      7300650070006100010053006500700061000100530065007000610001005300
      65007000610001000D000A007300740072007300740072005F0070006D005F00
      75006E006B006E006F0077006E005F007000610079006D0065006E0074000100
      4F006E00620065006B0065006E006400010049006E0063006F006E006E007500
      010055006E006B006E006F0077006E0001000D000A0073007400720073007400
      72005F00700075007200630068006100730065005F0070007200690063006500
      01004B006F006F00700073006F006D0001005000720069006D00650020007500
      6E00690071007500650001005000750072006300680061007300650020007000
      720069006300650001000D000A007300740072007300740072005F0079006500
      610072005F007000720065006D00690075006D0001004A006100610072006C00
      69006A006B007300650020007000720065006D00690065000100500072006900
      6D006500200061006E006E00750065006C006500010041006E006E0075006100
      6C0020007000720065006D00690075006D0001000D000A007300740072007300
      740072005F0070006D005F005400720061006E00730066006500720074005400
      6F0001004F0076006500720062006F0065006B0069006E00670020006E006100
      6100720001005400720061006E00730066006500720074002000E00001005400
      720061006E0073006600650072007400200074006F0001000D000A0073007400
      72007300740072005F0070006D005F005400720061006E007300660065007200
      7400460072006F006D0001004F0076006500720062006F0065006B0069006E00
      67002000760061006E0001005400720061006E00730066006500720074002000
      6400650001005400720061006E00730066006500720074002000660072006F00
      6D0001000D000A00730074004F00740068006500720053007400720069006E00
      670073005F0055006E00690063006F00640065000D000A007300740043006F00
      6C006C0065006300740069006F006E0073005F0055006E00690063006F006400
      65000D000A0047007200690064005000610079006D0065006E00740053007400
      61007400750073002E0043006F006C0075006D006E0073005B0030005D002E00
      43006800650063006B0042006F0078004600690065006C0064002E0046006900
      65006C006400560061006C00750065007300010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C007300650001000D000A00470072006900
      64005000610079006D0065006E0074005300740061007400750073002E004300
      6F006C0075006D006E0073005B0030005D002E005400690074006C0065002E00
      430061007000740069006F006E0001004A00610061007200010041006E006E00
      E90065000100590065006100720001000D000A00470072006900640050006100
      79006D0065006E0074005300740061007400750073002E0043006F006C007500
      6D006E0073005B0031005D002E0043006800650063006B0042006F0078004600
      690065006C0064002E004600690065006C006400560061006C00750065007300
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      650001000D000A0047007200690064005000610079006D0065006E0074005300
      740061007400750073002E0043006F006C0075006D006E0073005B0031005D00
      2E005400690074006C0065002E00430061007000740069006F006E0001004D00
      610061006E00640001004D006F006900730001004D006F006E00740068000100
      0D000A0047007200690064005000610079006D0065006E007400530074006100
      7400750073002E0043006F006C0075006D006E0073005B0032005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      5000610079006D0065006E0074005300740061007400750073002E0043006F00
      6C0075006D006E0073005B0032005D002E005400690074006C0065002E004300
      61007000740069006F006E00010056006500720073006300680075006C006400
      690067006400650020007000720065006D006900650001005000720069006D00
      6500200064007500650001005000720065006D00690075006D00200064007500
      650001000D000A0047007200690064005000610079006D0065006E0074005300
      740061007400750073002E0043006F006C0075006D006E0073005B0033005D00
      2E0043006800650063006B0042006F0078004600690065006C0064002E004600
      690065006C006400560061006C00750065007300010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001000D000A0047007200
      690064005000610079006D0065006E0074005300740061007400750073002E00
      43006F006C0075006D006E0073005B0033005D002E005400690074006C006500
      2E00430061007000740069006F006E0001004F00700065006E00730074006100
      61006E006400650020007000720065006D006900650001005000720069006D00
      6500200069006D00700061007900E90001004F00750074007300740061006E00
      640069006E00670020007000720065006D00690075006D0001000D000A004700
      7200690064005000610079006D0065006E007400530074006100740075007300
      2E0043006F006C0075006D006E0073005B0034005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A004700720069006400500061007900
      6D0065006E0074005300740061007400750073002E0043006F006C0075006D00
      6E0073005B0034005D002E005400690074006C0065002E004300610070007400
      69006F006E00010054006F00650067006500770065007A0065006E0001004100
      740074007200690062007500E9000100410073007300690067006E0065006400
      01000D000A0047007200690064005000610079006D0065006E00740073002E00
      43006F006C0075006D006E0073005B0030005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A0047007200690064005000610079006D00
      65006E00740073002E0043006F006C0075006D006E0073005B0030005D002E00
      5400690074006C0065002E00430061007000740069006F006E00010044006100
      740075006D00200062006500740061006C0069006E0067000100440061007400
      6500200064006500200070006100690065006D0065006E007400010050006100
      79006D0065006E0074002000640061007400650001000D000A00470072006900
      64005000610079006D0065006E00740073002E0043006F006C0075006D006E00
      73005B0031005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A0047007200690064005000610079006D0065006E00740073002E004300
      6F006C0075006D006E0073005B0031005D002E005400690074006C0065002E00
      430061007000740069006F006E000100420065007400610061006C0064006500
      20007000720065006D006900650001005000720069006D006500200070006100
      7900E900650001005000720065006D00690075006D0020007000610069006400
      01000D000A0047007200690064005000610079006D0065006E00740073002E00
      43006F006C0075006D006E0073005B0032005D002E0043006800650063006B00
      42006F0078004600690065006C0064002E004600690065006C00640056006100
      6C00750065007300010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C0073006500010074007200750065003B00
      660061006C007300650001000D000A0047007200690064005000610079006D00
      65006E00740073002E0043006F006C0075006D006E0073005B0032005D002E00
      5400690074006C0065002E00430061007000740069006F006E00010054006F00
      650067006500770065007A0065006E0001004100740074007200690062007500
      E9000100410073007300690067006E006500640001000D000A00470072006900
      64005000610079006D0065006E00740073002E0043006F006C0075006D006E00
      73005B0033005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A0047007200690064005000610079006D0065006E00740073002E004300
      6F006C0075006D006E0073005B0033005D002E005400690074006C0065002E00
      430061007000740069006F006E00010042006500740061006C0069006E006700
      7300770069006A007A00650001004D006F006400650020006400650020007000
      6100690065006D0065006E00740001005000610079006D0065006E0074002000
      6D006500740068006F00640001000D000A004700720069006400500061007900
      6D0065006E00740073002E0043006F006C0075006D006E0073005B0034005D00
      2E0043006800650063006B0042006F0078004600690065006C0064002E004600
      690065006C006400560061006C00750065007300010074007200750065003B00
      660061006C0073006500010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001000D000A0047007200
      690064005000610079006D0065006E00740073002E0043006F006C0075006D00
      6E0073005B0034005D002E005400690074006C0065002E004300610070007400
      69006F006E00010043006F006D006D0065006E00740061006100720001004300
      6F006D006D0065006E0074006100690072006500010043006F006D006D006500
      6E00740061007200790001000D000A0047007200690064005000610079006D00
      65006E00740073002E0043006F006C0075006D006E0073005B0035005D002E00
      43006800650063006B0042006F0078004600690065006C0064002E0046006900
      65006C006400560061006C00750065007300010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      74007200750065003B00660061006C007300650001000D000A00470072006900
      64005000610079006D0065006E00740073002E0043006F006C0075006D006E00
      73005B0035005D002E005400690074006C0065002E0043006100700074006900
      6F006E000100530065007000610020006200610074006300680020006E007200
      2E0001004E0075006D00E90072006F0020006400650020006C006F0074002000
      53006500700061002E0001005300650070006100200062006100740063006800
      20006E006F002E0001000D000A00730074004300680061007200530065007400
      73005F0055006E00690063006F00640065000D000A00}
  end
  object Status: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select  a.*,'
      '        b.amount_booked,'
      '        b.insurance_payments_id'
      'from    insurance_premium_status a'
      
        '        left outer join insurance_payments_to_premiums b on (b.i' +
        'nsurance_premium_status_id = a.id)'
      'order by a.period_year desc, a.period_month desc')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 232
    Top = 168
    object StatusID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object StatusCOMPANYID: TLargeintField
      FieldName = 'COMPANYID'
    end
    object StatusINSURANCE_FILE: TLargeintField
      FieldName = 'INSURANCE_FILE'
    end
    object StatusPERIOD_YEAR: TIntegerField
      FieldName = 'PERIOD_YEAR'
    end
    object StatusPERIOD_MONTH: TIntegerField
      FieldName = 'PERIOD_MONTH'
    end
    object StatusDUE_AMOUNT: TFloatField
      FieldName = 'DUE_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object StatusOPEN_AMOUNT: TFloatField
      FieldName = 'OPEN_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object StatusAMOUNT_BOOKED: TFloatField
      DisplayLabel = 'Toegewezen'
      FieldName = 'AMOUNT_BOOKED'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
  end
  object DsStatus: TDataSource
    DataSet = Status
    OnDataChange = DsStatusDataChange
    Left = 232
    Top = 222
  end
  object qryPremiumsToPayments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select  a.insurance_premium_status_id,'
      '        a.insurance_payments_id,'
      '        a.amount_booked,'
      '        c.id as transfid,'
      '        d.period_year,'
      '        d.period_month,'
      '        b.id as payment_id,'
      '        b.*'
      'from    insurance_payments_to_premiums a'
      
        '        inner join insurance_payments b on (b.id = a.insurance_p' +
        'ayments_id)'
      
        '        inner join insurance_premium_status d on (d.id = a.insur' +
        'ance_premium_status_id)'
      
        '        left outer join insurance_payments_transf c on (c.id = b' +
        '.id)'
      
        'where   insurance_premium_status_id = :insurance_premium_status_' +
        'id'
      'order by b.payment_date desc, b.id desc'
      '')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = PaymentsCalcFields
    Left = 872
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'insurance_premium_status_id'
        Value = nil
      end>
    object qryPremiumsToPaymentsPAYMENT_AMOUNT: TFloatField
      FieldName = 'PAYMENT_AMOUNT'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object qryPremiumsToPaymentsPAYMENT_DATE: TDateField
      FieldName = 'PAYMENT_DATE'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object qryPremiumsToPaymentsPAYMENT_METHOD: TSmallintField
      FieldName = 'PAYMENT_METHOD'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object qryPremiumsToPaymentsSEPA_BATCH_REFERENCE: TWideStringField
      FieldName = 'SEPA_BATCH_REFERENCE'
      ReadOnly = True
      Size = 50
    end
    object qryPremiumsToPaymentsPAYMENT_METHOD_DESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'PAYMENT_METHOD_DESCRIPTION'
      Size = 30
      Calculated = True
    end
    object qryPremiumsToPaymentsID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
      Required = True
    end
    object qryPremiumsToPaymentsAMOUNT_BOOKED: TFloatField
      FieldName = 'AMOUNT_BOOKED'
      DisplayFormat = '#,##0.00'
    end
    object qryPremiumsToPaymentsPAYMENT_COMMENT: TWideStringField
      FieldName = 'PAYMENT_COMMENT'
      ReadOnly = True
      Size = 250
    end
    object qryPremiumsToPaymentsTRANSFID: TLargeintField
      FieldName = 'TRANSFID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryPremiumsToPaymentsPERIOD_YEAR: TIntegerField
      DisplayLabel = 'Jaar'
      FieldName = 'PERIOD_YEAR'
      ReadOnly = True
    end
    object qryPremiumsToPaymentsPERIOD_MONTH: TIntegerField
      DisplayLabel = 'Maand'
      FieldName = 'PERIOD_MONTH'
      ReadOnly = True
    end
    object qryPremiumsToPaymentsINSURANCE_PAYMENTS_ID: TLargeintField
      FieldName = 'INSURANCE_PAYMENTS_ID'
      Required = True
      Visible = False
    end
    object qryPremiumsToPaymentsPAYMENT_ID: TLargeintField
      FieldName = 'PAYMENT_ID'
      ReadOnly = True
      Required = True
      Visible = False
    end
  end
  object dsPremiumsToPayments: TDataSource
    DataSet = qryPremiumsToPayments
    Left = 872
    Top = 254
  end
  object qryOpenPremiumAmount: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select   insurance_file,'
      
        '         coalesce (sum (case when (a.PERIOD_YEAR * 100 + a.PERIO' +
        'D_MONTH) <= :CurMonth1 then a.OPEN_AMOUNT else 0 END)'
      
        '          -sum (case when (a.PERIOD_YEAR * 100 + a.PERIOD_MONTH)' +
        '  > :CurMonth2 then a.DUE_AMOUNT - a.OPEN_AMOUNT else 0 END), 0)' +
        ' as OPEN_AMOUNT'
      'from    INSURANCE_PREMIUM_STATUS a'
      'group by insurance_file')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 232
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CurMonth1'
        Value = 202408
      end
      item
        DataType = ftInteger
        Name = 'CurMonth2'
        Value = 202408
      end>
    object qryOpenPremiumAmountINSURANCE_FILE: TLargeintField
      FieldName = 'INSURANCE_FILE'
    end
    object qryOpenPremiumAmountOPEN_AMOUNT: TFloatField
      FieldName = 'OPEN_AMOUNT'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
  end
  object dsOpenPremiumAmount: TDataSource
    DataSet = qryOpenPremiumAmount
    OnDataChange = dsOpenPremiumAmountDataChange
    Left = 232
    Top = 358
  end
  object qryNumberOfPayments: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select    insurance_file,'
      '          coalesce (count(*), 0) as PAYMENTS_COUNTER'
      'from      INSURANCE_PAYMENTS'
      'group by  insurance_file'
      '')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 416
    Top = 296
    object qryNumberOfPaymentsINSURANCE_FILE: TLargeintField
      FieldName = 'INSURANCE_FILE'
      Required = True
    end
    object qryNumberOfPaymentsPAYMENTS_COUNTER: TLargeintField
      FieldName = 'PAYMENTS_COUNTER'
      ReadOnly = True
    end
  end
  object dsNumberOfPayments: TDataSource
    DataSet = qryNumberOfPayments
    OnDataChange = dsOpenPremiumAmountDataChange
    Left = 416
    Top = 358
  end
end
