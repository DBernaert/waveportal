unit DashboardInsuranceFiles;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniImage, uniButton, UniThemeButton, uniLabel,
  uniGUIBaseClasses, uniPanel, uniMultiItem, uniComboBox, Data.DB, MemDS,
  DBAccess, IBC, uniHTMLFrame, UniFSGoogleChart, VirtualTable, siComp, siLngLnk,
  Vcl.Imaging.jpeg, Vcl.Imaging.pngimage, ContributorsMain;

var
	str_stopped:                 string = 'Stopgezet'; // TSI: Localized (Don't modify!)
	str_not_stopped:             string = 'Niet-stopgezet'; // TSI: Localized (Don't modify!)
	str_not_indexed:             string = 'Niet-ge�ndexeerd'; // TSI: Localized (Don't modify!)
	str_indexed:                 string = 'Ge�ndexeerd'; // TSI: Localized (Don't modify!)
	str_medical:                 string = 'Medisch'; // TSI: Localized (Don't modify!)
	str_not_medical:             string = 'Niet-medisch'; // TSI: Localized (Don't modify!)
	str_backlog:                 string = 'Achterstallig'; // TSI: Localized (Don't modify!)
	str_not_backlog:             string = 'Niet-achterstallig'; // TSI: Localized (Don't modify!)
	str_average_insured_capital: string = 'Gemiddeld verzekerd kapitaal: '; // TSI: Localized (Don't modify!)

type
  TDashboardInsuranceFilesFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerHeaderLeft: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerHeaderRight: TUniContainerPanel;
    ComboboxProduct: TUniComboBox;
    Products: TIBCQuery;
    ContainerBody: TUniContainerPanel;
    ContainerGeneralNumbers: TUniContainerPanel;
    LblTitleAverageInsuredCapital: TUniLabel;
    ContainerStatistics: TUniContainerPanel;
    ContainerProducts: TUniContainerPanel;
    ContainerTotals: TUniContainerPanel;
    ContainerIndexation: TUniContainerPanel;
    ContainerRetard: TUniContainerPanel;
    ContainerHeaderTotals: TUniContainerPanel;
    LblTitleTotals: TUniLabel;
    LblTitleTotalNumberInsuranceFiles: TUniLabel;
    LblValueTotalNumberInsuranceFiles: TUniLabel;
    LblTitleNumberOfFilesStopped: TUniLabel;
    LblValueNumberOfFilesStopped: TUniLabel;
    LblValuePercentageOfFilesStopped: TUniLabel;
    LblTitleNetTotalNumberInsuranceFiles: TUniLabel;
    LblValueNumberOfFilesActive: TUniLabel;
    ContainerHeaderIndexation: TUniContainerPanel;
    LblTitleIndexation: TUniLabel;
    LblTitleIndexedInsuranceFiles: TUniLabel;
    LblTitleNonIndexedInsuranceFiles: TUniLabel;
    LblValueNumberOfFilesNotIndexed: TUniLabel;
    LblValueNumberOfFilesIndexed: TUniLabel;
    LblValuePercentageOfFilesIndexed: TUniLabel;
    LblValuePercentageOfFilesNotIndexed: TUniLabel;
    ContainerHeaderRetard: TUniContainerPanel;
    LblTitleBehindInsuranceFiles: TUniLabel;
    LblTitleBehindNumberInsuranceFiles: TUniLabel;
    LblTitleNotBehindNumberInsuranceFiles: TUniLabel;
    LblValueNumberOfFilesBackLog: TUniLabel;
    LblValueNumberOfFilesNotBackLog: TUniLabel;
    LblValuePercentageOfFilesBackLog: TUniLabel;
    LblValuePercentageOfFilesNotBackLog: TUniLabel;
    ChartTotals: TUniFSGoogleChart;
    ChartIndexation: TUniFSGoogleChart;
    ChartBackLog: TUniFSGoogleChart;
    TblTotals: TVirtualTable;
    TblTotalsDescription: TStringField;
    TblTotalsQuantity: TCurrencyField;
    TblIndexation: TVirtualTable;
    StringField1: TStringField;
    CurrencyField1: TCurrencyField;
    TblBackLog: TVirtualTable;
    StringField3: TStringField;
    CurrencyField3: TCurrencyField;
    LinkedLanguageDashboardInsuranceFiles: TsiLangLinked;
    PanelFamilyCare: TUniPanel;
    LogoPatronale: TUniImage;
    LblShortCutPortalFamilyCare: TUniLabel;
    PanelNucleus: TUniPanel;
    ImagePrimaLifeProduction: TUniImage;
    LblShortCutSimulation: TUniLabel;
    LblShortCutPortal: TUniLabel;
    LblTitleFamilyCare: TUniLabel;
    LblTitleFuneralCare: TUniLabel;
    ImagePatronale: TUniImage;
    ImageFuneralCare: TUniImage;
    AxaImage1: TUniImage;
    AxaImage2: TUniImage;
    procedure ComboboxProductChange(Sender: TObject);
    procedure LinkedLanguageDashboardInsuranceFilesChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
    procedure LblShortCutPortalClick(Sender: TObject);
    procedure LblShortCutSimulationClick(Sender: TObject);
    procedure LblShortCutPortalFamilyCareClick(Sender: TObject);
  private
    { Private declarations }
    procedure Create_contributor_statistics;
  public
    { Public declarations }
    ContributorsFrm: TContributorsMainFrm;
    procedure Start_dashboard_insurancefiles;
  end;

implementation

{$R *.dfm}

uses MainModule, DateUtils, FuneralCareCalculator;

{ TDashboardInsuranceFilesFrm }

procedure TDashboardInsuranceFilesFrm.ComboboxProductChange(Sender: TObject);
begin
  Create_contributor_statistics;
end;

procedure TDashboardInsuranceFilesFrm.Start_dashboard_insurancefiles;
var SQL: string;
begin
  if UniMainModule.Portal_theme_color <> 0
  then begin
         ContainerHeaderTotals.Color     := UniMainModule.Portal_theme_color;
         ContainerHeaderIndexation.Color := UniMainModule.Portal_theme_color;
         ContainerHeaderRetard.Color     := UniMainModule.Portal_theme_color;
       end;

  Products.Close;
  Products.SQL.Clear;
  SQL := 'select distinct ' +
         'insurance_institutions.id, insurance_institutions.name ' +
         'from insurance_institutions ' +
         'where insurance_institutions.id in ' +
         '(select insurance_institution from contr_insur_comission_schemes where contributor=' + IntToStr(UniMainModule.Reference_contributor_id) + ' ' +
         ' and removed=''0'' ) ' +
         'and insurance_institutions.removed=''0'' ' +
         'order by insurance_institutions.name';
  Products.SQL.Add(SQL);
  Products.Open;
  Products.First;
  while not Products.Eof
  do begin
       ComboboxProduct.Items.Add(Products.FieldByName('Name').AsString);
       Products.Next;
     end;
  Products.First;
  Create_contributor_statistics;
end;

procedure TDashboardInsuranceFilesFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TDashboardInsuranceFilesFrm.UpdateStrings;
begin
  str_average_insured_capital := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_average_insured_capital' (* 'Gemiddeld verzekerd kapitaal: ' *) );
  str_not_backlog             := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_not_backlog' (* 'Niet-achterstallig' *) );
  str_backlog                 := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_backlog' (* 'Achterstallig' *) );
  str_not_medical             := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_not_medical' (* 'Niet-medisch' *) );
  str_medical                 := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_medical' (* 'Medisch' *) );
  str_indexed                 := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_indexed' (* 'Ge�ndexeerd' *) );
  str_not_indexed             := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_not_indexed' (* 'Niet-ge�ndexeerd' *) );
  str_not_stopped             := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_not_stopped' (* 'Niet-stopgezet' *) );
  str_stopped                 := LinkedLanguageDashboardInsuranceFiles.GetTextOrDefault('strstr_stopped' (* 'Stopgezet' *) );
end;

procedure TDashboardInsuranceFilesFrm.Create_contributor_statistics;
var SQL:                 string;
    StatisticsEndDate:   TDateTime;
    InsuranceCompany:    integer;

    TotalFileCount:      longint;
    WorkCount:           longint;
    WorkPercentage:      currency;
    TotalInsuredCapital: currency;
begin
  StatisticsEndDate       := EnCodeDateTime(YearOf(Date), MonthOf(Date), 1, 0, 0, 0, 0);
  StatisticsEndDate       := IncDay(StatisticsEndDate, -1);
  if ComboboxProduct.Itemindex = 0
  then InsuranceCompany := 0
  else begin
         Products.First;
         Products.MoveBy(ComboboxProduct.ItemIndex - 1);
         InsuranceCompany := Products.FieldByName('Id').AsInteger;
       end;

  //Total number of insurance files
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select count(*) as counter from insurance_files '                                  +
         'where CompanyId=' + IntToStr(UniMainModule.Company_id) + ' '                       +
         'and removed=''0'' ';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ' or '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '
  else SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Responsable_contributor_credit) + ' and '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';

  if InsuranceCompany <> 0
  then SQL := SQL + ' and insurance_company=' + IntToStr(InsuranceCompany) + ' ';
  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  TotalFileCount := UniMainModule.QWork.FieldByName('Counter').asInteger;
  LblValueTotalNumberInsuranceFiles.Caption := IntToStr(TotalFileCount);
  UniMainModule.QWork.Close;

  //Total number of insurance files stopped
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select count(*) as counter from insurance_files '                                  +
         'where CompanyId=' + IntToStr(UniMainModule.Company_id) + ' '                       +
         'and removed=''0'' ';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ' or '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '               +
              'and cancelled=1 '
  else SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Responsable_contributor_credit) + ' and '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '               +
              'and cancelled=1 ';

  if InsuranceCompany <> 0
  then SQL := SQL + ' and insurance_company=' + IntToStr(InsuranceCompany) + ' ';

  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  WorkCount := UniMainModule.QWork.FieldByName('Counter').asInteger;
  LblValueNumberOfFilesStopped.Caption := IntToStr(WorkCount);
  UniMainModule.QWork.Close;

  //Calculate percentage
  Try
    WorkPercentage := WorkCount / TotalFileCount * 100;
  Except
    WorkPercentage := 0;
  End;

  LblValuePercentageOfFilesStopped.Caption := FormatCurr('#,##0.00 %', WorkPercentage);
  LblValueNumberOfFilesActive.Caption      := IntToStr(TotalFileCount - WorkCount);

  TblTotals.First;
  while not TblTotals.Eof
  do TblTotals.Delete;

  TblTotals.Append;
  TblTotals.FieldByName('Description').AsString := str_stopped;
  TblTotals.FieldByName('Quantity').AsCurrency  := WorkCount;
  TblTotals.Post;

  TblTotals.Append;
  TblTotals.FieldByName('Description').AsString := str_not_stopped;
  TblTotals.FieldByName('Quantity').AsCurrency  := TotalFileCount - WorkCount;
  TblTotals.Post;

  //Total number of insurance files active
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select count(*) as counter from insurance_files '                                  +
         'where CompanyId=' + IntToStr(UniMainModule.Company_id) + ' '                       +
         'and removed=''0'' '                                                                +
         'and trim(insurance_files.insurance_file_number) <> '''' ';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ' or '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '
  else SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Responsable_contributor_credit) + ' and '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';

  SQL := SQL +
         'and cancelled=0 '                                                                  +
         'and start_date_insurance <= ' + QuotedStr(FormatDateTime('YYYY-MM-DD', StatisticsEndDate)) + ' ';

  if InsuranceCompany <> 0
  then SQL := SQL + ' and insurance_company=' + IntToStr(InsuranceCompany) + ' ';
  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  UniMainModule.QWork.Close;

  //Average insured capital
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select sum(Total_amount_insurance) as totalAmount from insurance_files ' +
         'where companyId=' + IntToStr(UniMainModule.Company_id) + ' '             +
         'and removed=''0'' ';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ' or ' +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '
  else SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Responsable_contributor_credit) + ' and ' +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';

  if InsuranceCompany <> 0
  then SQL := SQL + ' and insurance_company=' + IntToStr(InsuranceCompany) + ' ';

  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  TotalInsuredCapital := UniMainModule.QWork.FieldByName('totalAmount').AsCurrency;
  UniMainModule.QWork.Close;

  //Calculate average
  Try
    WorkPercentage := TotalInsuredCapital / TotalFileCount;
  Except
    WorkPercentage := 0;
  End;
  LblTitleAverageInsuredCapital.Caption    := str_average_insured_capital + FormatCurr('#,##0.00 �', WorkPercentage);

  //Total number of indexed files
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select count(*) as counter from insurance_files '                        +
         'where CompanyId=' + IntToStr(UniMainModule.Company_id) + ' '             +
         'and removed=''0'' ';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ' or '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '     +
              'and indexation=1 '
  else SQL := SQL +
              'and (contributor_id=' + IntToStr(UniMainModule.Responsable_contributor_credit) + ' and '  +
              'agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '     +
              'and indexation=1 ';

  if InsuranceCompany <> 0
  then SQL := SQL + ' and insurance_company=' + IntToStr(InsuranceCompany) + ' ';
  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  WorkCount := UniMainModule.QWork.FieldByName('Counter').asInteger;
  LblValueNumberOfFilesIndexed.Caption := IntToStr(WorkCount);
  UniMainModule.QWork.Close;

  //Calculate percentage
  Try
    WorkPercentage := WorkCount / TotalFileCount * 100;
  Except
    WorkPercentage := 0;
  End;
  LblValuePercentageOfFilesIndexed.Caption    := FormatCurr('#,##0.00 %', WorkPercentage);
  LblValueNumberOfFilesNotIndexed.Caption := IntToStr(TotalFileCount - WorkCount);
  LblValuePercentageOfFilesNotIndexed.Caption := FormatCurr('#,##0.00 %', 100 - WorkPercentage);

  TblIndexation.First;
  while not TblIndexation.Eof
  do TblIndexation.Delete;

  TblIndexation.Append;
  TblIndexation.FieldByName('Description').AsString := str_not_indexed;
  TblIndexation.FieldByName('Quantity').AsCurrency  := TotalFileCount - WorkCount;
  TblIndexation.Post;

  TblIndexation.Append;
  TblIndexation.FieldByName('Description').AsString := str_indexed;
  TblIndexation.FieldByName('Quantity').AsCurrency  := WorkCount;
  TblIndexation.Post;

  //Total number of files with retarded payments
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;

  SQL := 'select count(*) as counter ' +
         'from insurance_files insf  ' +
         'left outer join insurance_institutions insc on (insf.insurance_company = insc.id) ' +
         'where insf.CompanyId=' + IntToStr(UniMainModule.Company_id) + ' '                   +
         'and insf.removed=''0'' '                                                            +
         'and trim(insf.insurance_file_number) <> '''' ';

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then SQL := SQL +
              'and (insf.contributor_id=' + IntToStr(UniMainModule.Reference_contributor_id)  + ' or ' +
              'insf.agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') '
  else SQL := SQL +
              'and (insf.contributor_id=' + IntToStr(UniMainModule.Responsable_contributor_credit)  + ' and ' +
              'insf.agent_id=' + IntToStr(UniMainModule.Reference_contributor_id) + ') ';

  SQL := SQL +
         'and insf.cancelled=0 '                                                              +
         'and insf.first_monthly_premium_payed = 1 '                                          +
         'and insf.required_doc_okay = 1 '                                                    +
         'and insc.include_in_backlog_lists = 1 '                                             +
         'and insf.start_date_insurance <= ' + QuotedStr(FormatDateTime('YYYY-MM-DD', StatisticsEndDate)) + ' ';

  if InsuranceCompany <> 0
  then SQL := SQL + ' and insf.insurance_company=' + IntToStr(InsuranceCompany) + ' ';

  SQL := SQL +
         'and ' +
         'CASE ' +
         'WHEN insf.premium_payment = 0 ' +
         'THEN datediff(month, (select first 1 payment_period_date from insurance_payments where insurance_payments.insurance_file = insf.id order by insurance_payments.payment_period_date desc), current_date) > 1 ' +
         'WHEN insf.premium_payment = 1 ' +
         'THEN datediff(month, (select first 1 payment_period_date from insurance_payments where insurance_payments.insurance_file = insf.id order by insurance_payments.payment_period_date desc), current_date) > 4 ' +
         'WHEN insf.premium_payment = 2 ' +
         'THEN datediff(month, (select first 1 payment_period_date from insurance_payments where insurance_payments.insurance_file = insf.id order by insurance_payments.payment_period_date desc), current_date) > 7 ' +
         'WHEN insf.premium_payment = 3 ' +
         'THEN datediff(month, (select first 1 payment_period_date from insurance_payments where insurance_payments.insurance_file = insf.id order by insurance_payments.payment_period_date desc), current_date) > 13 ' +
         'END ';

  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  WorkCount := UniMainModule.QWork.FieldByName('Counter').asInteger;
  LblValueNumberOfFilesBackLog.Caption := IntToStr(WorkCount);
  UniMainModule.QWork.Close;

  //Calculate percentage
  Try
    WorkPercentage := WorkCount / TotalFileCount * 100;
  Except
    WorkPercentage := 0;
  End;
  LblValuePercentageOfFilesBackLog.Caption    := FormatCurr('#,##0.00 %', WorkPercentage);

  LblValueNumberOfFilesNotBackLog.Caption := IntToStr(TotalFileCount - WorkCount);
  LblValuePercentageOfFilesNotBackLog.Caption := FormatCurr('#,##0.00 %', 100 - WorkPercentage);

  TblBackLog.First;
  while not TblBackLog.Eof
  do TblBackLog.Delete;

  TblBackLog.Append;
  TblBackLog.FieldByName('Description').AsString := str_backlog;
  TblBackLog.FieldByName('Quantity').AsCurrency  := WorkCount;
  TblBackLog.Post;

  TblBackLog.Append;
  TblBackLog.FieldByName('Description').AsString := str_not_backlog;
  TblBackLog.FieldByName('Quantity').AsCurrency  := TotalFileCount - WorkCount;
  TblBackLog.Post;

  ChartTotals.LoadChart;
  ChartIndexation.LoadChart;
  ChartBackLog.LoadChart;
end;

procedure TDashboardInsuranceFilesFrm.LblShortCutPortalClick(Sender: TObject);
begin
  UniSession.AddJS('window.open(''https://nucleuslifeag.siaspa.com'', ''_blank'')');
end;

procedure TDashboardInsuranceFilesFrm.LblShortCutPortalFamilyCareClick(
  Sender: TObject);
begin
  UniSession.AddJS('window.open(''https://cares.siaspa.com/icn/app/#/access/signin'', ''_blank'')');
end;

procedure TDashboardInsuranceFilesFrm.LblShortCutSimulationClick(
  Sender: TObject);
begin
  With FuneralCareCalculatorFrm
  do begin
       Start_funeral_care_calculator;
       ShowModal;
     end;
end;

procedure TDashboardInsuranceFilesFrm.LinkedLanguageDashboardInsuranceFilesChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

end.


