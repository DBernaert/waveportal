object InsuranceStatementsMainFrm: TInsuranceStatementsMainFrm
  Left = 0
  Top = 0
  Width = 1226
  Height = 690
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1226
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 107
      Height = 25
      Hint = ''
      Caption = 'Borderellen'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object ContainerBodyDocuments: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1226
    Height = 657
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 1
    Layout = 'hbox'
    LayoutConfig.Region = 'center'
    object ContainerInsuranceStatements: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 1226
      Height = 657
      Hint = ''
      ParentColor = False
      Align = alClient
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Flex = 1
      LayoutConfig.Height = '100%'
      object ContainerFilterDocuments1: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 1226
        Height = 30
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Region = 'north'
        object ContainerFilterRight: TUniContainerPanel
          Left = 1198
          Top = 0
          Width = 28
          Height = 30
          Hint = ''
          ParentColor = False
          Align = alRight
          TabOrder = 1
          LayoutConfig.Region = 'east'
          object BtnPrintDocument: TUniThemeButton
            Left = 0
            Top = 0
            Width = 28
            Height = 28
            Hint = 'Afdrukken'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            TabStop = False
            TabOrder = 1
            Images = UniMainModule.ImageList
            ImageIndex = 51
            OnClick = BtnPrintDocumentClick
            ButtonTheme = uctPrimary
          end
        end
      end
      object GridInsuranceStatements: TUniDBGrid
        Left = 0
        Top = 30
        Width = 1226
        Height = 627
        Hint = ''
        DataSource = DsInsuranceStatements
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
        WebOptions.Paged = False
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        LayoutConfig.Cls = 'customGrid'
        LayoutConfig.Region = 'center'
        LayoutConfig.Margin = '8 0 0 0'
        Align = alClient
        TabOrder = 2
        OnDblClick = BtnPrintDocumentClick
        OnFieldImageURL = GridInsuranceStatementsFieldImageURL
        Columns = <
          item
            FieldName = 'PRO_FORMA'
            Title.Alignment = taCenter
            Title.Caption = 'PF'
            Width = 40
            Alignment = taCenter
            ImageOptions.Visible = True
            ImageOptions.Width = 12
            ImageOptions.Height = 12
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            Flex = 1
            FieldName = 'BATCH_YEAR'
            Title.Alignment = taRightJustify
            Title.Caption = 'Jaar'
            Width = 64
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            Flex = 1
            FieldName = 'BATCH_MONTH'
            Title.Alignment = taRightJustify
            Title.Caption = 'Periode'
            Width = 64
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            Flex = 1
            FieldName = 'DOCUMENT_DATE'
            Title.Caption = 'Document datum'
            Width = 85
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            Flex = 1
            FieldName = 'DOCUMENT_TOTAL_AMOUNT'
            Title.Alignment = taRightJustify
            Title.Caption = 'Totaal bedrag'
            Width = 71
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
    end
  end
  object InsuranceStatements: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  INSURANCE_DOCUMENT.ID,'
      '  INSURANCE_DOCUMENT.INSURANCE_DOCUMENT_BATCH,'
      '  INSURANCE_DOCUMENT.DOCUMENT_DATE,'
      '  INSURANCE_DOCUMENT.PRO_FORMA,'
      '  INSURANCE_DOCUMENT.DOCUMENT_TOTAL_AMOUNT,'
      '  INSURANCE_DOCUMENT_BATCH.BATCH_MONTH,'
      '  INSURANCE_DOCUMENT_BATCH.BATCH_YEAR'
      'FROM'
      '  INSURANCE_DOCUMENT'
      
        '  LEFT OUTER JOIN INSURANCE_DOCUMENT_BATCH ON (INSURANCE_DOCUMEN' +
        'T.INSURANCE_DOCUMENT_BATCH = INSURANCE_DOCUMENT_BATCH.ID)'
      
        'ORDER BY INSURANCE_DOCUMENT_BATCH.BATCH_YEAR desc, INSURANCE_DOC' +
        'UMENT_BATCH.BATCH_MONTH desc, INSURANCE_DOCUMENT.DOCUMENT_DATE d' +
        'esc')
    MasterFields = 'ID'
    Left = 575
    Top = 248
    object InsuranceStatementsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InsuranceStatementsDOCUMENT_DATE: TDateField
      FieldName = 'DOCUMENT_DATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object InsuranceStatementsDOCUMENT_TOTAL_AMOUNT: TFloatField
      FieldName = 'DOCUMENT_TOTAL_AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InsuranceStatementsBATCH_MONTH: TIntegerField
      FieldName = 'BATCH_MONTH'
      ReadOnly = True
    end
    object InsuranceStatementsBATCH_YEAR: TIntegerField
      FieldName = 'BATCH_YEAR'
      ReadOnly = True
    end
    object InsuranceStatementsINSURANCE_DOCUMENT_BATCH: TLargeintField
      FieldName = 'INSURANCE_DOCUMENT_BATCH'
      Required = True
    end
    object InsuranceStatementsPRO_FORMA: TSmallintField
      FieldName = 'PRO_FORMA'
    end
  end
  object DsInsuranceStatements: TDataSource
    DataSet = InsuranceStatements
    Left = 575
    Top = 304
  end
  object LinkedLanguageInsuranceStatementsMain: TsiLangLinked
    Version = '7.8.5.1'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST')
    SmartExcludeProps.Strings = (
      'TInsuranceStatementsMainFrm.Layout'
      'ContainerHeader.Layout'
      'ContainerBodyDocuments.Layout'
      'ContainerInsuranceStatements.Layout'
      'ContainerFilterDocuments1.Layout'
      'ContainerFilterRight.Layout'
      'InsuranceStatementsID.DisplayLabel'
      'InsuranceStatementsDOCUMENT_DATE.DisplayLabel'
      'InsuranceStatementsDOCUMENT_TOTAL_AMOUNT.DisplayLabel'
      'InsuranceStatementsBATCH_MONTH.DisplayLabel'
      'InsuranceStatementsBATCH_YEAR.DisplayLabel'
      'InsuranceStatementsINSURANCE_DOCUMENT_BATCH.DisplayLabel'
      'InsuranceStatementsPRO_FORMA.DisplayLabel')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 576
    Top = 368
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C006500010042006F007200
      64006500720065006C006C0065006E00010042006F0072006400650072006500
      6100750078000100530074006100740065006D0065006E007400730001000D00
      0A0073007400480069006E00740073005F0055006E00690063006F0064006500
      0D000A00420074006E005000720069006E00740044006F00630075006D006500
      6E0074000100410066006400720075006B006B0065006E00010049006D007000
      720069006D006500720001005000720069006E00740001000D000A0073007400
      44006900730070006C00610079004C006100620065006C0073005F0055006E00
      690063006F00640065000D000A007300740046006F006E00740073005F005500
      6E00690063006F00640065000D000A00730074004D0075006C00740069004C00
      69006E00650073005F0055006E00690063006F00640065000D000A0073007400
      53007400720069006E00670073005F0055006E00690063006F00640065000D00
      0A00730074004F00740068006500720053007400720069006E00670073005F00
      55006E00690063006F00640065000D000A007300740043006F006C006C006500
      6300740069006F006E0073005F0055006E00690063006F00640065000D000A00
      470072006900640049006E0073007500720061006E0063006500530074006100
      740065006D0065006E00740073002E0043006F006C0075006D006E0073005B00
      30005D002E0043006800650063006B0042006F0078004600690065006C006400
      2E004600690065006C006400560061006C007500650073000100740072007500
      65003B00660061006C0073006500010074007200750065003B00660061006C00
      73006500010074007200750065003B00660061006C007300650001000D000A00
      470072006900640049006E0073007500720061006E0063006500530074006100
      740065006D0065006E00740073002E0043006F006C0075006D006E0073005B00
      30005D002E005400690074006C0065002E00430061007000740069006F006E00
      01005000460001005000460001005000460001000D000A004700720069006400
      49006E0073007500720061006E0063006500530074006100740065006D006500
      6E00740073002E0043006F006C0075006D006E0073005B0031005D002E004300
      6800650063006B0042006F0078004600690065006C0064002E00460069006500
      6C006400560061006C00750065007300010074007200750065003B0066006100
      6C0073006500010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C007300650001000D000A004700720069006400
      49006E0073007500720061006E0063006500530074006100740065006D006500
      6E00740073002E0043006F006C0075006D006E0073005B0031005D002E005400
      690074006C0065002E00430061007000740069006F006E0001004A0061006100
      7200010041006E006E00E90065000100590065006100720001000D000A004700
      72006900640049006E0073007500720061006E00630065005300740061007400
      65006D0065006E00740073002E0043006F006C0075006D006E0073005B003200
      5D002E0043006800650063006B0042006F0078004600690065006C0064002E00
      4600690065006C006400560061006C0075006500730001007400720075006500
      3B00660061006C0073006500010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C007300650001000D000A004700
      72006900640049006E0073007500720061006E00630065005300740061007400
      65006D0065006E00740073002E0043006F006C0075006D006E0073005B003200
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      50006500720069006F006400650001005000E900720069006F00640065000100
      50006500720069006F00640001000D000A00470072006900640049006E007300
      7500720061006E0063006500530074006100740065006D0065006E0074007300
      2E0043006F006C0075006D006E0073005B0033005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A00470072006900640049006E007300
      7500720061006E0063006500530074006100740065006D0065006E0074007300
      2E0043006F006C0075006D006E0073005B0033005D002E005400690074006C00
      65002E00430061007000740069006F006E00010044006F00630075006D006500
      6E007400200064006100740075006D0001004400610074006500200064006F00
      630075006D0065006E007400010044006F00630075006D0065006E0074002000
      640061007400650001000D000A00470072006900640049006E00730075007200
      61006E0063006500530074006100740065006D0065006E00740073002E004300
      6F006C0075006D006E0073005B0034005D002E0043006800650063006B004200
      6F0078004600690065006C0064002E004600690065006C006400560061006C00
      750065007300010074007200750065003B00660061006C007300650001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C007300650001000D000A00470072006900640049006E00730075007200
      61006E0063006500530074006100740065006D0065006E00740073002E004300
      6F006C0075006D006E0073005B0034005D002E005400690074006C0065002E00
      430061007000740069006F006E00010054006F007400610061006C0020006200
      6500640072006100670001004D006F006E00740061006E007400200074006F00
      740061006C00010054006F00740061006C00200061006D006F0075006E007400
      01000D000A0073007400430068006100720053006500740073005F0055006E00
      690063006F00640065000D000A00}
  end
end
