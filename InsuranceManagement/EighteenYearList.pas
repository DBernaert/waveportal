unit EighteenYearList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniBasicGrid,
  uniDBGrid, uniEdit, uniImage, uniLabel, uniGUIBaseClasses, uniPanel,
  uniDateTimePicker, uniButton, UniThemeButton, siComp, siLngLnk;

var
	str_list_eighteen_year: string = 'Lijst 18-jarigen'; // TSI: Localized (Don't modify!)

type
  TEighteenYearListFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    HeaderList: TUniContainerPanel;
    ContainerFilterRightBackLog: TUniContainerPanel;
    ContainerFilterLeftBackLog: TUniContainerPanel;
    GridEighteenYear: TUniDBGrid;
    ListEighteenYear: TIBCQuery;
    DsListEighteenYear: TDataSource;
    EditStartDate: TUniDateTimePicker;
    EditEndDate: TUniDateTimePicker;
    BtnSearch: TUniThemeButton;
    LinkedLanguageEighteenYearList: TsiLangLinked;
    ListEighteenYearID: TLargeintField;
    ListEighteenYearINSURANCE_FILE_NUMBER: TWideStringField;
    ListEighteenYearLAST_NAME: TWideStringField;
    ListEighteenYearFIRST_NAME: TWideStringField;
    ListEighteenYearADDRESS_CITY: TWideStringField;
    ListEighteenYearPHONE_MOBILE: TWideStringField;
    ListEighteenYearEMAIL: TWideStringField;
    ListEighteenYearBIRTHDATE: TDateField;
    ListEighteenYearPHONE_HOME: TWideStringField;
    ListEighteenYearAGENTNAME: TWideStringField;
    ListEighteenYearAGENTFIRSTNAME: TWideStringField;
    ListEighteenYearAGENTCOMPANYNAME: TWideStringField;
    ListEighteenYearAGENTPHONEMOBILE: TWideStringField;
    ListEighteenYearAGENTEMAIL: TWideStringField;
    BtnExportListBackLog: TUniThemeButton;
    procedure BtnSearchClick(Sender: TObject);
    procedure BtnExportListBackLogClick(Sender: TObject);
    procedure EditStartDateKeyPress(Sender: TObject; var Key: Char);
    procedure LinkedLanguageEighteenYearListChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure UniFrameCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_eighteen_year_list_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Main;



{ TEighteenYearListFrm }

procedure TEighteenYearListFrm.BtnExportListBackLogClick(Sender: TObject);
begin
  if ListEighteenYear.Active = False
  then exit;

  MainForm.ExportGrid(GridEighteenYear, str_list_eighteen_year, ListEighteenYear, 'Id');
end;

procedure TEighteenYearListFrm.BtnSearchClick(Sender: TObject);
begin
  if (EditStartDate.DateTime = 0) or (EditEndDate.DateTime = 0)
  then exit;

  ListEighteenYear.Close;

  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then begin
         ListEighteenYear.MacroByName('Condition').Value :=
         '(insurance_files.contributor_id = :IdContributor or insurance_files.agent_id = :IdAgent)';

         ListEighteenYear.Params.ParamByName('IdContributor').AsInteger :=
         UniMainModule.Reference_contributor_id;

         ListEighteenYear.Params.ParamByName('IdAgent').AsInteger :=
         UniMainModule.Reference_contributor_id;
       end
  else begin
         ListEighteenYear.MacroByName('Condition').Value :=
         '(insurance_files.contributor_id = :IdContributor and insurance_files.agent_id = :IdAgent)';

         ListEighteenYear.Params.ParamByName('IdContributor').AsInteger :=
         UniMainModule.Responsable_contributor_credit;

         ListEighteenYear.Params.ParamByName('IdAgent').AsInteger :=
         UniMainModule.Reference_contributor_id;
       end;

  ListEighteenYear.Params.ParamByName('ListStartdate').AsDate    := EditStartDate.DateTime;
  ListEighteenYear.Params.ParamByName('ListEnddate').AsDate      := EditEndDate.DateTime;
  ListEighteenYear.Open;
end;

procedure TEighteenYearListFrm.EditStartDateKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #13
  then begin
         Key := #0;
         BtnSearchClick(Sender);
       end;
end;

procedure TEighteenYearListFrm.LinkedLanguageEighteenYearListChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TEighteenYearListFrm.Start_eighteen_year_list_module;
begin
  EditStartDate.DateTime := Date;
  EditEndDate.DateTime   := IncMonth(Date, 2);
  EditStartDate.SetFocus;
  BtnSearchClick(nil);
end;

procedure TEighteenYearListFrm.UniFrameCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TEighteenYearListFrm.UpdateStrings;
begin
  str_list_eighteen_year := LinkedLanguageEighteenYearList.GetTextOrDefault('strstr_list_eighteen_year' (* 'Lijst 18-jarigen' *) );
end;

end.

