unit InsuranceQuotesMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniGUIBaseClasses, uniPanel,
  uniMultiItem, uniComboBox, uniButton, UniThemeButton, uniEdit, uniBasicGrid,
  uniDBGrid, Data.DB, MemDS, DBAccess, IBC, uniDBText, siComp, siLngLnk;

var
	str_natural_person: string = 'Natuurlijk persoon'; // TSI: Localized (Don't modify!)
	str_company:        string = 'Bedrijf'; // TSI: Localized (Don't modify!)
	str_no:             string = 'Nee'; // TSI: Localized (Don't modify!)
	str_yes:            string = 'Ja'; // TSI: Localized (Don't modify!)
	str_dutch:          string = 'Nederlands'; // TSI: Localized (Don't modify!)
	str_french:         string = 'Frans'; // TSI: Localized (Don't modify!)
	str_quotes:         string = 'Offertes'; // TSI: Localized (Don't modify!)

type
  TInsuranceQuotesMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    BtnSearch: TUniThemeButton;
    ContainerFilterRight: TUniContainerPanel;
    BtnPrint: TUniThemeButton;
    BtnExportList: TUniThemeButton;
    TblQuotes: TIBCQuery;
    TblQuotesID: TLargeintField;
    TblQuotesCOMPANYID: TLargeintField;
    TblQuotesQUOTE_CREATION_TIME: TDateTimeField;
    TblQuotesQUOTE_MAIL_TIME: TDateTimeField;
    TblQuotesFLAG_MAILED: TSmallintField;
    TblQuotesCONTRIBUTOR_ID: TLargeintField;
    TblQuotesDEMANDER_COMPANY_NAME: TWideStringField;
    TblQuotesDEMANDER_DATE_OF_BIRTH: TDateField;
    TblQuotesDEMANDER_TYPE: TSmallintField;
    TblQuotesDEMANDER_EMAIL: TWideStringField;
    TblQuotesFILE_COSTS: TSmallintField;
    TblQuotesSTART_DATE_INSURANCE: TDateField;
    TblQuotesCAPITAL_1: TFloatField;
    TblQuotesPURCHASE_PRICE_1: TFloatField;
    TblQuotesGROSS_PREMIUM_MONTHLY_1: TFloatField;
    TblQuotesGROSS_PREMIUM_QUARTERLY_1: TFloatField;
    TblQuotesGROSS_PREMIUM_YEARLY_1: TFloatField;
    TblQuotesCAPITAL_2: TFloatField;
    TblQuotesPURCHASE_PRICE_2: TFloatField;
    TblQuotesGROSS_PREMIUM_MONTHLY_2: TFloatField;
    TblQuotesGROSS_PREMIUM_QUARTERLY_2: TFloatField;
    TblQuotesGROSS_PREMIUM_YEARLY_2: TFloatField;
    TblQuotesCAPITAL_3: TFloatField;
    TblQuotesPURCHASE_PRICE_3: TFloatField;
    TblQuotesGROSS_PREMIUM_MONTHLY_3: TFloatField;
    TblQuotesGROSS_PREMIUM_QUARTERLY_3: TFloatField;
    TblQuotesGROSS_PREMIUM_YEARLY_3: TFloatField;
    TblQuotesDemanderType: TStringField;
    TblQuotesSendByMail: TStringField;
    TblQuotesDEMANDER_FULLNAME: TWideStringField;
    TblQuotesLANGUAGE: TSmallintField;
    TblQuotesLanguageDescription: TStringField;
    TblQuotesCONTRIBUTOR_FULLNAME: TWideStringField;
    TblQuotesCOMPANY_NAME: TWideStringField;
    DsQuotes: TDataSource;
    GridQuotes: TUniDBGrid;
    PnlDetails: TUniPanel;
    LblTitleName: TUniLabel;
    LblTitleCompanyName: TUniLabel;
    LblTitleLanguage: TUniLabel;
    LblTitleEmail: TUniLabel;
    LblValueLastName: TUniDBText;
    LblValueCompanyName: TUniDBText;
    LblValueLanguage: TUniDBText;
    LblValueEmail: TUniDBText;
    LblTitleDateOfBirth: TUniLabel;
    LblTitleStartDateInsurance: TUniLabel;
    LblTitleFileCosts: TUniLabel;
    LblTitleDemanderType: TUniLabel;
    LblValueDateOfBirth: TUniDBText;
    LblValueStartDateInsurance: TUniDBText;
    LblValueFileCosts: TUniDBText;
    LblValueDemanderType: TUniDBText;
    LblTitleTimeCreation: TUniLabel;
    LblTitleTimeEmail: TUniLabel;
    LblTitleSendByMail: TUniLabel;
    LblValueTimeCreation: TUniDBText;
    LblValueTimeEmail: TUniDBText;
    LblValueSendByMail: TUniDBText;
    LblTitlePropositions: TUniLabel;
    LblTitleCapital: TUniLabel;
    LblValueCapital1: TUniDBText;
    LblValueCapital2: TUniDBText;
    LblValueCapital3: TUniDBText;
    LblTitlePurchasePrice: TUniLabel;
    LblValuePurchasePrice1: TUniDBText;
    LblValuePurchasePrice2: TUniDBText;
    LblValuePurchasePrice3: TUniDBText;
    LblTitleGrossMonthPremium: TUniLabel;
    LblValueGrossMonthPremium1: TUniDBText;
    LblValueGrossMonthPremium2: TUniDBText;
    LblValueGrossMonthPremium3: TUniDBText;
    LblTitleGrossSemesterPremium: TUniLabel;
    LblValueGrossSemesterPremium1: TUniDBText;
    LblValueGrossSemesterPremium2: TUniDBText;
    LblValueGrossSemesterPremium3: TUniDBText;
    LblTitleGrossYearPremium: TUniLabel;
    LblValueGrossYearPremium1: TUniDBText;
    LblValueGrossYearPremium2: TUniDBText;
    LblValueGrossYearPremium3: TUniDBText;
    LinkedLanguageInsuranceQuotesMain: TsiLangLinked;
    TblQuotesDEMANDER_NAME: TWideStringField;
    TblQuotesDEMANDER_FIRSTNAME: TWideStringField;
    TblQuotesINSURED_NAME: TWideStringField;
    TblQuotesINSURED_FIRSTNAME: TWideStringField;
    procedure BtnSearchClick(Sender: TObject);
    procedure TblQuotesCalcFields(DataSet: TDataSet);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure BtnExportListClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageInsuranceQuotesMainChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure BtnPrintClick(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackPrintQuote(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_insurance_quotes_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Utils, Main, InsuranceQuotePreview;



{ TInsuranceQuotesMainFrm }

procedure TInsuranceQuotesMainFrm.BtnExportListClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridQuotes, str_quotes, TblQuotes, 'Id');
end;

procedure TInsuranceQuotesMainFrm.BtnPrintClick(Sender: TObject);
begin
  if (TblQuotes.Active) and (not TblQuotes.FieldByName('Id').IsNull)
  then begin
         with InsuranceQuotePreviewFrm
         do begin
              Generate_quote(TblQuotes.FieldByName('Id').AsInteger);
              ShowModal(CallBackPrintQuote);
            end;
       end;
end;

procedure TInsuranceQuotesMainFrm.BtnSearchClick(Sender: TObject);
var SQL: string;
begin
  TblQuotes.Close;

  SQL :=
  'select ' +
  'insurance_quotes.id, insurance_quotes.companyid, ' +
  'insurance_quotes.quote_creation_time, insurance_quotes.quote_mail_time, ' +
  'insurance_quotes.flag_mailed, insurance_quotes.contributor_id, ' +
  'insurance_quotes.demander_name, insurance_quotes.demander_firstname, ' +
  'coalesce(insurance_quotes.demander_name, '''') || '' '' || coalesce(insurance_quotes.demander_firstname, '''') demander_fullname, ' +
  'insurance_quotes.demander_company_name, insurance_quotes.demander_date_of_birth, ' +
  'insurance_quotes.demander_type, insurance_quotes.demander_email, ' +
  'insurance_quotes.file_costs, insurance_quotes.start_date_insurance, ' +
  'insurance_quotes.capital_1, insurance_quotes.purchase_price_1, ' +
  'insurance_quotes.gross_premium_monthly_1, insurance_quotes.gross_premium_quarterly_1, ' +
  'insurance_quotes.gross_premium_yearly_1, insurance_quotes.capital_2, ' +
  'insurance_quotes.purchase_price_2, insurance_quotes.gross_premium_monthly_2, ' +
  'insurance_quotes.gross_premium_quarterly_2, insurance_quotes.gross_premium_yearly_2, ' +
  'insurance_quotes.capital_3, insurance_quotes.purchase_price_3, ' +
  'insurance_quotes.gross_premium_monthly_3, insurance_quotes.gross_premium_quarterly_3, ' +
  'insurance_quotes.gross_premium_yearly_3, insurance_quotes.language, ' +
  'insurance_quotes.insured_name, insurance_quotes.insured_firstname, ' +
  'coalesce(credit_contributors.last_name, '''') || '' '' || coalesce(credit_contributors.first_name, '''') contributor_fullname, ' +
  'credit_contributors.company_name ' +
  'from credit_contributors ' +
  'right outer join insurance_quotes on (credit_contributors.id = insurance_quotes.contributor_id) ' +
  'where insurance_quotes.contributor_id=:contributor_id ';

  if Trim(EditSearch.Text) <> ''
  then SQL :=
       SQL + 'and (insurance_quotes.demander_name like '
             + Utils.SearchValue(EditSearch.Text) + ' OR '    +
             'insurance_quotes.demander_firstname like '      +
             Utils.SearchValue(EditSearch.text) + ' OR '      +
             'insurance_quotes.demander_company_name like '   +
             Utils.SearchValue(EditSearch.text) + ' OR '      +
             'credit_contributors.last_name like '            +
             Utils.SearchValue(EditSearch.Text) + ' OR '      +
             'credit_contributors.first_name like '           +
             Utils.SearchValue(EditSearch.Text) + ' OR '      +
             'credit_contributors.company_name like '         +
             Utils.SearchValue(EditSearch.Text) + ' OR '      +
             'insurance_quotes.insured_name like '            +
             Utils.SearchValue(EditSearch.Text) + ' OR '      +
             'insurance_quotes.insured_firstname like '       +
             Utils.SearchValue(EditSearch.text) + ')' ;

  SQL :=
  SQL + ' order by insurance_quotes.quote_creation_time desc';

  TblQuotes.SQL.Text := SQL;

  TblQuotes.ParamByName('Contributor_id').AsInteger :=
  UniMainModule.User_id;

  TblQuotes.Open;
end;

procedure TInsuranceQuotesMainFrm.CallBackPrintQuote(Sender: TComponent;
  AResult: Integer);
begin
  if TblQuotes.Active
  then TblQuotes.Refresh;
end;

procedure TInsuranceQuotesMainFrm.EditSearchKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13
  then begin
         Key := #0;
         BtnSearchClick(Sender);
       end;
end;

procedure TInsuranceQuotesMainFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInsuranceQuotesMainFrm.LinkedLanguageInsuranceQuotesMainChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInsuranceQuotesMainFrm.Start_insurance_quotes_module;
begin
  EditSearch.SetFocus;
  BtnSearchClick(nil);
end;

procedure TInsuranceQuotesMainFrm.TblQuotesCalcFields(DataSet: TDataSet);
begin
  case TblQuotes.FieldByName('Demander_type').AsInteger of
    0: TblQuotes.FieldByName('DemanderType').AsString := str_natural_person;
    1: TblQuotes.FieldByName('DemanderType').AsString := str_company;
  end;

  case TblQuotes.FieldByName('Flag_mailed').AsInteger of
    0: TblQuotes.FieldByName('SendByMail').AsString := str_no;
    1: TblQuotes.FieldByName('SendByMail').AsString := str_yes;
  end;

  case TblQuotes.FieldByName('Language').AsInteger of
    0: TblQuotes.FieldByName('LanguageDescription').AsString := str_dutch;
    1: TblQuotes.FieldByName('LanguageDescription').AsString := str_french;
  end;
end;

procedure TInsuranceQuotesMainFrm.UpdateStrings;
begin
  str_quotes         := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_quotes' (* 'Offertes' *) );
  str_french         := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_french' (* 'Frans' *) );
  str_dutch          := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_dutch' (* 'Nederlands' *) );
  str_yes            := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_yes' (* 'Ja' *) );
  str_no             := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_no' (* 'Nee' *) );
  str_company        := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_company' (* 'Bedrijf' *) );
  str_natural_person := LinkedLanguageInsuranceQuotesMain.GetTextOrDefault('strstr_natural_person' (* 'Natuurlijk persoon' *) );
end;

end.

