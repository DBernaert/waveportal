unit InvalidSepa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniLabel, uniGUIBaseClasses, uniPanel,
  uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC, uniDateTimePicker,
  uniButton, UniThemeButton, siComp, siLngLnk;

var
	str_no_valid_mandate: string = 'Geen geldig mandaat'; // TSI: Localized (Don't modify!)
	str_creditor_blocked_by_debtor: string = 'Schuldeiser geblokkeerd door debiteur'; // TSI: Localized (Don't modify!)
	str_debtor_account_closed: string = 'Debiteurenrekening gesloten'; // TSI: Localized (Don't modify!)
	str_debtor_account_blocked: string = 'Debiteurenrekening geblokkeerd'; // TSI: Localized (Don't modify!)
	str_invalid_twikey_mandates: string = 'Ongeldige sepa''s'; // TSI: Localized (Don't modify!)

type
  TInvalidSepaFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    LblTitle: TUniLabel;
    HeaderList: TUniContainerPanel;
    ContainerFilterRightBackLog: TUniContainerPanel;
    BtnExportListBackLog: TUniThemeButton;
    InvalidList: TIBCQuery;
    InvalidListID: TLargeintField;
    InvalidListCONTRIBUTOR_ID: TLargeintField;
    InvalidListSTART_DATE_INSURANCE: TDateField;
    InvalidListINSURANCE_FILE_NUMBER: TWideStringField;
    InvalidListPHONE_MOBILE: TWideStringField;
    InvalidListINSURANCE_TAKER: TWideStringField;
    InvalidListINSURED_PERSON: TWideStringField;
    InvalidListPREMIUM: TFloatField;
    InvalidListAGENT_NAME: TWideStringField;
    InvalidListINSURANCE_COMPANY_NAME: TWideStringField;
    InvalidListSTRUCTURED_STATEMENT: TWideStringField;
    InvalidListREASON_TWIKEY_INVALID: TSmallintField;
    InvalidListReason_invalid_description: TStringField;
    InvalidListLASTPAYMENT: TDateField;
    InvalidListDELTA: TLargeintField;
    DsInvalidList: TDataSource;
    GridInvalidList: TUniDBGrid;
    LinkedLanguageInvalidSepa: TsiLangLinked;
    procedure InvalidListCalcFields(DataSet: TDataSet);
    procedure BtnExportListBackLogClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageInvalidSepaChangeLanguage(Sender: TObject);
    procedure UpdateStrings;
    procedure GridInvalidListDrawColumnCell(Sender: TObject; ACol,
      ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_invalid_sepa_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Main;



{ TInvalidSepaFrm }

procedure TInvalidSepaFrm.BtnExportListBackLogClick(Sender: TObject);
begin
  MainForm.ExportGrid(GridInvalidList, str_invalid_twikey_mandates, InvalidList, 'Id');
end;

procedure TInvalidSepaFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInvalidSepaFrm.GridInvalidListDrawColumnCell(Sender: TObject; ACol,
  ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if InvalidList.FieldByName('Delta').AsInteger >= 4
  then Attribs.Font.Color := UniMainModule.Color_red;
end;

procedure TInvalidSepaFrm.InvalidListCalcFields(DataSet: TDataSet);
begin
  case InvalidList.FieldByName('reason_twikey_invalid').AsInteger of
    0: InvalidList.FieldByName('Reason_invalid_description').AsString := str_no_valid_mandate;
    1: InvalidList.FieldByName('Reason_invalid_description').AsString := str_creditor_blocked_by_debtor;
    2: InvalidList.FieldByName('Reason_invalid_description').AsString := str_debtor_account_closed;
    3: InvalidList.FieldByName('Reason_invalid_description').AsString := str_debtor_account_blocked;
  end;
end;

procedure TInvalidSepaFrm.LinkedLanguageInvalidSepaChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInvalidSepaFrm.Start_invalid_sepa_module;
begin
  if (UniMainModule.Responsable_contributor_credit = 0) or (Trim(UpperCase(UniMainModule.Company_name)) = 'ARMONA')
  then begin
         InvalidList.MacroByName('Condition').Value :=
         '(insf.contributor_id=:contributorid or insf.agent_id=:agentid)';

         InvalidList.Params.ParamByName('contributorId').AsInteger :=
         UniMainModule.Reference_contributor_id;

         InvalidList.Params.ParamByName('agentId').AsInteger :=
         UniMainModule.Reference_contributor_id;
       end
  else begin
         InvalidList.MacroByName('Condition').Value :=
         '(insf.contributor_id=:contributorid and insf.agent_id=:agentid)';

         InvalidList.Params.ParamByName('contributorId').AsInteger :=
         UniMainModule.Responsable_contributor_credit;

         InvalidList.Params.ParamByName('agentId').AsInteger :=
         UniMainModule.Reference_contributor_id;
       end;

  InvalidList.Open;
end;

procedure TInvalidSepaFrm.UpdateStrings;
begin
  str_invalid_twikey_mandates := LinkedLanguageInvalidSepa.GetTextOrDefault('strstr_invalid_twikey_mandates' (* 'Ongeldige sepa's' *) );
  str_debtor_account_blocked := LinkedLanguageInvalidSepa.GetTextOrDefault('strstr_debtor_account_blocked' (* 'Debiteurenrekening geblokkeerd' *) );
  str_debtor_account_closed := LinkedLanguageInvalidSepa.GetTextOrDefault('strstr_debtor_account_closed' (* 'Debiteurenrekening gesloten' *) );
  str_creditor_blocked_by_debtor := LinkedLanguageInvalidSepa.GetTextOrDefault('strstr_creditor_blocked_by_debtor' (* 'Schuldeiser geblokkeerd door debiteur' *) );
  str_no_valid_mandate := LinkedLanguageInvalidSepa.GetTextOrDefault('strstr_no_valid_mandate' (* 'Geen geldig mandaat' *) );
end;

end.

