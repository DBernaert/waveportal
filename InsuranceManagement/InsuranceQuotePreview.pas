unit InsuranceQuotePreview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniPanel,
  uniGUIBaseClasses, uniURLFrame, frxClass, frxExportBaseDialog, frxExportPDF,
  siComp, siLngLnk;

var
	str_mail_send: string = 'E-mail verstuurd'; // TSI: Localized (Don't modify!)
	str_error_sending_mail: string = 'Fout bij het versturen van de e-mail'; // TSI: Localized (Don't modify!)

type
  TInsuranceQuotePreviewFrm = class(TUniForm)
    URLFrame: TUniURLFrame;
    PanelFooter: TUniContainerPanel;
    ContainerButtons: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    PdfExport: TfrxPDFExport;
    Report: TfrxReport;
    BtnSendMail: TUniThemeButton;
    LinkedLanguageInsuranceQuotePreview: TsiLangLinked;
    BtnDownload: TUniThemeButton;
    procedure BtnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LinkedLanguageInsuranceQuotePreviewChangeLanguage(
      Sender: TObject);
    procedure UpdateStrings;
    procedure BtnDownloadClick(Sender: TObject);
    procedure BtnSendMailClick(Sender: TObject);
  private
    { Private declarations }
    _QuoteId: longint;
    _FileName: string;
    _Demander_email: string;
    _Contributor_email: string;
    _Additional_email: string;
    _Language: integer;
  public
    { Public declarations }
    procedure Generate_quote(QuoteId: longint);
  end;

function InsuranceQuotePreviewFrm: TInsuranceQuotePreviewFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule, EASendMailObjLib_TLB, Main;

function InsuranceQuotePreviewFrm: TInsuranceQuotePreviewFrm;
begin
  Result := TInsuranceQuotePreviewFrm(UniMainModule.GetFormInstance(TInsuranceQuotePreviewFrm));
end;

procedure TInsuranceQuotePreviewFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TInsuranceQuotePreviewFrm.BtnDownloadClick(Sender: TObject);
begin
  UniSession.SendFile(_FileName);
end;

procedure TInsuranceQuotePreviewFrm.BtnSendMailClick(Sender: TObject);
var oSmtp: EASendMailObjLib_TLB.TMail;
    Body:  string;
    SourceName: string;
    Destname: string;
    Recipients: string;
begin
  //Update the record with the send date
  UniMainModule.QBatch.Close;

  UniMainModule.QBatch.SQL.Text :=
  'Update insurance_quotes set quote_mail_time=:quote_mail_time, ' +
  'flag_mailed=:flag_mailed ' +
  'where id=:id';

  UniMainModule.QBatch.ParamByName('Quote_mail_time').AsDateTime := Now;
  UniMainModule.QBatch.ParamByName('Flag_mailed').AsInteger      := 1;

  UniMainModule.QBatch.ParamByName('Id').AsInteger := _QuoteId;

  Try
    UniMainModule.QBatch.Execute;
    UniMainModule.UpdTr_Batch.Commit;
  Except
    if UniMainModule.UpdTr_Batch.Active
    then UniMainModule.UpdTr_Batch.Rollback;
  End;

  oSmtp := EASendMailObjLib_TLB.TMail.Create(UniApplication);
  oSmtp.SSL_init();

  oSmtp.LicenseCode        := 'ES-D1508812687-00323-4AC1DB277882BAF4-1878D6BC21AAA48U';

  oSmtp.ServerAddr    := 'mail.smtp2go.com';
  oSmtp.ServerPort    := 465;
  oSmtp.FromAddr      := 'info@cares-assistance.eu';
  oSmtp.From          := 'Cares Assistance Info';

  oSmtp.AddRecipientEx(_Demander_email, 0);

  Recipients := '';

  if Trim(_Contributor_email) <> ''
  then begin
         Recipients := _Contributor_email;
       end;

  if Trim(_Additional_email) <> ''
  then begin
         if Recipients <> ''
         then Recipients := Recipients + ';' + _Additional_email
         else Recipients := _Additional_email;
       end;

  if Trim(Recipients) <> ''
  then oSmtp.AddRecipientEx(Recipients, 1);

  UniMainModule.QWork.Close;

  UniMainModule.QWork.SQL.Text :=
  'Select * from mail_templates where Template_id=:template_id';
  UniMainModule.QWork.ParamByName('Template_id').AsInteger :=
  7024;

  UniMainModule.QWork.Open;

  if UniMainModule.QWork.RecordCount = 1
  then begin
         case _Language of
           0: begin
                oSmtp.Subject := UniMainModule.QWork.FieldByName('Title_dutch').asString;
                Body          := UniMainModule.QWork.FieldByName('Content_dutch').AsString;
              end;
           1: begin
                oSmtp.Subject := UniMainModule.QWork.FieldByName('Title_french').asString;
                Body          := UniMainModule.QWork.FieldByName('Content_french').AsString;
              end;
         end;
       end;

  UniMainModule.QWork.Close;

  oSmtp.BodyFormat         := 1;

  oSmtp.ImportHtml(Body, 'C:\Modules\WaveDeskInsurance');

  oSmtp.AddAttachment(_FileName);

  UniMainModule.QWork.Close;

  UniMainModule.QWork.SQL.Text :=
  'Select * from files_archive ' +
  'where Module_id=' + IntToStr(1000) +
  'and files_archive.removed=:removed ' +
  'and record_id=:recordid ' +
  'and files_archive.language_id=:language_id';

  UniMainModule.QWork.ParamByName('Removed').AsString := '0';
  UniMainModule.QWork.ParamByName('Recordid').AsInteger := 7024;
  UniMainModule.QWork.ParamByName('Language_id').AsInteger := _Language;

  UniMainModule.QWork.Open;

  UniMainModule.QWork.First;

  while not UniMainModule.QWork.Eof
  do begin
       SourceName := UniMainModule.Root_archive + UniMainModule.Company_account_name + '\';

       if Trim(UniMainModule.QWork.FieldByName('Folder').AsString) <> ''
       then SourceName := SourceName + UniMainModule.QWork.FieldByName('Folder').AsString + '\';

       if Trim(UniMainModule.QWork.FieldByName('SubFolder').AsString) <> ''
       then SourceName := SourceName + UniMainModule.QWork.FieldByName('SubFolder').AsString + '\';
       SourceName := SourceName + UniMainModule.QWork.FieldByName('Destination_name').AsString;
       DestName   := UniServerModule.NewCacheFile('', UniMainModule.QWork.FieldByName('Original_name').asString, '', True);

       if CopyFile(PChar(SourceName), PChar(DestName), False)
       then oSmtp.AddAttachment(DestName);

       UniMainModule.QWork.Next;
     end;

  UniMainModule.QWork.Close;

  oSmtp.UserName      := 'wavedesk.be';
  oSmtp.Password      := 'ILJ2ORQFDjZH';
  oSmtp.Protocol      := 0;

  if osmtp.SendMailToQueue  = 0
  then MainForm.WaveShowConfirmationToast(str_mail_send)
  else MainForm.WaveShowErrorToast(str_error_sending_mail);
  Close;
end;

procedure TInsuranceQuotePreviewFrm.FormCreate(Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInsuranceQuotePreviewFrm.Generate_quote(QuoteId: longint);
var AUrl:                                 string;
    _Quotedate:                           TDate;
    _Demander_name:                       string;
    _Demander_firstname:                  string;
    _Demander_companyname:                string;
    _Demander_date_of_birth:              TDate;
    _Demander_type:                       integer;
    _Start_date_insurance:                TDate;
    _Insured_name:                        string;
    _Insured_firstname:                   string;
    _Insured_date_of_birth:               TDate;
    _Capital1:                            currency;
    _Premium1:                            currency;
    _Capital2:                            currency;
    _Premium2:                            currency;
    _Capital3:                            currency;
    _Premium3:                            currency;
    _Premium_payment:                     integer;
    _Indexation:                          integer;
    _Contributor_id:                      longint;
    _Contributor_name:                    string;
    _Contributor_street:                  string;
    _Contributor_zipcode:                 string;
    _Contributor_city:                    string;
begin
  WindowState  := WsMaximized;

  _QuoteId := QuoteId;
  _Demander_email := '';
  _Contributor_email := '';
  _Additional_email := '';

  //Get all the data needed
  UniMainModule.QWork.Close;

  UniMainModule.QWork.SQL.Text :=
  'Select * from insurance_quotes where id=:id';

  UniMainModule.QWork.ParamByName('Id').AsInteger :=
  QuoteId;

  UniMainModule.QWork.Open;

  _QuoteDate :=
  UniMainModule.QWork.FieldByName('Quote_creation_time').AsDateTime;

  _Demander_name :=
  UniMainModule.QWork.FieldByName('Demander_name').AsString;

  _Demander_firstname :=
  UniMainModule.QWork.FieldByName('Demander_firstname').AsString;

  _Demander_companyname :=
  UniMainModule.QWork.FieldByName('Demander_company_name').AsString;

  _Demander_date_of_birth :=
  UniMainModule.QWork.FieldByName('Demander_date_of_birth').AsDateTime;

  _Demander_type :=
  UniMainModule.QWork.FieldByName('Demander_type').AsInteger;

  _Start_date_insurance :=
  UniMainModule.QWork.FieldByName('Start_date_insurance').AsDateTime;

  _Insured_name :=
  UniMainModule.QWork.FieldByName('Insured_name').AsString;

  _Insured_firstname :=
  UniMainModule.QWork.FieldByName('Insured_firstname').AsString;

  _Insured_date_of_birth :=
  UniMainModule.QWork.FieldByName('Insured_date_of_birth').AsDateTime;

  _Capital1 :=
  UniMainModule.QWork.FieldByName('Capital_1').AsCurrency;

  _Capital2 :=
  UniMainModule.QWork.FieldByName('Capital_2').AsCurrency;

  _Capital3 :=
  UniMainModule.QWork.FieldByName('Capital_3').AsCurrency;

  case UniMainModule.QWork.FieldByName('Premium_payment').AsInteger of
    0: begin
         _Premium1 :=
         UniMainModule.QWork.FieldByName('Gross_premium_monthly_1').AsCurrency;

         _Premium2 :=
         UniMainModule.QWork.FieldByName('Gross_premium_monthly_2').AsCurrency;

         _Premium3 :=
         UniMainModule.QWork.FieldByName('Gross_premium_monthly_3').AsCurrency;
       end;
    1: begin
         _Premium1 :=
         UniMainModule.QWork.FieldByName('Gross_premium_quarterly_1').AsCurrency;

         _Premium2 :=
         UniMainModule.QWork.FieldByName('Gross_premium_quarterly_2').AsCurrency;

         _Premium3 :=
         UniMainModule.QWork.FieldByName('Gross_premium_quarterly_3').AsCurrency;
       end;
    2: begin
         _Premium1 :=
         UniMainModule.QWork.FieldByName('Gross_premium_yearly_1').AsCurrency;

         _Premium2 :=
         UniMainModule.QWork.FieldByName('Gross_premium_yearly_2').AsCurrency;

         _Premium3 :=
         UniMainModule.QWork.FieldByName('Gross_premium_yearly_3').AsCurrency;
       end;
    3: begin
         _Premium1 :=
         UniMainModule.QWork.FieldByName('Purchase_price_1').AsCurrency;
         _Premium2 :=
         UniMainModule.QWork.FieldByName('Purchase_price_2').AsCurrency;
         _Premium3 :=
         UniMainModule.QWork.FieldByName('Purchase_price_3').AsCurrency;
       end
    else begin
           _Premium1 := 0;
           _Premium2 := 0;
           _Premium3 := 0;
         end;
  end;

  _Premium_payment :=
  UniMainModule.QWork.FieldByName('Premium_payment').AsInteger;

  _Demander_email :=
  UniMainModule.QWork.FieldByName('Demander_email').AsString;

  _Additional_email :=
  UniMainModule.QWork.FieldByName('Additional_email').AsString;

  _Contributor_id :=
  UniMainModule.QWork.FieldByName('Contributor_id').AsInteger;

  _Language :=
  UniMainModule.QWork.FieldByName('Language').AsInteger;

  _Indexation :=
  UniMainModule.QWork.FieldByName('Indexation').AsInteger;

  UniMainModule.QWork.Close;

  //Get the contributor data
  UniMainModule.QWork.SQL.Text :=
  'Select last_name, first_name, company_name, address_street, ' +
  'address_postal_code, address_city, email ' +
  'from credit_contributors where id=:id';

  UniMainModule.QWork.ParamByName('Id').AsInteger :=
  _Contributor_id;

  UniMainModule.QWork.Open;

  if Trim(UniMainModule.QWork.FieldByName('Company_name').AsString) <> ''
  then _Contributor_name :=
       UniMainModule.QWork.FieldByName('Company_name').AsString
  else _Contributor_name :=
       Trim(UniMainModule.QWork.FieldByName('Last_name').AsString + ' ' +
            UniMainModule.QWork.FieldByName('First_name').AsString);

  _Contributor_street :=
  UniMainModule.QWork.FieldByName('Address_street').AsString;

  _Contributor_zipcode :=
  UniMainModule.QWork.FieldByName('Address_postal_code').AsString;

  _Contributor_city :=
  UniMainModule.QWork.FieldByName('Address_city').AsString;

  _Contributor_email :=
  UniMainModule.QWork.FieldByName('Email').AsString;

  UniMainModule.QWork.Close;

  Report.Clear;

  if _Language = 0
  then begin
         if FileExists(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'Cares_quote_dutch.fr3')
         then Report.LoadFromFile(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'Cares_quote_dutch.fr3')
      end
  else if _Language = 1
       then begin
              if FileExists(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'Cares_quote_french.fr3')
              then Report.LoadFromFile(UniMainModule.Template_archive + UniMainModule.Company_account_name + '\' + 'Cares_quote_french.fr3');
            end;

  Report.Variables['StartDateInsurance']      := QuotedStr(FormatDateTime('dd/mm/yyyy', _start_date_insurance));
  if _Demander_type = 0
  then begin
         Report.Variables['NameInsuranceTaker']      := QuotedStr(_Demander_name);
         Report.Variables['FirstnameInsuranceTaker'] := QuotedStr(_Demander_firstname);
       end
  else if _Demander_type = 1
       then begin
              Report.Variables['NameInsuranceTaker']      := QuotedStr(_Demander_companyname);
              Report.Variables['FirstnameInsuranceTaker'] := QuotedStr('');
            end;

  if _Demander_date_of_birth = 0
  then Report.Variables['BirthDateInsuranceTaker'] := QuotedStr('')
  else Report.Variables['BirthDateInsuranceTaker'] := QuotedStr(FormatDateTime('dd/mm/yyyy', _Demander_date_of_birth));
  Report.Variables['NameInsuredPerson']       := QuotedStr(_Insured_name);
  Report.Variables['FirstnameInsuredPerson']  := QuotedStr(_Insured_firstname);
  Report.Variables['BirthDateInsuredPerson']  := QuotedStr(FormatDateTime('dd/mm/yyyy', _Insured_date_of_birth));
  Report.Variables['InsuredCapital']          := _Capital1;
  Report.Variables['Indexation']              := _Indexation;
  Report.Variables['MonthlyPremium']          := _Premium1;
  Report.Variables['InsuredCapital2']         := _Capital2;
  Report.Variables['MonthlyPremium2']         := _Premium2;
  Report.Variables['InsuredCapital3']         := _Capital3;
  Report.Variables['MonthlyPremium3']         := _Premium3;
  Report.Variables['QuoteDate']               := QuotedStr(FormatDateTime('dd/mm/yyyy', _QuoteDate));
  Report.Variables['PremiumPayment']          := _Premium_payment;
  Report.Variables['ContributorName']         := QuotedStr(_Contributor_name);
  Report.Variables['ContributorStreet']       := QuotedStr(_Contributor_street);
  Report.Variables['ContributorZipCode']      := QuotedStr(_Contributor_zipcode);
  Report.Variables['ContributorCity']         := QuotedStr(_Contributor_city);

  Report.PrintOptions.ShowDialog              := False;
  Report.ShowProgress                         := False;
  Report.EngineOptions.SilentMode             := True;
  Report.EngineOptions.EnableThreadSafe       := True;
  Report.EngineOptions.DestroyForms           := False;
  Report.EngineOptions.UseGlobalDataSetList   := False;
  Report.PreviewOptions.AllowEdit             := False;
  PdfExport.Background                        := True;
  PdfExport.ShowProgress                      := False;
  PdfExport.ShowDialog                        := False;
  PdfExport.DefaultPath                       := '';
  _FileName :=
  IntToStr(QuoteId) + ' - ' + Trim(_Insured_name) + ' ' + Trim(_Insured_firstname);

  _FileName :=
  UniServerModule.NewCacheFileUrl(False, 'pdf', _FileName, '', AUrl, True);

  PdfExport.FileName :=
  _FileName;

  Report.PrepareReport;

  Report.Export(PdfExport);

  UrlFrame.URL := AUrl;
end;

procedure TInsuranceQuotePreviewFrm.LinkedLanguageInsuranceQuotePreviewChangeLanguage(
  Sender: TObject);
begin
  UpdateStrings;
end;

procedure TInsuranceQuotePreviewFrm.UpdateStrings;
begin
  str_error_sending_mail := LinkedLanguageInsuranceQuotePreview.GetTextOrDefault('strstr_error_sending_mail' (* 'Fout bij het versturen van de e-mail' *) );
  str_mail_send := LinkedLanguageInsuranceQuotePreview.GetTextOrDefault('strstr_mail_send' (* 'E-mail verstuurd' *) );
end;

end.
