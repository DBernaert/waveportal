object InsuranceQuotePreviewFrm: TInsuranceQuotePreviewFrm
  Left = 0
  Top = 0
  ClientHeight = 834
  ClientWidth = 1312
  Caption = 'Offerte'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'border'
  LayoutConfig.Padding = '16 16 16 16'
  Images = UniMainModule.ImageList
  ImageIndex = 51
  OnCreate = FormCreate
  TextHeight = 15
  object URLFrame: TUniURLFrame
    Left = 0
    Top = 0
    Width = 1312
    Height = 799
    Hint = ''
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '0 0 16 0'
    Align = alClient
    TabOrder = 0
    ParentColor = False
    Color = clBtnFace
  end
  object PanelFooter: TUniContainerPanel
    Left = 0
    Top = 799
    Width = 1312
    Height = 35
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 1
    Layout = 'border'
    object ContainerButtons: TUniContainerPanel
      Left = 807
      Top = 0
      Width = 505
      Height = 35
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      object BtnClose: TUniThemeButton
        Left = 368
        Top = 0
        Width = 137
        Height = 33
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        ScreenMask.Target = Owner
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
      object BtnSendMail: TUniThemeButton
        Left = 0
        Top = 0
        Width = 177
        Height = 33
        Hint = ''
        Caption = 'Versturen e-mail'
        TabOrder = 0
        Images = UniMainModule.ImageList
        ImageIndex = 74
        OnClick = BtnSendMailClick
        ButtonTheme = uctPrimary
      end
      object BtnDownload: TUniThemeButton
        Left = 184
        Top = 0
        Width = 177
        Height = 33
        Hint = ''
        Caption = 'Downloaden'
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 83
        OnClick = BtnDownloadClick
        ButtonTheme = uctPrimary
      end
    end
  end
  object PdfExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    EmbedFontsIfProtected = False
    InteractiveFormsFontSubset = 'A-Z,a-z,0-9,#43-#47 '
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'WAVEDESK'
    Subject = 'WAVEDESK'
    Creator = 'WAVEDESK'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    PDFStandard = psNone
    PDFVersion = pv17
    Left = 640
    Top = 272
  end
  object Report: TfrxReport
    Version = '2023.1.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43910.444921006940000000
    ReportOptions.LastChange = 43910.444921006940000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 640
    Top = 336
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object LinkedLanguageInsuranceQuotePreview: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST'
      'TWIDESTRINGS')
    SmartExcludeProps.Strings = (
      'Report.ScriptText'
      'TInsuranceQuotePreviewFrm.Layout'
      'PanelFooter.Layout'
      'ContainerButtons.Layout'
      'PdfExport.Author'
      'PdfExport.Creator'
      'PdfExport.InteractiveFormsFontSubset'
      'PdfExport.Subject'
      'Report.IniFile'
      'Report.ScriptLanguage'
      'Report.Version')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageInsuranceQuotePreviewChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 640
    Top = 424
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A00540049006E0073007500720061006E006300650051007500
      6F00740065005000720065007600690065007700460072006D0001004F006600
      6600650072007400650001004F0066006600720065000100510075006F007400
      650001000D000A00420074006E0043006C006F0073006500010053006C007500
      6900740065006E0001004600650072006D0065007200010043006C006F007300
      650001000D000A00420074006E00530065006E0064004D00610069006C000100
      560065007200730074007500720065006E00200065002D006D00610069006C00
      010045006E0076006F007900650072002000700061007200200065002D006D00
      610069006C000100530065006E00640020006200790020006D00610069006C00
      01000D000A00420074006E0044006F0077006E006C006F006100640001004400
      6F0077006E006C006F006100640065006E0001005400E9006C00E90063006800
      61007200670065007200010044006F0077006E006C006F006100640001000D00
      0A0073007400480069006E00740073005F0055006E00690063006F0064006500
      0D000A007300740044006900730070006C00610079004C006100620065006C00
      73005F0055006E00690063006F00640065000D000A007300740046006F006E00
      740073005F0055006E00690063006F00640065000D000A00730074004D007500
      6C00740069004C0069006E00650073005F0055006E00690063006F0064006500
      0D000A007300740053007400720069006E00670073005F0055006E0069006300
      6F00640065000D000A007300740072007300740072005F006D00610069006C00
      5F00730065006E006400010045002D006D00610069006C002000760065007200
      730074007500750072006400010045002D006D00610069006C00200065006E00
      76006F007900E900010045002D006D00610069006C002000730065006E006400
      01000D000A007300740072007300740072005F006500720072006F0072005F00
      730065006E00640069006E0067005F006D00610069006C00010046006F007500
      74002000620069006A0020006800650074002000760065007200730074007500
      720065006E002000760061006E00200064006500200065002D006D0061006900
      6C00010045007200720065007500720020006C006F0072007300200064006500
      20006C00270065006E0076006F00690020006400650020006C00270065002D00
      6D00610069006C0001004500720072006F0072002000730065006E0064006900
      6E0067002000740068006500200065006D00610069006C0001000D000A007300
      74004F00740068006500720053007400720069006E00670073005F0055006E00
      690063006F00640065000D000A007300740043006F006C006C00650063007400
      69006F006E0073005F0055006E00690063006F00640065000D000A0073007400
      430068006100720053006500740073005F0055006E00690063006F0064006500
      0D000A00}
  end
end
