object InvalidSepaFrm: TInvalidSepaFrm
  Left = 0
  Top = 0
  Width = 1328
  Height = 822
  OnCreate = FormCreate
  Layout = 'border'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1328
    Height = 33
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    LayoutConfig.Region = 'north'
    object LblTitle: TUniLabel
      Left = 0
      Top = 0
      Width = 245
      Height = 30
      Hint = ''
      Caption = 'Ongeldige sepa mandaten'
      ParentFont = False
      Font.Height = -21
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
          '= "lfp-pagetitle";'#13#10'}')
      TabOrder = 1
      LayoutConfig.Cls = 'boldtext'
    end
  end
  object HeaderList: TUniContainerPanel
    Left = 0
    Top = 33
    Width = 1328
    Height = 30
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerFilterRightBackLog: TUniContainerPanel
      Left = 1300
      Top = 0
      Width = 28
      Height = 30
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      object BtnExportListBackLog: TUniThemeButton
        Left = 0
        Top = 0
        Width = 28
        Height = 28
        Hint = 'Exporteren lijst'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 31
        OnClick = BtnExportListBackLogClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object GridInvalidList: TUniDBGrid
    Left = 0
    Top = 63
    Width = 1328
    Height = 759
    Hint = ''
    DataSource = DsInvalidList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Cls = 'customGrid'
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 0 0 0'
    Align = alClient
    TabOrder = 2
    OnDrawColumnCell = GridInvalidListDrawColumnCell
    Columns = <
      item
        FieldName = 'INSURANCE_COMPANY_NAME'
        Title.Caption = 'Maatschappij'
        Width = 130
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'INSURANCE_FILE_NUMBER'
        Title.Caption = 'Polisnummer'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'START_DATE_INSURANCE'
        Title.Alignment = taCenter
        Title.Caption = 'Ingangsdatum'
        Width = 100
        Alignment = taCenter
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'INSURANCE_TAKER'
        Title.Caption = 'Verzekeringsnemer'
        Width = 184
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'INSURED_PERSON'
        Title.Caption = 'Verzekerde'
        Width = 184
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'PHONE_MOBILE'
        Title.Caption = 'Gsm'
        Width = 135
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'PREMIUM'
        Title.Alignment = taRightJustify
        Title.Caption = 'Premie'
        Width = 85
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'AGENT_NAME'
        Title.Caption = 'Agent'
        Width = 370
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'STRUCTURED_STATEMENT'
        Title.Caption = 'Gestructureerde mededeling'
        Width = 184
        Visible = False
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'Reason_invalid_description'
        Title.Caption = 'Reden'
        Width = 230
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'DELTA'
        Title.Alignment = taCenter
        Title.Caption = 'Termijnen'
        Width = 80
        Alignment = taCenter
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object InvalidList: TIBCQuery
    DMLRefresh = True
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'select'
      '    insf.id,'
      '    insf.contributor_id,'
      '    insf.start_date_insurance,'
      '    insf.insurance_file_number,'
      '    insf.structured_statement,'
      '    insf.reason_twikey_invalid,'
      '    insc.name insurance_company_name,'
      
        '    coalesce(agent.last_name, '#39#39') || '#39' '#39' || coalesce(agent.first' +
        '_name, '#39#39') agent_name,'
      '    CASE'
      '      WHEN insf.premium_payment = 0 THEN insf.premium_amount'
      '      WHEN insf.premium_payment = 1 THEN insf.premium_quarterly'
      '      WHEN insf.premium_payment = 2 THEN insf.premium_semesterly'
      '      WHEN insf.premium_payment = 3 THEN insf.premium_yearly'
      '    end premium,'
      
        '    coalesce(crmcontact.last_name, '#39#39') || '#39' '#39' || coalesce(crmcon' +
        'tact.first_name, '#39#39') insurance_taker,'
      '    crmcontact.phone_mobile,'
      
        '    (select first 1 coalesce(inspcontact.last_name, '#39#39') || '#39' '#39' |' +
        '| coalesce(inspcontact.first_name, '#39#39') insured_person from insur' +
        'ance_insured_persons insperson'
      
        '     left outer join crm_contacts inspcontact on (insperson.insu' +
        'red_person_contact_id = inspcontact.id)'
      
        '     where insperson.insurance_file = insf.id and inspcontact.bi' +
        'rthdate < dateadd(year, -18, current_date)),'
      
        '    (select first 1 payment_period_date from insurance_payments ' +
        'where insurance_payments.insurance_file = insf.id order by insur' +
        'ance_payments.payment_period_date desc) lastpayment,'
      '    CASE'
      '      WHEN insf.premium_payment = 0'
      
        '      THEN datediff(month, (select first 1 payment_period_date f' +
        'rom insurance_payments where insurance_payments.insurance_file =' +
        ' insf.id order by insurance_payments.payment_period_date desc), ' +
        'current_date)'
      '      WHEN insf.premium_payment = 1'
      
        '      THEN datediff(month, (select first 1 payment_period_date f' +
        'rom insurance_payments where insurance_payments.insurance_file =' +
        ' insf.id order by insurance_payments.payment_period_date desc), ' +
        'dateadd(month, 3, current_date))'
      '      WHEN insf.premium_payment = 2'
      
        '      THEN datediff(month, (select first 1 payment_period_date f' +
        'rom insurance_payments where insurance_payments.insurance_file =' +
        ' insf.id order by insurance_payments.payment_period_date desc), ' +
        'dateadd(month, 6, current_date))'
      '      WHEN insf.premium_payment = 3'
      
        '      THEN datediff(year, (select first 1 payment_period_date fr' +
        'om insurance_payments where insurance_payments.insurance_file = ' +
        'insf.id order by insurance_payments.payment_period_date desc), d' +
        'ateadd(month, 12, current_date))'
      '    END delta'
      'from insurance_files insf'
      
        '   left outer join insurance_institutions insc on (insf.insuranc' +
        'e_company = insc.id)'
      
        '   left outer join crm_contacts crmcontact on (insf.insurance_ta' +
        'ker = crmcontact.id)'
      
        '   left outer join credit_contributors agent on (insf.agent_id =' +
        ' agent.id)'
      'where'
      '    insf.removed='#39'0'#39
      '  and'
      '&condition '
      '  and'
      '    insf.start_date_insurance <= current_date'
      '  and'
      '    insf.cancelled = 0'
      '  and '
      
        '    (insf.premium_payment=0 or insf.premium_payment=1 or insf.pr' +
        'emium_payment=2 or insf.premium_payment=3)'
      '  and'
      '    insf.twikey_invalid = 1'
      'order by datediff'
      
        '     (month, (select first 1 payment_period_date from insurance_' +
        'payments where insurance_payments.insurance_file = insf.id order' +
        ' by insurance_payments.payment_period_date desc), current_date) ' +
        'desc')
    MasterFields = 'CONTRIBUTOR_ID'
    DetailFields = 'CONTRIBUTOR_ID'
    FetchAll = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = InvalidListCalcFields
    Left = 648
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'contributor_id'
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'contributor_id'
        Value = nil
      end>
    MacroData = <
      item
        Name = 'condition'
        Value = 
          '(insf.contributor_id=:contributor_id or insf.agent_id=:contribut' +
          'or_id)'
      end>
    object InvalidListID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InvalidListCONTRIBUTOR_ID: TLargeintField
      FieldName = 'CONTRIBUTOR_ID'
    end
    object InvalidListSTART_DATE_INSURANCE: TDateField
      FieldName = 'START_DATE_INSURANCE'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object InvalidListINSURANCE_FILE_NUMBER: TWideStringField
      FieldName = 'INSURANCE_FILE_NUMBER'
    end
    object InvalidListPHONE_MOBILE: TWideStringField
      FieldName = 'PHONE_MOBILE'
      ReadOnly = True
    end
    object InvalidListINSURANCE_TAKER: TWideStringField
      FieldName = 'INSURANCE_TAKER'
      ReadOnly = True
      Size = 61
    end
    object InvalidListINSURED_PERSON: TWideStringField
      FieldName = 'INSURED_PERSON'
      ReadOnly = True
      Size = 61
    end
    object InvalidListPREMIUM: TFloatField
      FieldName = 'PREMIUM'
      ReadOnly = True
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvalidListAGENT_NAME: TWideStringField
      FieldName = 'AGENT_NAME'
      ReadOnly = True
      Size = 61
    end
    object InvalidListINSURANCE_COMPANY_NAME: TWideStringField
      FieldName = 'INSURANCE_COMPANY_NAME'
      ReadOnly = True
      Size = 50
    end
    object InvalidListSTRUCTURED_STATEMENT: TWideStringField
      FieldName = 'STRUCTURED_STATEMENT'
      Size = 30
    end
    object InvalidListREASON_TWIKEY_INVALID: TSmallintField
      FieldName = 'REASON_TWIKEY_INVALID'
    end
    object InvalidListReason_invalid_description: TStringField
      FieldKind = fkCalculated
      FieldName = 'Reason_invalid_description'
      Size = 60
      Calculated = True
    end
    object InvalidListLASTPAYMENT: TDateField
      FieldName = 'LASTPAYMENT'
      ReadOnly = True
    end
    object InvalidListDELTA: TLargeintField
      FieldName = 'DELTA'
      ReadOnly = True
    end
  end
  object DsInvalidList: TDataSource
    DataSet = InvalidList
    Left = 648
    Top = 240
  end
  object LinkedLanguageInvalidSepa: TsiLangLinked
    Version = '7.9.2'
    StringsTypes.Strings = (
      'TIB_STRINGLIST'
      'TSTRINGLIST'
      'TWIDESTRINGS')
    SmartExcludeProps.Strings = (
      'InvalidListID.DisplayLabel'
      'InvalidListCONTRIBUTOR_ID.DisplayLabel'
      'InvalidListSTART_DATE_INSURANCE.DisplayLabel'
      'InvalidListINSURANCE_FILE_NUMBER.DisplayLabel'
      'InvalidListPHONE_MOBILE.DisplayLabel'
      'InvalidListINSURANCE_TAKER.DisplayLabel'
      'InvalidListINSURED_PERSON.DisplayLabel'
      'InvalidListPREMIUM.DisplayLabel'
      'InvalidListAGENT_NAME.DisplayLabel'
      'InvalidListINSURANCE_COMPANY_NAME.DisplayLabel'
      'InvalidListSTRUCTURED_STATEMENT.DisplayLabel'
      'InvalidListREASON_TWIKEY_INVALID.DisplayLabel'
      'InvalidListReason_invalid_description.DisplayLabel'
      'InvalidListLASTPAYMENT.DisplayLabel'
      'InvalidListDELTA.DisplayLabel'
      'TInvalidSepaFrm.Layout'
      'ContainerHeader.Layout'
      'HeaderList.Layout'
      'ContainerFilterRightBackLog.Layout'
      'InvalidList.DetailFields')
    NumOfLanguages = 3
    LangDispatcher = UniMainModule.LanguageDispatcher
    OnChangeLanguage = LinkedLanguageInvalidSepaChangeLanguage
    LangDelim = 1
    LangNames.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    Language = 'Nederlands'
    ExcludedProperties.Strings = (
      'Category'
      'SecondaryShortCuts'
      'HelpKeyword'
      'InitialDir'
      'HelpKeyword'
      'ActivePage'
      'ImeName'
      'DefaultExt'
      'FileName'
      'FieldName'
      'PickList'
      'DisplayFormat'
      'EditMask'
      'KeyList'
      'LookupDisplayFields'
      'DropDownSpecRow'
      'TableName'
      'DatabaseName'
      'IndexName'
      'MasterFields'
      'SQL'
      'DeleteSQL'
      'UpdateSQL'
      'ModifySQL'
      'KeyFields'
      'LookupKeyFields'
      'LookupResultField'
      'DataField'
      'KeyField'
      'ListField')
    Left = 648
    Top = 296
    TranslationData = {
      73007400430061007000740069006F006E0073005F0055006E00690063006F00
      640065000D000A004C0062006C005400690074006C00650001004F006E006700
      65006C0064006900670065002000730065007000610020006D0061006E006400
      6100740065006E0001004D0061006E0064006100740073002000730065007000
      6100200069006E00760061006C006900640065007300010049006E0076006100
      6C00690064002000730065007000610020006D0061006E006400610074006500
      730001000D000A0073007400480069006E00740073005F0055006E0069006300
      6F00640065000D000A00420074006E004500780070006F00720074004C006900
      730074004200610063006B004C006F00670001004500780070006F0072007400
      6500720065006E0020006C0069006A007300740001004500780070006F007200
      74006500720020006C00610020006C0069007300740065000100450078007000
      6F007200740020006C0069007300740001000D000A0073007400440069007300
      70006C00610079004C006100620065006C0073005F0055006E00690063006F00
      640065000D000A007300740046006F006E00740073005F0055006E0069006300
      6F00640065000D000A00730074004D0075006C00740069004C0069006E006500
      73005F0055006E00690063006F00640065000D000A0073007400530074007200
      69006E00670073005F0055006E00690063006F00640065000D000A0073007400
      72007300740072005F006300720065006400690074006F0072005F0062006C00
      6F0063006B00650064005F00620079005F0064006500620074006F0072000100
      53006300680075006C0064006500690073006500720020006700650062006C00
      6F006B006B006500650072006400200064006F006F0072002000640065006200
      69007400650075007200010043007200E90061006E0063006900650072002000
      62006C006F0071007500E900200070006100720020006C00650020006400E900
      62006900740065007500720001004300720065006400690074006F0072002000
      62006C006F0063006B0065006400200062007900200064006500620074006F00
      720001000D000A007300740072007300740072005F0064006500620074006F00
      72005F006100630063006F0075006E0074005F0062006C006F0063006B006500
      640001004400650062006900740065007500720065006E00720065006B006500
      6E0069006E00670020006700650062006C006F006B006B006500650072006400
      010043006F006D0070007400650020006400E900620069007400650075007200
      200062006C006F0071007500E900010044006500620074006F00720020006100
      630063006F0075006E007400200062006C006F0063006B006500640001000D00
      0A007300740072007300740072005F0064006500620074006F0072005F006100
      630063006F0075006E0074005F0063006C006F00730065006400010044006500
      62006900740065007500720065006E00720065006B0065006E0069006E006700
      20006700650073006C006F00740065006E00010043006F006D00700074006500
      20006400E900620069007400650075007200200063006C00F400740075007200
      E900010044006500620074006F00720020006100630063006F0075006E007400
      200063006C006F0073006500640001000D000A00730074007200730074007200
      5F0069006E00760061006C00690064005F007400770069006B00650079005F00
      6D0061006E006400610074006500730001004F006E00670065006C0064006900
      6700650020007300650070006100270073000100530065007000610073002000
      6E006F006E002000760061006C006900640065007300010049006E0076006100
      6C006900640020007300650070006100730001000D000A007300740072007300
      740072005F006E006F005F00760061006C00690064005F006D0061006E006400
      61007400650001004700650065006E002000670065006C006400690067002000
      6D0061006E0064006100610074000100500061007300200075006E0020006D00
      61006E006400610074002000760061006C0069006400650001004E006F007400
      200061002000760061006C006900640020006D0061006E006400610074006500
      01000D000A00730074004F00740068006500720053007400720069006E006700
      73005F0055006E00690063006F00640065000D000A007300740043006F006C00
      6C0065006300740069006F006E0073005F0055006E00690063006F0064006500
      0D000A00470072006900640049006E00760061006C00690064004C0069007300
      74002E0043006F006C0075006D006E0073005B0037005D002E00540069007400
      6C0065002E00430061007000740069006F006E0001004100670065006E007400
      01004100670065006E00740001004100670065006E00740001000D000A004700
      72006900640049006E00760061006C00690064004C006900730074002E004300
      6F006C0075006D006E0073005B0038005D002E005400690074006C0065002E00
      430061007000740069006F006E00010047006500730074007200750063007400
      750072006500650072006400650020006D00650064006500640065006C006900
      6E006700010043006F006D006D0075006E00690063006100740069006F006E00
      200073007400720075006300740075007200E900650001005300740072007500
      630074007500720065006400200063006F006D006D0075006E00690063006100
      740069006F006E0001000D000A00470072006900640049006E00760061006C00
      690064004C006900730074002E0043006F006C0075006D006E0073005B003500
      5D002E005400690074006C0065002E00430061007000740069006F006E000100
      470073006D000100470073006D0001004D006F00620069006C00650001000D00
      0A00470072006900640049006E00760061006C00690064004C00690073007400
      2E0043006F006C0075006D006E0073005B0032005D002E005400690074006C00
      65002E00430061007000740069006F006E00010049006E00670061006E006700
      730064006100740075006D000100440061007400650020006400650020006400
      E90062007500740001005300740061007200740069006E006700200064006100
      7400650001000D000A00470072006900640049006E00760061006C0069006400
      4C006900730074002E0043006F006C0075006D006E0073005B0030005D002E00
      5400690074006C0065002E00430061007000740069006F006E0001004D006100
      6100740073006300680061007000700069006A00010053006F0063006900E900
      7400E900010043006F006D00700061006E00790001000D000A00470072006900
      640049006E00760061006C00690064004C006900730074002E0043006F006C00
      75006D006E0073005B0031005D002E005400690074006C0065002E0043006100
      7000740069006F006E00010050006F006C00690073006E0075006D006D006500
      720001004E002E00200064006500200070006F006C0069006300650001005000
      6F006C0069006300790020006E0075006D0062006500720001000D000A004700
      72006900640049006E00760061006C00690064004C006900730074002E004300
      6F006C0075006D006E0073005B0036005D002E005400690074006C0065002E00
      430061007000740069006F006E0001005000720065006D006900650001005000
      720069006D00650001005000720065006D00690075006D0001000D000A004700
      72006900640049006E00760061006C00690064004C006900730074002E004300
      6F006C0075006D006E0073005B0039005D002E005400690074006C0065002E00
      430061007000740069006F006E00010052006500640065006E00010052006100
      690073006F006E00010052006500610073006F006E0001000D000A0047007200
      6900640049006E00760061006C00690064004C006900730074002E0043006F00
      6C0075006D006E0073005B00310030005D002E005400690074006C0065002E00
      430061007000740069006F006E0001005400650072006D0069006A006E006500
      6E0001005400650072006D006500730001005400650072006D00730001000D00
      0A00470072006900640049006E00760061006C00690064004C00690073007400
      2E0043006F006C0075006D006E0073005B0037005D002E004300680065006300
      6B0042006F0078004600690065006C0064002E004600690065006C0064005600
      61006C00750065007300010074007200750065003B00660061006C0073006500
      010074007200750065003B00660061006C007300650001007400720075006500
      3B00660061006C007300650001000D000A00470072006900640049006E007600
      61006C00690064004C006900730074002E0043006F006C0075006D006E007300
      5B00310030005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A00470072006900640049006E00760061006C00690064004C0069007300
      74002E0043006F006C0075006D006E0073005B0039005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640049006E00
      760061006C00690064004C006900730074002E0043006F006C0075006D006E00
      73005B0038005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A00470072006900640049006E00760061006C00690064004C0069007300
      74002E0043006F006C0075006D006E0073005B0033005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640049006E00
      760061006C00690064004C006900730074002E0043006F006C0075006D006E00
      73005B0030005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A00470072006900640049006E00760061006C00690064004C0069007300
      74002E0043006F006C0075006D006E0073005B0031005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640049006E00
      760061006C00690064004C006900730074002E0043006F006C0075006D006E00
      73005B0032005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A00470072006900640049006E00760061006C00690064004C0069007300
      74002E0043006F006C0075006D006E0073005B0035005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640049006E00
      760061006C00690064004C006900730074002E0043006F006C0075006D006E00
      73005B0036005D002E0043006800650063006B0042006F007800460069006500
      6C0064002E004600690065006C006400560061006C0075006500730001007400
      7200750065003B00660061006C0073006500010074007200750065003B006600
      61006C0073006500010074007200750065003B00660061006C00730065000100
      0D000A00470072006900640049006E00760061006C00690064004C0069007300
      74002E0043006F006C0075006D006E0073005B0034005D002E00430068006500
      63006B0042006F0078004600690065006C0064002E004600690065006C006400
      560061006C00750065007300010074007200750065003B00660061006C007300
      6500010074007200750065003B00660061006C00730065000100740072007500
      65003B00660061006C007300650001000D000A00470072006900640049006E00
      760061006C00690064004C006900730074002E0043006F006C0075006D006E00
      73005B0034005D002E005400690074006C0065002E0043006100700074006900
      6F006E0001005600650072007A0065006B006500720064006500010050006500
      720073006F006E006E006500200061007300730075007200E900650001004900
      6E0073007500720065006400200070006500720073006F006E0001000D000A00
      470072006900640049006E00760061006C00690064004C006900730074002E00
      43006F006C0075006D006E0073005B0033005D002E005400690074006C006500
      2E00430061007000740069006F006E0001005600650072007A0065006B006500
      720069006E00670073006E0065006D0065007200010041007300730075007200
      E900010050006F006C0069006300790068006F006C0064006500720001000D00
      0A0073007400430068006100720053006500740073005F0055006E0069006300
      6F00640065000D000A00}
  end
end
